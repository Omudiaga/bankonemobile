﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankOneMobile.LoanOrigination.WindowsService
{

    public class Response
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }

    public static class Extension
    {
        public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
    public class Extensions
    {
    }
}
