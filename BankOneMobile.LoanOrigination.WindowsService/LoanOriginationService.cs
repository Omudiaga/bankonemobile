﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BankOneMobile.LoanOrigination.WindowsService
{
    public partial class LoanOriginationService : ServiceBase
    {
       LoanOriginationEngine engine = null;
       public LoanOriginationService()
        {
            engine = new LoanOriginationEngine();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (engine != null)
            {
                engine.Start();
            }
        }

        protected override void OnStop()
        {
            if (engine != null)
            {
                engine.Stop();
            }
        }
    }
}
