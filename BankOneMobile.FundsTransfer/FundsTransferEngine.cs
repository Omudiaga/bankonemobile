﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.FundsTransfer
{
    public class FundsTransferEngine
    {
        private Timer timer = null;
        private bool isBusy = false;
        public FundsTransferEngine()
        {
            System.Threading.Thread thWorker = new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate
                {
                    try
                    {
                        isBusy = true;
                        System.Diagnostics.Trace.TraceInformation("Trace Just Began");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Trace Just Began"));

                        BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                        // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                        //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                        ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                        ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                        ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                        ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                        System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                        ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                        ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                        ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                        ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                        System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                        BankOneMobile.Extension.ApplicationInitializer.Init();

                        System.Diagnostics.Trace.TraceInformation("Init Succesfull"); 
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                        throw;
                    }
                    finally
                    {
                        isBusy = false;
                    }
                }
                ));
            thWorker.Start();
            this.timer = new Timer();
            this.timer.Elapsed+=new ElapsedEventHandler(this.timer_Elapsed);
        }

        public void Start()
        {
            this.timer.Start();
        }
        public void Stop()
        {
            if (timer != null)
            {
                this.timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!isBusy)
            {
                this.timer.Stop();
                try
                {
                    try
                    {
                        #region Requery Diamond bank Transactions
                        Trace.TraceInformation(String.Format("**************************************************************"));
                            Trace.TraceInformation(String.Format("BankOneMobile Requery Transaction Service. "));
                            string mfbCode = ConfigurationManager.AppSettings["InstitutionCode"];
                            List<IDBNFundsTransferTransactionType> pendingTransfers = new List<IDBNFundsTransferTransactionType>();
                            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                            {
                                pendingTransfers = new DBNFundsTransferTransactionTypeSystem().GetPendingTransfersToDiamondBank(_theDataSource, mfbCode);
                            }
                            Trace.TraceInformation(String.Format("Gotten Pending Transfers. {0} found... ", pendingTransfers.Count));
                        if(pendingTransfers != null && pendingTransfers.Count > 0)
                        {
                            foreach (var pendingTransfer in pendingTransfers)
                            {
                                string requeryResponse = string.Empty;
                                Trace.TraceInformation(String.Format("About to get {0} status from diamond bank.", pendingTransfer.PaymentReference));

                                try
                                {
                                    using (DiamondBankCBARef.diamondbankoneConnectSoapClient diamondBankClient = new DiamondBankCBARef.diamondbankoneConnectSoapClient())
                                    {
                                        requeryResponse = diamondBankClient.requeryTransaction(pendingTransfer.PaymentReference);
                                    }
                                }
                                catch(Exception ex)
                                {
                                    Trace.TraceError(string.Format("Web service error in requery: {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Requery Web Service Error: {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                            client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                emails, string.Format("Requery Web service Error On BankOneMobile:{0}", ex.Message), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                }
                                Trace.TraceInformation(String.Format("Gotten {0} status from diamond bank [{1}]", pendingTransfer.PaymentReference, requeryResponse));
                                #region Pending On Diamond bank
                                //if transfer is still pending or no response was recieved, we skip to the next transaction
                                if (String.IsNullOrEmpty(requeryResponse) || (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "pending".ToLower()))
                                {
                                    continue;
                                }
                                #endregion

                                #region Successful On Diamond bank
                                //if transfer is successful, we update transaction to successful
                                if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "successful".ToLower())
                                {
                                    pendingTransfer.TransferStatus = FundsTransferStatus.Successful;
                                    pendingTransfer.ResponseCode = "00";
                                    pendingTransfer.ResponseDate = DateTime.Now;

                                    try
                                    {
                                        Trace.TraceInformation(String.Format("About to update {0} status on bankone Mobile.", pendingTransfer.PaymentReference));
                                        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                        {
                                            new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, (DBNFundsTransferTransactionType)pendingTransfer);
                                        }
                                        Trace.TraceInformation(String.Format("{0} Update done.", pendingTransfer.PaymentReference));
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(string.Format("Transaction with Reference No. {0} was successful on Diamond Bank but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("Transaction with Reference No. {0} was successful on Diamond Bank but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("DBN Funds Transfer-Error Updating Successful Transfer On BankOneMobile:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                #region Failed On Diamond bank
                                //if transfer is successful, we update transaction to successful
                                if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "failed".ToLower())
                                {
                                    //we ensure this Reversal has not been previously posted on corebanking
                                    Trace.TraceError(String.Format("Now requerying transaction: {0}", "R"+pendingTransfer.PaymentReference));
                                    Services.SwitchServiceRef.Response rp = new Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            rp = client.RequeryTransactionStatus(mfbCode, "R" + pendingTransfer.PaymentReference);
                                        }
                                        Trace.TraceError(String.Format("Done requerying transaction: {0}. status: {1}", "R"+pendingTransfer.PaymentReference, rp.IsSuccessful));
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(String.Format("Web Service Error in Requery: {0}", ex.Message));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-FT Service SwitchingService Trx Requery Error:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }

                                    if(rp.IsSuccessful)
                                    {
                                        pendingTransfer.ReversalStatus = DBNReversalStatus.Successful;
                                        pendingTransfer.TransferStatus = FundsTransferStatus.Failed;
                                        pendingTransfer.ResponseCode = "06";
                                        pendingTransfer.ResponseDate = DateTime.Now;

                                        try
                                        {
                                            Trace.TraceInformation(String.Format("About to update {0} status on bankone Mobile.", pendingTransfer.PaymentReference));
                                            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                            {
                                                new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, (DBNFundsTransferTransactionType)pendingTransfer);
                                            }
                                            Trace.TraceInformation(String.Format("{0} Update done.", pendingTransfer.PaymentReference));
                                            continue;
                                        }
                                        catch (Exception ex)
                                        {
                                            Trace.TraceError(string.Format("Reversal with Reference No. {0} was successful but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", "R"+pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                            #region send error mail
                                            try
                                            {
                                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                                {
                                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                    string body = string.Format("Reversal with Reference No. {0} was successful but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", "R" + pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                    client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                        emails, string.Format("DBN Funds Transfer-Reversal Error Updating FAILED Transfer On BankOneMobile:{0}", ex.Message), body);
                                                }
                                            }
                                            catch (Exception emailException)
                                            {
                                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                            }
                                            #endregion
                                            continue;
                                        }
                                    }

                                    Agent agent = null;
                                    using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                    {
                                        agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(pendingTransfer.AgentPhoneNumber) as Agent;
                                    }
                                    if (agent == null)
                                    {
                                        throw new Exception(string.Format("Could not retreive agent with phonenumber: {0}", pendingTransfer.AgentPhoneNumber));
                                    }

                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of funds Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1}. Reference is {2} ", pendingTransfer.Amount, pendingTransfer.OriginatorAccountNumber, pendingTransfer.PaymentReference));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mfbCode, pendingTransfer.OriginatorAccountNumber, pendingTransfer.Amount, pendingTransfer.Fee, pendingTransfer.VATInPercentage, "R"+ pendingTransfer.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch(Exception ex)
                                    {
                                        Trace.TraceError(string.Format("Web service Error reversing transaction with Reference No. {0}. Error: {1}\n{2}\n{3}", pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("Web service Error reversing transaction with Reference No. {0}. Error: {1}\n{2}\n{3}", pendingTransfer.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("DBN Funds Transfer-Web Service Error Reversing FAILED Transfer On Corebanking:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                        continue;
                                    }

                                    Trace.TraceInformation(string.Format("Done reversing funds. Ref: {0}, Status: {1} ",pendingTransfer.PaymentReference, reversalResponse.IsSuccessful));
                                    if (reversalResponse.IsSuccessful)
                                    {
                                        pendingTransfer.ReversalStatus = DBNReversalStatus.Successful;
                                        pendingTransfer.TransferStatus = FundsTransferStatus.Failed;
                                        pendingTransfer.ResponseCode = "06";
                                        pendingTransfer.ResponseDate = DateTime.Now;
                                    }
                                    else
                                    {
                                        pendingTransfer.ReversalStatus = DBNReversalStatus.NotSuccessful;
                                        pendingTransfer.TransferStatus = FundsTransferStatus.Failed;
                                        pendingTransfer.ResponseCode = "06";
                                        pendingTransfer.ResponseDate = DateTime.Now;
                                    }

                                    try
                                    {
                                        Trace.TraceInformation(String.Format("About to update {0} status on bankone Mobile.", pendingTransfer.PaymentReference));
                                        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                        {
                                            new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, (DBNFundsTransferTransactionType)pendingTransfer);
                                        }
                                        Trace.TraceInformation(String.Format("{0} Update done.", pendingTransfer.PaymentReference));
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(string.Format("Transaction with Reference No. {0} was NOT successful on Diamond Bank but an error occured when trying to update its status on bankonemobile.Reversal of funds on Core banking was {1}. Error: {2}\n{3}\n{4}", pendingTransfer.PaymentReference, reversalResponse.IsSuccessful, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("Transaction with Reference No. {0} was NOT successful on Diamond Bank but an error occured when trying to update its status on bankonemobile.Reversal of funds on Core banking was {1}. Error: {2}\n{3}\n{4}", pendingTransfer.PaymentReference, reversalResponse.IsSuccessful, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("DBN Funds Transfer-Error Updating FAILED Transfer On BankOneMobile:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                            }
                            
                        }

                        
                        Trace.TraceInformation(String.Format("BankOneMobile Requery Transaction Service. DONE... "));
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            
                        #endregion

                        #region Requery Other bank Transactions
                        Trace.TraceInformation(String.Format("**************************************************************"));
                            Trace.TraceInformation(String.Format("BankOneMobile Other Banks Requery Transaction Service. "));
                            pendingTransfers = new List<IDBNFundsTransferTransactionType>();
                            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                            {
                                pendingTransfers = new DBNFundsTransferTransactionTypeSystem().GetPendingTransfersToOtherBanks(_theDataSource);
                            }
                            Trace.TraceInformation(String.Format("Gotten Pending Transfers. {0} found... ", pendingTransfers.Count));
                            if (pendingTransfers != null && pendingTransfers.Count > 0)
                            {
                                foreach (var pendingTransfer in pendingTransfers)
                                {

                                    Agent agent = null;
                                    using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                    {
                                        agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(pendingTransfer.AgentPhoneNumber) as Agent;
                                    }
                                    if (agent == null)
                                    {
                                        throw new Exception(string.Format("Could not retreive agent with phonenumber: {0}", pendingTransfer.AgentPhoneNumber));
                                    }

                                    string errorMessage = string.Empty;
                                    string requeryResponse = string.Empty;
                                    try
                                    {
                                        requeryResponse = new TransactionSystem().RequeryOtherBanksTransferTransaction("999063", pendingTransfer.SessionID, out errorMessage);
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(String.Format("DBN/OtherBanks Transfer Requery Error: {0}", ex.Message));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile FT Service-DBN/OtherBanks Transfer Requery Error:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                    if (String.IsNullOrEmpty(requeryResponse))
                                    {
                                        //Response should be null or empty hence we skip this and try again
                                        continue;
                                    }
                                    else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() != "-96" && requeryResponse.ToLower() != "00")
                                    {
                                        #region Reversal
                                        //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                        string narration = String.Format("Reversal of Interbank Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                        Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", pendingTransfer.Amount, pendingTransfer.OriginatorAccountNumber));
                                        var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                        try
                                        {
                                            using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                            {
                                                reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mfbCode, pendingTransfer.OriginatorAccountNumber, pendingTransfer.Amount, pendingTransfer.Fee, pendingTransfer.VATInPercentage, "R" + pendingTransfer.PaymentReference, narration, true, true);
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            #region send error mail
                                            try
                                            {
                                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                                {
                                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                    string body = string.Format("{0}\n", errorMessage);
                                                    client.SendEmail("100040", "donotreply@mybankone.com",
                                                        emails, string.Format("BankOneMobile FT Service-DBN/OtherBanks Transfer Requery web service Error from DBN:{0}", errorMessage), body);
                                                }
                                            }
                                            catch (Exception emailException)
                                            {
                                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                            }
                                            #endregion
                                            continue;
                                        }
                                        Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", pendingTransfer.Amount, pendingTransfer.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                        pendingTransfer.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                        pendingTransfer.TransferStatus = FundsTransferStatus.Failed;
                                        pendingTransfer.ResponseCode = requeryResponse.ToLower();
                                        pendingTransfer.ResponseDate = DateTime.Now;

                                        #endregion
                                    }
                                    else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "00")
                                    {
                                        pendingTransfer.TransferStatus = FundsTransferStatus.Successful;
                                        pendingTransfer.ResponseCode = requeryResponse;
                                        pendingTransfer.ResponseDate = DateTime.Now;
                                    }
                                    else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "-96")
                                    {
                                        //Since this is an Error from Diamond Bank, we do not know whether this transaction was successful on NIBBS
                                        //hence, we skip
                                        Trace.TraceError(String.Format("DBN/OtherBanks Transfer Requery Error from DBN: {0}", errorMessage));

                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n", errorMessage);
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile FT Service-DBN/OtherBanks Transfer Requery Error from DBN:{0}", errorMessage), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                        continue;
                                    }
                                }
                            }

                            Trace.TraceInformation(String.Format("BankOneMobile Other Banks Requery Transaction Service. "));
                            Trace.TraceInformation(String.Format("**************************************************************"));
                        #endregion

                            #region Post Failed Reversals
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            Trace.TraceInformation(String.Format("BankOneMobile Post Failed Reversals. "));
                            List<IDBNFundsTransferTransactionType> failedReversals = new List<IDBNFundsTransferTransactionType>();
                            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                            {
                                failedReversals = new DBNFundsTransferTransactionTypeSystem().GetFailedReversals(_theDataSource, mfbCode);
                            }
                            Trace.TraceInformation(String.Format("Gotten Failed Reversals. {0} found... ", failedReversals.Count));
                            if (failedReversals != null && failedReversals.Count > 0)
                            {
                                foreach (var failedReversal in failedReversals)
                                {

                                    //we ensure this Reversal has not been previously posted on corebanking
                                    Trace.TraceError(String.Format("Now requerying transaction: {0}", "R" + failedReversal.PaymentReference));
                                    Services.SwitchServiceRef.Response rp = new Services.SwitchServiceRef.Response();
                                    try
                                    {

                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            rp = client.RequeryTransactionStatus(mfbCode, "R" + failedReversal.PaymentReference);
                                        }
                                        Trace.TraceError(String.Format("Done requerying transaction: {0}. status: {1}", "R" + failedReversal.PaymentReference, rp.IsSuccessful));
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(String.Format("Web Service Error in Requery for Failed Reversal: {0}", ex.Message));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-FT Service Failed Reversal SwitchingService Trx Requery Error:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }

                                    if (rp.IsSuccessful)
                                    {
                                        failedReversal.ReversalStatus = DBNReversalStatus.Successful;
                                        failedReversal.TransferStatus = FundsTransferStatus.Failed;
                                        failedReversal.ResponseCode = "06";
                                        failedReversal.ResponseDate = DateTime.Now;

                                        try
                                        {
                                            Trace.TraceInformation(String.Format("About to update {0} status on bankone Mobile.", failedReversal.PaymentReference));
                                            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                            {
                                                new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, (DBNFundsTransferTransactionType)failedReversal);
                                            }
                                            Trace.TraceInformation(String.Format("{0} Update done.", failedReversal.PaymentReference));
                                            continue;
                                        }
                                        catch (Exception ex)
                                        {
                                            Trace.TraceError(string.Format("Failed Reversal with Reference No. {0} was successful but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", "R" + failedReversal.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                            #region send error mail
                                            try
                                            {
                                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                                {
                                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                    string body = string.Format("Failed Reversal with Reference No. {0} was successful but an error occured when trying to update its status on bankonemobile. Error: {1}\n{2}\n{3}", "R" + failedReversal.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                    client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                        emails, string.Format("DBN Funds Transfer-Failed Reversal Error Updating FAILED Transfer On BankOneMobile:{0}", ex.Message), body);
                                                }
                                            }
                                            catch (Exception emailException)
                                            {
                                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                            }
                                            #endregion
                                            continue;
                                        }
                                    }

                                    Agent agent = null;
                                    using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                    {
                                        agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(failedReversal.AgentPhoneNumber) as Agent;
                                    }
                                    if (agent == null)
                                    {
                                        throw new Exception(string.Format("Could not retreive agent with phonenumber: {0}", failedReversal.AgentPhoneNumber));
                                    }

                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of funds Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to repost failed reversal. Amount = {0}. Account is {1}. Reference is {2} ", failedReversal.Amount, failedReversal.OriginatorAccountNumber,failedReversal.PaymentReference));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mfbCode, failedReversal.OriginatorAccountNumber, failedReversal.Amount, failedReversal.Fee, failedReversal.VATInPercentage, "R" + failedReversal.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(string.Format("Web service Error reposting failed reversal transaction with Reference No. {0}. Error: {1}\n{2}\n{3}", failedReversal.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("Web service Error reposting failed reversal transaction with Reference No. {0}. Error: {1}\n{2}\n{3}", failedReversal.PaymentReference, ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("DBN Funds Transfer-Web Service Error Reposting Failed Reversal on Corebanking:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        continue;
                                    }

                                    Trace.TraceInformation(string.Format("Done reposting failed reversal. Amount = {0}. Account is {1}. Status: {2}. Ref: {3} ", failedReversal.Amount, failedReversal.OriginatorAccountNumber, reversalResponse.IsSuccessful, failedReversal.PaymentReference));
                                    
                                    failedReversal.TransferStatus = FundsTransferStatus.Failed;
                                    failedReversal.ResponseCode = "06";
                                    failedReversal.ResponseDate = DateTime.Now;

                                    if (reversalResponse.IsSuccessful)
                                    {
                                        failedReversal.ReversalStatus = DBNReversalStatus.Successful;
                                    }
                                    else
                                    {
                                        failedReversal.ReversalStatus = DBNReversalStatus.NotSuccessful;
                                    }

                                    try
                                    {
                                        Trace.TraceInformation(String.Format("About to update {0} status on bankone Mobile.", failedReversal.PaymentReference));
                                        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                        {
                                            new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, (DBNFundsTransferTransactionType)failedReversal);
                                        }
                                        Trace.TraceInformation(String.Format("{0} Update done.", failedReversal.PaymentReference));
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(string.Format("Posting failed Reversal Transaction with Reference No. {0} was {1} but an error occured when trying to update its status on bankonemobile. Error: {2}\n{3}\n{4}", failedReversal.PaymentReference, reversalResponse.IsSuccessful ? "SUCCESSFUL" : "NOT SUCCESSFUL", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("Posting failed Reversal Transaction with Reference No. {0} was {1} but an error occured when trying to update its status on bankonemobile. Error: {2}\n{3}\n{4}", failedReversal.PaymentReference, reversalResponse.IsSuccessful ? "SUCCESSFUL" : "NOT SUCCESSFUL", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("Failed Reversal Posting - Error Updating FAILED Reversal Reposting On BankOneMobile:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                }
                            }
                            Trace.TraceInformation(String.Format("BankOneMobile Post Failed Reversals. DONE....."));
                            Trace.TraceInformation(String.Format("**************************************************************"));

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(String.Format(ex.Message));
                        #region send error mail
                        try
                        {
                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                            {
                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                    emails, string.Format("BankOneMobile-DBN Funds Transfer And Reversal Posting:{0}", ex.Message), body);
                            }
                        }
                        catch (Exception emailException)
                        {
                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                        }
                        #endregion
                    }
                    this.timer.Interval = 1000 * Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"]);
                }
                finally
                {
                    this.timer.Start();
                }
            }
        }
    }
}
