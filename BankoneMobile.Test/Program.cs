﻿using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using GAF;
using GAF.Extensions;
using GAF.Operators;
using System.Web;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;


namespace BankoneMobile.Test
{
    public static class Extension
    {
        public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
    internal class Program
    {
        
        private static List<City> _cities;
        private static double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = System.Math.Sin(deg2rad(lat1)) * System.Math.Sin(deg2rad(lat2)) + System.Math.Cos(deg2rad(lat1)) * System.Math.Cos(deg2rad(lat2)) * System.Math.Cos(deg2rad(theta));
            dist = System.Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double deg2rad(double deg)
        {
            return (deg * System.Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double rad2deg(double rad)
        {
            return (rad / System.Math.PI * 180.0);
        }

        public static void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback +=

                delegate(
                    Object sender1,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
        }
//        static void Main(string[] args)
//        {

//            //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
////:::                                                                         :::
////:::  This routine calculates the distance between two points (given the     :::
////:::  latitude/longitude of those points). It is being used to calculate     :::
////:::  the distance between two locations using GeoDataSource(TM) products    :::
////:::                                                                         :::
////:::  Definitions:                                                           :::
////:::    South latitudes are negative, east longitudes are positive           :::
////:::                                                                         :::
////:::  Passed to function:                                                    :::
////:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
////:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
////:::    unit = the unit you desire for results                               :::
////:::           where: 'M' is statute miles (default)                         :::
////:::                  'K' is kilometers                                      :::
////:::                  'N' is nautical miles                                  :::
////:::                                                                         :::
////:::  Worldwide cities and other features databases with latitude longitude  :::
////:::  are available at http://www.geodatasource.com                          :::
////:::                                                                         :::
////:::  For enquiries, please contact sales@geodatasource.com                  :::
////:::                                                                         :::
////:::  Official Web site: http://www.geodatasource.com                        :::
////:::                                                                         :::
////:::           GeoDataSource.com (C) All Rights Reserved 2015                :::
////:::                                                                         :::
////:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::



//            //Console.WriteLine(distance(6.501325000000001, 3.3797416666666664, 6.500073333333334, 3.3796166666666663, 'M'));
//            Console.WriteLine(distance(6.501325000000001, 3.3797416666666664, 6.4378816, 3.4277746, 'K'));
//            //Console.WriteLine(distance(6.501325000000001, 3.3797416666666664, 6.500073333333334, 3.3796166666666663, 'N'));





//            //BypassCertificateError();
//            //using (DiamondBankOneConnectRef.diamondbankoneConnectSoapClient diamondBankClient = new DiamondBankOneConnectRef.diamondbankoneConnectSoapClient())
//            //{
//            //    string requeryResponse = diamondBankClient.requeryTransaction("2678383337");
//            //}

            

//            Random rnd = new Random();
//            string randomNumber = rnd.Next(0, 999999).ToString("000000") + rnd.Next(0, 999999).ToString("000000");
//            string dt = DateTime.Now.ToString("yyMMddHHmmss");
//            string sessionId = "999063" + dt + randomNumber;

//            rnd = new Random();
//            randomNumber = rnd.Next(0, 999999).ToString("000000") + rnd.Next(0, 999999).ToString("000000");
//            dt = DateTime.Now.ToString("yyMMddHHmmss");
//            string nameEnqSessionId = "999063" + dt + randomNumber;

//            string sessionID1 = "999063151130150357307762450737";
//            string sessionID2 = "999063151130145147295189135945";

//            object[] response = null;
//            string requeryXMLRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
//                                                            <TSQuerySingleRequest>
//	                                                            <SourceInstitutionCode>{0}</SourceInstitutionCode>
//	                                                            <ChannelCode>2</ChannelCode>
//                                                                <SessionID>{1}</SessionID>
//                                                            </TSQuerySingleRequest>
//                                                 ", "999063", "999063151130150357307762450738");

//            string nameEnqRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
//                                                    <NESingleRequest>
//	                                                    <SessionID>{0}</SessionID>
//	                                                    <DestinationInstitutionCode>{1}</DestinationInstitutionCode>
//	                                                    <ChannelCode>2</ChannelCode>
//	                                                    <AccountNumber>{2}</AccountNumber>
//                                                    </NESingleRequest>
//                                                 ", nameEnqSessionId, "999033", "1005847601");

//            string transferXMLRequest= string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
//                                                            <FTSingleCreditRequest>
//	                                                            <SessionID>{0}</SessionID>
//	                                                            <NameEnquiryRef>{1}</NameEnquiryRef>
//	                                                            <DestinationInstitutionCode>{2}</DestinationInstitutionCode>
//	                                                            <ChannelCode>2</ChannelCode>
//	                                                            <BeneficiaryAccountName>{3}</BeneficiaryAccountName>
//	                                                            <BeneficiaryAccountNumber>{4}</BeneficiaryAccountNumber>
//                                                                <BeneficiaryBankVerificationNumber>{5}</BeneficiaryBankVerificationNumber>
//	                                                            <BeneficiaryKYCLevel>{6}</BeneficiaryKYCLevel>
//	                                                            <OriginatorAccountName>{7}</OriginatorAccountName>
//	                                                            <OriginatorAccountNumber>{8}</OriginatorAccountNumber>
//	                                                            <OriginatorBankVerificationNumber>{9}</OriginatorBankVerificationNumber>
//	                                                            <OriginatorKYCLevel>{10}</OriginatorKYCLevel>
//	                                                            <TransactionLocation></TransactionLocation>
//                                                                <Narration>{11}</Narration>
//                                                                <PaymentReference>{12}</PaymentReference>
//	                                                            <Amount>{13}</Amount>
//                                                            </FTSingleCreditRequest>
//
//                                                 ", sessionId, nameEnqSessionId, "999033", "KUSORO SUNDAY OLADAYO",
//                                                  "1005847601", "22222668163", "",
//                                                  "Test", "1100019582", "",
//                                                  "", "Test Narration", DateTime.Now.ToString("yyyyMMddhhmmss"), "200");


//            string dre = string.Format(@"
//                                        - <NESingleResponse>
//                                          <SessionID>999063151130143724548905092900</SessionID>
//                                          <DestinationInstitutionCode>999033</DestinationInstitutionCode>
//                                          <ChannelCode>2</ChannelCode>
//                                          <AccountNumber>1005847601</AccountNumber>
//                                          <AccountName>KUSORO SUNDAY OLADAYO</AccountName>
//                                          <BankVerificationNumber>22222668163</BankVerificationNumber>
//                                          <KYCLevel />
//                                          <ResponseCode>00</ResponseCode>
//                                          </NESingleResponse>
//                                        ");
//            try
//            {
//                using (var client = new BankOneMobile.Services.NibssServiceReference.localnibbsServiceSoapClient())
//                {
//                    //response = client.executeStatusEnquiryThruNibbs(requeryXMLRequest);
//                    response = client.executeNameEnquiryThruNibbs(nameEnqRequest);
//                    response = client.executeCreditTransferThruNibbs(transferXMLRequest);
//                }
//            }
//            catch (TimeoutException tex)
//            {

//            }
//            catch (Exception ex)
//            {
//            }

//            BankOneMobile.Services.RegionServiceRef.Region[] regions = null;
//            using (var client = new BankOneMobile.Services.RegionServiceRef.RegionServiceClient())
//            {
//                regions=client.RetrieveAll("5");
//            }


//            BankOneMobile.Services.BranchServiceRef.Branch[] branches = null;
//            using (var client = new BankOneMobile.Services.BranchServiceRef.BranchServiceClient())
//            {
//                branches = client.RetrieveAll("5");
//            }
//            BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
//            // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
//            //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
//            ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
//            ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
//            ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
//            ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());


//            ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
//            ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
//            ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
//            ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
//            Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
//            System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
//            BankOneMobile.Extension.ApplicationInitializer.Init();
//            System.Diagnostics.Trace.TraceInformation("Init Succesfull");   


//            string pin = "1111";
//            pin = HSMCenter.GeneratePinBlock("07066321504", pin, AppZone.HsmInterface.PinBlockFormats.ANSI);

//            byte[] encryptedPinBytes = new byte[8];
//            if (!string.IsNullOrEmpty(pin))
//            {
//                ThalesSim.Core.Utility.HexStringToByteArray(pin, ref encryptedPinBytes);
//            }

//            string message = string.Empty;
//            using (var client = new MobileServiceReference.BankOneMobileServiceClient())
//            {
//                bool status = client.SaveAgentGeoLocation("08095185440", "34.12.43.56.21", "34.988.90.6.54", out message);
//            }
//            Console.WriteLine("success");
//            Console.ReadLine();

//            Console.WriteLine(new PANE.Framework.Utility.MD5Password().CreateSecurePassword("1234567890"));
//            Console.ReadLine();
//            ResponseCodes theResponse = (ResponseCodes)Enum.Parse(typeof(ResponseCodes), "161");
//            Console.WriteLine(PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(theResponse.ToString()));
//            Console.ReadLine();






            


//            string te = new PANE.Framework.Utility.MD5Password().CreateSecurePassword("1234567890");
//            //string custID = "6468";
//            //string acctNo = "00010119090000773";
//            //string mfbCode = "000000";
//            //long branchID = 1;
//            //1120722100100 succ //1120723100046 succ //1120612100124 error
//            //14405954 //100022 //14293843 100136

//            try
//            {
//               // TestTransfer.TestTrxServiceClient cl = new TestTransfer.TestTrxServiceClient();
              


//            }
//            catch (Exception ex)
//            {
//            }
//        }


        public static List<int> GetShortestPathAndDistance(List<City> Cities, out double ShortestDistance)
        {
            var nearestNeighTour = _cities.Skip(1).Aggregate(
            new List<int>() { 0 },
            (list, curr) =>
            {
                var lastCity = _cities[list.Last()];
                var minDist = Enumerable.Range(0, _cities.Count).Where(i => !list.Contains(i)).Min(cityIdx => lastCity.GetDistanceFromPosition(_cities[cityIdx].Latitude, _cities[cityIdx].Longitude));
                var minDistCityIdx = Enumerable.Range(0, _cities.Count).Where(i => !list.Contains(i)).First(cityIdx => minDist == lastCity.GetDistanceFromPosition(_cities[cityIdx].Latitude, _cities[cityIdx].Longitude));
                list.Add(minDistCityIdx);
                return list;
            });

            double shortestDistance = 0;
            City previousCity = null;

            foreach (var pt in nearestNeighTour)
            {
                var currentCity = _cities[pt];

                if (previousCity != null)
                {
                    var distance = previousCity.GetDistanceFromPosition(currentCity.Latitude,
                                                                        currentCity.Longitude);
                    shortestDistance += distance;
                }
                previousCity = currentCity;
            }
            ShortestDistance = shortestDistance;

            return nearestNeighTour;
        }

        
        //private static void Main(string[] args)
        //{
        //    //get our cities
        //    _cities = CreateCities().ToList();
        //    double shortestDistance = 0.0;

        //    var shortestPath = GetShortestPathAndDistance(_cities, out shortestDistance);


        //    var subList = _cities.Skip(2).Take(5).ToList();

        //    //Each city can be identified by an integer within the range 0-15
        //    //our chromosome is a special case as it needs to contain each city 
        //    //only once. Therefore, our chromosome will contain all the integers
        //    //between 0 and 15 with no duplicates

        //    //we can create an empty population as we will be creating the 
        //    //initial solutions manually.
        //    var population = new Population();

        //    //create the chromosomes
        //    for (var p = 0; p < 100; p++)
        //    {

        //        var chromosome = new Chromosome();
        //        foreach (var city in _cities)
        //        {
        //            chromosome.Genes.Add(new Gene(city));
        //        }
        //        chromosome.Genes.ShuffleFast();
        //        population.Solutions.Add(chromosome);
        //    }

        //    //create the elite operator
        //    var elite = new Elite(5);

        //    //create the crossover operator
        //    var crossover = new Crossover(0.8)
        //        {
        //            CrossoverType = CrossoverType.DoublePointOrdered
        //        };

        //    //create the mutation operator
        //    var mutate = new SwapMutate(0.002);

        //    //create the GA
        //    var ga = new GeneticAlgorithm(population, CalculateFitness);

        //    //hook up to some useful events
        //    ga.OnGenerationComplete += ga_OnGenerationComplete;
        //    ga.OnRunComplete += ga_OnRunComplete;

        //    //add the operators
        //    ga.Operators.Add(elite);
        //    ga.Operators.Add(crossover);
        //    ga.Operators.Add(mutate);

        //    //run the GA
        //    ga.Run(Terminate);

        //    Console.Read();
        //}
        
        private static void Main(string[] args)
        {
            List<accountStatement> statements =new List<accountStatement>();
            var homeAddress = new address
            {
                city = "Yaba",
                country = "",
                line2 = "",
                postalCode = "",
                line1 = "Ajao Estate",
                state = "Lagos"
            };
            var businessAddress = new address
            {
                city="Yaba",
                country="",
                line2="",
                postalCode="",
                line1 = "Victoria Island",
                state = "Lagos"
            };
            var nextOfKin = new nextOfKin
            {
                name = "Adebo Tunde",
                phoneNumber = "07066321504"
            };
            var statement = new accountStatement
            {
                amount = 100,
                narration = "Test narration",
                transactionDate = DateTime.Now.Truncate(TimeSpan.FromMilliseconds(1)).Truncate(TimeSpan.FromSeconds(1)),
            };
            statements.Add(statement);

            //PASSPORT,NATIONAL_ID,DRIVERS_LICENSE,VOTERS_CARD,EMPLOYMENT_ID,OTHER
            var identification = new identification
            {
                idNumber = "12345",
                idType = "PASSPORT"
            };
            var client = new client
            {
                accountNumber = "1100019612",
                birthDate = DateTime.Now.Truncate(TimeSpan.FromMilliseconds(1)).Truncate(TimeSpan.FromSeconds(1)),
                businessAddress = businessAddress,
                homeAddress = homeAddress,
                businessDescription = "Test business desc",
                bvn = "22109909878",
                creditScore = 25,
                email = "iyk@gmail.com",
                firstName = "Ikenna",
                //FEMALE,MALE
                gender = "MALE",
                identification = identification,
                lastName = "Agugua",
                //SINGLE,MARRIED,WIDOWED,DIVORCED
                maritalStatus = "",
                mobileNumber1 = "07066321504",
                nextOfKin = nextOfKin
            };
            var onecreditLoanOrigination = new OneCreditLoanOrigination
            {
                client = client,
                amount = 5000,
               // statements = statements
            };

            string output = JsonConvert.SerializeObject(onecreditLoanOrigination);

            OneCreditLoanOrigination one = new OneCreditLoanOrigination();
            one = JsonConvert.DeserializeObject<OneCreditLoanOrigination>(output);

            one.statements = new List<accountStatement>();
            one.statements.Add(statement);


            HttpClient client1 = new HttpClient();
            client1.BaseAddress = new Uri("http://54.149.41.47/");

            // Add an Accept header for JSON format.
            client1.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            //var content = new
            HttpResponseMessage response = client1.PostAsJsonAsync("betakwik-api/loan",one).Result;
            //response.EnsureSuccessStatusCode();
            var contents = response.Content.ReadAsStringAsync();
            var result = contents.Result;


            dynamic obj = JsonConvert.DeserializeObject(result);
            var messageFromServer = obj.message;
            var messageFromServer1 = obj.errors;
            if (response.IsSuccessStatusCode)
            {
               
            }
            else
            {
                string res = "Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase;
            }


            // Create a request using a URL that can receive a post. 
            WebRequest request_ = WebRequest.Create("http://54.149.41.47/betakwik-api/loan");
            // Set the Method property of the request to POST.
            request_.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = output;
            byte[] byteArray_ = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request_.ContentType = "application/json; charset=utf-8";


            //using (var streamWriter = new StreamWriter(request_.GetRequestStream()))
            //{

            //    streamWriter.Write(output);
            //    streamWriter.Flush();
            //    streamWriter.Close();

            //    var httpResponse = (HttpWebResponse)request_.GetResponse();
            //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //    {
            //        var result = streamReader.ReadToEnd();
            //    }
            //}

            // Set the ContentLength property of the WebRequest.
            request_.ContentLength = byteArray_.Length;
            // Get the request stream.
            Stream dataStream_ = request_.GetRequestStream();
            // Write the data to the request stream.
            dataStream_.Write(byteArray_, 0, byteArray_.Length);
            // Close the Stream object.
            dataStream_.Close();
            // Get the response.
            WebResponse webResponse = request_.GetResponse();
            // Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream_ = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader_ = new StreamReader(dataStream_);
            // Read the content.
            string responseFromServer = reader_.ReadToEnd();

            // Display the content.
            //Console.WriteLine(responseFromServer);
            // Clean up the streams.
            reader_.Close();
            dataStream_.Close();
            webResponse.Close();

            Response response1 = JsonConvert.DeserializeObject<Response>(responseFromServer);
        }

        static void ga_OnRunComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];
            foreach (var gene in fittest.Genes)
            {
                Console.WriteLine(((City)gene.ObjectValue).Name);
            }
        }

        private static void ga_OnGenerationComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];
            var distanceToTravel = CalculateDistance(fittest);
            Console.WriteLine("Generation: {0}, Fitness: {1}, Distance: {2}", e.Generation, fittest.Fitness, distanceToTravel);     
        }

        private static List<City> CreateCities()
        {
            var cities = new List<City>
            {
                
                new City("Bristol", 6.501325000000001, 3.3797416666666664),
                new City("Birmingham", 6.500853333333334, 3.3792616666666664),
                
                new City("Leeds",6.501004999999999, 3.381791666666667),
                new City("London", 6.500073333333334, 3.3796166666666663),
                //new City("Manchester", 53.478239, -2.258549),
                //new City("Liverpool", 53.409532, -3.000126),
                //new City("Hull", 53.751959, -0.335941),
                //new City("Newcastle", 54.980766, -1.615849),
                //new City("Carlisle", 54.892406, -2.923222),
                //new City("Edinburgh", 55.958426, -3.186893),
                //new City("Glasgow", 55.862982, -4.263554),
                //new City("Swansea", 51.624837, -3.94495),
                //new City("Cardiff", 51.488224, -3.186893),                
                //new City("Falmouth", 50.152266, -5.065556),
                //new City("Exeter", 50.726024, -3.543949),
                new City("Canterbury", 6.438108333333334, 3.427831666666666)
            };
            return cities;
        }

        private static List<City> CreateClusterCities()
        {
            var cities = new List<City>
            {
                new City("Birmingham", 6.500853333333334, 3.3792616666666664),
                new City("Bristol", 6.501325000000001, 3.3797416666666664),
                new City("London", 6.500073333333334, 3.3796166666666663),
                new City("Leeds",6.501004999999999, 3.381791666666667),
                new City("Manchester", 53.478239, -2.258549),
                new City("Liverpool", 53.409532, -3.000126),
                new City("Hull", 53.751959, -0.335941),
                new City("Newcastle", 54.980766, -1.615849),
                new City("Carlisle", 54.892406, -2.923222),
                new City("Edinburgh", 55.958426, -3.186893),
                new City("Glasgow", 55.862982, -4.263554),
                new City("Cardiff", 51.488224, -3.186893),
                new City("Swansea", 51.624837, -3.94495),
                new City("Exeter", 50.726024, -3.543949),
                new City("Falmouth", 50.152266, -5.065556),
                new City("Canterbury", 6.438108333333334, 3.427831666666666)
            };
            return cities;
        }

        public static double CalculateFitness(Chromosome chromosome)
        {
            var distanceToTravel = CalculateDistance(chromosome);
            return 1 - distanceToTravel / 10000;
        }
    
        private static double CalculateDistance(Chromosome chromosome)
        {
            var distanceToTravel = 0.0;
            City previousCity = null;

            //run through each city in the order specified in the chromosome
            foreach (var gene in chromosome.Genes)
            {
                var currentCity = (City)gene.ObjectValue;

                if (previousCity != null)
                {
                    var distance = previousCity.GetDistanceFromPosition(currentCity.Latitude,
                                                                        currentCity.Longitude);
                    distanceToTravel += distance;
                }
                previousCity = currentCity;
            }
            return distanceToTravel;
        }

        public static bool Terminate(Population population, int currentGeneration, long currentEvaluation)
        {
            return currentGeneration > 600;
        }
    }

    [Serializable]
    public class City
    {
        public City(string name, double latitude, double longitude)
        {
            Name = name;
            Latitude = latitude;
            Longitude = longitude;
        }

        public string Name { set; get; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public double GetDistanceFromPosition(double latitude, double longitude)
        {
            var R = 6371; // radius of the earth in km
            var dLat = DegreesToRadians(latitude - Latitude);
            var dLon = DegreesToRadians(longitude - Longitude);
            var a =
                System.Math.Sin(dLat / 2) * System.Math.Sin(dLat / 2) +
                System.Math.Cos(DegreesToRadians(Latitude)) * System.Math.Cos(DegreesToRadians(latitude)) *
                System.Math.Sin(dLon / 2) * System.Math.Sin(dLon / 2)
                ;
            var c = 2 * System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
            var d = R * c; // distance in km
            return d;
        }

        private static double DegreesToRadians(double deg)
        {
            return deg * (System.Math.PI / 180);
        }

        public byte[] ToBinaryString()
        {
            var result = new byte[6];
            return result;
        }

        public override bool Equals(object obj)
        {
            var item = obj as City;
            return Equals(item);
        }

        protected bool Equals(City other)
        {
            return string.Equals(Name, other.Name) &&
            Latitude.Equals(other.Latitude) &&
            Longitude.Equals(other.Longitude);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Latitude.GetHashCode();
                hashCode = (hashCode * 397) ^ Longitude.GetHashCode();
                return hashCode;
            }
        } 
    }

    public class Response
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }

    public static class tes
    {
        public static IEnumerable<T> SkipOnce<T>(this IEnumerable<T> source, T itemToSkip)
        {
            bool skipped = false;
            var comparer = EqualityComparer<T>.Default;
            foreach (var item in source)
            {
                if (!skipped && comparer.Equals(item, itemToSkip))
                    skipped = true;
                else
                    yield return item;
            }
        }
        public static IEnumerable<IEnumerable<T>> Permutations<T>(this IEnumerable<T> source)
        {
            var query =
                from item in source
                from others in source.SkipOnce(item).Permutations()
                select new[] { item }.Concat(others);
            return query.DefaultIfEmpty(Enumerable.Empty<T>());
        }
    }
}
