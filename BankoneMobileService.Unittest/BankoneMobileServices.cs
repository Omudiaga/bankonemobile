﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.NHibernate;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.NHibernate.FluentNHibernate.Conventions;
using System.Web;
using FluentNHibernate.Automapping;
using BankOneMobile.Data.NHibernate.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NHibernate;
using NHibernate.Cfg;
using AppZoneSwitch.Extension;


namespace BankoneMobileService.Unittest
{
    [TestClass]
    public class BankoneMobileServices
    {
        public BankoneMobileServices()
        {


            AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
            ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
            ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
            ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
            ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());
            ssl.Register<IThirdPartyMobileProfileRepository>(() => new BankOneMobile.Data.NHibernate.DAO.ThirdPartyMobileProfileRepository());
            ssl.Register<IDbContext>(() => new DbContext());
            Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);

            AppZoneSwitch.Extension.ApplicationInitializer.Init();
            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();


            AutoPersistenceModel autoPersistenceModel = new AutoPersistenceModel();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();

            NHibernateSession.Init(new SimpleSessionStorage(),
   new string[] { System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';')[0] },
                    autoPersistenceModel, DataSourceFactory.GetDataSource(DataCategory.Core)
                 );

            AutoPersistenceModel autoPersistenceModel2 = new AutoPersistenceModel();
            autoPersistenceModel2.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel2.Conventions.Add<HasManyConvention>();
            autoPersistenceModel2.Conventions.Add<ReferencesConvention>();


            NHibernateSession.Init(new SimpleSessionStorage(),
      new string[] { System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';')[0] },
                   autoPersistenceModel2, DataSourceFactory.GetDataSource(DataCategory.Shared)
                );

            AutoPersistenceModel autoPersistenceModel3 = new AutoPersistenceModel();
            autoPersistenceModel3.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel3.Conventions.Add<HasManyConvention>();
            autoPersistenceModel3.Conventions.Add<ReferencesConvention>();

            string auxConfigFile = System.Configuration.ConfigurationManager.AppSettings["AuxilliaryNhibernateFile"];
            NHibernateSession.Init(new SimpleSessionStorage(),
      new string[] { System.Configuration.ConfigurationManager.AppSettings["AuxilliaryMappingAssembly"].Split(';')[0] },
                   autoPersistenceModel3, auxConfigFile, DataSourceFactory.GetDataSource(DataCategory.Auxilliary)
                );

        }

        [TestMethod]
        public void Check_That_CheckBothAccountStatus_Returns_True()
        {
            new BankoneMobileServices();
            IThirdPartyMobileProfileRepository _repository = SafeServiceLocator<IThirdPartyMobileProfileRepository>.GetService();

            Mock<BankOneMobile.Data.Contracts.IDataSource> mock = new Mock<IDataSource>();
            //mock.Setup(x=>x.)

            ThirdPartyMobileProfileSystem thirdParty = new ThirdPartyMobileProfileSystem(mock.Object);

            var obj = thirdParty.CheckBothAccountStatus("7798909898787", "0001");

            Assert.IsTrue(obj);
        }
    }
}
