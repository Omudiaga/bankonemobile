﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Linq;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Services.Utility;
using System.Diagnostics;

namespace BankOneMobile.AgentOptimization.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Initialization
            try
            {
                System.Diagnostics.Trace.TraceInformation("Trace Just Began");

                BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                BankOneMobile.Extension.ApplicationInitializer.Init();

                System.Diagnostics.Trace.TraceInformation("Init Succesfull");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                throw;
            }
            #endregion

            #region Agent Optimization
            try
            {
                string institutionCode = ConfigurationManager.AppSettings["InstitutionCode"];

                List<ICustomerGeoLocation> customerLocations = new List<ICustomerGeoLocation>();
                System.Diagnostics.Trace.TraceError(string.Format("About to get customer locations"));
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    customerLocations = new CustomerGeoLocationSystem().GetByInstitutionCode(source, institutionCode);
                }
                System.Diagnostics.Trace.TraceError(string.Format("Done getting customer locations. Count: {0}", customerLocations == null ? 0 : customerLocations.Count));
                if (customerLocations != null && customerLocations.Count > 0)
                {
                    var customerPhoneNumbers = customerLocations.Select(x => x.CustomerPhoneNumber).ToList();
                    var customerIDs = (from p in new MobileAccountSystem().GetAllMobileAccounts() where customerPhoneNumbers.Contains(p.MobilePhone) select p.CustomerID).Distinct().ToList();
                    BankOneMobile.Services.SwitchServiceRef.Customer[] customers = null;
                    try
                    {
                        System.Diagnostics.Trace.TraceError(string.Format("About to get customers from corebanking. Count: {0}", customerIDs.Count));
                        using (var client = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
                        {
                            customers = client.GetCustomers(institutionCode, customerIDs.ToArray());
                        }
                        System.Diagnostics.Trace.TraceError(string.Format("Done getting customers from corebanking. Count: {0}", customers.Length));

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError(string.Format("Web Service Error while trying to get Customers from CoreBanking:{0}", ex.Message));
                        #region send error mail
                        try
                        {
                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                            {
                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                string body = string.Format("Web Service Error while trying to get Customers from CoreBanking. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                    emails, string.Format("BankOneMobile-Agent Optimization:{0}", ex.Message), body);
                            }
                        }
                        catch (Exception emailException)
                        {
                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                        }
                        #endregion
                        return;
                    }
                    List<CustomerLocationsModel> customersLocationsModel = new List<CustomerLocationsModel>();
                    if (customers != null && customers.Length > 0)
                    {
                        foreach (var location in customerLocations)
                        {
                            if (customers.FirstOrDefault(x => x.PhoneNo == location.CustomerPhoneNumber) == null)
                            {
                                continue;
                            }
                            customersLocationsModel.Add(new CustomerLocationsModel
                            {
                                AgentPhoneNumber = location.AgentPhoneNumber,
                                CustomerPhoneNumber = location.CustomerPhoneNumber,
                                BranchID = customers.FirstOrDefault(x => x.PhoneNo == location.CustomerPhoneNumber).BranchID,
                                Latitude = location.Latitude,
                                Longitude = location.Longitude
                            });
                        }

                        var groupedCustomerLocationsByBranch = customersLocationsModel.GroupBy(x => x.BranchID).ToDictionary(x => x.Key, x => x.ToList());
                        var groupedCustomerLocationsByAgent = customersLocationsModel.GroupBy(x => x.AgentPhoneNumber).ToDictionary(x => x.Key, x => x.ToList());
                        List<City> _cities = new List<City>();
                        List<BankOneMobile.Core.Implementations.AgentOptimization> agentOptimizations = new List<BankOneMobile.Core.Implementations.AgentOptimization>();
                        List<BankOneMobile.Core.Implementations.AgentOptimization> optimizationToAddAfresh = new List<BankOneMobile.Core.Implementations.AgentOptimization>();
                        List<BankOneMobile.Core.Implementations.AgentOptimization> optimizationToUpdate = new List<BankOneMobile.Core.Implementations.AgentOptimization>();
                        List<BankOneMobile.Core.Implementations.AgentOptimization> optimizationToAddToArchive = new List<BankOneMobile.Core.Implementations.AgentOptimization>();
                        Dictionary<string, List<BankOneMobile.Core.Implementations.AgentOptimization>> groupedAgentOptimizations = new Dictionary<string, List<BankOneMobile.Core.Implementations.AgentOptimization>>();

                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            agentOptimizations = new AgentOptimizationSystem().GetAll(source);
                        }
                        if(agentOptimizations != null && agentOptimizations.Count > 0)
                        {
                            groupedAgentOptimizations = agentOptimizations.GroupBy(x => x.AgentPhoneNumber).ToDictionary(x => x.Key, x => x.ToList());
                        }
                        foreach (var agent in groupedCustomerLocationsByAgent)
                        {
                            _cities = new List<City>();
                            foreach (var location in agent.Value)
                            {
                                _cities.Add(new City(location.AgentPhoneNumber, double.Parse(location.Latitude),
                                    double.Parse(location.Longitude)));
                            }

                            double shortestAgentPath = 0.0;
                            var path = GetShortestPathAndDistance(_cities, out shortestAgentPath);
                            long branchID = agent.Value.FirstOrDefault().BranchID;

                            List<CustomerLocationsModel> allCustomersInBranch = null;
                            if (groupedCustomerLocationsByBranch.TryGetValue(branchID, out allCustomersInBranch))
                            {
                                _cities = new List<City>();
                                foreach (var location in allCustomersInBranch)
                                {
                                    _cities.Add(new City(location.AgentPhoneNumber, double.Parse(location.Latitude),
                                        double.Parse(location.Longitude)));
                                }

                                double totalClusterDistance = 0.0;
                                path = GetShortestPathAndDistance(_cities, out totalClusterDistance);

                                allCustomersInBranch = allCustomersInBranch.Zip(path, (o, i) => new { o, i })
                                .OrderBy(x => x.i)
                                .Select(x => x.o).ToList();

                                List<double> calculatedAgentDistances = new List<double>();
                                List<double> calculatedDistances = new List<double>();
                                //int numberOfCustomers = 0;
                                var groupedAgents = allCustomersInBranch.GroupBy(x => x.AgentPhoneNumber).ToDictionary(x => x.Key, x => x.ToList());
                                //foreach (var theAgent in groupedAgents)
                                //{
                                //    calculatedAgentDistances = new List<double>();
                                //    numberOfCustomers = theAgent.Value.Count;
                                //    if (numberOfCustomers == 1) continue;
                                //    for (int i = 0; i < numberOfCustomers; i++)
                                //    {
                                //        var theCustomers = allCustomersInBranch.Skip(i).Take(numberOfCustomers).ToList();
                                //        if (theCustomers.Count == numberOfCustomers)
                                //        {
                                //            _cities = new List<City>();
                                //            foreach (var location in theCustomers)
                                //            {
                                //                _cities.Add(new City(location.AgentPhoneNumber, double.Parse(location.Latitude),
                                //                    double.Parse(location.Longitude)));
                                //            }
                                //            double sap = 0.0;
                                //            path = GetShortestPathAndDistance(_cities, out sap);
                                //            calculatedAgentDistances.Add(sap);
                                //        }
                                //    }
                                //    calculatedDistances.Add(calculatedAgentDistances.Min());
                                //    //var theCustomers = allCustomersInBranch.ToArray().PickRandom(numberOfCustomers);

                                //}
                                double averageDistance = totalClusterDistance / allCustomersInBranch.Count;
                                double negatedPaths = averageDistance * groupedAgents.Count;


                                //double optimizedTotalClusterDistance = calculatedDistances.Sum();
                                double optimizedTotalClusterDistance = totalClusterDistance - negatedPaths;
                                double agentSap = shortestAgentPath;
                                int numberOfCustomersAssignedToAgent = agent.Value.Count;
                                int numberOfCustomersInCluster = allCustomersInBranch.Count;

                                double cdi = ((agentSap -
                                    (optimizedTotalClusterDistance * numberOfCustomersAssignedToAgent / numberOfCustomersInCluster))
                                    / (optimizedTotalClusterDistance * numberOfCustomersAssignedToAgent / numberOfCustomersInCluster))
                                    * 100;

                                BankOneMobile.Core.Implementations.AgentOptimization agentOptimization = new Core.Implementations.AgentOptimization
                                {
                                    AgentPhoneNumber = agent.Key,
                                    BranchID = branchID,
                                    ShortestAgentPath = agentSap,
                                    OptimizedTotalClusterDistance = optimizedTotalClusterDistance,
                                    CdiIndex = Convert.ToInt32(cdi),
                                    InstitutionCode=institutionCode,
                                    DateGenerated=DateTime.Now
                                };

                                try
                                {
                                    List<BankOneMobile.Core.Implementations.AgentOptimization> agOp = new List<BankOneMobile.Core.Implementations.AgentOptimization>();
                                    if (groupedAgentOptimizations.TryGetValue(agent.Key, out agOp))
                                    {
                                        BankOneMobile.Core.Implementations.AgentOptimization ag = (BankOneMobile.Core.Implementations.AgentOptimization)agOp.FirstOrDefault();
                                        optimizationToAddToArchive.Add(ag);

                                        BankOneMobile.Core.Implementations.AgentOptimization ao = new BankOneMobile.Core.Implementations.AgentOptimization();
                                        ao.ID = ag.ID;
                                        ao.AgentPhoneNumber = agent.Key;
                                        ao.BranchID = branchID;
                                        ao.ShortestAgentPath = agentSap;
                                        ao.OptimizedTotalClusterDistance = optimizedTotalClusterDistance;
                                        ao.CdiIndex = Convert.ToInt32(cdi);
                                        ao.InstitutionCode = institutionCode;
                                        ao.DateGenerated = DateTime.Now;

                                        optimizationToUpdate.Add(ao);
                                    }
                                    else
                                    {
                                        optimizationToAddAfresh.Add(agentOptimization);
                                    }                                                                     
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceError(string.Format("Error while trying to save: {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION"));
                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Agent Optimization Error occured when trying to save: {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                            client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                emails, string.Format("Agent Optimization Error occured when trying to save:{0}", ex.Message), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                    continue;
                                }
                            }
                        }

                        if (optimizationToUpdate.Count > 0)
                        {
                            Trace.TraceInformation(String.Format("About to Update optimization data to be updated."));
                            foreach (var item in optimizationToUpdate)
                            {
                                using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                {
                                    new AgentOptimizationSystem().UpdateAgentOptimization(_theDataSource, item);
                                }
                            }
                            Trace.TraceInformation(String.Format("Done Updating."));
                        }

                        if (optimizationToAddToArchive.Count > 0)
                        {
                            Trace.TraceInformation(String.Format("About to archive optimization data."));
                            new BankOneMobile.Data.NHibernate.DAO.CoreDAO<BankOneMobile.Core.Implementations.AgentOptimization>().SqlBulkInsert(optimizationToAddToArchive, "AgentOptimizationArchives", new List<string> { "AgentName", "Branch" });
                            Trace.TraceInformation(String.Format("Done archiving optimization data."));
                        }

                        if (optimizationToAddAfresh.Count > 0)
                        {
                            Trace.TraceInformation(String.Format("About to save optimization data."));
                            new BankOneMobile.Data.NHibernate.DAO.CoreDAO<BankOneMobile.Core.Implementations.AgentOptimization>().SqlBulkInsert(optimizationToAddAfresh, "AgentOptimizations", new List<string> { "AgentName", "Branch" });
                            Trace.TraceInformation(String.Format("Done saving optimization data."));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format(ex.Message));
                #region send error mail
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile Agent Optimization Error:{0}", ex.Message), body);
                    }
                }
                catch (Exception emailException)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                }
                #endregion
            }
            #endregion
        }


        [Serializable]
        public class City
        {
            public City(string name, double latitude, double longitude)
            {
                Name = name;
                Latitude = latitude;
                Longitude = longitude;
            }

            public string Name { set; get; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public double GetDistanceFromPosition(double latitude, double longitude)
            {
                var R = 6371; // radius of the earth in km
                var dLat = DegreesToRadians(latitude - Latitude);
                var dLon = DegreesToRadians(longitude - Longitude);
                var a =
                    System.Math.Sin(dLat / 2) * System.Math.Sin(dLat / 2) +
                    System.Math.Cos(DegreesToRadians(Latitude)) * System.Math.Cos(DegreesToRadians(latitude)) *
                    System.Math.Sin(dLon / 2) * System.Math.Sin(dLon / 2)
                    ;
                var c = 2 * System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
                var d = R * c; // distance in km
                return d;
            }

            private static double DegreesToRadians(double deg)
            {
                return deg * (System.Math.PI / 180);
            }

            public byte[] ToBinaryString()
            {
                var result = new byte[6];
                return result;
            }

            public override bool Equals(object obj)
            {
                var item = obj as City;
                return Equals(item);
            }

            protected bool Equals(City other)
            {
                return string.Equals(Name, other.Name) &&
                Latitude.Equals(other.Latitude) &&
                Longitude.Equals(other.Longitude);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = (Name != null ? Name.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ Latitude.GetHashCode();
                    hashCode = (hashCode * 397) ^ Longitude.GetHashCode();
                    return hashCode;
                }
            }
        }

        public class CustomerLocationsModel
        {
            public string AgentPhoneNumber { get; set; }
            public string CustomerPhoneNumber { get; set; }
            public long BranchID { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
        }

        public static List<int> GetShortestPathAndDistance(List<City> _cities, out double ShortestDistance)
        {
            var nearestNeighTour = _cities.Skip(1).Aggregate(
            new List<int>() { 0 },
            (list, curr) =>
            {
                var lastCity = _cities[list.Last()];
                var minDist = Enumerable.Range(0, _cities.Count).Where(i => !list.Contains(i)).Min(cityIdx => lastCity.GetDistanceFromPosition(_cities[cityIdx].Latitude, _cities[cityIdx].Longitude));
                var minDistCityIdx = Enumerable.Range(0, _cities.Count).Where(i => !list.Contains(i)).First(cityIdx => minDist == lastCity.GetDistanceFromPosition(_cities[cityIdx].Latitude, _cities[cityIdx].Longitude));
                list.Add(minDistCityIdx);
                return list;
            });

            double shortestDistance = 0;
            City previousCity = null;

            foreach (var pt in nearestNeighTour)
            {
                var currentCity = _cities[pt];

                if (previousCity != null)
                {
                    var distance = previousCity.GetDistanceFromPosition(currentCity.Latitude,
                                                                        currentCity.Longitude);
                    shortestDistance += distance;
                }
                previousCity = currentCity;
            }
            ShortestDistance = shortestDistance;

            return nearestNeighTour;
        }

    }
}
