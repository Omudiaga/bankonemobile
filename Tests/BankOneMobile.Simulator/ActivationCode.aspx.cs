﻿using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void btnActivationCode_Click(object sender, EventArgs e)
    {
        if(txtPhoneNumber.Text.Trim() == string.Empty)
        {
            lblResponse.Text = "<font color = \"red\">**No phone number entered**</font>";
        }
        else
        {
            if (!CheckPhoneNumber(txtPhoneNumber.Text.Trim()))
            {
                lblResponse.Text = "<span style=\"color:red;font-weight:bold\">**Invalid Phone number**</span>";
            }
            else
            {
                var mobileAccount = new MobileAccountService.MobileAccount();
                using (var client = new MobileAccountService.MobileAccountServiceClient())
                {
                    mobileAccount = client.GetMobileAccountByPhoneNumber(txtPhoneNumber.Text.Trim());
                }
                if (mobileAccount == null)
                {
                    lblResponse.Text = "<span style=\"color:red;font-weight:bold\">**Phone number does not exist**</span>";
                }
                else
                {
                    lblResponse.Text = string.Format("Activation Code: <span style=\"color:red;font-weight:bold\">{0}</span>", mobileAccount.ActivationCode);
                }
            }
        }
    }
    public static bool CheckPhoneNumber(string entry)
    {
        string validEntries = "080;070;081;071;090;091";
        string[] checkEntries = validEntries.Split(";".ToCharArray());
        bool toReturn = true;
        long digits = 0;

        foreach (string ss in checkEntries)
        {
            if (entry.StartsWith(ss))
            {
                toReturn = true;
                break;
            }

            toReturn = false;

        }
        if (entry.Length != 11)
        {
            toReturn = false;
        }
        if (!Int64.TryParse(entry, out  digits))
        {
            toReturn = false;
        }
        return toReturn;
    }
}
