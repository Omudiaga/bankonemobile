﻿using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            long pendingCount,failedCount = 0;
            using (var client = new MobileAccountService.MobileAccountServiceClient())
            {
                failedCount = client.FailedAccountsCount();
                pendingCount = client.PendingAccountsCount();
            }

            lblResponse.Text = string.Format("Pending Accounts: <span style=\"color:red;font-weight:bold\">{0}</span>", pendingCount);

            lblResponse.Text += string.Format("<br /><br />Failed Accounts: <span style=\"color:red;font-weight:bold\">{0}</span>", failedCount);
            
        }
        catch (Exception ex)
        {
            lblResponse.Text = ex.Message;
        }
    }
}
