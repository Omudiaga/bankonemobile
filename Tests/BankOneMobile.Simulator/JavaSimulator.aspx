﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="JavaSimulator.aspx.cs" Debug="true" Inherits="JavaSimulator" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Label ID="Label1" runat="server" Text="Enter Phone No:"></asp:Label>
&nbsp;<br />
    <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnStart" runat="server" Text="Start Session" 
        onclick="btnStart_Click" />
<br />
<br />
<asp:Literal ID="lblResponse" runat="server"></asp:Literal>
<br />
<asp:TextBox ID="txtResponse" runat="server" Visible="True"></asp:TextBox>
<asp:Button ID="btnSend" runat="server" Text="Send" onclick="btnSend_Click" 
        Visible="True" />
</asp:Content>