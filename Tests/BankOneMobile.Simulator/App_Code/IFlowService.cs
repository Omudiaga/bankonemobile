﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Contracts;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFlowService" in both code and config file together.
[ServiceContract]
[ServiceKnownType(typeof(BankOneMobile.Core.Implementations.Flow))]
public interface IFlowService
{
	[OperationContract]
	long SaveFlow(IFlow theFlow);

	[OperationContract]
	List<IFlow> GetAllFlows();

	[OperationContract]
	IFlow GetFlowById(long id);

	[OperationContract]
	void UpdateFlow(IFlow theFlow);

}
