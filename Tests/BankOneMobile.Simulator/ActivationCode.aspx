﻿<%@ Page Title="BankOneMobile::Activation Code" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ActivationCode.aspx.cs" Inherits="About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Label ID="Label1" runat="server" Text="Enter Phone No:"></asp:Label>
    &nbsp;<br />
    <asp:TextBox ID="txtPhoneNumber" runat="server" MaxLength="11"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnActivationCode" runat="server" Text="Get Activation Code" OnClick="btnActivationCode_Click" />
    <br />
    <br />
    <asp:Literal ID="lblResponse" runat="server"></asp:Literal>
    <br />
</asp:Content>
