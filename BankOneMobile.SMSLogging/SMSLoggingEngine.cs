﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using System.Timers;

namespace BankOneMobile.SMSLogging
{
    public class SMSLoggingEngine
    {
        private Timer timer = null;
        private bool isBusy = false;
        public SMSLoggingEngine()
        {
            System.Threading.Thread thWorker = new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate
                {
                    try
                    {
                        isBusy = true;
                        System.Diagnostics.Trace.TraceInformation("Trace Just Began");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Trace Just Began"));

                        BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                        // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                        //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                        ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                        ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                        ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                        ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                        System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                        ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                        ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                        ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                        ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                        System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                        BankOneMobile.Extension.ApplicationInitializer.Init();

                        System.Diagnostics.Trace.TraceInformation("Init Succesfull");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                        throw;
                    }
                    finally
                    {
                        //System.Diagnostics.Trace.TraceInformation("Now setting busy to false");
                        isBusy = false;
                    }
                }
                ));
            thWorker.Start();
            this.timer = new Timer();
            this.timer.Elapsed += new ElapsedEventHandler(this.timer_Elapsed);

            //System.Diagnostics.Trace.TraceInformation("Just added timer event");
        }



        public void Start()
        {
            //System.Diagnostics.Trace.TraceInformation("About to start timer");
            this.timer.Start();
            //System.Diagnostics.Trace.TraceInformation("Timer started");
        }
        public void Stop()
        {
            if (timer != null)
            {
                this.timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //System.Diagnostics.Trace.TraceInformation(string.Format("Timer just elapsed. Is busy is: {0}", isBusy));
            if (!isBusy)
            {
                this.timer.Stop();
                try
                {
                    try
                    {
                        #region Account creation SMS Logging
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("BankOneMobile SMS Sending Service....BEGIN ")));
                        //string mfbCode = ConfigurationManager.AppSettings["InstitutionCode"];
                        //IInstitution institution = null;
                        List<ICustomerRegistration> pendingSMSes = new List<ICustomerRegistration>();
                        pendingSMSes = new CustomerRegistrationSystem().GetPendingSMSes();
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("Gotten pending SMSes. {0} found... ", pendingSMSes.Count)));
                        if (pendingSMSes != null && pendingSMSes.Count > 0)
                        {
                            foreach (var smsToSend in pendingSMSes)
                            {
                                string message = string.Format("Congratulations. You have now opened a {0} from {1}:\n AccountNumber:{2}\n Mobile PIN: [{3}]", smsToSend.ProductName, smsToSend.InstitutionName, smsToSend.NUBAN, smsToSend.ActivationCode);
                                bool sent = new MobileAccountSystem().SendInstitutionSMS(message, smsToSend.PhoneNumber, smsToSend.InstitutionCode, smsToSend.NUBAN);
                                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SMS Sending returned: {0}", sent)));
                                if (!sent)
                                {
                                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SMS was not sent to customer's phone. Message: {0}, Phone Number: {1}, Request ID: {2}", message, smsToSend.PhoneNumber, smsToSend.ID)));
                                    try
                                    {
                                        //BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient()
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Message:{0}\nAgentID:{1}\nCustomerPhoneNo: {2}", message, smsToSend.AgentID, smsToSend.PhoneNumber);
                                            client.SendEmail(string.IsNullOrEmpty(smsToSend.InstitutionCode) ? "BankOneMobile" : smsToSend.InstitutionCode, "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile:{0}", "SMS Not Delivered To Customer"), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Error while trying to send email. Exception:{0}", emailException.Message)));
                                    }
                                }
                                else
                                {
                                    smsToSend.IsSmsSent = true;
                                    try
                                    {
                                        new CustomerRegistrationSystem().UpdateCustomerRegistration(smsToSend as CustomerRegistration);
                                    }
                                    catch (Exception ex)
                                    {
                                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SMS has been sent to the customer but could not update its status to successful. ID: {0}, Exception: {1}", smsToSend.ID, ex.Message)));
                                    }
                                }
                            }
                        }
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("BankOneMobile SMS Sending Service....END ")));
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("**SMS SENDING EXCEPTION** | {0}", ex.Message)));
                        try
                        {
                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                            {
                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                    emails, string.Format("BankOneMobile-CommitPoint: SMS Sending:{0}", ex.Message), body);
                            }
                        }
                        catch (Exception emailException)
                        {
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("**SMS SENDING EXCEPTION** |Error while trying to send email. Exception:{0}", emailException.Message)));
                        }
                    }
                    this.timer.Interval = 1000 * Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"]);
                }
                finally
                {
                    this.timer.Start();
                }
            }
        }

        private MobileAccount CreateAccountOnCoreBanking(CustomerRegistration pendingAccountToCreate)
        {
            pendingAccountToCreate.CommitPoint = CommitPoint.CoreBanking;
            try
            {
                new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Account about to be created on Corebanking but could not update this request. Exception: {0}", ex.Message));
                throw ex;
            }

            MobileAccount toReturn = new MobileAccount();
            SwitchingServiceRef.Account acc = null;
            try
            {
                SwitchingServiceRef.Gender gender = (SwitchingServiceRef.Gender)Enum.Parse(typeof(SwitchingServiceRef.Gender), pendingAccountToCreate.TheGender.ToString());
                SwitchingServiceRef.AccountInformationSource ifoSource = SwitchingServiceRef.AccountInformationSource.Others; //(SwitchingServiceRef.AccountInformationSource)Enum.Parse(typeof(SwitchingServiceRef.AccountInformationSource), "");
                AccountSource mobileIfoSource = AccountSource.Others; //(AccountSource)Enum.Parse(typeof(AccountSource), "");

                var bankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                var type = bankAccountHelper.GetInstitutionAccountNumberType(pendingAccountToCreate.InstitutionCode);

                using (SwitchingServiceRef.SwitchingServiceClient client = new SwitchingServiceRef.SwitchingServiceClient())
                {

                    acc = client.CreateAccount(pendingAccountToCreate.InstitutionCode, pendingAccountToCreate.LastName, pendingAccountToCreate.FirstName,
                        pendingAccountToCreate.PhoneNumber, gender, pendingAccountToCreate.ProductCode, pendingAccountToCreate.AccountOfficerCode,
                        pendingAccountToCreate.PlaceOfBirth, pendingAccountToCreate.DateOfBirth, pendingAccountToCreate.Address, "", pendingAccountToCreate.NOKName,
                        pendingAccountToCreate.NOKPhone, "", "", false, ifoSource, "", null);
                }
                if (acc == null)
                {
                    toReturn = null;
                    Trace.TraceInformation(String.Format("Account not created on Corebanking... ID: {0}", pendingAccountToCreate.ID));
                    throw new Exception("Account not created on Corebanking");
                }

                pendingAccountToCreate.StandardAccountNumber = acc.Number;
                pendingAccountToCreate.NUBAN = acc.NUBAN;
                pendingAccountToCreate.CustomerID = acc.CustomerID;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Account has been created on corebanking but could not update this request. NUBAN: {0}, Standard: {1} CustomerID: {2}. Exception: {3}", acc.NUBAN, acc.Number, acc.CustomerID, ex.Message));
                }


                toReturn.DateOfBirth = pendingAccountToCreate.DateOfBirth;
                toReturn.PlaceOfBirth = pendingAccountToCreate.PlaceOfBirth;
                toReturn.NOKName = pendingAccountToCreate.NOKName;
                toReturn.NOKPhone = pendingAccountToCreate.NOKPhone;
                toReturn.Address = pendingAccountToCreate.Address;
                toReturn.ReferalPhone = "";
                toReturn.CustomerID = acc.CustomerID;
                toReturn.TheGender = pendingAccountToCreate.TheGender;
                toReturn.HasSufficientInfo = false;
                toReturn.AccountSource = mobileIfoSource;
                toReturn.NUBAN = acc.NUBAN;

                //Dont freak out. just used to hold the accNo for now
                switch (type)
                {
                    case AccountNumberType.NUBAN:
                        toReturn.DisplayMessage = acc.NUBAN; break;
                    case AccountNumberType.Standard:
                    default:
                        toReturn.DisplayMessage = acc.Number; break;
                }
                isCoreBankingDone = true;
            }
            catch (Exception ex)
            {
                isCoreBankingDone = false;
                System.Diagnostics.Trace.TraceError("Core banking web service exception.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //pendingAccountToCreate.Status = RegistrationStatus.Failed;
                pendingAccountToCreate.ErrorInfo = ex.Message;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception innerEx)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("A core banking exception occured and could not update this request. Exception: {0}", innerEx.Message));
                }
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("{0}\nAgentName:{1}\nCustomerPhone: {2}\n{3}\n{4}", ex.Message, pendingAccountToCreate.AgentName, pendingAccountToCreate.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(pendingAccountToCreate.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile-CommitPoint: CoreBanking:{0}", ex.Message), body);
                    }
                }
                catch (Exception emailException)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                }
            }
            return toReturn;
        }
        private string LinkCardAccountNumber(CustomerRegistration pendingAccountToCreate)
        {
            pendingAccountToCreate.CommitPoint = CommitPoint.CardManagement;
            try
            {
                new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Account about to be created on Corebanking but could not update this request. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
            }
            string toReturn = string.Empty;
            try
            {
                //Agent agent = null;
                //using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                //{
                //    agent = new AgentSystem(_theDataSource).GetByCode(pendingAccountToCreate.AgentCode) as Agent;
                //}
                IssuanceServiceRef.BankOneCardIssuanceClient client = new IssuanceServiceRef.BankOneCardIssuanceClient();
                toReturn = client.LinkCardAccount(pendingAccountToCreate.CardSerialNumber, pendingAccountToCreate.NUBAN, pendingAccountToCreate.InstitutionCode, pendingAccountToCreate.AgentCode);

                if (!toReturn.StartsWith("00"))
                {
                    throw new ApplicationException(toReturn.Split("-".ToCharArray())[1]);
                }
                isSuccessful = true;
                client.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Card linking error. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
                pendingAccountToCreate.Status = RegistrationStatus.Failed;
                pendingAccountToCreate.ErrorInfo = ex.Message;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception innerEx)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Card linking error. Error while trying to update request. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, innerEx.Message));
                }
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("{0}\nAgentName:{1}\nCustomerPhone: {2}\n{3}\n{4}", ex.Message, pendingAccountToCreate.AgentName, pendingAccountToCreate.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(pendingAccountToCreate.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile-CommitPoint: Card Management:{0}", ex.Message), body);
                    }
                }
                catch (Exception emailException)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                }
            }
            return toReturn;
        }

    }
}
