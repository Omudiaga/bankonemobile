﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BankOneMobile.AgentOptimization.Service
{
    public partial class AgentOptimizationService : ServiceBase
    {
        AgentOptimizationEngine engine = null;
        public AgentOptimizationService()
        {
            engine = new AgentOptimizationEngine();
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            if (engine != null)
            {
                engine.Start();
            }
        }

        protected override void OnStop()
        {
            if (engine != null)
            {
                engine.Stop();
            }
        }
    }
}
