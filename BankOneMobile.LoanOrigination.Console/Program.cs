﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BankOneMobile.LoanOrigination.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Initialize();
                #region One Credit Loan Origination
                System.Diagnostics.Trace.TraceInformation(String.Format("BankOneMobile OneCredit Loan Origination Service....BEGIN "));
                //string mfbCode = ConfigurationManager.AppSettings["InstitutionCode"];
                //IInstitution institution = null;
                List<IOneCreditLoanRequest> pendingRequests = new List<IOneCreditLoanRequest>();
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                {
                    pendingRequests = new OneCreditLoanRequestSystem(source).GetPendingLoanRequests();
                }
                System.Diagnostics.Trace.TraceInformation(String.Format("Gotten pending requests. {0} found... ", pendingRequests.Count));
                if (pendingRequests != null && pendingRequests.Count > 0)
                {
                    foreach (var requestToSend in pendingRequests)
                    {
                        OneCreditLoanOrigination oneCreditLoanOrigination = new OneCreditLoanOrigination();
                        oneCreditLoanOrigination = JsonConvert.DeserializeObject<OneCreditLoanOrigination>(requestToSend.JsonRequest);
                        oneCreditLoanOrigination.statements = new List<accountStatement>();

                        oneCreditLoanOrigination.client.identification.idNumber = "0";
                        oneCreditLoanOrigination.client.identification.idType = "PASSPORT";

                        //get last 6 months transaction from corebanking
                        System.Diagnostics.Trace.TraceInformation(String.Format("About to get transaction history for request with ID: {0}... ", requestToSend.ID));
                        SwitchingServiceRef.MiniStatementHistory[] trxHistory = null;
                        using (var client = new SwitchingServiceRef.SwitchingServiceClient())
                        {
                            trxHistory = client.GetCustomerTransactionsByCurrentDate(requestToSend.InstitutionCode, oneCreditLoanOrigination.client.accountNumber, DateTime.Now.AddMonths(-6), DateTime.Now);
                        }
                        System.Diagnostics.Trace.TraceInformation(String.Format("Gotten transaction history for request with ID:{0}. {1} found... ", requestToSend.ID, trxHistory == null ? 0 : trxHistory.Length));

                        if (trxHistory != null && trxHistory.Length > 0)
                        {
                            foreach (var trx in trxHistory)
                            {
                                oneCreditLoanOrigination.statements.Add(new accountStatement
                                {
                                    amount = trx.RecordType == SwitchingServiceRef.RecordType.Credit ? +trx.Amount : -trx.Amount,
                                    narration = trx.Narration,
                                    transactionDate = trx.TransactionDate.Truncate(TimeSpan.FromMilliseconds(1)).Truncate(TimeSpan.FromSeconds(1))
                                });
                            }
                        }

                        string jsonToSend = JsonConvert.SerializeObject(oneCreditLoanOrigination);
                        //Now we send the request to OneCredit
                        System.Diagnostics.Trace.TraceInformation(String.Format("About to send request to OneCredit. JSON Request: {0}", jsonToSend));
                        try
                        {
                            HttpClient client1 = new HttpClient();
                            client1.BaseAddress = new Uri("http://54.149.41.47/");

                            // Add an Accept header for JSON format.
                            client1.DefaultRequestHeaders.Accept.Add(
                                new MediaTypeWithQualityHeaderValue("application/json"));
                            //var content = new
                            HttpResponseMessage response = client1.PostAsJsonAsync("betakwik-api/loan", oneCreditLoanOrigination).Result;
                            //response.EnsureSuccessStatusCode();

                            if (response.IsSuccessStatusCode)
                            {
                                System.Diagnostics.Trace.TraceInformation(string.Format("Loan Request was successfully sent"));
                                requestToSend.Status = RegistrationStatus.Successful;
                                requestToSend.Response = response.StatusCode.ToString();
                                requestToSend.StatusMessage = response.ReasonPhrase;
                                requestToSend.DateSent = DateTime.Now;

                                try
                                {
                                    System.Diagnostics.Trace.TraceInformation(String.Format("About to update"));
                                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                                    {
                                        new OneCreditLoanRequestSystem().UpdateOneCreditLoanRequestSystem(source, requestToSend as OneCreditLoanRequest);
                                    }
                                    System.Diagnostics.Trace.TraceInformation(String.Format("Update done"));
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Trace.TraceInformation(string.Format("Loan Request was successfully sent but could not update the request. Exception: {0}", ex.ToString()));
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Message:{0}|RequestID:{1}", ex.ToString(), requestToSend.ID);
                                            client.SendEmail(string.IsNullOrEmpty(requestToSend.InstitutionCode) ? "BankOneMobile" : requestToSend.InstitutionCode, "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-Loan Origination(OneCredit):{0}", "Loan Request Successful BUT Unable to Update Request"), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceInformation(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                }
                            }
                            else
                            {
                                var contents = response.Content.ReadAsStringAsync();
                                var result = contents.Result;
                                dynamic obj = JsonConvert.DeserializeObject(result);
                                var messageFromServer = obj.message;
                                var errorFromServer = obj.errors;
                                string res = "Error Code - " + response.StatusCode + " : Message - " + response.ReasonPhrase;
                                string finalResult = string.Concat(res, " | ", messageFromServer, " | Error: ", errorFromServer == null ? "Null" : errorFromServer);

                                System.Diagnostics.Trace.TraceInformation(string.Format("Loan Request was NOT successfully sent. Reason: {0}", finalResult));

                                requestToSend.Status = RegistrationStatus.Failed;
                                requestToSend.Response = response.StatusCode.ToString();
                                requestToSend.StatusMessage = finalResult;
                                requestToSend.DateSent = DateTime.Now;

                                try
                                {
                                    System.Diagnostics.Trace.TraceInformation(String.Format("About to update"));
                                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                                    {
                                        new OneCreditLoanRequestSystem().UpdateOneCreditLoanRequestSystem(source, requestToSend as OneCreditLoanRequest);
                                    }
                                    System.Diagnostics.Trace.TraceInformation(String.Format("Update done"));
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Trace.TraceInformation(string.Format("Loan Request was NOT successfully sent AND could not update the request. Exception: {0}", ex.ToString()));
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Message:{0}RequestID:{1}", ex.ToString(), requestToSend.ID);
                                            client.SendEmail(string.IsNullOrEmpty(requestToSend.InstitutionCode) ? "BankOneMobile" : requestToSend.InstitutionCode, "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-Loan Origination(OneCredit):{0}", "Loan Request NOT Successful and Unable to Update Request"), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceInformation(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.TraceInformation(string.Format("Loan Origination Service Exception.{0}", ex.ToString()));
                            try
                            {
                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                {
                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                    string body = string.Format("Message:{0}RequestID:{1}", ex.ToString(), requestToSend.ID);
                                    client.SendEmail(string.IsNullOrEmpty(requestToSend.InstitutionCode) ? "BankOneMobile" : requestToSend.InstitutionCode, "donotreply@mybankone.com",
                                        emails, string.Format("BankOneMobile-Loan Origination(OneCredit) Service Exception:{0}", "OneCredit Loan Request Service Exception"), body);
                                }
                            }
                            catch (Exception emailException)
                            {
                                System.Diagnostics.Trace.TraceInformation(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                            }
                        }
                    }
                }
                System.Diagnostics.Trace.TraceInformation(String.Format("BankOneMobile Loan Origination Service....END "));
                #endregion
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation(String.Format("**LOAN ORIGINATION SERVICE EXCEPTION** | {0}", ex.ToString()));
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile Loan Origination: {0}", ex.Message), body);
                    }
                }
                catch (Exception emailException)
                {
                    System.Diagnostics.Trace.TraceInformation(string.Format("**LOAN ORIGINATION SERVICE EXCEPTION** |Error while trying to send email. Exception:{0}", emailException.Message));
                }
            }
        }

        private static void Initialize()
        {
            try
            {
                System.Diagnostics.Trace.TraceInformation("Trace Just Began");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Trace Just Began"));

                BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                BankOneMobile.Extension.ApplicationInitializer.Init();

                System.Diagnostics.Trace.TraceInformation("Init Succesfull");
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                throw;
            }
            finally
            {
            }
        }
    }
    public static class Extension
    {
        public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
}
