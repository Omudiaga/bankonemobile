﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BankOneMobile.CustomerCreationAndCardLinking.WindowsService
{
    public partial class CustomerCreationAndCardLinkingService : ServiceBase
    {
        CustomerCreationAndCardLinkingEngine engine = null;
        public CustomerCreationAndCardLinkingService()
        {
            engine = new CustomerCreationAndCardLinkingEngine();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if(engine != null)
            {
                engine.Start();
            }
        }

        protected override void OnStop()
        {
            if(engine != null)
            {
                engine.Stop();
            }
        }
    }
}
