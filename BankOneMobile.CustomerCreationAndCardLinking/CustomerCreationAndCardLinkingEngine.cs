﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.CustomerCreationAndCardLinking
{
    public class CustomerCreationAndCardLinkingEngine
    {
        private Timer timer = null;
        private bool isBusy = false;
        private bool isSuccessful = false;
        private bool isCoreBankingDone = false;
        private bool isMobileAccountDone = false;
        public CustomerCreationAndCardLinkingEngine()
        {
            System.Threading.Thread thWorker = new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate
                {
                    try
                    {
                        isBusy = true;
                        System.Diagnostics.Trace.TraceInformation("Trace Just Began");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Trace Just Began"));

                        BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                        // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                        //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                        ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                        ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                        ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                        ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                        System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                        ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                        ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                        ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                        ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                        System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                        BankOneMobile.Extension.ApplicationInitializer.Init();

                        System.Diagnostics.Trace.TraceInformation("Init Succesfull"); 
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                        throw;
                    }
                    finally
                    {
                        //System.Diagnostics.Trace.TraceInformation("Now setting busy to false");
                        isBusy = false;
                    }
                }
                ));
            thWorker.Start();
            this.timer = new Timer();
            this.timer.Elapsed+=new ElapsedEventHandler(this.timer_Elapsed);

            //System.Diagnostics.Trace.TraceInformation("Just added timer event");
        }

        
        
        public void Start()
        {
            //System.Diagnostics.Trace.TraceInformation("About to start timer");
            this.timer.Start();
            //System.Diagnostics.Trace.TraceInformation("Timer started");
        }
        public void Stop()
        {
            if(timer != null)
            {
                this.timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //System.Diagnostics.Trace.TraceInformation(string.Format("Timer just elapsed. Is busy is: {0}", isBusy));
            if (!isBusy)
            {
                this.timer.Stop();
                try
                {
                    try
                    {
                        //Customer Creation and Image upload are done between different configured time. e.g. Account Opening: "06h:00m" (6.00am) to 7pm
                        //while image upload: "19h:00m" (7.00pm) to 6am
                        TimeSpan fromTimeSpan, toTimeSpan;
                        if (!TimeSpan.TryParse(ConfigurationManager.AppSettings["AccountOpeningStartTime"].Replace("m", "").Replace("h", ""), out fromTimeSpan))
                        {
                            throw new Exception("Invalid Configured Start Time");
                        }
                        if (!TimeSpan.TryParse(ConfigurationManager.AppSettings["AccountOpeningEndTime"].Replace("m", "").Replace("h", ""), out toTimeSpan))
                        {
                            throw new Exception("Invalid Configured End Time");
                        }

                        if (DateTime.Now.TimeOfDay < fromTimeSpan || DateTime.Now.TimeOfDay > toTimeSpan)
                        {
                            #region Image Upload
                            //Trace.TraceInformation(String.Format("Customer Creation Scheduled Run Time between {0} and {1} has not yet elapsed. Now we begin Image Upload", fromTimeSpan, toTimeSpan));
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            Trace.TraceInformation(String.Format("Image Upload Service. "));
                            List<ICustomerRegistration> pendingCustomerImageUpload = new List<ICustomerRegistration>();
                            pendingCustomerImageUpload = new CustomerRegistrationSystem().GetSuccessfulCustomerRegistrationsWhoseImagesAreNotSynced();
                            Trace.TraceInformation(String.Format("Gotten pending image uploads. {0} found... ", pendingCustomerImageUpload.Count));
                            if (pendingCustomerImageUpload != null && pendingCustomerImageUpload.Count > 0)
                            {
                                foreach (var customerImageToUpload in pendingCustomerImageUpload)
                                {
                                    try
                                    {
                                        //Trace.TraceInformation(String.Format("------------------------------------------------------------------------"));
                                        var imageToUpload = new CustomerRegistrationImageSystem().GetCustomerRegistrationImage(customerImageToUpload.PassportID);
                                        Trace.TraceInformation(String.Format("Gotten image. Status: {0}, PassportID: {1}", imageToUpload == null ? "Null" : "Not Null", customerImageToUpload.PassportID));
                                        if (imageToUpload != null)
                                        {
                                            Trace.TraceInformation(String.Format("Sending image to corebanking"));
                                            SwitchingServiceRef.Response response = new SwitchingServiceRef.Response();
                                            using (var client = new SwitchingServiceRef.SwitchingServiceClient())
                                            {
                                                response = client.SaveCustomerImage(customerImageToUpload.InstitutionCode, customerImageToUpload.CustomerID, imageToUpload.Passport);
                                            }
                                            Trace.TraceInformation(String.Format("Done sending image to corebanking. Is Successful? {0}. Response Message: {1}", response.IsSuccessful, response.ResponseMessage));
                                            if (response.IsSuccessful)
                                            {
                                                try
                                                {
                                                    customerImageToUpload.IsImageSync = true;
                                                    new CustomerRegistrationSystem().UpdateCustomerRegistration(customerImageToUpload as CustomerRegistration);
                                                }
                                                catch (Exception ex)
                                                {
                                                    Trace.TraceInformation(string.Format("Image upload was successful but could not update this request. Exception: {0}", ex.Message));
                                                    throw ex;
                                                }
                                            }
                                            else
                                            {
                                                if(response.ResponseMessage == string.Format("Customer with ID: {0} already has an image", customerImageToUpload.CustomerID))
                                                {
                                                    try
                                                    {
                                                        customerImageToUpload.IsImageSync = true;
                                                        new CustomerRegistrationSystem().UpdateCustomerRegistration(customerImageToUpload as CustomerRegistration);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Trace.TraceInformation(string.Format("Image already exist on bankone but could not update this request. Exception: {0}", ex.Message));
                                                        throw ex;
                                                    }
                                                }
                                            }
                                            //Trace.TraceInformation(String.Format("------------------------------------------------------------------------"));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceInformation(String.Format(ex.Message));
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-Image Upload:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            Trace.TraceInformation(string.Format("Error while trying to send error email. Exception:{0}", emailException.Message));
                                        }
                                    }
                                }
                            }
                            Trace.TraceInformation(String.Format("Image Upload Service DONE... "));
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            #endregion
                        }
                        else
                        {
                            #region Customer creation Serice
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            Trace.TraceInformation(String.Format("BankOneMobile Customer Creation And Card Linking Service. "));
                            //string mfbCode = ConfigurationManager.AppSettings["InstitutionCode"];
                            //IInstitution institution = null;
                            List<ICustomerRegistration> pendingCustomerRegistration = new List<ICustomerRegistration>();
                            pendingCustomerRegistration = new CustomerRegistrationSystem().GetPendingCustomerRegistrations();
                            Trace.TraceInformation(String.Format("Gotten pending/failed registrations. Pending: {0} | Failed: {1}... ",
                                pendingCustomerRegistration.Where(x => x.Status == RegistrationStatus.Pending).ToList().Count,
                                pendingCustomerRegistration.Where(x => x.Status == RegistrationStatus.Failed).ToList().Count));
                            if (pendingCustomerRegistration != null && pendingCustomerRegistration.Count > 0)
                            {
                                //var groupedPendingCustomerRegistration = pendingCustomerRegistration.GroupBy(x => x.InstitutionCode).ToDictionary(x => x.Key, x => x.ToList());

                                foreach (var pendingAccountToCreate in pendingCustomerRegistration)
                                {
                                    isCoreBankingDone = false;
                                    isMobileAccountDone = false;
                                    isSuccessful = false;
                                    string instName = string.Empty;
                                    //Agent agent = null;
                                    //using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                                    //{
                                    //    agent = new AgentSystem(_theDataSource).GetByCode(pendingAccountToCreate.AgentCode) as Agent;
                                    //}
                                    //if (agent == null)
                                    //{
                                    //    throw new Exception(string.Format("Could not retreive agent with code: {0}", pendingAccountToCreate.AgentCode));
                                    //}
                                    instName = pendingAccountToCreate.InstitutionName;
                                    string delimiter = ";";
                                    MobileAccount mob = new MobileAccount { LastName = pendingAccountToCreate.LastName, OtherNames = pendingAccountToCreate.FirstName, DateCreated = DateTime.Now, MobilePhone = pendingAccountToCreate.PhoneNumber, IsAgentAccount = false, OpenedBy = AccountRegistrarType.Agent };
                                    mob.RegistrarID = pendingAccountToCreate.AgentID;
                                    mob.RegistrarCode = pendingAccountToCreate.AgentCode;
                                    mob.RegistrarName = pendingAccountToCreate.AgentName;



                                    // mob.MobileAccountStatus = false ? MobileAccountStatus.PendingFundsTransfer : MobileAccountStatus.New;
                                    mob.MobileAccountStatus = MobileAccountStatus.Active;
                                    bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);



                                    mob.ActivationCode = new MobileAccountSystem().GenerateActivationCode();
                                    pendingAccountToCreate.ActivationCode = mob.ActivationCode;
                                    try
                                    {
                                        mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                                    }
                                    catch
                                    {
                                        throw new NoHSMResponseException("No Response From HSM");
                                    }

                                    //TODO set activation code as PIN
                                    mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());

                                    mob.InstitutionCode = pendingAccountToCreate.InstitutionCode;

                                    MobileAccount coreBankingAcct = new MobileAccount();
                                    if (pendingAccountToCreate.Status == RegistrationStatus.Failed && pendingAccountToCreate.CommitPoint == CommitPoint.BankOneMobile)
                                    {
                                        Trace.TraceInformation(String.Format("****Now processing a failed registration. ID: {0}**** ", pendingAccountToCreate.ID));
                                        if(string.IsNullOrEmpty(pendingAccountToCreate.CustomerID) || string.IsNullOrEmpty(pendingAccountToCreate.NUBAN) || string.IsNullOrEmpty(pendingAccountToCreate.StandardAccountNumber))
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Failed Reg with id {0} failed again because one or more required values are missing", pendingAccountToCreate.ID));
                                            continue;
                                        }
                                        #region Failed Registration Fix - BankOneMobile

                                        var bankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                                        var type = bankAccountHelper.GetInstitutionAccountNumberType(pendingAccountToCreate.InstitutionCode);

                                        coreBankingAcct.DateOfBirth = pendingAccountToCreate.DateOfBirth;
                                        coreBankingAcct.PlaceOfBirth = pendingAccountToCreate.PlaceOfBirth;
                                        coreBankingAcct.NOKName = pendingAccountToCreate.NOKName;
                                        coreBankingAcct.NOKPhone = pendingAccountToCreate.NOKPhone;
                                        coreBankingAcct.Address = pendingAccountToCreate.Address;
                                        coreBankingAcct.ReferalPhone = "";
                                        coreBankingAcct.CustomerID = pendingAccountToCreate.CustomerID;
                                        coreBankingAcct.TheGender = pendingAccountToCreate.TheGender;
                                        coreBankingAcct.HasSufficientInfo = false;
                                        coreBankingAcct.AccountSource = AccountSource.Others;
                                        coreBankingAcct.NUBAN = pendingAccountToCreate.NUBAN;

                                        //Dont freak out. just used to hold the accNo for now
                                        switch (type)
                                        {
                                            case AccountNumberType.NUBAN:
                                                coreBankingAcct.DisplayMessage = pendingAccountToCreate.NUBAN; break;
                                            case AccountNumberType.Standard:
                                            default:
                                                coreBankingAcct.DisplayMessage = pendingAccountToCreate.StandardAccountNumber; break;
                                        }
                                        isCoreBankingDone = true;
                                        #endregion
                                        Trace.TraceInformation(String.Format("****DONE processing failed registration.**** "));
                                    }
                                    else
                                    {
                                        #region Core Banking Account Creation
                                        System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                        Trace.TraceInformation(String.Format("****CoreBanking Customer Creation**** "));
                                        coreBankingAcct = CreateAccountOnCoreBanking(pendingAccountToCreate as CustomerRegistration);
                                        Trace.TraceInformation(String.Format("****CoreBanking Customer Creation. DONE...**** "));
                                        System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                        #endregion
                                    }

                                    #region Mobile Account Creation
                                    if (coreBankingAcct != null)
                                    {
                                        if (isCoreBankingDone)
                                        {
                                            try
                                            {
                                                System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                                Trace.TraceInformation(String.Format("****Mobile account Creation**** "));
                                                pendingAccountToCreate.CommitPoint = CommitPoint.BankOneMobile;
                                                try
                                                {
                                                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate as CustomerRegistration);
                                                }
                                                catch (Exception ex)
                                                {
                                                    System.Diagnostics.Trace.TraceError(string.Format("Core banking account has been created and about to create mobile account but could not update this request. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
                                                }
                                                LinkingBankAccount acc = new LinkingBankAccount
                                                {
                                                    //TheProduct = prod,
                                                    InstitutionCode = pendingAccountToCreate.InstitutionCode,
                                                    TheMobileAccount = mob,
                                                    Activated = false,
                                                    CustomerID = coreBankingAcct.CustomerID,
                                                    InstitutionName = instName,
                                                    TheGender = coreBankingAcct.TheGender,
                                                    //Ensure you set institution code before doing this
                                                    BankAccount = coreBankingAcct.DisplayMessage,
                                                    NUBAN = coreBankingAcct.DisplayMessage,
                                                    //Passport = pendingAccountToCreate.Passport
                                                };
                                                if (new MobileAccountSystem().GetByAccountNumber(acc.BankAccount, pendingAccountToCreate.InstitutionCode) != null)
                                                {
                                                    throw new AlreadyExistingAccountException("Account Already Exists");
                                                }

                                                mob.SelectedBankAccounts = new List<ILinkingBankAccount>();
                                                mob.SelectedBankAccounts.Add(acc);
                                                mob.RecievingBankAccount = acc;
                                                mob.TheGender = pendingAccountToCreate.TheGender;
                                                mob.CustomerID = coreBankingAcct.CustomerID;
                                                acc.ProductCode = pendingAccountToCreate.ProductCode;
                                                acc.ProductName = pendingAccountToCreate.ProductName;
                                                mob.ReceivingAccountNumber = mob.RecievingBankAccount.BankAccount;

                                                Trace.TraceInformation(String.Format("About to save the mobile account"));
                                                var theMobileAccount = new MobileAccountSystem().SaveMobileAccountFromService(mob);
                                                isMobileAccountDone = true;
                                                Trace.TraceInformation(String.Format("****Mobile Account Creation Done**** "));
                                                System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                            }
                                            catch (Exception ex)
                                            {
                                                isMobileAccountDone = false;
                                                System.Diagnostics.Trace.TraceError(string.Format("Core banking account has been created but something went wrong trying to create the mobile account. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
                                                pendingAccountToCreate.Status = RegistrationStatus.Failed;
                                                pendingAccountToCreate.ErrorInfo = ex.Message;
                                                try
                                                {
                                                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate as CustomerRegistration);
                                                }
                                                catch (Exception innerEx)
                                                {
                                                    System.Diagnostics.Trace.TraceError(string.Format("Mobile account creation error. Error while trying to update request. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, innerEx.Message));
                                                }
                                                try
                                                {
                                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                                    {
                                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                        string body = string.Format("{0}\nAgentName:{1}\nCustomerPhone: {2}\n{3}\n{4}", ex.Message, pendingAccountToCreate.AgentName, pendingAccountToCreate.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                        client.SendEmail(pendingAccountToCreate.InstitutionCode, "donotreply@mybankone.com",
                                                            emails, string.Format("BankOneMobile-CommitPoint: BankOneMobile:{0}", ex.Message), body);
                                                    }
                                                }
                                                catch (Exception emailException)
                                                {
                                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                                }
                                                System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Card Linking
                                    if (isMobileAccountDone)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                        Trace.TraceInformation(String.Format("****Card Linking**** "));

                                        string response = LinkCardAccountNumber(pendingAccountToCreate as CustomerRegistration);

                                        Trace.TraceInformation(String.Format("****Card Linking DONE**** "));
                                        System.Diagnostics.Trace.TraceError(string.Format("-----------------------------------------------------------------------------------"));
                                    }
                                    #endregion
                                    if (isSuccessful)
                                    {
                                        
                                        pendingAccountToCreate.Status = RegistrationStatus.Successful;
                                        pendingAccountToCreate.CommitPoint = 0;
                                        pendingAccountToCreate.IsSmsSent = false;
                                        try
                                        {
                                            new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate as CustomerRegistration);
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("All has gone well but could not update this request to successful. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
                                        }
                                        //System.Diagnostics.Trace.TraceError(string.Format("All has gone well. Now sending SMS. ID: {0}", pendingAccountToCreate.ID));
                                   
                                        //string message = string.Format("Congratulations. You have now opened a {0} from {1}:\n AccountNumber:{2}\n Mobile PIN: [{3}]", pendingAccountToCreate.ProductName, pendingAccountToCreate.InstitutionName, pendingAccountToCreate.NUBAN, pendingAccountToCreate.ActivationCode);
                                        //bool sent = new MobileAccountSystem().SendInstitutionSMS(message, pendingAccountToCreate.PhoneNumber, pendingAccountToCreate.InstitutionCode, pendingAccountToCreate.NUBAN);
                                        //System.Diagnostics.Trace.TraceError(string.Format("SMS Sending returned: {0}", sent));
                                        //if (!sent)
                                        //{
                                        //    System.Diagnostics.Trace.TraceError(string.Format("SMS was not sent to customer's phone. Message: {0}, Phone Number: {1}, Request ID: {2}", message, pendingAccountToCreate.PhoneNumber, pendingAccountToCreate.ID));
                                        //    try
                                        //    {
                                        //        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        //        {
                                        //            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        //            string body = string.Format("Message:{0}\nAgentID:{1}\nCustomerPhoneNo: {2}", message, pendingAccountToCreate.AgentID, pendingAccountToCreate.PhoneNumber);
                                        //            client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                                        //                emails, string.Format("BankOneMobile:{0}", "SMS Not Delivered To Customer"), body);
                                        //        }
                                        //    }
                                        //    catch (Exception emailException)
                                        //    {
                                        //        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        //    }
                                        //}
                                        System.Diagnostics.Trace.TraceError(string.Format("-----------------------MOVING ON TO THE NEXT-----------------------"));
                                    }
                                }
                            }
                            Trace.TraceInformation(String.Format("BankOneMobile Customer Creation And Card Linking Service. DONE... "));
                            Trace.TraceInformation(String.Format("**************************************************************"));
                            #endregion

                            
                        }    
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(String.Format(ex.Message));
                        try
                        {
                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                            {
                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                    emails, string.Format("BankOneMobile-CommitPoint: CoreBanking:{0}", ex.Message), body);
                            }
                        }
                        catch (Exception emailException)
                        {
                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                        }
                    }
                    this.timer.Interval = 1000 * Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"]);
                }
                finally
                {
                    this.timer.Start();
                }
            }
        }

        private MobileAccount CreateAccountOnCoreBanking(CustomerRegistration pendingAccountToCreate)
        {
            pendingAccountToCreate.CommitPoint = CommitPoint.CoreBanking;
            try
            {
                new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Account about to be created on Corebanking but could not update this request. Exception: {0}", ex.Message));
                throw ex;
            }

            MobileAccount toReturn = new MobileAccount();
            SwitchingServiceRef.Account acc = null;
            try
            {
                SwitchingServiceRef.Gender gender = (SwitchingServiceRef.Gender)Enum.Parse(typeof(SwitchingServiceRef.Gender), pendingAccountToCreate.TheGender.ToString());
                SwitchingServiceRef.AccountInformationSource ifoSource = SwitchingServiceRef.AccountInformationSource.Others; //(SwitchingServiceRef.AccountInformationSource)Enum.Parse(typeof(SwitchingServiceRef.AccountInformationSource), "");
                AccountSource mobileIfoSource = AccountSource.Others; //(AccountSource)Enum.Parse(typeof(AccountSource), "");

                var bankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                var type = bankAccountHelper.GetInstitutionAccountNumberType(pendingAccountToCreate.InstitutionCode);

                using (SwitchingServiceRef.SwitchingServiceClient client = new SwitchingServiceRef.SwitchingServiceClient())
                {
                    
                    acc = client.CreateAccount(pendingAccountToCreate.InstitutionCode, pendingAccountToCreate.LastName, pendingAccountToCreate.FirstName,
                        pendingAccountToCreate.PhoneNumber, gender, pendingAccountToCreate.ProductCode, pendingAccountToCreate.AccountOfficerCode,
                        pendingAccountToCreate.PlaceOfBirth, pendingAccountToCreate.DateOfBirth, pendingAccountToCreate.Address, "", pendingAccountToCreate.NOKName,
                        pendingAccountToCreate.NOKPhone, "", "", false, ifoSource, "", null);                   
                }
                if (acc == null)
                {
                    toReturn = null;
                    Trace.TraceInformation(String.Format("Account not created on Corebanking... ID: {0}", pendingAccountToCreate.ID));
                    throw new Exception("Account not created on Corebanking");
                }

                pendingAccountToCreate.StandardAccountNumber = acc.Number;
                pendingAccountToCreate.NUBAN = acc.NUBAN;
                pendingAccountToCreate.CustomerID = acc.CustomerID;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Account has been created on corebanking but could not update this request. NUBAN: {0}, Standard: {1} CustomerID: {2}. Exception: {3}", acc.NUBAN, acc.Number, acc.CustomerID, ex.Message));
                }


                toReturn.DateOfBirth = pendingAccountToCreate.DateOfBirth;
                toReturn.PlaceOfBirth = pendingAccountToCreate.PlaceOfBirth;
                toReturn.NOKName = pendingAccountToCreate.NOKName;
                toReturn.NOKPhone = pendingAccountToCreate.NOKPhone;
                toReturn.Address = pendingAccountToCreate.Address;
                toReturn.ReferalPhone = "";
                toReturn.CustomerID = acc.CustomerID;
                toReturn.TheGender = pendingAccountToCreate.TheGender;
                toReturn.HasSufficientInfo = false;
                toReturn.AccountSource = mobileIfoSource;
                toReturn.NUBAN = acc.NUBAN;

                //Dont freak out. just used to hold the accNo for now
                switch (type)
                {
                    case AccountNumberType.NUBAN:
                        toReturn.DisplayMessage = acc.NUBAN; break;
                    case AccountNumberType.Standard:
                    default:
                        toReturn.DisplayMessage = acc.Number; break;
                }
                isCoreBankingDone = true;
            }
            catch (Exception ex)
            {
                isCoreBankingDone = false;
                System.Diagnostics.Trace.TraceError("Core banking web service exception.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //pendingAccountToCreate.Status = RegistrationStatus.Failed;
                pendingAccountToCreate.ErrorInfo = ex.Message;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception innerEx)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("A core banking exception occured and could not update this request. Exception: {0}",innerEx.Message));
                }
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("{0}\nAgentName:{1}\nCustomerPhone: {2}\n{3}\n{4}", ex.Message, pendingAccountToCreate.AgentName, pendingAccountToCreate.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(pendingAccountToCreate.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile-CommitPoint: CoreBanking:{0}", ex.Message), body);
                    }
                }
                catch(Exception emailException)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                }
            }
            return toReturn;
        }
        private string LinkCardAccountNumber(CustomerRegistration pendingAccountToCreate)
        {
            pendingAccountToCreate.CommitPoint = CommitPoint.CardManagement;
            try
            {
                new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Account about to be created on Corebanking but could not update this request. ID: {0}, Exception: {1}",pendingAccountToCreate.ID, ex.Message));
            }
            string toReturn = string.Empty;
            try
            {
                //Agent agent = null;
                //using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                //{
                //    agent = new AgentSystem(_theDataSource).GetByCode(pendingAccountToCreate.AgentCode) as Agent;
                //}
                IssuanceServiceRef.BankOneCardIssuanceClient client = new IssuanceServiceRef.BankOneCardIssuanceClient();
                toReturn = client.LinkCardAccount(pendingAccountToCreate.CardSerialNumber, pendingAccountToCreate.NUBAN, pendingAccountToCreate.InstitutionCode, pendingAccountToCreate.AgentCode);

                if (!toReturn.StartsWith("00"))
                {
                    throw new ApplicationException(toReturn.Split("-".ToCharArray())[1]);
                }
                isSuccessful = true;
                client.Close();
            }
            catch(Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Card linking error. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, ex.Message));
                pendingAccountToCreate.Status = RegistrationStatus.Failed;
                pendingAccountToCreate.ErrorInfo = ex.Message;
                try
                {
                    new CustomerRegistrationSystem().UpdateCustomerRegistration(pendingAccountToCreate);
                }
                catch (Exception innerEx)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Card linking error. Error while trying to update request. ID: {0}, Exception: {1}", pendingAccountToCreate.ID, innerEx.Message));
                }
                try
                {
                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        string body = string.Format("{0}\nAgentName:{1}\nCustomerPhone: {2}\n{3}\n{4}", ex.Message, pendingAccountToCreate.AgentName, pendingAccountToCreate.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(pendingAccountToCreate.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile-CommitPoint: Card Management:{0}", ex.Message), body);
                    }
                }
                catch (Exception emailException)
                {
                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                }
            }
            return toReturn;
        }

    }
}
