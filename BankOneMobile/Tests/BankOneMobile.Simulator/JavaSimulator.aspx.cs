﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BankOneMobile.Services;

public partial class JavaSimulator : System.Web.UI.Page
{
    private const string SS_ID = "::SessionID::";

    private const string SS_PhoneNo = "::PhoneNo::";

    private Guid SessionID
    {
        get
        {
            if (Session[SS_ID] == null)
            {
                Session[SS_ID] = Guid.NewGuid();
            }
            return (Guid)Session[SS_ID];
        }
        set
        {
            Session[SS_ID] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtPhoneNumber.Text = "2348034730365";
        }
    }
    protected void btnStart_Click(object sender, EventArgs e)
    {
        CryptoUtil.CryptoServices crypt = new CryptoUtil.CryptoServices();
        string phoneNo = txtPhoneNumber.Text;
        string activationCode =  string.Empty;
        string url = System.Configuration.ConfigurationManager.AppSettings["HttpServiceURL"];
        string encryptedPhone = crypt.BCEncrypt(phoneNo);
        string encryptedActCode = crypt.BCEncrypt(activationCode);
        string encryptedSess = crypt.BCEncrypt(SessionID.ToString());
        string encryptedOperation = crypt.BCEncrypt("Start");
        string postData = string.Format("OPERATION={0}&MSISDN={1}&SESSION_ID={2}&ACTIVATION_CODE={3}&USE_XML=false", encryptedOperation, encryptedPhone, encryptedSess,encryptedActCode);
        string rspnse =  SessionSystem.DoHttpRequest(url, postData);
        rspnse = crypt.BCDecrypt(rspnse);
        string ch = @"Response>";
        int textToRemove = rspnse.LastIndexOf(ch);
        if (textToRemove> - 1)
        {
            rspnse = rspnse.Remove(textToRemove - 1);
            rspnse.Remove(rspnse.Length - 1);
        }

        lblResponse.Text = rspnse.Replace("\r\n", "<br/>").Replace("\n", "<br/>"); 
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        string phoneNo = txtPhoneNumber.Text;
        string text = txtResponse.Text;

        string url = System.Configuration.ConfigurationManager.AppSettings["HttpServiceURL"];
        
        CryptoUtil.CryptoServices crypt = new CryptoUtil.CryptoServices();
        string operation = crypt.BCEncrypt("NEXT");
        string encryptedPhone = crypt.BCEncrypt(phoneNo);
        string encryptedSessID = crypt.BCEncrypt(SessionID.ToString());
        string encryptedResponse = crypt.BCEncrypt(text);

        string postData = string.Format("OPERATION={0}&MSISDN={1}&SESSION_ID={2}&TEXT={3}&USE_XML=True", operation, encryptedPhone, encryptedSessID, encryptedResponse);
        string rspnse = SessionSystem.DoHttpRequest(url, postData);
        rspnse = crypt.BCDecrypt(rspnse);
        string ch = @"Response>";
        int textToRemove = rspnse.LastIndexOf(ch);
        if (textToRemove > -1)
        {
            rspnse = rspnse.Remove(textToRemove - 1);
            rspnse.Remove(rspnse.Length - 1);
        }

        lblResponse.Text = rspnse.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
    }
}