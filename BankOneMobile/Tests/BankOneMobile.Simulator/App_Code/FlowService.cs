﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FlowService" in code, svc and config file together.
[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class FlowService : IFlowService
{
    private FlowSystem _flowSystem = new FlowSystem(DataSourceFactory.GetDataSource(DataCategory.Shared));
    #region IFlowService Members


    public long SaveFlow(IFlow theFlow)
    {
        _flowSystem.SaveFlow(theFlow);
        return theFlow.ID;
    }

    public List<IFlow> GetAllFlows()
    {
        return _flowSystem.GetAllFlows();
    }

    public void UpdateFlow(IFlow theFlow)
    {
        _flowSystem.UpdateFlow(theFlow);
    }

    public IFlow GetFlowById(long id)
    {
        return _flowSystem.GetFlow(id);
    }

    #endregion
}
