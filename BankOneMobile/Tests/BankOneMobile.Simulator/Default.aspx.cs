﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page
{
    private const string SS_ID = "::SessionID::";

    private const string SS_PhoneNo = "::PhoneNo::";

    private Guid SessionID
    {
        get
        {
            if (Session[SS_ID] == null)
            {
                Session[SS_ID] = Guid.NewGuid();
            }
            return (Guid)Session[SS_ID];
        }
        set
        {
            Session[SS_ID] = value;
        }
    }

    private string PhoneNo
    {
        get
        {
            if (Session[SS_PhoneNo] == null)
            {
                Session[SS_PhoneNo] = "2348071543645";//2348044730365
            }
            return Session[SS_PhoneNo] as string;
        }
        set
        {
            Session[SS_PhoneNo] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtPhoneNumber.Text = "2348076774533";
        }
    }

    protected List<string> GetPhoneNumbers()
    {
        List<string> numbers = new List<string>();
        //for (long i = 0; i < 1000; i++)
        //{
        //    numbers.Add((i+300000000).ToString());
        //}
        numbers = new List<string>()
        {
           // "08034730365"//,"07037400771",
            "08127714071"//,"08062108208"
        };
        return numbers;
    }
    protected void btnStart_Click(object sender, EventArgs e)
    {
        PhoneNo = txtPhoneNumber.Text;

        //var list = new List<Guid>();
        //for (int i = 0; i < 100; i++)
        //{
        //    list.Add(Guid.NewGuid());
        //}
      
        //    for (int i = 0; i < 10; i++)
        //    {
        //        Thread newThread = new Thread(new ThreadStart(() =>
        //        //list.AsParallel().ForAll(sessionid =>

        //        //for (int i = 0; i < 1000; i++)
        //        {
        //            //   GetPhoneNumbers().AsParallel().ForAll(no =>
        //            //{
        //            //    var sesId = Guid.NewGuid();
        //            try
        //            {
        SessionID = Guid.NewGuid();
                        using (BankOneService.UssdThirdPartyWsPortClient client = new BankOneService.UssdThirdPartyWsPortClient())
                        {
                            var response = client.onSessionStart(SessionID.ToString(), new BankOneService.menuRequestParams()
                            {
                                msisdn = PhoneNo
                            });
                            ProcessResponse(response);

            //                response = client.onUserResponse(sessionid.ToString(), new BankOneService.menuRequestParams()
            //                {
            //                    msisdn = PhoneNo,
            //                    text = "2*1*08032762996*100*1111"
            //                });
            //                //});
            //                client.onSessionEnd(sessionid.ToString(), 200, "Life");
            //                //Thread.Sleep(100);
                        }
            //        }
            //        catch { throw; }
            //    }));
            //    newThread.Start();
            //}
        
        //Thread.Sleep(60000);
    }

    protected void btnEnd_Click(object sender, EventArgs e)
    {
        //PhoneNo = txtPhoneNumber.Text;
        using (BankOneService.UssdThirdPartyWsPortClient client = new BankOneService.UssdThirdPartyWsPortClient())
        {
            foreach (var num in GetPhoneNumbers())
            {
                client.onSessionEnd(num,200,"Life");                
            }
        }

    }

    private void ProcessResponse(BankOneService.menuResponseParams response)
    {
        if (response != null)
        {
            if (response.ussdMenu != null)
            {
                
                //System.IO.File.WriteAllText("C:\\TFS\\PANE\\BankOneMobile\\Main\\Source\\BankOneMobile\\Tests\\BankOneMobile.Simulator\\response.txt", response.ussdMenu);
                bool isXmlResponse = false;
                if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("XmlResponse"))
                {
                    isXmlResponse = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["XmlResponse"]);
                }
                if(isXmlResponse)
                {
                    ParseXmlResponse(response.ussdMenu);
                }
                else
                {
                    lblResponse.Text = response.ussdMenu.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
                }
            }
            txtResponse.Visible = btnSend.Visible = !response.shouldClose;
            if (response.shouldClose)
            {
                using (BankOneService.UssdThirdPartyWsPortClient client = new BankOneService.UssdThirdPartyWsPortClient())
                {
                    client.onSessionEnd(SessionID.ToString(), 200, "Ended Normally");
                }
            }
        }
        txtResponse.Text = string.Empty;
        
    }

    private void ParseXmlResponse(string response)
    {
        try
        {
            XElement xmlResponse = XElement.Parse(response);
            //Get Element that determines whether to mask the textbox
            txtResponse.TextMode = TextBoxMode.SingleLine;
            XElement shouldMask = xmlResponse.Elements().First(x => x.Name == "ShouldMask");
            if(Convert.ToInt32(shouldMask.Value) == 1)
            {
                //Mask the textbox
                txtResponse.TextMode = TextBoxMode.Password;
            }
            XElement display = xmlResponse.Elements().First(x => x.Name == "Display");
            if(display.HasElements)
            {
                //This is a menu screen
                //Before Menu
                if(display.Elements().Any(x => x.Name == "BeforeMenu"))
                {
                    XElement beforeMenu = display.Elements().First(x => x.Name == "BeforeMenu");
                    lblResponse.Text = beforeMenu.Value.Replace("\r\n", "<br/>").Replace("\n", "<br/>") + "<br/>";
                }
                //Menu
                if (display.Elements().Any(x => x.Name == "Menu"))
                {
                    XElement menu = display.Elements().First(x => x.Name == "Menu");
                    foreach(XElement menuItem in menu.Elements())
                    {
                        if(menuItem.Attributes().Any(x => x.Name == "Index"))
                        {
                            //Contains Index Attribute
                            lblResponse.Text += string.Format("{0}.{1}<br />", menuItem.Attributes().First(x => x.Name == "Index").Value, menuItem.Value);
                        }
                        else
                        {
                            //No Index Attribute
                            lblResponse.Text += string.Format("{0}<br />", menuItem.Value);
                        }
                    }
                }
                //After Menu
                if (display.Elements().Any(x => x.Name == "AfterMenu"))
                {
                    XElement afterMenu = display.Elements().First(x => x.Name == "AfterMenu");
                    lblResponse.Text += afterMenu.Value.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
                }
            }
            else
            {
                //Text Response
                lblResponse.Text = display.Value.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
            }
        }
        catch
        {
            //Must be plain text not xml
            lblResponse.Text = response.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        using (BankOneService.UssdThirdPartyWsPortClient client = new BankOneService.UssdThirdPartyWsPortClient())
        {
            var response = client.onUserResponse(SessionID.ToString(), new BankOneService.menuRequestParams()
            {
                msisdn = PhoneNo,
                text = txtResponse.Text
            });

            ProcessResponse(response);
        }
        
    }
}
