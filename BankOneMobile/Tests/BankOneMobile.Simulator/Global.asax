﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs ev) 
    {
        // Code that runs on application startup
        
       // AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
        //BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
        //    ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
        //    ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
        //    ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
        //    Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
        //    AppZoneSwitch.Extension.ApplicationInitializer.Init();
        //    AppZoneSwitch.Core.Implementations.Node node = new AppZoneSwitch.Core.Implementations.Node();
        //    AppZoneSwitch.Services.RoutingSystem.InitializeEngine();
        //    AppZoneSwitch.Services.RoutingSystem.SystemInitialize += (o, e) =>
        //    {
        //        AppZoneSwitch.Services.Implementations.TransactionSplitter.InitializeEngine();
        //        AppZoneSwitch.Services.ReversalSystem.InitializeEngine();
        //        System.Diagnostics.Trace.TraceInformation("System Initialized");
        //    };
              
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
