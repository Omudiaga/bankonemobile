﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Label ID="Label1" runat="server" Text="Enter Phone No:"></asp:Label>
&nbsp;<br />
    <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnStart" runat="server" Text="Start Session" 
        onclick="btnStart_Click" />

    <asp:Button ID="btnEnd" runat="server" Text="End Session" 
        onclick="btnEnd_Click" />
<br />
<br />
<asp:Literal ID="lblResponse" runat="server"></asp:Literal>
<br />
<asp:TextBox ID="txtResponse" runat="server" ></asp:TextBox>
<asp:Button ID="btnSend" runat="server" Text="Send" onclick="btnSend_Click" 
        />
</asp:Content>
