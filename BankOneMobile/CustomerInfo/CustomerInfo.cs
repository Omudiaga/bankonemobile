﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.CustomerInfo.Interface;

namespace CustomerInfo
{
    public class CustomerInfo : ICustomerInfo
    {
      public string CustomerID { set; get; }
      public  string LastName { set; get; }
      public string OtherNames { set; get; }
      public string Address { set; get; }
      public string EmailAddress { set; get; }
      public string Sex { set; get; }
      public DateTime DOB { set; get; }
      public List<IAccount> Accounts { set; get; }
      public ICustomerInfo GetCustomerInfo(string InstitutionCode,string CustomerID)
      {
          CustomerInfo customer = new CustomerInfo();
          customer.CustomerID = CustomerID;
          customer.LastName  = "Alale";
          customer.DOB = DateTime.Today;
          customer.Address = "4 Alagomeji Yaba,Lagos";
          customer.EmailAddress = "Fread.Alale@yahoo.com";
          customer.OtherNames = "Fread";
          customer.Sex = "Male";

          customer.Accounts = new List<IAccount>();
          customer.Accounts.Add(new Account { AccountNumber = "0340001112229", Type = "Savings" });
          customer.Accounts.Add(new Account { AccountNumber = "0340001113559", Type = "Savings" });
          customer.Accounts.Add(new Account { AccountNumber = "0310001115409", Type = "Current" });
          customer.Accounts.Add(new Account { AccountNumber = "0310001111199", Type = "Current" });
          return customer;
      }

      public string CreateCustomerAccount(ICustomerInfo customer, out string customerID)
      {
          customerID = "7364235";
          string accountNumber = "0341112227654";


          return accountNumber;

      }
    }
}
