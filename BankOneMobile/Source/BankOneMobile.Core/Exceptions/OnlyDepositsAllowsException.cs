﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class OnlyDepositsAllowsException : ApplicationException
    {

        public OnlyDepositsAllowsException(string msg)
            : base(msg)
        {
        }
    }
}
