﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidSenderPhoneNumberException : ApplicationException
    {
        public InvalidSenderPhoneNumberException(string msg)
            : base(msg)
        {
        }
    }
}
