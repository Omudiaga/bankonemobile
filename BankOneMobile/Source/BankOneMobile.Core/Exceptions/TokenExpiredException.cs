﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class TokenExpiredException : ApplicationException
    {
        public TokenExpiredException(string msg)
            : base(msg)
        {
        }
    }
}
