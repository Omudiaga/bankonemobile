﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidInstitutionException : ApplicationException
    {
        public InvalidInstitutionException(string msg)
            : base(msg)
        {
        }
    }
}
