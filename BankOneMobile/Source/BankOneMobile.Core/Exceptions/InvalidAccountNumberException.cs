﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidAccountNumberException : ApplicationException
    {
        public InvalidAccountNumberException(string msg)
            : base(msg)
        {
        }
    }
}
