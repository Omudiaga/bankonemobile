﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidRecieverPhoneNumberException : ApplicationException
    {
        public InvalidRecieverPhoneNumberException(string msg)
            : base(msg)
        {
        }
    }
}
