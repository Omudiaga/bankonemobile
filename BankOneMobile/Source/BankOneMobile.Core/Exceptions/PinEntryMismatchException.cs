﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class PinEntryMismatchException : ApplicationException
    {
        public PinEntryMismatchException(string msg)
            : base(msg)
        {
        }
    }
}
