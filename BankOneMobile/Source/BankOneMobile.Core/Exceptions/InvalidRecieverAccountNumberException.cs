﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidRecieverAccountNumberException : ApplicationException
    {
        public InvalidRecieverAccountNumberException(string msg)
            : base(msg)
        {
        }
    }
}
