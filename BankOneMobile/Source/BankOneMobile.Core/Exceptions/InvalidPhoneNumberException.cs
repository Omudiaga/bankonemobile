﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidPhoneNumberException : ApplicationException
    {
        public InvalidPhoneNumberException(string msg)
            : base(msg)
        {
        }
    }
}
