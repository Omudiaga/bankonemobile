﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class NonUniqueSessionIDException : ApplicationException
    {
        public NonUniqueSessionIDException(string msg)
            : base(msg)
        {
        }
    }
}
