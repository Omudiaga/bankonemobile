﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BankOneMobile.Core.Exceptions.WebService
{
    [DataContract]
    public class NoHSMResponseException
    {
        [DataMember]
        public string Message { set; get; }
        public NoHSMResponseException(string msg)
        {
            Message = msg;
        }
    }
}
