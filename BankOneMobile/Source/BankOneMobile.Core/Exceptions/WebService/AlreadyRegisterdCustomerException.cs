﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Exceptions.WebService
{
    [DataContract]
    public class AlreadyRegisterdCustomerException
    {
        [DataMember]
        public string Message { set; get; }
        public AlreadyRegisterdCustomerException(string msg)
        {
            Message = msg;
        }
    }
}
