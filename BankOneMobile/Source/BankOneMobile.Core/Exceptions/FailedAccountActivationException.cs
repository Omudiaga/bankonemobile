﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class FailedAccountActivationException : ApplicationException
    {
        public FailedAccountActivationException(string msg)
            : base(msg)
        {
        }
    }
}
