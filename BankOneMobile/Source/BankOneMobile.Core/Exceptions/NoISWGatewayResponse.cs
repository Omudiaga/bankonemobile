﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
   public  class NoISWGatewayResponse : ApplicationException
    {
       public NoISWGatewayResponse(string msg)
           : base(msg)
       {
       }
    }
}
