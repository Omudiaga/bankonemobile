﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class NoEligibleLoanProductException : ApplicationException
    {
        public NoEligibleLoanProductException(string msg)
            : base(msg)
        {
        }
    }
}
