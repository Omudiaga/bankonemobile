﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidTokenException : ApplicationException
    {
        public InvalidTokenException(string msg)
            : base(msg)
        {
        }
    }
}
