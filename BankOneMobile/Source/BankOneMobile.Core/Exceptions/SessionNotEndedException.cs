﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class SessionNotEndedException : ApplicationException
    {
        public SessionNotEndedException(string msg)
            : base(msg)
        {
        }
    }
}
