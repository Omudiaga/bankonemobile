﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class TransactionNotSupportedByInstitution : ApplicationException
    {
        public TransactionNotSupportedByInstitution(string msg)
            : base(msg)
        {
        }
    }
}
