﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class NoSwitchResponseException : ApplicationException
    {
        public NoSwitchResponseException(string msg)
            : base(msg)
        {
        }
    }
}
