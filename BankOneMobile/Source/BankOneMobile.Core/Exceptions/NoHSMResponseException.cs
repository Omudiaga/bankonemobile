﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    //[DataContract]
    public class NoHSMResponseException : ApplicationException
    {
        public NoHSMResponseException(string msg)
            : base(msg)
        {
        }
    }
}
