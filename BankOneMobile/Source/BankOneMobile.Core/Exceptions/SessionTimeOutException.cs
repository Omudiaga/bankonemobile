﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class SessionTimeOutException : ApplicationException
    {
        public SessionTimeOutException(string msg)
            : base(msg)
        {
        }
    }
}
