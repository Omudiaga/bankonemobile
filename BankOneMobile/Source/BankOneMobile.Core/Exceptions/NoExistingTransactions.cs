﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
   public  class NoExistingTrnsactionException : ApplicationException
    {
       public NoExistingTrnsactionException(string msg)
           : base(msg)
       {
       }
    }
}
