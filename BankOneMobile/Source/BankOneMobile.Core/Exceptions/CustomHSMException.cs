﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class CustomHSMException : ApplicationException
    {
        public CustomHSMException(string msg)
            : base(msg)
        {
        }
    }
}
