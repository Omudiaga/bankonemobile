﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class TransactionNotPermittedToNonAgentsException : ApplicationException
    {
        public TransactionNotPermittedToNonAgentsException(string msg)
            : base(msg)
        {
        }
    }
}
