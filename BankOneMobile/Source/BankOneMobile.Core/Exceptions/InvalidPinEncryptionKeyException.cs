﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidPinEncryptionKeyException : ApplicationException
    {
        public InvalidPinEncryptionKeyException(string msg)
            : base(msg)
        {
        }
    }
}
