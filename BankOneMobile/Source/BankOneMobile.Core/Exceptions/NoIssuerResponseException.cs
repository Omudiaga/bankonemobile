﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class NoIssuerResponseException : ApplicationException
    {
        public NoIssuerResponseException(string msg)
            : base(msg)
        {
        }
    }
}
