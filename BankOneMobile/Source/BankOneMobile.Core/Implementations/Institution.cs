﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class Institution :EntityWithEnableDisable, IInstitution
    {
        public virtual string Code { set; get; }
        public virtual string Name { set; get; }
        public virtual string ShortName { get; set; }
        public virtual bool AllowInterMFBTrans { get; set; }
        public virtual string  TokenLifeSpan { get; set; }
       public virtual  IFlow TheAgentStartFlow { get; set; }
       public virtual  IFlow TheCustomerStartFlow { get; set; }
       public virtual BankOneMobile.Core.Helpers.Enums.AccountNumberType TheAccountNumberType { get; set; }
       
    }
}
