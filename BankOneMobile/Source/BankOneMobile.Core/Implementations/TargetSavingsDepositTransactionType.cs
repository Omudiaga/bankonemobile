﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class TargetSavingsDepositTransactionType : EntityWithEnableDisable, ITargetSavingsDepositTransactionType
    {
        public virtual string TargetSavingsAccountNumber { get; set; }
        public virtual string BetaAccountNumber { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual LinkingBankAccount TheBankAccount { get; set; }
        public virtual ITransaction TheTransaction { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

    }
}
