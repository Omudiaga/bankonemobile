﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Attributes;

namespace BankOneMobile.Core.Implementations
{
    public class PaymentItem : EntityWithEnableDisable, IPaymentItem
    {
      public virtual   string Code { set; get; }
      
      public virtual string Name { set; get; }
        [CustomerIDAttribute]
      public virtual string CustomerIDName { get; set; }
      public virtual IMerchant TheMerchant { get; set; }
      public override string ToString()
      {
          return string.Format("{0} (N{1})", this.Name, (this.UnitPrice / 100).ToString("N"));

      }
        [AmountAttribute]
      public virtual decimal UnitPrice { get; set; }
     
    }
}
