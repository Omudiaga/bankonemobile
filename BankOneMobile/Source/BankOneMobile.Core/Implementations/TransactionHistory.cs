﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
   public  class TransactionHistory
    {

        public virtual DateTime Date { get; set; }
       public virtual string  Status { get; set; }
       /// <summary>
       ///The response gotten 
       /// </summary>
       
       public virtual decimal Amount { get; set; }
       /// <summary>
       ///Descripton  of the  Response gotten
       /// </summary>
       
       public virtual string TransactionType { get; set; }
        public virtual TransactionDirection? Direction {get;set;}
       public override string ToString()
       {
           return string.Format("N{1} {0} ON {2}", TransactionType,(Amount/100).ToString("N"), Date.ToString());
       }
       public string GetDetails()
       {
           StringBuilder builder = new StringBuilder();
           builder.AppendFormat(TransactionType,System.Environment.NewLine);
           if (Direction.HasValue)
           {
               builder.AppendFormat(Amount.ToString("N"), System.Environment.NewLine);
               builder.AppendFormat(Direction.ToString(), System.Environment.NewLine);
           }
           builder.Append(Date.ToString());
           return builder.ToString();
       }
    }
}
