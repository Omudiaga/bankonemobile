﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

using BankOneMobile.Core.Helpers.Enums;
using System.Runtime.Serialization;
using PANE.Framework.AuditTrail.Attributes;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [Serializable]
    [Trailable]
    public class MobileAccount : EntityWithEnableDisable, IMobileAccount
    {
       [DataMember]
       [TrailableIgnore]
      public virtual string CustomerID { set; get; }
       [DataMember]
       
      public virtual string LastName { set; get; }

       [DataMember]
      public virtual string OtherNames { set; get; }
      //public virtual string Address { set; get; }
      //public virtual string EmailAddress { set; get; }
       [DataMember]
        
      public virtual string MobilePhone { set; get; }
     // public virtual string Sex { set; get; }
       [DataMember]
       [TrailableIgnore]
      public virtual string PIN { set; get; }
       [DataMember]
       [TrailableIgnore]
      public virtual string PINOffset { set; get; }
       [DataMember]
       [TrailableIgnore]
      public virtual string ActivationCode { get; set; }
      //public virtual DateTime DOB { set; get; }
        [TrailableIgnore]
       public virtual int PinTries { get; set; }
        [TrailableName("The Bank Accounts")]
        public virtual IList<ILinkingBankAccount> BankAccounts { set; get; }
        [TrailableIgnore]
      public virtual IList<ILinkingBankAccount> SelectedBankAccounts { set; get; }
       [DataMember]

      public virtual bool IsAgentAccount { get; set; }
    
        [TrailableInnerClass(true, "BankAccount")]
        [TrailableName("The Default Account")]
      public virtual ILinkingBankAccount RecievingBankAccount { get; set; }
        
        private ILinkingBankAccount _AccountToDebit;
        public virtual ILinkingBankAccount BankAccountToDebit 
        { 
            get 
            {
                return _AccountToDebit == null ? RecievingBankAccount : _AccountToDebit;
            }
            set
            {
                _AccountToDebit = value;
            }
        }
        [DataMember]
        public virtual string ReceivingAccountNumber
        {
            get;
            set;
        }
   [TrailableName("Status")]
      public virtual MobileAccountStatus MobileAccountStatus { get; set; }
      public virtual AccountRegistrarType OpenedBy { get; set; }
       [DataMember]
       [TrailableIgnore]
      public virtual long   RegistrarID { get; set; }
        [TrailableIgnore]
       public virtual string  RegistrarCode { get; set; }
       public virtual string RegistrarName { get; set; }

       public virtual string ProductCode { get; set; }
       public virtual string ProductName { get; set; }

      public virtual DateTime DateCreated { get; set; }
      public virtual string PersonalPhoneNumber { get; set; }
      public virtual DateTime DateActivated { get; set; }
      [DataMember]
      [TrailableIgnore]
      public virtual string InstitutionCode { get; set; }
        [TrailableIgnore]
      public virtual string Status
      {
          get
          {
              return MobileAccountStatus.ToString();
          }
          set
          {
          }
      }
        [TrailableIgnore]
      public virtual string StandardPhoneNumber{get;set;}
     // public virtual IList<ILinkingCard> Cards { set; get; }
        [TrailableIgnore]
      public virtual Gender TheGender { get; set; }
        /// <summary>
        /// /used to set wat kind of update to be perform during approval
        /// </summary>
         [TrailableIgnore]
        public virtual UpdateType TheUpdateType { get; set; }

        public virtual AccountRestriction AccountRestriction { get; set; }
        [TrailableIgnore]
         public virtual bool ActivateViaJava { get; set; }
        [TrailableIgnore]
        public virtual bool ActivateViaUSSD { get; set; }
        [TrailableIgnore]
        public virtual MobileAccountType TheMobileAccountType
        {
            get
            {
                if (!IsAgentAccount)
                {
                    return MobileAccountType.Customer;
                }
                else
                {
                    return MobileAccountType.Agent;
                }
            }
        }
      public virtual  string ReferalPhone { get; set; }
      public virtual string StarterPackNo { get; set; }
      public virtual string PlaceOfBirth { get; set; }
      public virtual string idNo { get; set; }
      public virtual string NOKPhone { get; set; }
      public virtual string NOKName { get; set; }
      public virtual  string Address { get; set; }
      public virtual string NUBAN { get; set; }
      public virtual DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Has Sufficient info on Account 
        /// </summary>
      public virtual bool HasSufficientInfo { get; set; }
      public virtual AccountSource AccountSource { get; set; }
      public virtual bool? Verified { get; set; }
    }
}
