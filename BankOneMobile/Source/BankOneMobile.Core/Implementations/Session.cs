﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using System.Configuration;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    public class Session : EntityWithEnableDisable, ISession
    {
        public virtual string SessionID { get; set; }
        public virtual string MobilePhoneNumber { get; set; }
        public virtual ISession ChildSession { get; set; }
        public virtual SessionStatus Status { get; set; }
        public virtual string WorkFlowID { get; set; }
        public virtual IMobileAccount TheMobileAccount { get; set; }





        [DataMember]
        public virtual DateTime StartTime
        {
            get;
            set;
        }
        [DataMember]
        public virtual  DateTime StopTime
        {
            get;
            set;
        }
        [DataMember]
        public virtual long TimeInterval
        {
            get;
            set;
        }
        [DataMember]
        public virtual long SessionLength
        {
            get;
            set;
        }
        public virtual IList<  ITransaction> TheTransactions
        {
            get;
            set;
        }
        [DataMember]
        public virtual string Payer
        {
            get;
            set;


        }
        [DataMember]
        public virtual string PayerAccountNumber
        {
            get;
            set;
        }
        [DataMember]
        public virtual string PayerInstitutionCode
        {
            get;
            set;
        }
        [DataMember]
        public virtual  MobileAccountType MobileAccountType
        {
            get;
            set;
        }
        public virtual decimal Amount
        {
            get;
            set;
        }
        
        /// <summary>
        /// if save on bank one was successful
        /// </summary>
        public virtual bool SavedOnCoreBanking { get; set; }





        [DataMember]
        public virtual  string BillingDescription
        {
            get;
            set;
        }

        
    }
}
