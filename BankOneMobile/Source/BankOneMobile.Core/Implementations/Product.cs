﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class Product : EntityWithEnableDisable, IProduct
    {
      public virtual   string Code { set; get; }
      public virtual string InstitutionCode { set; get; }
      public virtual string Name { set; get; }
      public virtual Int32 Tenure { get; set; }



      public override string ToString()
      {
          return Name;
      }
     
     
    }
}
