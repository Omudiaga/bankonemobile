﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class BillsPaymentTransactionType : EntityWithEnableDisable, IBillsPaymentTransactionType
    {
     
      public virtual  string GoodsDescription { get; set; }
      

      public virtual  IMerchant TheMerchant
      {
          get;
          set;
      }

      public virtual IMobileAccount TheMobileAccount
      {
          get;
          set;
      }

      public virtual IPaymentItem ThePaymentItem
      {
          get;
          set;
      }


      public virtual string CustomerID { get; set; }



      public virtual string PaymentReference
      {
          get;
          set;
      }

      public virtual string TransactionReference
      {
         get;set;
      }



      public virtual  decimal Amount
      {
          get;
          set;
      }

      public virtual string ValuePurchased
      {
          get;
          set;
      }
     
      

      public virtual ITransaction TheTransaction
      {
          get;
          set;
      }


      public virtual string CoreBankingSTAN
      {
         get;set;
      }

      public virtual  string ISWSTAN
      {
          get;set;
      }
      public virtual  string MerchantName { get; set; }
      public virtual string MerchantCode { get; set; }
      public virtual string PaymentItemName { get; set; }
      public virtual string PaymentItemCode { get; set; }


      public virtual LinkingBankAccount TheBankAccount { get; set; }


      #region ITransactionType Members


      public virtual  IFeeTransactionType TheFee
      {
          get;
          set;
      }

      #endregion
    }
}
