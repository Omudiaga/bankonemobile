﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class SystemConfig :  EntityWithEnableDisable,  ISystemConfig
    {


        public virtual long SessionLength { get; set; }
        public virtual decimal AmountPerSession { get; set; }
        public virtual int MaximumPinTries { get; set; }
       public virtual  string PEK { get; set; }
       public virtual string PVK { get; set; }
       public virtual string TMK { get; set; }
        




       
        
    }
}
