﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [Serializable]
    public class UssdBillingConfiguration : Entity, IUssdBillingConfiguration 
    {
        [DataMember]
        public virtual int Period { get; set; }
        [DataMember]
        public virtual decimal Amount { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
    }
}
