﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class CustomerEligibleLoan : EntityWithEnableDisable, ICustomerEligibleLoan
    {
      public virtual   string CustomerID { set; get; }
      public virtual string InstitutionCode { set; get; }
      public virtual string LoanProductID { set; get; }
      public virtual string LoanProductName { set; get; }



      public override string ToString()
      {
          return LoanProductName;
      }
     
     
    }
}
