﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class TargetSavingsRegistrationTransactionType : EntityWithEnableDisable, ITargetSavingsRegistrationTransactionType
    {

      public virtual string AccountNumber { get; set; }
      public virtual string Frequency { get; set; }
      public virtual string Amount { get; set; }
      public virtual decimal TotalCommitmentAmount { get; set; }
      public virtual string Tenure { get; set; }
      public virtual Int32 TargetSavingsProductTenure { get; set; }
      public virtual string PhoneNumber { get; set; }
      public virtual string StandingOrder { get; set; }
      public virtual bool IsReapplying { get; set; }
      public virtual string TargetSavingsAccountNumberForReactivation { get; set; }
      public virtual Product SelectedProduct { get; set; }
      public virtual DateTime Date { get; set; }
      public virtual ITransaction TheTransaction { get; set; }
      public virtual IMobileAccount TheMobileAccount { get; set; }
      public virtual IFeeTransactionType TheFee
      {
          get;
          set;
      }
    }
}
