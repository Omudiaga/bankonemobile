﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class LoanRequestTransactionType : EntityWithEnableDisable, ILoanRequestTransactionType
    {
      public virtual string LoanProductID { get; set; }
      public virtual string LoanAmount { get; set; }
      public virtual string PhoneNumber { get; set; }
      public virtual string AccountNumber { get; set; }
      public virtual string BetaFriendPerception{get;set;}
      public virtual string Tenure { get; set; }
      public virtual string InstitutionCode { get; set; }
      public virtual ITransaction TheTransaction { get; set; }
      public virtual IMobileAccount TheMobileAccount { get; set; }
      public virtual DateTime Date { get; set; }
      public virtual IFeeTransactionType TheFee
      {
          get;
          set;
      }
      
    }
}
