﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class RechargeTransactionType : EntityWithEnableDisable, IRechargeTransactionType
    {

        public virtual string MerchantName { get; set; }
        public virtual string MerchantCode { get; set; }
        public  virtual IMerchant TheMerchant { get; set; }
       public virtual RechargeType RechargeType { get; set; }
       public virtual ITransaction TheTransaction { get; set; }
       public virtual IMobileAccount TheMobileAccount { get; set; }
       public virtual string Beneficiary{get;set;}
       public virtual string RechargeCode { get; set; }
     public virtual   string PaymentItemCode { get; set; }
       public virtual decimal Amount
       {
           get;
           set;
       }
       public virtual string TransactionReference { get; set; }
       public virtual string PaymentReference { get; set; }
       public virtual string CoreBankingSTAN
       {
           get;
           set;
       }

       public virtual  string ISWSTAN
       {
           get;
           set;
       }
       public virtual IFeeTransactionType TheFee
       {
           get;
           set;
       }

       public virtual LinkingBankAccount TheBankAccount { get; set; }

       
    }
}
