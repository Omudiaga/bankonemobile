﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class RegistrationTransactionType : EntityWithEnableDisable, IRegistrationTransactionType
    {

      public virtual string LastName { get; set; }
      public virtual string FirstName { get; set; }
      public virtual string PhoneNumber { get; set; }
      public virtual string ActivationCode { get; set; }
      public virtual DateTime Date { get; set; }
      public virtual ITransaction TheTransaction { get; set; }
      public virtual IMobileAccount TheMobileAccount { get; set; }
      public virtual Gender TheGender { get; set; }
      public virtual string Gender{get;set;}
      public virtual string ProductCode { get; set; }
      public virtual IProduct TheProduct{get;set;}

      public virtual string PlaceOfBirth { get; set; }
      public virtual DateTime DateOfBirth { get; set; }
      public virtual string Address { get; set; }
      public virtual string StarterPackNumber { get; set; }
      public virtual string IDNumber { get; set; }
      public virtual string NOKName { get; set; }
      public virtual string NOKPhone { get; set; }
      public virtual string ReferralPhoneNumber { get; set; }
      public virtual string ReferralName { get; set; }
      public virtual bool IsEnhanced { get; set; }
      public virtual bool HasSufficientInfo { get; set; }
      public virtual AccountSource AccountSource { get; set; }
      public virtual string OtherAccountInfo { get; set; }
      public virtual byte[] Passport { get; set; }
      public virtual string Country { get; set; }
      public virtual string State { get; set; }
      public virtual string District { get; set; }
      public virtual string Territory { get; set; }
      public virtual DateTime IDIssuedDate { get; set; }
      public virtual DateTime IDExpiryDate { get; set; }
      public virtual string IDTypeCode { get; set; }
      public virtual IFeeTransactionType TheFee
      {
          get;
          set;
      }




      
    }
}
