﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class MerchantCategory : EntityWithEnableDisable, IMerchantCategory
    {
      public virtual   string Code { set; get; }
      
      public virtual string Name { set; get; }

      public override string ToString()
      {
          return this.Name;
      }
      
     
    }
}
