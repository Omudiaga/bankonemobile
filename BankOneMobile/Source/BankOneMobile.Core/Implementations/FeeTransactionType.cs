﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class FeeTransactionType : EntityWithEnableDisable, IFeeTransactionType
    {
        public virtual string TransactionTypeName { set; get; }
       public virtual bool FlatOrPercent { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual IFeeTransactionType TheFee { get; set; }

      
    }
}
