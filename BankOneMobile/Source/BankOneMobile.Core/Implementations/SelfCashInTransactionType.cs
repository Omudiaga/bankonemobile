﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class SelfCashInTransactionType : EntityWithEnableDisable, ISelfCashInTransactionType
    {
        public virtual string AgentCode { get; set; }
        public virtual string Token { get; set; }
        public virtual ITransaction TheTransaction { get; set; }
        public virtual decimal Amount{get;set;}
        public virtual string DepositorPhoneNumber { get; set; }
        public virtual string CustomerPhoneNumber { get; set; }
        public virtual bool IsCash { get; set; }
        public virtual string BeneficiaryPhoneNumber { get; set; }
        public virtual bool IsConsumed { get; set; }
        public virtual IFundsTransferCashOutTransactionType TheCashOut { get; set; }
        public virtual string DepositorAccountNumber { get; set; }







        #region ITransactionType Members


        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

        #endregion
    }
}
