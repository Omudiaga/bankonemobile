﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class ConfigurationTransactionType : EntityWithEnableDisable, IConfigurationTransactionType
    {

        public virtual ITransaction TheTransaction { get; set; }
        public virtual IMobileAccount TheMobileAccount { get; set; }
        public virtual string Type { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

        
    }
}
