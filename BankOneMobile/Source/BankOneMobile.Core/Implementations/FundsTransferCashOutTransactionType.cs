﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class FundsTransferCashOutTransactionType : EntityWithEnableDisable, IFundsTransferCashOutTransactionType
    {
        public virtual string Token { get; set; }
        public virtual string AgentCode { get; set; }
       public virtual ITransaction TheTransaction { get; set; }
       public virtual decimal Amount { get; set; }
       public virtual bool IsCash { get; set; }
       public virtual string DepositorPhoneNumber { get; set; }
       public virtual string BeneficiaryPhoneNumber { get; set; }  
       public virtual IMobileAccount TheMobileAccount { get; set; }
       public virtual bool IsConsumed { get; set; }
       public virtual  IList<IFundsTransferCashInTransactionType> TheCashIns { get; set; }
       public virtual decimal OriginalAmount { get; set; }
       public virtual string ActivationCode { get; set; }
       public virtual IFeeTransactionType TheFee
       {
           get;
           set;
       }

      
    }
}
