﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    /// <summary>
    /// Defines the Default behaviour of an Entity with enable/disable feature
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class EntityWithEnableDisable : Entity, BankOneMobile.Core.Contracts.IEnableDisable
    {
        #region IEnableDisable Members
        [DataMember]
        public virtual bool IsActive
        {
            get;
            set;
        }
        [DataMember]
        public virtual string DisplayMessage { get; set; }
      
        #endregion
    }
}
