﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    /// <summary>
    /// Defines the Default Behaviour of an Entity
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class Entity : BankOneMobile.Core.Contracts.IEntity
    {
        #region IEntity Members
        [DataMember]
        public virtual long ID
        {
            get;
            set; //protected
        }

     
        #endregion

       

        
    }
}
