﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Attributes;

namespace BankOneMobile.Core.Implementations
{
    public class Merchant : EntityWithEnableDisable, IMerchant
    {
      public virtual   string Code { set; get; }
      
      public virtual string Name { set; get; }
        [CustomerIDAttribute]
      public virtual string CustomerIDField { set; get; }

      public virtual IList<IPaymentItem> ThePaymentItems { get; set; }
      public override string ToString()
      {
          return this.Name;
      }
     
    }
}
