﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Utility;

namespace BankOneMobile.Core.Implementations
{
    public class LinkingCard : Entity, ILinkingCard
    {
        private string _newcardPAN;
        public virtual string InstitutionCode { set; get; }
        public virtual IMobileAccount TheMobileAccount { set; get; }
        public virtual string CardPAN { set; get; }
        public virtual DateTime ExpiryDate { set; get; }
        public virtual string DecryptedCardPAN
        {
            get
            {
                if (!string.IsNullOrEmpty(this._newcardPAN))
                {
                    return this._newcardPAN;

                }
                 else if (!string.IsNullOrEmpty(this.CardPAN))
                {
                    return Crypter.Decrypt(this.InstitutionCode, this.CardPAN);
                }
              
                else {

                    return string.Empty;
                }
            }

            set
            {

                _newcardPAN = value;
            }


        }
        public virtual void EncryptPANS()
        {
            if (!string.IsNullOrEmpty(this.InstitutionCode) && !string.IsNullOrEmpty(_newcardPAN))
            {

                this.CardPAN = Crypter.Encrypt(this.InstitutionCode, _newcardPAN);

            }


          


           
        }
    }
}
