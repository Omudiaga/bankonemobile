﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class CashOutTransactionType : EntityWithEnableDisable, ICashOutTransactionType
    {
        public  virtual string AgentCode { get; set; }
       public  virtual  string Token { get; set; }
        public  virtual string WithdrawalLocation { get; set; }
        public virtual ITransaction TheTransaction { get; set; }
       public virtual  LinkingBankAccount TheBankAccount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

        public virtual bool IsPhoneLess { get; set; }
        public virtual string enteredParameter { get; set; }
        public virtual string CustomerAccount { get; set; }
        public virtual string CustomerPhoneNumber { get; set; }

    }
}
