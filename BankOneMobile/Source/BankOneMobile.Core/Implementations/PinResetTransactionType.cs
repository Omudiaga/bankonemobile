﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class PinResetTransactionType : EntityWithEnableDisable, IPinResetTransactionType
    {

        public virtual ITransaction TheTransaction { get; set; }
        public virtual IMobileAccount TheMobileAccount { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

       
    }
}
