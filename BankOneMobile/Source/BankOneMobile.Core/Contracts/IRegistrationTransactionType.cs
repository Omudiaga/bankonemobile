﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IRegistrationTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string LastName { get; set; }
        string FirstName{get;set;}
        string PhoneNumber{get;set;}
        string Gender { get; set; }
        string ProductCode { get; set; }
        string ActivationCode{get;set;}
        IMobileAccount TheMobileAccount{get;set;}
        IProduct TheProduct { get; set; }
        Gender TheGender { get; set; }
        DateTime Date { get; set; }
        string PlaceOfBirth { get; set; }
        DateTime DateOfBirth { get; set; }
        string Address { get; set; }
        string StarterPackNumber { get; set; }
        string IDNumber { get; set; }
        string NOKName { get; set; }
        string NOKPhone { get; set; }
        string ReferralPhoneNumber { get; set; }
        string ReferralName { get; set; }
        bool IsEnhanced { get; set; }
        bool HasSufficientInfo { get; set; }
        AccountSource AccountSource { get; set; }
        string OtherAccountInfo { get; set; }
        byte[] Passport { get; set; }
         string State { get; set; }
         string District { get; set; }
         string Territory { get; set; }
         string Country { get; set; }
         DateTime IDIssuedDate { get; set; }
         DateTime IDExpiryDate { get; set; }
         string IDTypeCode { get; set; }
        
    }
}
