﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface ICustomerInfo
    {
        string CustomerID { set; get; }
        string LastName { set; get; }
        string OtherNames { set; get; }
        string Address { set; get; }
        string EmailAddress { set; get; }
        string Sex { set; get; }
        DateTime DOB { set; get; }
        
        ICustomerInfo CreateCustomerInfo();
        
    }
}
