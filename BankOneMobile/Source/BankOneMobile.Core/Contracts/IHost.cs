﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IHost : IEntity , IEnableDisable
    {
        string ClassType { set; get; }
        string Name { set; get; }
        string DllPath { set; get; }
    }
}
