﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
   public interface ICustomerCreationLog
    {
        string CustomerName{set;get;}
        string PhoneNumber{set;get;}
        string Institution {set;get;}
        string State{set;get;}
        string AccountNumber { set; get; }
       
    }
}
