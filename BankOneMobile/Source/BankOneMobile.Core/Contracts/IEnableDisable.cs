﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    /// <summary>
    /// Defines Basic contracts for an entity with Enable/Disable features 
    /// </summary>
    public interface IEnableDisable
    {
        bool IsActive
        {
            get;
            set;
        }
        string DisplayMessage
        {
            get;
            set;
        }
        
    }
}
