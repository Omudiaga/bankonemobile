﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IMerchant : IEntity , IEnableDisable
    {
        string Code { set; get; }
        string Name { get; set; }
        IList<IPaymentItem> ThePaymentItems { get; set; }
        
        

    }
}
