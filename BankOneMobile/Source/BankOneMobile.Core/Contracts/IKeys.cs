﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IKey :IEntity
    {
        string ValueUnderLMK { get; set; }
        string ValueUnderParent { get; set; }
        string Type { get; set; }

        

    }
}
