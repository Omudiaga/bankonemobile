﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Contracts
{
    
    public  interface IBalanceInquiryTransactionType :ITransactionType, IEntity , IEnableDisable
    {
        IMobileAccount TheMobileAccount { get; set; }
        bool IsForThirdParty { get; set; }
        bool IsForSelfService { get; set; }
    }
}
