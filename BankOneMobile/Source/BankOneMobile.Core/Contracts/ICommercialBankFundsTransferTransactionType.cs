﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface ICommercialBankFundsTransferTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string BankName { get; set; }
        string BankCode { get; set; }
        string AccountNumber { get; set; }
        string AgentCode { get; set; }
        string Token { get; set; }
        decimal Amount { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
        AccountMode TheAccountType { get; set; }
         string TransactionReference { get; set; }
         string PaymentReference { get; set; }
         string BeneficiaryLastName { get; set; }
         string BeneficiaryOtherNames { get; set; }
         string BeneficiaryPhoneNumber { get; set; }
         string CoreBankingSTAN {get; set;}
         string ISWSTAN { get; set; }
          LinkingBankAccount TheBankAccount { get; set; }
          DateTime TransactionDate { get; set; }
          string TransactionStatus { get; set; }
          string TransactionDetails { get; set; }
          decimal TransactionAmount { get; set; }
          string Institution  {get;set; }
          string MobilePhone { get; set; }
          long TransactionID { get; set; }
          
    }
}
