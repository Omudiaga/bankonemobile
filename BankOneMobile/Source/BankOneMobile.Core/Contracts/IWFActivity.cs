﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    /// <summary>
    /// Defines the basic definition of a workFlow activity
    /// </summary>
    public interface IWFActivity : IEntity
    {
        string XamlDefinition
        {
            get;
            set;
        }

        Guid UniqueId
        {
            get;
            set;
        }

        System.Activities.Activity TheActivity
        {
            get;
        }
    }
}
