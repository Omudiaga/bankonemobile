﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    
    public interface ILinkingBankAccount : IEntity
    {
      IMobileAccount TheMobileAccount { set; get; }
      string BankAccount { set; get; }
      decimal Balance { get; set; }
      IHost TheHost { set; get; }
      string CustomerID { set; get; }
      string Name { get; set; }
        /// <summary>
        /// Status of the bank account for mobile transactions
        /// </summary>
      bool Activated { get; set; }
      byte[] Signature { get; set; }
       string InstitutionCode { get; set; }
       string InstitutionName { get; set; }
       string ProductName { get; set; }
       string ProductCode { get; set; }
       Gender TheGender { get; set; }
       BankAccountStatus BankStatus { get; set; }
       IProduct TheProduct { get; set; }
       string CoreBankingNames { get; set; }
       byte[] Passport { get; set; }
       string PlaceOfBirth { get; set; }
       DateTime DateOfBirth { get; set; }
       string Address { get; set; }
       string StarterPackNumber { get; set; }
       string IDNumber { get; set; }
       string NOKName { get; set; }
       string NOKPhone { get; set; }
       string ReferralPhoneNumber { get; set; }
       string NUBAN { get; set; }
       string StandardAccount { set; get; }

       

        
    }
}
