﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.Core.Contracts
{
    public  interface IToken : IEntity , IEnableDisable
    {
        string TokenValue { set; get; }
        IInstitution TheInstitution { get; set; }
        
        DateTime DateGenerated { get; set; }
        DateTime ExpiryDate { get; }

        bool IsExpired { get; }
       
        bool IsConsumed { get; set; }
        DateTime DateConsumed { get; set; }
        string PhoneNumber { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
    
         
        

    }
}
