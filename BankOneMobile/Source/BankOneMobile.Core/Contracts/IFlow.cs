﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IFlow : IEntity , IEnableDisable, IRentable, IWFActivity
    {
        string Name { set; get; }
        string Description { get; set; }
        bool IsMainFlow { get; set; }
        bool IsAgent { get; set; }
        bool IsAccountActivation { get; set; }
        bool IsFundsTransfer { get; set; }
    }
}
