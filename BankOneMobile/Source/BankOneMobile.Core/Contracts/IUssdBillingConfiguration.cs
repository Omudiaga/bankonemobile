﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface IUssdBillingConfiguration : IEntity
    {
        /// <summary>
        /// The interval specified to the billing timer in seconds
        /// </summary>
        int Period { get; set; }
        /// <summary>
        /// The amount to debit from the customer
        /// </summary>
        decimal Amount { get; set; }
        /// <summary>
        /// Name of the configuration
        /// </summary>
        string Name { get; set; }
    }
}
