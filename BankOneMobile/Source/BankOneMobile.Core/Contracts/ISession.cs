﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public  interface ISession : IEntity 
    {
        string SessionID { get; set; }
        string MobilePhoneNumber{get;set;}
        DateTime StartTime { get; set; }
        DateTime StopTime { get; set; }
        long TimeInterval { get; set; }
         IList<  ITransaction> TheTransactions {get;set;}
        string Payer { get; set; }
        string PayerAccountNumber { get; set; }
        SessionStatus Status { get; set; }
        decimal Amount { get; set; }
        string BillingDescription { get; set; }
        bool SavedOnCoreBanking { get; set; }
        string WorkFlowID { get; set; }
        long SessionLength { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
        
         string PayerInstitutionCode{get;set;}
        
         MobileAccountType MobileAccountType { get; set; } 
        
        ISession ChildSession { get; set; }
    }
}
