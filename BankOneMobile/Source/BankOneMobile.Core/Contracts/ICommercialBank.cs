﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface ICommercialBank : IEntity , IEnableDisable
    {
        string Code { set; get; }
        string Name { get; set; }
        string ShortName { get; set; }
    }
}
