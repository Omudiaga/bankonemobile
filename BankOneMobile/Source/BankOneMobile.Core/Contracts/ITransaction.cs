﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public  interface ITransaction : IEntity , IEnableDisable
    {
        string To { get; set; }
        /// <summary>
        /// The phone Number of the Mobile Account originating the transaction
        /// </summary>
         bool NotValidatePin { get; set; }
        string From { get; set; }
        /// <summary>
        /// The Phone number of the mobile account originating   the transaction
        /// </summary>
        string FromPhoneNumber { get; set; }
        decimal Amount { get; set; }
        ISession Session { get; set; }
        DateTime Date { get; set; }
        TransactionStatus Status { get; set; }
        long TransactionTypeID { get; set; }
        TransactionTypeName TransactionTypeName { get; set; }
        byte[] EncryptedPIN { get; set; }
        string STAN { get; set; }
        /// <summary>
        /// The time the transaction occured on the switch
        /// </summary>
        DateTime SwitchTransactionTime { get; set; }
        string FromInstitutionCode { get; set; }
        string ToInstitutionCode { get; set; }
        string AgentCode { get; set; }
        string Narration { get; set; }
        IFeeTransactionType TheFee { get; set; }
        ServiceCodes ServiceCodes { get; set; }
        MerchantType TheMerchantType { get; set; }
        Service TheService { get; set; }
        string InstitutionCode { get; set; }
        string TheTransactionType { get; }
        string StatusDetails { get; set; }
        string StringAmount { get; }
        string ToPhoneNumber { get; set; }
        //ILinkingBankAccount TheBankAccount { get; set; }
    }


    public interface IMajorTransaction : IEntity
    {
        string TransactionReference { set; get; }
        string PaymentReference { set; get; }
        TransactionStatus Status { set; get; }
        string StatusDetails { set; get; }
        DateTime Date { set; get; }
        string CoreBankingSTAN { set; get; }
        string ISWSTAN { set; get; }
        string InstitutionCode { set; get; }
        long TellerID { set; get; }
        string TellerName { set; get; }
        decimal Amount { set; get; }
        decimal Fee { set; get; }
        string DisplayAmount { get; }
        string DisplayFee { get; }
    }
}
