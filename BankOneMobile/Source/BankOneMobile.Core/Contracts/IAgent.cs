﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public  interface IAgent : IEntity , IEnableDisable
    {
        string Code { set; get; }
        string LastName { get; set; }
        string OtherNames { get; set; }
        string AddressLocation { get; set; }
        string PhoneNumber { get; set; }
        IMobileAccount MobileAccount { get; set; }
        string InstitutionCode { get; set; }
         ILinkingBankAccount TheAgentAccount { get; set; }
         bool Linked { get; set; }
         string TheAgentAccountNumber { get; set; }
         bool IsMobileTeller { get; set; }
         string InstitutionName { get; set; }
         string ProductName { get; set; }
         string ProductCode { get; set; }
         Gender TheGender { get; set; }
         bool ActivateViaJava { get; set; }
         string PersonalPhoneNumber { get; set; }
         decimal PostingLimit { get; set; }
         long BranchID { get; set; }

    }
}
