﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ICashInTransactionType :ITransactionType, IEntity, IEnableDisable
    {
        string AgentCode { get; set; }
        string Token { get; set; }
        decimal Amount { get; set; }
        LinkingBankAccount TheBankAccount { get; set; }
        bool IsPhoneNumber { get; set; }
        string EnteredParameter { get; set; }
        
    }
}
