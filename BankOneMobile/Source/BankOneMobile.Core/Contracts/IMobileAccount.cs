﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Contracts
{
    
   public interface IMobileAccount : IEntity , IEnableDisable
    {
       
       string CustomerID { set; get; }
       string ProductCode { get; set; }
       string LastName { set; get; }
       string OtherNames { set; get; }
       //string Address { set; get; }
       //string EmailAddress { set; get; }
       string MobilePhone { set; get; }
       //string Sex { set; get; }
       string PIN { set; get; }
       string RegistrarName { get; set; }
       string PINOffset { get; set; }
       int PinTries { get; set; }
       string ActivationCode{get;set;}
        string InstitutionCode { get; set; }
        string ProductName { get; set; }
        string RegistrarCode { get; set; }
        string PersonalPhoneNumber { get; set; }
       //DateTime DOB { set; get; }
       IList<ILinkingBankAccount> BankAccounts { set; get; }
       IList<ILinkingBankAccount> SelectedBankAccounts { set; get; }
       ILinkingBankAccount RecievingBankAccount { get; set; }
       ILinkingBankAccount BankAccountToDebit { get; set; }
       bool IsAgentAccount { get; set; }
       /// <summary>
       /// Date the account was created
       /// </summary>
       
       DateTime DateCreated { get; set; }
       /// <summary>
       /// Date the account was first activated
       /// </summary>
       
       DateTime DateActivated { get; set; }
        
       MobileAccountStatus MobileAccountStatus { get; set; }
       /// <summary>
       /// Opened by Agent orBranch or whatever means
       /// </summary>
       AccountRegistrarType OpenedBy { get; set; }
       
       string Status
       {
           get;
           set;
       }
       /// <summary>
       /// the ID of the meand if Branch,Branch ID of Agent,Agent ID
       /// </summary>
       /// 
        
       long  RegistrarID { get; set; }
       
       string StandardPhoneNumber { get; set; }//phone number formated to international standard
       Gender TheGender { get; set; }
       bool ActivateViaJava { get; set; }
       bool ActivateViaUSSD { get; set; }
       ///
       /// Restriction on Transctions allows for the account
       AccountRestriction AccountRestriction { get; set; }
       string ReferalPhone { get; set; }
       string StarterPackNo { get; set; }
       string PlaceOfBirth { get; set; }
       string idNo { get; set; }
       string NOKPhone { get; set; }
       string NOKName { get; set; }
       string Address { get; set; }
       DateTime DateOfBirth { get; set; }
       string NUBAN { get; set; }
     //  IList<ILinkingCard> Cards { set; get; }

       bool? Verified { get; set; }
    }
}
