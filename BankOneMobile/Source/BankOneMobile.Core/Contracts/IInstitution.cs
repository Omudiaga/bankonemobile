﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface IInstitution
    {
        string Code {set;get;}
        string Name { set; get; }
        string ShortName { get; set; }
        bool AllowInterMFBTrans { get; set; }
        string TokenLifeSpan { get; set; }
        IFlow TheAgentStartFlow { get; set; }
        IFlow TheCustomerStartFlow { get; set; }
        BankOneMobile.Core.Helpers.Enums.AccountNumberType TheAccountNumberType { get; set; }
    }

}
