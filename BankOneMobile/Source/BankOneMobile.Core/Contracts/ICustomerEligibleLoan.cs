﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface ICustomerEligibleLoan : IEntity , IEnableDisable
    {
        string CustomerID { set; get; }
        string LoanProductID { get; set; }
        string LoanProductName { get; set; }
        string InstitutionCode { get; set; }
        

    }
}
