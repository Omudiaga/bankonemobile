﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ISelfCashInTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string AgentCode { get; set; }
        string Token { get; set; }
        decimal Amount { get; set; }
        string DepositorPhoneNumber { get; set; }
        string DepositorAccountNumber { get; set; }
        bool IsCash { get; set; }
        string BeneficiaryPhoneNumber { get; set; }
        bool IsConsumed { get; set; }
        IFundsTransferCashOutTransactionType TheCashOut { get; set; }
        
    }
}
