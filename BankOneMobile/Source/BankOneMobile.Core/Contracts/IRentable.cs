﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    /// <summary>
    /// Defines the basic properties for a rentable Entity
    /// </summary>
    public interface IRentable
    {
        /// <summary>
        /// TheTenant Owner
        /// </summary>
        IInstitution TheTenant
        {
            get;
            set;
        }
    }
}
