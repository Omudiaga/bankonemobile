﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
   
    public  interface ITransactionType : IEntity , IEnableDisable
    {
        ITransaction TheTransaction { get; set; }
        IFeeTransactionType TheFee { get; set; }
        
        
    }
}
