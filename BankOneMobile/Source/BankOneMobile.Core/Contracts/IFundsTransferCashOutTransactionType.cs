﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BankOneMobile.Core.Contracts
{
    public interface IFundsTransferCashOutTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string AgentCode { get; set; }
        string ActivationCode { get; set; }
        string Token { get; set; }
        decimal Amount { get; set; }
        bool IsCash { get; set; }
        string  DepositorPhoneNumber { get; set; }
        string BeneficiaryPhoneNumber { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
        bool IsConsumed { get; set; }
        IList<IFundsTransferCashInTransactionType> TheCashIns { get; set; }
        decimal OriginalAmount { get; set; }
        
    }
}
