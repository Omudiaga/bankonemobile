using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace BankOneMobile.Core.Configuration
{
    public class ConfigurationManager
    {
        private static NameValueCollection USSDSessionConfig
        {
            get
            {
                return System.Configuration.ConfigurationManager.GetSection("USSDSessionConfig") as NameValueCollection;
            }
        }

        public static long USSDSessionInterval
        {
            get
            {
                return Convert.ToInt64(USSDSessionConfig["SessionInterval"]);
            }
        }


        public static decimal  AmountPerSession
        {
            get
            {
                return Convert.ToDecimal(USSDSessionConfig["AmountPerSession"]);
            }
        }
       

    }
}
