﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum BankAccountStatus
    {
        
        Active=0,
        InActive =1,      
       AwaitingApproval=2
    }
}
