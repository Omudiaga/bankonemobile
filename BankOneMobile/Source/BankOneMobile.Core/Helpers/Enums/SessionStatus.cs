﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum SessionStatus
    {
        Started=1,
        EndedByNetwork=2,
        EndedByTPA=3,
        EndedByUser=4,
        TimedOut=5,
        EndedNormally = 6

    }
}
