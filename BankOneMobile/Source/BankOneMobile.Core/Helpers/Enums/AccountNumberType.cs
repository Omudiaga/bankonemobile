﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum AccountNumberType
    {
        Standard = 1,
        NUBAN = 2
    }
}
