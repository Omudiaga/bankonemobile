using System;
using System.Collections.Generic;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum Service
    {        
        AgentCashIn=1001,
        AgentCashOut=1002,
        MobileTellerCashIn=0926,
        MobileTellerCashOut=0927,
        NullService=0000,
        CustomerRecharge=1006,
        CustomerRechargeCompletion=1019,
        CustomerBillsPayment=1005,
        CustomerBillsPaymentCompletion=1021,
        OffNetTransfer=1007,
        OffNetTransferCompletion=1020,
        UssdPinlessBilling = 4321,
        MobileTellerCustomerBalanceEnquiryMS=0928,
        MobileLocalFundsTransfer=1022,
        SelfServiceBalanceEnquiry=1023
    }
}
