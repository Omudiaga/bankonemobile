﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum CashOutLocation
    {
        ATM =1,
        BankOneBranch=2,
        CommercialBankBranch
    }
}
