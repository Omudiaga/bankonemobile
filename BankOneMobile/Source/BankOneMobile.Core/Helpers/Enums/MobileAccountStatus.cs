﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum MobileAccountStatus
    {
        New=1,
        Active=2,
        InActive =3,
        PendingFundsTransfer=4,
        HotListed=5,     
        AwaitingPinReset=6,
        AwaitingApproval
    }
}
