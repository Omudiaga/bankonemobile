﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum ServiceCodes
    {
        NormalPayment =50,
        FromServiceAccountPayment=02,
        ToServiceAccountPayment=01
    }
}
