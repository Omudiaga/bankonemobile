﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum TransactionTypeName 
    {
        BalanceEnquiry =1,
        MiniStatement=2,
        Recharge=3,
        BillsPayment=4,
        CashIn=5,
        CashOut=6,
        PINChange=7,
        PINReset=8,
        Registration=9,
        FundsTransferCommercialBank=10,
        FundsTransferCashIn=11,
        FundsTransferCashOut=12,
        FundsTransferLocal=13,
        AccountActivation=14,
        LocalFundsTransfer=15,
        SetDefaultAccount





    }
}
