﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PANE.Framework.Functions.DTO;

namespace PANE.Framework.AuditTrail.DTO
{
    public interface IAuditable
    {
        IUser AuditableUser { get; set; }
    }
}
