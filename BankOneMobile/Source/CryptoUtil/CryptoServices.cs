﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Utilities.Encoders;

namespace CryptoUtil
{
    public class CryptoServices
    {
        private TripleDESCryptoServiceProvider TDESAlgo;
        private string keystring;
        private IBlockCipher engineBlockCipher;
        private BufferedBlockCipher cipher;

        public CryptoServices()
        {
            keystring = "B738C0DB478907CAE98CF476";
            init();
            engineBlockCipher = new DesEdeEngine();
            cipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(engineBlockCipher), new Org.BouncyCastle.Crypto.Paddings.Pkcs7Padding());
        }

        public CryptoServices(bool usePadding)
        {
            keystring = "B738C0DB478907CAE98CF476";
            init();
            engineBlockCipher = new DesEdeEngine();
            cipher = usePadding ? new PaddedBufferedBlockCipher(new CbcBlockCipher(engineBlockCipher), new Org.BouncyCastle.Crypto.Paddings.Pkcs7Padding())
                :  new PaddedBufferedBlockCipher(new CbcBlockCipher(engineBlockCipher), new Org.BouncyCastle.Crypto.Paddings.ZeroBytePadding());
        }

        public CryptoServices(string key)
        {
            if (!String.IsNullOrEmpty(key))
            {
                if (key.Length == 24)
                {
                    keystring = key;
                }
                else
                {
                    throw new ArgumentException("key is invalid. Must be hexadecimal and 24 characters long");
                }
            }
            else
            {
                throw new ArgumentException("key is invalid. Must be hexadecimal and 24 characters long");
            }
        }

        private void init()
        {
            TDESAlgo = new TripleDESCryptoServiceProvider();
            TDESAlgo.Padding = PaddingMode.PKCS7;
            TDESAlgo.Mode = CipherMode.ECB;
            byte[] key = Encoding.UTF8.GetBytes(keystring);//Convert.FromBase64String("1111111111111111111111111111111");//new byte[24];
            TDESAlgo.Key = key;
        }

        public string EncryptData(string plaintext)
        {
            string output = null;
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, TDESAlgo.CreateEncryptor(), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cs);
            writer.Write(plaintext);
            writer.Flush();
            writer.Close();
            cs.Close();
            byte[] encMsg = ms.ToArray();
            ms.Close();

            output = Convert.ToBase64String(encMsg);

            //ICryptoTransform encrytor = TDESAlgo.CreateEncryptor();
            //byte[] input = Encoding.UTF8.GetBytes(plaintext);
            //byte[] encmsg = encrytor.TransformFinalBlock(input, 0, input.Length);
            //output = Convert.ToBase64String(encmsg);
            return output;
        }

        public string DecryptData(string ciphertext)
        {
            string output = null;
            byte[] enc = Convert.FromBase64String(ciphertext);
            //MemoryStream ms = new MemoryStream();
            //CryptoStream cs = new CryptoStream(ms, TDESAlgo.CreateDecryptor(), CryptoStreamMode.Write);
            //StreamWriter writer = new StreamWriter(cs);
            //writer.Write(enc);
            //writer.Flush();
            //writer.Close();
            //cs.Close();
            //byte[] plainBytes = ms.ToArray();
            //ms.Close();
            //output = Encoding.UTF8.GetString(plainBytes);
            ICryptoTransform decryptor = TDESAlgo.CreateDecryptor();
            byte[] input = Convert.FromBase64String(ciphertext);
            byte[] plainBytes = decryptor.TransformFinalBlock(input, 0, input.Length);
            output = Encoding.UTF8.GetString(plainBytes);
            return output;
        }

        public string BCDecrypt(string ciphertxt)
        {
            string output = null;
            byte[] key = Encoding.UTF8.GetBytes(keystring);
            if (ciphertxt != null)
            {
                cipher.Init(false, new KeyParameter(key));
                byte[] inputBytes = Hex.Decode(ciphertxt);
                byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
                int len = cipher.ProcessBytes(inputBytes, 0, inputBytes.Length, outputBytes, 0);
                try
                {
                    cipher.DoFinal(outputBytes, len);
                }
                catch (Exception ex)
                {
                }
                output = Encoding.UTF8.GetString(outputBytes);
            }
            if (output != null)
            {
                output = output.TrimEnd('\0');
            }
            return output;
        }

        public string BCEncrypt(string plaintxt)
        {
            StringBuilder output = new StringBuilder();
            byte[] key = Encoding.UTF8.GetBytes(keystring);
            if (plaintxt != null)
            {
                cipher.Init(true, new KeyParameter(key));
                byte[] inputBytes = Encoding.UTF8.GetBytes(plaintxt);
                byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
                int len = cipher.ProcessBytes(inputBytes, 0, inputBytes.Length, outputBytes, 0);
                try
                {
                    cipher.DoFinal(outputBytes, len);
                }
                catch (Exception ex)
                {
                }
                //foreach (byte b in outputBytes)
                //{
                //    output.AppendFormat("{0:x2}", b);
                //}
                output.Append(Hex.ToHexString(outputBytes));
            }

            return output.ToString();
        }
    }
}