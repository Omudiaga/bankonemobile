﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(ReceiveMoneyTransactionDesigner))]
    public class ReceiveMoneyTransactionAction : ActionNode
    {
        /// <summary>
        /// User PAN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> Bank
        {
            get
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    return activity.Bank;
                }
                return null;
            }
            set
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    activity.Bank = value;
                }
            }
        }

        /// <summary>
        /// Card Expiry Date
        /// </summary>
        public InArgument<string> ExpiryDate
        {
            get
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    return activity.ExpiryDate;
                }
                return null;
            }
            set
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    activity.ExpiryDate = value;
                }
            }
        }

        
        /// <summary>
        /// Amount for the transaction
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }

        /// <summary>
        /// Account Number for speecified PAN and bank
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                ReceiveMoneyTransactionActivity activity = this.InternalState as ReceiveMoneyTransactionActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }

        public ReceiveMoneyTransactionAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ReceiveMoneyTransactionActivity();
        }

    }

    public class ReceiveMoneyTransactionActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        
        /// <summary>
        /// Selected bank
        /// </summary>
        public InArgument<string> Bank { get; set; }

        /// <summary>
        /// Account Name for speecified PAN and bank
        /// </summary>
        public InArgument<string> ExpiryDate { get; set; }

        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> Amount { get; set; }

        /// <summary>
        /// Account Number for speecified PAN and bank
        /// </summary>
        public OutArgument<string> Notification { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //string pin = pin.Get(context);
            //string accountNumber = AccountNumber.Get(context);

            ////TODO: Do Balance Enquiry
            //WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            //string sessionID = currentWorkflowInfo.SessionID;
            //MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            //string phoneNumber = mob.MobilePhone;

            //ITransactionType tranType = new BalanceInquiryTransactionType() ;

            //TransactionResponse response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            //decimal returnValue = Convert.ToDecimal(response.ResponseMessage);
            //Balance.Set(context,string.Format("{0:N}", returnValue));

        }
    }
}
