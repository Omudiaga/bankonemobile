﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using System.Security.Cryptography;
using BankOneMobile.Services.Utility;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(FundsTransferCashOutAccActivationDesigner))]
    public class FundsTransferCashOutAccActivationAction : ActionNode
    {
        /// <summary>
        /// User Activation Code
        /// </summary>
        public InArgument<string> ActivationCode {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }
        }
        /// <summary>
        /// Transfer Code
        /// </summary>
        public InArgument<string> TransferCode
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.TransferCode;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.TransferCode = value;
                }
            }
        }
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        /// <summary>
        /// New PIN entered the first Time
        /// </summary>
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> NewPIN1
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.NewPIN1;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.NewPIN1 = value;
                }
            }
        }
        public InArgument<string> NewPIN2
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.NewPIN2;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.NewPIN2 = value;
                }
            }
        }
        public OutArgument<string> Response
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.Response;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }
        /// <summary>
        /// User Activation Code
        /// </summary>
        public InArgument<bool> IsActivateOnly
        {
            get
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    return activity.IsActivateOnly;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAccActivationActivity activity = this.InternalState as FundsTransferCashOutAccActivationActivity;
                if (activity != null)
                {
                    activity.IsActivateOnly = value;
                }
            }
        }
        public FundsTransferCashOutAccActivationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new FundsTransferCashOutAccActivationActivity();
        }

    }

    public class FundsTransferCashOutAccActivationActivity : CodeActivity
    {
        /// <summary>
        /// Activation Code
        /// </summary>
        public InArgument<string> ActivationCode { get; set; }
        /// <summary>
        /// Activation Code
        /// </summary>
        public InArgument<string> TransferCode { get; set; }
        /// <summary>
        /// New PIN entered the second time
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// New PIN entered the second time
        /// </summary>
        public InArgument<string> NewPIN1 { get; set; }
        /// <summary>
        /// New PIN entered the second time
        /// </summary>
        public InArgument<string> NewPIN2 { get; set; }
        /// <summary>
        /// bool IsActivateOnly
        /// </summary>
        public InArgument<bool> IsActivateOnly { get; set; }
        /// <summary>
        /// Succesful or Failed Response 
        /// </summary>
        public OutArgument<string> Response { get; set; }
        
        

        protected override void Execute(CodeActivityContext context)
        {
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string activationCode = ActivationCode.Get(context);
            string transferCode = TransferCode.Get(context);
            string amount = Amount.Get(context);
            string pin1 = NewPIN1.Get(context);
            string pin2 = NewPIN2.Get(context);
            decimal amt = Convert.ToDecimal(amount);
            bool activateOnly = IsActivateOnly.Get(context);
            string pin = mob.ActivationCode;
            FundsTransferCashOutTransactionType trantType = new FundsTransferCashOutTransactionType();
            trantType.TheFee = new FeeTransactionType { Amount = 100 };
            try
            {
                pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            if (pin1 != pin2)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PinEntriesDoNotMatch), Utilities.GetDescription(ErrorCodes.PinEntriesDoNotMatch));
                
            }
            
           


          IList<  IFundsTransferCashInTransactionType> cashInEntries = new FundsTransferCashInTransactionTypeSystem().GetByBeneficiaryPhoneNumber(currentWorkflowInfo.DataSource, mob.MobilePhone);

          if (cashInEntries == null || cashInEntries.Count==0)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.NoPendingFundsTransferForThisPhoneNumber), Utilities.GetDescription(ErrorCodes.NoPendingFundsTransferForThisPhoneNumber));
                
            }
          bool correctToken = false; bool isConsumed = false; decimal total = 0M; trantType.TheCashIns = new List<IFundsTransferCashInTransactionType>();
          foreach (FundsTransferCashInTransactionType cashInEntry in cashInEntries)
          {

              if (cashInEntry.IsConsumed)
              {
                  isConsumed = true;
              }
              if (cashInEntry.Token == new MD5Password().CreateSecurePassword(transferCode))
              {
                  correctToken = true;
                  
              }
              total = total + cashInEntry.Amount;
              cashInEntry.TheCashOut = trantType;
              trantType.TheCashIns.Add(cashInEntry);
              
          }
          if (!correctToken)
          {
              throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidFundsTransferCode), Utilities.GetDescription(ErrorCodes.InvalidFundsTransferCode));
          }
          if (isConsumed)
          {
              throw new FailureException(Utilities.GetCode(ErrorCodes.FundsTransferHasAlreadyBeenCashed), Utilities.GetDescription(ErrorCodes.FundsTransferHasAlreadyBeenCashed));
          }
             
             if (total < amt)
             {
               string message=   string.Format("You cannot cashout more than the total  trasferred amount (NGN {0})",total);
                  throw new FailureException(Utilities.GetCode(ErrorCodes.FundsTransferError),message);
             }
             
             //trantType.TheMobileAccount = mob;
             trantType.Amount = amt;
             trantType.BeneficiaryPhoneNumber = mob.MobilePhone;
             trantType.DepositorPhoneNumber = cashInEntries[0].DepositorPhoneNumber;
             
             trantType.OriginalAmount = total;
             //trantType.TheCashIn = cashInEntries[0];
             trantType.TheFee = new FeeTransactionType { Amount = 100 };
             trantType.IsCash = true;// Please Remove Later
             FundsTransferCashOutTransactionTypeSystem.SaveFundsTransferCashOutTransactionType(currentWorkflowInfo.DataSource, trantType);
             bool consumed = false;

             try
             {
                 TransactionResponse response = new TransactionSystem().RunTransaction(pin, trantType, mob, mob.BankAccounts[0].BankAccount, mob.BankAccounts[0].BankAccount, amt, sessionID);
                 consumed = true;
                 

                 Response.Set(context, trantType.ActivationCode);
             }
             catch (AccountCreatedNoFundsTranferException ex)
             {
                 //CleanUp(mob, trantType, pin1, sessionID, consumed);
                 throw new FailureException(Utilities.GetCode(ErrorCodes.FundsTransferError), "Your account as been created but the funds have not yet been transfered");
             }
             catch (FundsTransferNoCashOutException ex)
             {
                 //consumed = true;
                 CleanUp(mob, trantType, pin1, sessionID,consumed, currentWorkflowInfo);
                 throw new FailureException(Utilities.GetCode(ErrorCodes.AccountCreatedNoFundsTransfer),String.Format("{0}. Reason-{1}", Utilities.GetDescription(ErrorCodes.AccountCreatedNoFundsTransfer),ex.Message));
             }

             catch (InvalidAgentCodeException)
             {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));

             }
             catch (NoSwitchResponseException)
             {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
             }
             catch (NoIssuerResponseException)
             {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
             }

             catch (FailedTransactionException ex)
             {
                 throw new FailureException("13", ex.Message);
             }
                 
             catch (NoHSMResponseException)
             {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
             }
             catch (Exception ex)
             {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
             }
            
            //Response.Set(context, response.ResponseMessage);

            finally
            {


                //STOP.... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Close(DataSourceFactory.GetDataSource(DataCategory.Shared));

                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);

                }

            }
            

        }
        public void CleanUp(MobileAccount mob, FundsTransferCashOutTransactionType tranType,string newPin,string sessionID,bool isConsumed, WorkflowInfo theWorkflowInfo)
        {
            tranType.TheMobileAccount = mob;
            tranType.ActivationCode = mob.ActivationCode;

            if (isConsumed)
            {
                foreach (FundsTransferCashInTransactionType ch in tranType.TheCashIns)
                {
                    ch.IsConsumed = true;
                }
            }
            FundsTransferCashOutTransactionTypeSystem.UpdateFundsTransferCashOutTransactionType(theWorkflowInfo.DataSource, tranType);
            ChangePin(mob, mob.ActivationCode, newPin,sessionID);
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
            {
                new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, theWorkflowInfo.DataSource);

            }
           // new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).UpdateMobileAccount(mob);
        }
        public void ChangePin(MobileAccount mob, string activationCode, string newPin,string sessionID)
        {
            var workflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionID);
            try
            {
                activationCode = HSMCenter.GeneratePinBlock(mob.MobilePhone, activationCode, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }


            ITransaction trans = new Transaction();
            IPinChangeTransactionType tranType = new PinChangeTransactionType();

            // ta.EncryptedPIN = pin1;
            tranType.NewPIN = HSMCenter.GeneratePinBlock(mob.MobilePhone, newPin, AppZone.HsmInterface.PinBlockFormats.ANSI);

            trans.FromInstitutionCode = mob.InstitutionCode;
            trans.From = mob.BankAccounts[0].BankAccount; //mob.RecievingBankAccount.BankAccount;
            try
            {
                new TransactionSystem().RunTransaction(activationCode, tranType, mob, trans.From, string.Empty, 0.0M, sessionID);
                // (pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, workflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                

            }
        }
    }


}
