﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.CustomActions
{

    public class SelectAccountNumberScreen : MenuScreen
    {

        State _NextNode;
        CompletionCallback _OnCompleted;
        public string SelectedAccountNumber { get; set; }
        
        public bool UseEnteredPhoneNo { get; set; }
        public Dictionary<int, string> InstitutionCodes { get; set; }
        public InArgument<string> EnteredPhoneNumber
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as SelectAccountNumberActivity).PhoneNumber;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as SelectAccountNumberActivity).PhoneNumber = value;
                }
            }
        }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            _OnCompleted = onCompleted;
            nextNode = null;
            _NextNode = nextNode;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
            context.ScheduleActivity(this.InternalActivity,this.TheFlow.Resume_Flow_Completed);

            nextNode = _NextNode;
            return false;
            
        }
        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            int selectedMenu = 0;
            if (response != null)
            {
                Int32.TryParse(response, out selectedMenu);
            }
            if (selectedMenu == 0 || selectedMenu > this.MenuItems.Count)
            {
                currentWorkflowInfo.DisplayMessage = "Unknown number. Try again\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown number. Try again<br/>");
                //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                if (this.Transitions != null && this.Transitions.Count == 1)
                {
                    this.Next = this.Transitions[0].To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }
        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {

            InstitutionCodes = new Dictionary<int, string>();
            string phone = (this.InternalActivity as SelectAccountNumberActivity).phoneNumber;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            IList<ILinkingBankAccount> accounts = null;
            if (!UseEnteredPhoneNo)
            {
                accounts = currentWorkflowInfo.InitiatedBy.BankAccounts.Where(x=>x.BankStatus!=BankAccountStatus.InActive).ToList();
            }
            else
            {
                accounts = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetByPhoneNumber(phone).BankAccounts.Where(x => x.BankStatus != BankAccountStatus.InActive).ToList();
            }
            int i = 1;
            this.MenuItems.Clear();
            foreach (LinkingBankAccount lbc in accounts)
            {
                this.MenuItems.Add(new MenuItem() { Text = string.Format("{0}:{1}", lbc.BankAccount, lbc.InstitutionName), Index = i });
                InstitutionCodes.Add(i, lbc.InstitutionCode);
                i++;
            }

            if (accounts.Count == 1)
            {
                if (this.Transitions != null && this.Transitions.Count == 1)
                {
                    this.Next = this.Transitions[0].To;
                    (this.InternalActivity as SelectAccountNumberActivity).AccountNumber = string.Format("{0}:{1}", accounts[0].BankAccount, accounts[0].InstitutionName);
                    currentWorkflowInfo.AttachedCompletedCallback = _OnCompleted;
                    context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                   // _OnCompleted(context, instance);
                    return;
                }
                
            }
             base.Execute(context, _OnCompleted, out _NextNode);

        }

        public SelectAccountNumberScreen()
        {
            MenuItems = new List<MenuItem>();
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new SelectAccountNumberActivity();
        }

    }
    public class SelectAccountNumberActivity : SubActivity
    {
        public string AccountNumber { get; set; }
        public string instCode { get; set; }
        public string phoneNumber { get; set; }
        public OutArgument<string> InstitutionCode { get; set; }
        public InArgument<string> PhoneNumber { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            base.Execute(context);
            phoneNumber = PhoneNumber.Get(context);
            if (!string.IsNullOrEmpty(AccountNumber))
            {
                context.SetValue(Response, AccountNumber);
            }
            context.SetValue(InstitutionCode, instCode);
            
        }
    }

}
