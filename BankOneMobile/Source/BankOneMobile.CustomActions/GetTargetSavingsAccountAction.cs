﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetTargetSavingsAccountDesigner))]
    public class GetTargetSavingsAccountAction : ActionNode
    {
        /// <summary>
        /// Account Number
        /// </summary>
        public OutArgument<string> BetaAccountNumber
        {
            get
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {

                    return activity.BetaAccountNumber;
                }
                return null;
            }
            set
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {
                    activity.BetaAccountNumber = value;
                }
            }
        }

        public OutArgument<IList<TargetSavingsAccount>> TargetSavingsAccountNumbers
        {
            get
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {

                    return activity.TargetSavingsAccountNumbers;
                }
                return null;
            }
            set
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {
                    activity.TargetSavingsAccountNumbers = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {

                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        public InArgument<bool> IsReapplying
        {
            get
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {

                    return activity.IsReapplying;
                }
                return null;
            }
            set
            {
                GetTargetSavingsAccountActivity activity = this.InternalState as GetTargetSavingsAccountActivity;
                if (activity != null)
                {
                    activity.IsReapplying = value;
                }
            }
        }

        public GetTargetSavingsAccountAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetTargetSavingsAccountActivity();
        }

    }

    public class GetTargetSavingsAccountActivity : CodeActivity
    {
        /// <summary>
        /// Entered Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<bool> IsReapplying { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public OutArgument<IList<TargetSavingsAccount>> TargetSavingsAccountNumbers { get; set; }
        public OutArgument<string> BetaAccountNumber { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            Institution inst = null;
            string custPhoneNumber = PhoneNumber.Get(context);
            bool isReapplying = IsReapplying.Get(context);
            string instCode = "";
            try
            {
                Agent ag;
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                if (ag == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("Non -Agent Initiated Trnsactions");
                }
                instCode = ag.MobileAccount.InstitutionCode;
                mob.InstitutionCode = instCode;
            }
            catch (InvalidInstitutionException)
            {
                throw new FailureException("51", "Your Institution is not set up for Mobile Transactions");
            }

            mob.InstitutionCode = instCode;


            LinkingBankAccount bankAccount = null;
            MobileAccount mobile = null;
            BankOneMobile.Services.SwitchServiceRef.Account[] account = null;
            IList<TargetSavingsAccount> targetAccounts = new List<TargetSavingsAccount>();
            string message = string.Empty;
            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("about to get beta account: Phone:{0}", custPhoneNumber)));
                mobile = new MobileAccountSystem().GetByPhoneNumber(custPhoneNumber); //client.GetAccountByAccountNumber(phoneNumber) as LinkingBankAccount;
                if (mobile == null)
                {
                    throw new Exception("Phone number is not tied to any BETA account. A BETA account with this phone number is required to proceed");
                }
                else
                {
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten mobile account. Number of Bank accounts: {0}", mobile.BankAccounts == null ? "null" : mobile.BankAccounts.Count.ToString())));
                    bankAccount = mobile.BankAccounts.First() as LinkingBankAccount;
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten bank account: {0}", bankAccount == null ? "null" : bankAccount.BankAccount)));
                    using (BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient client = new Services.SwitchServiceRef.SwitchingServiceClient())
                    {
                        account = client.GetCommitmentSavingsAccountByPhoneNo(out message, mob.InstitutionCode, custPhoneNumber);
                    }


                    if (isReapplying)
                    {
                        account = account.Where(x => x.Status == Services.SwitchServiceRef.AccountStatus.InActive).ToArray();
                    }
                    else
                    {
                        account = account.Where(x => x.Status == Services.SwitchServiceRef.AccountStatus.Active).ToArray();
                    }
                    if (account == null || account.Count() == 0)
                    {
                        throw new Exception(string.Format("This customer has no commitment savings account {0}", isReapplying ? "for reactivation" : ""));
                    }
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten target savings account-count: {0}. message: {1}", account.Count(), message)));
                    foreach (var item in account)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("about to add item: {0}|item number:{1}", item.NUBAN, item.Number)));
                        targetAccounts.Add(new TargetSavingsAccount
                        {
                            NUBAN = item.NUBAN,
                            Name = item.Name,
                            Number = item.Number,
                            CustomerID = item.CustomerID
                        });
                    }
                }

                // response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (InsufficientFundsException ex)
            {
                System.Diagnostics.Trace.TraceError("Minimum balance required is 5,000. Kindly fund your BETA account to proceed");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw;
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }

            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            BetaAccountNumber.Set(context, bankAccount.BankAccount);
            TargetSavingsAccountNumbers.Set(context, targetAccounts);
        }
    }
}
