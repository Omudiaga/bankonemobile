﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(FundsTransferCashOutAgentDesigner))]
    public class FundsTransferCashOutAgentAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        ///Product for the Account
        /// </summary>
        public InArgument<Product> Product
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Transfer Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.Amount;

                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }

        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerFirstName
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerFirstName;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.CustomerFirstName = value;
                }
            }
        }
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerLastName
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerLastName;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.CustomerLastName = value;
                }
            }
        }
        /// <summary>
        ///sex
        /// </summary>
        public InArgument<string> Sex
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.Sex;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.Sex = value;
                }
            }
        }
        /// <summary>
        ///is renewed cashout
        /// </summary>
        public InArgument<bool> IsRenewCashOut
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.IsRenewCashOut;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.IsRenewCashOut = value;
                }
            }
        }
        public OutArgument<string> ActivationCode
        {
            get
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    return activity.AccountActivationCode;
                }
                return null;
            }
            set
            {
                FundsTransferCashOutAgentActivity activity = this.InternalState as FundsTransferCashOutAgentActivity;
                if (activity != null)
                {
                    activity.AccountActivationCode = value;
                }
            }
        }



        public FundsTransferCashOutAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new FundsTransferCashOutAgentActivity();
        }

    }

    public class FundsTransferCashOutAgentActivity : CodeActivity
    {

        public  const string DISPLAY_TEXT = "The Activation Code is";
        public  const string COMPLETION_TEXT = "Please help the customer complete activation.";
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<Product> Product { get; set; }
        /// <summary>
        /// Activation Code
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }
        /// <summary>
        /// Customer Name
        /// </summary>
        public InArgument<string> CustomerFirstName { get; set; }
        /// <summary>
        /// Account Activation Code
        /// </summary>
        public InArgument<string> CustomerLastName { get; set; }
        /// <summary>
        /// Account Activation Code
        /// </summary>
        public InArgument<string> Sex { get; set; }
        /// <summary>
        /// Account Activation Code
        /// </summary>
        public OutArgument<string> AccountActivationCode { get; set; }
        /// <summary>
        /// Token IsRenewedCashOut
        /// </summary>
        public InArgument<string> Token { get; set; }
        /// <summary>
        ///  IsRenewedCashOut
        /// </summary>
        public InArgument<bool> IsRenewCashOut { get; set; }
        /// <summary>
        /// IsCash
        /// </summary>
        public InArgument<bool> IsCash { get; set; }
        


        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string pin = PIN.Get(context);
           
            try
            {
                pin = HSMCenter.GeneratePinBlock(mob.MobilePhone, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string customerPhone = CustomerPhoneNumber.Get(context);
            string customerLastName = CustomerLastName.Get(context);
            string customerFirstName = CustomerFirstName.Get(context);
            string sex = Sex.Get(context);
            string amount = Amount.Get(context);
            string fundsTransferCode = AccountActivationCode.Get(context);
            string agentCode= null;
            string token = Token.Get(context);
            bool isCash = IsCash.Get(context);
           // BankOneMobile.Core.Implementations.Product prod = Product.Get(context);

           // string result = TransactionSystem.RunTransaction(pin);
           // AccountActivationCode.Set(context, result);
            string product =Product.Get(context).Code;
            
            //MobileAccount depositorAcct = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(customerPhone);
           // customerPhone = customerPhone == null ? depositorAcct.BankAccounts[0].BankAccount : customerPhone;


            Agent agent = null;
            using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
            {
                agent = client.GetAgentByPhoneNumber(mob.MobilePhone);
            }
            //new AgentSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAgentByPhoneNumber(mob.MobilePhone) as Agent;
            if(agent ==null)
            {
                 throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));

                return;
            }
            

            IFundsTransferCashOutTransactionType tranType = new FundsTransferCashOutTransactionType();
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
          IList< IFundsTransferCashInTransactionType> cashInEntries = new List<IFundsTransferCashInTransactionType>();
            //IFundsTransferCashInTransactionType cashInEntry = new FundsTransferCashInTransactionType();
            cashInEntries = new FundsTransferCashInTransactionTypeSystem().GetByBeneficiaryPhoneNumber(currentWorkflowInfo.DataSource, customerPhone);
            if (cashInEntries == null || cashInEntries.Count == 0)//|| tranType.BeneficiaryPhoneNumber != customerPhone)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.NoPendingFundsTransferForThisPhoneNumber), Utilities.GetDescription(ErrorCodes.NoPendingFundsTransferForThisPhoneNumber));
                //AccountActivationCode.Set(context, "No Pending Funds Transfer For this phone number");
                //return;
            }
            else
            {
            }
            tranType.Amount = Convert.ToDecimal(amount);
            tranType.DepositorPhoneNumber = customerPhone;
            tranType.AgentCode = agentCode;
            tranType.Token = token;
            tranType.IsCash = isCash;           
            
            Gender gender = (Gender) Enum.Parse(typeof(Gender),sex);
           
            
            
            IMobileAccount mobileAcct  = null;
            

            try
            {
                using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                {
                    mobileAcct=  client.CreateMobileAccountFromAgent(customerLastName, customerFirstName, customerPhone, gender.ToString(), product, agent.Code,true);
                }
               // = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(customerPhone);
                //if (mobileAcct == null)
                //{

                //   mobileAcct  = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).CreateMobileAccountFromAgent(customerLastName, customerFirstName, customerPhone, gender.ToString(), product, agent,true);
                //}
                tranType.TheMobileAccount=mobileAcct;
                

                FundsTransferCashOutTransactionTypeSystem.SaveFundsTransferCashOutTransactionType(currentWorkflowInfo.DataSource, tranType as FundsTransferCashOutTransactionType);
                AccountActivationCode.Set(context, mobileAcct.ActivationCode);
                return;

                //TransactionResponse response = new TransactionSystem().RunTransaction(pin, tranType,mob, mob.MobilePhone,customerPhone, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (InvalidPhoneNumberException)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
            }


            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                //if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                //{
                //    using (USSDThirdPartyDemo.UssdThirdPartyWsPortClient client = new USSDThirdPartyDemo.UssdThirdPartyWsPortClient())
                //    {

                //        client.onSessionEnd(sessionID, 200, "Ended Normally");
                //    }
                //}

            }

        }
    }

}
