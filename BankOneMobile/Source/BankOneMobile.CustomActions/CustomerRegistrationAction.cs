﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Drawing;
using System.IO;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CustomerRegistrationDesigner))]
    public class CustomerRegistrationAction : ActionNode
    {
        /// <summary>
        /// Product Code
        /// </summary>
        public OutArgument<string> ProductCode
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ProductCode;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ProductCode = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
       
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerName
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerName;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerName = value;
                }
            }
        }
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerFirstName
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerFirstName;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerFirstName = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Customer Gender
        /// </summary>
        public InArgument<string> Gender
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Gender;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Gender = value;
                }
            }
        }
        /// <summary>
        ///Product for the Account
        /// </summary>
        public InArgument<Product> Product
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }
        
        
        
        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }
             
        }
        public OutArgument<string> AccountNumber
        {
            get
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                CustomerRegistrationActivity activity = this.InternalState as CustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }

        }
         
        public CustomerRegistrationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CustomerRegistrationActivity();
        }

    }

    public class CustomerRegistrationActivity : CodeActivity
    {
        private const string CONFIRMATION_TEXT = "The Customer has been successfully registered on Bank One.Please notify them of this Activation Code:";
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Customer Full Name
        /// </summary>
        public InArgument<string> CustomerName { get; set; }
        
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Customer First Name
        /// </summary>
        public InArgument<string> CustomerFirstName { get; set; }

        /// <summary>
        /// Customer Gender
        /// </summary>
        public InArgument<string> Gender { get; set; }
        /// <summary>
        /// Product for the Account
        /// </summary>
        public InArgument<Product> Product { get; set; }
        /// <summary>
        /// Passport image path
        /// </summary>
        public InArgument<string> Passport { get; set; }
        /// <summary>
        /// Customer Full Name
        /// </summary>
        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode { get; set; }
        /// <summary>
        /// Product Code
        /// </summary>
        public OutArgument<string> ProductCode { get; set; }
        public OutArgument<string> AccountNumber { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
             
            string pin = PIN.Get(context);
            string customerName = CustomerName.Get(context);
            string customerFirstName = CustomerFirstName.Get(context);
            string customerPhone = CustomerPhoneNumber.Get(context);
            string gender = Gender.Get(context);
            Product prod = Product.Get(context);
            if (prod == null)
            {
                prod = new Product { Code = "002", Name = "Savings and Current" };
            }
           string productCode =prod.Code;
            
            

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            IRegistrationTransactionType tranType = new RegistrationTransactionType();
            tranType.LastName = customerName;
            tranType.PhoneNumber = customerPhone;
            tranType.Gender = gender;
            tranType.TheGender=(Gender) Enum.Parse(typeof(Gender),gender);
            tranType.ProductCode = productCode;
            tranType.TheProduct = prod;
            tranType.FirstName = customerFirstName;
            //Get passport image
            System.Diagnostics.Trace.TraceInformation("About to get image from server location.");
            string path = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("ImageUploadPath") ? 
                System.Configuration.ConfigurationManager.AppSettings["ImageUploadPath"] : "C:\\";
            Image image = Image.FromFile(string.Format("{0}{1}.png", path, sessionID.Replace("+", "")));
            var ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            System.Diagnostics.Trace.TraceInformation("Gotten image from server location. about to tun transaction.");
            tranType.Passport = ms.ToArray();

            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, mob.MobilePhone, mob.MobilePhone, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (TransactionNotPermittedToNonAgentsException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (AlreadyRegisterdCustomerException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberAlreadyRegistered), Utilities.GetDescription(ErrorCodes.PhoneNumberAlreadyRegistered));
            }
            catch (AlreadyExistingAccountException ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), Utilities.GetDescription(ErrorCodes.SystemError));
            }
            catch (CoreBankingWebServiceException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), Utilities.GetDescription(ErrorCodes.UnableToConnectToCoreBanking));
            }
            catch (NoHSMResponseException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {
                //if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                //{
                //   // new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally);

                //}
                //STOP.... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            ActivationCode.Set(context,  response.ResponseMessage);
            AccountNumber.Set(context, response.ResponseDescription);

        }
    }

}
