﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CompareActionDesigner))]
    public class CompareAction : ActionNode
    {
        /// <summary>
        /// Test Value
        /// </summary>
        public InArgument<string> Value
        {
            get
            {
                CompareActivity activity = this.InternalState as CompareActivity;
                if (activity != null)
                {
                    return activity.Value;
                }
                return null;
            }
            set
            {
                CompareActivity activity = this.InternalState as CompareActivity;
                if (activity != null)
                {
                    activity.Value = value;
                }
            }
        }
        /// <summary>
        /// value to be compared against
        /// </summary>
        public InArgument<string> CompareWith
        {
            get
            {
                CompareActivity activity = this.InternalState as CompareActivity;
                if (activity != null)
                {
                    return activity.CompareWith;
                }
                return null;
            }
            set
            {
                CompareActivity activity = this.InternalState as CompareActivity;
                if (activity != null)
                {
                    activity.CompareWith = value;
                }
            }
        }
       
       

        public CompareAction()
        
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CompareActivity();
        }

    }

    public class CompareActivity : CodeActivity
    {
        /// <summary>
        /// value passed to the action
        /// </summary>

        
        public InArgument<string> Value { get; set; }
       
        /// <summary>
        /// value to be tested against
        /// </summary>
         public InArgument<string> CompareWith { get; set; }
       
        protected override void Execute(CodeActivityContext context)
        {
            string test = Value.Get(context);

            string compareValue = CompareWith.Get(context);
            if (test.Trim() != compareValue.Trim())
            {
                throw new Exception("");
            }

        }
    }
}
