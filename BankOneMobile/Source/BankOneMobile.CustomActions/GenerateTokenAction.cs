﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GenerateTokenActionDesigner))]
    public class GenerateTokenAction : ActionNode
    {
        
        public InArgument<string> PhoneNumber
        {
            get
            {
                GenerateTokenActivity activity = this.InternalState as GenerateTokenActivity;
                if (activity != null)
                {
                 
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GenerateTokenActivity activity = this.InternalState as GenerateTokenActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public OutArgument<string> TokenValue
        {
            get
            {
                GenerateTokenActivity activity = this.InternalState as GenerateTokenActivity;
                if (activity != null)
                {

                    return activity.TokenValue;
                }
                return null;
            }
            set
            {
                GenerateTokenActivity activity = this.InternalState as GenerateTokenActivity;
                if (activity != null)
                {
                    activity.TokenValue = value;
                }
            }
        }
      
        public GenerateTokenAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GenerateTokenActivity();
        }

    }

    public class GenerateTokenActivity : CodeActivity
    {
        
        
        public InArgument<string> PhoneNumber { get; set; }
        public OutArgument<string> TokenValue { get; set; }

        
        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = PhoneNumber.Get(context);
            string token = TokenValue.Get(context);

            Agent ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (ag == null)
            {
                throw new InvalidAgentCodeException("Invalid Agent Exception");
            }
            string instCode = ag.TheAgentAccount.InstitutionCode;
            
            string rsp = string.Empty;      
             
              
            
            try
            {
                 rsp = new TokenSystem(currentWorkflowInfo.DataSource).GenerateTokenServiceMethod(phoneNumber, instCode);
               
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            finally
            {
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            TokenValue.Set(context, rsp);
            
        
        }
    }
}
