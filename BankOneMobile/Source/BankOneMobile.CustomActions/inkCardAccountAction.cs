﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;
using System.Diagnostics;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(LinkCardAccountActionDesigner))]
    public class LinkCardAccountAction : ActionNode
    {
       
        public InArgument<string> AccountNumber
        {
            get
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        public OutArgument<string> Response
        {
            get
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    return activity.Response;
                }
                return null;
            }
            set
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }
        public InArgument<string> SerialNumber
        {
            get
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    return activity.SerialNumber;
                }
                return null;
            }
            set
            {
                LinkCardAccountActivity activity = this.InternalState as LinkCardAccountActivity;
                if (activity != null)
                {
                    activity.SerialNumber = value;
                }
            }
        }       


        public LinkCardAccountAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new LinkCardAccountActivity();
        }

    }

    public class LinkCardAccountActivity : CodeActivity
    {
       
        public OutArgument<string> Response { get; set; }
        public InArgument<string> AccountNumber { get; set; }
        public InArgument<string> SerialNumber { get; set; }
        
        

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source = DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount agentMob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = agentMob.MobilePhone;
            
            string serialNo = SerialNumber.Get(context);
            string acctNo = AccountNumber.Get(context);
           
            
            try
            {
                
              
                Agent ag = null;
                
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(agentMob.MobilePhone);


                if (ag == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("Transaction not allowed for Non-Agents");
                }
               else  if (!ag.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("Transaction not allowed for Non-Agents");
                }
               

                string instCode = ag.TheAgentAccount.InstitutionCode; string custId = string.Empty; MobileAccount mob = null;

               

                    try
                    {
                        Trace.TraceInformation("About to start card linking.");
                        new TransactionSystem().LinkCardAccountNumber(instCode, acctNo, serialNo, ag.Code);                       
                    }

                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError("Caught an error during card linking.");
                        System.Diagnostics.Trace.TraceError(ex.Message);
                        System.Diagnostics.Trace.TraceError(ex.StackTrace);
                        System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        throw new ApplicationException(String.Format("{0}", ex.Message));
                    }
                   
                    Response.Set(context, "00");
                    

                
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotInitiatedByAgent), Utilities.GetDescription(ErrorCodes.TransactionNotInitiatedByAgent));
            }
            catch (AlreadyExistingAccountException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.AccountNumberAlreadyExists), Utilities.GetDescription(ErrorCodes.AccountNumberAlreadyExists));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }

            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            
            
                        
            
            }
            
            
       
        
            
        
        
    }
}
