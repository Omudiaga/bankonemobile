﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankoneMobile.AdvansLafayette;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(TargetSavingsDepositDesigner))]
    public class TargetSavingsDepositAction : ActionNode
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public InArgument<string> TargetSavingsAccountNumber
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.TargetSavingsAccountNumber;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.TargetSavingsAccountNumber = value;
                }
            }
        }
        public InArgument<string> BetaAccountNumber
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.BetaAccountNumber;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.BetaAccountNumber = value;
                }
            }
        }
        public InArgument<string> PIN
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        /// <summary>
        /// Transaction successful
        /// </summary>
        public OutArgument<string> TransactionSuccessful
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.TransactionSuccessful;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.TransactionSuccessful = value;
                }
            }
        }
        public OutArgument<string> BetaAccountBalance
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.BetaAccountBalance;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.BetaAccountBalance = value;
                }
            }
        }
        public OutArgument<string> CommitmentSavingsAccountBalance
        {
            get
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    return activity.CommitmentSavingsAccountBalance;
                }
                return null;
            }
            set
            {
                TargetSavingsDepositActivity activity = this.InternalState as TargetSavingsDepositActivity;
                if (activity != null)
                {
                    activity.CommitmentSavingsAccountBalance = value;
                }
            }
        }
        
       
       
       

        public TargetSavingsDepositAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new TargetSavingsDepositActivity();
        }

    }

    public class TargetSavingsDepositActivity : CodeActivity
    {
        /// <summary>
        /// Target savings account number
        /// </summary>
        public InArgument<string> TargetSavingsAccountNumber { get; set; }
        /// <summary>
        /// Beta savings account number
        /// </summary>
        public InArgument<string> BetaAccountNumber { get; set; }
        
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }

        public InArgument<string> PIN{get; set; }
        public OutArgument<string> TransactionSuccessful { get; set; }
        public OutArgument<string> BetaAccountBalance { get; set; }
        public OutArgument<string> CommitmentSavingsAccountBalance { get; set; }
       

        
        
        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string targetSavingsAccountNumber = TargetSavingsAccountNumber.Get(context);

            string betaAccountNumber = BetaAccountNumber.Get(context);

            string amount = Amount.Get(context);
            string pin=PIN.Get(context);
            string realResponse = string.Empty;


            ITargetSavingsDepositTransactionType tranType = new TargetSavingsDepositTransactionType();
            //tranType.TheFee = new FeeTransactionType { Amount = 100 };
            string instCode = string.Empty;
            Agent ag;
            ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (ag == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Non-Agent Initiated Trnsactions");
            }
            instCode = ag.MobileAccount.InstitutionCode;
            mob.InstitutionCode = instCode;
            TransactionResponse response = new TransactionResponse();
            TransactionResponse responseBetaBalInq = new TransactionResponse();
            TransactionResponse responseComBalInq = new TransactionResponse();
            tranType.Amount = decimal.Parse(amount);
            tranType.BetaAccountNumber = betaAccountNumber;
            tranType.TargetSavingsAccountNumber = targetSavingsAccountNumber;
            tranType.TheBankAccount = mob.BankAccounts.First() as LinkingBankAccount;

            var balanceBeta = new BankOneMobile.Services.SwitchServiceRef.AccountBalance();
            var balanceTarget = new BankOneMobile.Services.SwitchServiceRef.AccountBalance();
            try
            {
                response = new TransactionSystem().RunTransaction(pin,tranType, mob, betaAccountNumber,targetSavingsAccountNumber, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                if (response.Status == TransactionStatus.Successful.ToString())
                {
                    realResponse = "true";
                }
                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    balanceBeta = client.GetBalance(mob.InstitutionCode, betaAccountNumber);
                    balanceTarget = client.GetCommitmentSavingsAccountBalance(mob.InstitutionCode, targetSavingsAccountNumber);
                }
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten beta accountbalance: {0}", balanceBeta == null ? "null" : balanceBeta.LedgerBalance.ToString())));
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten target accountbalance: {0}", balanceTarget == null ? "null" : balanceTarget.LedgerBalance.ToString())));
                if (balanceBeta == null)
                {
                    //throw new InvalidPhoneNumberException("Could not retrieve account balance");
                }
                if (balanceTarget == null)
                {
                    //throw new InvalidPhoneNumberException("Could not retrieve account balance");
                }
                
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents);
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
                
            }
            catch (InvalidAgentCodeException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.InvalidAgentCode);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.SwitchUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.IssuerUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                realResponse=ex.Message;
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                realResponse="System Error";
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {
                BetaAccountBalance.Set(context, (balanceBeta.AvailableBalance/100).ToString("N2"));
                CommitmentSavingsAccountBalance.Set(context, (balanceTarget.AvailableBalance/100).ToString("N2"));
                TransactionSuccessful.Set(context, realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP...... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }



           //tring result = TransactionSystem.RunTransaction(pin);
            //context.SetValue(Notification,response.ResponseMessage);
            //string result = TransactionSystem.RunTransaction(pin);

            

        }
    }

}
