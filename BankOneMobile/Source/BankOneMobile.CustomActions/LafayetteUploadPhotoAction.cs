﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(BalanceEnquiryDesigner))]
    public class UploadLafayettePhotoAction : ActionNode
    {
            
        public InArgument<string> CustomerID
        {
            get
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {

                    return activity.CustomerID;
                }
                return null;
            }
            set
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    activity.CustomerID = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        public InArgument<Byte[]> Photo
        {
            get
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    return activity.Photo;
                }
                return null;
            }
            set
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    activity.Photo = value;
                }
            }
        }

        
        public OutArgument<Boolean> Uploaded
        {
            get
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    return activity.Uploaded;
                }
                return null;
            }
            set
            {
                UploadLafayettePhotoActivity activity = this.InternalState as UploadLafayettePhotoActivity;
                if (activity != null)
                {
                    activity.Uploaded = value;
                }
            }
        }
        
        public UploadLafayettePhotoAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new UploadLafayettePhotoActivity();
        }

    }

    public class UploadLafayettePhotoActivity : CodeActivity
    {
       
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        public InArgument<string> CustomerID { get; set; }
        public InArgument<Byte[]> Photo { get; set; }
       

      
        public OutArgument<Boolean> Uploaded { get; set; }
       

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string custAcctNo = AccountNumber.Get(context);
            string custID = CustomerID.Get(context);
            byte[] image = Photo.Get(context);
            bool toReturn = false;

            toReturn = new BankoneMobile.AdvansLafayette.Services.ServiceInterfaceSystem().SaveImages(custAcctNo, custID, image);
            

            Uploaded.Set(context, toReturn);
            
            
        
        }
    }
}
