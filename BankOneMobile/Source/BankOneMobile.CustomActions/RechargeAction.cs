﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(RechargeDesigner))]
    public class RechargeAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        /// Mobile Networkd Name
        /// </summary>
        public InArgument<PaymentItem> ThePaymentItem
        {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.ThePaymentItem;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.ThePaymentItem = value;
                }
            }
        }
        /// <summary>
        /// Denomination
        /// </summary>
                /// <summary>
        
       
        
        /// <summary>
        /// Transaction Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public OutArgument<string> PaymentReference
        {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.PaymentReference;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.PaymentReference = value;
                }
            }
        }

        



        /// <summary>
        /// Recharege PIN Code
        /// </summary>
        public OutArgument<string> RechargePIN {
            get
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    return activity.RechargePIN;
                }
                return null;
            }
            set
            {
                RechargeActivity activity = this.InternalState as RechargeActivity;
                if (activity != null)
                {
                    activity.RechargePIN = value;
                }
            }
        }

        public RechargeAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new RechargeActivity();
        }

    }

    public class RechargeActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Accoun Number
        /// </summary>
        public InArgument<string>AccountNumber { get; set; }       
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount  { get; set; }
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; } 
        /// <summary>
        /// Recharge PIN 
        /// </summary>
        public OutArgument<string> RechargePIN { get; set; }
        /// <summary>
        /// The Payment Item 
        /// </summary>
        public InArgument<PaymentItem> ThePaymentItem { get; set; }
        /// <summary>
        ///Purchase Value ( eg Recharge PIN)
        /// </summary>
        public OutArgument<string> PurchaseValue { get; set; }
        /// <summary>
        ///Payment Reference
        /// </summary>
        public OutArgument<string> PaymentReference { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string pin = PIN.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string beneficiary = PhoneNumber.Get(context);
            

            PaymentItem thePaymentItem = ThePaymentItem.Get(context);
            string amount = Amount.Get(context);



            IMerchant merch = thePaymentItem.TheMerchant;

            //IDataSource source = DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;
                mob.InstitutionCode = instCode;
            }
            if (string.IsNullOrEmpty(beneficiary))
            {
                beneficiary = phoneNumber;
            }
            pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);

            //   pin = ThalesSim.Core.Utility.HexStringToByteArray(pin,);
            

            IRechargeTransactionType tranType = new RechargeTransactionType();
            //tranType.
            tranType.TheMobileAccount = mob;
            tranType.TheBankAccount = tranType.TheMobileAccount.BankAccounts.Where(x => x.BankAccount == accountNumber).FirstOrDefault() as LinkingBankAccount;
           
            tranType.Beneficiary = beneficiary;
            tranType.TheMerchant = merch;
            tranType.MerchantCode = merch.Code;
            tranType.MerchantName = merch.Name;
            tranType.Amount = thePaymentItem.UnitPrice==0M?Convert.ToDecimal(amount)*100:Convert.ToDecimal(amount);
            //tranType.RechargeType = thePaymentItem.UnitPrice==0 ? RechargeType.VTU : RechargeType.;
            tranType.TransactionReference = new TransactionSystem().GenerateTransactionRef();
            tranType.PaymentItemCode = thePaymentItem.Code;
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            //tranType.Denomination= Dem
            TransactionResponse response = null;

            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber,tranType.Amount, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }
            catch (NoISWGatewayResponse)
            {
                string errorDesc = "No Response from ISW Gateway";
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), errorDesc);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                PaymentReference.Set(context, tranType.TransactionReference);
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();
                

            }
            
             RechargePIN.Set(context,response.ResponseDescription);
             PaymentReference.Set(context, tranType.PaymentReference);

            

        }
    }

}
