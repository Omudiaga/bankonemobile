﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(LocalFundsTransferDesigner))]
    public class LocalFundsTransferAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> Acc_PhoneNumber
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Acc_PhoneNumber;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.Acc_PhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> FromAccountNumber
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.FromAccountNumber;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.FromAccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Transfer Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public InArgument<string> ReceivingAccountName
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.ReceivingAccountName;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.ReceivingAccountName = value;
                }
            }
        }
        public InArgument<bool> IsPhoneNumber
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.IsPhoneNumber;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// Specifies if the transfer is a cash transfer from Agent's phone
        /// </summary>
        public InArgument<bool> IsFromAgent
        {
            get
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    return activity.IsFromAgent;
                }
                return null;
            }
            set
            {
                LocalFundsTransferActivity activity = this.InternalState as LocalFundsTransferActivity;
                if (activity != null)
                {
                    activity.IsFromAgent = value;
                }
            }
        }
        
       

        public LocalFundsTransferAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new LocalFundsTransferActivity();
        }

    }

    public class LocalFundsTransferActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> Acc_PhoneNumber { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected AccoutNumber
        /// </summary>
        public InArgument<string> FromAccountNumber { get; set; }
        public InArgument<string> ReceivingAccountName { get; set; }
        /// <summary>
        /// Transfer Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Type OF ID i.e. account Number or Phone Number
        /// </summary>
        public InArgument<bool>  IsPhoneNumber { get; set; }
        /// <summary>
        /// Specifies if the transfer is a cash transfer from Agent's phone
        /// </summary>
        public InArgument<bool> IsFromAgent { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string pin = PIN.Get(context);
            try
            {
                pin = HSMCenter.GeneratePinBlock(mob.MobilePhone, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string amount = Amount.Get(context);
           
            string phoneNumber = Acc_PhoneNumber.Get(context);
            string accountNumber = FromAccountNumber.Get(context);
            string recevingAccountName= ReceivingAccountName.Get(context);
            
           string instCode = string.Empty;
           if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;

                mob.InstitutionCode = instCode;
            }

            ILocalFundsTransferTransactionType tranType = new LocalFundsTransferTransactionType();
            tranType.isPhoneNumber = IsPhoneNumber.Get(context);
            tranType.Amount = Convert.ToDecimal(amount)*100;
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            tranType.ToAccount = phoneNumber;
            tranType.ToInstCode = mob.InstitutionCode; // Will change wen we start inter-MFB Trx
            tranType.ReceivingAccountName = recevingAccountName;
            
           //tranType.
            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, phoneNumber, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();
            }
            Notification.Set(context, response.ResponseMessage);
            //TODO: Do Balance Enquiry
            //string balance = TransactionSystem.RunTransaction(pin, accountNumber);

            

        }
    }

}
