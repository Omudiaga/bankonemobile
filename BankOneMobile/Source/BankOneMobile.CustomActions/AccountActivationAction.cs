﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(AccountActivationDesigner))]
    public class AccountActivationAction : ActionNode
    {
        /// <summary>
        /// User Activation Code
        /// </summary>
        public InArgument<string> ActivationCode {
            get
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }
        }

        /// <summary>
        /// New PIN entered the first Time
        /// </summary>
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> NewPIN1
        {
            get
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    return activity.NewPIN1;
                }
                return null;
            }
            set
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    activity.NewPIN1 = value;
                }
            }
        }
        public InArgument<string> NewPIN2
        {
            get
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    return activity.NewPIN2;
                }
                return null;
            }
            set
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    activity.NewPIN2 = value;
                }
            }
        }
        public OutArgument<string> Response
        {
            get
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    return activity.Response;
                }
                return null;
            }
            set
            {
                AccountActivationActivity activity = this.InternalState as AccountActivationActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }

        public AccountActivationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new AccountActivationActivity();
        }

    }

    public class AccountActivationActivity : CodeActivity
    {
        /// <summary>
        /// Activation Code
        /// </summary>
        public InArgument<string> ActivationCode { get; set; }
       
        public InArgument<string> NewPIN1 { get; set; }
        /// <summary>
        /// New PIN entered the second time
        /// </summary>
        public InArgument<string> NewPIN2 { get; set; }
        /// <summary>
        /// Succesful or Failed Response
        /// </summary>
        public OutArgument<string> Response { get; set; }
        
        

        protected override void Execute(CodeActivityContext context)
        {
            string activationCode = ActivationCode.Get(context);
            string pin1 = NewPIN1.Get(context);
            string pin2 = NewPIN2.Get(context);

            

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            if (mob.MobileAccountStatus == MobileAccountStatus.AwaitingApproval)
            {
                throw new FailureException("01","Account Linking has not yet been approved");
            }
            string phoneNumber = mob.MobilePhone;


            if (mob.ActivationCode != activationCode)// Do pin validation on activation code
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidActivationCode), Utilities.GetDescription(ErrorCodes.InvalidActivationCode));
            }

            if (pin1 != pin2)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PinEntriesDoNotMatch), Utilities.GetDescription(ErrorCodes.PinEntriesDoNotMatch));

            }
            //TODO: Do Balance Enquiry
            
            
           
            

            IPinChangeTransactionType tranType = new PinChangeTransactionType();
            
          
           

            tranType.NewPIN = pin1;
            tranType.TheMobileAccount = mob;


           // mob.PIN = pin1;
            //mob.IsActive = true;
            //mob.MobileAccountStatus=MobileAccountStatus.Active;

            ChangePin(mob, activationCode,pin1, sessionID, currentWorkflowInfo);
           // mob.MobileAccountStatus = MobileAccountStatus.Active;
           // new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).UpdateMobileAccount(mob);
                //Response.Set(context, "Account Activation Succesful");
            
            
            


            //TODO  Encrypt the values
           // TransactionResponse response = new TransactionSystem().DoAccountActivation(tranType);
            
            //Response.Set(context, response.ResponseMessage);

            //Account activation was successful! Welcome to Bank One!
            //An error occured while activating your account. Dial 014406018 to try again
            Response.Set(context, "success");

            

        }
        public void ChangePin(MobileAccount mob, string activationCode, string newPin,string sessionID, WorkflowInfo currentWorkFlowInfo)
        {
            try
            {
                activationCode = HSMCenter.GeneratePinBlock(mob.MobilePhone, activationCode, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }


            ITransaction trans = new Transaction();
            IPinChangeTransactionType tranType = new PinChangeTransactionType();

            // ta.EncryptedPIN = pin1;
            tranType.NewPIN = HSMCenter.GeneratePinBlock(mob.MobilePhone, newPin, AppZone.HsmInterface.PinBlockFormats.ANSI);

            trans.FromInstitutionCode = mob.InstitutionCode;
           
            trans.From = mob.BankAccounts[0].BankAccount; //mob.RecievingBankAccount.BankAccount;
            try
            {
                new TransactionSystem(currentWorkFlowInfo.DataSource).RunTransaction(activationCode, tranType, mob, trans.From, string.Empty, 0.0M, sessionID);
                // (pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem(currentWorkFlowInfo.DataSource).StopSession(sessionID, SessionStatus.EndedNormally, currentWorkFlowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //This is CRIMINAL... Closing other thread's sessions
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
        }
    }


}
