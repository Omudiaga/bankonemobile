﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CustomerPinEnquiryDesigner))]
    public class CustomerPinEnquiryAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {

                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber
        {
            get
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {

                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        /// <summary>
        /// Response ISO Transaction
        /// </summary>
        public OutArgument<string> StatusResponse
        {
            get
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {
                    return activity.StatusResponse;
                }
                return null;
            }
            set
            {
                CustomerPinEnquiryActivity activity = this.InternalState as CustomerPinEnquiryActivity;
                if (activity != null)
                {
                    activity.StatusResponse = value;
                }
            }
        }

        public CustomerPinEnquiryAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CustomerPinEnquiryActivity();
        }

    }

    public class CustomerPinEnquiryActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        public InArgument<string> PhoneNumber { get; set; }

        public OutArgument<string> StatusResponse { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string phoneNumber = PhoneNumber.Get(context);
            string pin = PIN.Get(context);
            MobileAccount mob = new MobileAccountSystem().GetByPhoneNumber(phoneNumber);// currentWorkflowInfo.InitiatedBy as MobileAccount;

            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("ABOUT TO VALIDATE CUSTOMER PIN - PIN:{0},PHONENUMBER:{1}", pin, phoneNumber)));
            pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            byte[] encryptedPinBytes = new byte[8];
            if (!string.IsNullOrEmpty(pin))
            {
                ThalesSim.Core.Utility.HexStringToByteArray(pin, ref encryptedPinBytes);
            }

            string response = string.Empty;


            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().DoLinkedAccountEnquiry(mob, encryptedPinBytes);
                if (response != "00")
                {
                    throw new FailedTransactionException(response);
                }
                else
                {
                    StatusResponse.Set(context, "true");
                }
            }
            catch (NoSwitchResponseException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                StatusResponse.Set(context, ex.Message);
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }

            catch (Exception ex)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.SystemError));
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {

                //


            }



        }
    }
}
