﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    /// <summary>
    /// This custom action is for Diamond Bank Target Savings
    /// </summary>
    [Designer(typeof(GetAccountBalanceDesigner))]
    public class GetAccountBalanceAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> CustomerAccountNumber
        {
            get
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {

                    return activity.CustomerAccountNumber;
                }
                return null;
            }
            set
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    activity.CustomerAccountNumber = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {

                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Balance
        {
            get
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    return activity.Balance;
                }
                return null;
            }
            set
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    activity.Balance = value;
                }
            }
        }
        /// <summary>
        /// Available Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> AvailableBalance
        {
            get
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    return activity.AvailableBalance;
                }
                return null;
            }
            set
            {
                GetAccountBalanceActivity activity = this.InternalState as GetAccountBalanceActivity;
                if (activity != null)
                {
                    activity.AvailableBalance = value;
                }
            }
        }


        public GetAccountBalanceAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetAccountBalanceActivity();
        }

    }

    public class GetAccountBalanceActivity : CodeActivity
    {
        /// <summary>
        /// Entered Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        public OutArgument<string> CustomerAccountNumber { get; set; }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        /// 
        public OutArgument<string> AvailableBalance { get; set; }
        /// <summary>
        /// Available Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Balance { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            Institution inst = null;
            //   pin = ThalesSim.Core.Utility.HexStringToByteArray(pin,);
            string custPhoneNumber = PhoneNumber.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string custAcct = CustomerAccountNumber.Get(context);
            IBalanceInquiryTransactionType tranType = new BalanceInquiryTransactionType();

            if (!string.IsNullOrEmpty(custAcct))
            {
                tranType.IsForThirdParty = true;
                accountNumber = custAcct;
            }


            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                if (vals.Count > 0)
                {
                    try
                    {
                        Agent ag;
                        if (vals.Count() > 1)
                        {
                            inst = new InstitutionSystem(currentWorkflowInfo.DataSource).GetByShortName(currentWorkflowInfo.DataSource, vals[1]) as Institution;
                            instCode = inst.Code;
                            mob.InstitutionCode = instCode;
                            //Check if agent
                            if (!mob.IsAgentAccount)
                            {
                                // if not agent
                                //Then its customer doing self service
                                //Use self service serviceCode
                                tranType.IsForSelfService = true;
                            }
                            else
                            {
                                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                                // If agent
                                //check if agent accoun number is selected acct no
                                if (accountNumber == ag.TheAgentAccount.BankAccount)
                                {
                                    // if yes use teller service code
                                    //service code we have been using all this while
                                }
                                else
                                {
                                    //then its agent doing self service.
                                    // else use self service ServiceCode
                                    tranType.IsForSelfService = true;
                                }
                            }
                        }
                        else
                        {
                            ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                            if (ag == null)
                            {
                                throw new TransactionNotPermittedToNonAgentsException("Non -Agent Initiated Trnsactions");
                            }
                            instCode = ag.TheAgentAccount.InstitutionCode;
                            mob.InstitutionCode = instCode;
                        }
                    }
                    catch (InvalidInstitutionException)
                    {
                        throw new FailureException("51", "Your Institution is not set up for Mobile Transactions");
                    }
                }
                else
                {
                    // inst= 
                }
                mob.InstitutionCode = instCode;
            }

            LinkingBankAccount bankAccount = null;
            MobileAccount mobile = null;
            TransactionResponse response = null;
            var balance = new BankOneMobile.Services.CoreBankingService.AccountBalance();
            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("about to get mobile account by phone: {0}", custPhoneNumber)));
                mobile = new MobileAccountSystem().GetByPhoneNumber(custPhoneNumber); //client.GetAccountByAccountNumber(phoneNumber) as LinkingBankAccount;
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten mobile account by phone: {0}", mobile == null ? "null" : mobile.CustomerID)));
                //mobile = bankAccount == null ? null : bankAccount.TheMobileAccount as MobileAccount;
                if (mobile == null)
                {
                    throw new Exception("Phone number is not tied to any BETA account. A BETA account with this phone number is required to proceed");
                }
                else
                {
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten mobile account. Number of Bank accounts: {0}", mobile.BankAccounts == null ? "null" : mobile.BankAccounts.Count.ToString())));
                    bankAccount = mobile.BankAccounts.First() as LinkingBankAccount;
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten bank account: {0}", bankAccount == null ? "null" : bankAccount.BankAccount)));
                    using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new BankOneMobile.Services.CoreBankingService.SwitchingServiceClient())
                    {
                        balance = client.GetBalance(mob.InstitutionCode, bankAccount.BankAccount);
                    }
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten accountbalance: {0}", balance == null ? "null" : balance.LedgerBalance.ToString())));
                    if (balance == null)
                    {
                        throw new InvalidPhoneNumberException("Could not retrieve account balance");
                    }
                    if ((balance.LedgerBalance / 100) < 5000)
                    {
                        throw new InsufficientFundsException("Minimum balance required is 5,000. Kindly fund your BETA account to proceed");
                    }
                }

                // response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (InsufficientFundsException ex)
            {
                System.Diagnostics.Trace.TraceError("Minimum balance required is 5,000. Kindly fund your BETA account to proceed");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw;
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }

            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            CustomerAccountNumber.Set(context, bankAccount.BankAccount);
            Balance.Set(context, balance.LedgerBalance.ToString());//set ledger balance 
            AvailableBalance.Set(context, balance.AvailableBalance.ToString());//Set available balance


        }
    }
}
