﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using System.Xml.Linq;

namespace BankOneMobile.CustomActions
{
    
    public class PhoneOrAccountNumberScreen : ConfirmScreen
    {
        
       
         

        public PhoneOrAccountNumberScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new CustomScreenActivity();
        }

        static PhoneOrAccountNumberScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(ConfirmScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(ConfirmationText);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                }
            }
            else
            {
                result.AppendLine(ConfirmationText);
            }
            this.Response.Set(context, DetermineEntry(this.Response.Get(context)));
           // result.AppendLine("y: yes");
           // result.AppendLine("n: no");
            return result.ToString();
        }

        public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
            {
                if (context != null)
                {
                    node.SetValue(textToAdd + ConfirmationText);
                }
                else
                {
                    node.SetValue(textToAdd + "<Dynamic Text>");
                }
            }
            else
            {
                node.SetValue(textToAdd + ConfirmationText);
            }
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
            //xmlResponseTree.Add(new XElement("ShouldClose", new XAttribute("Close", false), 0));
            this.Response.Set(context, DetermineEntry(this.Response.Get(context)));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            string option = Convert.ToString(response);
            if (option == null || !(option.Trim().Equals("y", StringComparison.CurrentCultureIgnoreCase) || option.Trim().Equals("n", StringComparison.CurrentCultureIgnoreCase)))
            {
                currentWorkflowInfo.DisplayMessage = "Unknown option. Enter either y or n\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown option. Enter either y or n<br/>");
                //  currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //  currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                string lookupString = "no";
                if (option.Trim().Equals("y", StringComparison.CurrentCultureIgnoreCase))
                {
                    lookupString = "yes";
                }
                Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == lookupString);
                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }

        public string DetermineEntry(string value)
        {
           if (value.Length == 11 && (value.StartsWith("080") || value.StartsWith("070")))
            {
                return "y: yes";
            }
            else if (value.Length == 17)
            {
                return "n: no";
            }
            
            else return "account";
        }
       
    }

  
}
