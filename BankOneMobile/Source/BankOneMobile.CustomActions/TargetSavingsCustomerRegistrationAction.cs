﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(TargetSavingsCustomerRegistrationDesigner))]
    public class TargetSavingsCustomerRegistrationAction : ActionNode
    {
        /// <summary>
        /// Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
       
        /// <summary>
        ///Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public InArgument<string> Tenure
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Tenure;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Tenure = value;
                }
            }
        }
        /// <summary>
        ///Frequency
        /// </summary>
        public InArgument<string> Frequency
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Frequency;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Frequency = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }

        /// <summary>
        ///Standing Order
        /// </summary>
        public InArgument<string> StandingOrder
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.StandingOrder;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.StandingOrder = value;
                }
            }
        }
        public InArgument<bool> IsReapplying
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.IsReapplying;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.IsReapplying = value;
                }
            }
        }

        public InArgument<string> TargetSavingsAccountNumberForReactivation
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.TargetSavingsAccountNumberForReactivation;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.TargetSavingsAccountNumberForReactivation = value;
                }
            }
        }

        public InArgument<Product> Product
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }

        /// <summary>
        ///Target savings account number
        /// </summary>
        public OutArgument<string> TargetSavingsAccountNumber
        {
            get
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.TargetSavingsAccountNumber;
                }
                return null;
            }
            set
            {
                TargetSavingsCustomerRegistrationActivity activity = this.InternalState as TargetSavingsCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.TargetSavingsAccountNumber = value;
                }
            }
        }
        
         
        public TargetSavingsCustomerRegistrationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new TargetSavingsCustomerRegistrationActivity();
        }

    }

    public class TargetSavingsCustomerRegistrationActivity : CodeActivity
    {
        private const string CONFIRMATION_TEXT = "Target Savings account has successfully been created. Please notify them of this Account number:";
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }

        /// <summary>
        /// Tenure
        /// </summary>
        public InArgument<string> Tenure { get; set; }

        /// <summary>
        /// Frequency
        /// </summary>
        public InArgument<string> Frequency { get; set; }
        /// <summary>
        /// Standing order
        /// </summary>
        public InArgument<string> StandingOrder { get; set; }
        public InArgument<Product> Product { get; set; }
        /// <summary>
        /// Is the customer Reapplying?
        /// </summary>
        public InArgument<bool> IsReapplying { get; set; }
        /// <summary>
        /// TargetSavingsAccountNumber to reactivate if the customer is Reapplying
        /// </summary>
        public InArgument<string> TargetSavingsAccountNumberForReactivation { get; set; }
        public OutArgument<string> TargetSavingsAccountNumber { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
             
            string pin = PIN.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string frequency = Frequency.Get(context);
            string customerPhone = CustomerPhoneNumber.Get(context);
            string amount = Amount.Get(context);
            string standingOrder = StandingOrder.Get(context);
            //string tenure = Tenure.Get(context);
            bool isReapplying = IsReapplying.Get(context);
            string targetSavingsAccountNumberForReactivation = TargetSavingsAccountNumberForReactivation.Get(context);
            Product product = Product.Get(context);

            
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            //if(int.Parse(tenure) <= 0)
            //{
            //    throw new Exception("Tenure cannot be less than or equal to 0");
            //}

            ITargetSavingsRegistrationTransactionType tranType = new TargetSavingsRegistrationTransactionType();
            tranType.AccountNumber = accountNumber;
            tranType.PhoneNumber = customerPhone;
            tranType.StandingOrder = standingOrder;
            tranType.TargetSavingsProductTenure = product.Tenure;
            tranType.IsReapplying = isReapplying;
            tranType.TargetSavingsAccountNumberForReactivation = targetSavingsAccountNumberForReactivation;
            tranType.SelectedProduct = product;
            
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("frequency:{0},standingOrder:{1},tenure:{2},amount:{3}", frequency, standingOrder, product.Tenure,amount)));
            if (standingOrder == "Yes")
            {
                if (long.Parse(amount) <= 0)
                {
                    throw new Exception("Amount cannot be less than or equal to 0");
                }
                tranType.Amount = amount;
                tranType.Frequency = frequency;
                decimal amt = decimal.Parse(amount);
                if (frequency == "Daily")
                {
                    tranType.TotalCommitmentAmount = amt * product.Tenure;
                }
                else if (frequency == "Weekly")
                {
                    tranType.TotalCommitmentAmount = amt * (product.Tenure / 5);
                }
                else if (frequency == "Monthly")
                {
                    tranType.TotalCommitmentAmount = amt * (product.Tenure / 30);
                }
                if(tranType.TotalCommitmentAmount > 200000)
                {
                    //Services.CoreBankingService.Account account= null;
                    //using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new Services.CoreBankingService.SwitchingServiceClient())
                    //{
                    //    account = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumber);
                    //}
                    //if(account.AccountTierID == null || account.AccountTierID != 3)
                    //{
                    //    throw new Exception("Savings Goal above =N=200,000 requires KYC Upgrade.");
                    //}
                }
            }
            else
            {
                tranType.Amount= "0";
                tranType.Frequency = "Daily";
                tranType.TotalCommitmentAmount = 0;
            }
            
            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, mob.MobilePhone, mob.MobilePhone, 0.0M, sessionID);// use 0 cos we dont really care about amount in balance enquiry
            }
            catch (TransactionNotPermittedToNonAgentsException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (AlreadyRegisterdCustomerException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberAlreadyRegistered), Utilities.GetDescription(ErrorCodes.PhoneNumberAlreadyRegistered));
            }
            catch (AlreadyExistingAccountException ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.AccountNumberAlreadyExists), Utilities.GetDescription(ErrorCodes.AccountNumberAlreadyExists));
            }
            catch (CoreBankingWebServiceException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), Utilities.GetDescription(ErrorCodes.UnableToConnectToCoreBanking));
            }
            catch (NoHSMResponseException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during transaction.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
            }
            finally
            {


                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                //if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                //{
                //   // new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally);

                //}
                //if (File.Exists(filename))
                //{
                //    System.Diagnostics.Trace.TraceInformation("Deleting {0} image...", filename);
                //    File.Delete(filename);
                //}

            }

            TargetSavingsAccountNumber.Set(context, response.ResponseMessage);
            

        }
    }

}
