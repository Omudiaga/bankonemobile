﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetRechargePaymentItemsDesigner))]
    public class GetRechargePaymentItemsAction : ActionNode
    {
      
       

        public OutArgument<IList<PaymentItem>> RechargePaymentItems
        {
            get
            {
                GetRechargePaymentItemsActivity activity = this.InternalState as GetRechargePaymentItemsActivity;
                if (activity != null)
                {
                    return activity.RechargePaymentItems ;
                }
                return null;
            }
            set
            {
                GetRechargePaymentItemsActivity activity = this.InternalState as GetRechargePaymentItemsActivity;
                if (activity != null)
                {
                    activity.RechargePaymentItems = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetRechargePaymentItemsActivity activity = this.InternalState as GetRechargePaymentItemsActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetRechargePaymentItemsActivity activity = this.InternalState as GetRechargePaymentItemsActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        public GetRechargePaymentItemsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetRechargePaymentItemsActivity();
        }

    }

    public class GetRechargePaymentItemsActivity : CodeActivity
    {


        public OutArgument<IList<PaymentItem>> RechargePaymentItems { get; set; }
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<Merchant> SelectedMerchant { get; set; }
        

        protected override void Execute(CodeActivityContext context)
        {
            string enteredPhoneNo = PhoneNumber.Get(context);
            //Merchant merch = SelectedMerchant.Get(context);

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            string phoneNumber = mob.MobilePhone;
            if (enteredPhoneNo != "1")
            {
                phoneNumber = enteredPhoneNo;
            }

            Merchant merch = new MerchantSystem().GetRechargeMerchantByPhoneNumber(phoneNumber);
            IList<PaymentItem> items = new MerchantSystem().GetPaymentItemsByMerchant(merch);
            RechargePaymentItems.Set(context, items);
            


        }
    }
}
