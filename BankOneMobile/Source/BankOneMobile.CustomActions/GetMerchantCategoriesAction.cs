﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetMerchantCategoriesDesigner))]
    public class GetMerchantCategoriesAction : ActionNode
    {
      
       

        /// <summary>
        /// List of Products
        /// </summary>
        public OutArgument<IList<MerchantCategory>> MerchantCategories
        {
            get
            {
                GetMerchantCategoriesActivity activity = this.InternalState as GetMerchantCategoriesActivity;
                if (activity != null)
                {
                    return activity.MerchantCategories;
                }
                return null;
            }
            set
            {
                GetMerchantCategoriesActivity activity = this.InternalState as GetMerchantCategoriesActivity;
                if (activity != null)
                {
                    activity.MerchantCategories = value;
                }
            }
        }

        public GetMerchantCategoriesAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetMerchantCategoriesActivity();
        }

    }

    public class GetMerchantCategoriesActivity : CodeActivity
    {


        public OutArgument<IList<MerchantCategory>> MerchantCategories { get; set; }


        protected override void Execute(CodeActivityContext context)
        {

            IList<MerchantCategory> cat = null;
            try
            {
                cat = new MerchantSystem().GetMerchantCategories();
            }
            catch (FaultException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), Utilities.GetDescription(ErrorCodes.ISWGatewayUnavailable));
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), "Error on ISW Gateway");
            }
            
            MerchantCategories.Set(context, cat);
            


        }
    }
}
