﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetProductsDesigner))]
    public class CheckTransactionStatusAction : ActionNode
    {
      
       

        /// <summary>
        /// List of Products
        /// </summary>
        public OutArgument<Boolean> TransactionSuccessful
        {
            get
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.IsSuccessfulTransaction;
                }
                return null;
            }
            set
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    activity.IsSuccessfulTransaction = value;
                }
            }
        }

        public OutArgument<Boolean> TransactionCompleted
        {
            get
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.IsCompleted;
                }
                return null;
            }
            set
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    activity.IsCompleted = value;
                }
            }
        }
        public OutArgument<String> Completed
        {
            get
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.StrCompleted;
                }
                return null;
            }
            set
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    activity.StrCompleted = value;
                }
            }
        }
        public OutArgument<String> FailureReason
        {
            get
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.StrFailureReason;
                }
                return null;
            }
            set
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    activity.StrFailureReason = value;
                }
            }
        }
        public OutArgument<String> Successful
        {
            get
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.StrSuccesful;
                }
                return null;
            }
            set
            {
                CheckTransactionStatusActivity activity = this.InternalState as CheckTransactionStatusActivity;
                if (activity != null)
                {
                    activity.StrSuccesful = value;
                }
            }
        }

        public CheckTransactionStatusAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CheckTransactionStatusActivity();
        }

    }

    public class CheckTransactionStatusActivity : CodeActivity
    {


        public OutArgument<Boolean> IsSuccessfulTransaction { get; set; }
        public OutArgument<Boolean> IsCompleted { get; set; }
        public OutArgument<string> StrCompleted { get; set; }
        public OutArgument<string> StrSuccesful { get; set; }
        public OutArgument<string> StrFailureReason { get; set; }
        

        protected override void Execute(CodeActivityContext context)
        {
           

            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

                //if (currentWorkflowInfo.HasPreviousWorkflow)
                {
                    bool succes = currentWorkflowInfo.ChildTransactionSuccessful;
                    bool complete = currentWorkflowInfo.ChildTransactionCompleted;
                    IsSuccessfulTransaction.Set(context,succes );
                    IsCompleted.Set(context,complete );
                    StrSuccesful.Set(context,succes.ToString().ToLower());
                    StrCompleted.Set(context, complete.ToString().ToLower());
                    StrFailureReason.Set(context, currentWorkflowInfo.FailureReason);
                }
           
        }
    }
}
