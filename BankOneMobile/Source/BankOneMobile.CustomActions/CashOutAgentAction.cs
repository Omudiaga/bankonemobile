﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using System.Diagnostics;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CashOutAgentDesigner))]
    public class CashOutAgentAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }

        /// <summary>
        ///Customer Account Number
        /// </summary>
        public InArgument<string> CustomerAccountNumber
        {
            get
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerAccountNumber;
                }
                return null;
            }
            set
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    activity.CustomerAccountNumber = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    return activity.Amount;
                        
                }
                return null;
            }
            set
            {
                CashOutAgentActivity activity = this.InternalState as CashOutAgentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }


        public OutArgument<string> TransactionSuccessful
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.TransactionSuccesful;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.TransactionSuccesful = value;
                }
            }
        }

        public CashOutAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CashOutAgentActivity();
        }

    }

    public class CashOutAgentActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        
        
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Customer Account Number
        /// </summary>
        public InArgument<string> CustomerAccountNumber { get; set; }
        /// <summary>
        /// Notification
        /// </summary>
        public OutArgument<string> Notification { get; set; }

        public OutArgument<string> TransactionSuccesful { get; set; }
        

        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string acctNo = CustomerAccountNumber.Get(context);
            string pin = PIN.Get(context);
            string custAcct = string.Empty;
            string custInstCode = string.Empty;
            IInstitution theInst = null;
            ICashOutTransactionType tranType = new CashOutTransactionType();
             Agent ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (ag == null)
            {
                throw new InvalidAgentCodeException("Invalid Agent Exception");
            }
            string instCode = ag.TheAgentAccount.InstitutionCode;
            try
            {
                pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string customerPhone = CustomerPhoneNumber.Get(context);
            tranType.CustomerPhoneNumber = customerPhone;
            MobileAccount custMob =new MobileAccountSystem(currentWorkflowInfo.DataSource).GetByPhoneNumber(customerPhone);
            if (custMob != null)
            {
                List<ILinkingBankAccount> bankaccts = custMob.BankAccounts.Where(x => x.BankStatus != BankAccountStatus.InActive).ToList();
                if (bankaccts != null && bankaccts.Count > 0 && bankaccts[0] != null)
                {
                    tranType.CustomerAccount =  bankaccts[0].BankAccount;
                }
                else
                {
                    Trace.TraceInformation("Bank Accounts is Null");
                }
            }
            else
            {
                Trace.TraceInformation("Customer Phone is Null");
            }
           
            if(!string.IsNullOrEmpty(acctNo))
            {
                string[] actInst = acctNo.Split(":".ToCharArray());
                acctNo = actInst[0];
                custInstCode = actInst[1];
                theInst = new InstitutionSystem().GetByShortName(DataSourceFactory.GetDataSource(DataCategory.Shared), custInstCode);
                tranType.CustomerAccount = string.IsNullOrEmpty(acctNo) ? tranType.CustomerAccount : acctNo;


            }
           
            string amount = Amount.Get(context);

            

            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            
            if (customerPhone == null)
            {
                customerPhone = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;


            }
            else
            {
               // List<String> vals = customerPhone.Split(':').ToList();//Leave all this commented Cde. Will be useful for USSD TRX
                //customerPhone = vals[0];
                
               // instCode = new InstitutionSystem().GetByShortName(DataSourceFactory.GetDataSource(DataCategory.Shared),instCode ).Code;
                mob.InstitutionCode = instCode;
            }
            
           
            //string agentPhone = currentWorkflowInfo.ThePreviousWorkflow.InitiatedBy.MobilePhone;
           // Agent theAgent = new AgentSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAgentByPhoneNumber(agentPhone) as Agent;
            tranType.Amount = Convert.ToDecimal(amount) * 100;
            
            tranType.Token = mob.InstitutionCode;// just save it her cos we need it
            
            
            tranType.Amount = Convert.ToDecimal(amount);

            tranType.Token = theInst==null? mob.InstitutionCode:theInst.Code;//mob.InstitutionCode;// just save it here cos we need it
            Trace.TraceInformation("Sending Trx - {0}", tranType.CustomerPhoneNumber);
            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, customerPhone, ag.TheAgentAccount.BankAccount, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (OnlyDepositsAllowsException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.OnlyDepositTransactionsAreAllowedOnThisAccount), Utilities.GetDescription(ErrorCodes.OnlyDepositTransactionsAreAllowedOnThisAccount));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (NonAgentInitiatedTransaction)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotInitiatedByAgent), Utilities.GetDescription(ErrorCodes.TransactionNotInitiatedByAgent));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionFailed),ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                
            }
            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP......CRIMINAL.....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
                       
            //string result = TransactionSystem.RunTransaction(pin);


            //context.SetValue(Notification, response.ResponseMessage);
        }
    }

}
