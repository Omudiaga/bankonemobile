﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using FluentNHibernate;
using System.Reflection;
using FluentNHibernate.Automapping;
using BankOneMobile.Core.Helpers;
using FluentNHibernate.Conventions;
using BankOneMobile.Data.NHibernate.FluentNHibernate.Conventions;
using System;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.Data.NHibernate
{
	/// <summary>
	/// Provides a standard interface for managing session storage
	/// </summary>
	public interface ISessionStorage
	{
		ISession Session { get; set; }
	}

	/// <summary>
	/// Basic implementation of <see cref="ISessionStorage"/> using in-memory storage
	/// </summary>
	public class SimpleSessionStorage : ISessionStorage
	{
		public ISession Session { get; set; }
	}

	/// <summary>
	/// Main class for setting up and initializing the NHibernate Configurations, Settings, SessionFactories, ...
	/// </summary>
	public static class NHibernateSession
	{
		#region Init() overloads

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, null, null, null, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, string cfgFile, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, null, cfgFile, null, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, IDictionary<string, string> cfgProperties, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, null, null, cfgProperties, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, string cfgFile, string validatorCfgFile, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, null, cfgFile, null, validatorCfgFile, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, autoPersistenceModel, null, null, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, string cfgFile, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, autoPersistenceModel, cfgFile, null, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, IDictionary<string, string> cfgProperties, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, autoPersistenceModel, null, cfgProperties, null, dataSource);
		}

		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, string cfgFile, string validatorCfgFile, IDataSource dataSource)
		{
			return Init(storage, mappingAssemblies, autoPersistenceModel, cfgFile, null, validatorCfgFile, dataSource);
		}

		#endregion

		private static readonly Dictionary<string, ISessionFactory> SessionFactories =
			new Dictionary<string, ISessionFactory>();

		private static readonly Dictionary<string, ISessionStorage> SessionStorages =
			new Dictionary<string, ISessionStorage>();

		private static readonly Dictionary<string, Dictionary<Guid, ISessionStorage>> WorkflowSessionStorages =
			new Dictionary<string, Dictionary<Guid, ISessionStorage>>();


		public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, string cfgFile, IDictionary<string, string> cfgProperties, string validatorCfgFile, IDataSource dataSource)
		{
			//Pre-condition checks
			Check.Require(storage != null, "storage mechanism was null but must be provided");
			Check.Require(dataSource != null, "A unique key for the session factory was null, but must be provided.");

			
			try
			{
				lock (SessionFactories)
				{
					if (!SessionFactories.ContainsKey(dataSource.FactoryKey))
					{
						Configuration cfg = ConfigureNHibernate(cfgFile, cfgProperties);
						ConfigureNHibernateValidator(cfg, validatorCfgFile);
						AddMappingAssembliesTo(cfg, mappingAssemblies, autoPersistenceModel);

#if DEBUG
						//Auto-update the Database with missing Tables/Columns
						new global::NHibernate.Tool.hbm2ddl.SchemaUpdate(cfg).Execute(false, true);
#endif

						// SessionFactory = cfg.BuildSessionFactory();
						//Storage = storage;

						SessionFactories.Add(
							  dataSource.FactoryKey,
							  cfg.BuildSessionFactory());

						SessionStorages.Add(
							  dataSource.StorageKey, storage
							  );
						return cfg;
					}
				}
			}
			catch (HibernateException ex)
			{
				throw new HibernateException("Cannot create session factory.", ex);
			}
			return null;
		}

		private static void AddAutoMappingsTo(Configuration cfg, AutoPersistenceModel autoPersistenceModel) {
			if (autoPersistenceModel != null) {
				cfg.AddAutoMappings(autoPersistenceModel);
			}
		}

		/// <summary>
		/// Register an interceptor to be used with the NHibernate SessionFactory
		/// </summary>
		/// <param name="interceptor"></param>
		public static void RegisterInterceptor(IInterceptor interceptor) {
			Check.Require(interceptor != null, "interceptor may not be null");
			RegisteredInterceptor = interceptor;
		}

		/// <summary>
		/// The NHibernate SessionFactory
		/// </summary>
		public static ISessionFactory SessionFactory { get; set; }

		/// <summary>
		/// The Session Storage Object
		/// </summary>
		public static ISessionStorage Storage { get; set; }

		public static void Close(IDataSource dataSource)
		{
			ISessionStorage storage = null;
			if (dataSource.WorkflowId == Guid.Empty)
			{
				if (SessionStorages.ContainsKey(dataSource.StorageKey))
				{
					storage = SessionStorages[dataSource.StorageKey];
				}
			}
			else
			{
				if (WorkflowSessionStorages.ContainsKey(dataSource.FactoryKey))
				{
					var workflowSessionStorage = WorkflowSessionStorages[dataSource.FactoryKey];
					if (workflowSessionStorage.ContainsKey(dataSource.WorkflowId))
					{
						storage = workflowSessionStorage[dataSource.WorkflowId];
					}   
				}
				
			}
			if (storage != null)
			{
				//Get the current session from the storage object
				ISession session = storage.Session;
				if (session != null && session.IsOpen)
				{
					//try
					//{
					//    if (session.Transaction != null && session.Transaction.IsActive && !session.Transaction.WasCommitted && !session.Transaction.WasRolledBack)
					//    {
					//        session.Transaction.Commit();
					//        session.Flush();
					//    }
					//}
					//catch
					//{
					//}
					//finally
					//{
					//    session.Close();
					//}
					session.Close();
					
				}
				storage.Session = null;
			}
		}
		/// <summary>
		/// The Current NHibernate Session
		/// </summary>

		public static ISession Current(IDataSource dataSource)
		{
			ISessionStorage storage = null;
			if (dataSource.WorkflowId == Guid.Empty)
			{
				if (SessionStorages.ContainsKey(dataSource.StorageKey))
				{
					storage = SessionStorages[dataSource.StorageKey];
				}
			}
			else
			{
				if (!WorkflowSessionStorages.ContainsKey(dataSource.FactoryKey))
				{
					Dictionary<Guid, ISessionStorage> workflowStorage = new Dictionary<Guid, ISessionStorage>();
					workflowStorage.Add(dataSource.WorkflowId, new WorkflowSessionStorage(dataSource));
					try
					{
						WorkflowSessionStorages.Add(dataSource.FactoryKey, workflowStorage);
					}
					catch(ArgumentException)
					{
						//An item with the same key has already been added
						//One thread already beat you to it(added the Shared FactoryKey key to the dictionary).
						//So, be a good sport and move on!! Don't crash the app!!
					}
				}
				var workflowSessionStorage = WorkflowSessionStorages[dataSource.FactoryKey];
				if (!workflowSessionStorage.ContainsKey(dataSource.WorkflowId))
				{
					workflowSessionStorage.Add(dataSource.WorkflowId, new WorkflowSessionStorage(dataSource));
				}
				storage = workflowSessionStorage[dataSource.WorkflowId];
			}
			if (storage == null)
			{
				storage = new SimpleSessionStorage();
				SessionStorages.Add(dataSource.StorageKey, storage);
			}

			//Get the current session from the storage object
			ISession session = storage.Session;

			//Start a new session if no current session exists
			if (session == null|| !session.IsOpen)
			{
				if (!SessionFactories.ContainsKey(dataSource.FactoryKey))
				{
					AutoPersistenceModel autoPersistenceModel = new AutoPersistenceModel();
					autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
					autoPersistenceModel.Conventions.Add<HasManyConvention>();
					autoPersistenceModel.Conventions.Add<ReferencesConvention>();

					IDictionary<string, string> cfgProperties = new Dictionary<string, string>();

					//if (dataSource.Type == DataCategory.Tenant && !String.IsNullOrWhiteSpace(dataSource.TheTenant.DataSourceConfig))
					//{
					//    foreach(string cfgPropValue in dataSource.TheTenant.DataSourceConfig.Split('|'))
					//    {
					//        string[] cfg = cfgPropValue.Split('~');
					//        if (cfg.Length == 2)
					//        {
					//            cfgProperties.Add(cfg[0], cfg[1]);
					//        }
					//    }
					//}


					//NHibernateSession.Init(dataSource.WorkflowId == Guid.Empty? new SimpleSessionStorage() as ISessionStorage: new WorkflowSessionStorage(dataSource) as ISessionStorage,
					//    new[] { @"C:\AppZone\CloudDlls\Messiah.DataAccess.NHibernate.TenantDB.dll" }, 
					//    autoPersistenceModel, cfgProperties,
					//    dataSource);
				}
				//Apply the interceptor if any was registered and open the session
				if (RegisteredInterceptor != null)
				{
					session = SessionFactories[dataSource.FactoryKey].OpenSession(RegisteredInterceptor);
				}
				else
				{
					session = SessionFactories[dataSource.FactoryKey].OpenSession();
				}

				//Begin a transaction
				if (storage is BankOneMobile.Data.NHibernate.Web.WebSessionStorage)
				{
					session.BeginTransaction();
				}
				//Update the storage with the new session
				storage.Session = session;
			}


			return session;
		}

		private static void AddMappingAssembliesTo(Configuration cfg, ICollection<string> mappingAssemblies, AutoPersistenceModel autoPersistenceModel) {
			Check.Require(mappingAssemblies != null && mappingAssemblies.Count >= 1,
				"mappingAssemblies must be provided as a string array of assembly names which contain mapping artifacts. " +
				"The artifacts themselves may be HBMs or ClassMaps.  You may optionally include '.dll' on the assembly name.");

			foreach (string mappingAssembly in mappingAssemblies) {
				string loadReadyAssemblyName = (mappingAssembly.IndexOf(".dll") == -1)
					? mappingAssembly.Trim() + ".dll"
					: mappingAssembly.Trim();

				Assembly assemblyToInclude = Assembly.LoadFrom(loadReadyAssemblyName);
				// Looks for any HBMs
				cfg.AddAssembly(assemblyToInclude);

				if (autoPersistenceModel == null) {
					// Looks for any Fluent NHibernate ClassMaps
					cfg.AddMappingsFromAssembly(assemblyToInclude);
				}
				else {
					autoPersistenceModel.AddMappingsFromAssembly(assemblyToInclude);
					cfg.AddAutoMappings(autoPersistenceModel);
				}
			}
			
		}

		/// <summary>
		/// Gets the Nhibernate configuration from the file and applies the properties specified to it
		/// </summary>
		/// <param name="cfgFile">The Nhibernate Configuration filepath</param>
		/// <param name="cfgProperties">Configuration properties to apply</param>
		/// <returns>Nhibernate Configuration Object</returns>
		private static Configuration ConfigureNHibernate(string cfgFile, IDictionary<string, string> cfgProperties) {
			Configuration cfg = new Configuration();

			if (string.IsNullOrEmpty(cfgFile))
			{
				cfg = cfg.Configure();
			}
			else
			{
				cfg = cfg.Configure(cfgFile);
			}

			if (cfgProperties != null)
			{
				foreach(string key in cfgProperties.Keys)
				{
					cfg.Properties.Remove(key);
				}
				cfg.AddProperties(cfgProperties);
			}

			return cfg;
		}

		private static void ConfigureNHibernateValidator(Configuration cfg, string validatorCfgFile) {
			/*ValidatorEngine engine = new ValidatorEngine();

			if (string.IsNullOrEmpty(validatorCfgFile))
				engine.Configure();
			else
				engine.Configure(validatorCfgFile);

			// Register validation listeners with the current NHib configuration
			ValidatorInitializer.Initialize(cfg, engine);*/
		}

		/// <summary>
		/// The interceptor to apply when a session is created
		/// </summary>
		private static IInterceptor RegisteredInterceptor;
	}

   
}

