﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using NHibernate;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.Data.NHibernate
{
    /// <summary>
    /// This has to implement <see cref="IDbContext" />, but there's no reason for it to be 
    /// recreated more than once.  Therefore, it's been setup as a threadsafe singleton.
    /// Singleton guidance from http://www.yoda.arachsys.com/csharp/singleton.html.
    /// </summary>
    public sealed class DbContext : IDbContext
    {
        private DbContext() { }

        /// <summary>
        /// Singleton Instance of <see cref="DbContext"/>
        /// </summary>
        public static DbContext Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested() { }
            internal static readonly DbContext instance = new DbContext();
        }

        private ISession Session(IDataSource dataSource)
        {
            return NHibernateSession.Current(dataSource);
        }


        /// <summary>
        /// This isn't specific to any one DAO and flushes everything that has been 
        /// changed since the last commit.
        /// </summary>
        public void CommitChanges(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if(Session(dataSource).IsOpen)
            {
                Session(dataSource).Flush();
            }
        }

        public void CloseSources(IList<IDataSource> sources)
        {
            foreach (IDataSource source in sources)
            {
                Close(source);
            }
        }
        public void Terminate()
        {
            IList<IDataSource> sources = new List<IDataSource>();

            IDataSource coreSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDataSource sharedSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
            sources.Add(coreSource);
            sources.Add(sharedSource);
            CloseSources(sources);

        }
        public void Close(IDataSource dataSource)
        {
            NHibernateSession.Close(dataSource);
        }

        public void BeginTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction == null || !Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).BeginTransaction();
            }
        }

        public void CommitTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction != null && Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).Transaction.Commit();
            }
        }

        public void RollbackTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction != null && Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).Transaction.Rollback();
            }
        }

        #region IDbContext Members


        public void StartAuditTrail()
        {
            throw new System.NotImplementedException();
        }

        public void EndAuditTrail()
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
