﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;
using System.Diagnostics;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class AgentRepository : Repository<IAgent>, IAgentRepository
    {
        public IAgent GetByCode(IDataSource dataSource,string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgent));
            criteria.Add(Restrictions.Eq("Code", code));
            return criteria.UniqueResult<IAgent>();
        }
        
        public IList< IAgent> GetUnlinked(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgent));
            criteria.Add(Restrictions.Eq("Linked",false));
            return criteria.List<IAgent>();
        }
        public IMobileAccount GetMobileAccountByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            criteria.Add(Restrictions.Eq("MobilePhone", phoneNumber));
            return criteria.UniqueResult<IMobileAccount>();
        }

        


        public IAgent GetAgentByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
            IMobileAccount mob = new MobileAccountRepository().GetByPhoneNumber(dataSource,phoneNumber) as IMobileAccount;
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgent));
            criteria.Add(Expression.Eq("MobileAccount.ID", mob.ID));
            return criteria.UniqueResult<IAgent>();
        }
        public virtual List<IAgent> FindAgentsWithPaging(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {

            Trace.TraceInformation("Inside Data for Agent");

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgent));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(InstitutionID))
            {


                criteria.Add(Expression.Eq("InstitutionCode",InstitutionID));
            }
            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<IAgent>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;
            }
        }

        public List<Agent> GetAgentByInstitutionCode(IDataSource _theDataSource, string isntCode, bool isMobileTeller)
        {
            List<IAgent> toReturn = new List<IAgent>();
            ICriteria criteria = Session(_theDataSource).CreateCriteria(typeof(IAgent));
            criteria.Add(Restrictions.Eq("InstitutionCode", isntCode));
            criteria.Add(Restrictions.Eq("IsMobileTeller", isMobileTeller));
            toReturn= criteria.List<IAgent>().ToList();
            return toReturn.Cast<Agent>().ToList();
        }
    }
}
