﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Data.Implementations;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.NHibernate.DAO
{
    //[DataContract]
    [Serializable]
    public class DataInjectionHelper : BankOneMobile.Data.NHibernate.Repository, BankOneMobile.Core.Helpers.IDataInjectionHelper
    {
        public AccountNumberType GetInstitutionAccountNumberType(string institutionCode)
        {
            //Get the institution
            using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                var institutionCriteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
                institutionCriteria.Add(Restrictions.Eq("Code", institutionCode));
                var institution = institutionCriteria.UniqueResult<IInstitution>();
                if (institution != null && institution.TheAccountNumberType == AccountNumberType.NUBAN)
                {
                    //For institutions using nuban
                    return AccountNumberType.NUBAN;
                }
                else
                {
                    //Default i.e no institution/institution's account type not set in database/account type is standard
                    return AccountNumberType.Standard;
                }
            }
        }
    }
}
