﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class SystemConfigRepository : Repository<BankOneMobile.Core.Contracts.ISystemConfig>, ISystemConfigRepository
    {


      public   ISystemConfig GetConfig(IDataSource dataSource)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.ISystemConfig));
            criteria.SetMaxResults(1);
            BankOneMobile.Core.Contracts.ISystemConfig toReturn = criteria.UniqueResult<BankOneMobile.Core.Contracts.ISystemConfig>();
            return toReturn;
        }
        
    }
}
