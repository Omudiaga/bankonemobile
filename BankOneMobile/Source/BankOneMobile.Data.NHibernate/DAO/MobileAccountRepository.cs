﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using System.Collections;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Diagnostics;


namespace BankOneMobile.Data.NHibernate.DAO
{
    public class MobileAccountRepository : Repository<IMobileAccount>, IMobileAccountRepository
    {
        public IMobileAccount GetByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            criteria.Add(Restrictions.Eq("MobilePhone", phoneNumber));
            return criteria.UniqueResult<IMobileAccount>();
        }
        IMobileAccount IMobileAccountRepository.GetByStandardPhoneNumber(IDataSource dataSource, string standardPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            criteria.Add(Restrictions.Eq("StandardPhoneNumber", standardPhoneNumber));
            return criteria.UniqueResult<IMobileAccount>();
        }
        
        public IMobileAccount GetByActivationCode(IDataSource dataSource, string activationCode)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            criteria.Add(Restrictions.Eq("ActivationCode", activationCode));
            return criteria.UniqueResult<IMobileAccount>();
        }
        public ILinkingBankAccount GetByAccountNumber(IDataSource dataSource, string acctNo,string institutionCode)
        {
            //Get the institution
            var institutionCriteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            institutionCriteria.Add(Restrictions.Eq("Code", institutionCode));
            var institution = institutionCriteria.UniqueResult<IInstitution>();
            if (institution == null) return null;

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));

            //Trace.TraceInformation("About to check the account type {0}", acctNo);
            if (institution.TheAccountNumberType == AccountNumberType.Standard)
            {
                Trace.TraceInformation("Getting by standard account{0}", acctNo);
                criteria.Add(Restrictions.Eq("StandardAccount", acctNo));
            }
            else if (institution.TheAccountNumberType == AccountNumberType.NUBAN)
            {
                Trace.TraceInformation("Getting by nuban account{0}", acctNo);
                criteria.Add(Restrictions.Eq("NUBAN", acctNo));               
            }
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Not(Restrictions.Eq("BankStatus", BankAccountStatus.InActive)));
            try
            {
                Trace.TraceInformation( string.Format("About to  Get Unique Account Number -{0}", acctNo));
                return criteria.UniqueResult<ILinkingBankAccount>();
            }
            catch  (Exception ex)
            {
                string errorMsg = String.Format("Error Saving Flow - {0}, Inner Message Is {1}, Inner Stack Trace is {2}(3)", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.StackTrace,acctNo);
                Trace.TraceInformation(errorMsg);
                throw new Exception(string.Format("The  number {0}  has already been registerd on Bank One Mobile", acctNo));
            }
        }

        public ILinkingBankAccount GetByAccountNumberForAuthorization(IDataSource dataSource, string acctNo, string institutionCode)
        {
            //Get the institution
            var institutionCriteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            institutionCriteria.Add(Restrictions.Eq("Code", institutionCode));
            var institution = institutionCriteria.UniqueResult<IInstitution>();
            if (institution == null) return null;

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));

            //Trace.TraceInformation("About to check the account type {0}", acctNo);
            if (institution.TheAccountNumberType == AccountNumberType.Standard)
            {
                Trace.TraceInformation("Getting by standard account{0}", acctNo);
                criteria.Add(Restrictions.Eq("StandardAccount", acctNo));
            }
            else if (institution.TheAccountNumberType == AccountNumberType.NUBAN)
            {
                Trace.TraceInformation("Getting by nuban account{0}", acctNo);
                criteria.Add(Restrictions.Eq("NUBAN", acctNo));
            }
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Not(Restrictions.Eq("BankStatus", BankAccountStatus.InActive)));
            try
            {
                Trace.TraceInformation(string.Format("#Auhtorization About to  Get Unique Account Number -{0}", acctNo));
                var listResult = criteria.List<ILinkingBankAccount>();
                return listResult==null?null:listResult.FirstOrDefault();
            }
            catch (Exception ex)
            {
                string errorMsg = String.Format("Error Saving Flow - {0}, Inner Message Is {1}, Inner Stack Trace is {2}(3)", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.StackTrace, acctNo);
                Trace.TraceInformation(errorMsg);
                throw new Exception(string.Format("The  number {0}  has already been registerd on Bank One Mobile", acctNo));
            }
        }

        public ILinkingBankAccount GetByAccountNumber(IDataSource dataSource, string acctNo)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));

            criteria.Add(Restrictions.Eq("StandardAccount", acctNo));
            ICriterion crit = Restrictions.Eq("BankStatus", BankAccountStatus.InActive);
            criteria.Add(Restrictions.Not(crit));
            
            return criteria.UniqueResult<ILinkingBankAccount>();
        }

        public IList<IMobileAccount> SearchByInstitutionID(IDataSource dataSource, string institutionID,ICriteria criteria)
        {
            

            //ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            //criteria.Add(Restrictions.Like("MobilePhone", phoneNumber, MatchMode.Anywhere))
               criteria.CreateCriteria("BankAccounts").Add(Restrictions.Eq("InstitutionCode", institutionID));
            criteria.SetResultTransformer(new global::NHibernate.Transform.DistinctRootEntityResultTransformer());

            //DetachedCriteria crit = DetachedCriteria.For<ILinkingBankAccount>()
            //    .Add(Restrictions.Eq("InstitutionCode", institutionID))
            //    .SetProjection(Projections.Property("TheMobileAccount"));
            //criteria.SetProjection(Projections.SubQuery(crit))
            return criteria.List<IMobileAccount>();
        }
        public virtual List<IMobileAccount> FindWithPaging(IDataSource dataSource,string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));
            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(InstitutionID))
            {
                DetachedCriteria innerCriteria = DetachedCriteria.For<LinkingBankAccount>();
                innerCriteria.Add(Restrictions.Eq("InstitutionCode", InstitutionID));
                innerCriteria.SetProjection(Projections.Property("TheMobileAccount.ID"));
                criteria.Add(Subqueries.PropertyIn("ID", innerCriteria));
            }

            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<IMobileAccount>(dataSource, criteria,order, startIndex, maxSize, out  total);
                return results;
            }
        }
        public virtual List<IMobileAccount> FindPageAndOrder(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMobileAccount));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(InstitutionID))
            {
                criteria.CreateCriteria("BankAccounts").Add(Restrictions.Eq("InstitutionCode", InstitutionID));
                criteria.SetResultTransformer(new global::NHibernate.Transform.DistinctRootEntityResultTransformer());
            }
            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<IMobileAccount>(dataSource, criteria,order, startIndex, maxSize, out  total);
                return results;
            }
        }
    }
}
