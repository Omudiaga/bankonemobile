﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;


namespace BankOneMobile.Data.NHibernate.DAO
{
    public class FeeTransactionTypeRepository : Repository<IFeeTransactionType> , IFeeTransactionType 
    {

        

       

        public IFeeTransactionType GetByName(IDataSource dataSource,string name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFeeTransactionType));
            criteria.Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<IFeeTransactionType>();
        }
        public IFeeTransactionType GetByID(IDataSource dataSource, long  id)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFeeTransactionType));
            criteria.Add(Restrictions.Eq("ID", id));
            return criteria.UniqueResult<IFeeTransactionType>();
        }
        public IList<IFeeTransactionType> GetActiveFeeTransactionTypes(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFeeTransactionType));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<IFeeTransactionType>();
        }






        #region IFeeTransactionType Members

        public string TransactionTypeName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool FlatOrPercent
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public decimal Amount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IEntity<long> Members

        public long ID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IEnableDisable Members

        public bool IsActive
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string DisplayMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
