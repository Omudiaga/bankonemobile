﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using BankOneMobile.Core.Helpers.Enums;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CommercialBankFundsTransferTransactionTypeRepository : Repository<ICommercialBankFundsTransferTransactionType>, ICommercialBankFundsTransferTransactionTypeRepository
    {
        public virtual List<BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount)
        {

            
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType));


            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            if (!string.IsNullOrEmpty(InstitutionID) || !string.IsNullOrEmpty(phoneNumber) || dateFrom.HasValue || dateTo.HasValue || status.HasValue)
            {
                criteria.CreateCriteria("TheTransaction", "tr");
                //sumCriteria = criteria.SetProjection(Projections.Sum("Amount"));
            }


            if (!string.IsNullOrEmpty(InstitutionID))
            {
                criteria.Add(Expression.Eq("tr.FromInstitutionCode", InstitutionID));
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                criteria.Add(Expression.Eq("tr.FromPhoneNumber", phoneNumber));
            }
            if (dateFrom.HasValue)
            {
                criteria.Add(Expression.Ge("tr.Date", dateFrom.Value));
            }
            if (dateTo.HasValue)
            {
                criteria.Add(Expression.Le("tr.Date", dateTo.Value));
            }
            if (status.HasValue)
            {
                criteria.Add(Expression.Eq("tr.Status", status));
            }
            Order order = Order.Desc("ID");
            //ICriteria sumCriteria = cr
            lock (Session(dataSource))
            {

                var results = RetrieveUsingPaging<BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType>(dataSource, criteria, order, startIndex, maxSize, out  total,out amount);
                return results;

            }
        }
        public virtual List<BankOneMobile.Core.Contracts.ICommercialBankFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, string transRef ,string bankName,IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount)
        {


            string query = "Select ct.BankName,ct.AccountNumber,ct.BeneficiaryLastName,ct.BeneficiaryOtherNames,ct.PaymentReference,ct.TransactionReference,mt.Amount  as TransactionAmount,mt.Status as TransactionStatus,mt.StatusDetails as TransactionDetails, mt.Date as TransactionDate,mt.FromPhoneNumber as MobilePhone,mt.FromInstitutionCode as Institution,mt.ID as TransactionID from BankoneMobile.dbo.CommercialBankFundsTransferTransactionTypes  ct left outer join BankOneMobile.dbo.MobileTransactions mt on ct.TransactionID=mt.ID where ct.ID>0 #InstitutionCode# #PhoneNumber# #TransactionReference# #Status# #Date#";
            query = query.Replace("#InstitutionCode#", string.IsNullOrEmpty(InstitutionID) ? string.Empty : string.Format("And mt.FromInstitutionCode ='{0}'", InstitutionID));
            query = query.Replace("#PhoneNumber#", string.IsNullOrEmpty(phoneNumber) ? string.Empty : string.Format("And mt.FromPhoneNumber like '%{0}%'", phoneNumber));            
            query = query.Replace("#Status#", status.HasValue? string.Format("And mt.Status ='{0}'",status.Value.ToString()):string.Empty);
            query = query.Replace("#TransactionReference#", string.IsNullOrEmpty(transRef) ? string.Empty : string.Format("And ct.TransactionReference like '%{0}%'", transRef));
            query = query.Replace("#TransactionReference#", string.IsNullOrEmpty(bankName) ? string.Empty : string.Format("And ct.BankName like '%{0}%'", bankName));
            
            if (dateFrom.HasValue&&dateTo.HasValue)
            {
                query = query.Replace("#Date#", string.Format("And mt.Date >='{0}' and mt.Date<='{1}'",dateFrom.Value,dateTo.Value));
            }
            else if (dateFrom.HasValue)
            {
                query = query.Replace("#Date#", string.Format("And mt.Date >='{0}'", dateFrom.Value));
            }
            else if (dateTo.HasValue)
            {
                query = query.Replace("#Date#", string.Format("And mt.Date <='{0}'", dateTo.Value));
            }
            else
            {
                query = query.Replace("#Date#", string.Empty);
            }
            var qq = Session(dataSource).CreateSQLQuery(query)

                //.AddEntity("t",typeof(Core.Implementations.Transaction))
                //.AddEntity("r",typeof(Core.Implementations.Transaction))
                   .SetFirstResult(startIndex)
                   .SetMaxResults(maxSize)
                   .SetResultTransformer(new AliasToBeanResultTransformer(typeof(Core.Implementations.CommercialBankFundsTransferTransactionType)));

            //SetResultTransformer(new TransactionTTransformer());
            var res = qq.List<BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType>();
            total = res.Count; amount = 0;

            return res.Cast <BankOneMobile.Core.Contracts.ICommercialBankFundsTransferTransactionType>().ToList();
            
        }

        public virtual BankOneMobile.Core.Implementations.InterbankTransaction GetTransactionById(IDataSource dataSource, long  ID)
        {


            string query = "Select ct.BankName,ct.AccountNumber,ct.BeneficiaryLastName,ct.BeneficiaryOtherNames,ct.PaymentReference,ct.TransactionReference,mt.Amount  as TransactionAmount,mt.Status as TransactionStatus,mt.StatusDetails as TransactionDetails, mt.Date as TransactionDate,mt.FromPhoneNumber as MobilePhone,mt.FromInstitutionCode as Institution from BankoneMobile.dbo.CommercialBankFundsTransferTransactionTypes  ct left outer join BankOneMobile.dbo.MobileTransactions mt on ct.TransactionID=mt.ID where ct.ID=#ID#";
            query = query.Replace("#ID#",Convert.ToString(ID));


            var qq = Session(dataSource).CreateSQLQuery(query);

                //.AddEntity("t",typeof(Core.Implementations.Transaction))
                //.AddEntity("r",typeof(Core.Implementations.Transaction))
                   

            //SetResultTransformer(new TransactionTTransformer());
            var res = qq.UniqueResult<BankOneMobile.Core.Implementations.InterbankTransaction>();
            

            return res;

        }


       
    }
}
