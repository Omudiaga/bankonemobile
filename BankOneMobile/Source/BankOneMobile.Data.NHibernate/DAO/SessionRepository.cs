﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class SessionRepository : Repository<BankOneMobile.Core.Contracts.ISession>, ISessionRepository
    {
        public BankOneMobile.Core.Contracts.ISession GetBySessionID(IDataSource dataSource, string sessionID)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.ISession));
            criteria.Add(Restrictions.Eq("SessionID", sessionID));
            BankOneMobile.Core.Contracts.ISession toReturn =  criteria.UniqueResult<BankOneMobile.Core.Contracts.ISession>();
            return toReturn;

            
        }
        public BankOneMobile.Core.Contracts.ISession GetByWorkFlowID(IDataSource dataSource, string workFlowID)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.ISession));
            criteria.Add(Restrictions.Eq("WorkFlowID", workFlowID));
            BankOneMobile.Core.Contracts.ISession toReturn = criteria.UniqueResult<BankOneMobile.Core.Contracts.ISession>();
            return toReturn;


        }

        


        public IList<Core.Contracts.ISession> GetSessionsBetweenDates(IDataSource dataSource, DateTime fromDate, DateTime toDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.ISession));
            criteria.Add(Restrictions.Ge("StartTime", fromDate));
            criteria.Add(Restrictions.Le("StartTime", toDate));
          IList<  BankOneMobile.Core.Contracts.ISession> toReturn = criteria.List<BankOneMobile.Core.Contracts.ISession>();
          IList<Session> first = toReturn.Cast<BankOneMobile.Core.Implementations.Session>().ToList();
            IList<Session> second = toReturn  as IList<Session>;
            return toReturn;
        }

        
    }
}
