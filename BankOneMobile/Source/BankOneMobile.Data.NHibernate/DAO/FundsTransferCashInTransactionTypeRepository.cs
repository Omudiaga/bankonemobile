﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class FundsTransferCashInTransactionTypeRepository : Repository<IFundsTransferCashInTransactionType>, IFundsTransferCashInTransactionTypeRepository
    {
        

        public IFundsTransferCashInTransactionType GetByToken(IDataSource dataSource, string token)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferCashInTransactionType));
            criteria.Add(Restrictions.Eq("Token", token));
            return criteria.UniqueResult<IFundsTransferCashInTransactionType>();
            
        }

        public IList<IFundsTransferCashInTransactionType> GetByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferCashInTransactionType));
            criteria.Add(Restrictions.Eq("BeneficiaryPhoneNumber", phoneNumber));
            return criteria.List<IFundsTransferCashInTransactionType>();
        }
       
        
    }
}
