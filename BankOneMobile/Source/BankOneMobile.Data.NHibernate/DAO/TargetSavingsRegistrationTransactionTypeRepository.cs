﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class TargetSavingsRegistrationTransactionTypeRepository : Repository<ITargetSavingsRegistrationTransactionType>, ITargetSavingsRegistrationTransactionTypeRepository
    {
        public List<ITargetSavingsRegistrationTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string institutionCode, 
            string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ITargetSavingsRegistrationTransactionType));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(institutionCode) || !string.IsNullOrEmpty(phoneNumber) || dateFrom.HasValue || dateTo.HasValue)
            {
                criteria.CreateCriteria("TheTransaction", "tr");
                //sumCriteria = criteria.SetProjection(Projections.Sum("Amount"));
            }
            if (!string.IsNullOrEmpty(institutionCode))
            {
                criteria.Add(Expression.Eq("tr.FromInstitutionCode", institutionCode));
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                criteria.Add(Expression.Eq("tr.FromPhoneNumber", phoneNumber));
            }
            if (dateFrom.HasValue)
            {
                criteria.Add(Expression.Ge("tr.Date", dateFrom.Value));
            }
            if (dateTo.HasValue)
            {
                criteria.Add(Expression.Le("tr.Date", dateTo.Value));
            }
            Order order = Order.Desc("ID");
            //ICriteria sumCriteria = cr
            lock (Session(dataSource))
            {

                var results = RetrieveUsingPaging<ITargetSavingsRegistrationTransactionType>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;

            }
        }
    }
}
