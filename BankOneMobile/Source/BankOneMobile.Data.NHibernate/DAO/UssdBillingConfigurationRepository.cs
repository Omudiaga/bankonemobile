﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class UssdBillingConfigurationRepository : Repository<IUssdBillingConfiguration>, IUssdBillingConfigurationRepository
    {
        public List<IUssdBillingConfiguration> FindUssdBillingConfigurationsWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IUssdBillingConfiguration));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                var results = RetrieveUsingPaging<IUssdBillingConfiguration>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;
            }
        }
    }
}
