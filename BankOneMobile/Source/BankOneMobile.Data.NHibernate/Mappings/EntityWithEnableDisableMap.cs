﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.Mappings
{
    public class EntityWithEnableDisableMap<T> : EntityMap<T> where T : IEnableDisable, IEntity
    {
        public EntityWithEnableDisableMap()
        {
            Map(x => x.IsActive);
        }
    }
}
