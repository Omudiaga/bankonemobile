﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Mapping;

namespace BankOneMobile.Data.NHibernate.FluentNHibernate.Conventions
{
    /// <summary>
    /// Fluent NHibernate Convention for Handling HasMany(Bags/Sets/Lists) 
    /// <remarks>
    /// Removes the "The" and adds "ID" to the end of the Property Name of the Entity
    /// </remarks>
    /// <example>
    /// TheFunction --> FunctionID
    /// </example>
    /// </summary>
    public class HasManyConvention : IHasManyConvention
    {
        #region IConvention<IOneToManyCollectionInspector,IOneToManyCollectionInstance> Members

        public void Apply(global::FluentNHibernate.Conventions.Instances.IOneToManyCollectionInstance instance)
        {

            instance.Key.Column((instance.EntityType.Name.StartsWith("The") ? instance.EntityType.Name.Remove(0, 3) : instance.EntityType.Name) + "ID");
            instance.Cascade.AllDeleteOrphan();
            //instance.Not.LazyLoad();
            //target.AsBag();
            //target.SetAttribute("insert", "false");
            //target.SetAttribute("update", "false");
            instance.Inverse();

        }

        #endregion
    }
}
