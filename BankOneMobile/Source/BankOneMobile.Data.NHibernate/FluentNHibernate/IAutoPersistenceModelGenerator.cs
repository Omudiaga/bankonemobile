﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Automapping;

namespace BankOneMobile.Data.NHibernate.FluentNHibernate
{
    public interface IAutoPersistenceModelGenerator
    {
        AutoPersistenceModel Generate();
    }
}
