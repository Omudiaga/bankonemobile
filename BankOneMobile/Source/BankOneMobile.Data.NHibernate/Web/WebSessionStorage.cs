﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Web;

namespace BankOneMobile.Data.NHibernate.Web
{
    /// <summary>
    /// Implements <see cref="ISessionStorage"/> via the Open-Session-in-View pattern
    /// Handles storage of Nhibernate Storage using the HttpContext and Closes the session at the end of the HttpRequest.
    /// </summary>
    public class WebSessionStorage : ISessionStorage
    {
        /// <summary>
        /// Constant key for storing the session in the HttpContext
        /// </summary>
        private const string CurrentSessionKey = "nhibernate.current_session";

        /// <summary>
        /// Initialize the storage for the specified Web Application
        /// </summary>
        /// <param name="app">The HttpApplication of the calling web application</param>
        public WebSessionStorage(HttpApplication app)
        {
            app.EndRequest += Application_EndRequest;
        }

        public ISession Session
        {
            get
            {
                //Get the current session from the HttpContext Variable
                HttpContext context = HttpContext.Current;
                ISession session = context.Items[CurrentSessionKey] as ISession;
                return session;
            }
            set
            {
                //Saves the current session into the HttpContext
                HttpContext context = HttpContext.Current;
                context.Items[CurrentSessionKey] = value;
            }
        }

        void Application_EndRequest(object sender, EventArgs e)
        {
            ISession session = Session;

            //Closes the session if there's any open session
            if (session != null)
            {
                try
                {
                    if (session.Transaction != null && session.Transaction.IsActive && !session.Transaction.WasCommitted && !session.Transaction.WasRolledBack)
                    {
                        session.Transaction.Commit();
                        session.Flush();
                    }
                }
                catch { }
                finally
                {
                    session.Close();
                }
                HttpContext context = HttpContext.Current;
                context.Items.Remove(CurrentSessionKey);
            }
        }
    } 
}
