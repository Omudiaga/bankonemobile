﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Collections;
using BankOneMobile.Services.Utility;

namespace BankOneMobile.Services
{
  public  class LinkingBankAccountSystem
    {
        private static  IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private static  ILinkingBankAccountRepository _repository = SafeServiceLocator<ILinkingBankAccountRepository>.GetService();

        public LinkingBankAccountSystem(IDataSource dataSource)
        {
            _theDataSource = dataSource;
        }
        //public static ILinkingBankAccount SaveLinkingBankAccount(LinkingBankAccount theLinkingBankAccount)
        //{
            
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().Save(DataSourceFactory.GetDataSource(DataCategory.Shared), theLinkingBankAccount);
        //    SafeServiceLocator<ILinkingBankAccountRepository>.GetService().DbContext.CommitChanges(DataSourceFactory.GetDataSource(DataCategory.Shared));
        //    return result;

        //}

        //public static ILinkingBankAccount UpdateLinkingBankAccount(LinkingBankAccount theLinkingBankAccount)
        //{
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().Update(DataSourceFactory.GetDataSource(DataCategory.Shared), theLinkingBankAccount);
        //    return result;

        //}

        //public static ILinkingBankAccount GetLinkingBankAccount(long id)
        //{
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().Get(DataSourceFactory.GetDataSource(DataCategory.Shared), id);
        //    return result;

        //}

        //public static List<ILinkingBankAccount> GetAllLinkingBankAccounts()
        //{
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().GetAll(DataSourceFactory.GetDataSource(DataCategory.Shared));
        //    return result;

        //}

        //public static List<ILinkingBankAccount> GetActiveLinkingBankAccounts()
        //{
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().GetAll(DataSourceFactory.GetDataSource(DataCategory.Shared));
        //    result = result.ToList();
        //    return result;

        //}

        //public static List<ILinkingBankAccount> FindLinkingBankAccounts(IDictionary<string, object> propertyValuePairs)
        //{
        //    var result = SafeServiceLocator<ILinkingBankAccountRepository>.GetService().Search(DataSourceFactory.GetDataSource(DataCategory.Shared), propertyValuePairs);
        //    return result;
        //}

        public List<ILinkingBankAccount> Search(string phoneNumber,string isActive,string isAgentccount,string acctNo, string institutionCode, string productCode,string gender, string lastName, string otherNames, string registrarCode, string status, int startIndex, int maxSize, out int total)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ILinkingBankAccount> accs = new List<ILinkingBankAccount>();


            //if (!string.IsNullOrEmpty(registrarCode) && !string.IsNullOrEmpty(registrarCode.Trim()))
            //{
            //    dic.Add("RegistrarCode", registrarCode);

            //}
            if (!string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(lastName.Trim()))
            {
                dic.Add("LastName", lastName);

            }
            if (!string.IsNullOrEmpty(otherNames) && !string.IsNullOrEmpty(otherNames.Trim()))
            {
                dic.Add("OtherNames", otherNames);

            }
            //if (!string.IsNullOrEmpty(isActive) && !string.IsNullOrEmpty(isActive.Trim()))
            //{
            //    dic.Add("BankStatus", isActive);

            //}

            if (!string.IsNullOrEmpty(gender) && !string.IsNullOrEmpty(gender.Trim()))
            {
                Gender gend = (Gender)Enum.Parse(typeof(Gender), gender);
                dic.Add("TheGender", gend);

            }



            accs = FindAccounts(dic, acctNo, status, isAgentccount, phoneNumber, productCode, gender, institutionCode, startIndex, maxSize, out total);
            //if (_theDataSource.Type == DataCategory.Shared)
            //{
            //    //_repository.DbContext.Close(_theDataSource);
            //}
            return accs;

        }
        public static   List<ILinkingBankAccount> FindAccounts(IDictionary<string, object> propertyValuePairs,string acctNo,string status,string isAgentAcct,string phoneNumber,string productCode,string gender, string instId, int startIndex, int maxSize, out int total)
        {
            //var result = _repository.Search(_theDataSource, propertyValuePairs)
            
            var result = _repository.Find(_theDataSource,acctNo,status,isAgentAcct,phoneNumber,productCode,gender,instId, propertyValuePairs, startIndex, maxSize, out total);
            if (_theDataSource.Type == DataCategory.Shared)
            {
                //_repository.DbContext.Close(_theDataSource);
            }
            return result;
        }
        public virtual List<ILinkingBankAccount> FindActive(IDataSource dataSource, string instCode, string acctNo)
        {
            return _repository.FindActive(dataSource, instCode, acctNo);
        }
      public LinkingBankAccount GetByID(IDataSource source  ,long ID)
      {
          return _repository.Get(source, ID) as LinkingBankAccount;
      }
      public Dictionary<string,string> GetInstitutionProducts(string instCode)
      {
          Dictionary<string, string> toReturn = new Dictionary<string,string>(); string holder = string.Empty;
          ArrayList vals = _repository.GetProducts(_theDataSource, instCode);
          foreach (var obj in vals)
          {
              Object[] ar = obj as Object[];
              if (!string.IsNullOrEmpty(Convert.ToString(ar[0])) && !string.IsNullOrEmpty(Convert.ToString(ar[1])))
              {

                  if(!toReturn.ContainsKey(ar[0].ToString()))
                  {
                  toReturn.Add(ar[0].ToString(),ar[1].ToString());
                  }
              }
          }
          return toReturn;
      }

      public byte[] GetAccountImage(string entityID, string imageType)
      {
          byte[] toReturn = null;
          LinkingBankAccount lbc = GetByID(_theDataSource,Convert.ToInt64(entityID));
          if (lbc == null)
          {
              return toReturn;
          }
          else if (imageType == "passport")
          {
              return lbc.Passport;
          }
          else if (imageType == "signature")
          {
              return lbc.Signature;
          }
          return toReturn;
      }
      
      
    }
}
