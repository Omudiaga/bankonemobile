﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class RegistrationTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IRegistrationTransactionTypeRepository _repository = SafeServiceLocator<IRegistrationTransactionTypeRepository>.GetService();
       public RegistrationTransactionTypeSystem()
       {

       }

       public RegistrationTransactionTypeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        public  IRegistrationTransactionType SaveRegistrationTransactionType(RegistrationTransactionType theRegistrationTransactionType)
        {
            theRegistrationTransactionType.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theRegistrationTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  IRegistrationTransactionType UpdateRegistrationTransactionType(RegistrationTransactionType theRegistrationTransactionType)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theRegistrationTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  IRegistrationTransactionType GetRegistrationTransactionType(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

        public  List<IRegistrationTransactionType> GetAllRegistrationTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            return result;

        }

        public  List<IRegistrationTransactionType> GetActiveRegistrationTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public  List<IRegistrationTransactionType> FindRegistrationTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            return result;
        }

        public List<IRegistrationTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            var tr = new List<IRegistrationTransactionType>();

            tr = _repository.FindTransactionsWithPaging(dataSource, institutionCode, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;
        }
    }
}
