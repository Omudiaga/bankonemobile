﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using System.Diagnostics;

namespace BankOneMobile.Services
{
   public class InstitutionSystem
    {
       private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
       private IInstitutionRepository _repository = SafeServiceLocator<IInstitutionRepository>.GetService();

      public InstitutionSystem()
       {

       }

      public InstitutionSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
       public static List<IInstitution> GetAllInstitution()
       {
           List<IInstitution> institutions = new List<IInstitution>();

           using (InstitutionServiceRef.InstitutionServiceClient theClient = new InstitutionServiceRef.InstitutionServiceClient())
           {
              List<InstitutionServiceRef.Institution> institutionsObj =  theClient.GetAll();
               
              if (institutionsObj != null && institutionsObj.Count > 0)
              {
                  foreach(var institutionObj in institutionsObj)
                  {
                      IInstitution theInstitution = new Institution();
                      theInstitution.Code = institutionObj.Code;
                      theInstitution.Name = institutionObj.Name;
                      
                      institutions.Add(theInstitution);
                  }

              }

           }

           return institutions;

       }
       public IInstitution GetByCode(IDataSource dataSource, string code)
       {
           return _repository.GetByCode(dataSource,code);

       }
       public IInstitution Insert(IInstitution institution, out string displayMessage)
       {
           displayMessage = string.Empty;
           try
           {
               _repository.DbContext.BeginTransaction(_theDataSource);
               var result = _repository.Save(_theDataSource, institution);
               _repository.DbContext.CommitTransaction(_theDataSource);
               return result;
           }
           catch(Exception ex)
           {
               _repository.DbContext.RollbackTransaction(_theDataSource);
               PANE.ERRORLOG.ErrorLogger.Log(ex);
               displayMessage = ex.Message;
               return null;
           }
       }
       public IInstitution EnableDisable(IInstitution institution)
       {
           var inst = institution as Institution;
           inst.IsActive = !inst.IsActive;
           return Update(inst);
       }
       public IInstitution Update(IInstitution institution)
       {
           try
           {
               _repository.DbContext.BeginTransaction(_theDataSource);
               var result = _repository.Update(_theDataSource, institution);
               _repository.DbContext.CommitTransaction(_theDataSource);
               return result;
           }
           catch (Exception ex)
           {
               _repository.DbContext.RollbackTransaction(_theDataSource);
               PANE.ERRORLOG.ErrorLogger.Log(ex);
               //displayMessage = ex.Message;
               return null;
           }
       }
       public IInstitution GetByName(IDataSource dataSource, string name)
       {
           return _repository.GetByName(_theDataSource, name);
       }
       public IInstitution GetByShortName(IDataSource dataSource, string shortName)
       {
           return _repository.GetByShortName(dataSource, shortName);
       }
       public IList<IInstitution> GetActiveInstitutions(IDataSource dataSource)
       {
           return _repository.GetActiveInstitutions(dataSource);
       }
       public Dictionary<string,string> GetAllInstitutionNames(IDataSource dataSource)
       {
           Trace.TraceInformation("Inside System For Get Inst");
           Dictionary<string, string> toReturn = new Dictionary<string, string>(); 
           IList<IInstitution> insts  =null;
           try
           {
               Trace.TraceInformation("About To Get Inst");
               insts = _repository.GetAllInstitutions(dataSource);
           }
           catch (Exception ex)
           {
               Trace.TraceInformation("unable to get institutions -{0}", ex.Message);
           }
           foreach(IInstitution  st in insts)
           {
               toReturn.Add(st.Code,st.Name);
           }
           return toReturn;
       }
       public List<IInstitution> Search(string name, string code, int startIndex, int maxSize, out int total)
       {
           return _repository.FindWithPaging(_theDataSource, name, code, startIndex, maxSize, out total);
       }
       public static string GetInstitutionAppZoneIncomeAccount(string institutionCode)
       {
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               InstitutionSystem system = new InstitutionSystem(dataSource);
               Institution institution = system.GetByCode(dataSource, institutionCode) as Institution;
               return "";
               //AppZoneIncomeAccount incomeAccount = system._repository.GetAppZoneIncomeAccount(system._theDataSource) as AppZoneIncomeAccount;
               //if (incomeAccount == null)
               //{
               //    return "";
               //}
               //else
               //{
               //    return incomeAccount.AccountNumber;
               //}
           }
       }
    }

}
