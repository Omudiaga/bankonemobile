﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
    public class BillsPaymentTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IBillsPaymentTransactionTypeRepository _repository = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService();
        public List<BillsPaymentTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string merchantName, string transactionRefernce, string paymentReference, string customerID, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<BillsPaymentTransactionType> tr = new List<BillsPaymentTransactionType>();

            TransactionStatus? transStatus = null;
            if (!string.IsNullOrEmpty(status))
            {
                transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
            }
            if (!string.IsNullOrEmpty(transactionRefernce) && !string.IsNullOrEmpty(transactionRefernce.Trim()))
            {

                dic.Add("TransactionReference", transactionRefernce);

            }
            if (!string.IsNullOrEmpty(paymentReference) && !string.IsNullOrEmpty(paymentReference.Trim()))
            {

                dic.Add("PaymentReference", paymentReference);

            }
            if (!string.IsNullOrEmpty(customerID) && !string.IsNullOrEmpty(customerID.Trim()))
            {

                dic.Add("CustomerID", customerID);

            }
            if (!string.IsNullOrEmpty(merchantName) && !string.IsNullOrEmpty(merchantName.Trim()))
            {

                dic.Add("MerchantName", merchantName);

            }


            //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = _repository.FindTransactionsWithPaging(_theDataSource, institutionCode, transStatus, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;

        }
        public static IBillsPaymentTransactionType SaveBillsPaymentTransactionType(IDataSource dataSource, BillsPaymentTransactionType theBillsPaymentTransactionType)
        {
            theBillsPaymentTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theBillsPaymentTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            //SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService().DbContext.CommitChanges(dataSource);
            return result;

        }

        public static IBillsPaymentTransactionType UpdateBillsPaymentTransactionType(IDataSource dataSource, BillsPaymentTransactionType theBillsPaymentTransactionType)
        {
            var repo = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theBillsPaymentTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IBillsPaymentTransactionType GetBillsPaymentTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IBillsPaymentTransactionType> GetAllBillsPaymentTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IBillsPaymentTransactionType> GetActiveBillsPaymentTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IBillsPaymentTransactionType> FindBillsPaymentTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IBillsPaymentTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }
    }
}
