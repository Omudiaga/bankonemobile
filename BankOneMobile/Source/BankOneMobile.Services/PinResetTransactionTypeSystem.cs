﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class PinResetTransactionTypeSystem
    {
        public static IPinResetTransactionType SavePinResetTransactionType(IDataSource dataSource, PinResetTransactionType thePinResetTransactionType)
        {
            thePinResetTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(DataSourceFactory.GetDataSource(DataCategory.Shared), thePinResetTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IPinResetTransactionType UpdatePinResetTransactionType(IDataSource dataSource, PinResetTransactionType thePinResetTransactionType)
        {
            var repo = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(DataSourceFactory.GetDataSource(DataCategory.Shared), thePinResetTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IPinResetTransactionType GetPinResetTransactionType(long id)
        {
            var result = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService().Get(DataSourceFactory.GetDataSource(DataCategory.Shared), id);
            return result;

        }

        public static List<IPinResetTransactionType> GetAllPinResetTransactionTypes()
        {
            var result = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService().GetAll(DataSourceFactory.GetDataSource(DataCategory.Shared));
            return result;

        }

        public static List<IPinResetTransactionType> GetActivePinResetTransactionTypes()
        {
            var result = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService().GetAll(DataSourceFactory.GetDataSource(DataCategory.Shared));
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IPinResetTransactionType> FindPinResetTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IPinResetTransactionTypeRepository>.GetService().Search(DataSourceFactory.GetDataSource(DataCategory.Shared), propertyValuePairs);
            return result;
        }
    }
}
