﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using System.Data.SqlTypes;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class ProductSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private IProductRepository _repository = SafeServiceLocator<IProductRepository>.GetService();

      public ProductSystem()
       {

       }

      public ProductSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        public  IProduct SaveProduct(Product theProduct)
        {

            if(GetProductByName(theProduct.Name)!=null)
            {
                throw new ApplicationException("Name Already Exists");
            }
            if (GetProductByCode(theProduct.Code) != null)
            {
                throw new ApplicationException("Code Already Exists");
            }
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theProduct);
            _repository.DbContext.CommitTransaction(_theDataSource);
            
            return result;
            

        }

        public IProduct UpdateProduct(IProduct theProduct)
        {
            IProduct prod = GetProductByName(theProduct.Name);
            if (prod != null && prod.ID != theProduct.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            prod = GetProductByCode(theProduct.Code);
            if (prod != null && prod.ID != theProduct.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            //MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, theProduct.PhoneNumber) as MobileAccount;
            //if (existingAcct != null && theProduct.MobileAccount.ID != existingAcct.ID)
            //{
            //    throw new ApplicationException("Mobile Number already exists");
            //}
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theProduct);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
            

        }

        public IProduct GetProduct(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;
            
        }
        public IProduct GetProductByCode(string code)
        {
            var result = _repository.GetByCode(_theDataSource,code);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IProduct GetProductByName(string name)
        {
            var result = _repository.GetByName(_theDataSource, name);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IProduct> GetAllProducts()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IProduct> GetActiveProducts()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IList<Product> GetActiveProductsByInstitutionCode(string institutionCode)
        {
            IList<Product> prods = new List<Product>();
            using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
            {
                List<CoreBankingService.Product> coreBankingProducts = client.GetProducts(institutionCode).ToList();
                foreach (CoreBankingService.Product prod in coreBankingProducts)
                {
                    prods.Add(new Product { Name = prod.Name, Code = prod.Code });

                }
            }


            //prods.Add(new Product { Name = "Savings", Code = "0988" });
            //prods.Add(new Product { Name = "Current", Code = "9002" });
            return prods;

        }

        public List<IProduct> FindProducts(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
        public Product EnableDisableProduct(Product theProduct)
        {
            if (theProduct.IsActive)
            {
                theProduct.IsActive = false;
            }
            else
            {
                theProduct.IsActive = true;
            }
            UpdateProduct(theProduct);
            return theProduct;
        }

        public IList<Product> GetProductsByAgentPhone(string agentPhoneNumber)
        {
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
            Agent agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(agentPhoneNumber)  as Agent;
            if (agentPhoneNumber ==null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Transaction not allowed for non agents");
            }
            return GetActiveProductsByInstitutionCode(agent.InstitutionCode);
        }

        public IList<IProduct> GetProductsByInstitutionCode(string institutionCode)
        {
            var result = _repository.GetActiveProductsByInstitutionCode(_theDataSource, institutionCode);
            
            //Todo  Get real products by institution Code
            return result;

        }

      
      
      
    }
}
