﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using BankOneMobile.Data.Implementations;


namespace BankOneMobile.Services.WorkflowServices
{
    [System.Web.Services.Protocols.SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    [System.Web.Services.WebService(Namespace = "http://v2.ws.tp.ussd.services.infobip.org/", Name = "UssdThirdPartyWsDemo")]
    public class MockTpaWebService : UssdThirdPartyWsPort
    {

        #region IUssdThirdPartyWsPortBinding Members
        private static Dictionary<string, int> _Sessions = new Dictionary<string,int>();

        private string[] _SampleMenus = new string[]
        {
            "My BankOneMobile Menu\r\n1.Register\r\n2.Check Balance\r\n",
            "Register\r\nEnter Your Name\r\n",
            "Check Balance\r\nEnter your PIN",
            "Thank you for using BankOneMobile"
        };


        public bool isSesionActive(string sessionId)
        {
            return _Sessions.ContainsKey(sessionId);
        }

        public void onSessionEnd(string sessionId, int exitCode, bool exitCodeSpecified, string reason)
        {
            if (_Sessions.ContainsKey(sessionId))
            {
                _Sessions.Remove(sessionId);
            }
        }

        public global::menuResponseParams onSessionStart(string sessionId, global::menuRequestParams @params)
        {
            global::menuResponseParams response = new global::menuResponseParams();
            if (_Sessions.ContainsKey(sessionId))
            {
                response.ussdMenu = _SampleMenus[_Sessions[sessionId]];
            }
            else
            {
                response.ussdMenu = _SampleMenus[0];
                _Sessions.Add(sessionId, 0);
            }
            return response;
        }

        public global::menuResponseParams onUserResponse(string sessionId, global::menuRequestParams @params)
        {
            global::menuResponseParams response = new global::menuResponseParams();
            if (_Sessions.ContainsKey(sessionId))
            {
                int currentIndex = _Sessions[sessionId];
                int newIndex = 0;
                if (currentIndex == 0)
                {
                    switch (@params.text)
                    {
                        case "1":
                            newIndex = 1;
                            break;
                        case "2":
                            newIndex = 2;
                            break;
                        default:
                            break;
                    }
                }
                else if (currentIndex == 1 || currentIndex == 2)
                {
                    newIndex = 3;
                }
                else
                {
                    newIndex = 3;
                }
                if (newIndex == 3)
                {
                    response.shouldClose = true;
                }
                response.ussdMenu = _SampleMenus[newIndex];
                _Sessions[sessionId] = newIndex;
            }
            else
            {
                response.ussdMenu = _SampleMenus[0];
                _Sessions.Add(sessionId, 0);
            }
            return response;
        }

        #endregion


        public bool GenerateActivationCode(string phoneNumber)
        {
            return new  TpaWebService().GenerateActivationCode(phoneNumber,DataSourceFactory.GetDataSource(DataCategory.Shared));
        }
        public bool VerifyActivationCode(string phoneNo, string activationCode)
        {
           //return  new TpaWebService().ConfirmActicationCode(phoneNo,activationCode, false);
            return false;
        }
    }
}
