﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using System.Activities;
using System.IO;
using System.Activities.XamlIntegration;
using System.Xaml;
using System.Diagnostics;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using System.Threading;


namespace BankOneMobile.Services.WorkflowServices
{

    public class SessionInfo
    {
        public Timer BillingTimer { get; set; }
        public string SessionID { get; set; }
        public MobileAccount MobileAccount { get; set; }
        public bool HasDebitedCustomer { get; set; }
    }
    [System.Web.Services.Protocols.SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    [System.Web.Services.WebService(Namespace = "http://v2.ws.tp.ussd.services.infobip.org/", Name = "UssdThirdPartyWsDemo")]
    public class TpaWebService : UssdThirdPartyWsPort
    {
        private const string RETURN_TO_MAIN_MENU_KEY = "00";
        private const string UNREGISTERED_CUSTOMER_MESSAGE_1 = "Your phone number is not registered";
        private const string UNREGISTERED_CUSTOMER_MESSAGE_2 = "Please Register with the nearest Agent  to  gain access to this service";
        private const string INACTIVE_CUSTOMER_MESSAGE = "Your phone number is not registered for this service";
        private const string AWAITING_APPROVAL_MESSAGE = "Your mobile account has not yet been approved";
      

        #region IUssdThirdPartyWsPortBinding Members
        
        public bool isSesionActive(string sessionId)
        {
            Trace.TraceInformation(string.Format("Is Session Active - session id:{0}", sessionId));
            bool response = false;
            WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
            if (theWorkflowInfo != null)
            {
                if (!theWorkflowInfo.IsCompleted && !theWorkflowInfo.IsFaulted)
                {
                    response = true;
                }
            }
            Trace.TraceInformation(string.Format("Response - isSessionActive:{0}", response));
            return response;
        }

        public void onSessionEnd(string sessionId, int exitCode, bool exitCodeSpecified, string reason)
        {
            //lock (_lockObject)
            //{
                Trace.TraceInformation(string.Format("Session End - session id:{0}, exitcode:{1}, exitCodeSpecified:{2}, reason:{3}", sessionId, exitCode, exitCodeSpecified, reason));
                WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
                SessionStatus stat;
                if (theWorkflowInfo != null)
                {
                    //TODO: Log Reason and exitCode
                    switch (exitCode)
                    {
                        case 200: //Session Ended Normally  
                            stat = SessionStatus.EndedNormally;
                            break;
                        case 500: //Session aborted by Network
                            stat = SessionStatus.EndedByNetwork;
                            break;
                        case 510: //Session aborted by TPA
                            stat = SessionStatus.EndedByTPA;
                            break;
                        case 520: //Session aborted by User
                            stat = SessionStatus.EndedByUser;
                            break;
                        case 600: //Session timeout
                            stat = SessionStatus.TimedOut;
                            break;
                        default:
                            stat = SessionStatus.EndedByTPA;
                            break;
                    }
                    var items = WorkflowCenter.Instance.InitiatingWorkflows.ToList().Where(x => x.Value == theWorkflowInfo.TheWorkflow.Id);
                    foreach (var item in items)
                    {
                        Guid wId;
                        WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(item.Key, out wId);
                       // WorkflowCenter.Instance.RemoveWorkflow(wId);
                    }
                    Guid theguid;

                    WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(theWorkflowInfo.InitiatedBy.StandardPhoneNumber, out theguid);

                    
                    if (theWorkflowInfo.HasPreviousWorkflow)
                    {
                        theWorkflowInfo.ThePreviousWorkflow.InitiatingFlowID = null;
                    }

                    //WorkflowCenter.Instance.WorkflowInfos[theguid].TheWorkflow.Terminate("end");
                    //WorkflowCenter.Instance.RemoveWorkflow(theguid);
                    // new SessionSystem().StopSession(sessionId, stat);// Log session as ended and set bill amount
                    //Stop billing timer

                    Trace.TraceInformation(string.Format("Removing Workflow:{0}", sessionId));
                    WorkflowCenter.Instance.RemoveWorkflow(sessionId);
                    Trace.TraceInformation(string.Format("Workflow Removed:{0}", sessionId));
                }
            //}
        }
        private static readonly object _lockObject = new object();
        public global::menuResponseParams onSessionStart(string sessionId, global::menuRequestParams @params)
        {
            //lock (_lockObject)
            //{
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Inside TPA. About to start session"));
            bool includeXML = false;
            includeXML = string.IsNullOrEmpty(@params.optional) ? false : Convert.ToBoolean(@params.optional);
            global::menuResponseParams response = new global::menuResponseParams();
            if (string.IsNullOrEmpty(sessionId))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Session ID";
                return response;
            }
            //else  if (!CheckUniqueSessionID(sessionId))
            //{
            //    //response.shouldClose = true;
            //    //response.ussdMenu = "Session ID has already been used";
            //    //return response;
            //}
            else if (@params == null)
            {
                response.shouldClose = true;
                response.ussdMenu = "menuRequestParms is null";
                return response;

            }
            else if (string.IsNullOrEmpty(@params.msisdn))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Phone Number";
                return response;
            }
            IFlowRepository flowRepo = SafeServiceLocator<IFlowRepository>.GetService();

            string initiatingSessionID = string.Empty;
            string standardPhoneNo = @params.msisdn.StartsWith("+") ? @params.msisdn : "+" + @params.msisdn;//convert to standard i.e. with + and 234

            //Guid sessionGUID = Guid.NewGuid();
            using (var sessionDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                Trace.TraceInformation("DataSource - StorageKey:{0}, FactoryKey:{1}", sessionDataSource.StorageKey, sessionDataSource.FactoryKey);
                //IMobileAccount mobileAccount = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByStandardPhoneNumber(standardPhoneNo);
               IMobileAccount mobileAccount = new MobileAccountSystem(sessionDataSource).GetByStandardPhoneNumber(standardPhoneNo);

                //IMobileAccount mobileAccount = new MobileAccount()
                //{
                //    StandardPhoneNumber = standardPhoneNo,
                //    IsAgentAccount = false,
                //    IsActive = true,
                //    MobileAccountStatus = MobileAccountStatus.Active,
                //    RecievingBankAccount = new LinkingBankAccount(){
                //        InstitutionCode="10011"

                //    }
                //};

                if (mobileAccount == null)// check for the initiator's details
                {
                    using (var auxilliaryDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Auxilliary))
                    {
                        Trace.TraceInformation("Auxillary DataSource - StorageKey:{0}, FactoryKey:{1}", auxilliaryDataSource.StorageKey, auxilliaryDataSource.FactoryKey);
                        Trace.TraceInformation("About To Get To Auxilliary  Number");
                        mobileAccount = new MobileAccountSystem(auxilliaryDataSource).GetByStandardPhoneNumber(standardPhoneNo);
                        if (mobileAccount == null)
                        {
                            string schemeName = System.Configuration.ConfigurationManager.AppSettings["Scheme"];
                            response.ussdMenu = string.Format("{0} on {1}. {2}", UNREGISTERED_CUSTOMER_MESSAGE_1, schemeName, UNREGISTERED_CUSTOMER_MESSAGE_2);
                            response.shouldClose = true;
                            return response;
                        }

                        else
                        {
                            if (mobileAccount.MobileAccountStatus == MobileAccountStatus.InActive || mobileAccount.MobileAccountStatus == MobileAccountStatus.InActive)
                            {
                                response.ussdMenu = INACTIVE_CUSTOMER_MESSAGE;
                                response.shouldClose = true;
                                return response;
                            }

                            Trace.TraceInformation("About To Get To Start Up Flow");
                            IFlow demoStartFlow = flowRepo.GetStartupFlow(auxilliaryDataSource, mobileAccount.IsAgentAccount, false);
                            Trace.TraceInformation("Gotten Flow Flow is {0}", demoStartFlow.Name);
                            WorkflowInfo prevWorkflowInfo = null;
                            WorkflowInfo theWorkflow = null;
                            try
                            {
                                //get the Start Flow

                                // Trace.TraceInformation(string.Format("flow is {0}", demoStartFlow == null ? "none" : demoStartFlow.Name));


                                if (!String.IsNullOrEmpty(@params.msisdn))
                                {
                                    theWorkflow = WorkflowCenter.Instance.GetInitiatingWorkflowByPhoneNo(standardPhoneNo);
                                    if (theWorkflow != null && theWorkflow.InitiatingFlowID.HasValue)
                                    {
                                        initiatingSessionID = theWorkflow.SessionID.ToString();
                                        prevWorkflowInfo = theWorkflow;
                                        demoStartFlow = flowRepo.GetByUniqueId(auxilliaryDataSource, theWorkflow.InitiatingFlowID.Value);
                                    }
                                }
                            }
                            catch
                            {
                            }
                            try
                            {
                                Trace.TraceInformation("About to start local session");
                                new SessionSystem().StartSession(sessionId, mobileAccount as MobileAccount, initiatingSessionID, auxilliaryDataSource);
                            }
                            catch { }
                            if (!String.IsNullOrWhiteSpace(demoStartFlow.XamlDefinition))
                            {
                                Activity act = null;
                                //get the definition of the workflow from the function property
                                using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(demoStartFlow.XamlDefinition)))
                                {
                                    act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
                                }

                                WorkflowApplication app = null;
                                app = new WorkflowApplication(act);

                                //Check if session was workflow-initiated or user-initiated
                                if (prevWorkflowInfo == null) //user initiated
                                {
                                    app = new WorkflowApplication(act);
                                }
                                else
                                {
                                    app = new WorkflowApplication(act, prevWorkflowInfo.ContextInputs);
                                }

                                Guid newId = app.Id;

                                //TODO: Remove previous workflow from WorkflowCenter Store & Session for caller
                                //Guid oldWorkflowId = Guid.Empty;

                                WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.SetCurrentWorkflow(sessionId, demoStartFlow, app, prevWorkflowInfo);
                                //Get Mobile Account



                                currentWorkflowInfo.InitiatedBy = mobileAccount;
                                //currentWorkflowInfo.TheTenant = theUser.TheTenant;
                                //currentWorkflowInfo.TheTenant = new Tenant() { Code = "UBA", Name = "United Bank of Africa", IsActive = true, DataSourceConfig = @"connection.connection_string~server=.\sqlexpress;initial catalog=Messiah;User ID=sa;password=P@ssw0rd" };
                                Trace.TraceInformation("About To Get To MAIN Session");
                                currentWorkflowInfo.IncludeXML = includeXML;
                                string responseText = currentWorkflowInfo.Start();
                                response.ussdMenu = responseText;
                                response.shouldClose = currentWorkflowInfo.IsFinalScreen;
                                return response;

                            }
                        }
                    }
                }
                else if (mobileAccount.MobileAccountStatus == MobileAccountStatus.AwaitingApproval)
                {
                    string schemeName = System.Configuration.ConfigurationManager.AppSettings["Scheme"];
                    response.ussdMenu = AWAITING_APPROVAL_MESSAGE;
                    response.shouldClose = true;
                    return response;
                }
                bool isNew = mobileAccount.MobileAccountStatus == MobileAccountStatus.New;
                //else if (mobileAccount.MobileAccountStatus != MobileAccountStatus.Active && mobileAccount.MobileAccountStatus != MobileAccountStatus.FundsTransfer)// check for the initiator's details
                //{
                //    response.ussdMenu = "Your Bank One Mobile Account is not active.Please select Activate  Account from the main Menu to access other functions";
                //    response.shouldClose = true;
                //    return response;
                //}

                // log the session against the mobile account for session billing purposes



                Trace.TraceInformation(string.Format("Session Start - session id:{0}, msisdn:{1}, text:{2}, shortCode:{3}, imsi:{4}, optional:{5}", sessionId, @params.msisdn, @params.text, @params.shortCode, @params.imsi, @params.optional));
                try
                {
                    //get the Start Flow
                    IFlow startFlow = null;
                    Trace.TraceInformation("About To Check for Flow");
                    if (mobileAccount.MobileAccountStatus == MobileAccountStatus.PendingFundsTransfer)
                    {
                        Trace.TraceInformation("Mobile Account is not not null-pENDING TRANSFER");
                        startFlow = flowRepo.GetStartupFlowForFundsTransferCustomers(sessionDataSource);
                    }
                    else 
                    {
                        Trace.TraceInformation("Mobile Account is not not null-new account. Flow Repo is ", flowRepo == null ? "Null" : "Not Null");
                        if (mobileAccount.MobileAccountStatus != MobileAccountStatus.New)
                        {
                            //startFlow = new FlowSystem().GetStartUpFlow(mobileAccount);// flowRepo.GetStartupFlow(DataSourceFactory.GetDataSource(DataCategory.Shared), mobileAccount.IsAgentAccount, isNew);
                            startFlow = new FlowSystem(sessionDataSource).GetStartUpFlow(mobileAccount);// flowRepo.GetStartupFlow(DataSourceFactory.GetDataSource(DataCategory.Shared), mobileAccount.IsAgentAccount, isNew);
                        }
                        else
                        {
                            //startFlow = flowRepo.GetStartupFlow(DataSourceFactory.GetDataSource(DataCategory.Shared), mobileAccount.IsAgentAccount, true);
                            startFlow = flowRepo.GetStartupFlow(sessionDataSource, mobileAccount.IsAgentAccount, true);
                        }
                    }
                    Trace.TraceInformation(string.Format("flow is {0}", startFlow == null ? "none" : startFlow.Name));
                    WorkflowInfo prevWorkflowInfo = null;

                    if (!String.IsNullOrEmpty(@params.msisdn))
                    {
                        WorkflowInfo theWorkflow = WorkflowCenter.Instance.GetInitiatingWorkflowByPhoneNo(standardPhoneNo);

                        if (theWorkflow != null && theWorkflow.InitiatingFlowID.HasValue)
                        {
                            initiatingSessionID = theWorkflow.SessionID.ToString();
                            prevWorkflowInfo = theWorkflow;

                            //startFlow = flowRepo.GetByUniqueId(DataSourceFactory.GetDataSource(DataCategory.Shared), theWorkflow.InitiatingFlowID.Value);
                            startFlow = flowRepo.GetByUniqueId(sessionDataSource, theWorkflow.InitiatingFlowID.Value);
                        }
                        if (theWorkflow != null && theWorkflow.ThePreviousWorkflow != null)
                        {
                            initiatingSessionID = theWorkflow.ThePreviousWorkflow.SessionID;
                        }
                    }

                    //TODO : Affordance for Return to main menu
                    try
                    {
                        //new SessionSystem().StartSession(sessionId, mobileAccount as MobileAccount, initiatingSessionID, DataSourceFactory.GetDataSource(DataCategory.Shared));
                        new SessionSystem(sessionDataSource).StartSession(sessionId, mobileAccount as MobileAccount, initiatingSessionID, sessionDataSource);
                    }
                    catch { }

                    //Run the workflow
                    if (!String.IsNullOrWhiteSpace(startFlow.XamlDefinition))
                    {
                        Activity act = null;
                        //get the definition of the workflow from the function property
                        using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(startFlow.XamlDefinition)))
                        {
                            act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
                        }

                        WorkflowApplication app = null;


                        //Check if session was workflow-initiated or user-initiated
                       if (prevWorkflowInfo == null) //user initiated
                        {
                            app = new WorkflowApplication(act);
                        }
                        else
                        {
                            app = new WorkflowApplication(act, prevWorkflowInfo.ContextInputs);
                           
                        }
                       
                        Guid newId = app.Id;

                        //TODO: Remove previous workflow from WorkflowCenter Store & Session for caller
                        //Guid oldWorkflowId = Guid.Empty;

                        WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.SetCurrentWorkflow(sessionId, startFlow, app, prevWorkflowInfo);
                        //Get Mobile Account



                        currentWorkflowInfo.InitiatedBy = mobileAccount;
                        //currentWorkflowInfo.TheTenant = theUser.TheTenant;
                        //currentWorkflowInfo.TheTenant = new Tenant() { Code = "UBA", Name = "United Bank of Africa", IsActive = true, DataSourceConfig = @"connection.connection_string~server=.\sqlexpress;initial catalog=Messiah;User ID=sa;password=P@ssw0rd" };
                        currentWorkflowInfo.IncludeXML = includeXML;
                        string responseText = currentWorkflowInfo.Start();
                        response.ussdMenu = responseText;
                        response.shouldClose = currentWorkflowInfo.IsFinalScreen;
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(string.Format("Error-{0}\nINNER:{1}\nSOURCE:{2}\nSTACK TRACE:{3}",
                        ex.Message, ex.InnerException == null ? "NO INNER" : ex.InnerException.Message, ex.Source, ex.StackTrace));
                }
                Trace.TraceInformation(string.Format("Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));
                return response;
                //}
            }
        }

        public global::menuResponseParams onUserResponse(string sessionId, global::menuRequestParams @params)
        {
            //lock (_lockObject)
            //{
                bool includeXML = false;
                includeXML = string.IsNullOrEmpty(@params.optional) ? false : Convert.ToBoolean(@params.optional);
                global::menuResponseParams response = new global::menuResponseParams();

                if (string.IsNullOrEmpty(sessionId))
                {
                    response.shouldClose = true;
                    response.ussdMenu = "Invalid Session ID";
                    return response;
                }
                else if (CheckUniqueSessionID(sessionId))
                {
                    response.shouldClose = true;
                    response.ussdMenu = "Session ID Does not exist";
                    return response;
                }
                else if (@params == null)
                {
                    response.shouldClose = true;
                    response.ussdMenu = "menuRequestParms is null";
                    return response;

                }
                else if (string.IsNullOrEmpty(@params.msisdn))
                {
                    response.shouldClose = true;
                    response.ussdMenu = "Invalid Phone Number";
                    return response;
                }
                //Trace.TraceInformation(string.Format("User Response - session id:{0}, msisdn:{1}, text:{2}, shortCode:{3}, imsi:{4}, optional:{5}", sessionId, @params.msisdn, @params.text, @params.shortCode, @params.imsi, @params.optional));

                WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
                if (theWorkflowInfo != null)
                {
                    if (@params.text.Equals(RETURN_TO_MAIN_MENU_KEY, StringComparison.CurrentCultureIgnoreCase))
                    {
                        //Cancel previous workflow 
                        var items = WorkflowCenter.Instance.InitiatingWorkflows.ToList().Where(x => x.Value == theWorkflowInfo.TheWorkflow.Id);
                        foreach (var item in items)
                        {
                            Guid wId;
                            WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(item.Key, out wId);
                        }

                        if (theWorkflowInfo.HasPreviousWorkflow)
                        {
                            theWorkflowInfo.ThePreviousWorkflow.InitiatingFlowID = null;
                        }
                        WorkflowCenter.Instance.RemoveWorkflow(sessionId);

                        //Start new workflow
                        response = onSessionStart(sessionId, @params);
                    }
                    else
                    {
                        //Resume the current workflow
                        //Break user text into single requests (delimiter- '*')
                        string[] requests = @params.text.Split('*');
                        foreach (string request in requests)
                        {
                            //if request is blank/empty skip to next request
                            if (String.IsNullOrEmpty(request)) continue;
                            theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
                            theWorkflowInfo.IncludeXML = includeXML;

                            response.ussdMenu = theWorkflowInfo.Resume(request);
                            response.shouldClose = theWorkflowInfo.IsFinalScreen;
                            Trace.TraceInformation(string.Format("Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));

                         
                            //Check if Workflow should goto another flow
                            if (theWorkflowInfo.GotoFlow)
                            {
                                //End Current WorkflowApp
                                theWorkflowInfo.TheWorkflow.Abort();
                                WorkflowInfo removed;
                                WorkflowCenter.Instance.WorkflowInfos.TryRemove(theWorkflowInfo.TheWorkflow.Id, out removed);

                                IFlowRepository flowRepo = SafeServiceLocator<IFlowRepository>.GetService();

                                var startFlow = flowRepo.GetByUniqueId(theWorkflowInfo.DataSource, theWorkflowInfo.InitiatingFlowID.Value);
                                //Run the workflow
                                if (!String.IsNullOrWhiteSpace(startFlow.XamlDefinition))
                                {
                                    Activity act = null;
                                    //get the definition of the workflow from the function property
                                    using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(startFlow.XamlDefinition)))
                                    {
                                        act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
                                    }

                                    WorkflowApplication app = new WorkflowApplication(act, theWorkflowInfo.ContextInputs);

                                    Guid newId = app.Id;

                                    //Clear GOTO parameters
                                    theWorkflowInfo.GotoFlow = false;
                                    theWorkflowInfo.InitiatingFlowID = null;
                                    theWorkflowInfo.ContextInputs = null;
                                    theWorkflowInfo.IsIdle = false;
                                    theWorkflowInfo.IsCompleted = false;
                                    theWorkflowInfo.IsFaulted = false;
                                    theWorkflowInfo.IsFinalScreen = false;

                                    //TODO: Remove previous workflow from WorkflowCenter Store & Session for caller
                                    //Guid oldWorkflowId = Guid.Empty;
                                    theWorkflowInfo.TheWorkflow = app;
                                    theWorkflowInfo = WorkflowCenter.Instance.SetCurrentWorkflow(sessionId, startFlow, app, null, theWorkflowInfo);
                                    //Get Mobile Account

                                    string responseText = theWorkflowInfo.Start();
                                    response.ussdMenu = responseText;
                                    response.shouldClose = theWorkflowInfo.IsFinalScreen;
                                    Trace.TraceInformation(string.Format("Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));
                                }
                            }

                               //Jump out if its last menu/response, cannot continue any further
                            if (response.shouldClose)
                            {
                                Guid guid;
                                WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(@params.msisdn, out guid);
                                break;
                            }

                        }
                    }
                }
                Trace.TraceInformation(string.Format("Final Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));
                return response;
            //}
        }

        # endregion
        public   bool  GenerateActivationCode(string phoneNumber,IDataSource source)
        {
            bool toReturn = false;
             const  string message ="As a result of your request, your phone has been Configured for use as a BankOne Mobile Client. Please Complete the setup with this Activation Code : ";
            
            
            IMobileAccount mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
            if (mobileAccount == null)
            {
                source = DataSourceFactory.GetDataSource(DataCategory.Auxilliary);
                mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
                return false;
            }
            if (mobileAccount == null)
            {
                return false;
            }
            string code = new Random().Next(10000, 99999).ToString();
            string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
            mobileAccount.CustomerID = encryptedCode;
            mobileAccount.ProductCode = code;
            new MobileAccountSystem(source).UpdateMobileAccount(mobileAccount as MobileAccount);
            string smsMessage = message+code;
            toReturn =    SendSMS(mobileAccount as MobileAccount, smsMessage);            
            return toReturn;
        }
        public bool ConfirmActicationCode(string phoneNumber, string activationCode, bool firstTimeVerification)
        {
            phoneNumber = phoneNumber.StartsWith("+") ? phoneNumber : "+" + phoneNumber;//convert to standard i.e. with + and 234

            new PANE.ERRORLOG.Error().LogToFile(new Exception( phoneNumber));
            new PANE.ERRORLOG.Error().LogToFile(new Exception(activationCode));
            bool toReturn = false;             
            //IDataSource source = null;
            using(var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                new PANE.ERRORLOG.Error().LogToFile(new Exception("About to get Account"+phoneNumber));
                IMobileAccount mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
                if (mobileAccount == null)
                {
                    new PANE.ERRORLOG.Error().LogToFile(new Exception("Core is null"));
                }
                else
                {
                    new PANE.ERRORLOG.Error().LogToFile(new Exception("Core is not  null"));
                }
                
            
                //new PANE.ERRORLOG.Error().LogToFile(new Exception("Got from core" + mobileAccount==null?"True":"false"));


                if (mobileAccount == null)
                {
                    var _source = DataSourceFactory.GetDataSource(DataCategory.Auxilliary);
                    mobileAccount = new MobileAccountSystem(_source).GetByStandardPhoneNumber(phoneNumber);
                    return false;
                }
                else if (mobileAccount.CustomerID == new PANE.Framework.Utility.MD5Password().CreateSecurePassword(activationCode))
                {
                    //mobileAccount.CustomerID = null; 
                    //If just verifying the code, meaning we are not sending activation code again
                    //Used with new update where verification code is sent everytime for extra security!!!
                    if (!firstTimeVerification)
                    {
                        return true;
                    }
                    if (firstTimeVerification && mobileAccount.Verified.HasValue && mobileAccount.Verified.Value)
                    {
                        //Already verified
                        return false;
                    }
                    //First time verification - so send activation code
                    var system = new MobileAccountSystem(source);
                    mobileAccount.ProductName = mobileAccount.CustomerID;
                    if (system.SendActivationCode(mobileAccount as MobileAccount))
                    {
                        new PANE.ERRORLOG.Error().LogToFile(new Exception("SMS Sending Returned True" + phoneNumber));
                        mobileAccount.MobileAccountStatus = MobileAccountStatus.New;
                        mobileAccount.Verified = true;
                        system.UpdateMobileAccount(mobileAccount as MobileAccount);
                        return true;
                    }
                    else
                    {
                        new PANE.ERRORLOG.Error().LogToFile(new Exception("SMS Sending Returned False" + phoneNumber ));
                    }
                }
                else
                {
                    new PANE.ERRORLOG.Error().LogToFile(new Exception(string.Format("Customer ID is {0}, secure ID is{1}",mobileAccount.CustomerID,new PANE.Framework.Utility.MD5Password().CreateSecurePassword(activationCode))));

                }
                if (mobileAccount == null)
                {
                    new PANE.ERRORLOG.Error().LogToFile(new Exception("acct is Null" + phoneNumber));
                    return false;
                }
            }
            return toReturn;
        }

        private  bool SendSMS(MobileAccount mob, string message)
        {
            bool toReturn = false;
            string refNo = new TransactionSystem().GenerateTransactionRef();           
            
            string instCode = mob.RecievingBankAccount.InstitutionCode;
            string acctNo = mob.RecievingBankAccount.BankAccount;
            try
            {
                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    SMSService.SMS sms = new SMSService.SMS { AccountNo = acctNo, Body = message, To = mob.StandardPhoneNumber, ReferenceNo = refNo };
                     toReturn =     client.SendSMSBySwitchCodeSingle(instCode, sms);
                    
                }
            }
            catch
            {

            }

            
            return toReturn;
        }


        public bool VerifyActivationCode(string phoneNumber, string activationCode)
        {
            //return ConfirmActicationCode(phoneNumber, activationCode, false);
            return false;
        }
        public bool CheckUniqueSessionID(string sessionID)
        {
            return false;// new SessionSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetBySessionID(sessionID) == null; //TOOK OUT THE SESSION CHECK FOR NOW. LOOK AT IT WHEN DOING USSD
        }
    }
    [System.Web.Services.Protocols.SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    [System.Web.Services.WebService(Namespace = "http://v2.ws.tp.ussd.services.infobip.org/", Name = "UssdThirdPartyWs")]
    public class TpaUSSDWebService : UssdThirdPartyWsPort
    {
        private const string RETURN_TO_MAIN_MENU_KEY = "00";
        private const string UNREGISTERED_CUSTOMER_MESSAGE_1 = "Your phone number is not registered";
        private const string UNREGISTERED_CUSTOMER_MESSAGE_2 = "Please Register with the nearest Agent  to  gain access to this service";
        private const string INACTIVE_CUSTOMER_MESSAGE = "Your phone number is not registered for this service";
        private const string AWAITING_APPROVAL_MESSAGE = "Your mobile account has not yet been approved";

        private SessionInfo SessionInformation { get; set; }
        #region IUssdThirdPartyWsPortBinding Members

        public bool isSesionActive(string sessionId)
        {
            Trace.TraceInformation(string.Format("Is Session Active - session id:{0}", sessionId));
            bool response = false;
            WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
            if (theWorkflowInfo != null)
            {
                if (!theWorkflowInfo.IsCompleted && !theWorkflowInfo.IsFaulted)
                {
                    response = true;
                }
            }
            Trace.TraceInformation(string.Format("Response - isSessionActive:{0}", response));
            return response;
        }

        public void onSessionEnd(string sessionId, int exitCode, bool exitCodeSpecified, string reason)
        {

            Trace.TraceInformation(string.Format("Session End - session id:{0}, exitcode:{1}, exitCodeSpecified:{2}, reason:{3}", sessionId, exitCode, exitCodeSpecified, reason));
            WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
            SessionStatus stat;
            if (theWorkflowInfo != null)
            {
                //TODO: Log Reason and exitCode
                switch (exitCode)
                {
                    case 200: //Session Ended Normally  
                        stat = SessionStatus.EndedNormally;
                        break;
                    case 500: //Session aborted by Network
                        stat = SessionStatus.EndedByNetwork;
                        break;
                    case 510: //Session aborted by TPA
                        stat = SessionStatus.EndedByTPA;
                        break;
                    case 520: //Session aborted by User
                        stat = SessionStatus.EndedByUser;
                        break;
                    case 600: //Session timeout
                        stat = SessionStatus.TimedOut;
                        break;
                    default:
                        stat = SessionStatus.EndedByTPA;
                        break;
                }
                var items = WorkflowCenter.Instance.InitiatingWorkflows.ToList().Where(x => x.Value == theWorkflowInfo.TheWorkflow.Id);
                foreach (var item in items)
                {
                    Guid wId;
                    WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(item.Key, out wId);
                }
                Guid theguid;

                WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(theWorkflowInfo.InitiatedBy.StandardPhoneNumber, out theguid);


                if (theWorkflowInfo.HasPreviousWorkflow)
                {
                    theWorkflowInfo.ThePreviousWorkflow.InitiatingFlowID = null;
                }
                new SessionSystem().StopSession(sessionId, stat, theWorkflowInfo.DataSource);// Log session as ended and set bill amount
                //Stop timer
                //SessionInformation.BillingTimer.Change(Timeout.Infinite, Timeout.Infinite);
                WorkflowCenter.Instance.RemoveWorkflow(sessionId);
            }

        }

        public global::menuResponseParams onSessionStart(string sessionId, global::menuRequestParams @params)
        {
            bool includeXML = false;
            includeXML = string.IsNullOrEmpty(@params.optional) ? false : Convert.ToBoolean(@params.optional);
            global::menuResponseParams response = new global::menuResponseParams();
            if (string.IsNullOrEmpty(sessionId))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Session ID";
                return response;
            }
            //else  if (!CheckUniqueSessionID(sessionId))
            //{
            //    //response.shouldClose = true;
            //    //response.ussdMenu = "Session ID has already been used";
            //    //return response;
            //}
            else if (@params == null)
            {
                response.shouldClose = true;
                response.ussdMenu = "menuRequestParms is null";
                return response;

            }
            else if (string.IsNullOrEmpty(@params.msisdn))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Phone Number";
                return response;
            }
            IFlowRepository flowRepo = SafeServiceLocator<IFlowRepository>.GetService();

            string initiatingSessionID = string.Empty; string standardPhoneNo = @params.msisdn.StartsWith("+") ? @params.msisdn : "+" + @params.msisdn;//convert to standard i.e. with + and 234
            IMobileAccount mobileAccount = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByStandardPhoneNumber(standardPhoneNo);
            if (mobileAccount == null)// check for the initiator's details
            {
                Trace.TraceInformation("About To Get To Auxilliary  Number");
                mobileAccount = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Auxilliary)).GetByStandardPhoneNumber(standardPhoneNo);
                if (mobileAccount == null)
                {
                    string schemeName = System.Configuration.ConfigurationManager.AppSettings["Scheme"];
                    response.ussdMenu = string.Format("{0} on {1}. {2}", UNREGISTERED_CUSTOMER_MESSAGE_1, schemeName, UNREGISTERED_CUSTOMER_MESSAGE_2);
                    response.shouldClose = true;
                    return response;
                }

                else
                {
                    if (mobileAccount.MobileAccountStatus == MobileAccountStatus.InActive || mobileAccount.MobileAccountStatus == MobileAccountStatus.InActive)
                    {
                        response.ussdMenu = INACTIVE_CUSTOMER_MESSAGE;
                        response.shouldClose = true;
                        return response;
                    }
                    //Bill
                    Trace.TraceInformation("About To Get To Start Up Flow");
                    IFlow demoStartFlow = flowRepo.GetStartupFlow(DataSourceFactory.GetDataSource(DataCategory.Auxilliary), mobileAccount.IsAgentAccount, false);
                    Trace.TraceInformation("Gotten Flow Flow is {0}", demoStartFlow.Name);
                    WorkflowInfo prevWorkflowInfo = null;
                    WorkflowInfo theWorkflow = null;
                    try
                    {
                        //get the Start Flow

                        // Trace.TraceInformation(string.Format("flow is {0}", demoStartFlow == null ? "none" : demoStartFlow.Name));


                        if (!String.IsNullOrEmpty(@params.msisdn))
                        {
                            theWorkflow = WorkflowCenter.Instance.GetInitiatingWorkflowByPhoneNo(standardPhoneNo);
                            if (theWorkflow != null && theWorkflow.InitiatingFlowID.HasValue)
                            {
                                initiatingSessionID = theWorkflow.SessionID.ToString();
                                prevWorkflowInfo = theWorkflow;
                                demoStartFlow = flowRepo.GetByUniqueId(DataSourceFactory.GetDataSource(DataCategory.Auxilliary), theWorkflow.InitiatingFlowID.Value);
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        Trace.TraceInformation("About to start local session");
                        //USE  DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Auxilliary)
                        new SessionSystem().StartSession(sessionId, mobileAccount as MobileAccount, initiatingSessionID, DataSourceFactory.GetDataSource(DataCategory.Auxilliary));
                    }
                    catch { }
                    if (!String.IsNullOrWhiteSpace(demoStartFlow.XamlDefinition))
                    {
                        Activity act = null;
                        //get the definition of the workflow from the function property
                        using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(demoStartFlow.XamlDefinition)))
                        {
                            act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
                        }

                        WorkflowApplication app = null;
                        app = new WorkflowApplication(act);

                        //Check if session was workflow-initiated or user-initiated
                        if (prevWorkflowInfo == null) //user initiated
                        {
                            app = new WorkflowApplication(act);
                        }
                        else
                        {
                            app = new WorkflowApplication(act, prevWorkflowInfo.ContextInputs);
                        }

                        Guid newId = app.Id;

                        //TODO: Remove previous workflow from WorkflowCenter Store & Session for caller
                        //Guid oldWorkflowId = Guid.Empty;

                        WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.SetCurrentWorkflow(sessionId, demoStartFlow, app, prevWorkflowInfo);
                        //Get Mobile Account



                        currentWorkflowInfo.InitiatedBy = mobileAccount;
                        //currentWorkflowInfo.TheTenant = theUser.TheTenant;
                        //currentWorkflowInfo.TheTenant = new Tenant() { Code = "UBA", Name = "United Bank of Africa", IsActive = true, DataSourceConfig = @"connection.connection_string~server=.\sqlexpress;initial catalog=Messiah;User ID=sa;password=P@ssw0rd" };
                        Trace.TraceInformation("About To Get To MAIN Session");
                        currentWorkflowInfo.IncludeXML =includeXML;
                        string responseText = currentWorkflowInfo.Start();
                        response.ussdMenu = responseText;
                        response.shouldClose = currentWorkflowInfo.IsFinalScreen;
                        return response;
        # endregion
                    }
                }
            }
            else if (mobileAccount.MobileAccountStatus == MobileAccountStatus.AwaitingApproval)
            {
                string schemeName = System.Configuration.ConfigurationManager.AppSettings["Scheme"];
                response.ussdMenu = AWAITING_APPROVAL_MESSAGE;
                response.shouldClose = true;
                return response;
            }
            bool isNew = mobileAccount.MobileAccountStatus == MobileAccountStatus.New;
            //else if (mobileAccount.MobileAccountStatus != MobileAccountStatus.Active && mobileAccount.MobileAccountStatus != MobileAccountStatus.FundsTransfer)// check for the initiator's details
            //{
            //    response.ussdMenu = "Your Bank One Mobile Account is not active.Please select Activate  Account from the main Menu to access other functions";
            //    response.shouldClose = true;
            //    return response;
            //}

            // log the session against the mobile account for session billing purposes
            //Bill
            #region Start Billing
            SessionInformation = new SessionInfo()
            {
                SessionID = sessionId,
                MobileAccount = mobileAccount as MobileAccount
            };
            //Get period from config
            List<UssdBillingConfiguration> allConfiguration = new List<UssdBillingConfiguration>
                        (new UssdBillingConfigurationSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAll().Cast<UssdBillingConfiguration>());
            if (allConfiguration == null || allConfiguration.Count == 0)
            {
                throw new NoUssdConfigurationException("No configuration found");
            }
            UssdBillingConfiguration configuration = allConfiguration.First();
            int interval = configuration.Period;
            SessionInformation.BillingTimer = new Timer(BillCustomer, SessionInformation, 0, interval);
            if (!SessionInformation.HasDebitedCustomer)
            {
                onSessionEnd(SessionInformation.SessionID, 510, true, "Could not debit");
            }
            #endregion Start Billing
            Trace.TraceInformation(string.Format("Session Start - session id:{0}, msisdn:{1}, text:{2}, shortCode:{3}, imsi:{4}, optional:{5}", sessionId, @params.msisdn, @params.text, @params.shortCode, @params.imsi, @params.optional));
            try
            {
                //get the Start Flow
                IFlow startFlow = null;
                if (mobileAccount.MobileAccountStatus == MobileAccountStatus.PendingFundsTransfer)
                {
                    startFlow = flowRepo.GetStartupFlowForFundsTransferCustomers(DataSourceFactory.GetDataSource(DataCategory.Shared));
                }
                else
                {
                    startFlow = flowRepo.GetStartupFlow(DataSourceFactory.GetDataSource(DataCategory.Shared), mobileAccount.IsAgentAccount, isNew);
                }
                Trace.TraceInformation(string.Format("flow is {0}", startFlow == null ? "none" : startFlow.Name));
                WorkflowInfo prevWorkflowInfo = null;

                if (!String.IsNullOrEmpty(@params.msisdn))
                {
                    WorkflowInfo theWorkflow = WorkflowCenter.Instance.GetInitiatingWorkflowByPhoneNo(standardPhoneNo);

                    if (theWorkflow != null && theWorkflow.InitiatingFlowID.HasValue)
                    {
                        // initiatingSessionID = theWorkflow.SessionID.ToString();
                        prevWorkflowInfo = theWorkflow;

                        startFlow = flowRepo.GetByUniqueId(DataSourceFactory.GetDataSource(DataCategory.Shared), theWorkflow.InitiatingFlowID.Value);
                    }
                    if (theWorkflow != null && theWorkflow.ThePreviousWorkflow != null)
                    {
                        initiatingSessionID = theWorkflow.ThePreviousWorkflow.SessionID;
                    }
                }

                //TODO : Affordance for Return to main menu
                try
                {
                    //USE  DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared)
                    new SessionSystem().StartSession(sessionId, mobileAccount as MobileAccount, initiatingSessionID, DataSourceFactory.GetDataSource(DataCategory.Shared));
                }
                catch { }

                //Run the workflow
                if (!String.IsNullOrWhiteSpace(startFlow.XamlDefinition))
                {
                    Activity act = null;
                    //get the definition of the workflow from the function property
                    using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(startFlow.XamlDefinition)))
                    {
                        act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
                    }

                    WorkflowApplication app = null;


                    //Check if session was workflow-initiated or user-initiated
                    if (prevWorkflowInfo == null) //user initiated
                    {
                        app = new WorkflowApplication(act);
                    }
                    else
                    {
                        app = new WorkflowApplication(act, prevWorkflowInfo.ContextInputs);
                    }

                    Guid newId = app.Id;

                    //TODO: Remove previous workflow from WorkflowCenter Store & Session for caller
                    //Guid oldWorkflowId = Guid.Empty;

                    WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.SetCurrentWorkflow(sessionId, startFlow, app, prevWorkflowInfo);
                    //Get Mobile Account



                    currentWorkflowInfo.InitiatedBy = mobileAccount;
                    //currentWorkflowInfo.TheTenant = theUser.TheTenant;
                    //currentWorkflowInfo.TheTenant = new Tenant() { Code = "UBA", Name = "United Bank of Africa", IsActive = true, DataSourceConfig = @"connection.connection_string~server=.\sqlexpress;initial catalog=Messiah;User ID=sa;password=P@ssw0rd" };
                    currentWorkflowInfo.IncludeXML =includeXML;
                    string responseText = currentWorkflowInfo.Start();
                    response.ussdMenu = responseText;
                    response.shouldClose = currentWorkflowInfo.IsFinalScreen;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation(string.Format("Error-{0}", ex.Message));
            }
            Trace.TraceInformation(string.Format("Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));
            return response;
        }

        public global::menuResponseParams onUserResponse(string sessionId, global::menuRequestParams @params)
        {
            bool includeXML = false;
            includeXML = string.IsNullOrEmpty(@params.optional) ? false : Convert.ToBoolean(@params.optional);
            global::menuResponseParams response = new global::menuResponseParams();
            if (string.IsNullOrEmpty(sessionId))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Session ID";
                return response;
            }
            else if (CheckUniqueSessionID(sessionId))
            {
                response.shouldClose = true;
                response.ussdMenu = "Session ID Does not exist ";
                return response;
            }
            else if (@params == null)
            {
                response.shouldClose = true;
                response.ussdMenu = "menuRequestParms is null";
                return response;

            }
            else if (string.IsNullOrEmpty(@params.msisdn))
            {
                response.shouldClose = true;
                response.ussdMenu = "Invalid Phone Number";
                return response;
            }
            Trace.TraceInformation(string.Format("User Response - session id:{0}, msisdn:{1}, text:{2}, shortCode:{3}, imsi:{4}, optional:{5}", sessionId, @params.msisdn, @params.text, @params.shortCode, @params.imsi, @params.optional));

            WorkflowInfo theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
            if (theWorkflowInfo != null)
            {
                if (@params.text.Equals(RETURN_TO_MAIN_MENU_KEY, StringComparison.CurrentCultureIgnoreCase))
                {
                    //Cancel previous workflow 
                    var items = WorkflowCenter.Instance.InitiatingWorkflows.ToList().Where(x => x.Value == theWorkflowInfo.TheWorkflow.Id);
                    foreach (var item in items)
                    {
                        Guid wId;
                        WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(item.Key, out wId);
                    }

                    if (theWorkflowInfo.HasPreviousWorkflow)
                    {
                        theWorkflowInfo.ThePreviousWorkflow.InitiatingFlowID = null;
                    }
                    WorkflowCenter.Instance.RemoveWorkflow(sessionId);

                    //Start new workflow
                    response = onSessionStart(sessionId, @params);
                }
                else
                {
                    //Resume the current workflow
                    //Break user text into single requests (delimiter- '*')
                    string[] requests = @params.text.Split('*');
                    foreach (string request in requests)
                    {
                        
                        //if request is blank/empty skip to next request
                        if (String.IsNullOrEmpty(request)) continue;
                        theWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionId);
                        theWorkflowInfo.IncludeXML =includeXML;
                        response.ussdMenu = theWorkflowInfo.Resume(request);
                        response.shouldClose = theWorkflowInfo.IsFinalScreen;

                        //Jump out if its last menu/response, cannot continue any further
                        if (response.shouldClose)
                        {
                            Guid guid;
                            WorkflowCenter.Instance.InitiatingWorkflows.TryRemove(@params.msisdn, out guid);
                            break;
                        }
                    }
                }
            }
            Trace.TraceInformation(string.Format("Response - ussdMenu:{0}, shouldClose:{1}", response.ussdMenu, response.shouldClose));
            return response;
        }
        public bool GenerateActivationCode(string phoneNumber, IDataSource source)
        {
            bool toReturn = false;
            const string message = "As a result of your request, your phone has been Configured for use as a BankOne Mobile Client. Please Complete the setup with this Activation Code : ";


            IMobileAccount mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
            if (mobileAccount == null)
            {
                source = DataSourceFactory.GetDataSource(DataCategory.Auxilliary);
                mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
                return false;
            }
            if (mobileAccount == null)
            {
                return false;
            }
            string code = new Random().Next(10000, 99999).ToString();
            string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
            mobileAccount.CustomerID = encryptedCode;
            mobileAccount.ProductCode = code;
            new MobileAccountSystem(source).UpdateMobileAccount(mobileAccount as MobileAccount);
            string smsMessage = message + code;
            toReturn = SendSMS(mobileAccount as MobileAccount, smsMessage);
            return toReturn;
        }
        public bool ConfirmActicationCode(string phoneNumber, string activationCode)
        {
            new PANE.ERRORLOG.Error().LogToFile(new Exception(phoneNumber));
            bool toReturn = false;
            IDataSource source = null;
            source = DataSourceFactory.GetDataSource(DataCategory.Core);

            IMobileAccount mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
            new PANE.ERRORLOG.Error().LogToFile(new Exception("Got from core" + mobileAccount == null ? "True" : "false"));
            if (mobileAccount == null)
            {
                source = DataSourceFactory.GetDataSource(DataCategory.Auxilliary);
                mobileAccount = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
                return false;
            }
            else if (mobileAccount.CustomerID == new PANE.Framework.Utility.MD5Password().CreateSecurePassword(activationCode))
            {
                mobileAccount.CustomerID = null;
                new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).UpdateMobileAccount(mobileAccount as MobileAccount);
                return true;
            }
            else
            {
                new PANE.ERRORLOG.Error().LogToFile(new Exception(string.Format("Customer ID is {0}, secure ID is{1}", mobileAccount.CustomerID, new PANE.Framework.Utility.MD5Password().CreateSecurePassword(activationCode))));

            }
            if (mobileAccount == null)
            {
                new PANE.ERRORLOG.Error().LogToFile(new Exception("acct is Null" + phoneNumber));
                return false;
            }

            return toReturn;
        }

        private bool SendSMS(MobileAccount mob, string message)
        {
            bool toReturn = false;
            string refNo = new TransactionSystem().GenerateTransactionRef();

            string instCode = mob.RecievingBankAccount.InstitutionCode;
            string acctNo = mob.RecievingBankAccount.BankAccount;
            try
            {
                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    SMSService.SMS sms = new SMSService.SMS { AccountNo = acctNo, Body = message, To = mob.StandardPhoneNumber, ReferenceNo = refNo };
                    toReturn = client.SendSMSBySwitchCodeSingle(instCode, sms);

                }
            }
            catch
            {

            }


            return toReturn;
        }

        //private void StartTimer(SessionInfo info)
        //{            
        //    info.BillingTimer = new Timer(new TimerCallback(BillCustomer), info, 0, Timeout.Infinite);
        //}

        private void BillCustomer(object state)
        {
            SessionInfo sessionInfo = state as SessionInfo;
            try
            {
                if(!isSesionActive(sessionInfo.SessionID))
                {
                    throw new Exception();
                }
                //Reset HasDebited
                sessionInfo.HasDebitedCustomer = false;
                //Bill the customer
                //Get amount configured
                List<UssdBillingConfiguration> allConfiguration = new List<UssdBillingConfiguration>
                    (new UssdBillingConfigurationSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAll().Cast<UssdBillingConfiguration>());
                if (allConfiguration == null || allConfiguration.Count == 0)
                {
                    throw new NoUssdConfigurationException("No configuration found");
                }
                UssdBillingConfiguration configuration = allConfiguration.First();
                decimal amount = configuration.Amount;
                // account for credit is debiting account of mobile account
                MobileAccount mob = sessionInfo.MobileAccount;
                bool response = MobileAccountSystem.DebitAccountForUSSD(mob);
                if (response)
                {
                    //Continue timer
                    sessionInfo.HasDebitedCustomer = true;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                //Stop timer, end session
                if(isSesionActive(sessionInfo.SessionID))
                {
                    onSessionEnd(SessionInformation.SessionID, 510, true, "Could not debit account");
                }
                sessionInfo.HasDebitedCustomer = false;
                sessionInfo.BillingTimer.Change(Timeout.Infinite, Timeout.Infinite);
                sessionInfo.BillingTimer.Dispose();
            }
        }
        public bool VerifyActivationCode(string phoneNumber, string activationCode)
        {
            return ConfirmActicationCode(phoneNumber, activationCode);
        }
        public bool CheckUniqueSessionID(string sessionID)
        {
            return new SessionSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetBySessionID(sessionID) == null;
        }        
    }

}
