﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using System.Data.SqlTypes;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using System.Configuration;
using System.IO;
using System.Net;

namespace BankOneMobile.Services
{
  public  class SessionSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ISessionRepository _repository = SafeServiceLocator<ISessionRepository>.GetService();
        public SessionSystem()
        {
        }
        public SessionSystem(IDataSource dataSorce,ISessionRepository repository)
        {
            _theDataSource = dataSorce;
            _repository = repository;
        }
        public SessionSystem(IDataSource dataSorce)
        {
            _theDataSource = dataSorce;
            
        }
        public SessionSystem(ISessionRepository repository)
        {
            
            _repository = repository;
        }
        public  ISession SaveSession(Session theSession)
        {
            theSession.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theSession);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  ISession UpdateSession(Session theSession)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theSession);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  ISession GetSession(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public  List<ISession> GetAllSessions()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

       
        public  List<ISession> FindSessions(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
        public  ISession GetBySessionID(string sessionID)
        {
            return _repository.GetBySessionID(_theDataSource,sessionID);

        }
      public  bool StartSession(string sessionID,MobileAccount mob,string initiatingSessionID,IDataSource dataSource)
      {
          // Save the session with all the required properties
          Session session = new Session();
          Session initiatingSession = null;
          Session checkSession = new SessionSystem(dataSource).GetBySessionID(sessionID) as Session;
          if (checkSession != null && checkSession.Status == SessionStatus.Started)
          {
              //throw new InvalidSessionException("This session has already began");
              return true;
          }
          else if (checkSession != null)
          {
              //throw new InvalidSessionException("Session ID already exists");
              return true;

          }

          
          try
          {
              if (!string.IsNullOrEmpty(initiatingSessionID))
              {

                  initiatingSession = new SessionSystem(dataSource).GetBySessionID(initiatingSessionID) as Session;

                  if (initiatingSession == null)
                  {
                      throw new InvalidSessionException("No Session With this ID");
                  }
                  //else if (initiatingSession.Status != SessionStatus.Started)
                  //{
                  //    throw new InvalidSessionException("The Initating Session has already ended");
                  //}
                  else
                  {
                      initiatingSession.ChildSession = session;
                      session.Payer = initiatingSession.MobilePhoneNumber;
                      session.PayerAccountNumber = initiatingSession.PayerAccountNumber;
                      session.PayerInstitutionCode = initiatingSession.PayerInstitutionCode;
                      session.MobileAccountType = initiatingSession.MobileAccountType;
                      session.PayerInstitutionCode = mob.RecievingBankAccount.InstitutionCode;

                  }


              }
              else
              {
                  session.Payer = mob.MobilePhone;
                  session.PayerAccountNumber = mob.RecievingBankAccount.BankAccount;
                  session.PayerInstitutionCode = mob.RecievingBankAccount.InstitutionCode;
                  session.MobileAccountType = mob.TheMobileAccountType;
                  session.PayerInstitutionCode = mob.RecievingBankAccount.InstitutionCode;
              }
              
             
              session.SavedOnCoreBanking = false;
              session.Status = SessionStatus.Started;
              session.SessionID = sessionID;
              session.MobilePhoneNumber = mob.MobilePhone;
              session.StartTime = DateTime.Now;
              session.StopTime = DateTime.Now;

              //if(initiatingSession==null)
              //{
              //    new SessionSystem(dataSource).SaveSession(session);
              //}
              //else 
              //{
              //   new SessionSystem(dataSource). UpdateSession(initiatingSession);
              //}
              try
              {
               //   SaveSession(session);
              }
              catch
              {
                 // UpdateSession(initiatingSession);
              }

          }
          catch
          {
          }
          finally
          {
              //_repository.DbContext.CommitChanges(dataSource);
              //_repository.DbContext.Close(dataSource);
          }
         
          
          return true;    
      }
      public bool StopSession(string sessionID, SessionStatus status, IDataSource dataSource)
      {
          //IDataSource dataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
          bool toReturn = false;
          StringBuilder builder = new StringBuilder();

          Session checkSession = new SessionSystem(dataSource).GetBySessionID(sessionID) as Session;
          if (checkSession == null)
          {
              dataSource = DataSourceFactory.GetDataSource(DataCategory.Auxilliary);
              checkSession = new SessionSystem(dataSource).GetBySessionID(sessionID) as Session;
          }
          if (checkSession == null)
          {
              throw new InvalidSessionException("Invalid Session ID");
          }
          //else if (checkSession != null&&checkSession.Status!=SessionStatus.Started) 
          //{
          //    throw new InvalidSessionException("Session has already been stopped");
          //}
          checkSession.StopTime = DateTime.Now;
          checkSession.Status = status;
          //checkSession.Amount = checkSession.SessionLength * Configuration.ConfigurationManager.AmountPerSession;
         
            //  builder.AppendFormat("Payment For USSD Network Session  used at {0} for  the  {1} Transaction that lasted for {2} seconds",checkSession.StartTime, checkSession.TheTransaction!=null?checkSession.TheTransaction.TransactionTypeName.ToString():string.Empty, checkSession.TimeInterval);

              checkSession.TimeInterval = (checkSession.StopTime -checkSession. StartTime).Seconds;


              if (checkSession.TimeInterval % Configuration.ConfigurationManager.USSDSessionInterval == 0)
              {
                  checkSession.SessionLength= Convert.ToInt64( checkSession.TimeInterval / Configuration.ConfigurationManager.USSDSessionInterval);
              }
              else
              {
                  checkSession.SessionLength =Convert.ToInt64( checkSession.TimeInterval / Configuration.ConfigurationManager.USSDSessionInterval + 1);
              }
                
          checkSession.BillingDescription= builder.ToString();
          checkSession.SavedOnCoreBanking = LogBillingDetailsOnCoreBanking(checkSession,dataSource);

          checkSession.Payer = GetPayer(checkSession);
          checkSession.PayerAccountNumber = GetPayerAccountNumber(checkSession);
          
          new SessionSystem(dataSource).UpdateSession(checkSession);
          
          toReturn = true;
          //TODO Set Session status

          _repository.DbContext.Close(dataSource);
          return toReturn;
      }
      public bool LogBillingDetailsOnCoreBanking(Session session,IDataSource dataSource)
      {
          bool toReturn = false;
          if (session.Status == SessionStatus.Started)
          {
              throw new SessionNotEndedException("Session Is still Active");
          }
          # region Core Bankin Web  SERVICE Call
          //    TODO call the right core banking  Service
           toReturn = true;
         new SessionSystem(dataSource). UpdateSession(session);
          toReturn = true;
# endregion
        
          return toReturn;
      }
      public string GetPayer(Session session)
      {
          string toReturn = string.Empty;
          if (session.ChildSession == null)// Means not initiated by agent. who go pay before?
          {
              toReturn =session. MobilePhoneNumber;
          }
          else if (session.ChildSession.Status == SessionStatus.EndedByUser||session.ChildSession.Status==SessionStatus.EndedNormally)// Means initiated by agent . and ended succesfully customer is payer for the whole sessionFeeTransactionType
          {
              toReturn = session.ChildSession.MobilePhoneNumber;
          }
          else if (session.ChildSession.Status != SessionStatus.Started)//Means ended unlawfully.hmmmm agent go try pay
          {
              toReturn = session.MobilePhoneNumber;
          }
          else// Means Child session is still running we may need to re look at this
          {
              toReturn = session.MobilePhoneNumber;
          }
          return toReturn;
          
      }
      public string GetPayerAccountNumber(Session session)
      {
          string toReturn = string.Empty;
          if (session.ChildSession == null)// Means not initiated by agent. who go pay before?
          {
              toReturn = session.PayerAccountNumber;
          }
          else if (session.ChildSession.Status == SessionStatus.EndedByUser || session.ChildSession.Status == SessionStatus.EndedNormally)// Means initiated by agent . and ended succesfully customer is payer for the whole sessionFeeTransactionType
          {
              toReturn = session.ChildSession.PayerAccountNumber;
          }
          else if (session.ChildSession.Status != SessionStatus.Started)//Means ended unlawfully.hmmmm agent go try pay
          {
              toReturn = session.PayerAccountNumber;
          }
          else// Means Child session is still running we may need to re look at this
          {
              toReturn = session.PayerAccountNumber;
          }
          return toReturn;

      }
      


      public int AddNumbers(int num1, int num2)
      {
          return num1 + num2;
      }

      public IList<Session> GetSessionsForADay(DateTime dt)
      {
          throw new NotImplementedException();
      }


      public IList<Session> GetSessionBetweenDates(DateTime startDate, DateTime stopDate)
      {
          IList<Session> toReturn = new List<Session>();
          using (IDataSource source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
          {
              toReturn = _repository.GetSessionsBetweenDates(source, startDate, stopDate).Cast<Session>().ToList();// IList<Session>;
          }
          return toReturn;
      }


      public static string  DoHttpRequest(string url, string postData)
      {
          string strIcrawlURL = url;

          WebRequest mObjWebRequest = null;

          WebResponse mObjWebResponse = null;

          Stream mObjReceiveStream = null;

          string strResponse = string.Empty;

          // postData = "OPERATION=START&MSISDN=2348033333333&SESSION_ID=13254658758563";
          byte[] buffer = Encoding.UTF8.GetBytes(postData);

          try
          {
              // mObjWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
              mObjWebRequest = WebRequest.Create(strIcrawlURL);
              mObjWebRequest.Method = "POST";
              mObjWebRequest.ContentType = "application/x-www-form-urlencoded";
              mObjWebRequest.ContentLength = buffer.Length;

              Stream reqst = mObjWebRequest.GetRequestStream(); // add form data to request stream
              reqst.Write(buffer, 0, buffer.Length);
              reqst.Flush();
              reqst.Close();


              //// Use the default credential for accessing the page



              //// Send the 'WebRequest' and wait for response.

              mObjWebResponse = mObjWebRequest.GetResponse();

              //// Obtain a 'Stream' object associated with the response object.

              mObjReceiveStream = mObjWebResponse.GetResponseStream();

              //// Pipe the stream to a higher level stream reader with the required encoding format.

              StreamReader readStream = new StreamReader(mObjReceiveStream, System.Text.Encoding.GetEncoding("utf-8"));

              strResponse = readStream.ReadToEnd();

              readStream.Close();

              //////// Release the resources of response object.

              mObjWebResponse.Close();
              return strResponse;

          }

          catch (Exception ex)
          {
              //// Do something here for exception raised if thro

              return "Error " + ex.Message;


          }

          finally
          {

              mObjWebRequest = null;

              mObjWebResponse = null;

              mObjReceiveStream = null;

          }
         
      }
    }
}
