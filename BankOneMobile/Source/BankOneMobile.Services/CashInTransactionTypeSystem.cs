﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class CashInTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ICashInTransactionTypeRepository _repository = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService();
        public List<CashInTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string agentCode,string custPhone, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<CashInTransactionType> tr = new List<CashInTransactionType>();

            TransactionStatus? transStatus = null;
            if (!string.IsNullOrEmpty(status))
            {
                transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
            }
            if (!string.IsNullOrEmpty(agentCode) && !string.IsNullOrEmpty(agentCode.Trim()))
            {
                dic.Add("AgentCode", agentCode);
            }
            //if (!string.IsNullOrEmpty(custPhone) && !string.IsNullOrEmpty(custPhone.Trim()))
            //{
            //    new CashInTransactionType().
            //    dic.Add("AgentCode", agentCode);

            //}
            

            //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = _repository.FindTransactionsWithPaging(_theDataSource, institutionCode, transStatus, phoneNum,custPhone, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;

        }
        public static ICashInTransactionType SaveCashInTransactionType(IDataSource dataSource, CashInTransactionType theCashInTransactionType)
        {
            theCashInTransactionType.IsActive = false;
            var repo = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theCashInTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static ICashInTransactionType UpdateCashInTransactionType(IDataSource dataSource, CashInTransactionType theCashInTransactionType)
        {
            var repo = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theCashInTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static ICashInTransactionType GetCashInTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<ICashInTransactionType> GetAllCashInTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<ICashInTransactionType> GetActiveCashInTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<ICashInTransactionType> FindCashInTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }
        
    }
}
