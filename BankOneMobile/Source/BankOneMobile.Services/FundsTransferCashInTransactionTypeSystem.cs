﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class FundsTransferCashInTransactionTypeSystem
    {

      public IFundsTransferCashInTransactionType SaveFundsTransferCashInTransactionType(IDataSource dataSource, FundsTransferCashInTransactionType theFundsTransferCashInTransactionType)
        {
            theFundsTransferCashInTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
                var result = repo.Save(dataSource, theFundsTransferCashInTransactionType);
                repo.DbContext.CommitTransaction(dataSource);
            //SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().DbContext.CommitChanges(dataSource);
            return result;

        }

      public IFundsTransferCashInTransactionType UpdateFundsTransferCashInTransactionType(IDataSource dataSource, FundsTransferCashInTransactionType theFundsTransferCashInTransactionType)
        {
            var repo = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theFundsTransferCashInTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public IFundsTransferCashInTransactionType GetFundsTransferCashInTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public  List<IFundsTransferCashInTransactionType> GetAllFundsTransferCashInTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public  List<IFundsTransferCashInTransactionType> GetActiveFundsTransferCashInTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public List<IFundsTransferCashInTransactionType> FindFundsTransferCashInTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }

        public string GenerateToken()
        {
            return new Random().Next(10000, 99999).ToString();
        }
        public IList<IFundsTransferCashInTransactionType> GetByBeneficiaryPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().GetByPhoneNumber(dataSource, phoneNumber);
            return result;

        }
        public IFundsTransferCashInTransactionType GetByBeneficiaryToken(IDataSource dataSource, string token)
        {
            var result = SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().GetByToken(dataSource,token);
            return result;

        }
    }
}
