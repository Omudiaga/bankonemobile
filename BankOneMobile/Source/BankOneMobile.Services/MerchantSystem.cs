﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using System.Data.SqlTypes;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class MerchantSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private IMerchantRepository _repository = SafeServiceLocator<IMerchantRepository>.GetService();

      public MerchantSystem()
       {

       }

      public MerchantSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        

        public IMerchant UpdateMerchant(IMerchant theMerchant)
        {
            IMerchant prod = GetMerchantByName(theMerchant.Name);
            if (prod != null && prod.ID != theMerchant.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            prod = GetMerchantByCode(theMerchant.Code);
            if (prod != null && prod.ID != theMerchant.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            //MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, theMerchant.PhoneNumber) as MobileAccount;
            //if (existingAcct != null && theMerchant.MobileAccount.ID != existingAcct.ID)
            //{
            //    throw new ApplicationException("Mobile Number already exists");
            //}
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theMerchant);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
            

        }

        public IMerchant GetMerchant(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;
            
        }
        public IMerchant GetMerchantByCode(string code)
        {
            var result = _repository.GetByCode(_theDataSource,code);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IMerchant GetMerchantByName(string name)
        {
            var result = _repository.GetByName(_theDataSource, name);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IList<MerchantCategory> GetMerchantCategories()
        {
            IList<MerchantCategory> toReturn = new List<MerchantCategory>();

            try
            {
                using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
                {
                    foreach (ISWGatewayService.BillerCategory billCat in client.GetBillerCategories())
                    {
                        toReturn.Add(new MerchantCategory { Name = billCat.Name, Code = billCat.BillerCategoryID });
                    }
                }
            }
            catch(Exception ex)
            {
                throw new NoISWGatewayWebServiceResponseException(ex.Message);
            }

           // toReturn.Add(new MerchantCategory { Code = "10", Name = "Cable TV" });
            //TODO call the right web service

            return toReturn;
        }
        public IList<Merchant> GetMerchantByCategory(MerchantCategory cat)
        {
            IList<Merchant> toReturn = new List<Merchant>();
            try

            {
                //new ISWGatewayService.ServiceClient().
               // new ISWGatewayService.ServiceClient().GetBillerCategories()[0].
            using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
            {

               
                    foreach (ISWGatewayService.Biller biller in client.GetBillers(cat.Code))
                    {
                        toReturn.Add(new Merchant { Code = biller.BillerId, Name = biller.Name, CustomerIDField=biller.CustomerField1  });

                    }
             }
               
            }
            catch (Exception ex)
            {
                throw new NoISWGatewayWebServiceResponseException(ex.Message);
            }       // toReturn.Add(new Merchant { Name = "DSTV", Code = "104" });
            //TODO Call the right web service
            return toReturn;
        }
        public IList<PaymentItem> GetPaymentItemsByMerchant(Merchant merch)
        {
            IList<PaymentItem> toReturn = new List<PaymentItem>();
            try
                {
            using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
            {
                
                    foreach (ISWGatewayService.PaymentItem item in client.GetBillPaymentItems(merch.Code))
                    {
                        toReturn.Add(new PaymentItem { Code = item.PaymentItemID, CustomerIDName = item.CustomerIDName, Name = item.Name, UnitPrice = Convert.ToDecimal(item.Amount), TheMerchant = merch });
                    }
                }
               
            }
            catch (Exception ex)
            {
                throw new NoISWGatewayWebServiceResponseException(ex.Message);
            }
          //  toReturn.Add(new PaymentItem { Code = "01", UnitPrice = 5000M, Name = "Premium Bouquet" });
            //TODO call the right web service
            return toReturn;
        }
        public List<IMerchant> GetAllMerchants()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IMerchant> GetActiveMerchants()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
      

        public List<IMerchant> FindMerchants(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
        public Merchant EnableDisableMerchant(Merchant theMerchant)
        {
            if (theMerchant.IsActive)
            {
                theMerchant.IsActive = false;
            }
            else
            {
                theMerchant.IsActive = true;
            }
            UpdateMerchant(theMerchant as IMerchant);
            return theMerchant;
        }



        public Merchant GetRechargeMerchantByPhoneNumber(string phoneNumber)
        {

            string rechargeCategoryID = ISWInterface.Recharge.GetRechargeBillerCategory();
            string merchantCode =ISWInterface.Recharge.GetMerchantCodeByPhoneNumber(phoneNumber);
            ISWGatewayService.Biller biller = null;

            try
            {
                using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
                {
                    biller = client.GetBillers(rechargeCategoryID).Single(x => x.BillerId == merchantCode);
                }

            }
            catch (Exception ex)
            {
                throw new NoISWGatewayWebServiceResponseException(ex.Message);
            }
            return new Merchant{ Name=biller.Name,Code=biller.BillerId};
        }

        public IList<Merchant> GetMerchantsByCategoryID(string categoryID)
        {
            IList<Merchant> toReturn = new List<Merchant>();

            try
            {
                using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
                {
                    foreach (ISWGatewayService.Biller biller in client.GetBillers(categoryID))
                    {
                        toReturn.Add(new Merchant { Code = biller.BillerId, Name = biller.Name, CustomerIDField = biller.CustomerField1 });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NoISWGatewayWebServiceResponseException(ex.Message);
            }

            // toReturn.Add(new MerchantCategory { Code = "10", Name = "Cable TV" });
            //TODO call the right web service

            return toReturn;
        }

        public IList<Merchant> GetRechargeMerchants()
        {
            return GetMerchantsByCategoryID(ISWInterface.Recharge.GetRechargeBillerCategory());
        }
    }
}
