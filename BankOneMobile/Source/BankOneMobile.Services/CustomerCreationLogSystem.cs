﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Services
{
   public class CustomerCreationLogSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
         private ICustomerCreationLogRepository _repository = SafeServiceLocator<ICustomerCreationLogRepository>.GetService();

       public CustomerCreationLogSystem()
       {

       }

       public CustomerCreationLogSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }


       public ICustomerCreationLog Save(ICustomerCreationLog request)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, request);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ICustomerCreationLog Update(ICustomerCreationLog request)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, request);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ICustomerCreationLog Get(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

       public List<ICustomerCreationLog> FindRequestsBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

       internal ICustomerCreationLog GetByAccountNumber(string accountNumber)
       {
           ICustomerCreationLog result = _repository.GetByAccountNumber(_theDataSource, accountNumber);
           return result;
       }
    }
}
