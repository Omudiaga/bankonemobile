﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using System.Data.SqlTypes;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class CommercialBankSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private ICommercialBankRepository _repository = SafeServiceLocator<ICommercialBankRepository>.GetService();

      public CommercialBankSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
      public ICommercialBank SaveCommercialBank(ICommercialBank theCommercialBank)
        {

            _repository.DbContext.BeginTransaction(_theDataSource);
            if (GetCommercialBankByName(theCommercialBank.Name) != null)
            {
                throw new ApplicationException("Name Already Exists");
            }
            if (GetCommercialBankByCode(theCommercialBank.Code) != null)
            {
                throw new ApplicationException("Code Already Exists");
            }
            var result = _repository.Save(_theDataSource, theCommercialBank);
            _repository.DbContext.CommitTransaction(_theDataSource);
            
            return result;
            

        }

      public ICommercialBank UpdateCommercialBank(ICommercialBank theCommercialBank)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            ICommercialBank prod = GetCommercialBankByName(theCommercialBank.Name);
            if (prod != null && prod.ID != theCommercialBank.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            prod = GetCommercialBankByCode(theCommercialBank.Code);
            if (prod != null && prod.ID != theCommercialBank.ID)
            {
                throw new ApplicationException("Name already exists");
            }
            //MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, theCommercialBank.PhoneNumber) as MobileAccount;
            //if (existingAcct != null && theCommercialBank.MobileAccount.ID != existingAcct.ID)
            //{
            //    throw new ApplicationException("Mobile Number already exists");
            //}
            var result = _repository.Update(_theDataSource, theCommercialBank);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
            

        }

      public ICommercialBank GetCommercialBank(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;
            
        }
      public ICommercialBank GetCommercialBankByCode(string code)
        {
            var result = _repository.GetByCode(_theDataSource,code);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
      public ICommercialBank GetCommercialBankByName(string name)
        {
            var result = _repository.GetByName(_theDataSource, name);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

      public List<ICommercialBank> GetAllCommercialBanks()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

      public List<ICommercialBank> GetActiveCommercialBanks()
      {
          var result = _repository.GetActiveCommercialBanks(_theDataSource);
          result = result.Where(x => x.IsActive == true).ToList();

          //Do not do this again
          //_repository.DbContext.Close(_theDataSource);
          return result.ToList();
      }

      public List<ICommercialBank> FindCommercialBanks(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
      public CommercialBank EnableDisableCommercialBank(CommercialBank theCommercialBank)
        {
            if (theCommercialBank.IsActive)
            {
                theCommercialBank.IsActive = false;
            }
            else
            {
                theCommercialBank.IsActive = true;
            }
            UpdateCommercialBank(theCommercialBank);
            return theCommercialBank;
        }




      
      
      
    }
}
