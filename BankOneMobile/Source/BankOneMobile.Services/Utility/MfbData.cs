﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;

/// <summary>
/// Summary description for Utriilities
/// </summary>
public class MfbData
{
    private const string SS_MFBCODE = "::SS_INSTITUTION_CODE::";
    private const string SS_MFBINSTID = "::SS_MFBINSTID::";
    //[System.Diagnostics.DebuggerStepThroughAttribute]
    public static string MFBCode
    {
        get
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ManagedServicesTestMode"]))
            {
                return "LAWYERS";
            }
            else  if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                HttpContext.Current.Session[SS_MFBCODE] = HttpContext.Current.User.Identity.Name.Split(':')[1];
            }

            return Convert.ToString(HttpContext.Current.Session[SS_MFBCODE]);
        }
        set
        {
            HttpContext.Current.Session[SS_MFBCODE] = value;
        }
    }
    
	public MfbData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}