﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankOneMobile.Services.BankOneIncomeAccountService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AppZoneIncomeAccount", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Data")]
    [System.SerializableAttribute()]
    public partial class AppZoneIncomeAccount : BankOneMobile.Services.BankOneIncomeAccountService.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AccountNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitutionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountNumber {
            get {
                return this.AccountNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.AccountNumberField, value) != true)) {
                    this.AccountNumberField = value;
                    this.RaisePropertyChanged("AccountNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitution {
            get {
                return this.TheInstitutionField;
            }
            set {
                if ((object.ReferenceEquals(this.TheInstitutionField, value) != true)) {
                    this.TheInstitutionField = value;
                    this.RaisePropertyChanged("TheInstitution");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataObject", Namespace="http://schemas.datacontract.org/2004/07/PANE.Framework.DTO")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.Services.BankOneIncomeAccountService.Institution))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount))]
    public partial class DataObject : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsDeletedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool LogObjectField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MFBCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool UseAuditTrailField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsDeleted {
            get {
                return this.IsDeletedField;
            }
            set {
                if ((this.IsDeletedField.Equals(value) != true)) {
                    this.IsDeletedField = value;
                    this.RaisePropertyChanged("IsDeleted");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool LogObject {
            get {
                return this.LogObjectField;
            }
            set {
                if ((this.LogObjectField.Equals(value) != true)) {
                    this.LogObjectField = value;
                    this.RaisePropertyChanged("LogObject");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MFBCode {
            get {
                return this.MFBCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.MFBCodeField, value) != true)) {
                    this.MFBCodeField = value;
                    this.RaisePropertyChanged("MFBCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool UseAuditTrail {
            get {
                return this.UseAuditTrailField;
            }
            set {
                if ((this.UseAuditTrailField.Equals(value) != true)) {
                    this.UseAuditTrailField = value;
                    this.RaisePropertyChanged("UseAuditTrail");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Institution", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Data")]
    [System.SerializableAttribute()]
    public partial class Institution : BankOneMobile.Services.BankOneIncomeAccountService.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DbCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.Institution ParentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<long> ParentIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ShortNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.MFBStatus StatusField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Code {
            get {
                return this.CodeField;
            }
            set {
                if ((object.ReferenceEquals(this.CodeField, value) != true)) {
                    this.CodeField = value;
                    this.RaisePropertyChanged("Code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DbCode {
            get {
                return this.DbCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.DbCodeField, value) != true)) {
                    this.DbCodeField = value;
                    this.RaisePropertyChanged("DbCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.Institution Parent {
            get {
                return this.ParentField;
            }
            set {
                if ((object.ReferenceEquals(this.ParentField, value) != true)) {
                    this.ParentField = value;
                    this.RaisePropertyChanged("Parent");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<long> ParentID {
            get {
                return this.ParentIDField;
            }
            set {
                if ((this.ParentIDField.Equals(value) != true)) {
                    this.ParentIDField = value;
                    this.RaisePropertyChanged("ParentID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ShortName {
            get {
                return this.ShortNameField;
            }
            set {
                if ((object.ReferenceEquals(this.ShortNameField, value) != true)) {
                    this.ShortNameField = value;
                    this.RaisePropertyChanged("ShortName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.MFBStatus Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SubscriptionExpenseAccount", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Data")]
    [System.SerializableAttribute()]
    public partial class SubscriptionExpenseAccount : BankOneMobile.Services.BankOneIncomeAccountService.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AccountNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitutionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountNumber {
            get {
                return this.AccountNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.AccountNumberField, value) != true)) {
                    this.AccountNumberField = value;
                    this.RaisePropertyChanged("AccountNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitution {
            get {
                return this.TheInstitutionField;
            }
            set {
                if ((object.ReferenceEquals(this.TheInstitutionField, value) != true)) {
                    this.TheInstitutionField = value;
                    this.RaisePropertyChanged("TheInstitution");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SmsExpenseAccount", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Data")]
    [System.SerializableAttribute()]
    public partial class SmsExpenseAccount : BankOneMobile.Services.BankOneIncomeAccountService.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AccountNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitutionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountNumber {
            get {
                return this.AccountNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.AccountNumberField, value) != true)) {
                    this.AccountNumberField = value;
                    this.RaisePropertyChanged("AccountNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitution {
            get {
                return this.TheInstitutionField;
            }
            set {
                if ((object.ReferenceEquals(this.TheInstitutionField, value) != true)) {
                    this.TheInstitutionField = value;
                    this.RaisePropertyChanged("TheInstitution");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ChannelAccessCharge", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Data")]
    [System.SerializableAttribute()]
    public partial class ChannelAccessCharge : BankOneMobile.Services.BankOneIncomeAccountService.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ChannelAccessCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private float InstitutionShareField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal MonthlyFeeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitutionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ChannelAccessCode {
            get {
                return this.ChannelAccessCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ChannelAccessCodeField, value) != true)) {
                    this.ChannelAccessCodeField = value;
                    this.RaisePropertyChanged("ChannelAccessCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public float InstitutionShare {
            get {
                return this.InstitutionShareField;
            }
            set {
                if ((this.InstitutionShareField.Equals(value) != true)) {
                    this.InstitutionShareField = value;
                    this.RaisePropertyChanged("InstitutionShare");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal MonthlyFee {
            get {
                return this.MonthlyFeeField;
            }
            set {
                if ((this.MonthlyFeeField.Equals(value) != true)) {
                    this.MonthlyFeeField = value;
                    this.RaisePropertyChanged("MonthlyFee");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.Services.BankOneIncomeAccountService.Institution TheInstitution {
            get {
                return this.TheInstitutionField;
            }
            set {
                if ((object.ReferenceEquals(this.TheInstitutionField, value) != true)) {
                    this.TheInstitutionField = value;
                    this.RaisePropertyChanged("TheInstitution");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MFBStatus", Namespace="http://schemas.datacontract.org/2004/07/ManagedServices.Core.Utility")]
    public enum MFBStatus : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Migration = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        InActive = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Active = 2,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="BankOneIncomeAccountService.IUtilityService")]
    public interface IUtilityService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/ReloadAllCaches", ReplyAction="http://tempuri.org/IUtilityService/ReloadAllCachesResponse")]
        bool ReloadAllCaches();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveAllIncomeAccounts", ReplyAction="http://tempuri.org/IUtilityService/RetrieveAllIncomeAccountsResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount[] RetrieveAllIncomeAccounts();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetIncomeAccountByInstitutionID", ReplyAction="http://tempuri.org/IUtilityService/GetIncomeAccountByInstitutionIDResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount GetIncomeAccountByInstitutionID(long institutionID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetIncomeAccountByMfbCode", ReplyAction="http://tempuri.org/IUtilityService/GetIncomeAccountByMfbCodeResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount GetIncomeAccountByMfbCode(string mfbCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveAllSubscriptionExpenseAccounts", ReplyAction="http://tempuri.org/IUtilityService/RetrieveAllSubscriptionExpenseAccountsResponse" +
            "")]
        BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount[] RetrieveAllSubscriptionExpenseAccounts();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetSubscriptionExpenseAccountByInstitutionID", ReplyAction="http://tempuri.org/IUtilityService/GetSubscriptionExpenseAccountByInstitutionIDRe" +
            "sponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount GetSubscriptionExpenseAccountByInstitutionID(long institutionID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetSubscriptionExpenseAccountByMfbCode", ReplyAction="http://tempuri.org/IUtilityService/GetSubscriptionExpenseAccountByMfbCodeResponse" +
            "")]
        BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount GetSubscriptionExpenseAccountByMfbCode(string mfbCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveAllSmsExpenseAccounts", ReplyAction="http://tempuri.org/IUtilityService/RetrieveAllSmsExpenseAccountsResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount[] RetrieveAllSmsExpenseAccounts();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetSmsExpenseAccountByInstitutionID", ReplyAction="http://tempuri.org/IUtilityService/GetSmsExpenseAccountByInstitutionIDResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount GetSmsExpenseAccountByInstitutionID(long institutionID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/GetSmsExpenseAccountByMfbCode", ReplyAction="http://tempuri.org/IUtilityService/GetSmsExpenseAccountByMfbCodeResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount GetSmsExpenseAccountByMfbCode(string mfbCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveAllChannelAccessCharges", ReplyAction="http://tempuri.org/IUtilityService/RetrieveAllChannelAccessChargesResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge[] RetrieveAllChannelAccessCharges();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveChannelAccessChargeByInstitutionIDAndA" +
            "ccessCode", ReplyAction="http://tempuri.org/IUtilityService/RetrieveChannelAccessChargeByInstitutionIDAndA" +
            "ccessCodeResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge RetrieveChannelAccessChargeByInstitutionIDAndAccessCode(long institutionID, string channelAccessCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUtilityService/RetrieveChannelAccessChargeByMfbCodeAndAccessC" +
            "ode", ReplyAction="http://tempuri.org/IUtilityService/RetrieveChannelAccessChargeByMfbCodeAndAccessC" +
            "odeResponse")]
        BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge RetrieveChannelAccessChargeByMfbCodeAndAccessCode(string mfbCode, string channelAccessCode);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IUtilityServiceChannel : BankOneMobile.Services.BankOneIncomeAccountService.IUtilityService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UtilityServiceClient : System.ServiceModel.ClientBase<BankOneMobile.Services.BankOneIncomeAccountService.IUtilityService>, BankOneMobile.Services.BankOneIncomeAccountService.IUtilityService {
        
        public UtilityServiceClient() {
        }
        
        public UtilityServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public UtilityServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UtilityServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UtilityServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool ReloadAllCaches() {
            return base.Channel.ReloadAllCaches();
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount[] RetrieveAllIncomeAccounts() {
            return base.Channel.RetrieveAllIncomeAccounts();
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount GetIncomeAccountByInstitutionID(long institutionID) {
            return base.Channel.GetIncomeAccountByInstitutionID(institutionID);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.AppZoneIncomeAccount GetIncomeAccountByMfbCode(string mfbCode) {
            return base.Channel.GetIncomeAccountByMfbCode(mfbCode);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount[] RetrieveAllSubscriptionExpenseAccounts() {
            return base.Channel.RetrieveAllSubscriptionExpenseAccounts();
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount GetSubscriptionExpenseAccountByInstitutionID(long institutionID) {
            return base.Channel.GetSubscriptionExpenseAccountByInstitutionID(institutionID);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SubscriptionExpenseAccount GetSubscriptionExpenseAccountByMfbCode(string mfbCode) {
            return base.Channel.GetSubscriptionExpenseAccountByMfbCode(mfbCode);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount[] RetrieveAllSmsExpenseAccounts() {
            return base.Channel.RetrieveAllSmsExpenseAccounts();
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount GetSmsExpenseAccountByInstitutionID(long institutionID) {
            return base.Channel.GetSmsExpenseAccountByInstitutionID(institutionID);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.SmsExpenseAccount GetSmsExpenseAccountByMfbCode(string mfbCode) {
            return base.Channel.GetSmsExpenseAccountByMfbCode(mfbCode);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge[] RetrieveAllChannelAccessCharges() {
            return base.Channel.RetrieveAllChannelAccessCharges();
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge RetrieveChannelAccessChargeByInstitutionIDAndAccessCode(long institutionID, string channelAccessCode) {
            return base.Channel.RetrieveChannelAccessChargeByInstitutionIDAndAccessCode(institutionID, channelAccessCode);
        }
        
        public BankOneMobile.Services.BankOneIncomeAccountService.ChannelAccessCharge RetrieveChannelAccessChargeByMfbCodeAndAccessCode(string mfbCode, string channelAccessCode) {
            return base.Channel.RetrieveChannelAccessChargeByMfbCodeAndAccessCode(mfbCode, channelAccessCode);
        }
    }
}
