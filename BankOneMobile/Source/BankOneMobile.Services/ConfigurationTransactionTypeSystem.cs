﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class ConfigurationTransactionTypeSystem
    {
      public static IConfigurationTransactionType SaveConfigurationTransactionType(IDataSource dataSource, ConfigurationTransactionType theConfigurationTransactionType)
        {
            theConfigurationTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theConfigurationTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public static IConfigurationTransactionType UpdateConfigurationTransactionType(IDataSource dataSource, ConfigurationTransactionType theConfigurationTransactionType)
        {
            var repo = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theConfigurationTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public static IConfigurationTransactionType GetConfigurationTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IConfigurationTransactionType> GetAllConfigurationTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IConfigurationTransactionType> GetActiveConfigurationTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IConfigurationTransactionType> FindConfigurationTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IConfigurationTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }


      


    }
}
