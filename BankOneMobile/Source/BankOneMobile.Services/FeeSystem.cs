﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class FeeTransactionTypeSystem
    {
         private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
         private IFeeTransactionTypeRepository _repository = SafeServiceLocator<IFeeTransactionTypeRepository>.GetService();

       public FeeTransactionTypeSystem()
       {

       }

       public FeeTransactionTypeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }

      
        public IFeeTransactionType SaveFeeTransactionType(IFeeTransactionType theFeeTransactionType)
        {
            theFeeTransactionType.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theFeeTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IFeeTransactionType UpdateFeeTransactionType(IFeeTransactionType theFeeTransactionType)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theFeeTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IFeeTransactionType GetFeeTransactionType(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        

        public List<IFeeTransactionType> GetAllFeeTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IFeeTransactionType> FindFeeTransactionTypesBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public IList<IFeeTransactionType> GetAllActiveFeeTransactionTypes()
        {
            var result = _repository.GetActiveFeeTransactionTypes(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IFeeTransactionType GetByName(string name)
        {
            var result = _repository.GetByName(_theDataSource,name);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public FeeTransactionType GetDefault()
        {
            return new FeeTransactionType { Amount = 100 }; 
        }
    }
}
