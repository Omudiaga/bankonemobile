﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.Services
{
    public class FlowSystem
    {
         private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
         private IFlowRepository _repository = SafeServiceLocator<IFlowRepository>.GetService();

       public FlowSystem()
       {

       }

       public FlowSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }

       public FlowSystem(Guid workflowId)
       {
           WorkflowInfo wInfo = WorkflowCenter.Instance.GetWorkflowBy(workflowId);
           if (wInfo != null)
           {
               _theDataSource = wInfo.DataSource;
           }
       }
        public IFlow SaveFlow(IFlow theFlow)
        {
            theFlow.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theFlow);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IFlow UpdateFlow(IFlow theFlow)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theFlow);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IFlow GetFlow(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public IFlow GetFlowBy(Guid uniqueId)
        {
            var result = _repository.GetByUniqueId(_theDataSource, uniqueId);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }

        public List<IFlow> GetAllFlows()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IFlow> FindFlowsBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public IFlow FindFlowBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindOne(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public IFlow GetStartUpFlow(IMobileAccount mob)
        {
            var uniqueID = Guid.NewGuid();
           //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared, uniqueID);
            IFlow toReturn = null;
            IAgent ag = null;
            IInstitution inst = null;
            try
            {
                ag = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            }
            catch (TransactionNotPermittedToNonAgentsException)
            { 
            }
            
                if (ag != null)
                {
                    inst = new InstitutionSystem(_theDataSource).GetByCode(_theDataSource, ag.TheAgentAccount.InstitutionCode);
                    toReturn = inst.TheAgentStartFlow;
                }
                else
                {
                    inst = new InstitutionSystem(_theDataSource).GetByCode(_theDataSource, mob.RecievingBankAccount.InstitutionCode);
                    toReturn = inst.TheCustomerStartFlow;
                }
                //_repository.DbContext.Close(source);

           
            return toReturn;
        }
    }
}
