﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
   public class HostSystem
    {
        public static IHost SaveHost(Host theHost)
        {
            theHost.IsActive = false;
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var repo = SafeServiceLocator<IHostRepository>.GetService();
                repo.DbContext.BeginTransaction(source);
                var result = repo.Save(source, theHost);
                repo.DbContext.CommitTransaction(source);
                return result;
            }
        }

        public static IHost UpdateHost(Host theHost)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var repo = SafeServiceLocator<IHostRepository>.GetService();
                repo.DbContext.BeginTransaction(source);
                var result = repo.Update(source, theHost);
                repo.DbContext.CommitTransaction(source);
                return result;
            }
        }

        public static IHost GetHost(long id)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var result = SafeServiceLocator<IHostRepository>.GetService().Get(source, id);
                return result;
            }
        }

        public static List<IHost> GetAllHosts()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var result = SafeServiceLocator<IHostRepository>.GetService().GetAll(source);
                return result;
            }
        }

        public static List<IHost> GetActiveHosts()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var result = SafeServiceLocator<IHostRepository>.GetService().GetAll(source);
                result = result.Where(x => x.IsActive == true).ToList();
                return result;
            }
        }

        public static List<IHost> FindHosts(IDictionary<string, object> propertyValuePairs)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var result = SafeServiceLocator<IHostRepository>.GetService().Search(source, propertyValuePairs);
                return result;
            }
        }
    }
}
