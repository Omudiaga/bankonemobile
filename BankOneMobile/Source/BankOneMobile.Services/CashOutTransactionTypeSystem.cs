﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class CashOutTransactionTypeSystem
  {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
      private ICashOutTransactionTypeRepository _repository = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService();
      public List<CashOutTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string agentCode, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt, out int totalSuccess)
      {
          // BankOneMobile.Services.HSMCenter.ResetAllPins();
          //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
          IDictionary<string, object> dic = new Dictionary<string, object>();
          List<CashOutTransactionType> tr = new List<CashOutTransactionType>();

          TransactionStatus? transStatus = null;
          if (!string.IsNullOrEmpty(status))
          {
              transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
          }
          if (!string.IsNullOrEmpty(agentCode) && !string.IsNullOrEmpty(agentCode.Trim()))
          {

              dic.Add("AgentCode", agentCode);

          }


          //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
          tr = _repository.FindTransactionsWithPaging(dataSource, institutionCode, transStatus, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt, out totalSuccess);

          // //_repository.DbContext.Close(_theDataSource);
          return tr;

      }
      public static ICashOutTransactionType SaveCashOutTransactionType(IDataSource dataSource, CashOutTransactionType theCashOutTransactionType)
        {
            theCashOutTransactionType.IsActive = false;
            var repo = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theCashOutTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static ICashOutTransactionType UpdateCashOutTransactionType(IDataSource dataSource, CashOutTransactionType theCashOutTransactionType)
        {
            var repo = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theCashOutTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static ICashOutTransactionType GetCashOutTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<ICashOutTransactionType> GetAllCashOutTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<ICashOutTransactionType> GetActiveCashOutTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<ICashOutTransactionType> FindCashOutTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<ICashOutTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }
    }
}
