﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class PinChangeTransactionTypeSystem
    {
      public static IPinChangeTransactionType SavePinChangeTransactionType(IDataSource dataSource, PinChangeTransactionType thePinChangeTransactionType)
        {
            thePinChangeTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, thePinChangeTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;
        }

      public static IPinChangeTransactionType UpdatePinChangeTransactionType(IDataSource dataSource, PinChangeTransactionType thePinChangeTransactionType)
        {
            var repo = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, thePinChangeTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public static IPinChangeTransactionType GetPinChangeTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IPinChangeTransactionType> GetAllPinChangeTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IPinChangeTransactionType> GetActivePinChangeTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IPinChangeTransactionType> FindPinChangeTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IPinChangeTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }


      


    }
}
