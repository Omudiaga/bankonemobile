﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace BankOneMobile.FlowDesigner
{
    public class BankOneMobileCommands
    {
        static BankOneMobileCommands()
        {
            InsertMenuScreen = new RoutedUICommand("_Menu", "Menu", typeof(BankOneMobileCommands),
                new InputGestureCollection 
            {
                new KeyGesture(Key.E, ModifierKeys.Control, "Ctrl+E") 
            });

            InsertConfirmScreen = new RoutedUICommand("_Confirm", "Confirm", typeof(BankOneMobileCommands),
                new InputGestureCollection 
            {
                new KeyGesture(Key.O, ModifierKeys.Control, "Ctrl+O") 
            });

            InsertNotifyScreen = new RoutedUICommand("_Notify", "Notify", typeof(BankOneMobileCommands),
                new InputGestureCollection 
            {
                new KeyGesture(Key.N, ModifierKeys.Control, "Ctrl+N") 
            });

            InsertCustomScreen = new RoutedUICommand("C_ustom", "Custom", typeof(BankOneMobileCommands),
                new InputGestureCollection 
            {
                new KeyGesture(Key.C, ModifierKeys.Control, "Ctrl+CS") 
            });

            InsertInitiateSessionAction = new RoutedUICommand("Initiate _Session", "InitiateSession", typeof(BankOneMobileCommands),
               new InputGestureCollection 
            {
                new KeyGesture(Key.I & Key.S, ModifierKeys.Control, "Ctrl+IS") 
            });

            InsertGotoAction = new RoutedUICommand("_Goto Flow", "GotoFlow", typeof(BankOneMobileCommands),
                new InputGestureCollection 
            {
                new KeyGesture(Key.G, ModifierKeys.Control, "Ctrl+G") 
            });

            ManageScreen = new RoutedUICommand("_Manage Screen", "ManageScreen", typeof(BankOneMobileCommands),
               new InputGestureCollection 
            {
                new KeyGesture(Key.M, ModifierKeys.Control, "Ctrl+M") 
            });

            ToggleToolbox = new RoutedUICommand("_Toggle Toolbox", "ToggleToolbox", typeof(BankOneMobileCommands),
              new InputGestureCollection 
            {
                new KeyGesture(Key.T, ModifierKeys.Control, "Ctrl+T") 
            });

            ToggleActionProperties = new RoutedUICommand("Toggle _Action Properties", "ToggleActionProperties", typeof(BankOneMobileCommands),
              new InputGestureCollection 
            {
                new KeyGesture(Key.A & Key.P, ModifierKeys.Control, "Ctrl+AP") 
            });

            InsertPinEntryScreen = new RoutedUICommand("Insert Pin Entry Screen", "InsertPinEntryScreen", typeof(BankOneMobileCommands),
             new InputGestureCollection 
            {
                new KeyGesture(Key.P & Key.E, ModifierKeys.Control, "Ctrl+PE") 
            });

            SetMainFlow = new RoutedUICommand("Set Main Flow", "SetMainFlow", typeof(BankOneMobileCommands),
             new InputGestureCollection 
            {
                new KeyGesture(Key.S & Key.M, ModifierKeys.Control, "Ctrl+SM") 
            });
        }

        public static RoutedUICommand InsertMenuScreen { get; private set; }

        public static RoutedUICommand InsertNotifyScreen { get; private set; }

        public static RoutedUICommand InsertConfirmScreen { get; private set; }

        public static RoutedUICommand InsertPinEntryScreen { get; private set; }

        public static RoutedUICommand InsertCustomScreen { get; private set; }

        public static RoutedUICommand InsertInitiateSessionAction { get; private set; }

        public static RoutedUICommand InsertGotoAction { get; private set; }

        public static RoutedUICommand ManageScreen { get; private set; }

        public static RoutedUICommand ToggleToolbox { get; private set; }

        public static RoutedUICommand ToggleActionProperties { get; private set; }

        public static RoutedUICommand ToggleVariables { get; private set; }

        public static RoutedUICommand SetMainFlow { get; private set; }

    }

}
