﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.FlowDesigner
{
    /// <summary>
    /// Interaction logic for OpenWindow.xaml
    /// </summary>
    public partial class OpenWindow : Window
    {
        public Flow SelectedFlow {get;set;}
        public OpenWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lvFlows.ItemsSource = BankOneMobile.SessionNodeDesigners.Actions.InitiateSessionDesigner.TheFlows;
            /*using (var client = new BankOneMobileServices.FlowServiceClient())
            {
                lvFlows.ItemsSource = client.GetAllFlows();
            }*/
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            if (lvFlows.SelectedItem != null)
            {
                SelectedFlow = lvFlows.SelectedItem as Flow;
                DialogResult = true;
                this.Close();
            }

        }
    }
}
