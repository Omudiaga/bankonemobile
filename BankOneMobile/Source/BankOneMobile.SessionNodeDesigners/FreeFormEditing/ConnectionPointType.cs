//------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------

namespace Microsoft.Activities.Statements.Design.FreeFormEditing
{
    enum ConnectionPointType
    {        
        Default,
        Incoming,
        Outgoing,
    }
}
