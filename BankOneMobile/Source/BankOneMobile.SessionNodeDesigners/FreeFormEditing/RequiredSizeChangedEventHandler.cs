﻿//------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------

namespace Microsoft.Activities.Statements.Design.FreeFormEditing
{
    internal delegate void RequiredSizeChangedEventHandler(object sender, RequiredSizeChangedEventArgs e);
}