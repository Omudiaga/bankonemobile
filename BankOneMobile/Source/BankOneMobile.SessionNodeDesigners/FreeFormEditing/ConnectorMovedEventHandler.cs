//------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------

namespace Microsoft.Activities.Statements.Design.FreeFormEditing
{
    internal delegate void ConnectorMovedEventHandler(object sender, ConnectorMovedEventArgs e);
}
