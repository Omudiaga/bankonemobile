﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using BankOneMobile.Services;

namespace BankOneMobile.SessionNodeDesigners
{
    [Designer(typeof(ActionDesigner))]
    public class ActionNode : State
    {
        private CompletionCallback _onCompleted;
        public OutArgument<string> FailureCode {
            get
            {
                if (this.FailureActivity == null) return null;
                return this.FailureActivity.FaultCode;
            }
            set
            {
                if (this.FailureActivity != null)
                {
                    this.FailureActivity.FaultCode = value;
                }                
            }
        }
        public OutArgument<string> FailureDescription {
            get
            {
                if (this.FailureActivity == null) return null;
                return this.FailureActivity.FaultDescription;
            }
            set
            {
                if (this.FailureActivity != null)
                {
                    this.FailureActivity.FaultDescription = value;
                }
            }
        }

        public override State Next
        {
            get;
            set;
        }

        public virtual State SuccessNext
        {
            get
            {
                if (this.Transitions != null)
                {
                    var successTrans = this.Transitions.SingleOrDefault(x => x.DisplayName == "");
                    if (successTrans != null) return successTrans.To;
                }
                return null;
            }
        }

        public virtual State FailureNext
        {
            get
            {
                if (this.Transitions != null)
                {
                    var failureTrans = this.Transitions.SingleOrDefault(x => x.DisplayName == "failure");
                    if (failureTrans != null) return failureTrans.To;
                }
                return null;
            }
        }


        private FaultResponseActivity FailureActivity = new FaultResponseActivity();

        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            if (this.InternalState == null)
            {
                nextNode = this.Next = SuccessNext;
                return true;
            }

            this.Next = SuccessNext; 
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedFaultCallback = OnActionNodeFaulted;
            currentWorkflowInfo.AttachedCompletedCallback = onCompleted;
            context.ScheduleActivity(this.InternalState, onCompleted, TheFlow.OnActionNodeFaulted);
            nextNode = null;
            return false;
        }

        private void OnActionNodeFaulted(NativeActivityFaultContext context, Exception fault, System.Activities.ActivityInstance completedInstance)
        {
            this.Next = FailureNext;
            if (this.Next == null) throw fault;
            context.HandleFault();
            this.FailureActivity.TheFault = fault;

            context.ScheduleActivity(this.FailureActivity);
        }

        public override string GetDefaultDisplayName()
        {
            return "New Action";
        }

        public override void GetChildActivities(NativeActivityMetadata metadata)
        {
            metadata.AddChild(this.InternalState);
            metadata.AddChild(this.FailureActivity);
        }

        public class FaultResponseActivity : CodeActivity
        {
            public OutArgument<string> FaultCode { get; set; }
            public OutArgument<string> FaultDescription { get; set; }
            public Exception TheFault { get; set; }
            protected override void Execute(CodeActivityContext context)
            {
                if (TheFault != null)
                {
                    FailureException fault =TheFault as FailureException ;
                    if (fault == null)
                    {
                        fault = new FailureException("0", TheFault.Message);
                    }
                    
                    context.SetValue(FaultCode, fault.FailureCode);
                    context.SetValue(FaultDescription, fault.Message);
                }
            }
        }
    }

    public class FailureException : ApplicationException
    {
        
        public string FailureCode { get; private set; }

        public FailureException(string code, string reason)
            : base(reason)
        {
            FailureCode = code;
        }
    }
}
