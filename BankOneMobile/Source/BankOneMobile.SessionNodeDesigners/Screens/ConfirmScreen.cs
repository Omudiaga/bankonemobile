﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    
    public class ConfirmScreen : ScreenNode
    {
        private CompletionCallback _onCompleted;
        public InArgument<string> DisplayText
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as CustomScreenActivity).DisplayText;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as CustomScreenActivity).DisplayText = value;
                }
            }
        }
         public string ConfirmationText { set; get; }
         string _yes, _no;
         public string YesOption
         {
             set
             {
                 _yes = value;
             }
             get
             {
                 if(string.IsNullOrEmpty(_yes))
                 {
                     return "RETRY";
                }
                 return _yes;
             }
         }

         public string NoOption
         {
             set
             {
                 _no = value;
             }
             get
             {
                 if (string.IsNullOrEmpty(_no))
                 {
                     return "CANCEL";
                 }
                 return _no;
             }
         }

         public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            string options = string.Format("Enter 1 to {0},\n0 to {1}\n", YesOption, NoOption);
            if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(ConfirmationText + options);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                } 
            }
            else
            {
                result.AppendLine(ConfirmationText + options);
            }   
           // result.AppendLine("O: Cancel");
            return result.ToString();
        }

         public override string GetXmlDisplay(NativeActivityContext context)
         {
             XElement xmlResponseTree = new XElement("Response");
             XElement displayElement = new XElement("Display");
             if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
             {
                 if (context != null)
                 {
                     displayElement.SetValue(ConfirmationText);
                 }
                 else
                 {
                     displayElement.SetValue("<Dynamic Text>");
                 }
             }
             else
             {
                 //Confirmation Text as Before Menu
                 if (!String.IsNullOrWhiteSpace(ConfirmationText))
                     displayElement.Add(new XElement("BeforeMenu", ConfirmationText));
                 //Create Menu Element
                 XElement menuElement = new XElement("Menu");
                 //Populate Menu Items
                 menuElement.Add(new XElement("MenuItem", new XAttribute("Index", 1), YesOption));
                 menuElement.Add(new XElement("MenuItem", new XAttribute("Index", 0), NoOption));
                 //Add Menu element to Display
                 displayElement.Add(menuElement);
             }
             xmlResponseTree.Add(displayElement);
             xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
             
             //xmlResponseTree.Add(new XElement("ShouldClose", new XAttribute("Close", false), 0));
             return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
         }

         public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
         {
             XElement xmlResponseTree = new XElement("Response");
             XElement displayElement = new XElement("Display");
             if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
             {
                 if (context != null)
                 {
                     displayElement.SetValue(textToAdd + ConfirmationText);
                 }
                 else
                 {
                     displayElement.SetValue(textToAdd + "<Dynamic Text>");
                 }
             }
             else
             {
                 //Confirmation Text as Before Menu
                 if (!String.IsNullOrWhiteSpace(ConfirmationText))
                     displayElement.Add(new XElement("BeforeMenu", textToAdd + ConfirmationText));
                 //Create Menu Element
                 XElement menuElement = new XElement("Menu");
                 //Populate Menu Items
                 menuElement.Add(new XElement("MenuItem", new XAttribute("Index", 1), YesOption));
                 menuElement.Add(new XElement("MenuItem", new XAttribute("Index", 0), NoOption));
                 //Add Menu element to Display
                 displayElement.Add(menuElement);
             }
             xmlResponseTree.Add(displayElement);
             xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
             
             //xmlResponseTree.Add(new XElement("ShouldClose", new XAttribute("Close", false), 0));
             return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
         }

        public ConfirmScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new CustomScreenActivity();
        }

        static ConfirmScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(ConfirmScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            if (String.IsNullOrEmpty(this.ConfirmationText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                _onCompleted = onCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            return base.Execute(context, onCompleted, out nextNode);
        }

        internal void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.ConfirmationText = (instance.Activity as CustomScreenActivity).ComputedDisplay;
            State nextNode;
            base.Execute(context, _onCompleted, out nextNode);
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            string option = Convert.ToString(response);
            if (option == null || !(option.Trim().Equals("1", StringComparison.CurrentCultureIgnoreCase)|| option.Trim().Equals("0", StringComparison.CurrentCultureIgnoreCase)))
            {
                currentWorkflowInfo.DisplayMessage = "Unknown option. Enter either 0 or 1\r\n\r\n" + this.GetDisplay(context);
                //currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown option. Enter either 0 or 1\n");
              //  currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
              //  currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                string lookupString = "no";
                if (option.Trim().Equals("1", StringComparison.CurrentCultureIgnoreCase))
                {
                    lookupString = "yes";
                }
                Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == lookupString);
                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }

       
    }

  
}
