﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    
    public class TrueFalseRoutingScreen : ConfirmScreen
    {
        private CompletionCallback _onCompleted;
        public InArgument<string> DisplayText
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as RoutingScreenActivity).DisplayText;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as RoutingScreenActivity).DisplayText = value;
                }
            }
        }
        public InArgument<bool> Condition
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as RoutingScreenActivity).Condition;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as RoutingScreenActivity).Condition = value;
                }
            }
        }
         public string ConfirmationText { set; get; }

         public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(ConfirmationText);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                }
            }
            else
            {
                result.AppendLine(ConfirmationText);
            }   
           // result.AppendLine("O: Cancel");
            return result.ToString();
        }

         public override string GetXmlDisplay(NativeActivityContext context)
         {
             XElement xmlResponseTree = new XElement("Response");
             XElement node = new XElement("Display");
             if (String.IsNullOrEmpty(this.ConfirmationText) && DisplayText != null)
             {
                 if (context != null)
                 {
                     node.SetValue(ConfirmationText);
                 }
                 else
                 {
                     node.SetValue("<Dynamic Text>");
                 }
             }
             else
             {
                 node.SetValue(ConfirmationText);
             }
             xmlResponseTree.Add(node);
             xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
             
             return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
         }

        public TrueFalseRoutingScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new RoutingScreenActivity();
        }

        static TrueFalseRoutingScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(ConfirmScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        

        internal void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.ConfirmationText = (instance.Activity as RoutingScreenActivity).ComputedDisplay;
            State nextNode;
            base.Execute(context, _onCompleted, out nextNode);
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            InArgument<Boolean> selectedVal = (this.InternalActivity as RoutingScreenActivity).Condition;
            if (selectedVal.Get(context))
            {
                response = "yes";
            }
            else
            {
                response = "no";
            }
            string option = Convert.ToString(response);
            
                string lookupString = "no";
                if (option.Trim().Equals("1", StringComparison.CurrentCultureIgnoreCase))
                {
                    lookupString = "yes";
                }
                Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == lookupString);
                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            
        }

       
    }


    public class RoutingScreenActivity : CustomScreenActivity
    {
        public InArgument<bool> Condition
        {
            set;
            get;
        }

        
    }

  
}
