﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Activities.Presentation.Model;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.ComponentModel;

namespace BankOneMobile.SessionNodeDesigners.Screens
{
    /// <summary>
    /// Interaction logic for MenuManager.xaml
    /// </summary>
    public partial class MenuManager : Window
    {
        protected ModelItem _theMenu;
        protected PropertyChangedEventHandler _displayChanged;
        public MenuManager()
        {
            InitializeComponent();
        }

        public MenuManager(ModelItem menu, PropertyChangedEventHandler displayChanged)
            : this()
        {
            _theMenu = menu;
            _displayChanged = displayChanged;
            this.DataContext = _theMenu;
            BuildMenuItemList();
          
        }

        private void BuildMenuItemList()
        {
            spMenuItem.Children.Clear();
            foreach (var item in _theMenu.Properties["MenuItems"].Collection)
            {
                MenuItemControl ctrl = new MenuItemControl(item, _displayChanged);
                spMenuItem.Children.Add(ctrl);
            }
            lblIndex.Text = string.Format("{0}.", _theMenu.Properties["MenuItems"].Collection.Count + 1);
            //_theMenu.Properties["CurrentMenuNumber"].SetValue(_theMenu.Properties["MenuItems"].Collection.Count + 1);
            
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            //this._theMenu.MenuItems.Add(new MenuItem() { Index = this._theMenu.MenuItems.Count + 1, Text = "Testing This update" });
            this.DialogResult = true;
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtText.Text))
            {
                this._theMenu.Properties["MenuItems"].Collection.Add(new MenuItem() { Index = this._theMenu.Properties["MenuItems"].Collection.Count + 1, Text = txtText.Text });
                txtText.Clear();
            }
            else
            {
                MessageBox.Show("Please enter your Menu Text before clicking on Add.", "Add Menu Item Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            _theMenu.Properties["MenuItems"].Collection.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Collection_CollectionChanged);
            _theMenu.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(_displayChanged);            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _theMenu.Properties["MenuItems"].Collection.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Collection_CollectionChanged);
            _theMenu.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(_displayChanged);            
        }

        void Value_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            _theMenu.Properties["Display"].SetValue((_theMenu.GetCurrentValue() as MenuScreen).GetDisplay(null));
        }

        void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.)
            {
                ReOrderMenuItems();
            }
            BuildMenuItemList();
            _displayChanged(sender, new PropertyChangedEventArgs("MenuItems"));
            //_theMenu.Properties["Display"].SetValue((_theMenu.GetCurrentValue() as MenuScreen).GetDisplay());
        }

        private void ReOrderMenuItems()
        {
            int newIndex = 1;
            foreach (var item in _theMenu.Properties["MenuItems"].Collection)
            {
                item.Properties["Index"].SetValue(newIndex);
                newIndex++;
            }
        }

        private void chkTopMessage_Checked(object sender, RoutedEventArgs e)
        {
            txtBefore.Visibility = System.Windows.Visibility.Visible;
        }

        private void chkBottomMessage_Checked(object sender, RoutedEventArgs e)
        {
            txtAfter.Visibility = System.Windows.Visibility.Visible;
        }

        private void chkBottomMessage_Unchecked(object sender, RoutedEventArgs e)
        {
            txtAfter.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void chkTopMessage_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBefore.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
