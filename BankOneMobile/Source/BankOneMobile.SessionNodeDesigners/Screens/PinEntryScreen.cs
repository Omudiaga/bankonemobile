﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.Core.Exceptions;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    
    public class PinEntryScreen : ScreenNode
    {
        private const string DEFAULT_PIN_ENTRY = "Enter your PIN";
         public string RequestText { set; get; }
         public int PINLength { set; get; }
         public bool IsNumeric { set; get; }
         public bool NoVerification { set; get; }
         private string EncryptedPin { get; set; }

         public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrWhiteSpace(RequestText))
            {
                result.AppendLine(DEFAULT_PIN_ENTRY);
            }
            else
            {
                result.AppendLine(RequestText);
            }
            return result.ToString();
        }

         public override string GetXmlDisplay(NativeActivityContext context)
         {
             XElement xmlResponseTree = new XElement("Response");
             XElement node = new XElement("Display");
             if (String.IsNullOrWhiteSpace(RequestText))
             {
                 node.SetValue(DEFAULT_PIN_ENTRY);
             }
             else
             {
                 node.SetValue(RequestText);
             }
             xmlResponseTree.Add(node);
             xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", true), 1));
             xmlResponseTree.Add(new XElement("IsNumeric", new XAttribute("IsNumeric", IsNumeric ? true : false), IsNumeric ? 1 : 0));
             return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
         }

         public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
         {
             XElement xmlResponseTree = new XElement("Response");
             XElement node = new XElement("Display");
             if (String.IsNullOrWhiteSpace(RequestText))
             {
                 node.SetValue(textToAdd + DEFAULT_PIN_ENTRY);
             }
             else
             {
                 node.SetValue(RequestText);
             }
             xmlResponseTree.Add(node);
             xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", true), 1));
             xmlResponseTree.Add(new XElement("IsNumeric", new XAttribute("IsNumeric", IsNumeric ? true : false), IsNumeric ? 1 : 0));
             return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
         }

        public PinEntryScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new CustomScreenActivity();
            IsNumeric = true;
            PINLength = 4;
        }

        static PinEntryScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(PinEntryScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }


        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            try
            {
                int enteredPin;
                bool validNumber = response == null ? false : Int32.TryParse(response, out enteredPin);
                if (!validNumber && IsNumeric)
                {
                    currentWorkflowInfo.DisplayMessage = "Invalid PIN. Enter numbers only.\r\n\r\n" + this.GetDisplay(context);
                    //currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid PIN. Enter numbers only.<br/>");

                    context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                }
                else if (response != null && response.Trim().Length != PINLength)
                {
                    currentWorkflowInfo.DisplayMessage = string.Format("Invalid PIN. PIN should be {0} {1}.\r\n\r\n{2}", PINLength, IsNumeric ? "Digits" : "Characters", this.GetDisplay(context));
                    currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, string.Format("Invalid PIN. PIN should be {0} {1}.<br/>", PINLength, IsNumeric ? "Digits" : "Characters"));

                    context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                }
                else
                {
                    
                    
                        Transition theTrans = this.Transitions.FirstOrDefault();
                        if (theTrans != null)
                        {
                            this.Next = theTrans.To;
                        }
                        if (_OnCompleted != null)
                        {
                            _OnCompleted.Invoke(context, null);
                        }
                    
                }
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException("23", "No Response From HSM");
            }
            catch (CustomHSMException ex)
            {
                throw new FailureException("24", ex.Message);
            }
            

        }

        
    }
    
  
}
