﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using BankOneMobile.Services;
using System.Activities.Presentation.Metadata;
using System.ComponentModel;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Configuration;
using System.ServiceModel;

namespace BankOneMobile.SessionNodeDesigners.Actions
{
    public class InitiateSessionAction : ActionNode
    {
        public InArgument<string> WaitingText
        {
            get
            {
                if (this.InternalState == null) return null;
                return (this.InternalState as InitiateSessionActivity).WaitingText;
            }
            set
            {
                if (this.InternalState != null)
                {
                    (this.InternalState as InitiateSessionActivity).WaitingText = value;
                }
            }
        }

        public InArgument<string> PIN
        {
            get
            {
                if (this.InternalState == null) return null;
                return (this.InternalState as InitiateSessionActivity).PIN;
            }
            set
            {
                if (this.InternalState != null)
                {
                    (this.InternalState as InitiateSessionActivity).PIN = value;
                }
            }
        }

        public InArgument<string> PhoneNumber
        {
            get
            {
                if (this.InternalState == null) return null;
                return (this.InternalState as InitiateSessionActivity).PhoneNumber;
            }
            set
            {
                if (this.InternalState != null)
                {
                    (this.InternalState as InitiateSessionActivity).PhoneNumber = value;
                }
            }
        }

        //[Browsable(false)]
        public Guid FlowID
        {
            get
            {
                if (this.InternalState == null) return Guid.Empty;
                return (this.InternalState as InitiateSessionActivity).FlowID;
            }
            set
            {
                if (this.InternalState != null)
                {
                    (this.InternalState as InitiateSessionActivity).FlowID = value;
                }
            }
        }

        public IDictionary<string, InArgument<String>> Properties { get; set; }

        public InitiateSessionAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new InitiateSessionActivity() { SessionAction = this };
            this.Properties = new Dictionary<string, InArgument<String>>();
        }


        static InitiateSessionAction()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(InitiateSessionAction);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(InitiateSessionDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        public override void GetChildActivities(NativeActivityMetadata metadata)
        {
            if (this.InternalState != null) (this.InternalState as InitiateSessionActivity).SessionAction = this;
            base.GetChildActivities(metadata);
        }
//
    }

    public class InitiateSessionActivity : NativeActivity
    {
        private const string WAITING_TEXT ="Please wait for  the  Recipient to  complete his/her session.Press 1 to continue";
        public InitiateSessionAction SessionAction { get; set; }
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<string> WaitingText { get; set; }
        
        public InArgument<string> PIN { get; set; }
        public Guid FlowID { get; set; }

        protected override bool CanInduceIdle
        {
            get
            {
                return true ;
            }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            if (SessionAction != null)
            {
                foreach (KeyValuePair<string, InArgument<string>> keyVal in SessionAction.Properties)
                {
                    RuntimeArgument ra = new RuntimeArgument(keyVal.Key, typeof(string), ArgumentDirection.In);
                    metadata.Bind(keyVal.Value, ra);
                    metadata.AddArgument(ra);                    
                }
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            string phoneNo = PhoneNumber.Get(context);
            string dynamicValue = string.Empty;
            //string code = SessionAction.Properties["code"].Get(context);
            try
            {
              dynamicValue   = WaitingText.Get(context);
            }
            catch
            {
            }
            string waitingText = string.IsNullOrEmpty(dynamicValue) ? WAITING_TEXT : dynamicValue;
            if (!String.IsNullOrEmpty(phoneNo))
            {
                WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
                currentWorkflowInfo.InitiatingFlowID = FlowID;

                currentWorkflowInfo.ContextInputs = new Dictionary<string, object>();
                foreach (var keyVal in SessionAction.Properties)
                {
                    currentWorkflowInfo.ContextInputs.Add(keyVal.Key, keyVal.Value.Get(context));
                }

                string phoneNoToUse = phoneNo;
                if (!phoneNoToUse.StartsWith("+"))
                {
                    phoneNoToUse = "+234" + phoneNoToUse.TrimStart('0');
                }

                WorkflowCenter.Instance.InitiatingWorkflows.AddOrUpdate(phoneNoToUse, currentWorkflowInfo.TheWorkflow.Id, (x, y) => currentWorkflowInfo.TheWorkflow.Id);
                try
                {
                    new BankOneMobile.InfoBip.USSD.UssdManager().StartSession(phoneNoToUse, currentWorkflowInfo.SessionID);
                }
                catch
                {
                   // throw new FailureException("30", "USSD Initiation Failed");
                }

                currentWorkflowInfo.DisplayMessage = waitingText;
                currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Action_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(Resume_Action_Bookmark));

            }
        }

        protected void Resume_Action_Bookmark(NativeActivityContext context, Bookmark bookmark, object value)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            
            string response = Convert.ToString(value);
            currentWorkflowInfo.DisplayMessage = null;
            //TODO: Check if initiated session is through
            
            if (response == "1")
            {
                var items = WorkflowCenter.Instance.InitiatingWorkflows.ToList().Where(x => x.Key != currentWorkflowInfo.InitiatedBy.MobilePhone);

                if (!Convert.ToBoolean(ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    if (items.Count()> 1)
                    {
                        //Execute(context);
                    }
                }

                //if (currentWorkflowInfo.InitiatingFlowID.HasValue)
                //{
                //    //Execute(context);
                //}
                if(!currentWorkflowInfo.ChildTransactionCompleted)
                {

                    Execute(context);
                    this.WaitingText = string.Format("Customer's Transaction Failed .{0}",String.IsNullOrEmpty(currentWorkflowInfo.FailureReason)?string.Empty:string.Format("-Reason:{0}",(currentWorkflowInfo.FailureReason)));
                }
            }
            else
            {
                this.WaitingText = String.Format("Invalid Response {0}", WaitingText);
                Execute(context);
                
            }
        }

    }
}
