﻿//-------------------------------------------------------------------
// Copyright (c) Microsoft Corporation. All rights reserved
//-------------------------------------------------------------------



using System.Activities.Presentation.Metadata;
using BankOneMobile.SessionNodeDesigners;
using System;
using System.ComponentModel;
using System.Activities.Presentation.PropertyEditing;
using System.Activities.Presentation;
using System.Collections.Generic;
using BankOneMobile.SessionNodeDesigners.Screens;
using System.Collections;
using System.Windows.Data;
using System.Windows.Controls;
using System.Linq;
using BankOneMobile.Core.Contracts;
using System.Activities;
using System.IO;
using System.Activities.XamlIntegration;
using System.Text;
using System.Activities.Presentation.View;

namespace BankOneMobile.SessionNodeDesigners.Actions
{
    
    // Interaction logic for SimpleNativeDesigner.xaml
    public partial class InitiateSessionDesigner  : IStateDesigner
    {
        public static List<object> TheFlows { get; set; }

        public InitiateSessionDesigner()
        {
            InitializeComponent();
        }

        private void cmbFlow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            cmb.SelectedItem = TheFlows.Cast<IFlow>().SingleOrDefault(x => x.UniqueId == (Guid)ModelItem.Properties["FlowID"].ComputedValue);
        }


        private void LoadArguments(IFlow theFlow, StackPanel sp)
        {
            if (!String.IsNullOrEmpty(theFlow.XamlDefinition))
            {
                Activity act = null;
                //get the definition of the workflow from the function property
                using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(theFlow.XamlDefinition)))
                {
                    act = ActivityXamlServices.Load(new System.Xaml.XamlXmlReader(ms, new System.Xaml.XamlXmlReaderSettings() { }));
                }

                //this.ModelItem.Properties["Properties"].Dictionary.Clear();
                if (sp.Children.Count > 0)
                {
                    this.ModelItem.Properties["Properties"].Dictionary.Clear();
                    sp.Children.Clear();
                }
                foreach (var prop in (act as DynamicActivity).Properties)
                {
                    sp.Children.Add(new TextBlock()
                    {
                        Text = string.Format("{0}:", prop.Name)
                    });
                    var exprTb = new ExpressionTextBox()
                    {
                        ExpressionType = typeof(String),
                        HintText = String.Format("Enter {0}", prop.Name),
                        OwnerActivity = this.ModelItem,
                        MaxLines = 1
                    };

                    System.Activities.Presentation.Model.ModelItem itemModelItem = null;
                    if (this.ModelItem.Properties["Properties"].Dictionary.ContainsKey(prop.Name))
                    {
                        itemModelItem = this.ModelItem.Properties["Properties"].Dictionary[prop.Name];
                        // new InArgument<String>().Expression

                    }
                    else
                    {
                        itemModelItem = this.ModelItem.Properties["Properties"].Dictionary.Add(prop.Name, new InArgument<String>());
                    }
                    var bd = new Binding("Expression");
                    bd.Mode = BindingMode.TwoWay;
                    //bd.Converter = new ArgumentToExpressionConverter();
                    // bd.ConverterParameter = ArgumentDirection.In;
                    bd.Source = itemModelItem;
                    exprTb.SetBinding(ExpressionTextBox.ExpressionProperty, bd);
                    sp.Children.Add(exprTb);

                }
            }
        }

        private void cmbFlow_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = sender as ComboBox;

            IFlow selectedFlow = cmb.SelectedItem as IFlow;
            if (selectedFlow != null)
            {
                StackPanel sp = (cmb.Parent as StackPanel).FindName("spProperties") as StackPanel;

                LoadArguments(selectedFlow, sp);
            }

        }
    }
}
