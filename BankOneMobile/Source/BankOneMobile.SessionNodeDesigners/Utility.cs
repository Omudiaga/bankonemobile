﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Microsoft.Activities.Statements.Design.FreeFormEditing
{
    public class Utility
    {
        internal static T GetVisualAncestor<T>(DependencyObject child) where T:class
        {
            if (child == null)
            {
                return null;
            }
            do
            {
                child = VisualTreeHelper.GetParent(child);
            }
            while (child != null && !typeof(T).IsAssignableFrom(child.GetType()));

            return child as T;
        }
    }
}
