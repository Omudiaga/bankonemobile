﻿namespace BankOneMobile.SessionNodes
{
    using System;
    using System.Activities;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Windows.Markup;
using System.ComponentModel;
using BankOneMobile.SessionNodeDesigners;
    using BankOneMobile.Services;
    using System.Activities.Presentation;

    [Designer(typeof(FlowDesigner))]
    [ContentProperty("Nodes")]
    public sealed class Flow : NativeActivity
    {

        public OutArgument<string> Result { get; set; }

        [DefaultValue(null)]
        private Collection<State> _nodes;

        [DependsOn("InitialNode")]
        public Collection<State> Nodes
        {
            get
            {
                return (_nodes = _nodes ?? new Collection<State>());
            }
        }

        private LinkedList<State> _UsedStates;

        private Variable<string> currentNode = new Variable<string>();
        private Dictionary<string, State> allNodes = new Dictionary<string, State>();

        public Collection<Variable> Variables { get; set; }
      
        public Flow()
        {
            
            Variables = new Collection<Variable>();
        }

        /// <summary>
        /// It represents the start point of the StateMachine.
        /// </summary>
        [DefaultValue(null)]
        public State InitialNode
        {
            get;
            set;
        }

        protected override bool CanInduceIdle
        {
            get
            {
                return true;
            }
        }

        private CompletionCallback onActionCompleted;


        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddImplementationVariable(this.currentNode);
            if (this.Nodes != null)
            {
                this.allNodes.Clear();
                foreach (State node in this.Nodes)
                {
                    node.TheFlow = this;
                    if (node != null && node.StateId != null)
                    {
                        node.GetChildActivities(metadata);
                        //metadata.AddChild(node.InternalState);
                        allNodes.Add(node.StateId, node);
                        //if (node is ScreenNode)
                        //{
                        //    if ((node as ScreenNode).Response != null)
                        //    {
                        //        metadata.AddChild((node as ScreenNode).Response.Expression);
                        //    //    this.Variables.Add((node as ScreenNode).Result);
                        //    }
                        //    //metadata.AddArgument();
                        //}
                    }
                }
            }
            metadata.SetVariablesCollection(this.Variables);


            //this.GatherAllNodes(metadata);
            //
            //for (int i = 0; i < this.allNodes.Count; i++)
            //{
            //    this.allNodes[i].GetChildActivities(children);
            //}
            //List<Activity> list = new List<Activity>(children.Count);
            //foreach (Activity activity in children)
            //{
            //    list.Add(activity);
            //}
            //metadata.SetChildrenCollection(new Collection<Activity>(list));
        }

        protected override void Execute(NativeActivityContext context)
        {
            if (this.InitialNode != null)
            {
                this.ExecuteNodeChain(context, this.InitialNode, null);
            }

        }

        private void ExecuteNodeChain(NativeActivityContext context, State node, System.Activities.ActivityInstance completedInstance)
        {
            if (node == null)
            {
                if (context.IsCancellationRequested && (completedInstance.State != ActivityInstanceState.Closed))
                {
                    context.MarkCanceled();
                }
            }
            else if (context.IsCancellationRequested)
            {
                context.MarkCanceled();
            }
            else
            {
                State node2 = node;
                do
                {
                    node2.TheFlow = this;
                    State node3;
                    if (this.ExecuteSingleNode(context, node2, out node3))
                    {
                        node2 = node3;
                    }
                    else
                    {
                        this.currentNode.Set(context, node2.StateId);
                        node2 = null;
                    }
                }
                while (node2 != null);
            }
        }

        private bool ExecuteSingleNode(NativeActivityContext context, State node, out State nextNode)
        {

            // step = node as ActionNode;
            if (node != null)
            {
                if (this.onActionCompleted == null)
                {
                    this.onActionCompleted = new CompletionCallback(this.onActionNodeCompleted);
                }
                return node.SingleExecute(context, this.onActionCompleted, out nextNode);
            }
            nextNode = null;
            return false;//switch2.Execute(context, this);
        }

        private void onActionNodeCompleted(NativeActivityContext context, System.Activities.ActivityInstance completedInstance)
        {
            State currentNode = this.GetCurrentNode(context) as State;
            State next = currentNode.Next;
            this.ExecuteNodeChain(context, next, completedInstance);
        }

        private State GetCurrentNode(NativeActivityContext context)
        {
            string stateId = this.currentNode.Get(context);
            return this.allNodes[stateId];
        }

        public void OnActionNodeFaulted(NativeActivityFaultContext context, Exception fault, System.Activities.ActivityInstance completedInstance)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            if (currentWorkflowInfo != null && currentWorkflowInfo.AttachedFaultCallback != null)
            {
                currentWorkflowInfo.AttachedFaultCallback.Invoke(context, fault, completedInstance);
            }
        }

        public void Resume_Flow_Bookmark(NativeActivityContext context, Bookmark bookmark, object value)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            if (currentWorkflowInfo != null && currentWorkflowInfo.AttachedBookmarkCallback != null)
            {
                currentWorkflowInfo.AttachedBookmarkCallback.Invoke(context, bookmark, value);
            }
        }

        public void Resume_Flow_Completed(NativeActivityContext context, ActivityInstance instance)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            if (currentWorkflowInfo != null && currentWorkflowInfo.AttachedCompletedCallback != null)
            {
                currentWorkflowInfo.AttachedCompletedCallback.Invoke(context, instance);
            }
        }

    }
   
}

