﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using BankOneMobile.Services;
using System.Activities.Presentation.Services;
using System.Activities.Presentation.Model;
using System.Collections;

namespace BankOneMobile.SessionNodeDesigners
{
    public abstract class ScreenNode : State
    {
        protected CompletionCallback _OnCompleted;
        private OutArgument<string> resp;
        private OutArgument<string> xmlResp;
       // [TypeConverter(typeof(VariablesConverter))]
        public OutArgument<string> Response
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return this.InternalActivity.Response;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    this.InternalActivity.Response = value;
                }
                resp = value;
            }
            //set;
            //get;

        }
        
        public OutArgument<string> XmlResponse
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return this.InternalActivity.XmlResponse;
            }
            set
            {
                if(this.InternalActivity != null)
                {
                    this.InternalActivity.Response = value;
                }
                xmlResp = value;
            }
        }


        [Browsable(false)]
        public override State Next
        {
            get;
            set;
        }

        [Browsable(false)]
        public SubActivity InternalActivity
        {
            get;
            set;
        }

        RuntimeArgument arg;
        public abstract string GetDisplay(NativeActivityContext context);

        /// <summary>
        /// The display text in XML format
        /// </summary>
        /// <param name="context"></param>
        /// <returns>XML string</returns>
        public abstract string GetXmlDisplay(NativeActivityContext context);

        /// <summary>
        /// The display text in XML format
        /// </summary>
        /// <param name="context"></param>
        /// <param name="textToAdd">The text to add to the display</param>
        /// <returns>XML string</returns>
        public virtual string GetXmlDisplay(NativeActivityContext context, string textToAdd)
        {
            return this.GetXmlDisplay(context);
        }


        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            // currentWorkflowInfo.Outputs = outputs;
            // currentWorkflowInfo.BookmarkToResume = context.GetValue<string>(BookMark);

            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.BookmarkToResume = Guid.NewGuid().ToString();
            currentWorkflowInfo.AttachedBookmarkCallback = Resume_Screen_Bookmark;

            context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));

            nextNode = null;
            _OnCompleted = onCompleted;
            return false;
        }

        protected void Resume_Screen_Bookmark(NativeActivityContext context, Bookmark bookmark, object value)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string response = Convert.ToString(value);
            if (this.InternalActivity != null)
            {
                this.InternalActivity.TheResult = response;
                this.InternalActivity.TheResponse = ProcessResponse(response);
                currentWorkflowInfo.AttachedCompletedCallback = OnSubCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                return;
            }

           // Response.Set(context, response);
            
            currentWorkflowInfo.DisplayMessage = null;
            currentWorkflowInfo.XmlDisplayMessage = null;
            if (currentWorkflowInfo.Outputs.ContainsKey(this.StateId))
            {
                currentWorkflowInfo.Outputs[this.StateId] = response;
            }
            else
            {
                currentWorkflowInfo.Outputs.Add(this.StateId, response);
            }

            Resume_Bookmark(currentWorkflowInfo,context, response);
        }

        protected virtual string ProcessResponse(string value)
        {
            return value;
        }

        internal void OnSubCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            string response = (instance.Activity as SubActivity).TheResult;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.DisplayMessage = null;
            currentWorkflowInfo.XmlDisplayMessage = null;
            if (currentWorkflowInfo.Outputs.ContainsKey(this.StateId))
            {
                currentWorkflowInfo.Outputs[this.StateId] = response;
            }
            else
            {
                currentWorkflowInfo.Outputs.Add(this.StateId, response);
            }

            Resume_Bookmark(currentWorkflowInfo, context, response);
        }

        protected abstract void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response);

        public override void GetChildActivities(NativeActivityMetadata metadata)
        {
            if (this.InternalActivity != null)
            {
                metadata.AddChild(this.InternalActivity);
            }
           // arg = new RuntimeArgument("Response" + this.StateId, typeof(string), ArgumentDirection.Out);
           // metadata.Bind(Response, arg);T
           //// metadata.Bind(Result, TheResult);
           //metadata.AddArgument(arg);
            //metadata.AddVariable(Result);
        }

        public override string GetDefaultDisplayName()
        {
            return "New Screen";
        }
    }

    public class SubActivity : CodeActivity
    {
        public OutArgument<string> Response { get; set; }
        public OutArgument<string> XmlResponse { get; set; }
        
        public string TheResult { get; set; }
        public string ProcessesResponse { get; set; }
        public string TheResponse { get; set; }
        public string TheXmlResponse { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            context.SetValue(Response, TheResponse);
            context.SetValue(XmlResponse, TheXmlResponse); 
        }
    }

    internal class VariablesConverter : StringConverter
    {

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            ModelService modelService = (ModelService)context.GetService(typeof(ModelService));
            IEnumerable<ModelItem> flowCollection = modelService.Find(modelService.Root, typeof(Flow));
            ModelItem theFlow = flowCollection.FirstOrDefault();

            ArrayList variables = new ArrayList();
            foreach (ModelItem shv in theFlow.Properties["Variables"].Collection)
            {
                variables.Add((shv.GetCurrentValue() as Variable).Name);
            }
            return new StandardValuesCollection(variables);
        }
    }
}
