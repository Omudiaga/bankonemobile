﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.AuxilliaryDB.Mappings
{
    public class MobileAccountMap : EntityWithEnableDisableMap<MobileAccount>
    {
        public MobileAccountMap()
        {
            Map(x => x.CustomerID);
            Map(x => x.LastName);
            Map(x => x.OtherNames);
           // Map(x => x.EmailAddress);
            Map(x => x.MobilePhone);
            Map(x => x.PIN);
            Map(x => x.ActivationCode);
            Map(x => x.MobileAccountStatus);
           // Map(x => x.Sex);
            //Map(x => x.DOB);
           // Map(x => x.Address);
            HasMany<LinkingBankAccount>(x => x.BankAccounts).Cascade.All().Not.LazyLoad();
            HasMany<LinkingBankAccount>(x => x.SelectedBankAccounts).Cascade.All().Not.LazyLoad();
            Map(x => x.IsAgentAccount);
            Map(x => x.OpenedBy);
            
            Map(x => x.RegistrarID);
            Map(x => x.DateActivated);
            Map(X => X.DateCreated);
            Map(x => x.StandardPhoneNumber);
            Map(x => x.InstitutionCode);
            Map(x => x.PinTries);
            References<LinkingBankAccount>(x => x.RecievingBankAccount).Cascade.All().Not.LazyLoad();
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
