﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.AuxilliaryDB.Mappings
{
    public class LinkingBankAccountMap : EntityMap<LinkingBankAccount>
    {
        public LinkingBankAccountMap()
        {
            Map(x => x.BankAccount);
            References<MobileAccount>(x => x.TheMobileAccount).Not.LazyLoad();
            //References<Product>(x => x.TheProduct);
            //References<Host>(x => x.TheHost);
            Map(x => x.CustomerID);
            Map(x => x.Activated);
            Map(x => x.InstitutionCode);
            Map(x => x.Passport);
        }
    }
}
