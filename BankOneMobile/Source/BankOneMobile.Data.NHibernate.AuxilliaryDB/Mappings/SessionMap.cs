﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.AuxilliaryDB.Mappings
{
    public class SessionMap : EntityWithEnableDisableMap<Session>
    {
        public SessionMap()
        {
            Map(x => x.MobilePhoneNumber);
            Map(x => x.SessionID);
            References<Session>(x => x.ChildSession).Cascade.All();
            HasMany<Transaction>(x => x.TheTransactions).Cascade.All().Not.LazyLoad();
            
            Map(x => x.Status);
            Map(x => x.BillingDescription);
            Map(x => x.SavedOnCoreBanking);
            Map(x => x.StartTime);
            Map(x=>x.StopTime);
            Map(x => x.Amount);
            Map(x => x.SessionLength);
            Map(x => x.Payer);
            Map(x => x.TimeInterval);
            
            
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
