﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankOneMobile.SwitchIntegration.ISOConnection {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Message", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.C" +
        "lient.Messages")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.MobileBasedMessage))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.FinancialMessage))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry))]
    public partial class Message : Trx.Messaging.Iso8583.Iso8583Message, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MobileBasedMessage", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.C" +
        "lient.Messages")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.FinancialMessage))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry))]
    public partial class MobileBasedMessage : BankOneMobile.SwitchIntegration.ISOConnection.Message {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FinancialMessage", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.C" +
        "lient.Messages")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry))]
    public partial class FinancialMessage : BankOneMobile.SwitchIntegration.ISOConnection.MobileBasedMessage {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BalanceEnquiry", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.C" +
        "lient.Messages")]
    [System.SerializableAttribute()]
    public partial class BalanceEnquiry : BankOneMobile.SwitchIntegration.ISOConnection.FinancialMessage {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="IsoMessage", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.D" +
        "TO")]
    [System.SerializableAttribute()]
    public partial class IsoMessage : BankOneMobile.Core.Implementations.Entity, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int _TypeField;
        
        private long Amountk__BackingFieldField;
        
        private string CardAcceptorIDk__BackingFieldField;
        
        private string CardPANk__BackingFieldField;
        
        private string TerminalIDk__BackingFieldField;
        
        private string TransactionTypeCodek__BackingFieldField;
        
        private byte[] m_MessageField;
        
        private string m_OriginalDataElementsField;
        
        private BankOneMobile.SwitchIntegration.ISOConnection.MessageEntity m_ReceiverField;
        
        private string m_ReceiverIPField;
        
        private System.DateTime m_RequestDateField;
        
        private System.DateTime m_ResponseDateField;
        
        private byte[] m_ResponseMessageField;
        
        private long m_STANField;
        
        private BankOneMobile.SwitchIntegration.ISOConnection.MessageEntity m_SenderField;
        
        private string m_SenderIPField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _Type {
            get {
                return this._TypeField;
            }
            set {
                if ((this._TypeField.Equals(value) != true)) {
                    this._TypeField = value;
                    this.RaisePropertyChanged("_Type");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Amount>k__BackingField", IsRequired=true)]
        public long Amountk__BackingField {
            get {
                return this.Amountk__BackingFieldField;
            }
            set {
                if ((this.Amountk__BackingFieldField.Equals(value) != true)) {
                    this.Amountk__BackingFieldField = value;
                    this.RaisePropertyChanged("Amountk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<CardAcceptorID>k__BackingField", IsRequired=true)]
        public string CardAcceptorIDk__BackingField {
            get {
                return this.CardAcceptorIDk__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.CardAcceptorIDk__BackingFieldField, value) != true)) {
                    this.CardAcceptorIDk__BackingFieldField = value;
                    this.RaisePropertyChanged("CardAcceptorIDk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<CardPAN>k__BackingField", IsRequired=true)]
        public string CardPANk__BackingField {
            get {
                return this.CardPANk__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.CardPANk__BackingFieldField, value) != true)) {
                    this.CardPANk__BackingFieldField = value;
                    this.RaisePropertyChanged("CardPANk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<TerminalID>k__BackingField", IsRequired=true)]
        public string TerminalIDk__BackingField {
            get {
                return this.TerminalIDk__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.TerminalIDk__BackingFieldField, value) != true)) {
                    this.TerminalIDk__BackingFieldField = value;
                    this.RaisePropertyChanged("TerminalIDk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<TransactionTypeCode>k__BackingField", IsRequired=true)]
        public string TransactionTypeCodek__BackingField {
            get {
                return this.TransactionTypeCodek__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.TransactionTypeCodek__BackingFieldField, value) != true)) {
                    this.TransactionTypeCodek__BackingFieldField = value;
                    this.RaisePropertyChanged("TransactionTypeCodek__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public byte[] m_Message {
            get {
                return this.m_MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.m_MessageField, value) != true)) {
                    this.m_MessageField = value;
                    this.RaisePropertyChanged("m_Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string m_OriginalDataElements {
            get {
                return this.m_OriginalDataElementsField;
            }
            set {
                if ((object.ReferenceEquals(this.m_OriginalDataElementsField, value) != true)) {
                    this.m_OriginalDataElementsField = value;
                    this.RaisePropertyChanged("m_OriginalDataElements");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public BankOneMobile.SwitchIntegration.ISOConnection.MessageEntity m_Receiver {
            get {
                return this.m_ReceiverField;
            }
            set {
                if ((this.m_ReceiverField.Equals(value) != true)) {
                    this.m_ReceiverField = value;
                    this.RaisePropertyChanged("m_Receiver");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string m_ReceiverIP {
            get {
                return this.m_ReceiverIPField;
            }
            set {
                if ((object.ReferenceEquals(this.m_ReceiverIPField, value) != true)) {
                    this.m_ReceiverIPField = value;
                    this.RaisePropertyChanged("m_ReceiverIP");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.DateTime m_RequestDate {
            get {
                return this.m_RequestDateField;
            }
            set {
                if ((this.m_RequestDateField.Equals(value) != true)) {
                    this.m_RequestDateField = value;
                    this.RaisePropertyChanged("m_RequestDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.DateTime m_ResponseDate {
            get {
                return this.m_ResponseDateField;
            }
            set {
                if ((this.m_ResponseDateField.Equals(value) != true)) {
                    this.m_ResponseDateField = value;
                    this.RaisePropertyChanged("m_ResponseDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public byte[] m_ResponseMessage {
            get {
                return this.m_ResponseMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.m_ResponseMessageField, value) != true)) {
                    this.m_ResponseMessageField = value;
                    this.RaisePropertyChanged("m_ResponseMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public long m_STAN {
            get {
                return this.m_STANField;
            }
            set {
                if ((this.m_STANField.Equals(value) != true)) {
                    this.m_STANField = value;
                    this.RaisePropertyChanged("m_STAN");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public BankOneMobile.SwitchIntegration.ISOConnection.MessageEntity m_Sender {
            get {
                return this.m_SenderField;
            }
            set {
                if ((this.m_SenderField.Equals(value) != true)) {
                    this.m_SenderField = value;
                    this.RaisePropertyChanged("m_Sender");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string m_SenderIP {
            get {
                return this.m_SenderIPField;
            }
            set {
                if ((object.ReferenceEquals(this.m_SenderIPField, value) != true)) {
                    this.m_SenderIPField = value;
                    this.RaisePropertyChanged("m_SenderIP");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MessageEntity", Namespace="http://schemas.datacontract.org/2004/07/BankOneMobile.SwitchIntegration.ISO8583.D" +
        "TO")]
    public enum MessageEntity : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Client = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Server = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ExternalEntity = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Bridge = 4,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ISOConnection.IISOInterfaceWebService")]
    public interface IISOInterfaceWebService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IISOInterfaceWebService/DoWork", ReplyAction="http://tempuri.org/IISOInterfaceWebService/DoWorkResponse")]
        void DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IISOInterfaceWebService/DoISOTransaction", ReplyAction="http://tempuri.org/IISOInterfaceWebService/DoISOTransactionResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Trx.Messaging.Iso8583.Iso8583Message))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.FinancialMessage))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.MobileBasedMessage))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.Message))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.IsoMessage))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.MessageEntity))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.Core.Implementations.MobileAccount))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.Core.Implementations.EntityWithEnableDisable))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.Core.Implementations.Entity))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.Core.Implementations.TransactionResponse))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(System.Collections.Generic.Dictionary<object, object>))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Trx.Messaging.Message))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Trx.Messaging.MessagingComponent))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Trx.Messaging.FieldCollection))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Trx.Messaging.MessageHeader))]
        BankOneMobile.Core.Implementations.TransactionResponse DoISOTransaction(string pin, object tranType, BankOneMobile.Core.Implementations.MobileAccount mob, string fromAccountNumber, string ToaccountNumber, decimal amount, string sessionID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IISOInterfaceWebService/SendActualISO", ReplyAction="http://tempuri.org/IISOInterfaceWebService/SendActualISOResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.Message))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.MobileBasedMessage))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.FinancialMessage))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry))]
        Trx.Messaging.Iso8583.Iso8583Message SendActualISO(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry message);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IISOInterfaceWebService/RunTransaction", ReplyAction="http://tempuri.org/IISOInterfaceWebService/RunTransactionResponse")]
        BankOneMobile.Core.Implementations.TransactionResponse RunTransaction(string pin, BankOneMobile.Core.Implementations.MobileAccount mob, string from, string to, decimal amount, string sessionID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IISOInterfaceWebServiceChannel : BankOneMobile.SwitchIntegration.ISOConnection.IISOInterfaceWebService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ISOInterfaceWebServiceClient : System.ServiceModel.ClientBase<BankOneMobile.SwitchIntegration.ISOConnection.IISOInterfaceWebService>, BankOneMobile.SwitchIntegration.ISOConnection.IISOInterfaceWebService {
        
        public ISOInterfaceWebServiceClient() {
        }
        
        public ISOInterfaceWebServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ISOInterfaceWebServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ISOInterfaceWebServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ISOInterfaceWebServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void DoWork() {
            base.Channel.DoWork();
        }
        
        public BankOneMobile.Core.Implementations.TransactionResponse DoISOTransaction(string pin, object tranType, BankOneMobile.Core.Implementations.MobileAccount mob, string fromAccountNumber, string ToaccountNumber, decimal amount, string sessionID) {
            return base.Channel.DoISOTransaction(pin, tranType, mob, fromAccountNumber, ToaccountNumber, amount, sessionID);
        }
        
        public Trx.Messaging.Iso8583.Iso8583Message SendActualISO(BankOneMobile.SwitchIntegration.ISOConnection.BalanceEnquiry message) {
            return base.Channel.SendActualISO(message);
        }
        
        public BankOneMobile.Core.Implementations.TransactionResponse RunTransaction(string pin, BankOneMobile.Core.Implementations.MobileAccount mob, string from, string to, decimal amount, string sessionID) {
            return base.Channel.RunTransaction(pin, mob, from, to, amount, sessionID);
        }
    }
}
