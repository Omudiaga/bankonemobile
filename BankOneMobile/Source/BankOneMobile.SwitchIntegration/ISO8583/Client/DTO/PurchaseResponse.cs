using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using System.Runtime.Serialization;
using System.Xml.Linq;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    [DataContract]
    public class PurchaseResponse : MessageResponse
    {
        public PurchaseResponse(Trx.Messaging.Message responseMessage, AppZoneSwitch.Core.Contracts.ITransaction sourceTrans,AppZoneSwitch.Core.Contracts.ITransaction  originalTrans)
            : base(responseMessage,sourceTrans,originalTrans)
        {
            if (responseMessage.Fields.Contains(4))
            {
                this._amount = new Amount(Convert.ToInt64(responseMessage.Fields[4].Value), responseMessage.Fields[49].Value.ToString(), AmountType.Approved);
                
            }
            if (responseMessage.Fields[127].Value != null)
            {
                Trx.Messaging.Message msg = responseMessage.Fields[127].Value as Trx.Messaging.Message;
                if (msg.Fields[22] != null)
                {
                    string responseXML = msg.Fields[22].Value.ToString();
                    int val = responseXML.IndexOf('<');
                    responseXML = responseXML.Substring(val);
                    if (!string.IsNullOrEmpty(responseXML))
                    {
                        XDocument document = XDocument.Parse(responseXML);


                        foreach (var doc in document.Descendants("Response"))
                        {


                            this.TransactionReference = doc.Element("TransactionRef") != null ? doc.Element("TransactionRef").Value : string.Empty;


                            
                        }
                    }
                }
            }
            
        }

        private Amount _amount;

        [DataMember]
        public Amount Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private string  _valuePurchased;
        private string _transactionRefernce;
        private string _purchaseRefernce;
        private string  _purchaseResponseCode;
        private string _purchaseResponseDescription;

        [DataMember]
        public string  ValuePurchased 
        {
            get { return _valuePurchased; }
            set { _valuePurchased = value; }
        }
        public string TransactionReference
        {
            get { return _transactionRefernce; }
            set { _transactionRefernce = value; }
        }
        public string PurchaseResponseCode
        {
            get { return _purchaseResponseCode; }
            set { _purchaseResponseCode = value; }
        }
        public string PurchaseResponseDescription
        {
            get { return _purchaseResponseDescription; }
            set { _purchaseResponseDescription = value; }
        }

    }
}
