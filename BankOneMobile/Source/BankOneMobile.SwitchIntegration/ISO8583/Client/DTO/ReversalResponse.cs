using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    [DataContract]
    public class ReversalResponse : MessageResponse
    {
        public ReversalResponse(Trx.Messaging.Message responseMessage, AppZoneSwitch.Core.Contracts.ITransaction sourceTrans, AppZoneSwitch.Core.Contracts.ITransaction originalTrans)
            : base(responseMessage, sourceTrans, originalTrans)
        {

        }
    }
}
