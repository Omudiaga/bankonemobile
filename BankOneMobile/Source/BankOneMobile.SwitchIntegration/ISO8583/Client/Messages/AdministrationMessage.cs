using System;
using System.Collections.Generic;
using System.Text;
using Trx.Messaging.Iso8583;
using Trx.Messaging;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public  class AdministrationMessage : MobileBasedMessage
    {

        public AdministrationMessage(TerminalOwner  terminalOwnerID, string transactionID, TransactionType transType, MobileAccountDetails theMobileAccount, string fromAccountType, string toAccountType, bool isRepeat,string instCode)
            : base(200, terminalOwnerID, theMobileAccount, transactionID, isRepeat)
        {
            this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)transType).PadLeft(2, '0'),fromAccountType,toAccountType));

            this.Fields.Add(FieldNos.F4_TransAmount, "0");

            this.Fields.Add(FieldNos.F49_TransCurrencyCode, "566");

           this.Fields.Add(100, instCode);

            Trx.Messaging.Message msg = new Trx.Messaging.Message();
            msg.Fields.Add(19, "Appzone Group Ltd");

            this.Fields.Add(127, msg);
        }

    }
}
