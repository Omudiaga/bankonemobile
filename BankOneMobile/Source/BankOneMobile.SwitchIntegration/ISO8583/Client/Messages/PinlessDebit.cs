﻿using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class PinlessDebit : FinancialMessage
    {
        public PinlessDebit(TerminalOwner terminalOwner, Account fromAccount, Account toAccount, Amount transferAmount, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string toInstitutionCode, string transactionID, bool isRepeat, string narration, decimal? fee, Service? service)
            : base(terminalOwner, transactionID, TransactionType.Payment, theMobileAccount, fromInstitutionCode, fromAccount.Type, null, transferAmount, isRepeat, narration)
        {
            if (fee.HasValue)
            {
                this.Fields.Add(FieldNos.F28_Surcharge, String.Format("{0}{1}", "D", fee.ToString().PadLeft(8, '0')));
            }
            if(!string.IsNullOrEmpty(fromInstitutionCode))
            {
                this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
            }
            if (fromAccount != null)
            {
                this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
            }
            if (toAccount != null)
            {
                this.Fields.Add(FieldNos.F103_Account2, toAccount.Number);
            }
            if(!string.IsNullOrEmpty(toInstitutionCode))
            {
                this.Fields.Add(FieldNos.F101_InstitutionCode, toInstitutionCode);
            }
            if (service.Value != Service.NullService)
            {
                string val = ((int)service.Value).ToString();
                this.Fields.Add(FieldNos.F94_ServiceCode, val.PadLeft(4, '0'));
            }
        }
    }
}
