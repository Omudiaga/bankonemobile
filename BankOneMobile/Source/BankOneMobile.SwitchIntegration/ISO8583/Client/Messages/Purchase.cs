using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class Purchase : FinancialMessage
    {
        //public Purchase(TerminalOwner terminalOwner, Account fromAccount, Account toAccount, Amount transferAmount, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string toInstitutionCode, string transactionID, bool isRepeat, string xmlData, string narration, MerchantType? mercType,Service? codes)
        //    : base(terminalOwner, transactionID, TransactionType.Purchase, theMobileAccount, fromInstitutionCode, fromAccount.Type, toAccount.Type, transferAmount, isRepeat, narration)
        //{
        //    if (fromAccount != null)
        //    {
        //        this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
        //    }

        //    if (toAccount != null)
        //    {
        //        //this.Fields.Add(FieldNos.F103_Account2, toAccount.Number);
        //    }
        //    // this.Fields.Add(FieldNos.F103_Account2, "00050020010000047");
        //    //this.Fields.Add(FieldNos.F32_AcquiringInstitutionIDCode, toInstitutionCode);
        //    this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
        //    Trx.Messaging.Message subMessage = new Trx.Messaging.Message();
        //    subMessage.Fields.Add(22, xmlData);
        //    //this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}",  fromAccount.Type, fromAccount.Type,Convert.ToString((int)mercType)).PadLeft(6, '0'));
        //    if (codes.Value != Service.NullService)
        //    {
        //        string val = ((int)codes.Value).ToString();
        //        this.Fields.Add(FieldNos.F94_ServiceCode, val);
        //    }
        //    if (mercType.HasValue)
        //    {
        //        string val = ((int)mercType).ToString();
        //        this.Fields.Add(FieldNos.F18_MerchantType, val.Substring(val.Length-1));
        //    }
        //    this.Fields.Add(FieldNos.F18_MerchantType, Convert.ToString((int)mercType).PadLeft(4, '0'));
        //    this.Fields.Add(127, subMessage);

        //}
        public Purchase(TerminalOwner terminalOwner, Account fromAccount, Account toAccount, Amount transferAmount, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string toInstitutionCode, string transactionID, bool isRepeat, string xmlData, bool isBillsPayment, string narration, decimal fee, MerchantType? type,Service? codes)
            : base(terminalOwner, transactionID, TransactionType.Purchase, theMobileAccount, fromInstitutionCode, fromAccount.Type, toAccount.Type, transferAmount, isRepeat, narration)
        {
            if (fromAccount != null)
            {
                this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
            }

            
            //this.Fields.Add(FieldNos.F103_Account2, "00050020010000047");
            //this.Fields.Add(FieldNos.F32_AcquiringInstitutionIDCode, toInstitutionCode);
            this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
            Trx.Messaging.Message subMessage = new Trx.Messaging.Message();
            subMessage.Fields.Add(22, xmlData);
            this.Fields.Add(127, subMessage);
            this.Fields.Add(FieldNos.F28_Surcharge, string.Format("{0}{1}", "D", fee.ToString().PadLeft(8, '0')));
            this.Fields.Add(FieldNos.F18_MerchantType,Convert.ToString((int)type).PadLeft(4, '0'));
            //if (codes.HasValue)
            //{
            //    this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)codes.Value), fromAccount.Type, fromAccount.Type).PadLeft(6, '0'));
            //}
            if (codes.Value != Service.NullService)
            {
                string val = ((int)codes.Value).ToString();
                this.Fields.Add(FieldNos.F94_ServiceCode, val);
            }
            if (type.HasValue)
            {
                string val = ((int)type).ToString();
                this.Fields.Add(FieldNos.F18_MerchantType, val.Substring(val.Length - 1));
            }
            //this.Fields.Add(FieldNos.F37_RetrievalReference, Convert.ToString((int)ISOServiceCodes.Recharge));

        }
    }
}
