using System;
using System.Collections.Generic;
using System.Text;
using Trx.Messaging.Iso8583;
using Trx.Messaging;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using System.Runtime.Serialization;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    [DataContract]
    public abstract class MobileBasedMessage : Message
    {

        public MobileBasedMessage(int messageTypeIdentifier, TerminalOwner terminalOwner, MobileAccountDetails theMobileAccount, string transactionID, bool isRepeat)
            : base(messageTypeIdentifier, transactionID, isRepeat)
        {
            this.Fields.Add(FieldNos.F2_PAN, theMobileAccount.PhoneNumber);
            
           

            this.Fields.Add(FieldNos.F22_PosEntryMode, "011");
            this.Fields.Add(FieldNos.F25_PosConditionCode, "00");
            this.Fields.Add(FieldNos.F26_PinCaptureCode, "12");

            //this.Fields.Add(123, "100040165110119");
            this.Fields.Add(123, "000000000000000");

            if (theMobileAccount.PIN!=null)
            {
                this.Fields.Add(FieldNos.F52_PinData, theMobileAccount.PIN);
            }
            
            this.Fields.Add(FieldNos.F41_CardAcceptorTerminalCode, terminalOwner.TerminalID);
            this.Fields.Add(FieldNos.F42_CardAcceptorIDCode, terminalOwner.ID);
            this.Fields.Add(FieldNos.F43_CardAcceptorNameLocation, string.Format("{0}{1}{2}{3}", terminalOwner.Location, terminalOwner.City, terminalOwner.State, terminalOwner.Country));

        }


    }
}
