using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class Payment : FinancialMessage
    {
        public Payment(TerminalOwner terminalOwner, Account fromAccount, Account toAccount, Amount transferAmount, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string toInstitutionCode, string transactionID, bool isRepeat, string narration, ServiceCodes codes, decimal? fee,MerchantType? type,Service? service,string xmlData)
            : base(terminalOwner, transactionID, TransactionType.Payment, theMobileAccount,fromInstitutionCode, fromAccount.Type, toAccount.Type, transferAmount, isRepeat,narration)
        {

            if (!string.IsNullOrEmpty(xmlData))
            {
                Trx.Messaging.Message subMessage = new Trx.Messaging.Message();
                subMessage.Fields.Add(22, xmlData);
                this.Fields.Add(127, subMessage);
            }

            if (fee.HasValue)
            {
                this.Fields.Add(FieldNos.F28_Surcharge, String.Format("{0}{1}", "D", fee.ToString().PadLeft(8, '0')));
            }
            if (codes ==ServiceCodes.ToServiceAccountPayment)
            {
                this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
                this.Fields.Add(FieldNos.F102_Account1,fromAccount.Number);

               // this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)codes), fromAccount.Type, fromAccount.Type).PadLeft(6, '0'));
            }
            else if (codes == ServiceCodes.FromServiceAccountPayment)
            {
                this.Fields.Add(FieldNos.F103_Account2, toAccount.Number);
                this.Fields.Add(FieldNos.F101_InstitutionCode, toInstitutionCode);

                // this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)codes), fromAccount.Type, fromAccount.Type).PadLeft(6, '0'));
            }
            else
            {
                this.Fields.Add(FieldNos.F103_Account2, toAccount.Number);
                this.Fields.Add(FieldNos.F101_InstitutionCode, toInstitutionCode);
                this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
                this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
            }
            this.Fields.Add(FieldNos.F37_RetrievalReference, Convert.ToString((int)codes));
            
         
         if (service.Value!=Service.NullService)
         {
             string val = ((int)service.Value).ToString();
             this.Fields.Add(FieldNos.F94_ServiceCode, val.PadLeft(4,'0'));
         }
         else if (type.HasValue&&type.Value!=MerchantType.NullMerchantType)
         {
             string val = ((int)type).ToString();
             this.Fields.Add(FieldNos.F18_MerchantType, val.Substring(val.Length - 1));
         }
        }
    }
}
