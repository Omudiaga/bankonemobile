using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class FundsTransfer : FinancialMessage
    {
        public FundsTransfer(TerminalOwner terminalOwner, Account fromAccount, Account toAccount, Amount transferAmount, MobileAccountDetails theMobileAccount,string fromInstitutionCode,string toInstitutionCode, string transactionID, bool isRepeat,string xmlData,string narration,decimal fee,ISOServiceCodes? isoServ,MerchantType ? type)
            : base(terminalOwner, transactionID, TransactionType.AccountsTransfer, theMobileAccount,fromInstitutionCode, fromAccount.Type, toAccount.Type, transferAmount, isRepeat,narration)
        {
            this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
            //this.Fields.Add(FieldNos.F103_Account2, toAccount.Number);
           // this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, toInstitutionCode);
            this.Fields.Add(FieldNos.F101_InstitutionCode, fromInstitutionCode);

            //this.Fields.Add(FieldNos.F32_AcquiringInstitutionIDCode, toInstitutionCode);
            this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
            Trx.Messaging.Message subMessage = new Trx.Messaging.Message();
            subMessage.Fields.Add(22, xmlData);
            this.Fields.Add(127, subMessage);
            this.Fields.Add(FieldNos.F28_Surcharge, string.Format("{0}{1}", "D", fee.ToString().PadLeft(8, '0')));
            if (isoServ.HasValue)
            {
                this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)isoServ.Value), fromAccount.Type, fromAccount.Type).PadLeft(6, '0'));
            }
            if (type.HasValue)
            {
                string val = ((int)type).ToString();
                this.Fields.Add(FieldNos.F18_MerchantType, val.Substring(val.Length - 1));
            }
            //this.Fields.Add(FieldNos.F37_RetrievalReference, Convert.ToString((int)isoServ));
        }
    }
}
