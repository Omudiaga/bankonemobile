using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class ServicePayment : FinancialMessage
    {
        public ServicePayment(TerminalOwner terminalOwner, Account fromAccount, Account ToAccount, ServiceCodes codes, Amount transferAmount, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string toInstitutionCode, string transactionID, bool isRepeat, string narration,decimal fee,ISOServiceCodes? isoServ,MerchantType? type,Service? service)
            : base(terminalOwner, transactionID, TransactionType.Payment, theMobileAccount,fromInstitutionCode, AccountType.Default, AccountType.Default, transferAmount, isRepeat,narration)
        {
                  

            
              this.Fields.Add(FieldNos.F48_AdditionalData,((int)codes).ToString().PadLeft(2,'0'));
                            
                   
            if (codes == ServiceCodes.FromServiceAccountPayment)
            {
                this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
                this.Fields.Add(FieldNos.F101_InstitutionCode,toInstitutionCode);
                this.Fields.Add(FieldNos.F103_Account2, ToAccount.Number);
               // this.Fields.Add(FieldNos.F37_RetrievalReference, Convert.ToString((int)ISOServiceCodes.CashIn));
               // this.Fields.Add(FieldNos.F102_Account1, "00050020010000047");
            }

            else if (codes == ServiceCodes.ToServiceAccountPayment||codes==ServiceCodes.NormalPayment)
            {
                this.Fields.Add(FieldNos.F102_Account1, fromAccount.Number);
                this.Fields.Add(FieldNos.F100_ReceivingInstitutionID, fromInstitutionCode);
                
            }
            
            this.Fields.Add(FieldNos.F28_Surcharge, string.Format("{0}{1}", "D", fee.ToString().PadLeft(8, '0')));
            if (isoServ.HasValue)
            {
                this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)isoServ.Value), fromAccount.Type, fromAccount.Type).PadLeft(6, '0'));
            }

            if (service.HasValue)
            {
                string val = ((int)service).ToString();
                this.Fields.Add(FieldNos.F94_ServiceCode, val);
            }
            else if (type.HasValue && type.Value != MerchantType.NullMerchantType)
            {
                string val = ((int)type).ToString();
                this.Fields.Add(FieldNos.F18_MerchantType, val.Substring(val.Length - 1));
            }
            
           
        }
    }
}
