﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using Trx.Messaging;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    internal class FullStatement : AuthorizationMessage
    {
        public FullStatement(TerminalOwner terminalOwner, Account acct, MobileAccountDetails theMobileAccount, string transactionID, string request, bool isRepeat)
            : base(terminalOwner, transactionID, TransactionType.FullStatement, theMobileAccount, acct.Type, "00", new Amount(0, "566", AmountType.AvailableBalance), isRepeat)
        {
            this.Fields.Add(FieldNos.F102_Account1, acct.Number);

            Trx.Messaging.Message inner = new Trx.Messaging.Message();
            inner.Fields.Add(22,request);
            this.Fields.Add(127, inner);

        }
    }
}
