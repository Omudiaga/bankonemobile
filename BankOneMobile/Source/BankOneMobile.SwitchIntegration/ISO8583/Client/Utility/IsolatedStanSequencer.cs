using System;
using System.Collections.Generic;
using System.Text;
using Trx.Utilities;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization.Formatters.Binary;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Utility
{
    public class IsolatedStanSequencer : ISequencer
    {
        IsolatedStorageFile appFile = IsolatedStorageFile.GetMachineStoreForAssembly();
        private StanSequence sequence;
        private static object _lockObject = new object();
        public IsolatedStanSequencer()
        {
            byte[] buffer = null;
            lock(_lockObject)
            {
                IsolatedStorageFileStream fileStream = new IsolatedStorageFileStream("isoClient.ser", System.IO.FileMode.OpenOrCreate, appFile);
                buffer = new byte[fileStream.Length];
                fileStream.Read(buffer, 0, buffer.Length);
                fileStream.Close();
            }

            sequence = DeSerializeObject(buffer) as StanSequence;
            if (sequence == null)
            {
                sequence = new StanSequence();
            }
        }

        private Object DeSerializeObject(Byte[] serializedObject)
        {
            Object deSerializedObject = null;
            if (serializedObject != null)
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream(serializedObject);
                try
                {
                    deSerializedObject = new BinaryFormatter().Deserialize(ms);
                }
                catch
                {
                }
                finally
                {
                    ms.Close();
                }
            }
            return deSerializedObject;
        }

        private Byte[] SerializeObject(Object obj)
        {
            Byte[] serializedObject = null;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.TypeFormat = System.Runtime.Serialization.Formatters.FormatterTypeStyle.TypesAlways;
            bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Full;
            bf.Serialize(ms, obj);
            serializedObject = new Byte[ms.Length];
            ms.Position = 0;
            ms.Read(serializedObject, 0, (int)ms.Length);
            ms.Close();
            return serializedObject;
        }
        #region ISequencer Members

        public int CurrentValue()
        {
            return sequence.SequencerCurrentValue;
        }

        public int Increment()
        {
            sequence.SequencerCurrentValue += sequence.SequencerIncrementValue;
            if (sequence.SequencerCurrentValue > sequence.SequencerMaxValue)
            {
                sequence.SequencerCurrentValue = sequence.SequencerMinValue;
            }

            byte[] buffer = SerializeObject(sequence);
            lock (_lockObject)
            {
                IsolatedStorageFileStream fileStream = new IsolatedStorageFileStream("isoClient.ser", System.IO.FileMode.OpenOrCreate, appFile);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
            }
            return sequence.SequencerCurrentValue;
        }

        public int Maximum()
        {
            
            return sequence.SequencerMaxValue;
        }

        public int Minimum()
        {
            return sequence.SequencerMinValue;
        }

        #endregion
    }
}
