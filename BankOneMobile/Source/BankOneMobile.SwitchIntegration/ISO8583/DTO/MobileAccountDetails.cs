﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.SwitchIntegration.ISO8583.DTO
{




   public  class MobileAccountDetails
    {
        private byte[] _PIN = new byte[] { 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04 };
        private string _PhoneNumber;
       private string _ServiceCode;
        public byte[] PIN
        {
            get { return _PIN; }
            set { _PIN = value; }
        }
       public string PhoneNumber
       {
           get
           {
               return _PhoneNumber;
           }
           set
           {
               _PhoneNumber = value;
           }
       }
       public MobileAccountDetails(string phoneNumber, byte[] pin)
       {
           _PIN = pin;
           
           _PhoneNumber = phoneNumber;
       }
       public string ServiceCode
       {
           get { return _ServiceCode; }
           set{_ServiceCode=value;}
       }
    }
}
