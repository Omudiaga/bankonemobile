using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace BankOneMobile.SwitchIntegration.ISO8583.DTO
{
    public class Amount
    {
        private long _Balance;
        private string _CurrencyCode;
        private string _AmountType;
        private BalanceType _Type;

        /// <summary>
        /// Initializes a new instance of the <see cref="Amount"/> class.
        /// </summary>
        /// <param name="balance">The balance.</param>
        /// <param name="currencyCode">The currency code.</param>
        public Amount(long balance, string currencyCode, string amountType)
        {
            this._Balance = balance;
            this._AmountType = amountType;
            this._CurrencyCode = currencyCode;
        }

        public Amount(long balance, string currencyCode, string amountType, BalanceType type)
        {
            this._Balance = balance;
            this._AmountType = amountType;
            this._CurrencyCode = currencyCode;
            this._Type = type;
        }

        internal Amount(string isoAmount)
        {
            //Trace.TraceInformation(string.Format("I'm trying to convert {0}", isoAmount));
            if (!String.IsNullOrEmpty(isoAmount) && isoAmount.Length == 20)// && isoAmount.Length == 20)
            {
                this.AmountType = isoAmount.Substring(2, 2);
                this.CurrencyCode = isoAmount.Substring(4, 3);
                string type  =  isoAmount.Substring(7,1);
                this.TheBalanceType = (type == "c" || type == "C") ? BalanceType.Credit : BalanceType.Debit;
                this._Balance = Convert.ToInt64( isoAmount.Substring(8, 12));

                //this._AmountType = isoAmount.Substring(2, 2);
                //this._CurrencyCode = isoAmount.Substring(4, 3);
                //this._Balance = Convert.ToInt64(isoAmount.Substring(8));
                // this._Balance *= isoAmount.Substring(7, 1).Equals("C", StringComparison.CurrentCultureIgnoreCase) ? 1 : -1;
            }
            else if(isoAmount!=null)
            {
                this._Balance = Convert.ToInt64(isoAmount);
            }
        }

        /// <summary>
        /// Gets or sets the balance.
        /// </summary>
        /// <value>The balance.</value>
        public long Balance
        {
            get
            {
                return this._Balance;
            }
            set
            {
                this._Balance = value;
            }
        }

        /// <summary>
        /// Gets or sets the balance type(debit or credit).
        /// </summary>
        /// <value>The balance type enumeration value.</value>
        public BalanceType TheBalanceType
        {
            get
            {
                return this._Type;
            }
            set
            {
                this._Type = value;
            }
        }

        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        /// <value>The currency code.</value>
        public string CurrencyCode
        {
            get
            {
                return this._CurrencyCode;
            }
            set
            {
                this._CurrencyCode = value;
            }
        }

        public string AmountType
        {
            get
            {
                return this._AmountType;
            }
            set
            {
                this._AmountType = value;
            }
        }

    }

    public enum BalanceType
    {
        Debit,
        Credit
    }
}
