﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace BankOneMobile.InfoBip
{
    public class InfoBipConfiguration
    {
        private static NameValueCollection InfoConfig
        {
            get
            {
                NameValueCollection section = System.Configuration.ConfigurationManager.GetSection("InfoBip") as NameValueCollection;
                return section ?? new NameValueCollection();
            }
        }

        public static string SmsUrl
        {
            get
            {
                return InfoConfig.Get("SmsUrl");
            }
        }

        public static string SmsUsername
        {
            get
            {
                return InfoConfig.Get("SmsUsername");
            }

        }

        public static string SmsPassword
        {
            get
            {
                return InfoConfig.Get("SmsPassword");
            }
        }



        public static string UssdApplicationId
        {
            get
            {
                return InfoConfig.Get("UssdApplicationId");
            }
        }

        public static string UssdUsername
        {
            get
            {
                return InfoConfig.Get("UssdUsername");
            }

        }

        public static string UssdPassword
        {
            get
            {
                return InfoConfig.Get("UssdPassword");
            }
        }
    }
}
