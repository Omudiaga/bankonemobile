﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.NHibernate;
using BankOneMobile.Data.NHibernate.FluentNHibernate.Conventions;
using System.Web;
using FluentNHibernate.Automapping;
using BankOneMobile.Data.NHibernate.Web;

namespace BankOneMobile.Extension
{
   public class ApplicationInitializer
    {
        public static void Init()
        {
            AppZoneSwitch.Extension.ApplicationInitializer.Init();
             HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();


             AutoPersistenceModel autoPersistenceModel = new AutoPersistenceModel();
             autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
             autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
             autoPersistenceModel.Conventions.Add<ClassMappingConvention>();

             NHibernateSession.Init(new SimpleSessionStorage(),
    new string[] {System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';')[0] },
                     autoPersistenceModel, DataSourceFactory.GetDataSource(DataCategory.Core)
                  );

             AutoPersistenceModel autoPersistenceModel2 = new AutoPersistenceModel();
             autoPersistenceModel2.Conventions.Add<ClassMappingConvention>();
             autoPersistenceModel2.Conventions.Add<HasManyConvention>();
             autoPersistenceModel2.Conventions.Add<ReferencesConvention>();


             NHibernateSession.Init(new SimpleSessionStorage(),
       new string[] { System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';')[0] },
                    autoPersistenceModel2, DataSourceFactory.GetDataSource(DataCategory.Shared)
                 );

             AutoPersistenceModel autoPersistenceModel3 = new AutoPersistenceModel();
             autoPersistenceModel3.Conventions.Add<ClassMappingConvention>();
             autoPersistenceModel3.Conventions.Add<HasManyConvention>();
             autoPersistenceModel3.Conventions.Add<ReferencesConvention>();

             string auxConfigFile = System.Configuration.ConfigurationManager.AppSettings["AuxilliaryNhibernateFile"];
             NHibernateSession.Init(new SimpleSessionStorage(),
       new string[] { System.Configuration.ConfigurationManager.AppSettings["AuxilliaryMappingAssembly"].Split(';')[0] },
                    autoPersistenceModel3,auxConfigFile, DataSourceFactory.GetDataSource(DataCategory.Auxilliary)
                 );
        }

        public static void InitWeb(HttpApplication app)
        {
            
            
            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
            AutoPersistenceModel autoPersistenceModel = new AutoPersistenceModel();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel.Conventions.Add<ClassMappingConvention>();

            

            NHibernateSession.Init(new WebSessionStorage(app),
  System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';'),
                   autoPersistenceModel, DataSourceFactory.GetDataSource(DataCategory.Core)
                );

            AutoPersistenceModel autoPersistenceModel2 = new AutoPersistenceModel();
            autoPersistenceModel2.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel2.Conventions.Add<HasManyConvention>();
            autoPersistenceModel2.Conventions.Add<ReferencesConvention>();


            NHibernateSession.Init(new SimpleSessionStorage(),
  System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"].Split(';'),
                   autoPersistenceModel2, DataSourceFactory.GetDataSource(DataCategory.Shared)
                );
            AutoPersistenceModel autoPersistenceModel3 = new AutoPersistenceModel();
            autoPersistenceModel3.Conventions.Add<ClassMappingConvention>();
            autoPersistenceModel3.Conventions.Add<HasManyConvention>();
            autoPersistenceModel3.Conventions.Add<ReferencesConvention>();


            NHibernateSession.Init(new SimpleSessionStorage(),
  System.Configuration.ConfigurationManager.AppSettings["AuxilliaryMappingAssembly"].Split(';'),
                   autoPersistenceModel2, DataSourceFactory.GetDataSource(DataCategory.Auxilliary)
                );
          



            //NHibernateSession.Init(new SimpleSessionStorage(),
            //  new[] { @"C:\AppZone\CloudDlls\Messiah.DataAccess.NHibernate.SharedDB.dll" },
            //     autoPersistenceModel, DataSourceFactory.GetDataSource(DataCategory.Shared)
            //  );
        }
    }
}
