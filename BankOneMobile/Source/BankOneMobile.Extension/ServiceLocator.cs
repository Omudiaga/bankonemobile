﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using CuttingEdge.ServiceLocation;

namespace BankOneMobile.Extension
{
    public class ServiceLocator : AppZoneSwitch.Extension.ServiceLocator
    {
        public ServiceLocator()
            : base()
        {
            this.RegisterSingle<IDbContext>(BankOneMobile.Data.NHibernate.DbContext.Instance);
            this.RegisterSingle<ILinkingBankAccountRepository>(new BankOneMobile.Data.NHibernate.DAO.LinkingBankAccountRepository());
            this.RegisterSingle<ILinkingCardRepository>(new BankOneMobile.Data.NHibernate.DAO.LinkingCardRepository());
            this.RegisterSingle<IMobileAccountRepository>(new BankOneMobile.Data.NHibernate.DAO.MobileAccountRepository());
            this.RegisterSingle<IHostRepository>(new BankOneMobile.Data.NHibernate.DAO.HostRepository());
            this.RegisterSingle<IAgentRepository>(new BankOneMobile.Data.NHibernate.DAO.AgentRepository());
            this.RegisterSingle<ISessionRepository>(new BankOneMobile.Data.NHibernate.DAO.SessionRepository());
            this.RegisterSingle<IBalanceInquiryTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.BalanceInquiryTransactionTypeRepository());
            this.RegisterSingle<IBillsPaymentTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.BillsPaymentTransactionTypeRepository());
            this.RegisterSingle<ICashInTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.CashInTransactionTypeRepository());
            this.RegisterSingle<ICashOutTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.CashOutTransactionTypeRepository());
            this.RegisterSingle<IPinChangeTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.PinChangeTransactionTypeRepository());
            this.RegisterSingle<IConfigurationTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.ConfigurationTransactionTypeRepository());
            this.RegisterSingle<IPinResetTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.PinResetTransactionTypeRepository());
            this.RegisterSingle<IRechargeTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.RechargeTransactionTypeRepository());
            this.RegisterSingle<ICommercialBankFundsTransferTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.CommercialBankFundsTransferTransactionTypeRepository());
            this.RegisterSingle<ILocalFundsTransferTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.LocalFundsTransferTransactionTypeRepository());
            this.RegisterSingle<IFundsTransferCashInTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.FundsTransferCashInTransactionTypeRepository());
            this.RegisterSingle<IFundsTransferCashOutTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.FundsTransferCashOutTransactionTypeRepository());
            this.RegisterSingle<IMiniStatementTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.MiniStatementTransactionTypeRepository());
            this.RegisterSingle<IRegistrationTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.RegistrationTransactionTypeRepository());
            this.RegisterSingle<ITransactionRepository>(new BankOneMobile.Data.NHibernate.DAO.TransactionRepository());
            this.RegisterSingle<IFlowRepository>(new BankOneMobile.Data.NHibernate.DAO.FlowRepository());
            this.RegisterSingle<IProductRepository>(new BankOneMobile.Data.NHibernate.DAO.ProductRepository());
            this.RegisterSingle<ISystemConfigRepository>(new BankOneMobile.Data.NHibernate.DAO.SystemConfigRepository());
            this.RegisterSingle<IKeyRepository>(new BankOneMobile.Data.NHibernate.DAO.KeyRepository());
            this.RegisterSingle<IMerchantRepository>(new BankOneMobile.Data.NHibernate.DAO.MerchantRepository());
            this.RegisterSingle<ICommercialBankRepository>(new BankOneMobile.Data.NHibernate.DAO.CommercialBankRepository());
            this.RegisterSingle<IInstitutionRepository>(new BankOneMobile.Data.NHibernate.DAO.InstitutionRepository());
            this.RegisterSingle<ITokenRepository>(new BankOneMobile.Data.NHibernate.DAO.TokenRepository());
            this.RegisterSingle<ICustomerCreationLogRepository>(new BankOneMobile.Data.NHibernate.DAO.CustomerCreationLogRepository());
            this.RegisterSingle<ITargetSavingsDepositTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.TargetSavingsDepositTransactionTypeRepository());
            this.RegisterSingle<ITargetSavingsRegistrationTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.TargetSavingsRegistrationTransactionTypeRepository());
            this.RegisterSingle<ILoanRequestTransactionTypeRepository>(new BankOneMobile.Data.NHibernate.DAO.LoanRequestTransactionTypeRepository());

            this.RegisterSingle<IUssdBillingConfigurationRepository>(new BankOneMobile.Data.NHibernate.DAO.UssdBillingConfigurationRepository());
            this.RegisterSingle<IMobileRequestRepository>(new BankOneMobile.Data.NHibernate.DAO.MobileRequestRepository());

            //Special register for Injection of Data into Core for checking 
            //account type(nuban/standard) for each institution
            this.RegisterSingle<BankOneMobile.Core.Helpers.IDataInjectionHelper>(new BankOneMobile.Data.NHibernate.DAO.DataInjectionHelper());
         
        }
    }
}
