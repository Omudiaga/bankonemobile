﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class InstitutionMap : EntityWithEnableDisableMap<Institution>
    {
        public InstitutionMap()
        {
           Map(x=>x.Code);
           Map(x=>x.ShortName);
           Map(x => x.Name);
           Map(x => x.AllowInterMFBTrans);
           Map(x => x.TokenLifeSpan);
           References<Flow>(x => x.TheAgentStartFlow, "AgentFlowID");
           References<Flow>(x => x.TheCustomerStartFlow, "CustomerFlowID");
           Map(x => x.TheAccountNumberType);
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
