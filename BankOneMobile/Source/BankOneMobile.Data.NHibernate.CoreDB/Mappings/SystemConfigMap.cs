﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class SystemConfigMap : EntityWithEnableDisableMap<SystemConfig>
    {
        public SystemConfigMap()
        {
            Map(x => x.MaximumPinTries);
            Map(x => x.PEK);
            Map(x => x.PVK);
            Map(x => x.TMK);
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
