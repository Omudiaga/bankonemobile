﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class TargetSavingsDepositTransactionTypeMap : EntityWithEnableDisableMap<TargetSavingsDepositTransactionType>
    {
        public TargetSavingsDepositTransactionTypeMap()
        {
            //Table("CashInTransactionTypes");
            Map(x => x.AgentCode);
            Map(x => x.Amount);
            Map(X => X.BetaAccountNumber);
            Map(X => X.TargetSavingsAccountNumber);
            
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID").Cascade.None();
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
