﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class UssdBillingConfigurationMap : EntityMap<UssdBillingConfiguration>
    {
        public UssdBillingConfigurationMap()
        {
            Table("UssdBillingConfigurations");
            Map(x => x.Period);
            Map(x => x.Amount);
            Map(x => x.Name);

        }
    }
}
