﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CommercialBankMap : EntityWithEnableDisableMap<CommercialBank>
    {
        public CommercialBankMap()
        {
           Map(x=>x.Code);
           Map(x => x.Name);
           Map(x => x.ShortName);
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
