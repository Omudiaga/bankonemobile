﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class TokenMap : EntityWithEnableDisableMap<Token>
    {
        public TokenMap()
        {
           Map(x=>x.TokenValue);
           Map(x=>x.PhoneNumber);
           Map(x => x.IsConsumed);
           Map(x => x.DateGenerated);
           Map(x => x.DateConsumed);
           References<Institution>(x => x.TheInstitution);
           References<MobileAccount>(x => x.TheMobileAccount);

            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
