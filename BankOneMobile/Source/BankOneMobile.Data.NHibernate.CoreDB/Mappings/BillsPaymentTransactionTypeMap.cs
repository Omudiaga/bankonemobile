﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class BillsPaymentTransactionTypeMap : EntityWithEnableDisableMap<BillsPaymentTransactionType>
    {
        public BillsPaymentTransactionTypeMap()
        {
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
            Map(x => x.MerchantName);
            Map(x => x.MerchantCode);
            Map(x => x.PaymentItemCode);
            Map(x => x.PaymentItemName);
           // References<Merchant>(x => x.TheMerchant);
           // References<PaymentItem>(x => x.ThePaymentItem);
            Map(x => x.GoodsDescription);
            Map(x => x.TransactionReference);
            Map(x => x.PaymentReference);
            Map(x => x.CoreBankingSTAN);
            Map(x => x.ISWSTAN);
            Map(x => x.Amount);
            Map(x => x.CustomerID);
            References<LinkingBankAccount>(x => x.TheBankAccount);
        }
    }
}
