﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class LoanRequestTransactionTypeMap : EntityWithEnableDisableMap<LoanRequestTransactionType>
    {
        public LoanRequestTransactionTypeMap()
        {
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            References<FeeTransactionType>(x => x.TheFee);
            Map(x => x.AccountNumber);
            
            Map(x => x.BetaFriendPerception);
            Map(x => x.Date);


            
          //  Map(x => x.DateOfBirth);
            Map(x => x.InstitutionCode);
            Map(x => x.LoanAmount);
            Map(x => x.LoanProductID);
            Map(x => x.PhoneNumber);
            Map(x => x.Tenure);
            
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
