﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class MerchantMap : EntityWithEnableDisableMap<Merchant>
    {
        public MerchantMap()
        {
           Map(x=>x.Code);
           
           Map(x => x.Name);

           HasMany<PaymentItem>(x => x.ThePaymentItems);
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
