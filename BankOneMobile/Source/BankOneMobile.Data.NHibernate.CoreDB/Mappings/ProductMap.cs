﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class ProductMap : EntityWithEnableDisableMap<Product>
    {
        public ProductMap()
        {
           Map(x=>x.Code);
           Map(x=>x.InstitutionCode);
           Map(x => x.Name);
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
