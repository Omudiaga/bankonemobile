﻿using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.NHibernate.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CustomerCreationLogMap:EntityMap<CustomerCreationLog>
    {
        public CustomerCreationLogMap()
        {
            Map(x => x.CustomerName);
            Map(x => x.PhoneNumber);
            Map(x => x.State);
            Map(x => x.Institution);
            Map(x => x.AccountNumber);

        }
    }
}
