﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class LinkingCardMap : EntityMap<LinkingCard>
    {
        public LinkingCardMap()
        {
            Map(x => x.CardPAN);
            Map(x => x.ExpiryDate);
            References<MobileAccount>(x => x.TheMobileAccount);
            Map(x => x.InstitutionCode);

        }
    }
}
