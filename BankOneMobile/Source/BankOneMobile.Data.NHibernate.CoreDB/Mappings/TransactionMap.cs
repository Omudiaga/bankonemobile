﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class TransactionMap : EntityWithEnableDisableMap<Transaction>
    {
        public TransactionMap()
        {
            Table("MobileTransactions");
            Map(x => x.Amount).Not.LazyLoad();
            Map(x => x.From, "FromValue").Not.LazyLoad();
            Map(x => x.To, "ToValue").Not.LazyLoad();
            Map(x => x.Status).Not.LazyLoad();
            Map(x => x.Narration).Not.LazyLoad();
            Map(x => x.FromPhoneNumber).Not.LazyLoad();
            Map(x => x.TransactionTypeID).Not.LazyLoad();
            Map(x => x.TransactionTypeName).Not.LazyLoad();
            Map(x => x.Date).Not.LazyLoad();
            Map(x => x.StatusDetails).Not.LazyLoad();
            Map(x => x.FromInstitutionCode).Not.LazyLoad();
            Map(x => x.STAN).Not.LazyLoad();
            
            References<Session>(x => x.Session,"SessionID").Cascade.All().Not.LazyLoad();
            //References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID").Cascade.All();
            
          
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
