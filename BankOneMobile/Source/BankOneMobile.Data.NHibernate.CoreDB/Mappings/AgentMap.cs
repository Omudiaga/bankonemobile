﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class AgentMap : EntityWithEnableDisableMap<Agent>
    {
        public AgentMap()
        {
            Map(x => x.LastName);
            Map(x => x.InstitutionCode);
            Map(x => x.OtherNames);
            Map(x => x.PhoneNumber);
            Map(x => x.Code);
            Map(x => x.AddressLocation);
            Map(x => x.Linked);
            Map(x => x.AccountNumber);
            Map(x => x.IsMobileTeller);
            Map(x => x.PersonalPhoneNumber);
            Map(x => x.PostingLimit);
            Map(x => x.BranchID);
       
            References<MobileAccount>(x=>x.MobileAccount,"MobileAccountID").Cascade.All().Not.LazyLoad();
            References<LinkingBankAccount>(x => x.TheAgentAccount,"AgentAccountID").Cascade.All().Not.LazyLoad();
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
