﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CommercialBankFundsTransferTransactionTypeMap : EntityWithEnableDisableMap<CommercialBankFundsTransferTransactionType>
    {
        public CommercialBankFundsTransferTransactionTypeMap()
        {
            Map(x => x.AgentCode);
            
            Map(x => x.Token);
            Map(x => x.AccountNumber);
            Map(x => x.BankCode);
            Map(x => x.BankName);
            Map(x => x.BeneficiaryLastName);
            Map(x => x.BeneficiaryOtherNames);
            Map(x => x.PaymentReference);
            Map(x => x.TransactionReference);
            Map(x => x.ISWSTAN);
            Map(x => x.CoreBankingSTAN);
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID").Not.LazyLoad();
            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All().Not.LazyLoad();
            References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID");
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
