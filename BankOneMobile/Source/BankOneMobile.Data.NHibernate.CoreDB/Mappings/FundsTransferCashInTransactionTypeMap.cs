﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class FundsTransferCashInTransactionTypeMap : EntityWithEnableDisableMap<FundsTransferCashInTransactionType>
    {
        public FundsTransferCashInTransactionTypeMap()
        {
            Map(x => x.AgentCode);
            Map(x => x.Token);
            Map(x => x.BeneficiaryPhoneNumber);
            Map(x => x.DepositorPhoneNumber);
            Map(x => x.DepositorAccountNumber);
            Map(x => x.IsCash);
            Map(x => x.IsConsumed);
            Map(x => x.Amount);
            Map(x => x.FromSelfService);
            Map(x => x.DepositorName);

            References<FundsTransferCashOutTransactionType>(x => x.TheCashOut,"CashOutID");

            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
