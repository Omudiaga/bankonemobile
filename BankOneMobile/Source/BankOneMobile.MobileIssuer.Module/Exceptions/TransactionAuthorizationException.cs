﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.MobileIssuer.Module.Exceptions
{
    public class TransactionAuthorizationException : ApplicationException
    {
        public TransactionAuthorizationException(string msg)
            : base(msg)
        {
        }
    }
}
