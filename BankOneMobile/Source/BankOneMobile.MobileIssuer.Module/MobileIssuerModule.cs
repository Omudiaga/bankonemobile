﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneSwitch.Services.Contracts;

using System.ServiceModel;
using BankOneMobile.MobileIssuer.Module.Exceptions;
using System.Diagnostics;
using AppZoneSwitch.Services.Utility;
using BankOneMobile.MobileIssuer.Module.Enums;



namespace BankOneMobile.MobileIssuer.Module
{
    public class MobileIssuerModule:IAuthorizationService
    {
       
       System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
       //private static string _Url = "http://10.140.111.4:9090/WebServices/MobileIssuerServices/AuthorizationService.svc";
       //private static string _Url = "http://62.173.32.45:9001/WebServices/MobileIssuerServices/AuthorizationService.svc";
       private static string _Url = "http://10.140.111.6:9090/WebServices/MobileIssuerServices/AuthorizationService.svc";
       private static BasicHttpBinding binding = new BasicHttpBinding();
       private static EndpointAddress endpoint = new EndpointAddress(_Url);
       

     // static  Authorization.AuthorizationServiceClient client = new Authorization.AuthorizationServiceClient();
       static AuthorizationService.AuthorizationServiceClient client = new AuthorizationService.AuthorizationServiceClient(binding,endpoint);
        
           Trx.Messaging.Iso8583.Iso8583Message IAuthorizationService.Authorize(AppZoneSwitch.Core.Contracts.IScheme theScheme, Trx.Messaging.Iso8583.Iso8583Message theIsoMessage)
          //Trx.Messaging.Iso8583.Iso8583Message Authorize(AppZoneSwitch.Core.Contracts.IScheme theScheme, Trx.Messaging.Iso8583.Iso8583Message theIsoMessage)
        {
            Trace.TraceInformation("I'm in the mobile Issuer");
            Trx.Messaging.Iso8583.Iso8583Message toReturn = new Trx.Messaging.Iso8583.Iso8583Message();
            try
            {
                toReturn= InternalAuthorize(theScheme, theIsoMessage);
            }
            catch(Exception ex)
            {
                Trace.TraceInformation("Main: {0}, Inner :{1}",ex.Message,ex.InnerException==null?"None":ex.InnerException.Message);
                throw;
            }

            return toReturn;
        }



           public static Trx.Messaging.Iso8583.Iso8583Message InternalAuthorize(AppZoneSwitch.Core.Contracts.IScheme theScheme, Trx.Messaging.Iso8583.Iso8583Message theIsoMessage)
           {
               string stan = theIsoMessage.Fields[11].Value.ToString();
               # region Authorization
               bool verifyPin = true; ServiceCodes codes = ServiceCodes.StandardTransaction;

               if (theIsoMessage == null)
               {
                   Trace.TraceInformation("Null ISO Message");

                   throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);

               }
               Trace.TraceInformation("Field 48 is {0}", theIsoMessage.Fields[48] != null ? theIsoMessage.Fields[48].Value.ToString() : "NOthing");

               if (theIsoMessage.Fields[48] != null && theIsoMessage.Fields[48].Value.ToString() == "01")
               {
                   codes = ServiceCodes.SenderNoReciever;

               }

               else if (theIsoMessage.Fields[48] != null && theIsoMessage.Fields[48].Value.ToString() == "02")
               {
                   codes = ServiceCodes.RecieverNoSender;
               }
               Trace.TraceInformation("Codes is {0}", codes.ToString());

               if (codes != ServiceCodes.RecieverNoSender && theIsoMessage.Fields[102] == null || theIsoMessage.Fields[102].Value == null)
               {
                   Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "102");
                   throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);
               }
               //Trace.TraceInformation("I tried to check field 103");
               ////if (codes != ServiceCodes.SenderNoReciever && theIsoMessage.Fields[103] == null || theIsoMessage.Fields[103].Value == null)
               ////{

               ////   // theIsoMessage.Fields.Add(103,theIsoMessage.Fields[102].Value.ToString());
               ////   // Trace.TraceInformation("This is where I added Filed 103");
               ////}
               //Trace.TraceInformation("I actually  checked field 103");
               if (theIsoMessage.Fields[2] == null || theIsoMessage.Fields[2].Value == null)
               {
                   Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "2");
                   throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.InvalidCardNumber);
               }
               if (theIsoMessage.Fields[3] == null || theIsoMessage.Fields[3].Value == null)
               {
                   Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "3");
                   throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.TransactionNotPermitted);
               }
               string transactionType = theIsoMessage.Fields[3].Value.ToString();
               byte[] encryptedPinBytes = new byte[8];
               string pinBlock = string.Empty;
               if (theIsoMessage.Fields[52] == null || theIsoMessage.Fields[52].Value == null)
               {
                   verifyPin = false;
               }
               else
               {
                   encryptedPinBytes = (byte[])theIsoMessage.Fields[52].Value;
                   ThalesSim.Core.Utility.ByteArrayToHexString(encryptedPinBytes, ref pinBlock);
               }


               string from = theIsoMessage.Fields[102].Value != null ? theIsoMessage.Fields[102].Value.ToString() : String.Empty;
               Trace.TraceInformation("I actually got to setting the to value");
               string to = (theIsoMessage.Fields[103] != null && theIsoMessage.Fields[103].Value != null) ? theIsoMessage.Fields[103].Value.ToString() : string.Empty;
               Trace.TraceInformation("I actually got to setting the to value");
               string phoneNumber = theIsoMessage.Fields[2].Value.ToString();
               bool isPhoneNumber = CheckPhoneNumber(from);
               bool receiverIsPhoneNumber = CheckPhoneNumber(to);
               bool allowInactiveAcct = false;

               var institutionCode = theIsoMessage.Fields.Contains(FieldNos.F100_ReceivingInstitutionID) ?
                   (theIsoMessage.Fields[FieldNos.F100_ReceivingInstitutionID] != null ? theIsoMessage.Fields[FieldNos.F100_ReceivingInstitutionID].Value.ToString() : string.Empty)
                   : string.Empty;

               //new MobileIssuerModule().encoding.GetString(encryptedPinBytes);

               string confimredAccountNo = string.Empty;
               string confirmedSenderAcctNo = string.Empty;
               Trace.TraceInformation("About to call the service");
               string response = string.Empty;
               if (codes == ServiceCodes.StandardTransaction)
               {
                   Trace.TraceInformation("About to call the Validate Mobile Account Verification service 1-{0}", stan);
                   response = client.ValidateMobileAccount(out confirmedSenderAcctNo, phoneNumber, institutionCode, from, pinBlock, isPhoneNumber, verifyPin);
                   Trace.TraceInformation("Completed Mobile Account Verification Service Call -{0}", stan);
                   if (response.StartsWith("00") && transactionType == "500000")// validate the reciever for payments
                   {
                       //Trace.TraceInformation("This is where Crash Used to be");
                       Trace.TraceInformation(confimredAccountNo + ";" + to + ";" + receiverIsPhoneNumber.ToString() + " " + stan);

                       //response = client.ValidateBankAccount(out confimredAccountNo, to, receiverIsPhoneNumber);
                       //if (response.StartsWith("E06"))
                       //{
                       //    allowInactiveAcct = true;//payments can be made to inactive accts
                       //}
                   }
                   Trace.TraceInformation("About to call the service 3");

               }
               else if (codes == ServiceCodes.RecieverNoSender)
               {
                   //response = client.ValidateBankAccount(out confirmedSenderAcctNo, phoneNumber, from, pinBlock, isPhoneNumber, verifyPin);
                   Trace.TraceInformation("About to call the Validate Bank Account Verification service 1-{0}", stan);
                   response = client.ValidateBankAccount(out confimredAccountNo, to, institutionCode, receiverIsPhoneNumber);
                   Trace.TraceInformation("Completed Bank Account Verification Service Call -{0}", stan);
                   if (response.StartsWith("E06"))
                   {
                       allowInactiveAcct = true;
                   }
               }
               else if (codes == ServiceCodes.SenderNoReciever)
               {
                   response = client.ValidateBankAccount(out confimredAccountNo, from, institutionCode, isPhoneNumber);
               }
               Trace.TraceInformation("My response was {0} -{1}", response, stan);

               if (!response.StartsWith("00"))
               {
                   if (response.StartsWith("E200"))
                   {
                       Trace.TraceInformation("About to throw System Malfunction -{0}.But my full response was-{1}", stan, response);
                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.SystemMalfunction);
                   }
                   else if (response.StartsWith("E201"))
                   {
                       Trace.TraceInformation("About to throw System Malfunction -{0}.But my full response was-{1}", stan, response);
                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.SystemMalfunction);
                   }
                   else if (response.StartsWith("E08"))
                   {
                       //client.UpdatePinTries(phoneNumber, false);
                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.IncorrectPin);
                   }
                   else if (response.StartsWith("E07"))
                   {

                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.PinTriesExceeded);
                   }
                   else if (response.StartsWith("E01"))
                   {
                       Trace.TraceInformation("About to throw Invalid Card Number -{0}.But my full response was-{1}", stan, response);
                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.InvalidCardNumber);
                   }

                   else if (response.StartsWith("E02"))
                   {

                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.NoCheckAccount);
                   }
                   else if (response.StartsWith("E06"))
                   {
                       if (transactionType != "920000" && !allowInactiveAcct)// change pin
                       {
                           throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.InvalidCardNumber);
                       }

                   }
                   else if (response.StartsWith("E04"))
                   {

                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.NoSavingsAccount);
                   }


               }


               //else if (!servicePayment&& !new MobileIssuerModule().ValidateReceiverAcctNo(to, out confimredAccountNo, receiverIsPhoneNumber))//TODO call the right web servcice method to validate sender details
               //{
               //    throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.InvalidCardNumber);
               //}

               else if (receiverIsPhoneNumber)
               {
                   //theIsoMessage.Fields.Remove(103);
                   theIsoMessage.Fields.Add(102, confimredAccountNo);

               }
               if (isPhoneNumber)
               {
                   // theIsoMessage.Fields.Remove(102);
                   theIsoMessage.Fields.Add(102, confirmedSenderAcctNo);
               }

               # endregion

               # region Do Transaction
               Trace.TraceInformation("About to Process Transaction if it will be necessary-{0}", stan);

               # region PinChange
               if (transactionType == "920000")
               {
                   Trace.TraceInformation("I am Change Pin");
                   if (theIsoMessage.Fields[53] == null || theIsoMessage.Fields[53].Value == null)
                   {
                       Trace.TraceInformation("Pin change must have a value inn Field 53");
                       throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);
                   }

                   byte[] newBytes = (byte[])theIsoMessage.Fields[53].Value;
                   StringBuilder fromIso = new StringBuilder();
                   foreach (byte bt in newBytes)
                   {
                       fromIso.AppendFormat(":{0}", bt.ToString());
                   }
                   Trace.TraceInformation("ISO Bytes is {0}", fromIso.ToString());
                   Trace.TraceInformation("casted teh value");
                   byte[] usefulBytes = new byte[8];
                   for (int i = 1; i <= 8; i++)
                   {
                       usefulBytes[i - 1] = newBytes[i];
                   }
                   StringBuilder fromUsefulBytes = new StringBuilder();
                   foreach (byte bt in usefulBytes)
                   {
                       fromUsefulBytes.AppendFormat(":{0}", bt.ToString());
                   }


                   string newPinBlock = string.Empty;
                   ThalesSim.Core.Utility.ByteArrayToHexString(usefulBytes, ref newPinBlock);
                   StringBuilder builder = new StringBuilder();
                   foreach (byte bt in usefulBytes)
                   {
                       builder.AppendFormat(";{0}", bt.ToString());
                   }
                   Trace.TraceInformation("Converted back to hex:{0}", builder.ToString());
                   Trace.TraceInformation("Converted to Hex:{0}", newPinBlock);
                   try
                   {

                       bool changed = client.ChangePin(phoneNumber, newPinBlock);
                       if (changed)
                       {
                           throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.Approved);
                       }
                       else
                       {
                           throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.Error);
                       }
                   }
                   catch (Exception ex)
                   {
                       Trace.TraceInformation("Error in Pin Change : {0} Inner: {1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                       throw;
                   }

               }
               # endregion

               # region Set Default Account
               if (transactionType == "990000")
               {
                   Trace.TraceInformation("I am Set Default Account");



                   try
                   {

                       bool set = client.SetRecievingAccount(phoneNumber, from);

                       if (set)
                       {
                           throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.Approved);
                       }
                       else
                       {
                           throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.Error);
                       }
                   }
                   catch (Exception ex)
                   {
                       Trace.TraceInformation("Error in Set Default Account : {0} Inner: {1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                       throw;
                   }

               }
               # endregion

               # region  Account Enquiry
               if (transactionType == "390000")
               {
                   Trace.TraceInformation("I am Account Enquiry");

                   throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.Approved);



               }
               # endregion


               //else
               //{
               //    Trace.TraceInformation("The Transaction Type is {0}",transactionType);
               //}

               # endregion
               Trace.TraceInformation("Just About To Return Back To The Switch {0}", stan);
               return theIsoMessage;

           }
    
         public  static  string  ValidateMobileAccount(string phoneNumber, string institutionCode, string accountNumber, string encryptedPinBlock,bool isPhoneNumber,bool verifyPin,out string  confirmedAcctNo)
        {

            string val = string.Empty;

            val = client.ValidateMobileAccount(out confirmedAcctNo,phoneNumber, institutionCode, accountNumber, encryptedPinBlock, isPhoneNumber,verifyPin);
           
            return val;
        }

        public bool UpdatePinTries(string phoneNumber, bool isReset)
        {
            
            return client.UpdatePinTries(phoneNumber, isReset);

        }
        // public bool ValidateReceiverAcctNo(string identifier,out string acctNo,bool isPhoneNumber)
        //{
            
        //    bool toReturn = false;
           
               

        //  //toReturn =   client.ValidateBankAccount(out acctNo, identifier, isPhoneNumber);
            
        //    return toReturn;
        //}

        public bool ChangePin(Trx.Messaging.Iso8583.Iso8583Message theIsoMessage)
        {

            string newPinBlock = theIsoMessage.Fields[53].Value.ToString();
            string phoneNumber = theIsoMessage.Fields[2].Value.ToString();

            client.ChangePin(phoneNumber, newPinBlock);
            return true;
        }

        public static bool CheckPhoneNumber(string entry)
        {
            string validEntries = "080;070;081;071;090;091";
            string[] checkEntries = validEntries.Split(";".ToCharArray());
            bool toReturn = true;
            long digits = 0;

            foreach (string ss in checkEntries)
            {
                if (entry.StartsWith(ss))
                {
                    toReturn = true;
                    break;
                }

                toReturn = false;

            }
            if (entry.Length != 11)
            {
                toReturn = false;
            }
            if (!Int64.TryParse(entry, out  digits))
            {
                toReturn = false;
            }
            return toReturn;
        }       
    }
}
