﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
   public class LinkingBankAccountModel
    {
       public virtual string OtherNames { set; get; }
       public virtual string LastName { set; get; }
       public virtual string MobileNumber { set; get; }
       public virtual string IsActive { set; get; }
       public virtual string CustomerID { get; set; }
       public virtual long ID { get; set; }
       public virtual string gender { get; set; }
       public virtual string RegistrarCode { get; set; }
       public virtual string isAgentAccount { get; set; }
       public virtual string productCode { get; set; }
       public virtual string accountNumber { get; set; }
       
       public virtual List<ILinkingBankAccount> TheAccounts { set; get; }
       public virtual List<ILinkingBankAccount> SelectedAccounts { set; get; }
       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }
       public virtual IMobileAccount MobileAccount { get; set; }
       public virtual byte[] Signature { get; set; }



       public string  InstitutionCode { get; set; }
    }
}
