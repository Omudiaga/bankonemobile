﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
   public class MobileAccountModel
    {
       public virtual string OtherNames { set; get; }
       public virtual string LastName { set; get; }
       public virtual string MobileNumber { set; get; }
       public virtual string IsActive { set; get; }
       public virtual string CustomerID { get; set; }
       public virtual long ID { get; set; }
       public virtual string gender { get; set; }
       
       public virtual List<IMobileAccount> MobileAccounts { set; get; }
       public virtual List<IMobileAccount> SelectedMobileAccounts { set; get; }
       public virtual List<ILinkingBankAccount> Accounts { set; get; }
       public virtual List<ILinkingBankAccount> SelectedAccounts { set; get; }
       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }
       public virtual IMobileAccount MobileAccount { get; set; }
       public virtual string  PersonalPhoneNumber { get; set; }
       public virtual string IsAgentAccount { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string OpenedBy { get; set; }
       public virtual long? RegistrarID { get; set; }
       public virtual string  RegistrarCode { get; set; }
       public virtual string TheGender { get; set; }
       public virtual bool ActivateViaJava { get; set; }
       public virtual bool ActivateViaUSSD { get; set; }

       public string OldPIN { get; set; }
       public string NewPIN1 { get; set; }
       public string NewPIN2 { get; set; }
       
    }
}
