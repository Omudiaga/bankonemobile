﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.UI.Models
{
   public  class AccountTypeModel
    {
       public AccountType TheAccountType { set; get; }
       public string NextUrl { set; get; }
    }

    public enum AccountType
    {
        AcccountHolder,
        NonAccountHolder

    }
}
