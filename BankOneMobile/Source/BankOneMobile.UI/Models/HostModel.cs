﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
   public class HostModel
    {
       public virtual string Name { set; get; }
       public virtual string IsActive { set; get; }
       public virtual List<IHost> Hosts { set; get; }
    }
}
