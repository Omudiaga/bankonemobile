﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.Models
{
   public class AgentModel
    {
       public virtual string LastName { set; get; }
       public virtual string OtherNames { set; get; }
       public virtual string PhoneNumber { set; get; }
       public virtual string PersonalPhoneNumber { set; get; }
       public virtual string Code { set; get; }
       public virtual string IsActive { set; get; }
       public virtual string CustomerID { get; set; }
       public virtual string ReturnURL { get; set; }
       public virtual string DisplayMessage { get; set; }
       public virtual string AccountNumber { get; set; }
       public virtual string AddressLocation { get; set;}
       public virtual string InstitutionCode { get; set; }
       public virtual List<IAgent> Agents { set; get; }
       public virtual ILinkingBankAccount TheBankAccount  {get;set;}
       public virtual Agent TheAgent { get; set; }
       public virtual bool? IsMobileTeller { get; set; }
       public virtual bool ActivateViaJava { get; set; }
       public virtual bool ActivateViaUSSD { get; set; }
       public virtual decimal PostingLimit { get; set; }
       public virtual BankOneMobile.Services.BranchServiceRef.Branch TheBranch { get; set; }
       
    }

    public class InstitutionModel
    {
        public string DisplayMessage { set; get; }

        public string Code { set; get; }
        public string Name { set; get; }
        public string ShortName { set; get; }
        public string AccountNumberType { set; get; }
        public IFlow AgentFlow { set; get; }
        public IFlow CustomerFlow { set; get; }

        public Institution TheInstitution { set; get; }
    }

    public class InstitutionsListModel
    {
        public string Code { set; get; }
        public string Name { set; get; }

        public List<IInstitution> Institutions { set; get; }
    }

   public class MobileTellerModel
   {
       public virtual string LastName { set; get; }
       public virtual string OtherNames { set; get; }
       public virtual string PhoneNumber { set; get; }
       public virtual string Code { set; get; }
       public virtual string IsActive { set; get; }
       public virtual string CustomerID { get; set; }
       public virtual string ReturnURL { get; set; }
       public virtual string DisplayMessage { get; set; }
       public virtual string AccountNumber { get; set; }
       public virtual string AddressLocation { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual List<IAgent> Agents { set; get; }
       public virtual ILinkingBankAccount TheBankAccount { get; set; }
       public virtual Agent TheAgent { get; set; }
       public virtual bool ActivateViaJava { get; set; }

   }

}
