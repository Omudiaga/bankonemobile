﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Utility;
using BankOneMobile.CustomerInfo.Interface;
using System.Reflection;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.UI.UI.TransactionUI;
using BankOneMobile.UI.UI.AgentUI;

namespace BankOneMobile.UI.Logic
{
    public class MobileAccountLogic
    {
      //static   IDataSource _source = DataSourceFactory.GetDataSource(DataCategory.Shared);
        public static MobileAccount SaveMobileAccount(MobileAccount theMobileAccount)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                return new MobileAccountSystem(source).SaveMobileAccount(theMobileAccount) as MobileAccount;
            }
        }

        public static MobileAccount SaveMobileAccount(MobileAccount theMobileAccount, IHost theHost)
        {
            BankOneMobile.CustomerInfo.Interface.ICustomerInfo customer = null;
            string customerId = string.Empty;
            string accountNumber = string.Empty;
            if (theHost != null)
            {
                string filePath = System.Configuration.ConfigurationManager.AppSettings["CustomerDatasource"] + "\\" + theHost.DllPath;
                Assembly theAssembly = Assembly.LoadFile(filePath);
                Type t = theAssembly.GetTypes().Where(x => x.AssemblyQualifiedName == theHost.ClassType).FirstOrDefault();

                if (t != null)
                {
                    customer = Activator.CreateInstance(t) as BankOneMobile.CustomerInfo.Interface.ICustomerInfo;
                    if (customer != null)
                    {
                        //customer.DOB = theMobileAccount.DOB;
                        customer.LastName = theMobileAccount.LastName;
                        //customer.EmailAddress = theMobileAccount.EmailAddress;
                        // customer.Address = theMobileAccount.Address;
                        customer.OtherNames = theMobileAccount.OtherNames;
                        // customer.Sex = theMobileAccount.Sex;
                        accountNumber = customer.CreateCustomerAccount(customer, out customerId);
                    }

                }

            }

            //theMobileAccount.CustomerID = customerId;
            theMobileAccount.BankAccounts = new List<ILinkingBankAccount>();
            theMobileAccount.BankAccounts.Add(new LinkingBankAccount { BankAccount = accountNumber, TheMobileAccount = theMobileAccount, TheHost = theHost });//, CustomerID= theMobileAccount.CustomerID });
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                return new MobileAccountSystem(source).SaveMobileAccount(theMobileAccount) as MobileAccount;
            }
        }

        public static MobileAccount UpdateMobileAccount(MobileAccount theMobileAccount)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                return new MobileAccountSystem(source).UpdateMobileAccount(theMobileAccount) as MobileAccount;
            }
        }

        public static MobileAccount GetMobileAccount(long id)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                MobileAccount m = new MobileAccountSystem(source).GetMobileAccount(id) as MobileAccount;
                return m;
            }
        }

        public static List<string> Statuslist()
        {
            List<string> lst = new List<string>();
            lst.Add("Active");
            lst.Add("InActive");
            lst.Add("PendingFundsTransfer");
            lst.Add("New");
            lst.Add("HotListed");
            lst.Add("AwaitingApproval");
            return lst;

        }
        public static List<string> BankAccountStatuslist()
        {
            List<string> lst = new List<string>();
            lst.Add("Active");
            lst.Add("InActive");       
            
            lst.Add("AwaitingApproval");
            return lst;

        }
        public static List<string> TransactionStatusList()
        {
            List<string> lst = new List<string>();
            lst.Add("Successful");
            lst.Add("Failed");
            lst.Add("Reversed");
            
            return lst;

        }
        public static Dictionary<string,string> TransactionTypeList()
        {
            
            Dictionary<string, string> dicts = new Dictionary<string,string>();
            dicts.Add("On Net Transfer", "LocalFundsTransfer");
            dicts.Add("Off Net Transfer", "FundsTransferCommercialBank");
            dicts.Add("Cash Deposit", "CashIn");
            dicts.Add("Cash Withdrawal ", "CashOut");
            dicts.Add("Registration","Registration");
            dicts.Add("Pin Change", "PINChange");
            dicts.Add("Recharge", "Recharge");
            dicts.Add("Bills Payment", "BillsPayment");
            dicts.Add("Mini Statement", "MiniStatement");
            dicts.Add("Balance Enquiry", "BalanceEnquiry");
            
            
            
            
            return dicts;

        }
        public static List<string> GenderList()
        {
            List<string> lst = new List<string>();
            lst.Add("Male");
            lst.Add("Female");
           
            return lst;

        }
        public static List<string> IsActive()
        {
            List<string> lst = new List<string>();
            lst.Add("True");
            lst.Add("False");
            return lst;

        }

        public static List<IMobileAccount> Search(string lastname, string othernames, string mobile, string status)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<IMobileAccount> mobileAccounts = new List<IMobileAccount>();

            if (!string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(lastname.Trim()))
            {
                dic.Add("LastName", lastname);

            }

            if (!string.IsNullOrEmpty(othernames) && !string.IsNullOrEmpty(othernames.Trim()))
            {
                dic.Add("OtherNames", othernames);

            }

            if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(mobile.Trim()))
            {
                dic.Add("MobilePhone", mobile);

            }

            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                bool isActive = Convert.ToBoolean(status);
                dic.Add("IsActive", isActive);

            }

            int total = 0;
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                mobileAccounts = new MobileAccountSystem(source).FindMobileAccounts(dic, string.Empty, 0, 0, out total);
            }
            return mobileAccounts;

        }

        internal static bool ValidateEntry(MobileAccount x)
        {
            return true;
        }

        public static List<string> GetAllInstitution()
        {
            List<IInstitution> institutions = InstitutionSystem.GetAllInstitution();
            List<string> institutionCodes = new List<string>();
            if (institutions != null && institutions.Count > 0)
            {
                institutionCodes = institutions.Select<IInstitution, string>(x => x.Code).ToList();

            }
            return institutionCodes;
        }

        public static string MaskValue(string value)
        {
            string maskValue = string.Empty;
            maskValue = Crypter.Mask(value);
            return maskValue;

        }

        public static   string AgentName(string agentCode)
        { 
            string toReturn = "Invalid Teller";
            try
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    Agent agent = new AgentSystem(source).GetByCode(agentCode) as Agent;
                    toReturn = string.Format("{0}, {1}", agent.LastName, agent.OtherNames);
                }
            }
            catch
            {

            }
            return toReturn;
        }
        public static string AgentPhoneNumber(string agentCode)
        {
            string toReturn = "Invalid Teller";
            try
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    Agent agent = new AgentSystem(source).GetByCode(agentCode) as Agent;
                    toReturn = agent.PhoneNumber;
                }

            }
            catch
            {

            }
            return toReturn;
        }


        public  static string  MobileAccountName(string phoneNumber, bool isFullNumber)
        {
            IMobileAccount mob = null;
            string toReturn = "Invalid Account";
            try
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    if (isFullNumber)
                    {
                        mob = new MobileAccountSystem(source).GetByStandardPhoneNumber(phoneNumber);
                    }
                    else
                    {
                        mob = new MobileAccountSystem(source).GetByPhoneNumber(phoneNumber);
                    }
                }
                toReturn = string.Format("{0},{1}",mob.LastName,mob.OtherNames);
            }
            catch
            {
            }
            return toReturn;
            
        }
        public  static string  InstitutionName(string instCode)
        {
            
            
            string toReturn = "Invalid Institution";
            try
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    toReturn = new InstitutionSystem().GetByCode(source, instCode).Name;
                }
            }
            catch
            {
            }
            return toReturn;

        }
        public string TransactionTypeDetailURL(TransactionTypeName typeName,long ID)
        {
            string toReturn = string.Empty;
            if (typeName == TransactionTypeName.CashIn)
            {
                toReturn = String.Format("CashInTansactionReport.aspx?");
            }
            return toReturn;
            

        }
        public static  Type TheTransactionType(TransactionTypeName typeName)
        {

            Type toReturn = typeof(AddAgentUI);
            if (typeName == TransactionTypeName.CashIn)
            {
                toReturn = typeof(CashInDetailUI);
            }
            return toReturn;
        }
        public static string GetFullName(string phoneNumber)
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                var mob = new MobileAccountSystem(source).GetMobileAccountByPhoneNumber(phoneNumber);
                return string.Format("{0} {1}", mob.LastName, mob.OtherNames);
            }
        }
    }
}
