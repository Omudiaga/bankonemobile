﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Utility;
using BankOneMobile.CustomerInfo.Interface;
using System.Reflection;
using System.Diagnostics;

namespace BankOneMobile.UI.Logic
{
    public class AgentLogic
    {
        public static Agent SaveAgent(Agent theAgent)
        {

            return new AgentSystem().Insert(theAgent) as Agent;

        }

        public static List<string> Statuslist()
        {
            List<string> lst = new List<string>();
            lst.Add("True");
            lst.Add("False");
            return lst;

        }

        public static Agent UpdateAgent(Agent theAgent)
        {

            return new  AgentSystem().Update(theAgent) as Agent;

        }
        public static Dictionary<string,string>  GetTellersByInstitution(string  isntCode)
        {
            Dictionary<string, string> toReturn = new Dictionary<string, string>();
            List<Agent> ags =  new AgentSystem().GetByInstitutionCode( isntCode,true);
            if (ags != null && ags.Count > 0)
            {
                foreach (Agent ag in ags)
                {
                    if (ag.Code != null&&!toReturn.ContainsKey(ag.Code))
                    {
                        toReturn.Add(ag.Code, string.Format("{0},{1}", ag.LastName, ag.OtherNames));
                    }
                    else if (toReturn.ContainsKey(ag.Code))
                    {
                        Trace.TraceInformation("Agent Code Already Exists- {0}", ag.Code);
                    }
                }
            }
            return toReturn;

        }
        
        
    }
}
