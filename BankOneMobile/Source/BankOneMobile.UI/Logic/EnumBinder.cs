﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.UI.Logic
{
   
    public class EnumBinder
    {
        public static List<T> GetEnumNames<T>(string enumStringType)
        {
            List<T> result = new List<T>();
            Type enumType = Type.GetType(enumStringType);
            foreach (int enumValue in Enum.GetValues(enumType).Cast<int>())
            {
                T data = (T)Enum.Parse(typeof(T), Enum.GetName(enumType, enumValue));
                result.Add(data);
            }
            return result;
        }
    }
}
