﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Utility;
using BankOneMobile.CustomerInfo.Interface;
using System.Reflection;
using System.Web;
using BankOneMobile.Core.Exceptions;
using PANE.Framework.Functions.DTO;
using System.Web.Security;
using System.Configuration;
using System.Diagnostics;

namespace BankOneMobile.UI.Logic
{
    public class UserLogic
    {
        public UserLogic()
        {
        }
        private const string SS_MFBCODE = "::SS_INSTITUTION_CODE::";
        private const string SS_MFBINSTID = "::SS_MFBINSTID::";
        public static string MFBCode
        {
           
            get
            {
                try
                {
                    if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                    {
                        HttpContext.Current.Session[SS_MFBCODE] = HttpContext.Current.User.Identity.Name.Split(':')[1];
                    }

                    return Convert.ToString(HttpContext.Current.Session[SS_MFBCODE]);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                    throw new SessionTimeOutException("Your Session may have timed out.Please click the log out link then login again");
                }
            }
            set
            {
                HttpContext.Current.Session[SS_MFBCODE] = value;
            }
        
        }

        public static  string GetLoggedOnUserInstCode()
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ManagedServicesTestMode"]) && Convert.ToBoolean(ConfigurationManager.AppSettings["MFBUserLoggedIn"]))
            {
                return "10011";
            }
            else if (Convert.ToBoolean(ConfigurationManager.AppSettings["ManagedServicesTestMode"]) && !Convert.ToBoolean(ConfigurationManager.AppSettings["MFBUserLoggedIn"]))
            {
                return null;
            }
            try
            {
                FunctionsMembershipUser user = Membership.GetUser() as FunctionsMembershipUser;
                if (user == null)
                {
                    Trace.TraceInformation("User Null ");
                    new PANE.ERRORLOG.Error().LogToFile(new Exception("User Is Null"));
                    throw new SessionTimeOutException("The User is null");
                }
                else
                {
                    //Trace.TraceInformation("tRYING ALL TRACES-{0}",user.UserName);
                    if (user.UserDetails == null)
                    {
                        Trace.TraceInformation("DETAILS Null ");
                    }
                    else if (user.UserDetails.Role == null)
                    {
                        Trace.TraceInformation("Role is  Null ");
                    }
                    //else if (user.UserDetails.Role.UserCategory == null)
                    //{
                    //    Trace.TraceInformation("Category is  Null ");
                    //}
                    if (user.UserDetails.Role.UserCategory == UserCategory.Mfb)
                    {

                        string exMessage = user.UserDetails.Role.ToString();
                        new PANE.ERRORLOG.Error().LogToFile(new Exception(exMessage));
                        return GetInstitutionCode();
                    }
                    else
                    {
                        //new PANE.ERRORLOG.Error().LogToFile(new Exception("User Is not MFB"));
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation(ex.Message+ex.StackTrace);
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new SessionTimeOutException(string.Format("Your Session may have timed out. You may need to logout  and log back in-{0}",ex.Message));
            }

        }
        private static string GetInstitutionCode()
        {
            //TODO call Institutin Web Service
            //Trace.TraceInformation("About to Get MFB");
            string mfbCode = MFBCode;
            //Trace.TraceInformation(mfbCode);
            using (PANE.Framework.MfbServiceRef.MfbServiceClient client = new PANE.Framework.MfbServiceRef.MfbServiceClient())
            {

                PANE.Framework.MfbServiceRef.Mfb mfb = client.GetByCode(mfbCode);
                if (mfb != null)
                {
                    return mfb.Identity;
                }
            }

            throw new ApplicationException("Mfb Is Null");
        }
 
      
        
        
    }
}
