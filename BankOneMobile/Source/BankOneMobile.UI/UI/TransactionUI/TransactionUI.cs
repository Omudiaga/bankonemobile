﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using System.Diagnostics;

namespace BankOneMobile.UI.UI.TransactionUI
{
    public class ListTransactionsUI : AppZoneUI.Framework.EntityUI<TransactionModel>
    {
        Dictionary<string, string> Institutions;
        bool appzoneUser = false; 
        public ListTransactionsUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            appzoneUser = string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode());
            UseFullView();

            AddNorthSection()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateFrom).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("From"),
                            Map(x=>x.FromAccount).AsSectionField<ITextBox>(),                           
                           
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateTo).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("To"),
                             Map(x=>x.transactionType).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.TransactionTypeList().Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Transaction Type"),
                         
                     
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.status).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.TransactionStatusList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                         Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")

                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y =>appzoneUser).SetDefaultValue<TransactionModel>(appzoneUser)),
                          AddSectionButton()
                .WithText("Search").NoValidate()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x=>
                    {
                                              
                        
                        return x;
                    }
                ),
                 AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("TransactionReport.aspx"))
                }),
              }
              );





            HasMany(x => x.Transactions).AsCenter<Grid>()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))                    
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Mobile Transactions Export")
                    .ExportAllRows().SetPagingLimit<TransactionModel>(y => (int)System.Web.HttpContext.Current.Session["TransTotalCount"]))
                .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500)
                    .Popup<TransactionTypeUI>("Transaction Details")
                    .PrePopulate<ITransaction, TransactionModel>(y =>
                        {
                            var result = new TransactionModel()
                            {
                                SelectedTransaction = y
                            };
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                if (y.TransactionTypeName == TransactionTypeName.CashIn)
                                {
                                    result.TheCashInModel = CashInTransactionTypeSystem.GetCashInTransactionType(source, y.TransactionTypeID) as CashInTransactionType;
                                    //Force nhibernate to load the proxy objects     
                                    var item = result.TheCashInModel.TheTransaction.Amount;
                                }
                                if (y.TransactionTypeName == TransactionTypeName.CashOut)
                                {
                                    result.TheCashOutModel = CashOutTransactionTypeSystem.GetCashOutTransactionType(source, y.TransactionTypeID) as CashOutTransactionType;
                                    //Force nhibernate to load the proxy objects     
                                    var item = result.TheCashOutModel.TheTransaction.Amount;
                                }
                                else if (y.TransactionTypeName == TransactionTypeName.BalanceEnquiry)
                                {
                                    result.TheBalanceInquiryModel = new BalanceInquiryTransactionTypeSystem(source).GetBalanceInquiryTransactionType(y.TransactionTypeID) as BalanceInquiryTransactionType;
                                    //Force nhibernate to load the proxy objects              
                                    var item = result.TheBalanceInquiryModel.TheTransaction.Amount;
                                }
                                else if (y.TransactionTypeName == TransactionTypeName.MiniStatement)
                                {
                                    result.TheMiniStatementModel = MiniStatementTransactionTypeSystem.GetMiniStatementTransactionType(source, y.TransactionTypeID) as MiniStatementTransactionType;
                                    //Force nhibernate to load the proxy objects                                   
                                    var item = result.TheMiniStatementModel.TheTransaction.Amount;
                                }
                                else if (y.TransactionTypeName == TransactionTypeName.PINChange)
                                {
                                    result.ThePinChangeModel = PinChangeTransactionTypeSystem.GetPinChangeTransactionType(source, y.TransactionTypeID) as PinChangeTransactionType;
                                    //Force nhibernate to load the proxy objects                                   
                                    var item = result.ThePinChangeModel.TheTransaction.Amount;
                                }
                            }
                            return result;
                        })
                )
                //.ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<MobileAccountLogic.TheTransactionType(x>("Cash In Transaction Details"))
                .Of<ITransaction>()




                .WithRowNumbers()
                .IsPaged<TransactionModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    decimal amt = 0M;
                    string instCode = string.Empty;
                    try
                    {
                        string selectedType = x.transactionType;
                        if (!string.IsNullOrEmpty(x.transactionType))
                        {
                            selectedType = MobileAccountLogic.TransactionTypeList()[x.transactionType];
                            //x.transactionType = MobileAccountLogic.TransactionTypeList()[x.transactionType];
                        }
                        string inCode = UserLogic.GetLoggedOnUserInstCode();
                        x.InstitutionCode = string.IsNullOrEmpty(inCode) ? x.InstitutionCode : inCode;
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.Transactions = new TransactionSystem(source).Search(x.FromAccount, x.InstitutionCode, selectedType, x.dateFrom, x.dateTo, x.status, e.Start, e.Limit, out totalCount, out amt);
                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = ex.Message;
                        //PANE.ERRORLOG.ErrorLogger.Log(ex);
                        Trace.TraceError("Error on retrieving transactions for report");
                        Trace.TraceError(ex.Source);
                        Trace.TraceError(ex.Message);
                        Trace.TraceError(ex.StackTrace);
                        Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    }
                    System.Web.HttpContext.Current.Session["TransTotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;

                    return x;
                })
                 .WithColumn(x => x.Date)
                .WithColumn(x => x.Status)
                .WithColumn(x => x.StatusDetails, "Details")
                .WithColumn(x => DisplayTransactionTypeName(x.TransactionTypeName), "Transaction Type")
                .WithColumn(x => x.StringAmount, "Amount(=N=)")

                .WithColumn(x => x.FromPhoneNumber, "Mobile Account")

                .WithColumn(x => MobileAccountLogic.InstitutionName(x.FromInstitutionCode), "Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y =>appzoneUser).SetDefaultValue<TransactionModel>(appzoneUser))
                ;//, "MobileAccountDetail.aspx?id={0}", x => x.ID)



        }
        public string DisplayTransactionTypeName (TransactionTypeName type)
        {
            string toReturn = type.ToString();
            if (type == TransactionTypeName.CashIn)
            {
                toReturn = "Cash Deposit";
            }
            else  if (type == TransactionTypeName.CashOut)
            {
                toReturn = "Cash Withdrawal";
            }
            return toReturn;
        }

        
    }
    public class TransactionTypeUI : AppZoneUI.Framework.EntityUI<TransactionModel>
    {
        

        public TransactionTypeUI()
        {


            AddSubControl<CashInDetailUI>(x => x.TheCashInModel)
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName == TransactionTypeName.CashIn)
                    .Hide<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName != TransactionTypeName.CashIn));
            AddSubControl<BalanceEnquiryDetailUI>(x => x.TheBalanceInquiryModel)
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName == TransactionTypeName.BalanceEnquiry)
                .Hide<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName != TransactionTypeName.BalanceEnquiry));
            AddSubControl<PINChangeDetailUI>(x => x.ThePinChangeModel)
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName == TransactionTypeName.PINChange)
                .Hide<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName != TransactionTypeName.PINChange));
            AddSubControl<MiniStatementDetailUI>(x => x.TheMiniStatementModel)
              .ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName == TransactionTypeName.MiniStatement)
                .Hide<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName != TransactionTypeName.MiniStatement));
            AddSubControl<CashOutDetailUI>(x => x.TheCashOutModel)
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName == TransactionTypeName.CashOut)
                .Hide<TransactionModel>(y => y.SelectedTransaction.TransactionTypeName != TransactionTypeName.CashOut));

        }
    }
}


