﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using BankOneMobile.Core.Implementations;
using BankOneMobile.UI.Logic;

namespace BankOneMobile.UI.UI.HostUI
{
   public class HostDetailUI: EntityUI<Host>
    {
       public HostDetailUI()
        {
           
            PrePopulateFrom<Host>(x =>
            {
                return HostLogic.GetHost(x.ID);
            })
               .Using(x => x.ID, "itemID");

            AddSection()
             .WithTitle("Host Information")
             .WithColumns(new List<Column>()
                {
                    new Column(new List<IField>()
                        {
                            Map(x => x.Name).AsSectionField<ITextLabel>(),
                            Map(x=>x.DllPath).AsSectionField<ITextLabel>().LabelTextIs("File Name"),
                           
                        }),
                    new Column(new List<IField>()
                        {                         
                      
                             Map(x => x.ClassType).AsSectionField<ITextLabel>(),
                             Map(x => x.IsActive).AsSectionField<ITextLabel>(),
                        }),
       });

           

         
             AddButton()
             .WithText("Return to List")
             .SubmitTo(x => true)
             .OnSuccessRedirectTo("HostList.aspx");

             AddButton()
                      .WithText("Edit")
                      .SubmitTo(x => true)
                      .OnSuccessRedirectTo("AddEditHost.aspx?itemID={0}", x => x.ID);

             AddButton()
                     .WithText(x => x != null && x.IsActive ? "Disable" : "Enable")
                     .ConfirmWith(x => string.Format("Are you sure you want to {0} this Host?", x != null && x.IsActive ? "Disable" : "Enable"))
                     .SubmitTo(x =>
                     {

                         x.IsActive = !x.IsActive;

                         var thecardProfile = HostLogic.UpdateHost(x);
                         return thecardProfile != null && x.IsActive == thecardProfile.IsActive;
                     })
                     .OnSuccessDisplay(x => string.Format("Host succesfully {0}!", x.IsActive ? "Enabled" : "Disabled"))
                     .OnSuccessRedirectTo("HostDetail.aspx?itemID={0}", x => x.ID);
           
        }
    }
}
