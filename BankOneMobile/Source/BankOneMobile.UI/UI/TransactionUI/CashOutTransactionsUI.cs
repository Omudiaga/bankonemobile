﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.TransactionUI
{
     public class CashOutTransactionUI : AppZoneUI.Framework.EntityUI<CashOutTransactionModel>
    {
          Dictionary<string, string> Institutions;
        
         public CashOutTransactionUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem().GetAllInstitutionNames(source);
            }
            UseFullView();

            AddNorthSection()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.FromAccount).AsSectionField<ITextBox>(),
                            Map(x=>x.dateFrom).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("From"),
                            Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution")
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.AgentCode).AsSectionField<ITextBox>(),
                         Map(x=>x.dateTo).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("To"),
                    
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.status).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.TransactionStatusList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                        
                          AddSectionButton()
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Find))
                .UpdateWith(x=>
                    {
                        x.DisplaySummary = true;                        
                        return x;
                    }
                ),
                 AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Reload))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("CashOutTransactionReport.aspx"))
                }),
              }
              );


            AddSection()
             .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))
             .WithTitle("Summary")
             .IsCollapsible()
             .IsFramed()
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.TotalTransactions).AsSectionField<ITextBox>().LabelTextIs("Total Number of Transactions"),
                            Map(x=>x.SuccessfulTransactions).AsSectionField<ITextBox>().LabelTextIs("Successful Transactions"),
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.TotalSuccessfulAmount).AsSectionField<ITextBox>().LabelTextIs("Total Successful Withdrawals"),
                        }
                      ),
              });
              //.ApplyMod<VisibilityMod>(x => x.SetVisibility<CashOutTransactionModel>(xx => xx.DisplaySummary));


            HasMany(x => x.Transactions).AsCenter<Grid>()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Cash Out Export")
                    .ExportAllRows().SetPagingLimit<MobileAccountModel>(y => (int)System.Web.HttpContext.Current.Session["CO_TotalCount"]))
               .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<CashOutDetailUI>("Cash Out Transaction Details"))
                .Of<ICashOutTransactionType>()
                .WithColumn(x => x.TheTransaction.Date)
                .WithColumn(x => x.TheTransaction.Status)
                .WithColumn(x => x.TheTransaction.StatusDetails, "Details")
                .WithColumn(x => x.AgentCode,"Agent Code")
                .WithColumn(x => x.TheTransaction.StringAmount,"Amount(=N=)")               
                .WithColumn(x =>x.TheTransaction.Session.MobilePhoneNumber,"Mobile Account")//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                .WithColumn(x => x.TheTransaction.FromInstitutionCode, "Institution")//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                .WithRowNumbers()
                .IsPaged<CashOutTransactionModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    int totalSuccess = 0;
                    decimal amt = 0M;
                    string instCode = string.Empty;
                    try
                    {
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.Transactions = new CashOutTransactionTypeSystem().Search(source, x.FromAccount, x.InstitutionCode, x.AgentCode, x.dateFrom, x.dateTo, x.status, e.Start, e.Limit, out totalCount, out amt, out totalSuccess);
                        }
                        //x.DisplaySummary = true;
                        x.TotalTransactions = totalCount;
                        x.SuccessfulTransactions = totalSuccess;
                        x.TotalSuccessfulAmount = amt;
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = ex.Message;
                    }
                    System.Web.HttpContext.Current.Session["CO_TotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    
                    return x;
                });
        }
     }
    }


