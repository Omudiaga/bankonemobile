﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using BankOneMobile.UI.Models;
using BankOneMobile.Services;
using System.Web;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class ResetPINUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
       public ResetPINUI()
        {
            PrePopulateFrom<MobileAccount>(x =>
            {
                return HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel;


            });
            AddSection()
             .WithTitle("Customer Details")
             .WithFields
             (
                new List<IField>()
               {                  

                  
                   Map(x=>x.MobileNumber).AsSectionField<ITextLabel>(),
                   Map(x=>x.LastName).AsSectionField<ITextLabel>().LabelTextIs("Last Name"),
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>().LabelTextIs("Other Names"),
                   
                   
                  AddSectionButton()
                .WithText("Reset PIN")
                .ConfirmWith("Are you sure you want to  reset this customers PIN?This action cannot be undone")
               
                .OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}",x=>x.MobileAccount.ID)
                .OnSuccessDisplay("Pin Succesfully Reset! Please print out the activation code and give it to the customer")
                
                .OnFailureDisplay(x=>x.DisplayMessage)
                
                
                
                .SubmitTo(x=>
                  {
                      try
                      {



                         // new MobileAccountSystem().ResetPIN(x.MobileAccount as MobileAccount);
                       
                           return true;
                       
                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage = ex.Message;
                          return false;

                      }
                      
                  })
                   
               }
             );




        }
    }
}
