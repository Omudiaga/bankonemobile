﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using BankOneMobile.Core.Implementations;
using AppZoneUI.Framework;
using System.Web;
using BankOneMobile.Services;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using System.Diagnostics;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class LinkMobileAccountsUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        public LinkMobileAccountsUI()
        {
            PrePopulateFrom<MobileAccountModel>(x =>
            {
                var model = HttpContext.Current.Session["MobileAccountModel"];
                if (model != null)
                {
                    return model as MobileAccountModel;
                }
                return null;
            });
            UseFullView();
            WithTitle("Link Accounts -Step 2");
            AddSection()
                .IsFormGroup()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.MobileNumber).AsSectionField<ITextBox>().Required().LabelTextIs("Phone Number"),
                        }),
                })
                .WithTitle("Primary Mobile Instrument");

            AddSection()
                .IsFormGroup()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                   Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Customer ID"),
                   
                }
                        ),
                         new Column(
                         new List<IField>()
                {
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                   Map(x=>x.PersonalPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                    //Map(x=>x.IsActive).AsSectionField<DropDownList>()
                    //    .Of<string>(MobileAccountLogic.Statuslist()).AcceptBlank("All")
                    //    .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                }
                        )

                }




                )
                .WithTitle("Customer Details");

            AddSection()
                .IsFormGroup()
             .StretchFields(90)
              .WithColumns
              (
              new List<Column>(){
                new Column(
                    new List<IField>()
                {
                    //Map(x=>x.ActivateViaJava).AsSectionField<ICheckBox>().LabelTextIs("Account will be activated by the java application?"),
                    AddSubSection().StretchFields(90)
                        .ApplyMod<MultiFieldMod>(x=>{}).WithTitle("Account will be activated by").WithFields(
                        new List<IField>()
                        {
                            Map(x => x.ActivateViaJava).AsSectionField<CheckBox>()
                                .ApplyMod<OnValueChangeMod>(x => x.UpdateWith<MobileAccountModel>(y => 
                                {
                                    //y.ActivateViaJava = true;
                                    if(y.ActivateViaJava) y.ActivateViaUSSD = false;
                                    return y;
                                })),
                            Map(x => "Mobile Application").AsSectionField<TextLabel>(), 
                            Map(x => x.ActivateViaUSSD).AsSectionField<CheckBox>()
                                .ApplyMod<OnValueChangeMod>(x => x.UpdateWith<MobileAccountModel>(y => 
                                {
                                    //y.ActivateViaUSSD = true;
                                    if(y.ActivateViaUSSD) y.ActivateViaJava = false;
                                    return y;
                                })),
                            Map(x => "USSD").AsSectionField<TextLabel>(), 
                        }),
                }),
             }).WithTitle("Others");



            HasMany(x => x.SelectedAccounts).As<Grid>().Of<ILinkingBankAccount>((HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel).Accounts)

                .WithColumn(x => x.BankAccount)

                .WithCheckBoxes()
                .WithRowNumbers();

            AddButton()

                .WithText("Back")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BookPrevious))
                .OnSuccessRedirectTo("AccountRegistration.aspx?i=Register").NoValidate()
                .SubmitTo(x =>
                {
                    //HttpContext.Current.Session["MobileAccountModel"] = null;
                    HttpContext.Current.Session.Remove("MobileAccountModel");
                    return true;
                });

            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))

          .WithText("Link Account")

          .OnSuccessRedirectTo("MobileAccountsDetail.aspx?id={0}", x => x.MobileAccount.ID)
          .ConfirmWith("Are you sure you want to Link this account?")
          .OnFailureDisplay(x => x.DisplayMessage)
          .OnSuccessDisplay(x => x.DisplayMessage)
          .SubmitTo(x =>
            {
                try
                {
                    if (x.SelectedAccounts.Count == 0)
                    {
                        throw new Exception("Please Select at least 1 Account");
                    }
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        if (!MobileAccountSystem.CheckPhoneNumber(x.MobileNumber))
                        {
                            throw new Exception(string.Format("The Phone Number ({0}) is Invalid", x.MobileAccount.MobilePhone));
                        }

                        x.MobileAccount.MobilePhone = x.MobileNumber;

                        x.MobileAccount.PersonalPhoneNumber = x.PersonalPhoneNumber;
                        x.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();

                        x.MobileAccount.ActivateViaJava = x.ActivateViaJava;
                        x.MobileAccount.ActivateViaUSSD = x.ActivateViaUSSD;
                        x.MobileAccount.InstitutionCode = x.InstitutionCode;
                        x.MobileAccount.SelectedBankAccounts = x.SelectedAccounts;
                        x.MobileAccount.PINOffset = x.InstitutionCode;//Be calm! pin offset holds instCode and Product Name holds bank Account. be calm!!
                        x.MobileAccount.ProductName = x.MobileAccount.SelectedBankAccounts[0].BankAccount;
                        Trace.TraceInformation("InstCode is {0} ", x.MobileAccount.InstitutionCode);
                        Object obj = new MobileAccountSystem(source).Insert(x.MobileAccount as MobileAccount);
                        Trace.TraceInformation("Inserted");
                        if (obj is MobileAccount)
                        {
                            x.DisplayMessage = "Mobile Account Successfuly  Linked";
                        }
                        else
                        {
                            x.DisplayMessage = "Mobile Account Creation Logged For Approval";
                            //HttpContext.Current.Session["MobileAccountModel"] = null;
                            HttpContext.Current.Session.Remove("MobileAccountModel");
                            return false;
                        }
                        // x.MobileAccount = new MobileAccountSystem().CreateMobileAccount(x.MobileAccount as MobileAccount, x.SelectedAccounts as IList<ILinkingBankAccount>);//(x.CustomerID, x.MobileNumber);
                        //HttpContext.Current.Session["MobileAccountModel"] = null;
                        HttpContext.Current.Session.Remove("MobileAccountModel");
                        return true;
                    }
                }
                catch (AlreadyRegisterdCustomerException ex)
                {
                    x.DisplayMessage = ex.Message;
                    return false;
                }
                catch (Exception ex)
                {
                    x.DisplayMessage = ex.Message;
                    return false;

                }

            })
            .LabelTextIs("The Mobile Wallets");
        }

    }
}
