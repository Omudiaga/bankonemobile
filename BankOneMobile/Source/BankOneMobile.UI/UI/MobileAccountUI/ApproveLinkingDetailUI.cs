﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using AppZoneUI.Framework;
using BankOneMobile.Core.Helpers;
using System.Web;
using BankOneMobile.Core.Helpers.Enums;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.UI.Logic;

namespace BankOneMobile.UI.UI.LinkingBankAccountUI
{
    public class ApproveLinkingDetailUI : AppZoneUI.Framework.EntityUI<LinkingBankAccount>
    {

        LinkingBankAccount mob = null;
        public ApproveLinkingDetailUI()
        {
            PrePopulateFrom<LinkingBankAccount>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {

                    mob =
                        new LinkingBankAccountSystem(source).GetByID(source, x.ID) as LinkingBankAccount;
                    return mob;

                }


            }).Using(x => x.ID);
            AddSection()
                .IsFormGroup()
             .WithTitle("Linking Request Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheMobileAccount.LastName).AsSectionField<ITextLabel>().LabelTextIs("Last Name"),
                           Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                        //Map(x=>x.RegistrarID+"("+ x.OpenedBy")"),
                        Map(x=>x.TheMobileAccount.DateCreated.ToString()).AsSectionField<ITextLabel>().LabelTextIs("Date Created"),
                       
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheMobileAccount.OtherNames).AsSectionField<ITextLabel>(),
                          Map(x=>x.TheMobileAccount.MobileAccountStatus).AsSectionField<ITextLabel>().LabelTextIs("Status"),
                   
                    Map(x=>x.TheMobileAccount.DateActivated.Year>1900?x.TheMobileAccount.DateActivated.ToString("yyyy-MM-dd"):"Not Yet Activated").AsSectionField<ITextLabel>().LabelTextIs("Date Activated"),
                           
                          // Map(x=> DisplayOpeneBy(x.OpenedBy,x.RegistrarID.ToString()),
                       
                    
                }),
                new Column(
                        new List<IField>()
                        {
                             
                            Map(x=>x.TheMobileAccount.DateActivated).AsSectionField<ITextLabel>(),
                            Map(x=>x.TheMobileAccount.OpenedBy==AccountRegistrarType.Agent?string.Format("Agent:({0})",
                                x.TheMobileAccount.RegistrarCode):"Branch").AsSectionField<ITextLabel>().LabelTextIs("Opened By"),
                  
                  
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});
           
            string instCode = UserLogic.GetLoggedOnUserInstCode();
            HasMany(x =>  x.TheMobileAccount.BankAccounts).As<Grid>().Of<LinkingBankAccount>()
                .WithColumn(x => x.CoreBankingNames, "Account Name")
                .WithColumn(x => x.CustomerID, "CustomerID")
             .WithColumn(x => x.BankAccount,"Account Number")
            
            .WithColumn(x => x.ProductName,"Product")
            .WithColumn(x=>x.TheGender,"Gender")
            .WithColumn(x=>x.InstitutionName,"Institution")
            
             //.WithColumn(x => x.Activated,"Activated")
             
             
             .WithRowNumbers();
            //AddButton().WithText("Return To List").SubmitTo(x => true).OnSuccessRedirectTo("ViewLinkingBankAccounts.aspx");

            
            
            AddButton()
               .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
               .WithText("Generate Mobile App Verification Code").SubmitTo(x =>
               {
                   try
                   {
                       using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                       {
                           bool done = new MobileAccountSystem(source).GenerateVerificationCode(x.TheMobileAccount as MobileAccount, UserLogic.GetLoggedOnUserInstCode());
                           if (done)
                           {
                               x.TheMobileAccount.DisplayMessage = String.Format("Verification Code has been generated and sent to the Mobile Phone via SMS");
                           }
                           else
                           {
                               x.TheMobileAccount.DisplayMessage = String.Format("Verification Code  has not been delivered. please try again");
                           }
                       }
                   }
                   catch (Exception ex)
                   {
                       x.TheMobileAccount.DisplayMessage = String.Format("Error in sending  Verification Code -{0}", ex.Message);
                       return false;
                   }
                   return true;

               })
               .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
               .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                //.OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
               .ConfirmWith("Reset and Send Verification Code");
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                .WithText("Reset Pin").SubmitTo(x =>

             {
                 try
                {
                    x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                    (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.PinReset;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj = new MobileAccountSystem(source).ResetPin(x.TheMobileAccount as MobileAccount);
                        if (obj is MobileAccount)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Pin for Mobile Account Succesfully Reset");

                        }
                        else
                        {
                            x.TheMobileAccount.DisplayMessage = "Pin Reset of Mobile Account  loggged for approval ";
                            return false;
                        }
                    }
                    // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                }
                catch (Exception ex)
                {
                    x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                    return false;
                }
                
                return true;

            })
                 .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                .OnFailureDisplay(x=>x.TheMobileAccount.DisplayMessage)
              //  .OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Are you sure you want to Reset The PIN for this Account" );
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowDivide))
                .WithText("Reset Pin Tries").SubmitTo(x =>
           {
               try
               {
                   x.InstitutionCode = Logic.UserLogic.MFBCode;
                   (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.PinTryReset;
                   using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                   {
                       Object obj = new MobileAccountSystem(source).ResetMobileAccountPinTries(x.TheMobileAccount as MobileAccount);
                       if (obj is LinkingBankAccount)
                       {
                           x.TheMobileAccount.DisplayMessage = String.Format("Pin Tries Succefully Reset");

                       }
                       else
                       {
                           x.TheMobileAccount.DisplayMessage = "Pin tries Reset   loggged for approval ";
                           return false;
                       }
                   }
                   // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
               }
               catch (Exception ex)
               {
                   x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                   return false;
               }

               return true;

           })
            .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                //  .OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Are you sure you want to Reset The PIN Tries for this Account");
           

            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Lock))
                .WithText("Hot List Account").SubmitTo(x =>
            {
                try
                {
                    //string instCode = Logic.UserLogic.GetLoggedOnUserInstCode();
                    x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                    (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.HotList;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj = new MobileAccountSystem(source).HotList(x.TheMobileAccount as MobileAccount);
                        if (obj is MobileAccount)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Mobile Account Succesfully Hotlisted");

                        }
                        else
                        {
                            x.TheMobileAccount.DisplayMessage = "Hotlist of Mobile Account  loggged for approval ";
                            return false;
                        }
                    }
                    // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                }
                catch (Exception ex)
                {
                    x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                    return false;
                }
                
                return true;

            })
                 .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                .OnFailureDisplay(x=>x.TheMobileAccount.DisplayMessage)
                //.OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Are you sure you want to HotList This Account");


            //if (mob != null && (mob.LinkingBankAccountStatus == LinkingBankAccountStatus.Active || mob.LinkingBankAccountStatus == LinkingBankAccountStatus.InActive || mob.LinkingBankAccountStatus == LinkingBankAccountStatus.HotListed))
            //{
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                .WithText(x=>x.TheMobileAccount.MobileAccountStatus==MobileAccountStatus.Active? "Disable":"Enable").SubmitTo(x =>//x => x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
            {
                try
                {
                    x.InstitutionCode = Logic.UserLogic.MFBCode;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj = new MobileAccountSystem(source).EnableDisable(x.TheMobileAccount as MobileAccount, 0);
                        if (obj is MobileAccount)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Mobile Account Succesfully {0}", (obj as MobileAccount).MobileAccountStatus == MobileAccountStatus.Active ? "Enabled" : "Disabled");

                        }
                        else
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("{0} of Mobile Account  loggged for approval ", (obj as MobileAccount).MobileAccountStatus == MobileAccountStatus.Active ? "Disabling" : "Enabling");
                            return false;
                        }
                    }
                   // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                }
                catch(Exception ex)
                {
                    x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                    return false;
                }
                return true;

            })
                .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                .OnFailureDisplay(x=>x.TheMobileAccount.DisplayMessage)
                //.OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith(x => x != null && x.TheMobileAccount.IsActive ? "Are you sure you want to Disable this Mobile Account" : "Are you sure you want to Enable this Mobile Account");
           // }
           
           // }

           
        }
        public string DisplayOpeneBy(string type,string value )
        {
          AccountRegistrarType acc =  (AccountRegistrarType) Enum.Parse(typeof(AccountRegistrarType),type);
          return String.Format("{0} ({1})", value, acc.ToString());
        }
        public List<ILinkingBankAccount> GetByInstCode(IList<ILinkingBankAccount> accts, string instCode)
        {
            return accts.ToList();//.Where(x => x.InstitutionCode == instCode).ToList();
        }

    }
}
