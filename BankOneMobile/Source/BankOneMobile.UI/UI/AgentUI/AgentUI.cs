﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using BankOneMobile.Services;
using BankOneMobile.UI.Models;
using BankOneMobile.Core.Contracts;
using System.Web;
using BankOneMobile.Data.Implementations;
using AppZoneUI.Framework.Mods;
using System.Diagnostics;
using BankOneMobile.Core.Exceptions;
using System.Web.Security;
using System.Configuration;

namespace BankOneMobile.UI.UI.AgentUI
{

    public class AddAgentUI : AppZoneUI.Framework.EntityUI<AgentModel>
    {
        Agent agent = null; string responseMessage = string.Empty;
        Dictionary<string, string> Institutions;

        public AddAgentUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            PrePopulateFrom<AgentModel>(x =>
            {
                AgentModel model =
                  HttpContext.Current.Session["AgentModel"] as AgentModel;
                if (model.TheAgent == null)
                {
                    model.TheAgent = new Agent();
                }
                return model;




            });
            AddSection()
                .IsFormGroup()
               .WithColumns
               (
               new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    
                   Map(x=>x.PhoneNumber).AsSectionField<ITextBox>().Required(),                   
                }),
             }).WithTitle("Primary Mobile Instrument");
            AddSection()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                   Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Customer ID"),
                   
                }
                        ),
                         new Column(
                         new List<IField>()
                {
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                   Map(x=>x.PersonalPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                }
                        )

                }




                )
                .WithTitle("Personal Details");



            AddSection()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.AccountNumber).AsSectionField<ITextLabel>().LabelTextIs("Account Number"),
                    
                   
                   
                }
                        )
                        

                }


                )
                .WithTitle("Account Details");
            AddSection()
               .WithColumns
               (
               new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.ActivateViaJava).AsSectionField<ICheckBox>().LabelTextIs("Account will be activated by the java application?"),
                    
                   
                   
                }
                        )
                        

                }


               )
               .WithTitle("Others");


            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BookPrevious))
                .WithText("Back")


       .SubmitTo(x =>
       {
           try
           {

               return true;

           }

           catch (Exception ex)
           {
               x.DisplayMessage = ex.Message;
               return false;

           }

       })
       .OnSuccessRedirectTo("AgentAccountCreation.aspx.aspx");
            AddButton().WithText("Create Agent")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))

                .SubmitTo(x =>
                {
                    try
                    {
                        x.TheAgent.PhoneNumber = x.PhoneNumber;
                        x.TheAgent.LastName = x.LastName;
                        x.TheAgent.OtherNames = x.OtherNames;
                        x.TheAgent.AccountNumber = x.AccountNumber;;
                        x.TheAgent.PersonalPhoneNumber = x.PersonalPhoneNumber;
                        x.TheAgent.ActivateViaJava = x.ActivateViaJava;
                        x.TheAgent.ActivateViaUSSD = x.ActivateViaUSSD;
                        x.TheAgent.PostingLimit = Convert.ToDecimal(x.PostingLimit.ToString("N"));

                        x.TheAgent.ActivateViaJava = x.ActivateViaJava;
                        x.TheAgent.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();
                        new PANE.ERRORLOG.Error().LogToFile(new Exception("Starting" + x.TheAgent.PhoneNumber));
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            if (!MobileAccountSystem.CheckPhoneNumber(x.TheAgent.PhoneNumber))
                            {
                                throw new Exception(string.Format("The Phone Number ({0}) is Invalid", x.TheAgent.PhoneNumber));
                            }
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("Checked Phone"));

                            Object obj =
                             new AgentSystem(source).Insert(x.TheAgent);

                            if (obj is Agent)
                            {
                                x.TheAgent = obj as Agent;
                                x.DisplayMessage = String.Format("Agent Succesfully Saved");

                            }
                            else
                            {
                                x.DisplayMessage = "Agent Save Succesfully Logged For Approval";
                                return false;
                            }
                        }
                        return true;

                    }

                    catch (Exception ex)
                    {
                        new PANE.ERRORLOG.Error().LogToFile(ex);
                        x.DisplayMessage = String.Format("Error Saving Agent:{0}", ex.Message);
                        return false;

                    }

                })
             .OnSuccessDisplay(x => x.DisplayMessage)
             .OnSuccessRedirectTo("AgentDetail.aspx?id={0}", x => x.TheAgent.ID)
             .ConfirmWith("Are you sure you want to Create This Agent")
             .OnFailureDisplay(x => x.DisplayMessage);







        }
        public AddAgentUI(object[] parameters)
        {
            PrePopulateFrom<AgentModel>(x =>
            {


                return HttpContext.Current.Session["AgentModel"] as AgentModel;


            });
            if (parameters[0].ToString() == "MobileTeller")
            {
                AddSection()
                    .WithColumns
                    (
                    new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                   Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Staff ID"),
                   
                }
                        ),
                         new Column(
                         new List<IField>()
                {
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                   Map(x=>x.PhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                }
                        )

                }




                    )
                    .WithTitle("Personal Details");



                AddSection()
                    .WithColumns
                    (
                    new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.AccountNumber).AsSectionField<ITextLabel>().LabelTextIs("Account Number"),
                   
                   
                }
                        )
                        

                }


                    )
                    .WithTitle("Account Details");


                AddButton()
                    .WithText("Back")


           .SubmitTo(x =>
           {
               try
               {

                   return true;

               }

               catch (Exception ex)
               {
                   x.DisplayMessage = ex.Message;
                   return false;

               }

           })
           .OnSuccessRedirectTo("AgentAccountCreation.aspx");
                AddButton().WithText("Create Mobile Teller")
                    .SubmitTo(x =>
                    {
                        try
                        {
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("Starting"));
                            x.TheAgent.IsMobileTeller = true;
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("set mobile teller status"));
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj =
                                 new AgentSystem(source).Insert(x.TheAgent);
                                new PANE.ERRORLOG.Error().LogToFile(new Exception("gotten agent"));
                                if (obj is Agent)
                                {
                                    x.TheAgent = obj as Agent;
                                    x.DisplayMessage = String.Format("Agent Succesfully Saved");
                                }
                                else
                                {
                                    x.DisplayMessage = "Agent Save Succesfully Logged For Approval";
                                    return false;
                                }
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            new PANE.ERRORLOG.Error().LogToFile(ex);
                            x.DisplayMessage = String.Format("Error Saving Agent:{0}", ex.Message);
                            return false;
                        }
                    })
                 .OnSuccessDisplay(x => x.DisplayMessage)
                 .OnSuccessRedirectTo("AgentDetail.aspx?id={0}", x => x.TheAgent.ID)
                 .ConfirmWith("Are you sure you want to Create This Agent")
                 .OnFailureDisplay(x => x.DisplayMessage);



            }



        }

    }
    public class AgentDetailUI : AppZoneUI.Framework.EntityUI<Agent>
    {
        string message = string.Empty;
        Agent agent = null;
        private string ADD_EDIT_AGENT = "AddEditAgents";
        private string ENABLE_DISABLE_AGENT = "EnableDisableAgents";
        public AgentDetailUI()
        {
            PrePopulateFrom<MobileAccount>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    Agent agent =
                    new AgentSystem(source).GetAgent(x.ID) as Agent;
                    return agent;
                }
            }).Using(x => x.ID);
            AddSection()
                .WithTitle("Primary Mobile Instrument")
                .IsFormGroup()
              .WithColumns
              (
              new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                   Map(x=>x.PhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),                   
                }),
            });

            AddSection()
                .IsFormGroup()
             .WithTitle("Mobile Teller Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                           Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                           Map(x=>x.AddressLocation).AsSectionField<ITextLabel>(),              
                        //   Map(x=>x.MobileAccount.ActivationCode).AsSectionField<ITextLabel>().LabelTextIs("Activation Code")
                         // Map(x=>x.IsMobileTeller).AsSectionField<ICheckBox>().LabelTextIs("Mobile Teller?"),                     
                }),
                new Column(
                        new List<IField>()
                        {
                             Map(x=>x.Code).AsSectionField<ITextLabel>(),
                   Map(x=>x.PersonalPhoneNumber).AsSectionField<ITextLabel>(),
                  
                   Map(x=>x.IsActive?"Active":"In Active").AsSectionField<ITextLabel>().LabelTextIs("Status"),
                   
                        })});
            AddSection()
                .IsFormGroup()
                .WithTitle("Mobile Teller Account Details")
                .WithFields
             (
                new List<IField>()
               {                  

                   Map(x=>x.TheAgentAccount!=null? x.TheAgentAccount.BankAccount:string.Empty).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller Staff ID"),
                }
             );
            AddSection()
               .IsFormGroup()
               .WithTitle("Limits")
               .WithFields
            (
               new List<IField>()
               {      
                   Map(x=>x.PostingLimit.ToString("N")).AsSectionField<ITextLabel>().LabelTextIs("Posting Limit"),
               }
            );

            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<Agent>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.InstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              }
             );


            //if (Roles.IsUserInRole(ENABLE_DISABLE_AGENT))
            //{

            AddButton().WithText(x => x != null && x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
            {
                try
                {
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj =
                         new AgentSystem(source).EnableDisable(x);

                        if (obj is Agent)
                        {
                            x = obj as Agent;
                            x.DisplayMessage = String.Format("Mobile Teller Succesfully {0}", x.IsActive ? "Enabled" : "Disabled");
                            message = x.DisplayMessage;
                        }
                        else
                        {
                            x.DisplayMessage = "Mobile Teller Enabled/Disable Succesfully Logged For Approval";
                            message = x.DisplayMessage;
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    x.DisplayMessage = string.Format("Error in Enable/Disable - {0}", ex.Message);
                    message = x.DisplayMessage;
                    return false;
                }
            })
                .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x => x.DisplayMessage)
                //  .OnSuccessRedirectTo("AgentDetail.aspx?id={0}",x=>x.ID)
                .ConfirmWith(x => x != null && x.IsActive ? "Are you sure you want to disable this Mobile Teller" : "Are you sure you want to enable this Mobile Teller");
            // }
            //if (Roles.IsUserInRole(ADD_EDIT_AGENT))
            //{
            AddButton()
                .WithText("Edit")
                .ApplyMod<ButtonPopupMod>(x => x.Popup<EditAgentUI>("Edit Mobile Teller").PrePopulate<Agent, Agent>(y => y));
            // }
            //.SubmitTo(x => { return true; });
            //.OnSuccessRedirectTo("EditAgent.aspx?id={0}", x => x != null? x.ID:0);
            // AddButton().WithText("Return To List").SubmitTo(x => { return true; })
            //      .OnSuccessRedirectTo("AgentList.aspx?id={0}", x => x != null ? x.ID : 0);



        }

    }
    public class EditAgentUI : AppZoneUI.Framework.EntityUI<Agent>
    {
        Agent agent = null;

        public EditAgentUI()
        {

            WithTitle("Edit Mobile Teller");
            AddSection()
                  .WithTitle("Primary Mobile Instrument")
                  .IsFormGroup()


                .WithColumns
                (
                new List<Column>()
              {
                 
                      new Column
                      (
                      new List<IField>()
                        {
                             
                   Map(x=>x.PhoneNumber).AsSectionField<ITextLabel>(), 
                        }
                      )
              }
                );
            AddSection()
            .WithTitle("Personal Details")
            .IsFormGroup()


          .WithColumns
          (
          new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.LastName).AsSectionField<ITextBox>().TextFormatIs(TextFormat.alphanumeric).Required(),
                   Map(x=>x.OtherNames).AsSectionField<ITextBox>().TextFormatIs(TextFormat.alphanumeric).Required(),
                   Map(x=>x.PersonalPhoneNumber).AsSectionField<ITextBox>(), 
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.AddressLocation).AsSectionField<ITextArea>(), 
                   
                        }
                      )
              }
          );
            AddSection()
               .WithTitle("Limits")
               .IsFormGroup()


             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.PostingLimit).AsSectionField<ITextBox>().LabelTextIs("Posting Limit").TextFormatIs(TextFormat.numeric).Required(),
                  
                        }
                      ),
                     
              }
             );


            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))
                .WithText("Update")

                //.OnSuccessRedirectTo("AgentDetail.aspx?id={0}",x=>x.ID)

                .OnFailureDisplay(x => x.DisplayMessage)

                .OnSuccessDisplay(x => x.DisplayMessage)

                .SubmitTo(x =>
                {
                    try
                    {
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            object obj =
                                new AgentSystem(source).Update(x) as Agent;
                            if (obj is Agent)
                            {
                                x.DisplayMessage = "Mobile Teller Succesfully Updated";
                                return true;
                            }
                            else
                            {
                                x.DisplayMessage = "Mobile Teller Update Logged for Approval";
                                return true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = string.Format("Error Updating Mobile Teller:{0}", ex.Message);
                        return false;
                    }

                });


        }

    }
    public class ListAgentsUI : AppZoneUI.Framework.EntityUI<AgentModel>
    {
        bool appzoneUser = string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode());
        bool toUse = Convert.ToBoolean(ConfigurationManager.AppSettings["IsMFBScope"]);
        Dictionary<string, string> Institutions;
        BankOneMobile.Services.BranchServiceRef.Branch[] Branches;
        public ListAgentsUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            string code = UserLogic.GetLoggedOnUserInstCode();
            try
            {
                using (var client = new BankOneMobile.Services.BranchServiceRef.BranchServiceClient())
                {
                    var br = client.RetrieveAll(code);
                    if (br != null)
                    {
                        Branches = br;
                    }
                }
            }
            catch
            {

            }
            UseFullView();

            var searchList = new List<IField>()
                        {
                            
                            Map(x=>x.LastName).AsSectionField<TextBox>(),
                    Map(x => x.OtherNames).AsSectionField<TextBox>(),
                    Map(x => x.AccountNumber).AsSectionField<TextBox>().LabelTextIs("Staff ID"),
                    
                    
                        
                    
                };





            AddNorthSection()
                .IsFramed()
                .IsCollapsible()
              .WithTitle("Search")
              .WithColumns(
              new List<Column>(){
                    new Column(searchList),
                new Column(
                        new List<IField>()
                        {
                             //Map(x=>x.stat).AsSectionField<DropDownList>().Of<BeneficiaryValidation.Utilities.Enums.TransactionStatus>().Add(statusType).AcceptBlank("All").LabelTextIs("Status"),
                     Map(x => x.PhoneNumber).AsSectionField<TextBox>(),
                    Map(x => x.Code).AsSectionField<TextBox>(),
                     Map(x => x.TheBranch).AsSectionField<DropDownList>()
                        .Of<BankOneMobile.Services.BranchServiceRef.Branch>(Branches)
                        .ListOf(x => x.Name, x => x.ID).WithTypeAhead().AcceptBlank("---All---").LabelTextIs("Branch")
                        .ApplyMod<VisibilityMod>(x => x.SetVisibility<AgentModel>(y =>!appzoneUser).SetDefaultValue<AgentModel>(true)),
                     
                    AddSectionButton()
                    
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x=>
                    {
                       // x.Agents =new  AgentSystem().Search(x.LastName, x.OtherNames, x.Code, x.PhoneNumber,x.AddressLocation, x.IsActive);
                        
                        return x;
                    }
                ).NoValidate(),
                   
            AddSectionButton()
                .WithText("Reset")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .SubmitTo(x => false).NoValidate()
                .OnFailureRedirectTo(("AgentList.aspx"))
                }),
                 new Column(
                        new List<IField>()
                        {
                             //Map(x=>x.stat).AsSectionField<DropDownList>().Of<BeneficiaryValidation.Utilities.Enums.TransactionStatus>().Add(statusType).AcceptBlank("All").LabelTextIs("Status"),
                    
                    Map(x => x.AddressLocation).AsSectionField<TextBox>(),
                     Map(x=>x.IsActive).AsSectionField<DropDownList>()
                        .Of<string>(AgentLogic.Statuslist()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Is Active"),
                    Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x => x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<AgentModel>(y =>appzoneUser).SetDefaultValue<AgentModel>(appzoneUser)),
                   
                   
            
                })
                });

            HasMany(x => x.Agents).AsCenter<Grid>().LabelTextIs("Mobile Tellers")
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Mobile Tellers Export")
                    .ExportAllRows().SetPagingLimit<AgentModel>(y => (int)System.Web.HttpContext.Current.Session["MT_TotalCount"]))
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Table))
                .ApplyMod<ViewDetailsMod>(x => x.Popup<AgentDetailUI>("Mobile Teller Details"))
                .Of<Agent>()

               // .WithColumn(x => x.IsMobileTeller, "Mobile Teller?")

                .IsPaged<AgentModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    // x.InstitutionCode = string.IsNullOrEmpty(logInsCode)?x.InstitutionCode:logInsCode;
                    //x.Agent = new MobileAccountSystem().Search(x.MobileNumber, x.InstitutionCode, x.LastName, x.OpenedBy, x.RegistrarID, x.InstitutionCode, x.IsActive, e.Start, e.Limit, out totalCount);
                    // x.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();
                    string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                    x.InstitutionCode = string.IsNullOrEmpty(institutionCode) ? x.InstitutionCode : institutionCode;
                    long branchID = x.TheBranch == null ? 0 : x.TheBranch.ID;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        x.Agents = new AgentSystem(source).Search(x.LastName, x.InstitutionCode, x.OtherNames, x.Code, x.PhoneNumber, x.AddressLocation, x.IsActive, x.IsMobileTeller, x.AccountNumber, branchID, e.Start, e.Limit, out totalCount);
                    }
                    System.Web.HttpContext.Current.Session["MT_TotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    return x;
                })
                .WithRowNumbers()
                 .WithColumn(x => x.LastName)
                .WithColumn(x => x.OtherNames)
                .WithColumn(x => x.AccountNumber, "Staff ID")
                .WithColumn(x => x.Code)
                .WithColumn(x => x.MobileAccount == null ? string.Empty : x.MobileAccount.MobilePhone, "Phone Number")
                .WithColumn(x => x.IsActive ? "Active" : "In Active", "Status")
                .WithColumn(x => x.TheAgentAccount.InstitutionName, "Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<AgentModel>(y => appzoneUser).SetDefaultValue<AgentModel>(appzoneUser));

        }

    }
    public class AgentRegistrationUI : AppZoneUI.Framework.EntityUI<AgentModel>
    {
        public AgentRegistrationUI()
        {

            PrePopulateFrom<AgentModel>(x =>
            {

                AgentModel mod =
                    HttpContext.Current.Session["AgentModel"] as AgentModel == null ? new AgentModel() : HttpContext.Current.Session["AgentModel"] as AgentModel;
                mod.TheAgent = new Agent();

                return mod;


            });
            AddSection()
             .WithTitle("Agent Details")
             .WithFields
             (
                new List<IField>()
               {                  

                  
                   Map(x=>x.AccountNumber).AsSectionField<ITextBox>().Required().LabelTextIs("Account Number").WithLength(17),

                   
                   
                   
                  AddSectionButton()
                .WithText("Next")
               .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.NextGreen))
                
                .OnSuccessRedirectTo("AddAgent.aspx")
                
                .OnFailureDisplay(x=>x.DisplayMessage)
                
                
                
                .SubmitTo(x=>
                  {
                      try
                      {
                          new PANE.ERRORLOG.Error().LogToFile(new Exception("Starting"));
                          Trace.TraceInformation("About to Get Info");
                          string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                          x.TheAgent.InstitutionCode = institutionCode;
                          using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                          {
                            new PANE.ERRORLOG.Error().LogToFile(new Exception(String.Format("Gotten Institutin Code-{0}",institutionCode)));
                            x.TheAgent.InstitutionName = new InstitutionSystem(source).GetByCode(source,x.TheAgent.InstitutionCode).ShortName;
                          
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("Gottn Institution Name"));
                          
                            x.TheAgent = new AgentSystem(source).GetAgentFromCoreBanking(x.AccountNumber,institutionCode,false);
                          }
                          x.TheAgent.AccountNumber=  x.AccountNumber;
                          x.LastName = x.TheAgent.LastName;
                          x.OtherNames = x.TheAgent.OtherNames;
                          //x.PhoneNumber = x.TheAgent.PhoneNumber;
                          //if (!MobileAccountSystem.CheckPhoneNumber(x.TheAgent.PhoneNumber))
                          //{
                          //    throw new InvalidPhoneNumberException("Agent Phone Number is Invalid");
                          //}
                          new PANE.ERRORLOG.Error().LogToFile(new Exception("Checked Phone Number"));
                          HttpContext.Current.Session["AgentModel"] = x;
                           return true;
                       
                      }
                      catch (Exception ex)
                      {
                          new PANE.ERRORLOG.Error().LogToFile(ex);
                          x.DisplayMessage = ex.Message;
                          return false;

                      }
                      
                  })
                   
               }
             );




        }
    }
    public class MobileTellerRegistrationUI : AppZoneUI.Framework.EntityUI<AgentModel>
    {
        List<string> st;

        public MobileTellerRegistrationUI()
        {
            PrePopulateFrom<AgentModel>(x =>
            {

                AgentModel mod =
                    HttpContext.Current.Session["AgentModel"] as AgentModel == null ? new AgentModel() : HttpContext.Current.Session["AgentModel"] as AgentModel;
                mod.IsMobileTeller = true;
                mod.DisplayMessage = "MobileTeller";
                return mod;


            });
            UseFullView();
            WithTitle("Mobile Teller Registration - Step 1");

            AddSection()
             .WithTitle("Enter Mobile Teller Staff ID")
             .IsFormGroup()
             .WithFields(
                new List<IField>()
               {                 
                   //.TextFormatIs(TextFormat.alphanumeric)
                   Map(x=>x.AccountNumber).AsSectionField<ITextBox>().Required().LabelTextIs("Staff ID").WithLength(17),
               }
             );


            AddButton()
                .WithText("Next")
               .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.NextGreen))
                .OnSuccessRedirectTo("../MobileAccountManagement/UnLinkingAccount.aspx", x => x.DisplayMessage)
                .OnFailureDisplay(x => x.DisplayMessage)
                .SubmitTo(x =>
                  {
                      try
                      {
                          string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                          using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                          {
                              x.TheAgent = new AgentSystem(source).GetAgentFromCoreBanking(x.AccountNumber, institutionCode, true);
                          }
                          x.TheAgent.InstitutionCode = institutionCode;
                          x.TheAgent.IsMobileTeller = true;
                          x.TheAgent.AccountNumber = x.AccountNumber;
                          x.LastName = x.TheAgent.LastName;
                          x.OtherNames = x.TheAgent.OtherNames;
                          if (!MobileAccountSystem.CheckPhoneNumber(x.TheAgent.PhoneNumber))
                          {
                              throw new InvalidPhoneNumberException("Mobile Teller Phone Number is Invalid");
                          }

                          HttpContext.Current.Session["AgentModel"] = x;
                          return true;

                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage = ex.Message;
                          return false;

                      }

                  });

        }
    }
    public class AddMobileTeller : AppZoneUI.Framework.EntityUI<AgentModel>
    {
        Agent agent = null; string responseMessage = string.Empty;

        public AddMobileTeller()
        {
            PrePopulateFrom<AgentModel>(x =>
            {                
                return HttpContext.Current.Session["AgentModel"] as AgentModel;
            });

            UseFullView();
            WithTitle("Mobile Teller Registration - Step 2");

            AddSection()
                .IsFormGroup()
               .WithColumns
               (
               new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    
                   Map(x=>x.PhoneNumber).AsSectionField<ITextBox>().Required(),                   
                }),
             }).WithTitle("Primary Mobile Instrument");

            AddSection()
                .IsFormGroup()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                   Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Customer ID"),
                   
                }),
                    new Column(
                    new List<IField>()
                {
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                   Map(x=>x.PhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                })
            }).WithTitle("Personal Details");

            AddSection()
                .IsFormGroup()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {

                    Map(x=>x.AccountNumber).AsSectionField<ITextLabel>().LabelTextIs("Staff ID"),                  
                   
                })
                    })
                .WithTitle("Account Details");
            AddSection()
                .IsFormGroup()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                    Map(x=>x.PostingLimit).AsSectionField<ITextBox>().TextFormatIs(TextFormat.numeric).LabelTextIs("Posting Limit").Required(),
                   
                })
            }).WithTitle("Limits");

            AddSection()
                .IsFormGroup()
             .StretchFields(90)
              .WithColumns
              (
              new List<Column>(){
                new Column(
                    new List<IField>()
                {
                    //Map(x=>x.ActivateViaJava).AsSectionField<ICheckBox>().LabelTextIs("Account will be activated by the java application?"),
                    AddSubSection().StretchFields(90)
                        .ApplyMod<MultiFieldMod>(x=>{}).WithTitle("Account will be activated by").WithFields(
                        new List<IField>()
                        {
                            Map(x => x.ActivateViaJava).AsSectionField<CheckBox>()
                                .ApplyMod<OnValueChangeMod>(x => x.UpdateWith<AgentModel>(y => 
                                {
                                    if (y.ActivateViaJava) y.ActivateViaUSSD = false;
                                    return y;
                                })),
                            Map(x => "Mobile Application").AsSectionField<TextLabel>(), 
                            Map(x => x.ActivateViaUSSD).AsSectionField<CheckBox>()
                                .ApplyMod<OnValueChangeMod>(x => x.UpdateWith<AgentModel>(y => 
                                {
                                    if (y.ActivateViaUSSD) y.ActivateViaJava = false;
                                    return y;
                                })),
                            Map(x => "USSD").AsSectionField<TextLabel>(), 
                        }),
                }),
             }).WithTitle("Others");


            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BookPrevious))
                .WithText("Back").NoValidate()
                 .SubmitTo(x =>
                   {
                       try
                       {
                           return true;
                       }
                       catch (Exception ex)
                       {
                           x.DisplayMessage = ex.Message;
                           return false;
                       }
                   }).OnSuccessRedirectTo("../AgentManagement/MobileTellerCreation.aspx");

            AddButton().WithText("Create Mobile Teller")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))

                .SubmitTo(x =>
                {
                    try
                    {
                        x.TheAgent.PhoneNumber = x.PhoneNumber;
                        x.TheAgent.PersonalPhoneNumber = x.PersonalPhoneNumber;
                        x.TheAgent.ActivateViaJava = x.ActivateViaJava;
                        x.TheAgent.ActivateViaUSSD = x.ActivateViaUSSD;
                        x.TheAgent.PostingLimit = Convert.ToDecimal(x.PostingLimit.ToString("N"));
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            if (!MobileAccountSystem.CheckPhoneNumber(x.TheAgent.PhoneNumber))
                            {
                                throw new Exception(string.Format("The Phone Number ({0}) is Invalid", x.TheAgent.PhoneNumber));
                            }
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("Clicked Insert"));
                            Object obj =
                             new AgentSystem(source).Insert(x.TheAgent);
                            new PANE.ERRORLOG.Error().LogToFile(new Exception("Out of INSERT"));

                            if (obj is Agent)
                            {
                                x.TheAgent = obj as Agent;
                                x.DisplayMessage = String.Format("Mobile Teller Succesfully Saved");

                            }
                            else
                            {
                                x.DisplayMessage = "Creation of  Mobile Teller  Succesfully Logged For Approval";
                                return false;
                            }
                            return true;
                        }
                    }

                    catch (Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                        x.DisplayMessage = String.Format("Error Saving Mobile Teller:{0}", ex.Message);
                        return false;

                    }

                })
             .OnSuccessDisplay(x => x.DisplayMessage)
             .OnSuccessRedirectTo("../AgentManagement/AgentDetail.aspx?id={0}", x => x.TheAgent.ID)
             .ConfirmWith("Are you sure you want to Create This Mobile Teller")
             .OnFailureDisplay(x => x.DisplayMessage);
        }
    }
}
