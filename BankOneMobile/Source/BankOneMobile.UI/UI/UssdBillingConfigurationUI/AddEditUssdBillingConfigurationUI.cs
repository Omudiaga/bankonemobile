﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using AppZoneUI.Framework.Mods;

namespace BankOneMobile.UI.UI.UssdBillingConfigurationUI
{
    public class AddEditUssdBillingConfigurationUI : EntityUI<UssdBillingConfiguration>
    {
        public AddEditUssdBillingConfigurationUI()
        {
            AddSection()
                .IsFramed()
                .WithTitle("USSD Billing Configuration")
                .WithFields(new List<IField>() 
                { 
                    Map(x => x.Name)
                        .AsSectionField<ITextBox>()
                        .Required()
                        .WithLength(50),
                    Map(x => x.Amount)
                        .AsSectionField<ITextBox>()
                        .Required()
                        .LabelTextIs("Amount(Naira)")
                        .TextFormatIs(TextFormat.numeric)
                        .WithLength(10),
                    Map(x => x.Period)
                        .AsSectionField<ITextBox>()
                        .Required()
                        .LabelTextIs("Interval(seconds)")
                        .WithLength(10),
                    AddSectionButton()
                        .WithText("Save")
                        .SubmitTo(x =>
                        {
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                IUssdBillingConfiguration config = new UssdBillingConfigurationSystem(source).SaveOrUpdate(x);
                                return !(config == null);
                            }
                        })
                        .OnSuccessDisplay("Channel has been saved successfully")
                        .OnFailureDisplay("Channel save failed!")
                        .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.DatabaseSave))
                });
        }
    }
}
