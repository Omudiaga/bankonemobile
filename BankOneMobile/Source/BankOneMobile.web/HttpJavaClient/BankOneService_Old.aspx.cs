﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BankOneMobile.Services;
using BankOneMobile.Services.WorkflowServices;

public partial class HttpJavaClient_BankOneService : System.Web.UI.Page
{
    //global::menuRequestParams @params;
    string  response;
    Guid SessionID;

    private const string SS_OPERATION_SS = "Operation";
    private const string SS_MSISDN_SS = "MSISDN";
    private const string SS_TEXT_SS = "Text";
    private const string SS_EXIT_CODE_SS = "EXIT_CODE";
    private const string SS_EXIT_REASON_SS = "EXIT_REASON";
    private const string SS_EXIT_CODE_SPECIFIED_SS = "EXIT_CODE_SPECIFIED";
    private const string SS_SESSION_ID_SS = "SESSION_ID";
    private const string SS_ACTIVATION_CODE_SS = "ACTIVATION_CODE";
    private const string SS_VERIFICATION_CODE_SS = "VERIFICATION_CODE";

    protected void Page_Load(object sender, EventArgs e)
    {       
       // PANE.ERRORLOG.ErrorLogger.Log(new Exception("Startig Now. Just Entered"));
        string operation = Request.Params["OPERATION"];
        string sessionID = Request.Params["SESSION_ID"];
        string reason = Request.Params["EXIT_REASON"];
        string exitCode = Request.Params["EXIT_CODE"];
        string useXML = Request.Params["USE_XML"];
        string paddingString = Request.Params["USE_PADDING"];
        string text = Request.Params["TEXT"];
        string msisdn = Request.Params["MSISDN"];
        string activationCode = Request.Params["ACTIVATION_CODE"];
        string useVerificationCode = Request.Params["USE_VERIFICATION_CODE"];
        string verificationCode = Request.Params["VERIFICATION_CODE"];
    
        bool exitCodeSpecified =Convert.ToBoolean(Request.Params["EXIT_CODE_SPECIFIED"]);
        bool useXMLValue = string.IsNullOrEmpty(useXML) ? false : Convert.ToBoolean(Request.Params["USE_XML"]);
        //Determines whether to use padding while decrypting
        //Used for the PhoneGap Android and Blackberry apps
        //Default is true if query parameter is not set
        bool usePadding = string.IsNullOrEmpty(paddingString) ? true : Convert.ToBoolean(paddingString);
        bool useVerificationCodeValue = string.IsNullOrEmpty(useVerificationCode) ? false : Convert.ToBoolean(useVerificationCode);

        string msg = string.Format("operation :{0} ,sess:{1},usexml:{2}, text:{3},msisdn:{4},activationCode:{5}", operation,sessionID,useXML,text,msisdn,activationCode);
       // PANE.ERRORLOG.ErrorLogger.Log(new Exception(msg));
        CryptoUtil.CryptoServices crypt = new CryptoUtil.CryptoServices();
        if (!usePadding) crypt = new CryptoUtil.CryptoServices(usePadding);
        //string decMSISDN = crypt.DecryptData(msisdn);
        //string decActivationCode = crypt.DecryptData(activationCode);
        //string decOperation = crypt.DecryptData(operation);
        //string decSessionID = crypt.DecryptData(sessionID);
        

        if (string.IsNullOrEmpty(operation))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_OPERATION_SS),true);
            return;
        }
        //new PANE.ERRORLOG.Error().LogToFile(new Exception(msisdn + "Midway through"));
         if (string.IsNullOrEmpty(sessionID))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_SESSION_ID_SS),true);
            return;
        }
             
        else if (string.IsNullOrEmpty(msisdn))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_MSISDN_SS),true);
            return;
        }
        try
        {
         operation = crypt.BCDecrypt(operation);
        }
        catch(Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
         try
         {
             text = crypt.BCDecrypt(text);
         }
         catch (Exception ex)
         {
             PANE.ERRORLOG.ErrorLogger.Log(ex);
         }
        try
        {
         activationCode = crypt.BCDecrypt(activationCode);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            sessionID = crypt.BCDecrypt(sessionID);
            
        }

        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            msisdn = crypt.BCDecrypt(msisdn);

        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            verificationCode = crypt.BCDecrypt(verificationCode);

        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }


         if (operation.ToLower() != "start" && operation.ToLower() != "next" && operation.ToLower() != "activation")
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}",SS_OPERATION_SS),true);
            return;
        }
         if (useVerificationCodeValue && string.IsNullOrEmpty(verificationCode))
         {
             ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
             return;
         }
         string msgsss = string.Format(" After Decryption ::operation :{0} ,sess:{1},usexml:{2}, text:{3},msisdn:{4},activationCode:{5}", operation, sessionID, useXML, text, msisdn, activationCode);
       //  PANE.ERRORLOG.ErrorLogger.Log(new Exception(msgsss));
         msisdn = ConvertToStandard(msisdn);
         switch (operation.ToLower())
        {
            //NOW check verification before calling web service
            case "start":
                if (useVerificationCodeValue)
                {
                    bool verified = new TpaWebService().ConfirmActicationCode(msisdn, verificationCode, false);
                    if (!verified)
                    {
                        ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
                        return;
                    }
                }
                BankOneServiceReference.menuResponseParams rr = null;
                using (BankOneServiceReference.UssdThirdPartyWsDemo client = new BankOneServiceReference.UssdThirdPartyWsDemo())
                {
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} about to call START BankOneService.{1}", sessionID, DateTime.Now)));
                    try
                    {
                        rr = client.onSessionStart
                            (sessionID.ToString(), new BankOneServiceReference.menuRequestParams()
                            {
                                msisdn = msisdn,
                                text = text,
                                optional = useXMLValue.ToString()
                            });
                        @response = rr.ussdMenu;
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} out of START BankOneService.{1}", sessionID, DateTime.Now)));
                    }
                    catch(Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\n{2}\n{3}", ex.Message, ex.Source, ex.StackTrace,
                            ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        BankOneMobile.Services.Utility.Utility.SendErrorMail("", "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile session service Error: {0}", ex.Message), body);
                        ReturnResponse("An error occurred.", true);
                        return;
                    }
                }
                ReturnResponse(@response, rr.shouldClose);
                return;


            case "next":
                if (useVerificationCodeValue)
                {
                    bool verified = new TpaWebService().ConfirmActicationCode(msisdn, verificationCode, false);
                    if (!verified)
                    {
                        ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
                        return;
                    }
                }
                if (string.IsNullOrEmpty(text))
                {
                    ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_TEXT_SS),true);
                    return;
                }
                BankOneServiceReference.menuResponseParams rsp = null;
                using (BankOneServiceReference.UssdThirdPartyWsDemo cl = new BankOneServiceReference.UssdThirdPartyWsDemo())
                {

                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} about to call NEXT BankOneService.{1}", sessionID, DateTime.Now)));
                    try
                    {
                        rsp = cl.onUserResponse(sessionID.ToString(), new BankOneServiceReference.menuRequestParams()
                        {
                            msisdn = msisdn,
                            text = text,
                            optional=useXMLValue.ToString()
                        });
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} out of NEXT BankOneService.{1}", sessionID, DateTime.Now)));
                    }
                    catch(Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                         string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\n{2}\n{3}", ex.Message, ex.Source, ex.StackTrace, 
                            ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        BankOneMobile.Services.Utility.Utility.SendErrorMail("", "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile session service Error: {0}", ex.Message), body);
                        ReturnResponse("An error occurred.", true);
                        return;
                    }
                    if (rsp.shouldClose)
                    {
                        cl.onSessionEnd(sessionID, 200, true, "Ended Normally");
                        ReturnResponse(rsp.ussdMenu,rsp.shouldClose);
                        return;
                    }
                    else
                    {
                        ReturnResponse(rsp.ussdMenu, rsp.shouldClose);
                        return;
                    }                   

                  
                    
                }
                
            case "activation":
                {
                    if (string.IsNullOrEmpty(activationCode))
                    {
                        ReturnResponse("Invalid Parameters -Activation Code",true);
                        return;
                    }
                   // new PANE.ERRORLOG.Error().LogToFile(new Exception("msisdn is " + msisdn));
                  bool activated = new TpaWebService().ConfirmActicationCode(msisdn, activationCode, true);
                 // new PANE.ERRORLOG.Error().LogToFile(new Exception("msisdn is " + msisdn));
                    ReturnResponse(activated?"1":"0",true);

                    break;
                }
        }
         
    }

    string ConvertToStandard(string msisdn)
    {        
        if (msisdn.StartsWith("08"))
        {
            msisdn = msisdn.Substring(3);
            msisdn = "+23480" + msisdn;
        }
        if (msisdn.StartsWith("07"))
        {
            msisdn = msisdn.Substring(3);
            msisdn = "+23470" + msisdn;
        }
        if (!msisdn.StartsWith("+"))
        {
            msisdn = "+" + msisdn;
        }
        return msisdn;
    }


    private void ReturnResponse(string  response,bool shouldClose)
    {
        //response = "<Response><Display><BeforeMenu>Before</BeforeMenu><Menu><MenuItem Index='1'>First</MenuItem><MenuItem Index='2'>Second</MenuItem></Menu></Display><ShouldMask>0</ShouldMask></Response>";
        //response = response.Replace("\r\n", "").Replace("\n", "");
        string value = "<Response><Menu>$MENU$</Menu><ShouldClose>$SHOULDCLOSE$</ShouldClose></Response>";
        value = value.Replace("$MENU$", response).Replace("$SHOULDCLOSE$",shouldClose?"1":"0");
      response = value.Replace("\r\n", "\n");
      response = new CryptoUtil.CryptoServices().BCEncrypt(response);      
     Response.ClearHeaders();
     HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
      Response.ClearContent(); 
      Response.Write(response);
      if (Response.IsClientConnected)
      {
          Response.Flush();
      }
    }
}