﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs ef) 
    {
        // Code that runs on application startup
        try
        {

            System.Diagnostics.Trace.TraceInformation("Trace Just Began");
            BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
            // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
            //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
            ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
            ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
            ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
            ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

            

            
            System.Diagnostics.Trace.TraceInformation("Initialised Properties");

            
            ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
            ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
            ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
            ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
            Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
            System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
            BankOneMobile.Extension.ApplicationInitializer.Init();

            AppZoneSwitch.Services.RoutingSystem.SystemError += (o, a) =>
            {
                System.Diagnostics.Trace.TraceError(a.TheException.ToString());
            };
            AppZoneSwitch.Services.RoutingSystem.SystemInitialize += (o, a) =>
            {
                AppZoneSwitch.Services.Implementations.TransactionSplitter.InitializeEngine();
                AppZoneSwitch.Services.ReversalSystem.InitializeEngine();
                AppZoneSwitch.Services.ResyncSystem.InitializeEngine();
                AppZoneSwitch.Services.SwitchConnectionSystem.InitializeEngine();

                //TransactionLogSystem.InitializeEngine();







            };
            AppZoneSwitch.Services.RoutingSystem.InitializeEngine();
            new PANE.ERRORLOG.Error().LogToFile(new Exception("Init Successfull"));
            System.Diagnostics.Trace.TraceInformation("Init Succesfull");   
            
        }
        catch(Exception ex)
        { 
            new PANE.ERRORLOG.Error().LogToFile(ex);
            System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}",ex.Message,ex.InnerException==null?"None":ex.InnerException.Message);
            throw;
            
        }
      
    }
    
    
    
 
    void Application_End(object sender, EventArgs e)
    {
        //Code that runs on application shutdown  
         
    }
   
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs        
        //var ex = Server.GetLastError().GetBaseException();
        //string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
        //if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
        //{
        //    emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
        //}
        //string body = string.Format("{0}\n{1}\n{2}\n{3}", ex.Message, ex.Source, ex.StackTrace,
        //    ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
        //BankOneMobile.Services.Utility.Utility.SendErrorMail("", "donotreply@mybankone.com",
        //    emails, string.Format("BankOneMobile Unhandled Error"), body);
    }

    void Session_Start(object sender, EventArgs e)
    {

        // Code that runs when a new session is started
        //Ensure SessionID in order to prevent the folloing exception
        //when the Application Pool Recycles
        //[HttpException]: Session state has created a session id, but cannot
        //    save it because the response was already flushed by 
        string sessionID = Session.SessionID;

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.  
        
    
      
    }

    public override void Init()
    {

        try
        {
            base.Init();
          
        }
        catch(Exception ex)
        {
            new PANE.ERRORLOG.Error().LogToFile(ex);
        }
        
       // BankOneMobile.Extension.ApplicationInitializer.InitWeb(this);

    }
       
</script>
