﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coolite.Ext.Web;
using System.Web.Caching;
using System.Collections;

public partial class Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string val = "<Recharge><Response><ResponseData><ResponseCode>90000</ResponseCode><ResponseDesc>Successful</ResponseDesc><TransactionRef>PTR|WEB|DSTV|28102009141205|0000001</TransactionRef>< RechargePIN >76964555891</ RechargePIN ></ResponseData></Response></ Recharge >";

    }

    protected void RefreshHomeTabData(object sender, StoreRefreshDataEventArgs e)
    {
     
   /*
        var data = this.Page.Cache["ExamplesGroups"] as SiteMapNodeCollection;
        this.Page.Cache.Remove("ExamplesGroups");
        if (data == null)
        {
            data = GetChildNodes(SiteMap.RootNode);
            this.Page.Cache.Add("ExamplesGroups", data, null, DateTime.Now.AddHours(1), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }
        */


        this.Store1.DataSource = GetChildNodes(SiteMap.RootNode);
        this.Store1.DataBind();
    }

    private ArrayList GetChildNodes(SiteMapNode node)
    {
        ArrayList arr = new ArrayList();
        foreach (SiteMapNode childNode in node.ChildNodes)
        {
            var child = new { Title = childNode.Title, Url = childNode.Url, Description = childNode.Description, ResourceKey = childNode.ResourceKey, ChildNodes = GetChildNodes(childNode) };            
            arr.Add(child);
        }
        return arr;
    }
}