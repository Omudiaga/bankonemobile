﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Services;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "USSDBalanceEnquiryService" in code, svc and config file together.
//[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class USSDBalanceEnquiryService : IUSSDBalanceEnquiryService
{
	public bool CheckCustomerBalance(string phoneNumber)
    {
        bool val = new MobileAccountSystem().LoopThroughLinkedAccounts(phoneNumber);
        return val;
    }
}
