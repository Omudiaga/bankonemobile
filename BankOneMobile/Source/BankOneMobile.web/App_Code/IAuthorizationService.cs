﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAuthorizationService" in both code and config file together.
[ServiceContract]
public interface IAuthorizationService
{
    [OperationContract]
    bool SetRecievingAccount(string phoneNumber, string accountNumber);
    [OperationContract]
    bool UpdatePinTries(string phoneNumber, bool IsReset);
    [OperationContract]
    string ValidateMobileAccount(string phoneNumber, string institutionCode, string accountNumber, string encryptedPinBlock, bool isPhoneNumber, bool verifyPin, out string acctNo);
    [OperationContract]
    string ValidateBankAccount(string identifier, string institutionCode, bool isPhoneNumber, out string accountNumber);
    [OperationContract]
    bool ChangePin(string phoneNumber, string newPinBlock);
}
