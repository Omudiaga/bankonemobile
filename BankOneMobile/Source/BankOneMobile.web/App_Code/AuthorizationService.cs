﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Contracts;
using System.Diagnostics;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AuthorizationService" in code, svc and config file together.
//[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class AuthorizationService : IAuthorizationService
{
    public string ValidateMobileAccount(string phoneNumber, string institutionCode, string accountNumber, string encryptedPinBlock, bool isPhoneNumber, bool verifyPin, out string acctNo)
    {
        Trace.TraceInformation("Validate Mobile Account 1 {0}, {1}, {2}, {3},{4}, {5}",phoneNumber, institutionCode, accountNumber, encryptedPinBlock, isPhoneNumber, verifyPin);
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).ValidateMobileAccount(phoneNumber, institutionCode, accountNumber, encryptedPinBlock, isPhoneNumber, verifyPin, out  acctNo);
        }
    }

    public string ValidateBankAccount(string identifier, string institutionCode, bool isPhoneNumber, out string accountNumber)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).ValidateReciever(identifier, institutionCode, isPhoneNumber, out  accountNumber);
        }
    }

    public bool UpdatePinTries(string phoneNumber, bool IsReset)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).UpdatePinTries(phoneNumber, IsReset);
        }
    } 
    public bool ChangePin(string phoneNumber, string newPinBlock)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).ChangePin(phoneNumber, newPinBlock);
        }
        
    }
    public bool SetRecievingAccount(string phoneNumber, string accountNumber)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).SetRecievingAccount(phoneNumber, accountNumber);
        }
    }

    public string ValidateMobileAccount(string phoneNumber, string institutionCode, string encryptedPinBlock, bool isPhoneNumber, bool verifyPin, out string acctNo)
    {
        Trace.TraceInformation("Validate Mobile Account 2 {0}, {1}, {2}, {3},{4}", phoneNumber, institutionCode,  encryptedPinBlock, isPhoneNumber, verifyPin);
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).ValidateMobileAccount(phoneNumber, institutionCode, encryptedPinBlock, isPhoneNumber, verifyPin, out  acctNo);
        }
    }



}
