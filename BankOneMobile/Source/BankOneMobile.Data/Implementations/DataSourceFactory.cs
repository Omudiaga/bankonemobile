﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Implementations
{
    public class DataSourceFactory
    {

        public static IDataSource GetDataSource(DataCategory dataCategory)
        {
            return new DataSource(dataCategory);
        }

        public static IDataSource GetDataSourceWithUniqueKey(DataCategory dataCategory)
        {
            return new DataSource(dataCategory, Guid.NewGuid().ToString());
        }


        public static IDataSource GetDataSource(DataCategory dataCategory, Guid workflowId)
        {
            return new DataSource(dataCategory, workflowId);
        }

        public static IDataSource GetDataSource(DataCategory dataCategory, IInstitution tenant)
        {
            return new DataSource(dataCategory, tenant);
        }


        public static IDataSource GetDataSource(DataCategory dataCategory, IInstitution tenant, Guid workflowId)
        {
            return new DataSource(dataCategory, tenant, workflowId);
        }
    }
}
