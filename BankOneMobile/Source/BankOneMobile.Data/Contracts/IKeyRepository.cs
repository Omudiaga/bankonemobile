﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IKeyRepository : IRepository<IKey>
    {
         IKey GetPEK(IDataSource dataSource);
         IKey GetTMK(IDataSource dataSource);

        
    }
}
