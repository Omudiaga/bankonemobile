﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IDataSource : IDisposable
    {
        DataCategory Type { get; }
        IInstitution TheTenant { get; }

        string FactoryKey { get; }
        string SessionKey { get; }
        string StorageKey { get; }

        //Workflow-related
        Guid WorkflowId { get; }
    }
}
