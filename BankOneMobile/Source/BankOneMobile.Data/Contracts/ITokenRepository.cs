﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface ITokenRepository : IRepository<IToken>
    {

       IList< IToken>  GetByValueAndPhoneNumber(IDataSource dataSource,string tokenValue, string phoneNumber);
        
        
       
    }
}
