﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.Contracts
{
    /// <summary>
    /// Provides Interface for system Data-Access activities such as committing changes and rollbacks
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// Commits changes made to entities of a data-store (within and outside a transaction).
        /// </summary>
        void CommitChanges(IDataSource dataSource);

        /// <summary>
        /// Begins a data-store Transaction, after which a commit or rollback can be called.
        /// </summary>
        void BeginTransaction(IDataSource dataSource);

        /// <summary>
        /// Commits the changes made during a transaction.
        /// </summary>
        void CommitTransaction(IDataSource dataSource);

        /// <summary>
        /// Rollbacks the changes made during a transaction.
        /// </summary>
        void RollbackTransaction(IDataSource dataSource);


        void Close(IDataSource dataSource);
    }
}

