﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IFundsTransferCashOutTransactionTypeRepository : IRepository<IFundsTransferCashOutTransactionType>
    {
        IFundsTransferCashOutTransactionType GetByToken(IDataSource dataSource, string token);
        IFundsTransferCashOutTransactionType GetByPhoneNumber(IDataSource dataSource, string phoneNumber);
    }
}
