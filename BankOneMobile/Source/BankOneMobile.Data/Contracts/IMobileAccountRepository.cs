﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
  public  interface IMobileAccountRepository : IRepository<IMobileAccount>
    {
      IMobileAccount GetByPhoneNumber(IDataSource dataSource, string phoneNumber);
      IMobileAccount GetByStandardPhoneNumber(IDataSource dataSource, string standardPhoneNumber);
      ILinkingBankAccount GetByAccountNumber(IDataSource dataSource, string acctNo,string institutionCode);
      ILinkingBankAccount GetByAccountNumber(IDataSource dataSource, string acctNo);
      ILinkingBankAccount GetByAccountNumberForAuthorization(IDataSource dataSource, string acctNo, string instCode);
      IMobileAccount GetByActivationCode(IDataSource dataSource, string phoneNumber);
      List<IMobileAccount> FindWithPaging(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);
      
    }
}
