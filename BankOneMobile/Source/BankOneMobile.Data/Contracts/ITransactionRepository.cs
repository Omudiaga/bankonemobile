﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface ITransactionRepository : IRepository<ITransaction>
    {
         
          ITransactionType UpdateTransactionType(IDataSource dataSource, ITransactionType transactionType);
           List<ITransaction> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);
    }
}
