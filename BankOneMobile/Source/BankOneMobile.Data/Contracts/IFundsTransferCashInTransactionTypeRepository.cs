﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IFundsTransferCashInTransactionTypeRepository : IRepository<IFundsTransferCashInTransactionType>
    {
        IFundsTransferCashInTransactionType GetByToken(IDataSource dataSource, string token);
        IList<IFundsTransferCashInTransactionType> GetByPhoneNumber(IDataSource dataSource, string phoneNumber);
    }
}
