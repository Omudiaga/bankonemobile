﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
  public  interface ISessionRepository : IRepository<ISession>
    {
      ISession GetBySessionID(IDataSource dataSource, string sessionID);
      BankOneMobile.Core.Contracts.ISession GetByWorkFlowID(IDataSource dataSource, string workFlowID);
      IList<ISession> GetSessionsBetweenDates(IDataSource dataSource,DateTime fromDate,DateTime toDate);
    }
}
