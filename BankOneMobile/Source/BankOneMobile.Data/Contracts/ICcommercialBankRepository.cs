﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface ICommercialBankRepository : IRepository<ICommercialBank>
    {

        ICommercialBank GetByCode(IDataSource dataSource, string code);
        ICommercialBank GetByName(IDataSource dataSource, string name);

        IList<ICommercialBank> GetActiveCommercialBanks(IDataSource dataSource);
       
    }
}
