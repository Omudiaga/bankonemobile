﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IFlowRepository : IRepository<IFlow> 
    {
        IFlow GetStartupFlow(IDataSource dataSource);
        IFlow GetStartupFlow(IDataSource dataSource, bool isAgent);
        IFlow GetByUniqueId(IDataSource dataSource, Guid uniqueId);
        IFlow GetActivationFlow(IDataSource dataSource);
        IFlow GetStartupFlow(IDataSource dataSource, bool isAgent, bool isStartUp);
        IFlow  GetStartupFlowForFundsTransferCustomers(IDataSource dataSource);
    }
}
