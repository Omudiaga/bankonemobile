﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using HSM.Interface.Utility;
//using HSM.Interface.Enums;
//using HSM.Interface;

//namespace testHSmInterface
//{
using BankOneMobile.Services;
using System.Threading;
using System.Collections.Generic;
using System;
class Program       
{
    static void Main(string[] args)
    {
        new BankOneMobile.CustomActions.AdvansLafayetteCustomerRegistrationAction();
            
        //List<string> keyschemelst =   Utility.GetKeySchemes();
        //foreach (string keyscheme in keyschemelst)
        //{
        //    Console.WriteLine(keyscheme);

        //}

        //   KeyScheme keyscheme = Utility.GetKeySchemeEnum("Single Length Des Key ASNI", " ");

        /* generated ZPK */
        //ZPKGenerator generator = new ZPKGenerator();
        //generator.DefaultHost = true;
        //generator.Generate("", "C4B9832842F4B261");

        ///* Translate ZPK*/
        //ZPKTranslator translator = new ZPKTranslator();
        //translator.DefaultHost = true;
        //translator.TranslateToZMK("C4B9832842F4B261", "D543E5D2EC2EC9A2");


        //PINGenerator genr = new PINGenerator();
        //var acctNumber = "5044070000253211";
        //genr.DefaultHost = true;
        //genr.Generate(acctNumber, "04");

        //Console.Read();
        //string offSet = "0000FFFF";
        string phoneNo = "08034730365";
        string enteredPin = "0000";
        byte[] encryptedPinBytes = new byte[8];
        string valueUnderParent = "XEA14CEFD8F14885A7641735159236FEF";
        string configPvk = "U4E903A0A96B30A38B1C2CC9013A81CCC";
        string valueUnderLMK = "XDA6ACCC9D52BA4284C1143B10280C2E3";
        string pinOffset = "6445FFFFFFFF";
        int limit = 500;

        string encryptedPinBlock = HSMCenter.GeneratePinBlockForHSMTest(phoneNo, enteredPin, AppZone.HsmInterface.PinBlockFormats.ANSI, valueUnderParent);
        string encryptedPin = string.Empty;
        //  byte[] encryptedBytes = new byte[8];
        //Convert to Byte array
        //ThalesSim.Core.Utility.HexStringToByteArray(encryptedPinBlock, ref encryptedPinBytes);
        //Validate Pin


        // string generatedOffset = new HSMCenter().GeneratePINOffsetOnHSMForTest(phoneNo, enteredPin, configPvk);
        DateTime start = DateTime.Now;
        List<Thread> threads = new List<Thread>();
      //  for (int j = 0; j < 5; j++)
        {
            for (int i = 0; i < limit; i++)
            {
                Thread newThread = new Thread(new ParameterizedThreadStart(TestHSM));
                threads.Add(newThread);
                newThread.Start(new object[] { phoneNo, pinOffset, encryptedPinBlock, configPvk, valueUnderLMK, i });
                //System.Threading.Thread.Sleep(1000);
            }
           // System.Threading.Thread.Sleep(2000);
        }

        for (int i = 0; i < limit; i++)
        {
            threads[i].Join();
        }
        DateTime end = DateTime.Now;
        TimeSpan interval = end.Subtract(start);
        Console.WriteLine("\n\n{0} - {1}", interval, threads.Count);
        //
        Console.ReadKey();
    }

    

}
//}
