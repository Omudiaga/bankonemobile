﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.ThirdPartyWeb.Controllers
{
    public class AccountController : ApiController
    {
     
        internal class Feedback
        {
            public string AccountStatus { get; set; }
            public bool Status { get; set; }
            public string AccountNumber { get; set; }
        }
        public class AccountDetails 
        {
            public string PhoneNumber {get; set;}
            public string Lastname {get; set;}
            public string OtherNames {get; set;}
            public string Address {get; set;}
            public string Gender {get; set;}
            public string DateOfBirth_Format_ddMMyyyy { get; set; }
            public string InstitutionCode { get; set; }
            public Byte[] Passport { get; set; }
        }

        [HttpPost]
        public HttpResponseMessage CreateAccount(string PhoneNumber, string Lastname, string OtherNames, string Address, string Gender, string DateOfBirth_Format_ddMMyyyy, string InstitutionCode, [FromUri]byte[] Passport = null)
        {
            
            AccountDetails newAccount = new AccountDetails()
            {
                PhoneNumber = PhoneNumber,
                Lastname =Lastname,
                OtherNames=OtherNames,
                Address=Address,
                Gender = Gender,
                DateOfBirth_Format_ddMMyyyy =DateOfBirth_Format_ddMMyyyy,
                InstitutionCode = InstitutionCode,
                Passport=Passport
            };
            var msg = CreateAccount(newAccount);
            return msg;
        }

        [HttpPost]
        public HttpResponseMessage CreateAccount(AccountDetails ProspectAccount)
        {
            var requestObject = System.Web.HttpContext.Current;
            HttpResponseMessage msg = new HttpResponseMessage();
            Feedback FD = new Feedback();
            try
            {
                DateTime DOB = DateTime.ParseExact(ProspectAccount.DateOfBirth_Format_ddMMyyyy, "ddMMyyyy", new System.Globalization.CultureInfo("en-GB", true));
                using (CoreBankingService.SwitchingServiceClient thirdParty = new CoreBankingService.SwitchingServiceClient())
                {
                    var objInfo = CoreBankingService.AccountInformationSource.Others;
                    var objGender = CoreBankingService.Gender.Male;
                    string Prod = System.Configuration.ConfigurationManager.AppSettings["ProductCode"];
                    string AccountOfficer = System.Configuration.ConfigurationManager.AppSettings["AccountOfficerCode"];
                    if (!(string.IsNullOrEmpty(ProspectAccount.Gender)))
                    {
                        objGender = ProspectAccount.Gender.ToUpper() == "MALE" ? CoreBankingService.Gender.Male : CoreBankingService.Gender.Female;
                    }

                    var Account = thirdParty.CreateAccount(
                        ProspectAccount.InstitutionCode, ProspectAccount.Lastname, ProspectAccount.OtherNames, ProspectAccount.PhoneNumber,
                        objGender, Prod, AccountOfficer, "", DOB, ProspectAccount.Address, "", "", "", "", "", true, objInfo, "", ProspectAccount.Passport);
                    FD.AccountStatus = Account.Status.ToString();
                    FD.AccountNumber = Account.NUBAN;
                    FD.Status = FD.AccountNumber != null ? true : false;
                    msg = Request.CreateResponse(HttpStatusCode.Created, FD);
                }
                try // After the Above is successful, Now save the Mobile Profile
                {
                    ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                    ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                    string responseMessage = "";
                    MobileProfile.AccountNumber = FD.AccountNumber;
                    MobileProfile.Status = "Active";
                    MobileProfile.Gender = ProspectAccount.Gender;
                    MobileProfile.PhoneNumber = ProspectAccount.PhoneNumber;
                    MobileProfile.OtherName = ProspectAccount.OtherNames;
                    MobileProfile.LastName = ProspectAccount.Lastname;

                    Logic.Insert(MobileProfile, out responseMessage);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceInformation("An Error occured while saving Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                    throw;
                }


            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation("An Error Occurred Creating Account on CoreBanking");
                msg = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, ex.Message);
            }

            return msg;
        }

        [HttpGet]
        public HttpResponseMessage EnableExistingAccountByAccountNumber(string AccountNumber, string InstitutionCode)
        {
            HttpResponseMessage msg = new HttpResponseMessage();
            Feedback FD = new Feedback();
            try
            {
                using (CoreBankingService.SwitchingServiceClient thirdParty = new CoreBankingService.SwitchingServiceClient())
                {

                    //Do not forget to Log for Approval
                    var Account = thirdParty.GetAccountByAccountNo(InstitutionCode, AccountNumber);
                    var customer = thirdParty.GetCustomer(InstitutionCode, Account.CustomerID);
                    FD.AccountNumber = Account.NUBAN;
                    FD.AccountStatus = Account.Status.ToString();
                    FD.Status = FD.AccountNumber != null ? true : false;
                    msg = Request.CreateResponse(HttpStatusCode.Created, FD);

                    try // After the Above is successful, Now save the Mobile Profile
                    {
                        ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                        ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                        string responseMessage = "";
                        MobileProfile.AccountNumber = AccountNumber;
                        MobileProfile.Status = "Pending Approval";
                        MobileProfile.Gender = customer.Gender.ToString();
                        MobileProfile.PhoneNumber = Account.PhoneNo;
                        MobileProfile.OtherName = customer.FirstName + " " + customer.OtherNames;
                        MobileProfile.LastName = customer.LastName;

                        Logic.Insert(MobileProfile, out responseMessage);

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation("An Error occured while saving Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, ex.Message);
            }

            return msg;
        }

        [HttpGet]
        public HttpResponseMessage EnableExistingAccountByPhoneNumber(string PhoneNumber, string InstitutionCode)
        {
            HttpResponseMessage msg = new HttpResponseMessage();
            Feedback FD = new Feedback();
            try
            {

                CoreBankingService.SwitchingServiceClient thirdParty = new CoreBankingService.SwitchingServiceClient();

                //Do not forget to Log for Approval
                var Account = thirdParty.GetAccountsByPhoneNo(InstitutionCode, PhoneNumber);
                var customer = thirdParty.GetCustomer(InstitutionCode, Account[0].CustomerID);
                FD.AccountNumber = Account[0].NUBAN;
                FD.AccountStatus = Account[0].Status.ToString();
                FD.Status = FD.AccountNumber != null ? true : false;
                msg = Request.CreateResponse(HttpStatusCode.Created, FD);

                try // After the Above is successful, Now save the Mobile Profile
                {
                    ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                    ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                    string responseMessage = "";
                    MobileProfile.AccountNumber = FD.AccountNumber;
                    MobileProfile.Status = "Pending Approval";
                    MobileProfile.Gender = customer.Gender.ToString();
                    MobileProfile.PhoneNumber = PhoneNumber;
                    MobileProfile.OtherName = customer.FirstName + " " + customer.OtherNames;
                    MobileProfile.LastName = customer.LastName;

                    Logic.Insert(MobileProfile, out responseMessage);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceInformation("An Error occured while saving Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                    throw;
                }
            }
            catch (Exception ex)
            {
                msg = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, ex.Message);
            }

            return msg;
        }


        [HttpGet]
        private bool ConfirmAccountStatusOnCoreBanking(string PhoneNumber, string InstitutionCode)
        {
            bool rst = false;
            HttpResponseMessage msg = new HttpResponseMessage();
            Feedback FD = new Feedback();
            try
            {
                using (CoreBankingService.SwitchingServiceClient thirdParty = new CoreBankingService.SwitchingServiceClient())
                {
                    //Do not forget to Log for Approval
                    var Account = thirdParty.GetAccountsByPhoneNo(InstitutionCode, PhoneNumber);
                    FD.AccountNumber = Account[0].NUBAN;
                    FD.AccountStatus = Account[0].Status.ToString();
                    FD.Status = FD.AccountNumber != null ? true : false;
                    msg = Request.CreateResponse(HttpStatusCode.Created, FD);
                    if (Account[0].Status.ToString() == "Active")
                    {
                        rst = true;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                msg = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, ex.Message);
                rst = false;
            }

            return rst;
        }

        [HttpGet]
        private bool ConfirmAccountStatusOnMobileProfile(string AccountNo, string InstitutiionCode)
        {
            bool status = false;
            try
            {
                ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                var Mp = Logic.GetByAccountNumberAndInstitutionCode(AccountNo, InstitutiionCode);
                var stat = false;
                if (Mp.Status == "Active")
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation("An Error occured while verifying status of account on Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                throw;
            }
            return status;
        }

        [HttpGet]
        public bool CheckBothAccountStatus(string AccountNo, string InstitutiionCode)
        {
            bool status = false;
            try
            {
                ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                var Mp = Logic.GetByAccountNumberAndInstitutionCode(AccountNo, InstitutiionCode);
                var Mobile = ConfirmAccountStatusOnCoreBanking(Mp.PhoneNumber, InstitutiionCode);
                var CoreB = ConfirmAccountStatusOnMobileProfile(Mp.PhoneNumber,InstitutiionCode);
                if (Mobile == true && CoreB == true )
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceInformation("An Error occured while verifying status of account on Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                throw;
            }
            return status;
        }





        //
        //
    }
}
