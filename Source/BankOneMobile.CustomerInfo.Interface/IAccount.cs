﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.CustomerInfo.Interface
{
   public interface IAccount
    {
       string AccountNumber { get; set; }
       string Type { get; set; }
    }

   public class Account : IAccount
   {
     public  string AccountNumber { get; set; }
     public  string Type { get; set; }

   }
}
 