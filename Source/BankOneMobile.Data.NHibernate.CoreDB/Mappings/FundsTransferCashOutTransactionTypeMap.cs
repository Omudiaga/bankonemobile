﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class FundsTransferCashOutTransactionTypeMap : EntityWithEnableDisableMap<FundsTransferCashOutTransactionType>
    {
        public FundsTransferCashOutTransactionTypeMap()
        {
            Map(x => x.AgentCode);
            Map(x => x.Token);
            Map(x => x.BeneficiaryPhoneNumber);
            Map(x => x.DepositorPhoneNumber);
            Map(x => x.Amount);
            Map(x => x.IsCash);
            Map(x => x.IsConsumed);
            Map(x => x.ActivationCode);


            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
            HasMany<FundsTransferCashInTransactionType>(x => x.TheCashIns).Cascade.All();
            
           
        }
    }
}
