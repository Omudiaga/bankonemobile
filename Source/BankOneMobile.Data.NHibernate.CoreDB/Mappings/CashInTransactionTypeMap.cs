﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CashInTransactionTypeMap : EntityWithEnableDisableMap<CashInTransactionType>
    {
        public CashInTransactionTypeMap()
        {
            Table("CashInTransactionTypes");
            Map(x => x.AgentCode);
            Map(x => x.Token);
            Map(X => X.IsPhoneNumber);
            Map(X => X.EnteredParameter);
            
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID").Cascade.None();
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
