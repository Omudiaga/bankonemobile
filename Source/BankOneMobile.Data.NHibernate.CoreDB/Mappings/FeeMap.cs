﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class FeeTransactionTypeTransactionTypeMap : EntityWithEnableDisableMap<FeeTransactionType>
    {
        public FeeTransactionTypeTransactionTypeMap()
        {
            Map(x => x.Amount);
            Map(x => x.TransactionTypeName);
            Map(x => x.FlatOrPercent);
        }
    }
}
