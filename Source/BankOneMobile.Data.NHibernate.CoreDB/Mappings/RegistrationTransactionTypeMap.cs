﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class RegistrationTransactionTypeMap : EntityWithEnableDisableMap<RegistrationTransactionType>
    {
        public RegistrationTransactionTypeMap()
        {
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            Map(x => x.ActivationCode);
            
            Map(x => x.PhoneNumber);
            Map(x => x.Gender);


            
          //  Map(x => x.DateOfBirth);
            Map(x => x.PlaceOfBirth);
            Map(x => x.ReferralPhoneNumber);
            Map(x => x.NOKPhone);
            Map(x => x.NOKName);
            Map(x => x.StarterPackNumber);
            Map(x => x.IDNumber);
            //Map(x => x.Passport);
            
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
