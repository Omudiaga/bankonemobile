﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CustomerRegistrationMap : EntityWithEnableDisableMap<CustomerRegistration>
    {
        public CustomerRegistrationMap()
        {
            Map(x => x.InstitutionCode);
            Map(x => x.InstitutionName);
            Map(x => x.ProductCode);
            Map(x => x.ProductName);
            Map(x => x.TheGender);
            Map(x => x.PhoneNumber);
            Map(x => x.LastName);
            Map(x => x.FirstName);
            Map(x => x.PassportID);
            Map(x => x.NOKName);
            Map(x => x.NOKPhone);
            Map(x => x.DateOfBirth);
            Map(x => x.Address);
            Map(x => x.PlaceOfBirth);
            Map(x => x.CardSerialNumber);
            Map(x => x.ActivationCode);
            Map(x => x.CustomerID);
            Map(x => x.NUBAN);
            Map(x => x.StandardAccountNumber);
            Map(x => x.AgentID);
            Map(x => x.AgentName);
            Map(x => x.AgentCode);
            Map(x => x.AccountOfficerCode);
            Map(x => x.Date);
            Map(x => x.Status);
            Map(x => x.CommitPoint);
            Map(x => x.ErrorInfo);
            Map(x => x.IsImageSync);
            Map(x => x.IsSmsSent);
        }
    }
}
