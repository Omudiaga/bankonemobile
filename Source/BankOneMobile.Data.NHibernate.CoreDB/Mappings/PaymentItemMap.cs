﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class PaymentItemMap : EntityWithEnableDisableMap<PaymentItem>
    {
        public PaymentItemMap()
        {
           Map(x=>x.Code);
           
           Map(x => x.Name);
           References<Merchant>(x => x.TheMerchant, "MerchantID");
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
