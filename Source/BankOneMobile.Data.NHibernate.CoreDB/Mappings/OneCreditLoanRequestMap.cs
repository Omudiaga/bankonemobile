﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class OneCreditLoanRequestMap : EntityMap<OneCreditLoanRequest>
    {
        public OneCreditLoanRequestMap()
        {
            Map(x => x.InstitutionCode);
            Map(x => x.AgentPhoneNumber);
            Map(x => x.CustomerPhoneNumber);
            Map(x => x.JsonRequest);
            Map(x => x.Status);
            Map(x => x.StatusMessage);
            Map(x => x.Response);
            Map(x => x.DateLogged);
            Map(x => x.DateSent);
        }
    }
}
