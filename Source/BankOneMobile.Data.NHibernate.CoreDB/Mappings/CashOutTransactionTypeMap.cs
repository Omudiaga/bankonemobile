﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CashOutTransactionTypeMap : EntityWithEnableDisableMap<CashOutTransactionType>
    {
        public CashOutTransactionTypeMap()
        {
            Map(x => x.AgentCode);
            Map(x => x.WithdrawalLocation);
            Map(x => x.Token);
            Map(x => x.CustomerPhoneNumber);             

            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();

            References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID");

          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
