﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class OfflineDepositTransactionTypeMap : EntityMap<OfflineDepositTransactionType>
    {
        public OfflineDepositTransactionTypeMap()
        {
            Map(x => x.InstitutionCode);
            Map(x => x.SessionID);
            Map(x => x.AgentPhoneNumber);
            Map(x => x.CustomerPhoneNumber);
            Map(x => x.Amount);
            Map(x => x.DateLogged);
            Map(x => x.Status);
        }
    }
}
