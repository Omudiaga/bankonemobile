﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CustomerRegistrationImageMap : EntityMap<CustomerRegistrationImage>
    {
        public CustomerRegistrationImageMap()
        {
            Map(x => x.Passport);
        }
    }
}
