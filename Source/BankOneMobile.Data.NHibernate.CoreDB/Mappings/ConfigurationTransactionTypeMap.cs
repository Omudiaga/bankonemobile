﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class ConfigurationTransactionTypeMap : EntityWithEnableDisableMap<ConfigurationTransactionType>
    {
        public ConfigurationTransactionTypeMap()
        {
            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            Map(x => x.Type);
        }
    }
}
