﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class KeyMap : EntityWithEnableDisableMap<Key>
    {
        public KeyMap()
        {
            Table("Keies");
            Map(x => x.ValueUnderParent);
            Map(x => x.ValueUnderLMK);
            Map(x => x.Type);
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
