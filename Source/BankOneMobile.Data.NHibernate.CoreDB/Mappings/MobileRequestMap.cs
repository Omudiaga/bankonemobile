﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class MobileRequestMap : EntityMap<MobileRequest>
    {
        public MobileRequestMap()
        {
            Map(x => x.Request);
            Map(x => x.Operation);
            Map(x =>x.RequestTime);
            Map(x => x.ResponseTime);
            Map(x => x.SessionID);
            Map(x => x.RequestResponse);
        }
    }
}
