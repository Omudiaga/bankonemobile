﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class LinkingBankAccountMap : EntityMap<LinkingBankAccount>
    {
        public LinkingBankAccountMap()
        {
            //Map(x => x.BankAccount);
            References<MobileAccount>(x => x.TheMobileAccount,"MobileAccountID").Not.LazyLoad();
            //References<Product>(x => x.TheProduct);
            //References<Host>(x => x.TheHost);
            Map(x => x.CustomerID);
            Map(x => x.Activated);
            Map(x => x.InstitutionCode);
            Map(x => x.ProductCode);
            Map(x => x.ProductName);
            Map(x => x.InstitutionName);
            Map(x => x.TheGender);
            Map(x => x.BankStatus);
            Map(x => x.CoreBankingNames);
            Map(x => x.Signature);
            Map(x => x.Passport);
            Map(x => x.NUBAN);
            Map(x => x.StandardAccount, "BankAccount");

          //  Map(x => x.DateOfBirth);
          //  Map(x=>x.PlaceOfBirth);
            //Map(x=>x.ReferralPhoneNumber);
           // Map(x=>x.NOKPhone);
           // Map(x=>x.NOKName);
          //  Map(x=>x.StarterPackNumber);
            this.Not.LazyLoad();
        }
    }
}
