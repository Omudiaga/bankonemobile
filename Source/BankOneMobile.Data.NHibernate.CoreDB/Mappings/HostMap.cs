﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class HostMap : EntityWithEnableDisableMap<Host>
    {
        public HostMap()
        {
            Map(x => x.ClassType);
            Map(x => x.Name);
            Map(x => x.DllPath);
        }
    }
}
