﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class MobileAccountMap : EntityWithEnableDisableMap<MobileAccount>
    {
        public MobileAccountMap()
        {
            //Not.LazyLoad();
            Map(x => x.CustomerID, "CustomerID");
            Map(x => x.LastName);
            Map(x => x.OtherNames);
           // Map(x => x.EmailAddress);
            Map(x => x.MobilePhone);
            Map(x => x.PIN);
            Map(x => x.ActivationCode);
            Map(x => x.MobileAccountStatus);
           // Map(x => x.Sex);
            //Map(x => x.DOB);
           // Map(x => x.Address);
            HasMany<LinkingBankAccount>(x => x.BankAccounts).Not.LazyLoad().Cascade.All();
            HasMany<LinkingBankAccount>(x => x.SelectedBankAccounts).Cascade.All();
            Map(x => x.IsAgentAccount);
            Map(x => x.OpenedBy);
            Map(x => x.TheGender);
            Map(x => x.RegistrarID);
            Map(x => x.DateActivated);
            Map(X => X.DateCreated);
            Map(x => x.StandardPhoneNumber);
            Map(x => x.InstitutionCode);
            Map(x => x.PinTries);
            Map(x => x.RegistrarCode);
            Map(x => x.ProductName);
            Map(x => x.ProductCode, "ProductCode");
            Map(x => x.RegistrarName);
            Map(x =>x.ActivateViaJava);
            Map(x => x.AccountRestriction);
            Map(x => x.PersonalPhoneNumber);
            Map(x => x.Verified);
            
            
            
            References<LinkingBankAccount>(x => x.RecievingBankAccount,"RecievingBankAccountID").Cascade.All().Not.LazyLoad();
            References<LinkingBankAccount>(x => x.BankAccountToDebit, "DebitingBankAccountID").Cascade.All().Not.LazyLoad();
            Not.LazyLoad();
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
