﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class GenericAgentOptimizationMap<T> : EntityMap<T> where T : GenericAgentOptimization
    {
        public GenericAgentOptimizationMap()
        {
            Polymorphism.Explicit();
            Map(x => x.InstitutionCode);
            Map(x => x.AgentPhoneNumber);
            Map(x => x.BranchID);
            Map(x => x.CdiIndex);
            Map(x => x.ShortestAgentPath);
            Map(x => x.OptimizedTotalClusterDistance);
            Map(x => x.DateGenerated);
        }
    }
    public class AgentOptimizationMap : GenericAgentOptimizationMap<AgentOptimization>
    {
        public AgentOptimizationMap()
        {

        }
    }

    public class AgentOptimizationArchiveMap : GenericAgentOptimizationMap<AgentOptimizationArchive>
    {
        public AgentOptimizationArchiveMap()
        {

        }
    }
}
