﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class TargetSavingsRegistrationTransactionTypeMap : EntityWithEnableDisableMap<TargetSavingsRegistrationTransactionType>
    {
        public TargetSavingsRegistrationTransactionTypeMap()
        {
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            Map(x => x.AccountNumber);
            
            Map(x => x.PhoneNumber);
            Map(x => x.Amount);

            Map(x => x.Frequency);
            Map(x => x.StandingOrder);
            Map(x => x.Tenure);
            Map(x => x.TotalCommitmentAmount);
            
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
