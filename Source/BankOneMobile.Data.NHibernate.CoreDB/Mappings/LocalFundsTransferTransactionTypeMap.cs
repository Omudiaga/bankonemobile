﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class LocalFundsTransferTransactionTypeMap : EntityWithEnableDisableMap<LocalFundsTransferTransactionType>
    {
        public LocalFundsTransferTransactionTypeMap()
        {
            Map(x => x.AgentCode);
            Map(x => x.Amount);
            Map(x => x.ReceivingAccountName);
            
            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
            References<MobileAccount>(x => x.ToAccount, "ToAccountID");
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
