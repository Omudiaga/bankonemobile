﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class DBNFundsTransferTransactionTypeMap : EntityWithEnableDisableMap<DBNFundsTransferTransactionType>
    {
        public DBNFundsTransferTransactionTypeMap()
        {
            Map(x => x.Amount);
            Map(x => x.Fee);
            Map(x => x.VATInPercentage);
            Map(x => x.BeneficiaryAccountName);
            Map(x => x.BeneficiaryAccountNumber);
            Map(x => x.BeneficiaryBankVerificationNumber);
            Map(x => x.BeneficiaryKYCLevel);
            Map(x => x.ChannelCode);
            Map(x => x.DestinationInstitutionCode);
            Map(x => x.NameEnquirySessionID);
            Map(x => x.Narration);
            Map(x => x.OriginatorAccountName);
            Map(x => x.OriginatorAccountNumber);
            Map(x => x.OriginatorBankVerificationNumber);
            Map(x => x.OriginatorKYCLevel);
            Map(x => x.PaymentReference);
            Map(x => x.RequestDate);
            Map(x => x.ResponseDate);
            Map(x => x.ResponseCode);
            Map(x => x.ReversalStatus);
            Map(x => x.TransactionLocation);
            Map(x => x.SessionID);
            Map(x => x.DBNFundsTransferType);
            Map(x => x.AgentPhoneNumber);
            Map(x => x.StatusMessage);
            Map(x => x.TransferStatus);

            References<Transaction>(x => x.TheTransaction, "TransactionID").Cascade.All();
        }
    }
}
