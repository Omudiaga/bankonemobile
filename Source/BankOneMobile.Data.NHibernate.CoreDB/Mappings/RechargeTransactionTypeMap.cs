﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class RechargeTransactionTypeMap : EntityWithEnableDisableMap<RechargeTransactionType>
    {
        public RechargeTransactionTypeMap()
        {
            References<Transaction>(x => x.TheTransaction,"TransactionID").Cascade.All();
            References<MobileAccount>(x => x.TheMobileAccount, "MobileAccountID");
            
            
            Map(x => x.RechargeType);
            Map(x => x.Beneficiary);
            Map(x => x.Amount);
            Map(x => x.TransactionReference);
            Map(x => x.PaymentReference);
            Map(x => x.MerchantCode);
            Map(x => x.MerchantName);
            Map(x => x.PaymentItemCode);
            References<LinkingBankAccount>(x => x.TheBankAccount, "BankAccountID");
            
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
