﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class CustomerGeoLocationMap : EntityMap<CustomerGeoLocation>
    {
        public CustomerGeoLocationMap()
        {
            Map(x => x.InstitutionCode);
            Map(x => x.CustomerPhoneNumber);
            Map(x => x.AgentPhoneNumber);
            Map(x => x.Latitude);
            Map(x => x.Longitude);
            Map(x => x.DateLogged);
        }
    }
}
