﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.CoreDB.Mappings
{
    public class ThirdPartyMobileProfileMap : EntityWithEnableDisableMap<ThirdPartyMobileProfile>
    {
        public ThirdPartyMobileProfileMap()
        {
           Map(x=>x.LastName);
           Map(x=>x.OtherName);
           Map(x => x.AccountNumber);
           Map(x => x.PhoneNumber);
           Map(x => x.Gender);
           Map(x => x.Status);
           Map(x => x.InstitutionCode);

        }
    }
}
