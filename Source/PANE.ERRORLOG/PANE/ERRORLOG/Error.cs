﻿namespace PANE.ERRORLOG
{
    using PANE.Framework.Utility;
    using System;
    using System.IO;
    using System.Web;

    public class Error : IHttpModule
    {
        private void app_Error(object sender, EventArgs e)
        {
            this.SendEmailAndLogToFile(HttpContext.Current.Server.GetLastError());
        }

        protected string BuildErrorMsg(Exception EX)
        {
            if (EX != null)
            {
                string Page = string.Empty;
                string date = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss");
                string msg = EX.GetBaseException().Message;
                try
                {
                    Page = EX.GetBaseException().TargetSite!=null?EX.GetBaseException().TargetSite .Name:null;
                }
                catch { }
                if (HttpContext.Current != null)
                {
                    try
                    {
                        Page = HttpContext.Current.Request.RawUrl;
                    }
                    catch
                    {
                    }
                }
                string Trace = EX.GetBaseException().StackTrace;
                return string.Format("\r\n\r\n[{0}]\r\n Subject: \t{1}\r\n Page Request: \t{2}\r\n Stack Trace : \t{3}", new object[] { date, msg, Page, Trace });
            }
            return "";
        }

        public void Dispose()
        {
        }

        public void Init(HttpApplication app)
        {
            app.Error += new EventHandler(this.app_Error);
        }

        public void LogToFile(Exception EX)
        {
            
                File.AppendAllText(ConfigurationManager.DirectoryLog + DateTime.Now.ToString("dd-MMM-yyyy") + ".txt", this.BuildErrorMsg(EX));
            
            
        }

        public void SendEmail(Exception EX)
        {
            try
            {
                new MailMaster().SendEmail(ConfigurationManager.ToEmail, this.BuildErrorMsg(EX));
            }
            catch
            {
            }
        }

        public void SendEmailAndLogToFile(Exception EX)
        {
            try
            {
                this.SendEmail(EX);
                this.LogToFile(EX);
            }
            catch
            {
            }
        }
    }
}

