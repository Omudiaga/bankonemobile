﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Activities.Presentation.Model;
using BankOneMobile.SessionNodes;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.SessionNodeDesigners.Screens;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.View;
using System.Activities.Statements;
using BankOneMobile.Core.Contracts;
using System.Activities;
using System.IO;
using System.Activities.XamlIntegration;
using System.Xaml;
using System.Windows;
using BankOneMobile.SessionNodeDesigners.Actions;

namespace BankOneMobile.FlowDesigner
{
    public class CommandHandlers
    {
        private UserControlWord _mainControl;

        public CommandHandlers(UserControlWord mainControl)
        {
            _mainControl = mainControl;
            GetCommandBindings();
        }


        private void GetCommandBindings()
        {
            //Command Bindings
            CommandBinding cb = new CommandBinding(ApplicationCommands.New);
            cb.Executed += new ExecutedRoutedEventHandler(ExecuteNewFlow);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(ApplicationCommands.Save);
            cb.Executed += new ExecutedRoutedEventHandler(SaveFlow);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(ApplicationCommands.Open);
            cb.Executed += new ExecutedRoutedEventHandler(OpenFlow);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.InsertMenuScreen);
            cb.Executed += new ExecutedRoutedEventHandler(AddMenuScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);


            cb = new CommandBinding(BankOneMobileCommands.InsertNotifyScreen);
            cb.Executed += new ExecutedRoutedEventHandler(AddNotifyScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);


            cb = new CommandBinding(BankOneMobileCommands.InsertConfirmScreen);
            cb.Executed += new ExecutedRoutedEventHandler(AddConfirmScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.InsertPinEntryScreen);
            cb.Executed += new ExecutedRoutedEventHandler(AddPinEntryScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.InsertCustomScreen);
            cb.Executed += new ExecutedRoutedEventHandler(AddCustomScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.InsertInitiateSessionAction);
            cb.Executed += new ExecutedRoutedEventHandler(AddInitiateSessionAction);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.InsertGotoAction);
            cb.Executed += new ExecutedRoutedEventHandler(AddGotoAction);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.ManageScreen);
            cb.Executed += new ExecutedRoutedEventHandler(ManageScreen);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.ToggleToolbox);
            cb.Executed += new ExecutedRoutedEventHandler(ToggleToolbox);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);

            cb = new CommandBinding(BankOneMobileCommands.ToggleActionProperties);
            cb.Executed += new ExecutedRoutedEventHandler(ToggleActionProperties);
            cb.CanExecute += (o, e) => e.CanExecute = true;
            _mainControl.CommandBindings.Add(cb);
        }


        public ModelItem CurrentModelItem
        {
            get
            {
                if (_mainControl.currentSelection != null)
                {
                    ModelItem item = _mainControl.currentSelection.PrimarySelection;
                    return item;
                }
                return null;
            }
        }

        void ExecuteNewFlow(object sender, ExecutedRoutedEventArgs e)
        {
            _mainControl.CurrentFlow = null;
            _mainControl.LoadFlow((string)null);
            e.Handled = true;
        }

        void OpenFlow(object sender, ExecutedRoutedEventArgs e)
        {
            var openWin = new OpenWindow();
            bool? result = openWin.ShowDialog();
            if (result.HasValue && result.Value)
            {
                 IFlow startFlow = openWin.SelectedFlow;
                 _mainControl.CurrentFlow = startFlow;
                 _mainControl.LoadFlow(startFlow);
            }

            e.Handled = true;
        }

        internal void SaveFlow(object sender, ExecutedRoutedEventArgs e)
        {            
            if (_mainControl.CurrentFlow == null)
            {
                var win = new SaveWindow();
                bool? result = win.ShowDialog();
                if (!result.HasValue || !result.Value)
                {
                    return;
                }

                _mainControl.CurrentFlow = new Core.Implementations.Flow()
                    {
                        UniqueId = Guid.NewGuid(),
                        Name = win.FlowName
                    };
                GetCurrentFlow().DisplayName = win.FlowName;
            }
            else
            {
                _mainControl.CurrentFlow.Name = GetCurrentFlow().DisplayName;
            }

            _mainControl.wd.Flush();
            _mainControl.CurrentFlow.XamlDefinition = _mainControl.wd.Text;
            using (var client = new BankOneMobileServices.FlowServiceClient())
            {
                try
                {
                    if (_mainControl.CurrentFlow.ID == 0)
                    {
                        _mainControl.CurrentFlow.ID = client.SaveFlow(_mainControl.CurrentFlow);
                        MessageBox.Show("Flow Saved Successfully");
                    }
                    else
                    {
                        client.UpdateFlow(_mainControl.CurrentFlow);
                        MessageBox.Show("Flow Updated Successfully");
                    }

                    BankOneMobile.SessionNodeDesigners.Actions.InitiateSessionDesigner.TheFlows =
                        BankOneMobile.SessionNodeDesigners.Actions.GotoFlowDesigner.TheFlows =
                        client.GetAllFlows();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not connect to Flows Server: " + ex.Message);
                }
                finally
                {
                    client.Close();
                }
            }
            e.Handled = true;
        }
        internal void AddMenuScreen(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new MenuScreen()
                    {
                        DisplayName = "New Menu Screen"
                    });
            e.Handled = true;
        }

        internal void AddConfirmScreen(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new ConfirmScreen()
            {
                DisplayName = "New Confirm Screen"
            });
            e.Handled = true;
        }

        internal void AddNotifyScreen(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new NotifyScreen()
            {
                DisplayName = "New Notify Screen"
            });
            e.Handled = true;
        }

        internal void AddPinEntryScreen(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new PinEntryScreen()
            {
                DisplayName = "New Pin Entry Screen"
            });
            e.Handled = true;
        }

        internal void AddCustomScreen(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new CustomScreen()
            {
                DisplayName = "New Custom Screen"
            });
            e.Handled = true;
        }

        internal void AddInitiateSessionAction(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new InitiateSessionAction()
            {
                DisplayName = "New Initiate Session Action"
            });
            e.Handled = true;
        }

        internal void AddGotoAction(object sender, ExecutedRoutedEventArgs e)
        {
            AddToFlow(new GotoFlowAction()
            {
                DisplayName = "New Goto Flow Action"
            });
            e.Handled = true;
        }

        private void AddToFlow(State node)
        {
            ModelItem flowItem = GetCurrentFlowModelItem();
            if (flowItem != null)
            {
                ModelItem newItem = flowItem.Properties["Nodes"].Collection.Add(node);
                Selection.SelectOnly(_mainControl.wd.Context, newItem);
            }
        }


        private ModelItem GetCurrentFlowModelItem()
        {
            ModelItem flowItem = null;
            try
            {
                flowItem = _mainControl.wd.Context.Services.GetService<DesignerView>().ActivitySchema.Properties["Implementation"].Value;
            }
            catch
            {
            }
            return flowItem;
        }

        private Flow GetCurrentFlow()
        {
            ModelItem flowItem = GetCurrentFlowModelItem();
            if (flowItem != null)
            {
                return flowItem.GetCurrentValue() as Flow;
            }
            return null;
        }

        internal void ManageScreen(object sender, ExecutedRoutedEventArgs e)
        {
            if (CurrentModelItem != null)
            {
                if (CurrentModelItem != null && CurrentModelItem.ItemType.IsAssignableFrom(typeof(MenuScreen)))
                {
                    MenuManager win = new MenuManager(CurrentModelItem, MenuDisplayChanged);
                    win.ShowDialog();
                }
            }
            e.Handled = true;
        }

        internal void ToggleToolbox(object sender, ExecutedRoutedEventArgs e)
        {
            if (_mainControl.tbc.Visibility == System.Windows.Visibility.Visible)
            {
                _mainControl.tbc.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                _mainControl.tbc.Visibility = System.Windows.Visibility.Visible;
            }
            e.Handled = true;
        }

        internal void ToggleActionProperties(object sender, ExecutedRoutedEventArgs e)
        {
            if (_mainControl.PropertyBorder.Visibility == System.Windows.Visibility.Visible)
            {
                _mainControl.PropertyBorder.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                _mainControl.PropertyBorder.Visibility = System.Windows.Visibility.Visible;
            }
            e.Handled = true;
        }

        internal void MenuDisplayChanged(object sender, PropertyChangedEventArgs e)
        {
            if (CurrentModelItem != null)
            {
                (CurrentModelItem.View as MenuScreenDesigner).UpdateDisplay();
            }
        }
    }
}
