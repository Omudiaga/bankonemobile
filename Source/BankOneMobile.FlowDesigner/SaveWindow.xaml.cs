﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.FlowDesigner
{
    /// <summary>
    /// Interaction logic for SaveWindow.xaml
    /// </summary>
    public partial class SaveWindow : Window
    {
        public string FlowName {get;set;}

        protected List<object> ExistingFlows { get; set; }
        public SaveWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (var client = new BankOneMobileServices.FlowServiceClient())
            {
                ExistingFlows = client.GetAllFlows();
            }
            lvFlows.ItemsSource = ExistingFlows;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtFlowName.Text))
            {
                MessageBox.Show("Please enter a valid flow name", "Save Flow", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (ExistingFlows != null)
            {
                if (ExistingFlows.Any(x => (x as Flow).Name.Trim().Equals(txtFlowName.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)))
                {
                    MessageBox.Show("Please enter a valid flow name", "Save Flow", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }

            FlowName = txtFlowName.Text;
            DialogResult = true;
            this.Close();
        }
    }
}
