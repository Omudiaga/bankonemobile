﻿using System.Collections;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;
using Microsoft.Windows.Controls.Ribbon;
using BankOneMobile.FlowDesigner.ViewModel;
using System.Activities.Core.Presentation;
using System.Activities.Presentation;
using System;
using System.Linq;
using System.Activities.Presentation.View;
using BankOneMobile.SessionNodeDesigners;
using System.Activities.Presentation.Model;
using BankOneMobile.SessionNodes;
using System.Windows.Input;
using BankOneMobile.Core.Contracts;
using System.Collections.Generic;
using System.Activities;
using System.Activities.XamlIntegration;
using System.Text;

namespace BankOneMobile.FlowDesigner
{
    /// <summary>
    /// Interaction logic for UserControlWord.xaml
    /// </summary>
    public partial class UserControlWord : UserControl
    {
        private CommandHandlers _theCommandHandler;

        public UserControlWord()
        {
            
            //GetCommandBindings();
            InitializeComponent();
            //Setup Toolbox
            System.Activities.Presentation.Toolbox.ToolboxCategory tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Standard Screens");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.MenuScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.NotifyScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.ConfirmScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.PinEntryScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.CustomScreen)));

            tbc.Categories.Add(tbcat);

            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Standard Actions");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.Actions.InitiateSessionAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.Actions.GotoFlowAction)));         

            tbc.Categories.Add(tbcat);
            //agent end actions///////////
            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Agent End Actions");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.FundsTransferCashOutAgentAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.FundsTransferCashInAgentAction)));           
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CashInAgentAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.PhonelessCashInAgentAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CashOutAgentAction)));            
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CustomerRegistrationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.EnhancedCustomerRegistrationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.LinkAccountAction)));

            //tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.PINVerificationAction)));
            
            tbc.Categories.Add(tbcat);

            //custmer end actions/////////
            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Customer End Actions");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.BalanceEnquiryAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.PINChangeAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.MiniStatementAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SetRecievingAccountAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.RechargeAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.LocalFundsTransferAction)));            
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CommercialBankFundsTransferAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.AnyPhoneNumberFundsTransferAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CashOutAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.AccountActivationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ReceiveMoneyVerifyCardPANAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ReceiveMoneyTransactionAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.FundsTransferCashOutAccActivationAction)));
            //tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.NewFundsTransferCashOutAgentAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetMerchantCategoriesAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetMerchantsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetPaymentItemsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetRechargePaymentItemsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetCommercialBanksAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SendSMSAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SetTransactionStatusAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CheckTransactionStatusAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.AccountEnquiryAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.LinkCardAccountAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GenerateTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateTellerAgainstCardAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetMerchantsByCategoryAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetCustomerEligibleLoanAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SendLoanRequestXMLAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetAccountBalanceAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetTargetSavingsAccountAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.TargetSavingsCustomerRegistrationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.TargetSavingsDepositAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateAmountAgainstBalanceAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CustomerPinEnquiryAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetCommitmentSavingsProductsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetAccountBalanceForKwikLoanAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CheckForExistingLoansAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateAccountNumberAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.AccountToAccountTransferAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetAccountAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetDiamondBankAccountNameAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetSpecificSetOfCommercialBanksAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetOtherBanksAccountNameAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetInstitutionAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GenerateDbnNonAccountTransferTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateDbnNonAccountTransferTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.NonAccountToNonAccountTransferAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateNonAccountHolderTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GenerateNonAccountHolderTokenAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SavingsPlanCalculatorAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ServiceBasedCustomerRegistrationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CheckPhoneNumberOrCardSerialNumberAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.LoanReportsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetAgentGeoLocationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SaveOrUpdateCustomerGeoLocationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.ValidateCustomerAgainstAgentAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.OneCreditLoanOriginationAction)));
            

            tbc.Categories.Add(tbcat);

            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Custom Screens");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.PhoneOrAccountNumberScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.SelectAccountNumberScreen)));
            //tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.TrueFalseRoutingScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.CompareAction)));
            //tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.DisplayItemScreen<>)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.TestNotifyScreen<>)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.DisplayGenericItemScreen<>)));
            

            
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.DisplayProductScreen)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetProductsAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetAccountNameAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.SessionNodeDesigners.PinEntryConfirmScreen)));
            tbc.Categories.Add(tbcat);

            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Third Party Actions");
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.ThirdPartyCustomActions.LotteryAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.AdvansLafayetteCustomerRegistrationAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.RegisterLafayetteCustomerOnOrbitAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.GetLafayetteIDTypesAction)));
            tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(typeof(BankOneMobile.CustomActions.UploadLafayettePhotoAction)));
            tbc.Categories.Add(tbcat);


            tbcat = new System.Activities.Presentation.Toolbox.ToolboxCategory("Others");

           // foreach (Type theType in typeof(System.Activities.Statements.Sequence).Assembly.GetTypes()
           //     .Where(x => x.Namespace.StartsWith("System.Activities.Statements") && !x.IsAbstract && x.IsVisible && x.IsClass && !x.IsEnum))
            foreach (Type theType in typeof(System.Activities.Activity).Assembly.GetTypes().Where(x => x.Namespace == "System.Activities.Statements" && !x.IsAbstract && x.IsVisible && x.IsClass && !x.IsEnum).OrderBy(x=>x.Name))
            {
                try
                {
                    //Confirm that a parameterless constructor exists for the Type because this is required for any toolbox item
                    //DirectoryInfo
                    
                    Activator.CreateInstance(theType);
                    tbcat.Add(new System.Activities.Presentation.Toolbox.ToolboxItemWrapper(theType));
                }
                catch { }
            }

             tbc.Categories.Add(tbcat);
            _theCommandHandler = new CommandHandlers(this);
        }


        
        internal WorkflowDesigner wd;

        protected override void OnInitialized(System.EventArgs e)
        {
            base.OnInitialized(e);
            (new DesignerMetadata()).Register();

            wd = new WorkflowDesigner();
            LoadFlow((string)null);

        }

        internal void LoadFlow(string xaml)
        {
            if (wd != null) wd.Context.Items.Unsubscribe<Selection>(SelectionChanged);
            wd = new WorkflowDesigner();
            if (!String.IsNullOrEmpty(xaml))
            {
                Activity act = null;
                //get the definition of the workflow from the function property
                using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(xaml)))
                {
                    act = ActivityXamlServices.Load(new System.Xaml.XamlXmlReader(ms, new System.Xaml.XamlXmlReaderSettings() { }));
                }
                
                wd.Text = xaml;
                wd.Load();
            }
            else
            {
                wd.Load(new ActivityBuilder() { Implementation=new Flow()});
            }
           

            wd.Context.Items.Subscribe<Selection>(SelectionChanged);
            tbc.AssociatedDesigner = wd;

            DesignerBorder.Child = wd.View;
            PropertyBorder.Child = wd.PropertyInspectorView;
        }

        internal Selection currentSelection;

        void SelectionChanged(Selection sc)
        {
            currentSelection = sc;
            if (sc.PrimarySelection != null)
            {
                if (sc.PrimarySelection.ItemType.IsAssignableFrom(typeof(MenuScreen)))
                {
                    WordModel.ScreenTabGroup.IsVisible = true;
                    WordModel.PropertiesTab.IsSelected = true;
                    WordModel.ActionTabGroup.IsVisible = false;
                }
                else
                {
                    WordModel.ScreenTabGroup.IsVisible = false;
                    if (sc.PrimarySelection.ItemType.IsAssignableFrom(typeof(ActionNode)))
                    {
                        WordModel.ActionTabGroup.IsVisible = true;
                       // WordModel.PropertiesTab.IsSelected = true;
                    }
                    else
                    {
                        WordModel.ActionTabGroup.IsVisible = false;
                        if (WordModel.PropertiesTab.IsSelected)
                        {
                            HomeTab.IsSelected = true;
                        }
                    }
                }
            }
        }
        void wd_ModelChanged(object sender, EventArgs e)
        {
            
        }

        #region QAT Serialization

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ribbon.QuickAccessToolBar == null)
            {
                ribbon.QuickAccessToolBar = new RibbonQuickAccessToolBar();
            }

            LoadQatItems(ribbon.QuickAccessToolBar);
        }

        internal void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SaveQatItems(ribbon.QuickAccessToolBar);
        }

        private void LoadQatItems(RibbonQuickAccessToolBar qat)
        {
            if (qat != null)
            {
                if (File.Exists(_qatFileName))
                {
                    XmlReader xmlReader = XmlReader.Create(_qatFileName);
                    QatItemCollection qatItems = (QatItemCollection)XamlReader.Load(xmlReader);
                    xmlReader.Close();
                    if (qatItems != null)
                    {
                        int remainingItemsCount = qatItems.Count;
                        QatItemCollection matchedItems = new QatItemCollection();

                        if (qatItems.Count > 0)
                        {
                            for (int tabIndex = 0; tabIndex < ribbon.Items.Count && remainingItemsCount > 0; tabIndex++)
                            {
                                matchedItems.Clear();
                                for (int qatIndex = 0; qatIndex < qatItems.Count; qatIndex++)
                                {
                                    QatItem qatItem = qatItems[qatIndex];
                                    if (qatItem.ControlIndices[0] == tabIndex)
                                    {
                                        matchedItems.Add(qatItem);
                                    }
                                }

                                RibbonTab tab = ribbon.Items[tabIndex] as RibbonTab;
                                if (tab != null)
                                {
                                    LoadQatItemsAmongChildren(matchedItems, 0, tabIndex, tab, ref remainingItemsCount);
                                }
                            }

                            for (int qatIndex = 0; qatIndex < qatItems.Count; qatIndex++)
                            {
                                QatItem qatItem = qatItems[qatIndex];
                                Control control = qatItem.Instance as Control;
                                if (control != null)
                                {
                                    if (RibbonCommands.AddToQuickAccessToolBarCommand.CanExecute(null, control))
                                    {
                                        RibbonCommands.AddToQuickAccessToolBarCommand.Execute(null, control);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void LoadQatItemsAmongChildren(
                    QatItemCollection previouslyMatchedItems,
                    int matchLevel,
                    int controlIndex,
                    object parent,
                    ref int remainingItemsCount)
        {
            if (previouslyMatchedItems.Count == 0)
            {
                return;
            }
            if (IsLeaf(parent))
            {
                return;
            }

            int childIndex = 0;
            DependencyObject dependencyObject = parent as DependencyObject;
            if (dependencyObject != null)
            {
                IEnumerable children = LogicalTreeHelper.GetChildren(dependencyObject);
                foreach (object child in children)
                {
                    if (remainingItemsCount == 0)
                    {
                        break;
                    }

                    QatItemCollection matchedItems = new QatItemCollection();
                    LoadQatItemIfMatchesControl(previouslyMatchedItems, matchedItems, matchLevel + 1, childIndex, child, ref remainingItemsCount);
                    LoadQatItemsAmongChildren(matchedItems, matchLevel + 1, childIndex, child, ref remainingItemsCount);
                    childIndex++;
                }
            }
            if (childIndex != 0)
            {
                return;
            }

            // if we failed to get any logical children, enumerate the visual ones
            Visual visual = parent as Visual;
            if (visual == null)
            {
                return;
            }
            for (childIndex = 0; childIndex < VisualTreeHelper.GetChildrenCount(visual); childIndex++)
            {
                if (remainingItemsCount == 0)
                {
                    break;
                }

                Visual child = VisualTreeHelper.GetChild(visual, childIndex) as Visual;
                QatItemCollection matchedItems = new QatItemCollection();
                LoadQatItemIfMatchesControl(previouslyMatchedItems, matchedItems, matchLevel + 1, childIndex, child, ref remainingItemsCount);
                LoadQatItemsAmongChildren(matchedItems, matchLevel + 1, childIndex, child, ref remainingItemsCount);
            }
        }

        private void LoadQatItemIfMatchesControl(
                    QatItemCollection previouslyMatchedItems,
                    QatItemCollection matchedItems,
                    int matchLevel,
                    int controlIndex,
                    object control,
                    ref int remainingItemsCount)
        {
            for (int qatIndex = 0; qatIndex < previouslyMatchedItems.Count; qatIndex++)
            {
                QatItem qatItem = previouslyMatchedItems[qatIndex];
                if (qatItem.ControlIndices[matchLevel] == controlIndex)
                {
                    if (qatItem.ControlIndices.Count == matchLevel + 1)
                    {
                        qatItem.Instance = control;
                        remainingItemsCount--;
                    }
                    else if (qatItem.ControlIndices.Count == matchLevel + 2 && qatItem.ControlIndices[matchLevel + 1] == -1)
                    {
                        qatItem.IsSplitHeader = true;
                        Control element = control as Control;
                        if (element != null)
                        {
                            object splitControl = element.Template.FindName("PART_HeaderButton", element);
                            if (splitControl == null)
                            {
                                element.ApplyTemplate();
                                splitControl = element.Template.FindName("PART_HeaderButton", element);
                            }
                            if (splitControl != null)
                            {
                                qatItem.Instance = splitControl;
                            }
                        }
                        remainingItemsCount--;
                    }
                    else
                    {
                        matchedItems.Add(qatItem);
                    }
                }
            }
        }

        private void SaveQatItems(RibbonQuickAccessToolBar qat)
        {
            if (qat != null)
            {
                QatItemCollection qatItems = new QatItemCollection();
                QatItemCollection remainingItems = new QatItemCollection();

                if (qat.Items.Count > 0)
                {
                    for (int qatIndex = 0; qatIndex < qat.Items.Count; qatIndex++)
                    {
                        object instance = qat.Items[qatIndex];
                        bool isSplitHeader = false;

                        if (instance is FrameworkElement)
                        {
                            FrameworkElement element = (FrameworkElement)instance;

                            if (((FrameworkElement)instance).DataContext != null)
                            {
                                instance = ((FrameworkElement)instance).DataContext;
                                isSplitHeader =
                                    (instance is SplitMenuItemData && element is ButtonBase) ||
                                    (instance is SplitButtonData && element is ButtonBase);
                            }
                        }

                        QatItem qatItem = new QatItem(instance, isSplitHeader);
                        qatItems.Add(qatItem);
                        remainingItems.Add(qatItem);
                    }

                    for (int tabIndex = 0; tabIndex < ribbon.Items.Count && remainingItems.Count > 0; tabIndex++)
                    {
                        RibbonTab tab = ribbon.Items[tabIndex] as RibbonTab;
                        SaveQatItemsAmongChildren(remainingItems, tab, tabIndex);
                    }
                }

                XmlWriter xmlWriter = XmlWriter.Create(_qatFileName);
                XamlWriter.Save(qatItems, xmlWriter);
                xmlWriter.Close();
            }
        }

        private bool IsLeaf(object element)
        {
            if ((element is RibbonButton) ||
                (element is RibbonToggleButton) ||
                (element is RibbonRadioButton) ||
                (element is RibbonCheckBox) ||
                (element is RibbonTextBox) ||
                (element is RibbonSeparator))
            {
                return true;
            }

            RibbonMenuItem menuItem = element as RibbonMenuItem;
            if (menuItem != null &&
                menuItem.Items.Count == 0)
            {
                return true;
            }

            return false;
        }

        private void SaveQatItemsAmongChildrenInner(QatItemCollection remainingItems, object parent)
        {
            SaveQatItemsIfMatchesControl(remainingItems, parent);
            if (IsLeaf(parent))
            {
                return;
            }

            int childIndex = 0;
            DependencyObject dependencyObject = parent as DependencyObject;
            if (dependencyObject != null)
            {
                IEnumerable children = LogicalTreeHelper.GetChildren(dependencyObject);
                foreach (object child in children)
                {
                    SaveQatItemsAmongChildren(remainingItems, child, childIndex);
                    childIndex++;
                }
            }
            if (childIndex != 0)
            {
                return;
            }

            // if we failed to get any logical children, enumerate the visual ones
            Visual visual = parent as Visual;
            if (visual == null)
            {
                return;
            }
            for (childIndex = 0; childIndex < VisualTreeHelper.GetChildrenCount(visual); childIndex++)
            {
                Visual child = VisualTreeHelper.GetChild(visual, childIndex) as Visual;
                SaveQatItemsAmongChildren(remainingItems, child, childIndex);
            }
        }

        private void SaveQatItemsAmongChildren(QatItemCollection remainingItems, object control, int controlIndex)
        {
            if (control == null)
            {
                return;
            }

            for (int qatIndex = 0; qatIndex < remainingItems.Count; qatIndex++)
            {
                QatItem qatItem = remainingItems[qatIndex];
                qatItem.ControlIndices.Add(controlIndex);
            }

            SaveQatItemsAmongChildrenInner(remainingItems, control);

            for (int qatIndex = 0; qatIndex < remainingItems.Count; qatIndex++)
            {
                QatItem qatItem = remainingItems[qatIndex];
                int tail = qatItem.ControlIndices.Count - 1;
                qatItem.ControlIndices.RemoveAt(tail);
            }
        }

        private bool SaveQatItemsIfMatchesControl(QatItemCollection remainingItems, object control)
        {
            bool matched = false;
            FrameworkElement element = control as FrameworkElement;
            if (element != null)
            {
                object data = element.DataContext;
                if (data != null)
                {
                    for (int qatIndex = 0; qatIndex < remainingItems.Count; qatIndex++)
                    {
                        QatItem qatItem = remainingItems[qatIndex];
                        if (qatItem.Instance == data)
                        {
                            if (qatItem.IsSplitHeader)
                            {
                                // This is the case of the Header of a SplitButton 
                                // or a SplitMenuItem added to the QAT. Note -1 is 
                                // the sentinel being used to indicate this case.

                                qatItem.ControlIndices.Add(-1);
                            }

                            remainingItems.Remove(qatItem);
                            qatIndex--;
                            matched = true;
                        }
                    }
                }
            }
            return matched;
        }

        #endregion
        
        #region Commands

       
        
      

        internal void ToggleActionProperties()
        {
            if (PropertyBorder.Visibility == System.Windows.Visibility.Visible)
            {
                PropertyBorder.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                PropertyBorder.Visibility = System.Windows.Visibility.Visible;
            }
        }

        internal void ToggleToolbox()
        {
            if (tbc.Visibility == System.Windows.Visibility.Visible)
            {
                tbc.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                tbc.Visibility = System.Windows.Visibility.Visible;
            }
        }

        internal void ChangeFontSize(double? parameter)
        {
            _previousFontSize = 0;
            if (parameter.HasValue)
            {
                RTB.FontSize = parameter.Value;
            }
        }

        internal bool CanChangeFontSize(double? parameter)
        {
            return true;
        }

        internal void PreviewFontSize(double? parameter)
        {
            _previousFontSize = RTB.FontSize;
            if (parameter.HasValue)
            {
                RTB.FontSize = parameter.Value;
            }
        }

        internal void CancelPreviewFontSize()
        {
            if (_previousFontSize > 0)
            {
                RTB.FontSize = _previousFontSize;
                _previousFontSize = 0;
            }
        }

        internal void ChangeFontFace(FontFamily parameter)
        {
            _previousFontFamily = null;
            if (parameter != null)
            {
                RTB.FontFamily = parameter;
            }
        }

        internal bool CanChangeFontFace(FontFamily parameter)
        {
            return true;
        }

        internal void PreviewFontFace(FontFamily parameter)
        {
            _previousFontFamily = RTB.FontFamily;
            if (parameter != null)
            {
                RTB.FontFamily = parameter;
            }
        }

        internal void CancelPreviewFontFace()
        {
            if (_previousFontFamily != null)
            {
                RTB.FontFamily = _previousFontFamily;
                _previousFontFamily = null;
            }
        }

        internal void ChangeTextHighlightColor(Brush parameter)
        {
            _previousTextHighlightBrush = null;
            if (parameter != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.BackgroundProperty, parameter);
            }
        }

        internal bool CanChangeTextHighlightColor(Brush parameter)
        {
            return true;
        }

        internal void PreviewTextHighlightColor(Brush parameter)
        {
            _previousTextHighlightBrush = (Brush)RTB.Selection.GetPropertyValue(TextElement.BackgroundProperty);
            if (parameter != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.BackgroundProperty, parameter);
            }
        }

        internal void CancelPreviewTextHighlightColor()
        {
            if (_previousTextHighlightBrush != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.BackgroundProperty, _previousTextHighlightBrush);
                _previousTextHighlightBrush = null;
            }
        }

        internal void ChangeFontColor(Brush parameter)
        {
            _previousFontBrush = null;
            if (parameter != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, parameter);
            }
        }

        internal bool CanChangeFontColor(Brush parameter)
        {
            return true;
        }

        internal void PreviewFontColor(Brush parameter)
        {
            _previousFontBrush = (Brush)RTB.Selection.GetPropertyValue(TextElement.ForegroundProperty);
            if (parameter != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, parameter);
            }
        }

        internal void CancelPreviewFontColor()
        {
            if (_previousFontBrush != null)
            {
                RTB.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, _previousFontBrush);
                _previousFontBrush = null;
            }
        }

        internal void InsertTable()
        {
            _previousTable = null;

            // Activate the Table Tools contextual tab.
            WordModel.TableTabGroup.IsVisible = true;
            
            // Select first tab of contextual tab group.
            WordModel.TableTabGroup.TabDataCollection[0].IsSelected = true;
        }

        internal bool CanInsertTable()
        {
            return true;
        }

        internal void PreviewInsertTable(RowColumnCount parameter)
        {
            if (parameter != null)
            {
                _previousTable = CreateTable(parameter.RowCount, parameter.ColumnCount);
                flowDoc.Blocks.Add(_previousTable);
            }
        }

        internal void CancelPreviewInsertTable()
        {
            if (_previousTable != null)
            {
                flowDoc.Blocks.Remove(_previousTable);
                _previousTable = null;
            }
        }

        private void RTB_SelectionChanged(object sender, RoutedEventArgs e)
        {
            TextPointer start = RTB.Selection.Start;
            if (start != null)
            {
                // Activate/Deactivate the Table Tools contextual tab.
                // A quick way to know if caret is in a Table, 
                // since we know that Tables are added directly to the FlowDocument and wont be in a Paragraph
                // Note: We dont want to change Tab selection here.
                WordModel.TableTabGroup.IsVisible = start.Paragraph == null; 
            }
        }

        Table CreateTable(int rows, int columns)
        {
            Table _table = new Table();
            _table.CellSpacing = 0;
            _table.BorderBrush = Brushes.Black;
            _table.BorderThickness = new Thickness(1,1,0,0);

            _table.RowGroups.Add(new TableRowGroup());
            TableRow currentRow = null;
            for (int i = 0; i < rows; i++)
            {
                currentRow = new TableRow();
                _table.RowGroups[0].Rows.Add(currentRow);
                for (int j = 0; j < columns; j++)
                {
                    TableCell cell = new TableCell() { BorderThickness = new Thickness(0, 0, 1, 1), BorderBrush = Brushes.Black };
                    currentRow.Cells.Add(cell);
                }
            }
            return _table;
        }


        #endregion Commands

        #region Data

        private FontFamily _previousFontFamily = null;
        private double _previousFontSize = 0;
        private Brush _previousTextHighlightBrush = null;
        private Brush _previousFontBrush = null;
        private Table _previousTable = null;
        private const string _qatFileName = "UserControlWord_QAT.xml";

        #endregion Data

        internal IFlow CurrentFlow { get; set; }

        internal void LoadFlow(Core.Contracts.IFlow startFlow)
        {
            if (startFlow != null && !String.IsNullOrWhiteSpace(startFlow.XamlDefinition))
            {
                LoadFlow(startFlow.XamlDefinition);
            }
        }
    }
}
