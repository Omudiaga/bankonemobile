﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Activities.Presentation;
using System.Activities.Core.Presentation;
using System.Activities;
using System.Activities.Statements;
using System.IO;
using System.Configuration;
using System.Reflection;
using Microsoft.Windows.Controls.Ribbon;
using System.Diagnostics;

namespace Messiah.WorkflowManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        public MainWindow()
        {
            using (var client = new BankOneMobile.FlowDesigner.BankOneMobileServices.FlowServiceClient())
            {
                try
                {
                    // lblStatus.Text = "Loading Flows...";
                    BankOneMobile.SessionNodeDesigners.Actions.InitiateSessionDesigner.TheFlows =
                        BankOneMobile.SessionNodeDesigners.Actions.GotoFlowDesigner.TheFlows =
                        client.GetAllFlows();
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Could not Load Flows: " + ex.Message);
                    MessageBox.Show("Could not Load Flows: " + ex.Message);
                }
                finally
                {
                    client.Close();
                }
            }
            InitializeComponent();
            //Application.Current.Properties["WordControlRef"] = new WeakReference(WordControl);                     
           
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //Get xaml from designer
            //wd.Flush();
            //Clipboard.SetText(wd.Text);
            MessageBox.Show("Successfully saved!", "Result", MessageBoxButton.OK, MessageBoxImage.Information);
            
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
                   
             
            
        }

        private void RibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //lblStatus.Text = "Connecting to BankOneMobile Server...";
            
          //  lblStatus.Text = "Flows Loaded Successfully...";
            //MainWindow main = new MainWindow();
            //main.Closed += new EventHandler(main_Closed);
            //main.ShowActivated = true;
            //this.Hide();
            //main.ShowDialog();
            //this.Close(); 
        }
    }
}

