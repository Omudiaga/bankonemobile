﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.Models
{
   public class AddHostModel
    {
       public virtual Host TheHost { set; get; }
       public virtual string NextUrl { set; get; }
       public virtual DataSourceModel TheDatasource { set; get; }

    }
}
