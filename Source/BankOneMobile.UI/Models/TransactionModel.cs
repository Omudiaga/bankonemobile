﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.Models
{
   public class TransactionModel
    {
       public virtual string status{get;set;}
       public virtual string transactionType { get; set; }
       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual List<ITransaction> Transactions { set; get; }
       public virtual CashInTransactionType TheCashInModel { get; set; }
       public virtual MiniStatementTransactionType TheMiniStatementModel { get; set; }
       public virtual PinChangeTransactionType ThePinChangeModel { get; set; }
       public virtual BalanceInquiryTransactionType TheBalanceInquiryModel { get; set; }
       public virtual CashOutTransactionType TheCashOutModel { get; set; }
       public virtual ITransaction SelectedTransaction { set; get; }
      
       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }
       
       
    }
   public class CashInTransactionModel
   {
       public virtual string status { get; set; }
       
       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string AgentCode { get; set; }
       public virtual DateTime Date { get; set; }
       public virtual string CustomerPhoneNumber { get; set; }
       public virtual string AgentPhoneNumber { get; set; }

       public virtual List<CashInTransactionType> Transactions { set; get; }


       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }


   }
    public class RegistrationTransactionModel
    {
        public virtual string PhoneNumber { get; set; }
        public virtual string Gender { get; set; }
        public virtual DateTime? DateFrom { get; set; }
        public virtual DateTime? DateTo { get; set; }
        public virtual string InstitutionCode { get; set; }
        public virtual string AgentCode { get; set; }

        public virtual List<IRegistrationTransactionType> Transactions { set; get; }


        public virtual string DisplayMessage { get; set; }
        public virtual string ReturnURL { get; set; }
    }

   public class CashOutTransactionModel
   {
       public virtual string status { get; set; }

       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string AgentCode { get; set; }

       public virtual List<CashOutTransactionType> Transactions { set; get; }


       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }



       public bool DisplaySummary { get; set; }

       public decimal TotalSuccessfulAmount { get; set; }

       public long SuccessfulTransactions { get; set; }

       public long TotalTransactions { get; set; }
   }
   public class RechargeTransactionModel 
   {
       public virtual string status { get; set; }
       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string MerchantName { get; set; }
      
      
       
       public virtual string Beneficiary { get; set; }
      
       public virtual decimal Amount
       {
           get;
           set;
       }
       public virtual string TransactionReference { get; set; }
       public virtual string PaymentReference { get; set; }


       public virtual List<RechargeTransactionType> Transactions { set; get; }


   }
   public class BillsPaymentTransactionModel
   {
       public virtual string status { get; set; }
       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string MerchantName { get; set; }



       public virtual string CustomerID { get; set; }

       public virtual decimal Amount
       {
           get;
           set;
       }
       public virtual string TransactionReference { get; set; }
       public virtual string PaymentReference { get; set; }


       public virtual List<BillsPaymentTransactionType> Transactions { set; get; }


   }
   public class CommercialBankFundsTransferTransactionModel
   {
       public virtual string status { get; set; }
       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }public virtual string Token { get; set; }
       public virtual string BankName { get; set; }
       
       public virtual string AccountNumber { get; set; }
       
       public virtual decimal Amount { get; set; }
       
       public virtual string TransactionReference { get; set; }
       public virtual string PaymentReference { get; set; }
       public virtual string BeneficiaryLastName { get; set; }

       public virtual List<ICommercialBankFundsTransferTransactionType> Transactions { set; get; } 





       public virtual string BeneficiaryPhoneNumber
       {
           get;
           set;
       }


   }
   public class OnNetFundsTransferModel
   {
       public virtual string status { get; set; }

       public virtual string FromAccount { get; set; }
       public virtual DateTime? dateFrom { get; set; }
       public virtual DateTime? dateTo { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string AgentCode { get; set; }
       public virtual string receiver { get; set; }
       public virtual string receiverInsCode { get; set; }

       public virtual List<LocalFundsTransferTransactionType> Transactions { set; get; }


       public virtual string DisplayMessage { get; set; }
       public virtual string ReturnURL { get; set; }


   }
}
