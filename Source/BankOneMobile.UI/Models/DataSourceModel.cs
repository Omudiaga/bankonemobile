﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.UI.Models
{
   public class DataSourceModel
    {
       public virtual string ClassTypeName { set; get; }
       public virtual string AssemblyName { set; get; }
       public virtual string FriendlyName { set; get; }
    }
}
