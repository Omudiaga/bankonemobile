﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
   public class AddMobileAccountModel
    {
       public MobileAccount TheMobileAccount { set; get; }
       public string NextUrl { set; get; }
       public string CustomerID { set; get; }
       public List<ILinkingBankAccount> CustomerAccounts { set; get; }
       public Host TheHost { set; get; }
    }
}
