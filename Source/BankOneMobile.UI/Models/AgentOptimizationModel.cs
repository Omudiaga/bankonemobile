﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.UI.Models
{
    public class AgentOptimizationModel
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string AgentName { get; set; }
        public virtual BankOneMobile.Services.BranchServiceRef.Branch TheBranch { get; set; }
        public virtual string Branch { get; set; }
        public virtual int CdiIndex { get; set; }
        public virtual double ShortestAgentPath { get; set; }
        public virtual double OptimizedTotalClusterDistance { get; set; }
        public virtual DateTime DateGenerated { get; set; }
        public virtual AgentOptimization TheAgentOptimization { get; set; }

        public virtual List<AgentOptimization> TheAgentOptimizations { get; set; }
        public virtual List<TheAgentOptimizationModel> TheAgentOptimizationList { get; set; }

        public virtual string DisplayMessage { get; set; }
    }

    public class TheAgentOptimizationModel
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string AgentName { get; set; }
        public virtual BankOneMobile.Services.BranchServiceRef.Branch TheBranch { get; set; }
        public virtual string Branch { get; set; }
        public virtual int CdiIndex { get; set; }
        public virtual double ShortestAgentPath { get; set; }
        public virtual double OptimizedTotalClusterDistance { get; set; }
        public virtual DateTime DateGenerated { get; set; }

    }
}
