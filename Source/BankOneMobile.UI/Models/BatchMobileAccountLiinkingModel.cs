﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.UI.Models
{
    public class BatchMobileAccountLiinkingModel
    {
        [Newtonsoft.Json.JsonIgnore]
        public object UploadedFile { get; set; }

        public string DisplayMessage { set; get; }
        public List<BatchMobileAccountLinkingGroup> AccountLinkingDetailGroups { set; get; }
    }

    public class BatchMobileAccountLinkingGroup
    {
        public string CustomerID { set; get; }
        public string MobileNumber { set; get; }
        public string AccountNumber { set; get; }
    }
}
