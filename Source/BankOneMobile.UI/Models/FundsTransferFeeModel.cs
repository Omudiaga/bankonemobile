﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.UI.Models
{
    public class FundsTransferFeeModel
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string TransactionTypeName { set; get; }
        /// <summary>
        /// True for Flat, False for Percent
        /// </summary>
        public virtual bool IsFlat { set; get; }
        public virtual bool IsPercent { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual int VATInPercentage { set; get; }
        public virtual FundsTransferFee TheFundsTransferFee { get; set; }

        public virtual List<IFundsTransferFee> TheFundsTransferFees { get; set; }

        public virtual string DisplayMessage { get; set; }
    }
}
