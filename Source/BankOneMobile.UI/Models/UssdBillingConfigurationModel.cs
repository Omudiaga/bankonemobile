﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
    public class UssdBillingConfigurationModel
    {
        public string Name { get; set; }
        public List<IUssdBillingConfiguration> ConfigurationList { get; set; }

        public UssdBillingConfigurationModel()
        {
            ConfigurationList = new List<IUssdBillingConfiguration>();
        }
    }
}
