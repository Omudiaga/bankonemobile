﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.Models
{
    public class ThirdPartyMobileModel
    {
        public IList<IThirdPartyMobileProfile> ThirdPartyMobileProfile { get; set; }
        public virtual string PhoneNumber { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual string LastName { get; set; }
        public virtual string OtherName { get; set; }
        public virtual string Gender { get; set; }
        public virtual string Status { get; set; }
        public virtual string InstitutionCode { get; set; }

    }
}
