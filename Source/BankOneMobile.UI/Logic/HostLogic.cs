﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.UI.Models;
using System.IO;
using System.Reflection;
using System.Configuration;
using BankOneMobile.CustomerInfo.Interface;

namespace BankOneMobile.UI.Logic
{
   public class HostLogic
    {
        public static Host SaveHost(Host theHost)
        {

            return HostSystem.SaveHost(theHost) as Host;

        }

        public static Host UpdateHost(Host theHost)
        {

            return HostSystem.UpdateHost(theHost) as Host;

        }

        public static Host GetHost(long id)
        {
            return HostSystem.GetHost(id) as Host;

        }

        public static List<string> Statuslist()
        {
            List<string> lst = new List<string>();
            lst.Add("True");
            lst.Add("False");
            return lst;

        }

        public static IList<DataSourceModel> GetDataSources()
        {
            string path = ConfigurationManager.AppSettings["CustomerDatasource"];
            DirectoryInfo di = new DirectoryInfo(path);

            FileInfo[] file = di.GetFiles();

            IList<DataSourceModel> datasource = new List<DataSourceModel>();
            foreach (FileInfo f in file.Where(x => x.Extension == ".dll"))
            {

                Assembly theAssembly = Assembly.LoadFile(f.FullName);
                foreach (var dataSourceType in theAssembly.GetExportedTypes().Where(x => !x.IsAbstract && x.IsClass && x.GetInterfaces().Any(y => y.Equals(typeof(BankOneMobile.CustomerInfo.Interface.ICustomerInfo)))))
                {

                    datasource.Add(new DataSourceModel() { ClassTypeName = dataSourceType.AssemblyQualifiedName, AssemblyName = f.Name, FriendlyName = GetFriendlyName(dataSourceType.AssemblyQualifiedName) });
                }
            }
            return datasource;
        }

       public static string GetFriendlyName (string assemblyName)
       {
           string friendlyName = string.Empty;
           string[] parts = assemblyName.Split(',');
           if (parts.Count() >= 2)
           {
               friendlyName = parts[1];

           }
           return friendlyName;
      }

        public static List<IHost> Search(string name, string status)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<IHost> cardProfiles = new List<IHost>();
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(name.Trim()))
            {
                dic.Add("Name", name);

            }

          

            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                bool isActive = Convert.ToBoolean(status);
                dic.Add("IsActive", isActive);

            }

            cardProfiles = HostSystem.FindHosts(dic);
            return cardProfiles;

        }

        internal static bool ValidateEntry(Host x)
        {
            return true;
        }
    }
}
