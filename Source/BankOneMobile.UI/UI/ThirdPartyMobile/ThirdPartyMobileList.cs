﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using AppZoneUI.Framework;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core;
using BankOneMobile.Core.Implementations;
using BankOneMobile.UI.Models;
using BankOneMobile.Services;

namespace BankOneMobile.UI.UI.ThirdPartyMobile
{
    public class ThirdPartyMobileList : AppZoneUI.Framework.EntityUI<ThirdPartyMobileModel>
    {

        public ThirdPartyMobileList()
        {
            
            UseFullView();

            WithTitle("View Third Party Mobile Profile");
            AddSection()
                .IsFramed()
                .WithTitle("Search Profile")
                .WithColumns(new List<Column>() 
                { 
                    new Column(new List<IField>() { 
                        Map(x => x.AccountNumber).AsSectionField<TextBox>().WithLength(20).LabelTextIs("Account Number"),
                        Map(x => x.PhoneNumber).AsSectionField<TextBox>().WithLength(16).LabelTextIs("Phone Number"),
                        Map(x => x.LastName).AsSectionField<TextBox>().WithLength(16).LabelTextIs("Lastname"),
                }), 
                new Column(new List<IField>() 
                {
                       //Map(x => x.Status).AsSectionField<DropDownList>().Of<Status>(Utility.GetEnums<Status>).ListOf(x => x.ToString(), x => x).AcceptBlank("Select").LabelTextIs("Status"),
                        AddSectionButton().WithText("Search").UpdateWith(x => x)
                        .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.Magnifier)),

                        AddSectionButton()
                        .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                        .WithText("Reset").NoValidate()
                        .SubmitTo(x => false)
                        .OnFailureRedirectTo(("ThirdPartyMobileProfileList.aspx"))
                })
                })
                .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.LaptopMagnify));

            HasMany(x => x.ThirdPartyMobileProfile)
                .As<Grid>()
                .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.LaptopMagnify))
                .ApplyMod<ViewDetailsMod>(mod => mod.Popup<ThirdPartyMobileDetails>("ThirdParty Mobile Profile Details"))
                .Of<ThirdPartyMobileProfile>()
                .WithColumn(x => x.AccountNumber)
                .WithColumn(x => x.PhoneNumber)
                .WithColumn(x => x.LastName)
                .WithColumn(x => x.OtherName)
                .WithColumn(x => x.Gender)
                .WithColumn(x => x.Status.ToString(), "Status")
                .WithColumn(x => x.InstitutionCode)
                .WithAutoLoad()
                .WithRowNumbers()
                .IsPaged<ThirdPartyMobileModel>(10, (x, y) =>
                {   
                    int totalCount = -1;
                    int numOfResults = 0;
                    x.ThirdPartyMobileProfile = new ThirdPartyMobileProfileSystem().Search(x.AccountNumber,x.PhoneNumber, x.LastName, x.Status, x.InstitutionCode, y.Start, y.Limit, out numOfResults);
                    
                    y.TotalCount = numOfResults;
                    return x;
                }).LabelTextIs("ThirdParty Mobile profiles");
        }

    }
}
