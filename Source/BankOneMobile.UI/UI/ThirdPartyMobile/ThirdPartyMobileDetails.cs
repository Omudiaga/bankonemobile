﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core;
using BankOneMobile.Core.Implementations;
using AppZoneUI.Framework;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Services;

namespace BankOneMobile.UI.UI.ThirdPartyMobile
{
    public class ThirdPartyMobileDetails : EntityUI<ThirdPartyMobileProfile>
    {
        public bool edit;
        public ThirdPartyMobileDetails()
        {
            UseFullView();

            WithTitle("Thirdparty Mobile Profile Details");
            AddSection()
                .IsFormGroup()
                .WithTitle("Profile Details")
                .WithFields(new List<IField>()
                {
                        //branchdropdown
                        Map(x => x.AccountNumber).AsSectionField<TextLabel>().LabelTextIs("Account Number"),
                        Map(x => x.PhoneNumber).AsSectionField<TextLabel>().LabelTextIs("Phone Number"),
                        Map(x => x.LastName).AsSectionField<TextLabel>().LabelTextIs("LastName"),
                        Map(x => x.OtherName).AsSectionField<TextLabel>().LabelTextIs("Other Names"),
                        Map(x => x.Gender).AsSectionField<TextLabel>().LabelTextIs("Gender"),
                        Map(x => x.Status).AsSectionField<TextLabel>().LabelTextIs("Status"),


                }).ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.LaptopMagnify));


            AddSection()
                .IsFormGroup()
                .WithTitle("Edit Profile")
                .WithFields(new List<IField>(){
           // Map(x => x.Status).AsSectionField<TextBox>().WithLength(16).LabelTextIs("Status").Required(),
            
            })
            .ApplyMod<VisibilityMod>(mod => mod.SetVisibility<ThirdPartyMobileProfile>(x => edit))
            .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.User));

            AddButton().WithText(x => x.Status.ToString() == "Active" ? "HotList" : "Activate")
                .SubmitTo(x =>
                {
                    try
                    {
                        x.Status = x.Status.ToString() == "Active" ? "HotList" : "Active";
                        var obj = new ThirdPartyMobileProfileSystem().Update(x);
                        if(obj != null && !String.IsNullOrWhiteSpace(obj.Status))
                        {
                            return true;
                        } 
                    }
                    catch
                    {
                        
                    }
                    return false;
                }).ApplyMod<VisibilityMod>(mod => mod.SetVisibility<ThirdPartyMobileProfile>(x => !edit));

            AddButton().WithText("Edit Profile")
               .UpdateWith(x =>
               {
                   edit = true;
                   return x;
               })
               .ApplyMod<VisibilityMod>(mod => mod.SetVisibility<ThirdPartyMobileProfile>(x => !edit))
               .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.UserEdit));

            AddButton().WithText("Update")
                .UpdateWith(x =>
                {
                    try
                    {
                        new ThirdPartyMobileProfileSystem().Update(x);
                        edit = false;
                        return x;
                    }
                    catch
                    {
                        return null;
                    }
                }).ConfirmWith("Are you sure you want to continue?")
                .ApplyMod<VisibilityMod>(mod => mod.SetVisibility<ThirdPartyMobileProfile>(x => edit))
                .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.Magnifier));

           
        }
    }
}
