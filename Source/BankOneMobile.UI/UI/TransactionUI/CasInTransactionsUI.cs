﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.UI.TransactionUI
{
     public class CasInTransactionsUI : AppZoneUI.Framework.EntityUI<CashInTransactionModel>
    {
          Dictionary<string, string> Institutions;
          Dictionary<string, string> Tellers = AgentLogic.GetTellersByInstitution(UserLogic.GetLoggedOnUserInstCode());
          bool appzoneUser = string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode());

         public CasInTransactionsUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem().GetAllInstitutionNames(source);
            }
            UseFullView();
            WithTitle("Cash Deposit Details");
             
            AddNorthSection()
                
                
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))

              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.dateFrom).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("From"),
                            
                            
                             Map(x => x.AgentCode).AsSectionField<DropDownList>()
                        .Of<string>(Tellers.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x =>Tellers[x], x => x).LabelTextIs("Mobile Teller").ApplyMod<VisibilityMod>(x => x.SetVisibility<CashInTransactionModel>(y =>!appzoneUser).SetDefaultValue<CashInTransactionModel>(!appzoneUser)),
                       Map(x=>x.FromAccount).AsSectionField<ITextBox>().LabelTextIs("Mobile Teller Phone No.").ApplyMod<VisibilityMod>(x => x.SetVisibility<CashInTransactionModel>(y =>appzoneUser).SetDefaultValue<CashInTransactionModel>(appzoneUser)),
                           
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateTo).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("To"),
                             Map(x=>x.CustomerPhoneNumber).AsSectionField<ITextBox>(),
                             
                         
                    
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.status).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.TransactionStatusList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                        Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<CashInTransactionModel>(y =>appzoneUser).SetDefaultValue<CashInTransactionModel>(appzoneUser)),
                          AddSectionButton()
                .WithText("Search").NoValidate()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x=>
                    {
                                              
                        
                        return x;
                    }
                ),
                 AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("CashInTransactionReport.aspx"))
                }),
              }
              );

            HasMany(x => x.Transactions).AsCenter<Grid>()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))
                .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<CashInDetailUI>("Cash In Transaction Details"))
                .Of<ICashInTransactionType>()
            .WithRowNumbers()
              .IsPaged<CashInTransactionModel>(10, (x, e) =>
              {
                  int totalCount = -1;
                  decimal amt = 0M;
                  string instCode = string.Empty;
                  try
                  {
                      instCode = UserLogic.GetLoggedOnUserInstCode();
                      x.InstitutionCode = string.IsNullOrEmpty(instCode) ? x.InstitutionCode : instCode;
                      using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                      {
                          x.Transactions = new CashInTransactionTypeSystem().Search(source, x.FromAccount, x.InstitutionCode, x.AgentCode, x.CustomerPhoneNumber, x.dateFrom, x.dateTo, x.status, e.Start, e.Limit, out totalCount, out amt);
                      }
                  }
                  catch (Exception ex)
                  {
                      x.DisplayMessage = ex.Message;
                  }
                  e.TotalCount = totalCount;

                  return x;
              })





              .WithColumn(x => x.TheBankAccount.TheMobileAccount.MobilePhone, "Customer Phone No")
              .WithColumn(x => x.TheTransaction.FromPhoneNumber, "Mobile Teller Phone No")
              .WithColumn(x => x.TheTransaction.Date)
              .WithColumn(x => x.TheTransaction.StringAmount, "Amount(=N=)")
              .WithColumn(x => x.TheTransaction.Status)

             // .WithColumn(x => x.AgentCode,"Mobile Teller Name")



              .WithColumn(x => x.TheTransaction.StatusDetails, "Details")
                //, "MobileAccountDetail.aspx?id={0}", x => x.ID)
             .WithColumn(x => Institutions[x.TheTransaction.FromInstitutionCode], "Institution").ApplyMod<VisibilityMod>(x => x.SetVisibility<CashInTransactionModel>(y => appzoneUser).SetDefaultValue<CashInTransactionModel>(appzoneUser));
                

                 


               
        }
     }
    }



