﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.UI.UI.HostUI
{
    class HostListUI: EntityUI<HostModel>
    {
        public HostListUI()
        {
            AddSection()
              .WithTitle("Host Search")
              .WithFields(new List<IField>()
                {
                    Map(x => x.Name).AsSectionField<ITextBox>(),
                    Map(x=>x.IsActive).AsSectionField<DropDownList>()
                        .Of<string>(HostLogic.Statuslist()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Active")
                        ,
                     AddSectionButton()
                        .WithText("Search")
                        .UpdateWith(x=>
                            {
                                x.Hosts = HostLogic.Search(x.Name, x.IsActive);
                                return x;
                            })
                });
            Map(x => x.Hosts).As<Grid>()
              .Of<IHost>(new List<IHost>())
              .WithColumn(x => x.Name, "HostDetail.aspx?ItemID={0}", x => x.ID)
              .WithColumn(x => x.ClassType)
              .WithColumn(x => x.IsActive, "Is Active").IsPaged(10);
              ;

        }
    }
}
