﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using BankOneMobile.UI.Models;
using BankOneMobile.Core.Implementations;
using BankOneMobile.UI.Logic;

namespace BankOneMobile.UI.UI.HostUI
{
    public class AddEditHostUI : EntityUI<AddHostModel>
    {
        public AddEditHostUI()
        {

           
                
                PrePopulateFrom<Host>(x =>
                {
                    AddHostModel model = new AddHostModel();
                    model.TheHost = HostLogic.GetHost(x.ID);
                    if (model.TheHost != null)
                    {
                        model.TheDatasource = new DataSourceModel();
                        model.TheDatasource.AssemblyName = model.TheHost.DllPath;
                        model.TheDatasource.ClassTypeName = model.TheHost.ClassType;
                        model.TheDatasource.FriendlyName = HostLogic.GetFriendlyName(model.TheHost.ClassType);

                    }
                    return model;
                }).Using(x => x.ID, "itemID");
                ;

          
           
         
                AddSubControl<AddHostUI>(x => x.TheHost);

                AddSection()
               .WithTitle("DataSource Information")
                .WithFields(new List<IField>()
                {
                    Map(x=> x.TheDatasource).AsSectionField<DropDownList>()
                      .Of<DataSourceModel>(HostLogic.GetDataSources())
                      .ListOf(d=> d.FriendlyName, d=>d.ClassTypeName)
                                      
                }).StretchFields(30);
                AddButton()
                 .WithText("Save")
                 .SubmitTo(x =>
                 {
                     bool isSubmitted = HostLogic.ValidateEntry(x.TheHost);
                     if (isSubmitted)
                     {
                         if (x.TheHost.ID == 0)
                         {
                             if (x.TheDatasource != null)
                             {
                                 x.TheHost.ClassType = x.TheDatasource.ClassTypeName;
                                 x.TheHost.DllPath = x.TheDatasource.AssemblyName;

                             }
                             x.TheHost = HostLogic.SaveHost(x.TheHost);
                             x.NextUrl = "AddEditHost.aspx";
                         }
                         else
                         {
                             x.TheHost = HostLogic.UpdateHost(x.TheHost);
                             x.NextUrl = "HostDetail.aspx?ItemID=" + x.TheHost.ID.ToString();
                         }
                         isSubmitted = x.TheHost.ID < 1 ? false : true;
                        
                     }
                     return true;
                 }).OnSuccessDisplay(string.Format("Host has been Saved", ""))
                 .OnSuccessRedirectTo("{0}", x => x.NextUrl);
         
           
        }
    }

    public class AddHostUI : EntityUI<Host>
    {
        public AddHostUI()
        {
            AddSection()
            .WithTitle("Host Information")
             .WithFields(new List<IField>()
                {
                     Map(x => x.Name).AsSectionField<ITextBox>().Required()
                                      
                }).StretchFields(30); 

        }
    }
    
}
