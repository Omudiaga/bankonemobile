﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.UI.UI.TransactionUI
{
    public class CashInDetailUI : AppZoneUI.Framework.EntityUI<CashInTransactionType>,ITransactionTypeUI
    {
        public CashInDetailUI()
        {
            PrePopulateFrom<CashInTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    CashInTransactionType ct =
                     CashInTransactionTypeSystem.GetCashInTransactionType(source, x.ID) as CashInTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);
            
            AddSection()
                .IsFormGroup()
                .WithTitle("Cash Deposit Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                         Map(x=>x.TheBankAccount.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Customer's Phone No."),    
                             Map(x=>x.TheBankAccount.BankAccount).AsSectionField<ITextLabel>().LabelTextIs("Customer's  Account No"),
                             Map(x=>x.TheBankAccount.CoreBankingNames).AsSectionField<ITextLabel>().LabelTextIs("Customer's  Account Name"),
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>MobileAccountLogic.AgentName(x.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Name"),
                              Map(x=>MobileAccountLogic.AgentPhoneNumber(x.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Phone Number"),
                             
                        }
                      )
              });

            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(), 
                            Map(x=>x.TheTransaction.StringAmount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),
                        }),
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>x.TheTransaction.Date).AsSectionField<ITextLabel>(), 
                        })
              });

             AddSection()
                .IsFormGroup()
                .WithTitle("Institution")
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<CashInTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                    .Hide<CashInTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))                                                                                                                                                                                                                                                                                                                                                                                                                                                          
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              });
        }

        

    }

    public class BalanceEnquiryDetailUI : AppZoneUI.Framework.EntityUI<BalanceInquiryTransactionType>, ITransactionTypeUI
    {

        public BalanceEnquiryDetailUI()
        {

            PrePopulateFrom<BalanceInquiryTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    BalanceInquiryTransactionType ct =
                      new BalanceInquiryTransactionTypeSystem(source).GetBalanceInquiryTransactionType(x.ID) as BalanceInquiryTransactionType;
                    return ct;
                }
            });//.Using(x => x.ID);

            AddSection()
                .IsFormGroup()
                .WithTitle("Balance Enquiry Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Customer's Phone No."),    
                             Map(x=>x.TheTransaction.From).AsSectionField<ITextLabel>().LabelTextIs("Customer's  Account No"),                             
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>MobileAccountLogic.AgentName(x.TheTransaction.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Name"),
                              Map(x=>MobileAccountLogic.AgentPhoneNumber(x.TheTransaction.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Phone Number"),
                             
                        })
              });

            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),   
                           Map(x=>x.TheTransaction.Date).AsSectionField<ITextLabel>(), 
                        }),
                        new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),                 
                        })
              });

            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<BalanceInquiryTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
               .Hide<BalanceInquiryTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))  
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }),  
              });




        }



    }
    public class MiniStatementDetailUI : AppZoneUI.Framework.EntityUI<MiniStatementTransactionType>, ITransactionTypeUI
    {

        public MiniStatementDetailUI()
        {
            PrePopulateFrom<MiniStatementTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    MiniStatementTransactionType ct =
                       MiniStatementTransactionTypeSystem.GetMiniStatementTransactionType(source, x.ID) as MiniStatementTransactionType;
                    return ct;

                }
            }).Using(x => x.ID);
            
            

            AddSection()
                .IsFormGroup()
             .WithTitle("Mini Statement Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                          Map(x=>x.TheTransaction.Date).AsSectionField<ITextLabel>(), 
                     
                   
                  
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                                          
                  //Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Institution").,
                   
                 
                        })});

            AddSection()
                .IsFormGroup()
                .WithTitle("Transaction Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                         Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Customer's Phone No."),    
                             Map(x=>x.TheTransaction.From).AsSectionField<ITextLabel>().LabelTextIs("Customer's  Account No"),
                             
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>MobileAccountLogic.AgentName(x.TheTransaction.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Name"),
                              Map(x=>MobileAccountLogic.AgentPhoneNumber(x.TheTransaction.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Mobile Teller's Phone Number"),
                             
                        }
                      )
              }
              );

            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<MiniStatementTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                   .Hide<MiniStatementTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))  
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              }
             );




        }



    }
    public class PINChangeDetailUI : AppZoneUI.Framework.EntityUI<PinChangeTransactionType>, ITransactionTypeUI
    {

        public PINChangeDetailUI()
        {
            PrePopulateFrom<PinChangeTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    PinChangeTransactionType ct =
                        PinChangeTransactionTypeSystem.GetPinChangeTransactionType(source, x.ID) as PinChangeTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);
            
            AddSection()
                .IsFormGroup()
                .WithTitle("Pin Change Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                         Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Customer's Phone No."),    
                            
                             
                        }
                      ),   
              });

            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                           Map(x=>x.TheTransaction.Date).AsSectionField<ITextLabel>(), 
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                        })});

            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<PinChangeTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                   .Hide<PinChangeTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))  
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
              });
        }



    }
    public class CashOutDetailUI : AppZoneUI.Framework.EntityUI<CashOutTransactionType>
    {

        public CashOutDetailUI()
        {
            PrePopulateFrom<CashOutTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    CashOutTransactionType ct =
                     CashOutTransactionTypeSystem.GetCashOutTransactionType(source, x.ID) as CashOutTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);
            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                   
                   Map(x=>x.TheTransaction.FromPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),  
                   Map(x=>MobileAccountLogic.MobileAccountName(x.TheTransaction.FromPhoneNumber,false)).AsSectionField<ITextLabel>().LabelTextIs("Account Holder"),
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>x.TheTransaction.StringAmount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),                                 
                  
                   
                        })});
            AddSection()
                .IsFormGroup()
                .WithTitle("Cash Withdrawal Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.AgentCode).AsSectionField<ITextLabel>(),              
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>MobileAccountLogic.AgentName(x.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Agent Details"),
                        }
                      )
              }
              );

            AddSection()
                .IsFormGroup()
                .WithTitle("Institution")
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<CashOutTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                    .Hide<CashOutTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              });
        }
    }
    public class RechargeDetailUI : AppZoneUI.Framework.EntityUI<RechargeTransactionType>
    {

        public RechargeDetailUI()
        {
            PrePopulateFrom<RechargeTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    RechargeTransactionType ct =
                     RechargeTransactionTypeSystem.GetRechargeTransactionType(source, x.ID) as RechargeTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);
            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                   
                   Map(x=>x.TheTransaction.FromPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),  
                   Map(x=>MobileAccountLogic.MobileAccountName(x.TheTransaction.FromPhoneNumber,false)).AsSectionField<ITextLabel>().LabelTextIs("Account Holder"),
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>x.TheTransaction.StringAmount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),                           
                  
                   
                        })});
            AddSection()
                .IsFormGroup()
                .WithTitle("Recharge Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.Beneficiary).AsSectionField<ITextLabel>(),           
                             Map(x=>x.MerchantName).AsSectionField<ITextLabel>().LabelTextIs("Network"), 
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                              
                             Map(x=>x.PaymentReference).AsSectionField<ITextLabel>(),
                             Map(x=>x.TransactionReference).AsSectionField<ITextLabel>(),
                             
                        }
                      )
              }
              );
            AddSection()
                .IsFormGroup()
                .WithTitle("Institution")
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<RechargeTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                    .Hide<RechargeTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
              });



        }


    }
    public class BillsPaymentDetailUI : AppZoneUI.Framework.EntityUI<BillsPaymentTransactionType>
    {
        
        public BillsPaymentDetailUI()
        {
           
            PrePopulateFrom<BillsPaymentTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    BillsPaymentTransactionType ct =
                     BillsPaymentTransactionTypeSystem.GetBillsPaymentTransactionType(source, x.ID) as BillsPaymentTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);


            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                   
                   Map(x=>x.TheTransaction.FromPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),  
                   
                  Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Institution"),
                   Map(x=>MobileAccountLogic.MobileAccountName(x.TheTransaction.FromPhoneNumber,false)).AsSectionField<ITextLabel>().LabelTextIs("Account Holder"),
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>x.TheTransaction.StringAmount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),              
                  
                   
                        })});

            AddSection()
                .IsFormGroup()
                .WithTitle("Bill Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.CustomerID).AsSectionField<ITextLabel>(),           
                             Map(x=>x.MerchantName).AsSectionField<ITextLabel>().LabelTextIs("Merchant"), 
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                              
                             Map(x=>x.PaymentReference).AsSectionField<ITextLabel>(),
                             Map(x=>x.TransactionReference).AsSectionField<ITextLabel>(),
                             
                        })
              });
            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<BillsPaymentTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                   .Hide<BillsPaymentTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              });

        }
         

    }
    public class CommercialBankFundsTransferDetailUI : AppZoneUI.Framework.EntityUI<CommercialBankFundsTransferTransactionType>
    {
        string message = string.Empty;
        public CommercialBankFundsTransferDetailUI()
        {
            //PrePopulateFrom<CommercialBankFundsTransferTransactionType>(x =>
            //{
            //    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            //    {
            //        CommercialBankFundsTransferTransactionType ct =
            //         CommercialBankFundsTransferTransactionTypeSystem.GetCommercialBankFundsTransferTransactionType(source, x.ID) ;
            //        return ct;
            //    }

            //}).Using(x => x.ID);
            
           

            AddSection()
                .IsFormGroup()
             .WithTitle("Transaction Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TransactionStatus).AsSectionField<ITextLabel>(),            
                   
                   Map(x=>x.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),  
                   
                  Map(x=>MobileAccountLogic.InstitutionName(x.Institution)).AsSectionField<ITextLabel>().LabelTextIs("Institution"),
                   Map(x=>MobileAccountLogic.MobileAccountName(x.MobilePhone,false)).AsSectionField<ITextLabel>().LabelTextIs("Account Holder"),
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TransactionDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>(x.TransactionAmount/10).ToString("N2")).AsSectionField<ITextLabel>().LabelTextIs("Amount"),              
                  
                   
                        })});
            AddSection()
               .IsFormGroup()
               .WithTitle("Transfer Details")
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.BankName).AsSectionField<ITextLabel>(),           
                             Map(x=>x.TransactionReference).AsSectionField<ITextLabel>(),
                             Map(x=>x.BeneficiaryLastName).AsSectionField<ITextLabel>(), 
                             
                             
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                              
                             Map(x=>x.PaymentReference).AsSectionField<ITextLabel>(),
                             Map(x=>x.AccountNumber).AsSectionField<ITextLabel>(), 
                             
                             Map(x=>x.BeneficiaryPhoneNumber).AsSectionField<ITextLabel>(),
                             
                        }
                      )
              }
             );

            AddSection()
                .IsFormGroup()
                .WithTitle("Institution")
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<CommercialBankFundsTransferTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                    .Hide<CommercialBankFundsTransferTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              });

          
            AddButton().WithText("Confirm Status")
                .SubmitTo(x =>
            {
                try
                {
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        string status = string.Empty; string desc = string.Empty;
                        CommercialBankFundsTransferTransactionType obj =
                         new CommercialBankFundsTransferTransactionTypeSystem().ConfirmTransaction(source,x, out status, out desc);
                        
                        x.DisplayMessage = string.Format("Actual Transaction Status Obtained. Transaction Status is -{0}, Status Description is -{1}", status, desc);
                        return true;
                    }

                }
                catch (System.ServiceModel.EndpointNotFoundException)
                {
                    x.DisplayMessage = string.Format("Unable to Connect to the Service Provider At This Time. Please try again Later");
                    message = x.DisplayMessage;
                    return false;
                }
                catch (Exception ex)
                {
                    x.DisplayMessage = string.Format("Error in Transaction Confirmation - {0}", ex.Message);
                    message = x.DisplayMessage;
                    return false;
                }
            })
               .OnSuccessDisplay(x => x.DisplayMessage)
               .OnFailureDisplay(x => x.DisplayMessage)
                //  .OnSuccessRedirectTo("AgentDetail.aspx?id={0}",x=>x.ID)
               .ConfirmWith("Confirm This Transaction");

        }


    }
    public class OnNetTransfertDetailUI : AppZoneUI.Framework.EntityUI<LocalFundsTransferTransactionType>
    {

        public OnNetTransfertDetailUI()
        {

            PrePopulateFrom<LocalFundsTransferTransactionType>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    LocalFundsTransferTransactionType ct =
                    new LocalFundsTransferTransactionTypeSystem(source).GetLocalFundsTransferTransactionType(x.ID) as LocalFundsTransferTransactionType;
                    return ct;
                }

            }).Using(x => x.ID);

            AddSection()
               .IsFormGroup()
            .WithTitle("Transaction Details")
            .WithColumns(
             new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                           Map(x=>x.TheTransaction.Status).AsSectionField<ITextLabel>(),            
                   
                   Map(x=>x.TheTransaction.FromPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),  
                   
                  Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Institution"),
                   Map(x=>MobileAccountLogic.MobileAccountName(x.TheTransaction.FromPhoneNumber,false)).AsSectionField<ITextLabel>().LabelTextIs("Account Holder"),
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheTransaction.StatusDetails).AsSectionField<ITextLabel>().LabelTextIs("Details"),
                            Map(x=>x.TheTransaction.StringAmount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),                                 
                  
                   
                        })});

            AddSection()
                .IsFormGroup()
                .WithTitle("Transfer Details")
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.ToAccount).AsSectionField<ITextLabel>().LabelTextIs("Beneficiary"),
                              Map(x=>MobileAccountLogic.InstitutionName(x.ToInstCode)).AsSectionField<ITextLabel>().LabelTextIs("Receiving Institution"),
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>MobileAccountLogic.AgentName(x.AgentCode)).AsSectionField<ITextLabel>().LabelTextIs("Agent Details"),
                        }
                      )
              }
              );
            AddSection()
               .IsFormGroup()
               .WithTitle("Institution")
               .ApplyMod<VisibilityMod>(x => x.SetVisibility<LocalFundsTransferTransactionType>(y => string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode()))
                   .Hide<LocalFundsTransferTransactionType>(y => !string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode())))
             .WithColumns
             (
             new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {                            
                             Map(x=>MobileAccountLogic.InstitutionName(x.TheTransaction.FromInstitutionCode)).AsSectionField<ITextLabel>().LabelTextIs("Name")
                        }
                      ),
                      
              });
            

        }


    }

    public interface ITransactionTypeUI
    {
    }
}
