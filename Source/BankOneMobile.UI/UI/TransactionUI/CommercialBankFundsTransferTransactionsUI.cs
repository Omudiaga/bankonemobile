﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.UI.TransactionUI
{
     public class CommercialBankFundsTransferReportUI : AppZoneUI.Framework.EntityUI<CommercialBankFundsTransferTransactionModel>
    {
          Dictionary<string, string> Institutions;
        
         public CommercialBankFundsTransferReportUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            UseFullView();

            AddNorthSection()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateFrom).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("From"),
                            //Map(x=>x.BankName).AsSectionField<ITextBox>().LabelTextIs("Recieving Bank"),
                             Map(x=>x.BankName).AsSectionField<DropDownList>()
                        .Of<string>(() => 
                        {
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                return new CommercialBankSystem(source).GetActiveCommercialBanks().Select(xx => xx.Name).ToList();
                            }
                        }).AcceptBlank("All").ListOf(x=>x,x=>x).LabelTextIs("Bank"),
                            
                            //Map(x=>x.BeneficiaryPhoneNumber).AsSectionField<ITextBox>().LabelTextIs("Beneficiary Phone Number"),
                            
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateTo).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("To"),
                             
                         Map(x=>x.AccountNumber).AsSectionField<ITextBox>().LabelTextIs("Beneficiary AccountNumber"),
                        // Map(x=>x.PaymentReference).AsSectionField<ITextBox>(),
                        //   Map(x=>x.status).AsSectionField<DropDownList>()
                        //.Of<string>(MobileAccountLogic.TransactionStatusList()).AcceptBlank("All")
                        //.ListOf(x=>x,x=>x).LabelTextIs("Status"),
                        // Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        //.Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        //.ListOf(x => Institutions[x], x => x).LabelTextIs("Institution")
                    
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.FromAccount).AsSectionField<ITextBox>().LabelTextIs("Mobile Account"),
                            Map(x=>x.TransactionReference).AsSectionField<ITextBox>(),
                            //Map(x=>x.BeneficiaryPhoneNumber).AsSectionField<ITextBox>().LabelTextIs("Beneficiary Name"),
                           
                        
                        
                          AddSectionButton()
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Find))
                .UpdateWith(x=>
                    {
                                              
                        
                        return x;
                    }
                ),
                 AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Reload))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("CommercialBankFundsTransferTransactionReport.aspx"))
                }),
              }
              );
           
              



            HasMany(x => x.Transactions).AsCenter<Grid>()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))
               // .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<MobileAccountDetailUI>("Mobile Account Details"))
               //.ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<CommercialBankFundsTransferDetailUI>("Off Net Transfer Transaction Details"))
               
                .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<CommercialBankFundsTransferDetailUI>("Transfer Details").PrePopulate<CommercialBankFundsTransferTransactionType, CommercialBankFundsTransferTransactionType>
                    (                        //BECAUSE OF LAZY LOAD ISSUES

                        zz => {
                            using(var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                              //  CommercialBankFundsTransferTransactionType ct =  CommercialBankFundsTransferTransactionTypeSystem.GetCommercialBankFundsTransferTransactionType(dataSource, zz.ID) ;
                                return zz;
                            }                        


                        }
                    ))
                
                .Of<ICommercialBankFundsTransferTransactionType>()
                .WithColumn(x => x.MobilePhone, "Phone Number")
                .WithColumn(x => x.TransactionDate,"Date")
                .WithColumn(x => x.TransactionStatus,"Status")
                .WithColumn(x => x.TransactionDetails, "Details")
                .WithColumn(x => x.BankName, "Recieving Bank")
                .WithColumn(x => x.AccountNumber, "Reciever Acct No")
               // .WithColumn(x => x.BeneficiaryPhoneNumber, "Reciever Phone No")
                
                .WithColumn(x => x.TransactionReference, "Transaction Reference")
                .WithColumn(x => x.PaymentReference, "Payment Reference")
                

                .WithColumn(x => (x.TransactionAmount/10).ToString("N2"), "Amount")
                 
                
                 .WithColumn(x => Institutions[x.Institution], "Institution")
                

                //, "MobileAccountDetail.aspx?id={0}", x => x.ID)
               
                

                //.WithRowNumbers()

                .IsPaged<CommercialBankFundsTransferTransactionModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    decimal amt = 0M;
                    string instCode = string.Empty;
                    try
                    {
                        x.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            var results = new CommercialBankFundsTransferTransactionTypeSystem().Search(source, x.FromAccount, x.InstitutionCode, x.BankName, x.TransactionReference, x.PaymentReference, x.AccountNumber, x.BeneficiaryPhoneNumber, x.BeneficiaryLastName, x.dateFrom, x.dateTo, x.status, e.Start, e.Limit, out totalCount, out amt);
                            x.Transactions = results;
                        }
                    }
                    catch (Exception ex)
                    {
                        //x.DisplayMessage = ex.Message;
                    }
                    e.TotalCount = totalCount;
                    
                    return x;
                }
                
                );

        }
     }
    }


