﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.TransactionUI
{
     public class RechargeTransactionsUI : AppZoneUI.Framework.EntityUI<RechargeTransactionModel>
    {
          Dictionary<string, string> Institutions;
         
         public RechargeTransactionsUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem().GetAllInstitutionNames(source);
            }
            UseFullView();

            AddNorthSection()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.BorderTop))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateFrom).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("From"),
                            Map(x=>x.MerchantName).AsSectionField<ITextBox>().LabelTextIs("Network"),
                            Map(x=>x.Beneficiary).AsSectionField<ITextBox>().LabelTextIs("Beneficiary Phone Number"),
                            
                        }
                      ),
                  new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.dateTo).AsSectionField<DateField>().SetMaxDate(DateTime.Now).SetMinDate(DateTime.Parse("1-1-2011")).LabelTextIs("To"),
                             
                         
                         Map(x=>x.PaymentReference).AsSectionField<ITextBox>(),
                           Map(x=>x.status).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.TransactionStatusList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                    
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.FromAccount).AsSectionField<ITextBox>().LabelTextIs("Mobile Account"),
                            Map(x=>x.TransactionReference).AsSectionField<ITextBox>(),
                            
                           Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution"),
                        
                        
                          AddSectionButton()
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Find))
                .UpdateWith(x=>
                    {
                                              
                        
                        return x;
                    }
                ),
                 AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Reload))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("RechargeTransactionReport.aspx"))
                }),
              }
              );
           
              



            HasMany(x => x.Transactions).AsCenter<Grid>()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))
               // .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<MobileAccountDetailUI>("Mobile Account Details"))
               .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<RechargeDetailUI>("Recharge Transaction Details"))
                .Of<IRechargeTransactionType>()
                .WithColumn(x => x.TheTransaction.Date)
                .WithColumn(x => x.TheTransaction.Status)
                .WithColumn(x => x.TheTransaction.StatusDetails, "Details")
                .WithColumn(x => x.Beneficiary, "Beneficiary")
                .WithColumn(x => x.MerchantName, "Network")
                .WithColumn(x => x.TransactionReference, "Transaction Reference")
                .WithColumn(x => x.PaymentReference, "Payment Reference")

                .WithColumn(x => x.TheTransaction.StringAmount,"Amount(=N=)")               
               
                

                .WithColumn(x =>x.TheTransaction.Session.MobilePhoneNumber,"Mobile Account")//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                .WithColumn(x => x.TheTransaction.FromInstitutionCode, "Institution")//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                

                 


                .WithRowNumbers()
                .IsPaged<RechargeTransactionModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    decimal amt = 0M;
                    string instCode = string.Empty;
                    try
                    {
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.Transactions = new RechargeTransactionTypeSystem().Search(source, x.FromAccount, x.InstitutionCode, x.MerchantName, x.TransactionReference, x.PaymentReference, x.Beneficiary, x.dateFrom, x.dateTo, x.status, e.Start, e.Limit, out totalCount, out amt);
                        }
                    }
                    catch (Exception ex)
                    {
                        //x.DisplayMessage = ex.Message;
                    }
                    e.TotalCount = totalCount;
                    
                    return x;
                });
        }
     }
    }


