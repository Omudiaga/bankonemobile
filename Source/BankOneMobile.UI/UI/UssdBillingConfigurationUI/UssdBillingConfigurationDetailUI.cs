﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.UI.UI.UssdBillingConfigurationUI
{
    class UssdBillingConfigurationDetailUI : EntityUI<UssdBillingConfiguration>
    {
        public UssdBillingConfigurationDetailUI()
        {
            Map(x => x.Name).As<ITextLabel>();

            Map(x => x.Amount).As<ITextLabel>();

            Map(x => x.Period).As<ITextLabel>();

            AddButton()
                .WithText("Edit Configuration")
                .ApplyMod<ButtonPopupMod>(x => x.Popup<AddEditUssdBillingConfigurationUI>("Edit Configuration")
                    .PrePopulate<UssdBillingConfiguration, UssdBillingConfiguration>(y => y));
        }
    }
}
