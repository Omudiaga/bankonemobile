﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.UssdBillingConfigurationUI
{
    public class ViewUssdBillingConfigurationUI : EntityUI<UssdBillingConfigurationModel>
    {
        public ViewUssdBillingConfigurationUI()
        {
            AddSection()
                .IsFramed()
                .IsCollapsible()
                .WithTitle("Search USSD Billing Configuration")
                .WithFields(new List<IField>() 
                { 
                    Map(x => x.Name).AsSectionField<ITextBox>(),
                    AddSectionButton()
                        .WithText("Search")
                        .UpdateWith(x =>
                        {
                            return x;
                        })
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                });

            HasMany(x => x.ConfigurationList)
                .As<Grid>()
                .ApplyMod<ViewDetailsMod>(x => x.Popup<UssdBillingConfigurationDetailUI>("Configuration Details"))
                .Of<UssdBillingConfiguration>()
                .WithColumn(x => x.Name)
                .WithColumn(x => x.Amount)
                .WithColumn(x => x.Period, "Interval")
                .WithRowNumbers()
                .IsPaged<UssdBillingConfigurationModel>(10, (x, y) =>
                    {
                        int total;
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.ConfigurationList = new UssdBillingConfigurationSystem(source).Search(x.Name, y.Start, y.Limit, out total);
                            return x;
                        }
                    })
                .LabelTextIs("Search Results");
        }
    }
}
