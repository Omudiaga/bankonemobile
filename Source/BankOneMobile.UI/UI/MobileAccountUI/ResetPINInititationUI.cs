﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using System.Web;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class ResetPINInitiationUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        public ResetPINInitiationUI()
        {
            PrePopulateFrom<MobileAccount>(x =>
            {
                return new MobileAccountModel { ReturnURL="PINReset.aspx"};


            });
            AddSection()
             .WithTitle("Reset PIN")
             .WithFields
             (
                new List<IField>()
               {                  

                  
                   Map(x=>x.MobileNumber).AsSectionField<ITextBox>().TextFormatIs(TextFormat.alphanumeric).Required().WithLength(11),
                   
                   
                  AddSectionButton()
                .WithText("Next")
               
                
                .OnSuccessRedirectTo("{0}",x=>x.ReturnURL)
                
                .OnFailureDisplay(x=>x.DisplayMessage)
                
                
                
                .SubmitTo(x=>
                  {
                      try
                      {
                          using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                          {
                            MobileAccount mob  = new MobileAccountSystem(source).GetByPhoneNumber(x.MobileNumber);
                          
                            if(mob==null)
                            {
                                x.DisplayMessage = "Invald Phone Number";

                                return false;
                            }
                            else
                            {

                                x.MobileAccount = mob;
                                x.Accounts = x.MobileAccount.BankAccounts.ToList();
                                HttpContext.Current.Session["MobileAccountModel"] = x;
                                return true;
                            }
                          }
                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage = ex.Message;
                          return false;

                      }
                      
                  })
                   
               }
             );




        }
    }
}
