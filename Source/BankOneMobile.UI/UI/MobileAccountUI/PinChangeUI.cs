﻿using AppZoneUI.Framework;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Services;
using BankOneMobile.UI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class PinChangeUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        public PinChangeUI()
        {
            PrePopulateFrom<MobileAccount>(x =>
            {
                var model = HttpContext.Current.Session["ModifyAccountModel"] as MobileAccountModel;
                //Trace.TraceInformation("Model: {0}", model.MobileAccount == null ? "NULL" : model.MobileAccount.LastName);
                return model;
            });

            UseFullView();

            AddSection()
             .WithTitle("Customer Details")
             .WithColumns(
                new List<Column>()
                {
                    new Column(
                        new List<IField>()
                           {                                    
                               Map(x=>x.MobileAccount.LastName).AsSectionField<TextLabel>().LabelTextIs("Last Name"),
                               Map(x=>x.MobileAccount.OtherNames).AsSectionField<TextLabel>().LabelTextIs("Other Names"),
                           }),
                    new Column(
                        new List<IField>()
                           {                                    
                                Map(x=>x.MobileAccount.MobilePhone).AsSectionField<TextLabel>(),
                           })
                });

            AddSection()
            .WithTitle("PIN Change Details")
            .WithFields
            (
               new List<IField>()
               {            
                   Map(x=>x.OldPIN).AsSectionField<TextBox>().TextFormatIs(TextFormat.numeric).IsPassword().LabelTextIs("Current PIN").WithLength(4,4),
                   Map(x=>x.NewPIN1).AsSectionField<TextBox>().TextFormatIs(TextFormat.numeric).IsPassword().LabelTextIs("New PIN").WithLength(4,4),
                   Map(x=>x.NewPIN2).AsSectionField<TextBox>().TextFormatIs(TextFormat.numeric).IsPassword().LabelTextIs("Confirm New PIN").WithLength(4,4),
               });
            
            AddButton()
                .WithText("Change PIN")
                .ConfirmWith("Are you sure you want to  change this customers PIN? This action cannot be undone")               
                .OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}",x=>x.MobileAccount.ID)
                .OnSuccessDisplay("Pin Change Successful!")                
                .OnFailureDisplay(x=>x.DisplayMessage)   
                .SubmitTo(x=>
                  {
                      if (string.IsNullOrEmpty(x.OldPIN) || x.OldPIN.Length != 4)
                      {
                          x.DisplayMessage = "Invalid value for Old PIN!";
                          return false;
                      }
                      if (x.NewPIN1 != x.NewPIN2)
                      {
                          x.DisplayMessage = "The values for the New PIN do not match!";
                          return false;
                      }
                      if (string.IsNullOrEmpty(x.NewPIN1) || x.NewPIN1.Length != 4)
                      {
                          x.DisplayMessage = "Invalid value for New PIN!";
                          return false;
                      }
                      try
                      {
                          var oldPinBlock = HSMCenter.GeneratePinBlock(x.MobileAccount.MobilePhone, x.OldPIN, AppZone.HsmInterface.PinBlockFormats.ANSI);
                          var newPinBlock = HSMCenter.GeneratePinBlock(x.MobileAccount.MobilePhone, x.NewPIN1, AppZone.HsmInterface.PinBlockFormats.ANSI);
                          bool result = false;
                          var msg = string.Empty;
                          using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                          {
                              var system = new MobileAccountSystem(source);
                              result = system.ChangePINFromBranch(x.MobileAccount.MobilePhone, Logic.UserLogic.GetLoggedOnUserInstCode(), oldPinBlock, newPinBlock, out msg);

                              if (!result)
                              {
                                  x.DisplayMessage = msg;
                              }
                              else
                              {
                                  //Send SMS
                                  var pinChangeMessage = "Your Mobile PIN has been changed succesfully!";
                                  try
                                  {
                                      var messageSent = system.SendInstitutionSMS(pinChangeMessage, x.MobileAccount.MobilePhone,
                                          x.MobileAccount.RecievingBankAccount.InstitutionCode, x.MobileAccount.RecievingBankAccount.BankAccount);
                                  }
                                  catch (Exception ex)
                                  {
                                      System.Diagnostics.Trace.TraceError("Caught an error while sending SMS for pin change.{0}", x.MobileAccount.MobilePhone);
                                      System.Diagnostics.Trace.TraceError(ex.Message);
                                      System.Diagnostics.Trace.TraceError(ex.Source);
                                      System.Diagnostics.Trace.TraceError(ex.StackTrace);
                                  }
                              }
                          }
                          return result;
                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage = ex.Message;
                      }
                      return false;
                  });
        }
    }
}
