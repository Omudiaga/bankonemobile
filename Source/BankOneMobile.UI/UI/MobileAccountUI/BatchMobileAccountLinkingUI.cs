﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneUI.Framework;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework.Mods;
using AppZoneCsvReader.Utility;
using System.IO;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Services;
using BankOneMobile.UI.Logic;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using System.Diagnostics;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class BatchMobileAccountLinkingUI : EntityUI<BatchMobileAccountLiinkingModel>
    {
        public BatchMobileAccountLinkingUI()
        {
            AddSection()
                .ApplyMod<IconMod>(mod => mod.WithIcon(Coolite.Ext.Web.Icon.ShapesMany))
               .StretchFields(95)
               .WithTitle("Upload Batch File")
               .IsFormGroup()
               .WithFields(
                new List<IField>()
                {   
                    AddSubSection().StretchFields(95).WithFields(
                        new List<IField>()
                        {
                            Map(x => x.UploadedFile).AsSectionField<Upload>().LabelTextIs("Accounts Upload File"),

                            AddSectionButton()
                                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.DiskUpload))
                                .WithText("Upload").NoValidate()
                                .UpdateWith(x => 
                                {
                                    if(x.UploadedFile != null)
                                    {
                                        var theFile = (x.UploadedFile as System.Web.HttpPostedFile);
                                        if (!string.IsNullOrEmpty(theFile.FileName))
                                        {
                                            try
                                            {
                                                string uploadPath = System.Configuration.ConfigurationManager.AppSettings["BatchAccountsLinkingUploadFilePath"];
                                                string filename = uploadPath + System.IO.Path.GetFileName(theFile.FileName);
                                                while (System.IO.File.Exists(filename))
                                                {
                                                    filename = uploadPath + System.IO.Path.GetFileNameWithoutExtension(filename) + "1"
                                                        + System.IO.Path.GetExtension(theFile.FileName);
                                                }
                                                //x.FileName = filename;
                                                try
                                                {
                                                    theFile.SaveAs(filename);
                                                }
                                                catch(Exception ex)
                                                {
                                                    Coolite.Ext.Web.ScriptManager.AddInstanceScript(string.Format("window.alert('An error occured when saving the upload file.{0}');", ex.Message));                                                    
                                                }
                                                Trace.TraceInformation("File saved...about to read");
                                                List<CsvReaderItem> items = AppZoneCsvReader.Reader.ReadCsvItems(File.OpenRead(filename), true);
                                                if(items != null && items.Any())
                                                {
                                                    Trace.TraceInformation("Getting details from the saved file..");
                                                    x.AccountLinkingDetailGroups = items.Where(a => !string.IsNullOrEmpty(a.Column0) && MobileAccountSystem.CheckPhoneNumber(a.Column1)
                                                        && !string.IsNullOrEmpty(a.Column2)).Select(b => 
                                                            new BatchMobileAccountLinkingGroup { AccountNumber =  b.Column2, 
                                                                CustomerID = b.Column0, MobileNumber = b.Column1 }).ToList();
                                                    Trace.TraceInformation("Gotten details from the saved file..");
                                                }
                                            }
                                            catch(Exception ex)
                                            {
                                                //Ext.Net.X.AddScript("Ext.MessageBox.alert('{0}','{1}');", "Error", "Error saving attachment file.");
                                                Trace.TraceError("Error reading uploaded file...{0}", ex.Message);
                                                Coolite.Ext.Web.ScriptManager.AddInstanceScript(string.Format("window.alert('An error occured when reading the upload file.');", ex.Message));
                                                //throw new Exception("Error saving attachment file.");
                                            }
                                        }
                                    }
                                    return x;                                    
                                }),
                        }),      
                });

            AddButton().ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Accept))
                .WithText("Link Accounts")//.ConfirmWith("Are you syre you want to reassign the account officer for the accounts")
                .SubmitTo(x =>
                {
                    try
                    {
                        string instCode = UserLogic.GetLoggedOnUserInstCode();
                        for (int i = 0; i < x.AccountLinkingDetailGroups.Count; i++)
                        {
                            var group = x.AccountLinkingDetailGroups[i];
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                var mobileAccount = new MobileAccountSystem(source).GetMobileAccountFromCoreBanking(group.CustomerID, instCode);
                                mobileAccount.MobilePhone = group.MobileNumber;
                                mobileAccount.InstitutionCode = instCode;

                                mobileAccount.ActivateViaJava = true;
                                //mobileAccount.ActivateViaUSSD = x.ActivateViaUSSD;
                                var accounts = mobileAccount.BankAccounts.ToList();
                                mobileAccount.SelectedBankAccounts = new List<ILinkingBankAccount>();
                                mobileAccount.SelectedBankAccounts.Add(accounts.Find(a => a.BankAccount == group.AccountNumber));
                                mobileAccount.PINOffset = instCode;//Be calm! pin offset holds instCode and Product Name holds bank Account. be calm!!
                                mobileAccount.ProductName = mobileAccount.SelectedBankAccounts.Any() ? mobileAccount.SelectedBankAccounts[0].BankAccount : string.Empty;
                                Trace.TraceInformation("About to save. Phone:{0}", mobileAccount.MobilePhone);
                                try
                                {
                                    Object obj = new MobileAccountSystem(source).Insert(mobileAccount as MobileAccount);
                                    Trace.TraceInformation("Done saving acct for Phone:{0}", mobileAccount.MobilePhone);
                                }
                                catch (AlreadyRegisterdCustomerException ex)
                                {
                                    Trace.TraceInformation("Phone:{0} already registered..Moving on..", mobileAccount.MobilePhone);
                                    continue;
                                }
                            }
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = ex.Message;
                        return false;
                    }
                }).OnFailureDisplay(x => string.Format("An error occurred.{0}. Pleae try again.", x.DisplayMessage))
                .OnSuccessDisplay("All acccounts linked successfully")
                .OnSuccessRedirectTo("BatchAccountsLinking.aspx");
        }
    }
}
