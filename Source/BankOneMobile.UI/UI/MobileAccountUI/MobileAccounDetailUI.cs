﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using AppZoneUI.Framework;
using BankOneMobile.Core.Helpers;
using System.Web;
using BankOneMobile.Core.Helpers.Enums;
using AppZoneUI.Framework.Mods;
using System.Web.Security;
using BankOneMobile.UI.Logic;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class MobileAccountDetailUI : AppZoneUI.Framework.EntityUI<MobileAccount>
    {
        private string ENABLE_DISABLE_MOBILEACCOUNT = "EnableDisableMobileAccounts";
        private string HOTLIST_MOBILEACCOUNT = "HotlistMobileAccount";
        private string RESET_MOBILEACCOUNT_PIN = "ResetMobileAccountPins";
        private string RESET_MOBILEACCOUNT_PIN_TRIES = "ResetMobileAccountPinTries";
        private string APPROVE_ACCOUNT_LINKING = "ApproveAccountLinking";
        private string CHANGE_MOBILEACCOUNT_PIN = "ChangeMobileAccountPins";
        const string CHANGE_PIN_URL = "PinChange.aspx";
        MobileAccount mob = null;
        public MobileAccountDetailUI()
        {
            PrePopulateFrom<MobileAccount>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {

                    mob =
                    new MobileAccountSystem(source).GetMobileAccount(x.ID) as MobileAccount;
                    return mob;
                }
            }).Using(x => x.ID);
            AddSection()
                .IsFormGroup()
             .WithTitle("Mobile Wallet Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                           Map(x=>x.MobilePhone).AsSectionField<ITextLabel>(),
                        //Map(x=>x.RegistrarID+"("+ x.OpenedBy")"),
                        Map(x=>x.DateCreated.ToString()).AsSectionField<ITextLabel>().LabelTextIs("Date Created"),
                       
                    
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                          
                   
                    Map(x=>x.DateActivated.Year>1900?x.DateActivated.ToString("yyyy-MM-dd"):"Not Yet Activated").AsSectionField<ITextLabel>().LabelTextIs("Date Activated"),
                           
                          // Map(x=> DisplayOpeneBy(x.OpenedBy,x.RegistrarID.ToString()),
                       
                    
                }),
                new Column(
                        new List<IField>()
                        {
                             Map(x=>x.MobileAccountStatus).AsSectionField<ITextLabel>().LabelTextIs("Status"),
                          //  Map(x=>x.ActivationCode).AsSectionField<ITextLabel>(),
                            Map(x=>x.OpenedBy==AccountRegistrarType.Agent?string.Format("Mobile Teller:({0})",
                               MobileAccountLogic.AgentName( x.RegistrarCode)):"Branch").AsSectionField<ITextLabel>().LabelTextIs("Opened By"),
                  
                  
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});
            AddSection()
                .IsFormGroup()
                .WithTitle("Customer Details")
            .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            
                            Map(x=>x.LastName).AsSectionField<ITextLabel>(),
                        
                    
                }),
               
                new Column(
                        new List<IField>()
                        {
                             
                   
                  
                   Map(x=>x.OtherNames).AsSectionField<ITextLabel>(),
                   
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});

            HasMany(x => x.BankAccounts).As<Grid>().Of<LinkingBankAccount>()
                .WithColumn(x => x.CustomerID, "CustomerID")
                .WithColumn(x => x.CoreBankingNames, "Account Name")
             .WithColumn(x => x.BankAccount,"Account Number")
            
            .WithColumn(x => x.ProductName,"Product")
            .WithColumn(x=>x.TheGender,"Gender")
            .WithColumn(x => x.BankStatus, "Status")
            .WithColumn(x=>x.InstitutionName,"Institution")
            
             //.WithColumn(x => x.Activated,"Activated")
             
             
             .WithRowNumbers();

            AddButton()
                   .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                   .WithText("Back").SubmitTo(x =>
                   {
                       return true;
                   })
                .OnSuccessRedirectTo(HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
            
            if (Roles.IsUserInRole(APPROVE_ACCOUNT_LINKING))
            {
                AddButton()
                   .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                   .WithText("Approve Linking").SubmitTo(x =>
                   {
                       try
                       {
                           using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                           {
                               bool done = new MobileAccountSystem(source).ApproveLinking(x);
                               if (done)
                               {
                                   x.DisplayMessage = String.Format("Account has been succesfuly Linked");
                               }
                               else
                               {
                                   x.DisplayMessage = String.Format("Account Linking failed. please try again");
                               }
                           }

                       }
                       catch (Exception ex)
                       {
                           x.DisplayMessage = String.Format("Error in linking account -{0}", ex.Message);
                           return false;
                       }
                       return true;

                   })
                   .OnSuccessDisplay(x => x.DisplayMessage)
                   .OnFailureDisplay(x => x.DisplayMessage)
                    
                   .ConfirmWith(x => "Are you sure you want to Confirm the Linking of the specified Mobile Account to the Specified Bank Account");
                
            }
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                .WithText("Generate Mobile App Verification Code").SubmitTo(x =>
                {
                    try
                    {
                        System.Diagnostics.Trace.TraceInformation("About to generate verification code");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to generate verification code"));
                        new PANE.ERRORLOG.Error().LogToFile(new Exception("About to generate verification code"));

                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            System.Diagnostics.Trace.TraceInformation("About to generate verification code1");
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to generate verification code1"));
                            bool done = new MobileAccountSystem(source).GenerateVerificationCode(x, UserLogic.GetLoggedOnUserInstCode());
                            if (done)
                            {
                                x.DisplayMessage = String.Format("Verification Code has been generated and sent to the Mobile Phone via SMS");
                            }
                            else
                            {
                                x.DisplayMessage = String.Format("Verification Code  has not been delivered. please try again");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = String.Format("Error in sending  Verification Code -{0}", ex.Message);
                        return false;
                    }
                    return true;

                })
                .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x => x.DisplayMessage)
                //.OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Reset and Send Verification Code?");

            if (Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN))
            {
                AddButton()
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<MobileAccount>(y => y != null && y.MobileAccountStatus == MobileAccountStatus.Active))
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                    .WithText("Reset Pin").SubmitTo(x =>
                 {
                     try
                     {
                         x.InstitutionCode = Logic.UserLogic.MFBCode;
                         x.TheUpdateType = UpdateType.PinReset;
                         using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                         {
                             Object obj = new MobileAccountSystem(source).ResetPin(x);
                             if (obj is MobileAccount)
                             {
                                 x.DisplayMessage = String.Format("Pin for Mobile Account Succesfully Reset");

                             }
                             else
                             {
                                 x.DisplayMessage = "Pin Reset of Mobile Account  loggged for approval ";
                                 return false;
                             }
                         }
                         // x = new MobileAccountSystem().EnableDisableMobileAccount(x);
                     }
                     catch (Exception ex)
                     {
                         x.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                         return false;
                     }

                     return true;

                 })
                     .OnSuccessDisplay(x => x.DisplayMessage)
                    .OnFailureDisplay(x => x.DisplayMessage)
                    //  .OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith("Are you sure you want to Reset The PIN for this Account");
            }
            if (Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN))
            {
                AddButton()
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<MobileAccount>(y => y != null && y.MobileAccountStatus == MobileAccountStatus.New))
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                    .WithText("Send Activation Code").SubmitTo(x =>
                    {
                        try
                        {
                            x.InstitutionCode = Logic.UserLogic.MFBCode;
                            x.TheUpdateType = UpdateType.PinReset;
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem(source).ResetPin(x);
                                if (obj is MobileAccount)
                                {
                                    x.DisplayMessage = String.Format("Activation code succesfully sent");

                                }
                                else
                                {
                                    x.DisplayMessage = "Activation code sending loggged for approval ";
                                    return false;
                                }
                            }
                            // x = new MobileAccountSystem().EnableDisableMobileAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }

                        return true;

                    })
                     .OnSuccessDisplay(x => x.DisplayMessage)
                    .OnFailureDisplay(x => x.DisplayMessage)
                    //  .OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith("Are you sure you want to send the activation code for this Account");
            }
            if( Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN_TRIES))
            {
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowDivide))
                .WithText("Reset Pin Tries").SubmitTo(x =>
           {
               try
               {
                   x.InstitutionCode = Logic.UserLogic.MFBCode;
                   x.TheUpdateType = UpdateType.PinTryReset;
                   using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                   {
                       Object obj = new MobileAccountSystem(source).ResetMobileAccountPinTries(x);
                       if (obj is MobileAccount)
                       {
                           x.DisplayMessage = String.Format("Pin Tries Succefully Reset");

                       }
                       else
                       {
                           x.DisplayMessage = "Pin tries Reset   loggged for approval ";
                           return false;
                       }
                   }
                   // x = new MobileAccountSystem().EnableDisableMobileAccount(x);
               }
               catch (Exception ex)
               {
                   x.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                   return false;
               }

               return true;

           })
            .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x => x.DisplayMessage)
                //  .OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Are you sure you want to Reset The PIN Tries for this Account");
           
            }

            if(Roles.IsUserInRole(CHANGE_MOBILEACCOUNT_PIN))
            {
                AddButton()
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.PhoneKey))
                    .WithText("Change PIN").SubmitTo(x =>
                    {
                        return true;
                    })
                .OnSuccessRedirectTo(CHANGE_PIN_URL);
            
            }

            if(Roles.IsUserInRole(HOTLIST_MOBILEACCOUNT))
            {
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Lock))
                .WithText("Hot List Account").SubmitTo(x =>
            {
                try
                {
                    //string instCode = Logic.UserLogic.GetLoggedOnUserInstCode();
                    x.InstitutionCode = Logic.UserLogic.MFBCode;
                    x.TheUpdateType = UpdateType.HotList;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj = new MobileAccountSystem(source).HotList(x);
                        if (obj is MobileAccount)
                        {
                            x.DisplayMessage = String.Format("Mobile Account Succesfully Hotlisted");

                        }
                        else
                        {
                            x.DisplayMessage = "Hotlist of Mobile Account  loggged for approval ";
                            return false;
                        }
                    }
                    // x = new MobileAccountSystem().EnableDisableMobileAccount(x);
                }
                catch (Exception ex)
                {
                    x.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                    return false;
                }
                
                return true;

            })
                 .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x=>x.DisplayMessage)
                //.OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith("Are you sure you want to HotList This Account");

            }
            //if (mob != null && (mob.MobileAccountStatus == MobileAccountStatus.Active || mob.MobileAccountStatus == MobileAccountStatus.InActive || mob.MobileAccountStatus == MobileAccountStatus.HotListed))
            //{
            if(Roles.IsUserInRole(ENABLE_DISABLE_MOBILEACCOUNT))
            {
            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                .WithText(x=>x.MobileAccountStatus==MobileAccountStatus.Active? "Disable":"Enable").SubmitTo(x =>//x => x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
            {
                try
                {
                    x.InstitutionCode = Logic.UserLogic.MFBCode;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj = new MobileAccountSystem(source).EnableDisable(x, -1);
                        if (obj is MobileAccount)
                        {
                            x.DisplayMessage = String.Format("Mobile Account Succesfully {0}", (obj as MobileAccount).MobileAccountStatus == MobileAccountStatus.Active ? "Enabled" : "Disabled");
                        }
                        else
                        {
                            x.DisplayMessage = String.Format("{0} of Mobile Account  loggged for approval ", x.MobileAccountStatus == MobileAccountStatus.Active ? "Disabling" : "Enabling");
                            return false;
                        }
                    }
                   // x = new MobileAccountSystem().EnableDisableMobileAccount(x);
                }
                catch(Exception ex)
                {
                    x.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                    return false;
                }
                return true;

            })
                .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x=>x.DisplayMessage)
                //.OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                .ConfirmWith(x => x != null && x.IsActive ? "Are you sure you want to Disable this Mobile Account" : "Are you sure you want to Enable this Mobile Account");
           // }
           
           // }
            }
           
        }
        public string DisplayOpeneBy(string type,string value )
        {
          AccountRegistrarType acc =  (AccountRegistrarType) Enum.Parse(typeof(AccountRegistrarType),type);
          return String.Format("{0} ({1})", value, acc.ToString());
        }

    }
}
