﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Services;
using BankOneMobile.UI.UI.LinkingBankAccountUI;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class ListMFBAccountUI : AppZoneUI.Framework.EntityUI<LinkingBankAccountModel>
    {
        Dictionary<string, string> products;
        public ListMFBAccountUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                products = new LinkingBankAccountSystem(source).GetInstitutionProducts(UserLogic.GetLoggedOnUserInstCode());
            }
            AddNorthSection()

               .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Find))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              
              .WithColumns
              (
              new List<Column>()
              {
                  new Column
                      (
                      new List<IField>()
                        {
                             Map(x=>x.LastName).AsSectionField<TextBox>(),                            
                             
                             Map(x=>x.accountNumber).AsSectionField<TextBox>(),
                             
                              Map(x=>x.isAgentAccount).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.IsActive()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Mobile Teller"),
                             
                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.OtherNames).AsSectionField<TextBox>(),
                            Map(x=>x.gender).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.GenderList()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Gender"),
                        
                         Map(x=>x.IsActive).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.BankAccountStatuslist()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                         AddSectionButton()
                .WithText("Search").NoValidate()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x=>
                    {
                                             
                        
                        return x;
                    }
                ),
                AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .WithText("Reset").NoValidate()
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("ViewMfbAccount.aspx"))


                        }
                      ),
                      new Column
                      (
                      new List<IField>()
                        {
                            Map(x=>x.MobileNumber).AsSectionField<TextBox>(),
                              Map(x=>x.productCode).AsSectionField<DropDownList>()

                        .Of<string>(products.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x=>products[x],x=>x).LabelTextIs("Product"),                         
                             
                             

                             
                        }
                      ),
              }
              );
            UseFullView();

            HasMany(x => x.TheAccounts).AsCenter<Grid>().LabelTextIs("Mobile Wallets")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Money))
                .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<LinkingBankAccountDetailUI>("Wallet Details").PrePopulate<LinkingBankAccount, LinkingBankAccount>
                    (
                        //BECAUSE OF LAZY LOAD ISSUES
                        zz => {
                            using(var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                var mod =  new LinkingBankAccountSystem(dataSource).GetByID(dataSource, zz.ID);
                                return mod;
                            }
                        }
                    ))
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Mobile Wallets Export")
                    .ExportAllRows().SetPagingLimit<LinkingBankAccountModel>(y => (int)System.Web.HttpContext.Current.Session["LBATotalCount"]))
                .Of<ILinkingBankAccount>()
                .WithColumn(x => x.BankAccount,"Account Number")
                .WithColumn(x => x.CoreBankingNames, "Account Name")  
                .WithColumn(x => x.ProductName)
                .WithColumn(x => x.TheGender, "Gender")       
                .WithColumn(x => x.TheMobileAccount.MobilePhone,"Phone Number")//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                .WithColumn(x => x.TheMobileAccount.IsAgentAccount ? "Mobile Teller" : "Customer", "Account Type")
                .WithColumn(x => x.BankStatus, "Status")
                .WithColumn(x => x.TheMobileAccount.DateCreated).WithRowNumbers()
                .IsPaged<LinkingBankAccountModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    string instCode = string.Empty;
                    try
                    {
                        string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                        x.InstitutionCode = institutionCode;
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.TheAccounts = new LinkingBankAccountSystem(source).Search(x.MobileNumber, x.IsActive, x.isAgentAccount, x.accountNumber, x.InstitutionCode, x.productCode, x.gender, x.LastName, x.OtherNames, x.RegistrarCode, x.IsActive, e.Start, e.Limit, out totalCount);
                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = ex.Message;
                    }
                    System.Web.HttpContext.Current.Session["LBATotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    return x;
                });
        }

      
    }
}
              
              
        
        

    
