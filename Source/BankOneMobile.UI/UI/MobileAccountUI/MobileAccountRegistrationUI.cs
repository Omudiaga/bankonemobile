﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using System.Web;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using AppZoneUI.Framework.Mods;
using BankOneMobile.UI.Logic;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class MobileAccountRegistrationUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        public MobileAccountRegistrationUI()
        {
            string returnURL = HttpContext.Current.Request.QueryString["i"]=="Register"?"MobileAccountConfirmation.aspx":"PINReset.aspx";
            PrePopulateFrom<MobileAccountModel>(x =>
            {

                MobileAccountModel mod =
                    HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel == null ? new MobileAccountModel() : HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel;
                if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("i"))
                {
                    mod = new MobileAccountModel();
                }
                mod.ReturnURL = "MobileAccountConfirmation.aspx";
                return mod;


            });
            UseFullView();
            WithTitle("Account Linking -Step 1");
            AddSection()
                .IsFormGroup()
             .WithTitle("Customer Details")
             .WithFields
             (
                new List<IField>()
               {                  

                   Map(x=>x.CustomerID).AsSectionField<ITextBox>().TextFormatIs(TextFormat.alphanumeric).Required().LabelTextIs("Customer ID").WithLength(13),
                   
                   
                   
                  AddSectionButton()
                  .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.NextGreen))
                .WithText("Next")
               
                
                .OnSuccessRedirectTo("{0}",x=>x.ReturnURL)
                
                .OnFailureDisplay(x=>x.DisplayMessage)
                
                
                
                .SubmitTo(x=>
                  {
                      try
                      {
                          string instCode = UserLogic.GetLoggedOnUserInstCode();
                          using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                          {
                            x.MobileAccount = new MobileAccountSystem(source).GetMobileAccountFromCoreBanking(x.CustomerID, instCode);
                          }
                          x.Accounts = x.MobileAccount.BankAccounts.ToList();
                          //x.MobileNumber = x.MobileAccount.MobilePhone;
                          x.LastName = x.MobileAccount.LastName;
                          x.OtherNames = x.MobileAccount.OtherNames;
                          HttpContext.Current.Session["MobileAccountModel"] = x;
                           return true;
                       
                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage = ex.Message;
                          return false;

                      }
                      
                  })
                   
               }
             );




        }
    }
    public class SpecialMobileAccountActionUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        bool isHotList = false;
        public SpecialMobileAccountActionUI()
        {

            UseFullView();
            WithTitle("Modify Specific Account");
            AddSection()
                .IsFormGroup()
             .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.DoorOpen))
             .WithTitle("Enter the  Phone Number of the Account")
             .WithFields
             (
                new List<IField>()
               {                  

                   Map(x=>x.MobileNumber).AsSectionField<ITextBox>().TextFormatIs(TextFormat.alphanumeric).Required().LabelTextIs("Phone Number").WithLength(11),
                                    
                  AddSectionButton()
                .WithText("Next")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.NextGreen))
                .SubmitTo(x=>
                  {
                      try
                      {
                          x.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();
                         using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.MobileAccount = new MobileAccountSystem(source).GetByPhoneNumber(x.MobileNumber);
                            if (x.MobileAccount != null)
                            { 
                                int totalCount;
                                x.Accounts = new LinkingBankAccountSystem(source).Search(x.MobileNumber, string.Empty, string.Empty, string.Empty, x.InstitutionCode,string.Empty, string.Empty, x.LastName, x.OtherNames, x.RegistrarCode, x.IsActive, 0, 1000, out totalCount);
                            }
                          }
                          HttpContext.Current.Session["ModifyAccountModel"] = x;
                          if (x.MobileAccount==null)
                          {
                              throw new InvalidPhoneNumberException("Invalid Phone Number");
                          }
                          if (x.Accounts == null || !x.Accounts.Any())
                          {
                              throw new Exception("There are no accounts linked to this wallet");
                          }                        
                           return true;
                       
                      }
                      catch (Exception ex)
                      {
                          x.DisplayMessage =String.Format("Unable to Complete:Reason:{0}", ex.Message);
                          return false;

                      }
                      
                  })
                 
                  .OnSuccessRedirectTo("ModifyMobileAccount.aspx")//isHotList)
                
                .OnFailureDisplay(x=>x.DisplayMessage)
                   
               }
             );




        }
    }
}
