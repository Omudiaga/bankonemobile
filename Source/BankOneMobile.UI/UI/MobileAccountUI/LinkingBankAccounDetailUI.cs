﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using AppZoneUI.Framework;
using BankOneMobile.Core.Helpers;
using System.Web;
using BankOneMobile.Core.Helpers.Enums;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.UI.Logic;
using System.Diagnostics;
using System.Web.Security;

namespace BankOneMobile.UI.UI.LinkingBankAccountUI
{
    public class LinkingBankAccountDetailUI : AppZoneUI.Framework.EntityUI<LinkingBankAccount>
    {

        private string ENABLE_DISABLE_MOBILEACCOUNT = "EnableDisableMobileAccounts";
        private string HOTLIST_MOBILEACCOUNT = "HotlistMobileAccount";
        private string RESET_MOBILEACCOUNT_PIN = "ResetMobileAccountPins";
        private string RESET_MOBILEACCOUNT_PIN_TRIES = "ResetMobileAccountPinTries";
        private string APPROVE_ACCOUNT_LINKING = "ApproveAccountLinking";
        private string GENERATE_JAVA_APP_VERIFICATION_CODE = "GenerateVerificationCode";
        LinkingBankAccount mob = null;
        public LinkingBankAccountDetailUI()
        {
            //PrePopulateFrom<LinkingBankAccount>(x =>
            //{
            //    mob =
            //    new LinkingBankAccountSystem().GetByID(DataSourceFactory.GetDataSource(DataCategory.Shared),x.ID) as LinkingBankAccount;
            //    return mob;




            //}).Using(x => x.ID);
            //PrePopulateFrom<LinkingBankAccount>(x =>
            //{
            //    //Lazy load issues
            //    using(var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            //    {
            //        if (x != null && x.TheMobileAccount != null)
            //        {
            //            x.TheMobileAccount = new MobileAccountSystem(dataSource).GetMobileAccount(x.TheMobileAccount.ID);
            //        }
            //        return x;
            //    }
            //});
            AddSection()
                .ApplyMod<VisibilityMod>(y => y.SetVisibility<LinkingBankAccount>(z => z.BankStatus == BankAccountStatus.AwaitingApproval))
                .IsFormGroup()
             .WithTitle("Link Request Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.BankAccount).AsSectionField<ITextLabel>().LabelTextIs("Account Number"),
                           
                           
                            
                          Map(x=>GetSignature(x)).AsSectionField<Image>().SetHeight(100).SetWidth(100).LabelTextIs("Signature"), 
                           
                          
                    
                }),
                
                
               
                new Column(
                        new List<IField>()
                        {
                           Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                           
                           Map(x=>GetPassport(x)).AsSectionField<Image>().SetHeight(100).SetWidth(100).LabelTextIs("Passport"), 
 

                  
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});
            AddSection()
                .IsFormGroup()
             .WithTitle("Bank Account Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.CoreBankingNames).AsSectionField<ITextLabel>().LabelTextIs("Account Name"),
                            
                            Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Customer ID"),
                          
                       
                    
                }),
                
                new Column(
                        new List<IField>()
                        {
                           Map(x=>x.BankAccount).AsSectionField<ITextLabel>().LabelTextIs("Account Number"),
                           Map(x=>x.BankStatus).AsSectionField<ITextLabel>().LabelTextIs("Status"),
                        
                  
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});
            AddSection()
                .IsFormGroup()
             .WithTitle("Primary Mobile Instrument")
             .WithColumns(
              new List<Column>(){
                   
                
                new Column(
                        new List<IField>()
                        {
                           Map(x=>x.TheMobileAccount.MobilePhone).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                           
                        
                  
                    //Map(x=>x.DateActivated).AsSectionField<ITextLabel>(),
                        })});
            AddSection()
                .IsFormGroup()
             .WithTitle("Mobile Wallet Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheMobileAccount.LastName).AsSectionField<ITextLabel>().LabelTextIs("Last Name"),
                           Map(x=>x.TheMobileAccount.PersonalPhoneNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                        //Map(x=>x.RegistrarID+"("+ x.OpenedBy")"),
                        Map(x=>x.TheMobileAccount.DateCreated.ToString()).AsSectionField<ITextLabel>().LabelTextIs("Date Created"),
                       
                     Map(x=>x.TheMobileAccount.OpenedBy==AccountRegistrarType.Agent?string.Format("Mobile Teller:({0})",
                              MobileAccountLogic.AgentName( x.TheMobileAccount.RegistrarCode)):"Branch").AsSectionField<ITextLabel>().LabelTextIs("Opened By"),
                }),
                new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TheMobileAccount.OtherNames).AsSectionField<ITextLabel>(),
                          Map(x=>x.TheMobileAccount.MobileAccountStatus).AsSectionField<ITextLabel>().LabelTextIs("Status"),
                   
                    Map(x=>x.TheMobileAccount.DateActivated.Year>1900?x.TheMobileAccount.DateActivated.ToString("yyyy-MM-dd"):"Not Yet Activated").AsSectionField<ITextLabel>().LabelTextIs("Date Activated"),
                     //Map(x=>x.TheMobileAccount.ActivationCode).AsSectionField<ITextLabel>().LabelTextIs("Activation Code"),
                          // Map(x=> DisplayOpeneBy(x.OpenedBy,x.RegistrarID.ToString()),
                       
                    
                }),
               });

            string instCode = UserLogic.GetLoggedOnUserInstCode();
            HasMany(x => x.TheMobileAccount.BankAccounts.Where(a=>a.InstitutionCode==instCode).ToList()).As<Grid>().Of<LinkingBankAccount>()
                .WithColumn(x => x.CoreBankingNames, "Account Name")
                .WithColumn(x => x.CustomerID, "CustomerID")
             .WithColumn(x => x.BankAccount, "Account Number")

            .WithColumn(x => x.ProductName, "Product")
            .WithColumn(x => x.TheGender, "Gender")
            .WithColumn(x => x.BankStatus, "Status")
            //.WithColumn(x => x.InstitutionName, "Institution")

             //.WithColumn(x => x.Activated,"Activated")


             .WithRowNumbers().LabelTextIs("Associated Bank Accounts ");
            //AddButton().WithText("Return To List").SubmitTo(x => true).OnSuccessRedirectTo("ViewLinkingBankAccounts.aspx");
            AddButton()
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus == BankAccountStatus.AwaitingApproval))
              .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
              .WithText("Decline Request").SubmitTo(x =>
              {
                  try
                  {
                      using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                      {
                          bool done = new MobileAccountSystem(source).ApproveLinking(x, false);
                          if (done)
                          {
                              x.TheMobileAccount.DisplayMessage = String.Format("Linking Request was Succesfully Declined");
                          }
                          else
                          {
                              x.TheMobileAccount.DisplayMessage = String.Format("Action Failed");
                          }
                      }
                  }
                  catch (Exception ex)
                  {
                      x.TheMobileAccount.DisplayMessage = String.Format("Error Declining Link Request-{0}", ex.Message);
                      return false;
                  }
                  return true;

              })
              .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
              .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)

              .ConfirmWith("Decline Linking Request?");

            AddButton()
                .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus == BankAccountStatus.AwaitingApproval))
              .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
              .WithText("Approve Request").SubmitTo(x =>
              {
                  try
                  {
                      using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                      {
                          bool done = new MobileAccountSystem(source).ApproveLinking(x, true);
                          if (done)
                          {
                              x.TheMobileAccount.DisplayMessage = String.Format("Linking Request was Succesfully Approved");
                          }
                          else
                          {
                              x.TheMobileAccount.DisplayMessage = String.Format("Approval Failed");
                          }

                      }
                  }
                  catch (Exception ex)
                  {
                      x.TheMobileAccount.DisplayMessage = String.Format("Error Approving Link Request-{0}", ex.Message);
                      return false;
                  }
                  return true;

              })
              .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
              .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)

              .ConfirmWith("Approve Linking Request?");

            if (Roles.IsUserInRole(GENERATE_JAVA_APP_VERIFICATION_CODE))
            {
                AddButton()
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus == BankAccountStatus.Active))
                   .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                   .WithText("Generate Mobile App Verification Code").SubmitTo(x =>
                   {
                       try
                       {
                           using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                           {
                               bool done = new MobileAccountSystem(source).GenerateVerificationCode(x.TheMobileAccount as MobileAccount, UserLogic.GetLoggedOnUserInstCode());
                               if (done)
                               {
                                   x.TheMobileAccount.DisplayMessage = String.Format("Verification Code has been generated and sent to the Mobile Phone via SMS");
                               }
                               else
                               {
                                   x.TheMobileAccount.DisplayMessage = String.Format("Verification Code  has not been delivered. please try again");
                               }
                           }

                       }
                       catch (Exception ex)
                       {
                           x.TheMobileAccount.DisplayMessage = String.Format("Error in sending  Verification Code -{0}", ex.Message);
                           return false;
                       }
                       return true;

                   })
                   .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                   .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //.OnSuccessRedirectTo("MobileAccountDetail.aspx?id={0}", x => x.ID)
                   .ConfirmWith("Reset and Send Verification Code");
            }
            if (Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN))
            {
                AddButton()
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus == BankAccountStatus.Active && y.TheMobileAccount != null && y.TheMobileAccount.MobileAccountStatus == MobileAccountStatus.Active))
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                    .WithText("Reset Pin")
                    .SubmitTo(x =>
                    {
                        try
                        {
                            x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                            (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.PinReset;
                            x.TheMobileAccount.ProductName = x.BankAccount;
                            x.TheMobileAccount.PINOffset = x.InstitutionCode;
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem(source).ResetPin(x.TheMobileAccount as MobileAccount);
                                if (obj is MobileAccount)
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("Pin for Mobile Account Succesfully Reset");

                                }
                                else
                                {
                                    x.TheMobileAccount.DisplayMessage = "Pin Reset of Mobile Account  loggged for approval ";
                                    return false;
                                }
                            }
                            // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }

                        return true;

                    })
                     .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                    .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //  .OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith(x => string.Format("Are you sure you want to reset the PIN for this Account"));
            }
            if (Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN))
            {
                AddButton()
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.TheMobileAccount != null && y.TheMobileAccount.MobileAccountStatus == MobileAccountStatus.New))
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationSplit))
                    .WithText("Send Activation Code")
                    .SubmitTo(x =>
                    {
                        try
                        {
                            x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                            (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.PinReset;
                            x.TheMobileAccount.ProductName = x.BankAccount;
                            x.TheMobileAccount.PINOffset = x.InstitutionCode;
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem(source).ResetPin(x.TheMobileAccount as MobileAccount);
                                if (obj is MobileAccount)
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("Activation code succesfully sent");

                                }
                                else
                                {
                                    x.TheMobileAccount.DisplayMessage = "Activation code sending loggged for approval ";
                                    return false;
                                }
                            }
                            // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }

                        return true;

                    })
                     .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                    .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //  .OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith(x => string.Format("Are you sure you want to send the activation code for this Account"));
            }
            if (Roles.IsUserInRole(RESET_MOBILEACCOUNT_PIN_TRIES))
            {
                AddButton()
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowDivide))
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus == BankAccountStatus.Active))
                    .WithText("Reset Pin Tries").SubmitTo(x =>
                    {
                        try
                        {
                            x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                            (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.PinTryReset;
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem(source).ResetMobileAccountPinTries(x.TheMobileAccount as MobileAccount);
                                if (obj is LinkingBankAccount)
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("Pin Tries Succefully Reset");

                                }
                                else
                                {
                                    x.TheMobileAccount.DisplayMessage = "Pin tries Reset   loggged for approval ";
                                    return false;
                                }
                            }
                            // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }

                        return true;

                    })
                .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                    .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //  .OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith("Are you sure you want to Reset The PIN Tries for this Account");
            }
            if (Roles.IsUserInRole(HOTLIST_MOBILEACCOUNT))
            {


                AddButton()
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Lock))
                    //.ApplyMod<VisibilityMod>(x=>x.SetVisibility<LinkingBankAccount>(y=>y.BankStatus!=BankAccountStatus.))
                    .WithText("Hot List Account").SubmitTo(x =>
                    {
                        try
                        {
                            //string instCode = Logic.UserLogic.GetLoggedOnUserInstCode();
                            x.TheMobileAccount.InstitutionCode = Logic.UserLogic.MFBCode;
                            (x.TheMobileAccount as MobileAccount).TheUpdateType = UpdateType.HotList;
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem().HotList(x.TheMobileAccount as MobileAccount);
                                if (obj is MobileAccount)
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("Mobile Account Succesfully Hotlisted");

                                }
                                else
                                {
                                    x.TheMobileAccount.DisplayMessage = "Hotlist of Mobile Account  loggged for approval ";
                                    return false;
                                }
                            }
                            // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }

                        return true;

                    })
                     .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                    .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //.OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith("Are you sure you want to HotList This Account");
            }

            //if (mob != null && (mob.LinkingBankAccountStatus == LinkingBankAccountStatus.Active || mob.LinkingBankAccountStatus == LinkingBankAccountStatus.InActive || mob.LinkingBankAccountStatus == LinkingBankAccountStatus.HotListed))
            //{
            if (Roles.IsUserInRole(ENABLE_DISABLE_MOBILEACCOUNT))
            {
                AddButton()
                    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ApplicationFormDelete))
                    .ApplyMod<VisibilityMod>(x => x.SetVisibility<LinkingBankAccount>(y => y.BankStatus != BankAccountStatus.AwaitingApproval))
                    //.WithText(x => (x != null && x.TheMobileAccount != null) ? (x.TheMobileAccount.MobileAccountStatus == MobileAccountStatus.Active ? "Disable" : "Enable") : string.Empty).SubmitTo(x =>//x => x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
                    .WithText(x => (x != null && x.TheMobileAccount != null && x.TheMobileAccount.MobileAccountStatus == MobileAccountStatus.Active) ? "Disable" : "Enable").SubmitTo(x =>//x => x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
                    {
                        try
                        {
                            //  x.InstitutionCode = Logic.UserLogic.MFBCode;
                            LinkingBankAccount toUse = x.TheMobileAccount.RecievingBankAccount as LinkingBankAccount;
                            long theID = 0; int index = 0;
                            foreach (LinkingBankAccount lc in x.TheMobileAccount.BankAccounts)
                            {
                                if (lc.ID == x.ID)
                                {
                                    toUse = lc;
                                    theID = lc.ID;
                                    break;

                                }
                                else
                                {
                                    index++;
                                }
                            }
                            Trace.TraceInformation("ID is {0} number is {1}", theID, x.TheMobileAccount.BankAccounts.Count());


                            Trace.TraceInformation("About to enable disable ");
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Object obj = new MobileAccountSystem(source).EnableDisable(x.TheMobileAccount as MobileAccount, index, x);
                                if (obj is MobileAccount)
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("Mobile Account Succesfully {0}", (obj as MobileAccount).MobileAccountStatus == MobileAccountStatus.Active ? "Enabled" : "Disabled");

                                }
                                else
                                {
                                    x.TheMobileAccount.DisplayMessage = String.Format("{0} of Mobile Account  loggged for approval ", (obj as MobileAccount).MobileAccountStatus == MobileAccountStatus.Active ? "Disabling" : "Enabling");
                                    return false;
                                }
                            }
                            // x = new LinkingBankAccountSystem().EnableDisableLinkingBankAccount(x);
                        }
                        catch (Exception ex)
                        {
                            x.TheMobileAccount.DisplayMessage = String.Format("Error in Mobile Account Update -{0}", ex.Message);
                            return false;
                        }
                        return true;

                    })
                    .OnSuccessDisplay(x => x.TheMobileAccount.DisplayMessage)
                    .OnFailureDisplay(x => x.TheMobileAccount.DisplayMessage)
                    //.OnSuccessRedirectTo("LinkingBankAccountDetail.aspx?id={0}", x => x.ID)
                    .ConfirmWith(x => x != null && x.TheMobileAccount != null && x.TheMobileAccount.IsActive ? "Are you sure you want to Disable this Mobile Account" : "Are you sure you want to Enable this Mobile Account");
            }
            // }

            // }


        }
        public string GetSignature(LinkingBankAccount lbc)
        {
            string toReturn = "{0}/Images/Imager.ashx?ACCOUNT_ID={1}&IMAGE_TYPE=SIGNATURE";
            if (lbc.Signature == null)
            {
                using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new BankOneMobile.Services.CoreBankingService.SwitchingServiceClient())
                {

                    lbc.Signature = client.GetSignatureByCustomerID(lbc.InstitutionCode, lbc.CustomerID);
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        new MobileAccountSystem(source).UpdateMobileAccount(lbc.TheMobileAccount as MobileAccount);
                    }
                }
            }

            return string.Format(toReturn, System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"], lbc.ID);
        }
        public string GetPassport(LinkingBankAccount lbc)
        {
            string toReturn = "{0}/Images/Imager.ashx?ACCOUNT_ID={1}&IMAGE_TYPE=PASSPORT";
            if (lbc.Passport == null)
            {
                using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new BankOneMobile.Services.CoreBankingService.SwitchingServiceClient())
                {

                    lbc.Passport = client.GetPassportByCustomerID(lbc.InstitutionCode, lbc.CustomerID);
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        new MobileAccountSystem(source).UpdateMobileAccount(lbc.TheMobileAccount as MobileAccount);
                    }
                }
            }
            return string.Format(toReturn, System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"], lbc.ID);
        }
        public string DisplayOpeneBy(string type, string value)
        {
            AccountRegistrarType acc = (AccountRegistrarType)Enum.Parse(typeof(AccountRegistrarType), type);
            return String.Format("{0} ({1})", value, acc.ToString());
        }
        public List<ILinkingBankAccount> GetByInstCode(IList<ILinkingBankAccount> accts, string instCode)
        {
            return accts.ToList();//.Where(x => x.InstitutionCode == instCode).ToList();
        }

    }
}
