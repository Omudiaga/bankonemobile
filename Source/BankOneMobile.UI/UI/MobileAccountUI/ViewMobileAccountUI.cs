﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using System.Web;
using AppZoneUI.Framework.Mods;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using System.Web.Security;
using System.Configuration;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using System.Diagnostics;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class ViewMobileAccountUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        Dictionary<string, string> Institutions; 
        string theInstCode = string.Empty;
        public string InstitutionDisplayName(MobileAccount mob,string instName)
        {
            if(!string.IsNullOrEmpty(instName))
            {
                return instName;
            }
            else
            {
                return mob.RecievingBankAccount.InstitutionName;
            }
        }
        public ViewMobileAccountUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Trace.TraceInformation("About to get all institutions");
                Institutions = new InstitutionSystem().GetAllInstitutionNames(source);
                Trace.TraceInformation("Gotten all institutions:{0}", Institutions == null ? "NULL" : Institutions.Count.ToString());
                PrePopulateFrom<MobileAccountModel>(x =>
                {
                    MobileAccountModel mod = new MobileAccountModel(); int total = 0;
                    //mod.MobileAccounts = new MobileAccountSystem(source).Search(x.MobileNumber, x.InstitutionCode, x.IsAgentAccount, x.LastName, x.OtherNames, x.RegistrarCode, x.IsActive, x.gender,0, 10, out total);
                    return mod;
                });
            }
            Trace.TraceInformation("Getting User");
            theInstCode = UserLogic.GetLoggedOnUserInstCode();
            Trace.TraceInformation(" User Gotten {0}", theInstCode);
            var searchList = (
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.LastName).AsSectionField<TextBox>(),
                            Map(x=>x.MobileNumber).AsSectionField<TextBox>(),
                            
                    
                                                       
                             
                            //Map(x => x.RegistrarID).AsSectionField<DropDownList>().Of<Agent>(new AgentSystem().GetActiveAgents()as IList<Agent>).AcceptBlank("All").ListOf(x=>x.Code, x=>x.ID).LabelTextIs("Select Registrar"),
                   
                    
                })
                
                });
            
                searchList.Add(new Column(
                        new List<IField>()
                        { 
                            
                          Map(x=>x.OtherNames).AsSectionField<TextBox>(),
                     
                    Map(x=>x.IsAgentAccount).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.IsActive()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Mobile Teller Account"),
                       
                    AddSectionButton()
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x=>
                    {
                                             
                        
                        return x;
                    }
                ),
                AddSectionButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .WithText("Reset")
                .SubmitTo(x => false)
                .OnFailureRedirectTo(("ViewMobileAccounts.aspx"))
                }));
            
            
            searchList.Add(new Column(
                        new List<IField>()
                        { 
                            
                          
                     
                    Map(x=>x.IsActive).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.Statuslist()).AcceptBlank("All")
                        .ListOf(x=>x,x=>x).LabelTextIs("Status"),
                       
                         Map(x => x.InstitutionCode).AsSectionField<DropDownList>()
                        .Of<string>(Institutions.Select(x=>x.Key).ToList()).AcceptBlank("All")
                        .ListOf(x => Institutions[x], x => x).LabelTextIs("Institution")
                    
                   
                
                }));
          
            UseFullView();

            AddNorthSection()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Find))
              .WithTitle("Search")
              .IsCollapsible()
              .IsFramed()
              .WithColumns(searchList);



            HasMany(x => x.MobileAccounts).AsCenter<Grid>().LabelTextIs("Mobile Wallets")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Phone))
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Mobile Accounts Export")
                    .ExportAllRows().SetPagingLimit<MobileAccountModel>(y => (int)System.Web.HttpContext.Current.Session["MA_TotalCount"]))
                .ApplyMod<ViewDetailsMod>(x => x.WithHeight(500).Popup<MobileAccountDetailUI>("Mobile Account Details")
                .PrePopulate<MobileAccount, MobileAccount>
                    (
                        //BECAUSE OF LAZY LOAD ISSUES
                        
                        zz => {
                            using(var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                Trace.TraceInformation("Inside Lazy Load Solution Code");
                                var mod =  new MobileAccountSystem(dataSource).GetMobileAccount(zz.ID);
                                return mod as MobileAccount;
                            }
                        }
                    ))                
                .Of<IMobileAccount>()
                .WithColumn(x =>string.IsNullOrEmpty(theInstCode)?x.RecievingBankAccount.InstitutionName:x.BankAccounts.Where(z=>z.InstitutionCode==theInstCode).FirstOrDefault().InstitutionName,"Institution")
                .WithColumn(x => x.LastName)
                .WithColumn(x => x.OtherNames)
                .WithColumn(x => x.MobilePhone)//, "MobileAccountDetail.aspx?id={0}", x => x.ID)
                .WithColumn(x => x.IsAgentAccount ? "Teller Account" : "CustomerAccount", "Account Type")
                .WithColumn(x => x.MobileAccountStatus, "Status")                
                .WithColumn(x=>x.DateCreated)
                .WithColumn(x => x.DateActivated.Year.ToString()=="1900"?"Not Yet Activated":x.DateActivated.ToString(),"Date Activated")
               
                
                //.WithCheckBoxes()//.Of<IMobileAccount>((HttpContext.Current.Session["MobileAccountSearchList"] as IList<MobileAccount>).Where(x=>x.MobileAccountStatus==MobileAccountStatus.Active))
                .WithRowNumbers()
                .IsPaged<MobileAccountModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    string instCode = string.Empty;
                    try
                    {
                        theInstCode = x.InstitutionCode;
                        Trace.TraceInformation("Inside UI ");
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception(" Inside UI"));
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            x.MobileAccounts = new MobileAccountSystem(source).Search(x.MobileNumber, x.InstitutionCode, x.IsAgentAccount, x.LastName, x.OtherNames, x.RegistrarCode, x.IsActive, x.gender, e.Start, e.Limit, out totalCount);
                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = ex.Message;
                    }
                    System.Web.HttpContext.Current.Session["MA_TotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    return x;
                });
            //AddButton()
            //    .WithText("Enable")
            //    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.DvdAdd))
            //    .SubmitTo(x =>

            //        new MobileAccountSystem().EnableSelectedAcounts((HttpContext.Current.Session["MobileAccountSearchList"] as List<IMobileAccount>)))
            //        .OnSuccessRedirectTo("ViewMobileAccounts.aspx")
            //        .OnFailureDisplay("Select at least 1 Mobile Account")
            //        .OnSuccessDisplay("Selcted Mobile Accouts succesfully enabled")
            //        .ConfirmWith("Enable  Selected Mobile Accounts?");
            //AddButton()
            //    .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Delete))
            //    .WithText("Disable")
            //    .SubmitTo(x =>
            //        new MobileAccountSystem().DisableSelectedAcounts(x.SelectedMobileAccounts))
            //        .OnSuccessRedirectTo("ViewMobileAccounts.aspx")

            //        .OnFailureDisplay("Select at least 1 Mobile Account")
            //        .OnSuccessDisplay("Selcted mobile accouts succesfully disabled")
            //        .ConfirmWith("Disable Selected Mobile Accounts?");


        }

        //public string InstitutionDisplayName (MobileAccount mob,string instName)
    }
}
