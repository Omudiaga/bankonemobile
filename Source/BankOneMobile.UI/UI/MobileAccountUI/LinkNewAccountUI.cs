﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.UI.Models;
using System.Web;
using AppZoneUI.Framework;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.UI.UI.MobileAccountUI
{
    public class LinkNewAccountUI : AppZoneUI.Framework.EntityUI<MobileAccountModel>
    {
        public LinkNewAccountUI()
        {
            PrePopulateFrom<MobileAccountModel>(x =>
            {
                return HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel;
            });
            AddSection()
                .WithTitle("Select(s) Accounts(s) To Link to this Phone Number")
                .WithFields
                (
                new List<IField>()
                {
                   Map(x=>x.CustomerID).AsSectionField<ITextLabel>().LabelTextIs("Account Number"),
                   Map(x=>x.MobileNumber).AsSectionField<ITextLabel>().LabelTextIs("Phone Number"),
                }
                );



            HasMany(x => x.Accounts).As<Grid>().Of<LinkingBankAccount>((HttpContext.Current.Session["MobileAccountModel"] as MobileAccountModel).Accounts as IList<LinkingBankAccount>)
           .WithColumn(x => x.BankAccount)

           .WithCheckBoxes()
           .WithRowNumbers();


            AddButton()
          .WithText("Create Mobile Account")

          .OnSuccessRedirectTo("MobileAccountsDetail.aspx?id={0}", x => x.MobileAccount.ID)

          .OnFailureDisplay(x => x.DisplayMessage)



          .SubmitTo(x =>
            {
                try
                {
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        x.MobileAccount = new MobileAccountSystem(source).CreateMobileAccount(x.MobileAccount as MobileAccount, x.Accounts);//(x.CustomerID, x.MobileNumber);
                        return true;
                    }

                }
                catch (Exception ex)
                {
                    x.DisplayMessage = ex.Message;
                    return false;

                }

            });







        }
    }
}
