﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using BankOneMobile.Services;
using BankOneMobile.UI.Models;
using BankOneMobile.Core.Contracts;
using System.Web;
using BankOneMobile.Data.Implementations;
using AppZoneUI.Framework.Mods;
using System.Diagnostics;
using BankOneMobile.Core.Exceptions;
using System.Web.Security;
using System.Configuration;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.UI.UI.AgentUI
{
    public class ListAgentOptimizationUI : AppZoneUI.Framework.EntityUI<AgentOptimizationModel>
    {
        bool appzoneUser = string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode());
        bool toUse = Convert.ToBoolean(ConfigurationManager.AppSettings["IsMFBScope"]);
        Dictionary<string, string> Institutions;
        BankOneMobile.Services.BranchServiceRef.Branch[] Branches;
        public ListAgentOptimizationUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            string code = UserLogic.GetLoggedOnUserInstCode();
            try
            {
                using (var client = new BankOneMobile.Services.BranchServiceRef.BranchServiceClient())
                {
                    var br = client.RetrieveAll(code);
                    if (br != null)
                    {
                        Branches = br;
                    }
                }
            }
            catch
            {

            }
            UseFullView();

      

            AddNorthSection()
                .IsFramed()
                .IsCollapsible()
            .WithTitle("Search")
              .WithColumns(
              new List<Column>(){
                new Column(
                        new List<IField>()
                        {
                     Map(x => x.TheBranch).AsSectionField<DropDownList>()
                        .Of<BankOneMobile.Services.BranchServiceRef.Branch>(Branches)
                        .ListOf(x => x.Name, x => x.ID).WithTypeAhead().AcceptBlank("---All---").LabelTextIs("Branch")
                        .ApplyMod<VisibilityMod>(x => x.SetVisibility<AgentOptimizationModel>(y =>!appzoneUser).SetDefaultValue<AgentOptimizationModel>(true)),
                     
                    AddSectionButton()
                    
                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x => x).NoValidate(),
                   
            AddSectionButton()
                .WithText("Reset")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.ArrowUndo))
                .SubmitTo(x => false).NoValidate()
                .OnFailureRedirectTo(("AgentOptimizationList.aspx"))
                }),                 
                });


            HasMany(x => x.TheAgentOptimizations).AsCenter<Grid>().LabelTextIs("Agent Optimization")
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Agent Optimization Export")
                    .ExportAllRows().SetPagingLimit<AgentOptimization>(y => (int)System.Web.HttpContext.Current.Session["MT_TotalCount"]))
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Table))
                .Of<AgentOptimization>()

                .IsPaged<AgentOptimizationModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                    x.InstitutionCode = string.IsNullOrEmpty(institutionCode) ? x.InstitutionCode : institutionCode;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        if (x.TheBranch != null) dic.Add("BranchID", x.TheBranch.ID);
                        x.TheAgentOptimizations = new AgentOptimizationSystem().Find(source, dic);
                    }
                    System.Web.HttpContext.Current.Session["MT_TotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    if (x.TheAgentOptimizations != null && x.TheAgentOptimizations.Count > 0)
                    {
                        foreach (var agent in x.TheAgentOptimizations)
                        {
                            var theAgent = new AgentSystem().GetAgentByPhoneNumber(agent.AgentPhoneNumber);
                            string agentName = string.Empty;
                            if (theAgent != null)
                            {
                                agentName = theAgent.LastName + ", " + theAgent.OtherNames;
                            }
                            string branchName = Branches.FirstOrDefault(y=>y.ID == agent.BranchID)==null?"":Branches.FirstOrDefault(y=>y.ID == agent.BranchID).Name;
                            agent.AgentName = agentName;
                            agent.Branch = branchName;
                        }
                    }
                    else
                        x.TheAgentOptimizationList = null;
                    return x;
                })
                .WithRowNumbers()
                 .WithColumn(x => x.AgentName, "Agent")
                 .WithColumn(x => x.AgentPhoneNumber, "Phone Number")
                 .WithColumn(x => x.Branch)
                .WithColumn(x => x.ShortestAgentPath, "SAP (km)")
                .WithColumn(x => x.OptimizedTotalClusterDistance, "OCTD (km)")
                .WithColumn(x => x.CdiIndex < 0 ? 0 : x.CdiIndex, "CDI Index (%)")
                .WithColumn(x => x.DateGenerated, "Date Generated");
        }

    }
}
