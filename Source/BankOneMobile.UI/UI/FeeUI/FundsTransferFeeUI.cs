﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using AppZoneUI.Framework;
using BankOneMobile.UI.Logic;
using BankOneMobile.Services;
using BankOneMobile.UI.Models;
using BankOneMobile.Core.Contracts;
using System.Web;
using BankOneMobile.Data.Implementations;
using AppZoneUI.Framework.Mods;
using System.Diagnostics;
using BankOneMobile.Core.Exceptions;
using System.Web.Security;
using System.Configuration;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.UI.UI.FeeUI
{

    public class AddFundsTransferFeeUI : AppZoneUI.Framework.EntityUI<FundsTransferFeeModel>
    {
        FundsTransferFee fundsTransferFee = null; string responseMessage = string.Empty;
        Dictionary<string, string> Institutions;

        public AddFundsTransferFeeUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            PrePopulateFrom<FundsTransferFeeModel>(x =>
            {
                //FundsTransferFeeModel model = HttpContext.Current.Session["FundsTransferFeeModel"] as FundsTransferFeeModel;
                //if (model.TheFundsTransferFee == null)
                //{
                //    model.TheFundsTransferFee = new FundsTransferFee();
                //}
                //return model;
                FundsTransferFeeModel model = new FundsTransferFeeModel();
                model.TheFundsTransferFee = new FundsTransferFee();
                return model;
            });

            AddSection()
                .WithColumns
                (
                new List<Column>(){
                    new Column(
                        new List<IField>()
                {
                     Map(x=>x.TransactionTypeName).AsSectionField<DropDownList>()
                        .Of<string>(MobileAccountLogic.FundsTransferTransactionTypeList().Select(x=>x.Value).ToList())
                        .ListOf(x=>x,x=>x).LabelTextIs("Transaction Type").Required(),
                    
                        Map(x=>x.IsFlat).AsSectionField<ICheckBox>().LabelTextIs("Flat?"),
                        Map(x=>x.IsPercent).AsSectionField<ICheckBox>().LabelTextIs("Percent?"),
                        Map(x=>x.Amount).AsSectionField<ITextBox>().LabelTextIs("Amount").Required(),
                        Map(x=>x.VATInPercentage).AsSectionField<ITextBox>().LabelTextIs("VAT Percentage").Required(),
                   
                }
                        ),                      

                }
                )
                .WithTitle("Fee Details");

            AddButton().WithText("Create Fee")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))

                .SubmitTo(x =>
                {
                    if(x.IsFlat && x.IsPercent)
                    {
                        x.DisplayMessage = "Fee type can either be flat or percentage based";
                                return false;
                    }
                    if (!x.IsFlat && !x.IsPercent)
                    {
                        x.DisplayMessage = "Indicate whether this Fee is flat or percentage based";
                        return false;
                    }
                    try
                    {
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            if (new FundsTransferFeeSystem().GetByName(UserLogic.GetLoggedOnUserInstCode(),(TransactionTypeName)Enum.Parse(typeof(TransactionTypeName),x.TransactionTypeName)) != null)
                            {
                                x.DisplayMessage = "Fee for this transaction type has already been configured";
                                return false;
                            }
                        }

                        x.TheFundsTransferFee.Amount = x.Amount;
                        x.TheFundsTransferFee.VATInPercentage = x.VATInPercentage;
                        x.TheFundsTransferFee.FlatOrPercent = x.IsFlat;
                        x.TheFundsTransferFee.InstitutionCode = UserLogic.GetLoggedOnUserInstCode();
                        x.TheFundsTransferFee.TransactionTypeName = (TransactionTypeName)Enum.Parse(typeof(TransactionTypeName), x.TransactionTypeName);

                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {                      

                            Object obj =
                             new FundsTransferFeeSystem(source).SaveFee(x.TheFundsTransferFee);

                            if (obj is FundsTransferFee)
                            {
                                x.TheFundsTransferFee = obj as FundsTransferFee;
                                x.DisplayMessage = String.Format("Fee Succesfully Saved");

                            }
                            else
                            {
                                x.DisplayMessage = "Fee Save Succesfully Logged For Approval";
                                return false;
                            }
                        }
                        return true;

                    }

                    catch (Exception ex)
                    {
                        new PANE.ERRORLOG.Error().LogToFile(ex);
                        x.DisplayMessage = String.Format("Error Saving Fee:{0}", ex.Message);
                        return false;

                    }

                })
             .OnSuccessDisplay(x => x.DisplayMessage)
             .OnSuccessRedirectTo("FundsTransferFeeList.aspx")
             .ConfirmWith("Are you sure you want to Create This Fee")
             .OnFailureDisplay(x => x.DisplayMessage);







        }

    }
    public class FundsTransferFeeDetailUI : AppZoneUI.Framework.EntityUI<FundsTransferFee>
    {
        string message = string.Empty;
        FundsTransferFee fundsTransferFee = null;
        private string ADD_EDIT_FUNDS_TRANSFER_FEE = "AddEditFundsTransferFee";
        private string ENABLE_DISABLE_FUNDS_TRANSFER_FEE = "EnableDisableFundsTransferFee";
        public FundsTransferFeeDetailUI()
        {
            PrePopulateFrom<FundsTransferFee>(x =>
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    FundsTransferFee fundsTransferFee =
                    new FundsTransferFeeSystem(source).GetFeeByID(x.ID) as FundsTransferFee;
                    return fundsTransferFee;
                }
            }).Using(x => x.ID);
           

            AddSection()
                .IsFormGroup()
             .WithTitle("Fee Details")
             .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TransactionTypeName).AsSectionField<ITextLabel>()
                        .LabelTextIs("Transaction Type"),
                        Map(x=>x.IsActive?"Active":"Inactive").AsSectionField<ITextLabel>().LabelTextIs("Status"),
                                       
                }),
                new Column(
                        new List<IField>()
                        {
                              Map(x=>x.FlatOrPercent?"Flat":"Percent").AsSectionField<ITextLabel>().LabelTextIs("Fee Type"),
                        Map(x=>x.Amount).AsSectionField<ITextLabel>().LabelTextIs("Amount"),
                        Map(x=>x.VATInPercentage+"%").AsSectionField<ITextLabel>().LabelTextIs("VAT"),
                        })});
            


            //if (Roles.IsUserInRole(ENABLE_DISABLE_AGENT))
            //{

            AddButton().WithText(x => x != null && x.IsActive ? "Disable" : "Enable").SubmitTo(x =>
            {
                try
                {
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        Object obj =
                         new FundsTransferFeeSystem(source).EnableDisable(x);

                        if (obj is FundsTransferFee)
                        {
                            x = obj as FundsTransferFee;
                            x.DisplayMessage = String.Format("Fee Successfully {0}", x.IsActive ? "Enabled" : "Disabled");
                            message = x.DisplayMessage;
                        }
                        else
                        {
                            x.DisplayMessage = "Fee Enable/Disable Succesfully Logged For Approval";
                            message = x.DisplayMessage;
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    x.DisplayMessage = string.Format("Error in Enable/Disable - {0}", ex.Message);
                    message = x.DisplayMessage;
                    return false;
                }
            })
                .OnSuccessDisplay(x => x.DisplayMessage)
                .OnFailureDisplay(x => x.DisplayMessage)
                //  .OnSuccessRedirectTo("AgentDetail.aspx?id={0}",x=>x.ID)
                .ConfirmWith(x => x != null && x.IsActive ? "Are you sure you want to disable this Fee?" : "Are you sure you want to enable this Fee?");
            AddButton()
                .WithText("Edit")
                .ApplyMod<ButtonPopupMod>(x => x.Popup<EditFundsTransferFeeUI>("Edit Fee").PrePopulate<FundsTransferFee, FundsTransferFee>(y => y));
        }

    }
    public class EditFundsTransferFeeUI : AppZoneUI.Framework.EntityUI<FundsTransferFee>
    {
        FundsTransferFee fundsTransferFee = null;

        public EditFundsTransferFeeUI()
        {

            WithTitle("Edit Fee");
            AddSection()
                  .WithTitle("Funds Transfer Fee")
                  .IsFormGroup()


                .WithColumns(
              new List<Column>(){
                    new Column(
                        new List<IField>()
                        {
                            Map(x=>x.TransactionTypeName).AsSectionField<ITextLabel>()
                        .LabelTextIs("Transaction Type"),
                                       
                }),
                new Column(
                        new List<IField>()
                        {
                              Map(x=>x.FlatOrPercent).AsSectionField<ICheckBox>().LabelTextIs("FlatOrPercent"),
                        Map(x=>x.Amount).AsSectionField<ITextBox>().LabelTextIs("Amount").Required(),
                        Map(x=>x.VATInPercentage).AsSectionField<ITextBox>().LabelTextIs("VAT Percentage").Required(),
                        })});

            AddButton()
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Disk))
                .WithText("Update")

                //.OnSuccessRedirectTo("AgentDetail.aspx?id={0}",x=>x.ID)

                .OnFailureDisplay(x => x.DisplayMessage)

                .OnSuccessDisplay(x => x.DisplayMessage)

                .SubmitTo(x =>
                {
                    try
                    {
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            object obj =
                                new FundsTransferFeeSystem(source).UpdateFee(x) as FundsTransferFee;
                            if (obj is FundsTransferFee)
                            {
                                x.DisplayMessage = "Fee Succesfully Updated";
                                return true;
                            }
                            else
                            {
                                x.DisplayMessage = "Fee Update Logged for Approval";
                                return true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        x.DisplayMessage = string.Format("Error Updating Fee:{0}", ex.Message);
                        return false;
                    }

                });


        }

    }
    public class ListFundsTransferFeeUI : AppZoneUI.Framework.EntityUI<FundsTransferFeeModel>
    {
        bool appzoneUser = string.IsNullOrEmpty(UserLogic.GetLoggedOnUserInstCode());
        bool toUse = Convert.ToBoolean(ConfigurationManager.AppSettings["IsMFBScope"]);
        Dictionary<string, string> Institutions;
        BankOneMobile.Services.BranchServiceRef.Branch[] Branches;
        public ListFundsTransferFeeUI()
        {
            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
            {
                Institutions = new InstitutionSystem(source).GetAllInstitutionNames(source);
            }
            string code = UserLogic.GetLoggedOnUserInstCode();
            try
            {
                using (var client = new BankOneMobile.Services.BranchServiceRef.BranchServiceClient())
                {
                    var br = client.RetrieveAll(code);
                    if (br != null)
                    {
                        Branches = br;
                    }
                }
            }
            catch
            {

            }
            UseFullView();

      

            AddNorthSection()
                .IsFramed()
                .IsCollapsible();
            AddSectionButton()

                .WithText("Search")
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Magnifier))
                .UpdateWith(x =>
                    {
                        // x.Agents =new  AgentSystem().Search(x.LastName, x.OtherNames, x.Code, x.PhoneNumber,x.AddressLocation, x.IsActive);

                        return x;
                    }
                ).NoValidate();

            HasMany(x => x.TheFundsTransferFees).AsCenter<Grid>().LabelTextIs("Funds Transfer Fees")
                .ApplyMod<ExportMod>(x => x.ExportToCsv().ExportToExcel()
                .SetFileName("Funds Transfer Fees Export")
                    .ExportAllRows().SetPagingLimit<FundsTransferFeeModel>(y => (int)System.Web.HttpContext.Current.Session["MT_TotalCount"]))
                .ApplyMod<IconMod>(x => x.WithIcon(Coolite.Ext.Web.Icon.Table))
                .ApplyMod<ViewDetailsMod>(x => x.Popup<FundsTransferFeeDetailUI>("Fee Details"))
                .Of<FundsTransferFee>()

               // .WithColumn(x => x.IsMobileTeller, "Mobile Teller?")

                .IsPaged<FundsTransferFeeModel>(10, (x, e) =>
                {
                    int totalCount = -1;
                    string institutionCode = UserLogic.GetLoggedOnUserInstCode();
                    x.InstitutionCode = string.IsNullOrEmpty(institutionCode) ? x.InstitutionCode : institutionCode;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        x.TheFundsTransferFees = new FundsTransferFeeSystem(source).FindFeesBy(dic);
                    }
                    System.Web.HttpContext.Current.Session["MT_TotalCount"] = e.TotalCount = totalCount;
                    //e.TotalCount = totalCount;
                    return x;
                })
                .WithRowNumbers()
                 .WithColumn(x => x.TransactionTypeName.ToString(),"Fee Type")
                 .WithColumn(x => x.FlatOrPercent?"Flat":"Percent","Type")
                .WithColumn(x => x.Amount)
                .WithColumn(x => x.VATInPercentage+"%","VAT")
                .WithColumn(x => x.IsActive ? "Active" : "In Active", "Status");
        }

    }
}
