﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOA.Framework.FunctionHelper
{
    public static class Utility
    {
        public static FS.UserCategory ConvertCategoryFrom(PANE.Framework.Functions.DTO.UserCategory userCategory)
        {
            switch (userCategory)
            {
                case PANE.Framework.Functions.DTO.UserCategory.Customer:
                    return FS.UserCategory.Customer;
                case PANE.Framework.Functions.DTO.UserCategory.Mfb:
                    return FS.UserCategory.Mfb;
                case PANE.Framework.Functions.DTO.UserCategory.CorrespondentBank:
                //case PANE.Framework.Functions.DTO.UserCategory.SchemeOwner:
                    return FS.UserCategory.CorrespondentBank;
                case PANE.Framework.Functions.DTO.UserCategory.ServiceProvider:
                    return FS.UserCategory.ServiceProvider;

                default:
                    return FS.UserCategory.Customer;
            }
        }
    }
}
