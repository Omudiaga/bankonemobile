﻿namespace SOA.Framework.FunctionHelper
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.Web.Security;
    using System.Collections.Generic;
    using PANE.Framework.Functions.DTO;
    using PANE.Framework.Functions.DAO;
    using PANE.Framework.Approval.DAO;
    //using SOA.Framework.MS;
    using PANE.Framework.Approval.DTO;
    using System.Linq;
    using SOA.Framework.FunctionHelper.DAO;
    using PANE.Framework.NHibernateManager.Configuration;

    [ServiceContract(Namespace = ""), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FunctionService
    {
        [OperationContract]
        public List<FunctionItem> GetFunctionsForApprovalConfiguration(UserCategory userCategory)
        {
            //Instead of retrieving all the FunctionItems, we'll retrieve those for the Current Users' category
            //that have IsApproval as true.
            List<FunctionItem> functions = SOA.Framework.FunctionHelper.DAO.FunctionItemDAO.RetrieveFunctionsForApprovalConfiguration(userCategory);
            return functions;
        }

        [OperationContract]
        public List<FunctionItem> RetrieveAll(UserCategory userCategory)
        {
            //List<FunctionItem> functions = SOA.Framework.FunctionHelper.DAO.FunctionItemDAO.RetrieveAll("", DatabaseSource.Core);
            //return functions;

            //Instead of retrieving all the FunctionItems, we'll retrieve those for the Current Users' category
            List<FunctionItem> functions = SOA.Framework.FunctionHelper.DAO.FunctionItemDAO.RetrieveFunctions(userCategory);
            return functions;
        }

        /// <summary>
        /// Usually going to be used on the User Role page where a User Role
        /// </summary>
        /// <param name="functionItems"></param>
        /// <returns></returns>
        [OperationContract]
        public bool SaveThUserRoleFunctionItems(string mfbCode, List<UserRoleFunctionItem> userRoleFunctionItems)
        {
            //Save all the UserRole FunctionItems and their corresponding SubRoles
            foreach (UserRoleFunctionItem urfi in userRoleFunctionItems)
            {
                urfi.MFBCode = mfbCode;
                UserRoleFunctionItemDAO.Save(urfi);

                if (urfi.SubUserRoleItems != null)
                {
                    foreach (UserRoleFunctionSubRoleItem urfsr in urfi.SubUserRoleItems)
                    {
                        urfsr.MFBCode = mfbCode;
                        urfsr.TheUserRoleFunctionItem = urfi;
                        UserRoleFunctionSubRoleItemDAO.Save(urfsr);
                    }
                }
            }
            UserRoleFunctionSubRoleDAO.CommitChanges(mfbCode);
            return true;
        }

        [OperationContract]
        public bool UpdateThUserRoleFunctionItems(string mfbCode, List<UserRoleFunctionItem> userRoleFunctionItems, long id)
        {
            bool updated = false;
            List<UserRoleFunctionItem> userRoleFunctionItemsOld = UserRoleFunctionItemDAO.RetrieveByUserRoleID(mfbCode, id);
            foreach (UserRoleFunctionItem userRoleFunctionItem in userRoleFunctionItemsOld)
            {
                userRoleFunctionItem.MFBCode = mfbCode;
                UserRoleFunctionItemDAO.Delete(userRoleFunctionItem);

            }

            updated = SaveThUserRoleFunctionItems(mfbCode, userRoleFunctionItems);
            return updated;
        }

        [OperationContract]
        public FunctionItem ConvertFunction(Function function)
        {
            return new FunctionItem(function);
        }

        [OperationContract]
        public FunctionItem GetFunctionByID(long id)
        {

            return FunctionItemDAO.RetreiveByID(id);

        }

        [OperationContract]
        public FunctionItem GetFunctionByRoleName(string  rolename, UserCategory userCategory)
        {
            return FunctionItemDAO.RetrieveByRoleName(rolename, userCategory);
        }

        [OperationContract]
        public List<FunctionItem> GetFunctionsForUserRole(string mfbCode,long id) {
            List<long> ids = new List<long>();
            if (mfbCode == null) throw new FaultException("MFBCode is null!!");
            List<UserRoleFunctionItem> userRoleFunctionItems = UserRoleFunctionItemDAO.RetrieveByUserRoleID(mfbCode, id);
            if (userRoleFunctionItems != null)
            {
                foreach (UserRoleFunctionItem userRoleFunctionItem in userRoleFunctionItems)
                {
                    if (userRoleFunctionItem != null)
                    {
                        ids.Add(userRoleFunctionItem.TheFunctionItemID);
                    }
                }
            }
            return FunctionItemDAO.RetreiveByIDs(ids.ToArray());
        }

        [OperationContract]
        public List<FunctionItem> GetDependencyFunctionsForUserRole(string mfbCode, long id)
        {
            List<long> ids = new List<long>();

            List<UserRoleFunctionItem> userRoleFunctionItems = UserRoleFunctionItemDAO.RetrieveByUserRoleID(mfbCode, id);

            foreach (UserRoleFunctionItem userRoleFunctionItem in userRoleFunctionItems)
            {

                ids.Add(userRoleFunctionItem.TheFunctionItemID);
            }

            List<FunctionItem> mfy = FunctionItemDAO.RetreiveByIDs(ids.ToArray());
            return FunctionItemDAO.RetreiveByIDs(ids.ToArray()).Where(f => f.Dependency == true).ToList<FunctionItem>();
        }

        [OperationContract]
        public List<FunctionItem> RetrieveFunctionItemsForUserRoleID(string mfbCode, long userRoleID)
        {
            //Get the attached UserRole FunctionItems.
            List<UserRoleFunctionItem> urfs = UserRoleFunctionItemDAO.RetrieveByUserRoleID(mfbCode, userRoleID);

            //Use the functionItem IDs and retreive the corresponding FunctionItems.
            long[] functionItemIDs = urfs.Select(f => f.TheFunctionItemID).Distinct().ToArray();
            List<FunctionItem> functionItemsForCurrentUser = FunctionItemDAO.RetreiveByIDs(functionItemIDs);
            return functionItemsForCurrentUser;
        }

    }
}
