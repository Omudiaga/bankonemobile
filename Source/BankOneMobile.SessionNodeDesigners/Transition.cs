﻿//-----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//-----------------------------------------------------------------------------

namespace BankOneMobile.SessionNodes
{
    using System.ComponentModel;
    using System.Activities;
    using System.Windows.Markup;

    /// <summary>
    /// This class represents a Transition of a State.
    /// </summary>
    public sealed class Transition
    {
        /// <summary>
        /// DisplayName of the Transition
        /// </summary>
        [Browsable(false)]
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// It represents the Trigger activity of the Transition.
        /// When the Trigger activity is completed, the StateMachine will start to evaluate whether the Transition should be taken.
        /// It's required.
        /// </summary>
        [DefaultValue(null)]
        [Browsable(false)]
        public Activity Trigger
        {
            get;
            set;
        }

        /// <summary>
        /// It represents the target State of the Transition.
        /// It's required.
        /// </summary>
        [DependsOn("Trigger")]
        [DefaultValue(null)]
        [Browsable(false)]
        public State To
        {
            get;
            set;
        }

        /// <summary>
        /// It represents the Action activity which should be executed when the Transtion is taken.
        /// It's optional.
        /// </summary>
        [DependsOn("To")]
        [DefaultValue(null)]
        [Browsable(false)]
        public Activity Action
        {
            get;
            set;
        }

        /// <summary>
        /// It represents the Condition to decide whether the Transition should be taken after the Trigger activity is completed.
        /// It's optional. 
        /// If the Condition is null, the Transition would always be taken when the Trigger activity is completed.
        /// </summary>
        [DependsOn("Action")]
        [DefaultValue(null)]
        [Browsable(false)]
        public Activity<bool> Condition
        {
            get;
            set;
        }

        /// <summary>
        /// Source represents source state of transition
        /// </summary>
        internal State Source
        {
            get;
            set;
        }
    }
}
