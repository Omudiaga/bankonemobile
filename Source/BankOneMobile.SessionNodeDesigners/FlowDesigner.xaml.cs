﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Microsoft.Activities.Statements.Design.FreeFormEditing;
using System.Activities.Presentation;
using System.Activities.Presentation.Model;
using System.Activities.Presentation.View;
using System.Windows.Threading;
using Microsoft.Activities.Statements.Design;
using System.Activities;
using System.Activities.Presentation.Services;
using BankOneMobile.SessionNodes;
using System.ComponentModel;
using System.Collections.Specialized;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using BankOneMobile.SessionNodeDesigners.Actions;

namespace BankOneMobile.SessionNodeDesigners
{
    // Interaction logic for ActivityDesigner1.xaml
    public partial class FlowDesigner : ICompositeView
    {

        const double startSymbolTopMargin = 10.0;
        const double GridSize = 10;
        const double DefaultNodeWidth = 100;
        const double DefaultNodeHeight = 25;
        const double DefaultWidth = 600;
        const double DefaultHeight = 600;
       
        const string ShapeLocationViewStateKey = "ShapeLocation";
        const string ShapeSizeViewStateKey = "ShapeSize";
        const double InitialFinalWidth = 60;
        const double InitialFinalHeight = 75;
        const double ChildViewMinWidth = 20;
        internal const string ConnectorLocationViewStateKey = "ConnectorLocation";
        internal const string WidthViewStateKey = "NodeContainerWidth";
        internal const string HeightViewStateKey = "NodeContainerHeight";
        internal const string ChildPropertyName = "Nodes";

        public static readonly DependencyProperty ConnectionPointsProperty = DependencyProperty.RegisterAttached(
           "ConnectionPoints",
           typeof(List<ConnectionPoint>),
           typeof(FlowDesigner),
           new FrameworkPropertyMetadata());

        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(
            "IsReadOnly",
            typeof(bool), typeof(FlowDesigner),
            new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty ConnectorModelItemProperty = DependencyProperty.RegisterAttached(
        "ConnectorModelItem",
        typeof(ModelItem),
        typeof(FlowDesigner),
        new FrameworkPropertyMetadata());

        bool populated = false;

        // To keep track of all child state designer
        Dictionary<ModelItem, UIElement> modelItemToUIElement;


        // To register / unregister the editor as the default composite view on its parent
        ICompositeViewEvents compositeViewEvents = null;

        // shapeLocations is useful to avoid pasting on existing shapes.
        HashSet<Point> shapeLocations = null;

        // Flag whether the view state change has already been committed in the UI.
        bool internalViewStateChange = false;

        // To keep track of transition collections that the outmost editor listens to the CollectionChanged events.
        HashSet<ModelItem> listenedTransitionCollections;


        // activeSrcConnectionPoint is required for connector creation gesture to store the source of the link.
        ConnectionPoint activeSrcConnectionPoint;

        // selectedConnector is a placeholder for the last connector selected. 
        Connector selectedConnector = null;

        // Used for connector creation
        UIElement lastConnectionPointMouseUpElement = null;

        // Only used by the outmost editor to keep track of transitions added when editing scope completes
        List<ModelItem> transitionModelItemsAdded;

        // To keep track of whether the initial state is changed during an EditingScope
        bool initialStateChanged = false;

        // The initial node symbol
        UIElement initialNode = null;


        const string ExpandViewStateKey = "IsExpanded";
        internal const string InitialNodePropertyName = "InitialNode";
        internal const string VariablesPropertyName = "Variables";

        protected override void OnModelItemChanged(object newItem)
        {
            if (this.Context != null)
            {
                ViewStateService viewStateService = this.Context.Services.GetService<ViewStateService>();
                if (viewStateService != null)
                {
                    // Make StateMachine designer always collapsed by default, but only if the user didn't explicitly specify collapsed or expanded.
                    bool? isExpanded = (bool?)viewStateService.RetrieveViewState((ModelItem)newItem, ExpandViewStateKey);
                    if (isExpanded == null)
                    {
                        viewStateService.StoreViewState((ModelItem)newItem, ExpandViewStateKey, false);
                    }
                }
            }
            base.OnModelItemChanged(newItem);
        }


        public FlowDesigner()
        {
            InitializeComponent();
            this.modelItemToUIElement = new Dictionary<ModelItem, UIElement>();
            this.shapeLocations = new HashSet<Point>();
            this.listenedTransitionCollections = new HashSet<ModelItem>();
            this.transitionModelItemsAdded = new List<ModelItem>();

            Binding readOnlyBinding = new Binding();
            readOnlyBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DesignerView), 1);
            readOnlyBinding.Path = new PropertyPath(DesignerView.IsReadOnlyProperty);
            readOnlyBinding.Mode = BindingMode.OneWay;
            this.SetBinding(IsReadOnlyProperty, readOnlyBinding);

            this.Loaded += (s, e) =>
            {
                if (this.ShouldInitialize())
                {
                   // WorkflowViewElement parent = this;//Utility.GetVisualAncestor<WorkflowViewElement>(this);
                    //this.ModelItem = parent.ModelItem;
                    this.FlowModelItem = this.ModelItem;//FlowDesigner.GetFlowModelItem(this.ModelItem);
                    //this.Context = parent.Context;
                    this.compositeViewEvents = this;
                    if (this.compositeViewEvents != null)
                    {
                        this.compositeViewEvents.RegisterDefaultCompositeView(this);
                    }
                    if (!this.populated)
                    {
                        this.Populate();
                        this.populated = true;
                    }
                }
            };

            this.Unloaded += (s, e) =>
            {
                if (this.compositeViewEvents != null)
                {
                    (compositeViewEvents).UnregisterDefaultCompositeView(this);
                    this.compositeViewEvents = null;
                }
                if (this.populated)
                {
                    this.Cleanup();
                    this.populated = false;
                }
            };
        }


        bool ShouldInitialize()
        {
            //WorkflowViewElement parent = this;//Utility.GetVisualAncestor<WorkflowViewElement>(this);
            return this.ModelItem != null && this.ShowExpanded;
        }

        internal static ModelItem GetFlowModelItem(ModelItem modelItem)
        {
            ModelItem currentModelItem = modelItem;
            while (currentModelItem != null && currentModelItem.ItemType != typeof(Flow))
            {
                currentModelItem = currentModelItem.Parent;
            }
            return currentModelItem;
        }

        //public ModelItem ModelItem
        //{
        //    get { return (ModelItem)GetValue(ModelItemProperty); }
        //    set { SetValue(ModelItemProperty, value); }
        //}

        ModelItem FlowModelItem
        {
            get;
            set;
        }

        

        #region PopulateCleanup

        void Populate()
        {
            // Keep track of the outmost editor, which may not be accessible by traversing the visual tree when the designer is deleted.
            

            this.panel.LocationChanged += new LocationChangedEventHandler(OnFreeFormPanelLocationChanged);
            this.panel.ConnectorMoved += new ConnectorMovedEventHandler(OnFreeFormPanelConnectorMoved);
            this.panel.LayoutUpdated += new EventHandler(OnFreeFormPanelLayoutUpdated);
            this.panel.RequiredSizeChanged += new RequiredSizeChangedEventHandler(OnFreeFormPanelRequiredSizeChanged);

            this.ViewStateService.ViewStateChanged += new ViewStateChangedEventHandler(OnViewStateChanged);

            this.ModelItem.Properties[ChildPropertyName].Collection.CollectionChanged += new NotifyCollectionChangedEventHandler(OnStateCollectionChanged);
            this.ModelItem.PropertyChanged += new PropertyChangedEventHandler(this.OnModelPropertyChanged);

            ModelTreeManager modelTreeManager = this.Context.Services.GetService<ModelTreeManager>();
            modelTreeManager.EditingScopeCompleted += new EventHandler<EditingScopeEventArgs>(this.OnEditingScopeCompleted);

           

            object widthViewState = this.ViewStateService.RetrieveViewState(this.ModelItem, WidthViewStateKey);
            if (widthViewState != null)
            {
                this.Width = (double)widthViewState;
            }

            object heightViewState = this.ViewStateService.RetrieveViewState(this.ModelItem, HeightViewStateKey);
            if (heightViewState != null)
            {
                this.Height = (double)heightViewState;
            }


            panel.Children.Clear();
            this.modelItemToUIElement.Clear();
            this.shapeLocations.Clear();

            this.AddInitialNode();

            foreach (ModelItem nodeModel in this.ModelItem.Properties[ChildPropertyName].Collection)
            {
                ModelItemCollection transitions = nodeModel.Properties[StateDesignerConstants.TransitionsPropertyName].Collection;
                if (!this.listenedTransitionCollections.Contains(transitions))
                {
                    transitions.CollectionChanged += new NotifyCollectionChangedEventHandler(this.OnTransitionCollectionChanged);
                    this.listenedTransitionCollections.Add(transitions);
                }
            }
            this.AddStateVisuals(this.ModelItem.Properties[ChildPropertyName].Collection);

               
                

            
        }

        void Cleanup()
        {
            this.panel.Children.Clear();
            // Cleaning up the designers as they might be re-used.
            foreach (UIElement element in this.modelItemToUIElement.Values)
            {
                element.MouseEnter -= new MouseEventHandler(OnChildElementMouseEnter);
                element.MouseLeave -= new MouseEventHandler(OnChildElementMouseLeave);
                ((FrameworkElement)element).SizeChanged -= new SizeChangedEventHandler(OnChildElementSizeChanged);
            }
            this.modelItemToUIElement.Clear();
            this.panel.LocationChanged -= new LocationChangedEventHandler(OnFreeFormPanelLocationChanged);
            this.panel.ConnectorMoved -= new ConnectorMovedEventHandler(OnFreeFormPanelConnectorMoved);
            this.panel.LayoutUpdated -= new EventHandler(OnFreeFormPanelLayoutUpdated);
            this.panel.RequiredSizeChanged -= new RequiredSizeChangedEventHandler(OnFreeFormPanelRequiredSizeChanged);
            this.ViewStateService.ViewStateChanged -= new ViewStateChangedEventHandler(OnViewStateChanged);

            this.ModelItem.PropertyChanged -= new PropertyChangedEventHandler(this.OnModelPropertyChanged);

            ModelTreeManager modelTreeManager = this.Context.Services.GetService<ModelTreeManager>();
            modelTreeManager.EditingScopeCompleted -= new EventHandler<EditingScopeEventArgs>(this.OnEditingScopeCompleted);
        }

        #endregion

        #region InitialNode

        void AddInitialNode()
        {
            // Instantiate the initial node
            InitialNode initialNode = new InitialNode();
            SetInitialFinalViewSize(initialNode);
            this.initialNode = initialNode;
            this.PopulateConnectionPoints(this.initialNode);
            this.initialNode.MouseEnter += new MouseEventHandler(OnChildElementMouseEnter);
            this.initialNode.MouseLeave += new MouseEventHandler(OnChildElementMouseLeave);
            // Add the initial node and set its location and size.
            this.initialNode.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
            double startHeight = this.initialNode.DesiredSize.Height;
            double startWidth = this.initialNode.DesiredSize.Width;
            Point startPoint = new Point(panel.MinWidth / 2, startSymbolTopMargin + startHeight / 2);
            Point startLocation = SnapVisualToGrid(this.initialNode, startPoint, new Point(-1, -1));
            this.panel.Children.Add(this.initialNode);
            FreeFormPanel.SetLocation(this.initialNode, startLocation);
            FreeFormPanel.SetChildSize(this.initialNode, new Size(startWidth, startHeight));
        }

        static void SetInitialFinalViewSize(ContentControl control)
        {
            control.Width = InitialFinalWidth;
            control.Height = InitialFinalHeight;
        }

        void AddInitialNodeConnector()
        {
            //Debug.Assert(this.ModelItem.ItemType == typeof(Flow), "Only StateMachine should have initial state.");
            List<Connector> attachedConnectors = FlowDesigner.GetAttachedConnectors(this.initialNode);
            if (attachedConnectors.Count == 0)
            {
                this.AddConnector(this.initialNode, (UIElement)this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].Value.View, this.ModelItem);
            }
        }

        // Create a connector by view state of the connector model item, and if failed, just create a connector using the connection points
        // of the source and destination designers. Then add the connector created to the free form panel.
        void AddConnector(UIElement sourceDesigner, UIElement destinationDesigner, ModelItem connectorModelItem)
        {
           // Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");

            Connector connector = CreateConnectorByConnectorModelItemViewState(sourceDesigner, destinationDesigner, connectorModelItem);
            if (connector == null)
            {
                ConnectionPoint sourceConnectionPoint;
                ConnectionPoint destinationConnectionPoint;
                GetEmptySrcDestConnectionPoints(sourceDesigner, destinationDesigner, out sourceConnectionPoint, out destinationConnectionPoint);
                if (sourceConnectionPoint != null && destinationConnectionPoint != null)
                {
                    PointCollection connectorPoints = new PointCollection(ConnectorRouter.Route(this.panel, sourceConnectionPoint, destinationConnectionPoint));
                    this.ViewStateService.StoreViewState(connectorModelItem, ConnectorLocationViewStateKey, connectorPoints);
                    connector = CreateConnector(sourceConnectionPoint, destinationConnectionPoint, connectorPoints, connectorModelItem);
                }
            }
            if (connector != null)
            {
                this.panel.Children.Add(connector);
            }
        }

        Connector CreateConnectorByConnectorModelItemViewState(UIElement source, UIElement dest, ModelItem connectorModelItem)
        {
            //Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");
            Connector connector = null;
            object connectorLocation = this.ViewStateService.RetrieveViewState(connectorModelItem, ConnectorLocationViewStateKey);
            PointCollection locationPts = connectorLocation as PointCollection;
            if (locationPts != null)
            {
                ConnectionPoint srcConnPoint, destConnPoint;
                srcConnPoint = ConnectionPointHitTest(source, locationPts[0], ConnectionPointType.Outgoing);
                destConnPoint = ConnectionPointHitTest(dest, locationPts[locationPts.Count - 1], ConnectionPointType.Incoming);
                if (srcConnPoint == null && destConnPoint == null)
                {
                    FlowDesigner.GetEmptySrcDestConnectionPoints(source, dest, out srcConnPoint, out destConnPoint);
                }
                else if (srcConnPoint == null && destConnPoint != null)
                {
                    List<ConnectionPoint> srcConnectionPoints = GetEmptyConnectionPoints(source);
                    if (srcConnectionPoints.Count > 0)
                    {
                        srcConnPoint = FlowDesigner.GetClosestConnectionPointNotOfType(destConnPoint, srcConnectionPoints, ConnectionPointType.Incoming);
                    }
                }
                else if (destConnPoint == null && srcConnPoint != null)
                {
                    List<ConnectionPoint> destConnectionPoints = GetEmptyConnectionPoints(dest);
                    if (destConnectionPoints.Count > 0)
                    {
                        destConnPoint = FlowDesigner.GetClosestConnectionPointNotOfType(srcConnPoint, destConnectionPoints, ConnectionPointType.Outgoing);
                    }
                }
                if (srcConnPoint != null && destConnPoint != null)
                {
                    connector = this.CreateConnector(srcConnPoint, destConnPoint, locationPts, connectorModelItem);
                }
            }
            return connector;
        }

        static void GetEmptySrcDestConnectionPoints(UIElement source, UIElement dest, out ConnectionPoint srcConnPoint, out ConnectionPoint destConnPoint)
        {
            srcConnPoint = null;
            destConnPoint = null;
            List<ConnectionPoint> srcConnectionPoints = GetEmptyConnectionPoints(source);
            List<ConnectionPoint> destConnectionPoints = GetEmptyConnectionPoints(dest);
            if (srcConnectionPoints.Count > 0 && destConnectionPoints.Count > 0)
            {
                GetClosestConnectionPointPair(srcConnectionPoints, destConnectionPoints, out srcConnPoint, out destConnPoint);
            }
        }


        Connector CreateConnector(ConnectionPoint srcConnPoint, ConnectionPoint destConnPoint, PointCollection points, ModelItem connectorModelItem)
        {
           // Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");
            Connector connector = new Connector();
            connector.FocusVisualStyle = null;
            connector.Focusable = true;
            DesignerView.SetCommandMenuMode(connector, CommandMenuMode.NoCommandMenu);

            SetConnectorLabel(connector, connectorModelItem);
            
            //if (connectorModelItem != null)// && connectorModelItem.ItemType == typeof(Transition))
            //{
            //    SetConnectorLabel(connector, connectorModelItem);
            //    SetConnectorStartDotToolTip(connector.startDotGrid, connectorModelItem);
            //    connector.IsTransition = true;
            //    connector.ToolTip = "EditTransitionTooltip";//SR.EditTransitionTooltip;
            //    connector.startDotGrid.MouseDown += new MouseButtonEventHandler(OnConnectorStartDotMouseDown);
            //    connector.startDotGrid.MouseUp += new MouseButtonEventHandler(OnConnectorStartDotMouseUp);
            //}
            connector.GotKeyboardFocus += new KeyboardFocusChangedEventHandler(OnConnectorGotKeyboardFocus);
            connector.RequestBringIntoView += new RequestBringIntoViewEventHandler(OnConnectorRequestBringIntoView);
            connector.GotFocus += new RoutedEventHandler(OnConnectorGotFocus);
            connector.LostFocus += new RoutedEventHandler(OnConnectorLostFocus);
            connector.MouseDoubleClick += new MouseButtonEventHandler(OnConnectorMouseDoubleClick);
            connector.MouseDown += new MouseButtonEventHandler(OnConnectorMouseDown);
            connector.KeyDown += new KeyEventHandler(OnConnectorKeyDown);
            connector.ContextMenuOpening += new ContextMenuEventHandler(OnConnectorContextMenuOpening);
            SetConnectorSrcDestConnectionPoints(connector, srcConnPoint, destConnPoint);
            FlowDesigner.SetConnectorModelItem(connector, connectorModelItem);
            connector.Unloaded += new RoutedEventHandler(OnConnectorUnloaded);
            connector.Points = points;
            return connector;
        }

        static void SetConnectorStartDotToolTip(FrameworkElement startDot, ModelItem connectorModelItem)
        {
            //ModelItem triggerModelItem = connectorModelItem.Properties[TransitionDesigner.TriggerPropertyName].Value as ModelItem;
            //string triggerName = null;
            //if (triggerModelItem == null)
            //{
            //    triggerName = "(null)";
            //}
            //else
            //{
            //    ModelItem displayNameModelItem = triggerModelItem.Properties["DisplayName"].Value;
            //    if (displayNameModelItem != null)
            //    {
            //        triggerName = displayNameModelItem.GetCurrentValue() as string;
            //    }
            //}
            //startDot.ToolTip = SR.TriggerNameToolTip + triggerName + Environment.NewLine + SR.SharedTriggerToolTip;
        }
        

        static void SetConnectorLabel(Connector connector, ModelItem connectorModelItem)
        {
            connector.SetBinding(Connector.LabelTextProperty, new Binding()
            {
                Source = connectorModelItem,
                Path = new PropertyPath("DisplayName")
            });

            if (Convert.ToString(connectorModelItem.Properties["DisplayName"].ComputedValue) == "failure")
            {
                connector.labelTextBlock.FontWeight = FontWeights.Bold;
                connector.labelTextBlock.Foreground = new SolidColorBrush(Colors.White);
                connector.LayoutRoot.BorderBrush =  connector.LayoutRoot.Background =
                    connector.TopLine.Stroke = connector.BottomLine.Stroke = new SolidColorBrush(Colors.Red);
                //connector.arrowShape.Stroke = new SolidColorBrush(Colors.Maroon);
                connector.TopLine.StrokeThickness = connector.BottomLine.StrokeThickness = 1;
            }
        }

        static void SetConnectorSrcDestConnectionPoints(Connector connector, ConnectionPoint srcConnectionPoint, ConnectionPoint destConnectionPoint)
        {
            FreeFormPanel.SetSourceConnectionPoint(connector, srcConnectionPoint);
            FreeFormPanel.SetDestinationConnectionPoint(connector, destConnectionPoint);
            srcConnectionPoint.AttachedConnectors.Add(connector);
            destConnectionPoint.AttachedConnectors.Add(connector);
        }

        
        static ConnectionPoint ConnectionPointHitTest(UIElement element, Point hitPoint, ConnectionPointType type)
        {
            ConnectionPoint hitConnectionPoint = null;
            List<ConnectionPoint> connectionPoints = type == ConnectionPointType.Default ? FlowDesigner.GetConnectionPoints(element) : FlowDesigner.GetConnectionPoints(element).Where(x => x.PointType == type || x.PointType == ConnectionPointType.Default).ToList();
            foreach (ConnectionPoint connPoint in connectionPoints)
            {
                if (connPoint != null)
                {
                    // We need to transform the connection point location to be relative to the outmost panel
                    FreeFormPanel panel = Utility.GetVisualAncestor<FreeFormPanel>(element);
                    if (new Rect(panel.GetLocationRelativeToOutmostPanel(connPoint.Location) + connPoint.HitTestOffset, connPoint.HitTestSize).Contains(hitPoint))
                    {
                        hitConnectionPoint = connPoint;
                        break;
                    }
                }
            }
            return hitConnectionPoint;
        }

        static internal ConnectionPoint GetClosestConnectionPoint(ConnectionPoint srcConnPoint, List<ConnectionPoint> destConnPoints, out double minDist)
        {
            minDist = double.PositiveInfinity;
            double dist = 0;
            ConnectionPoint closestPoint = null;
            foreach (ConnectionPoint destConnPoint in destConnPoints)
            {
                if (srcConnPoint != destConnPoint)
                {
                    dist = DesignerGeometryHelper.DistanceBetweenPoints(srcConnPoint.Location, destConnPoint.Location);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        closestPoint = destConnPoint;
                    }
                }
            }

            return closestPoint;
        }

        static ConnectionPoint GetClosestConnectionPointNotOfType(ConnectionPoint srcConnectionPoint, List<ConnectionPoint> targetConnectionPoints, ConnectionPointType illegalConnectionPointType)
        {
            double minDist;
            List<ConnectionPoint> filteredConnectionPoints = new List<ConnectionPoint>();
            foreach (ConnectionPoint connPoint in targetConnectionPoints)
            {
                if (connPoint.PointType != illegalConnectionPointType && !connPoint.Equals(srcConnectionPoint) && connPoint.AttachedConnectors.Count == 0)
                {
                    filteredConnectionPoints.Add(connPoint);
                }
            }
            return GetClosestConnectionPoint(srcConnectionPoint, filteredConnectionPoints, out minDist);
        }

        static void GetClosestConnectionPointPair(List<ConnectionPoint> srcConnPoints, List<ConnectionPoint> destConnPoints, out ConnectionPoint srcConnPoint, out ConnectionPoint destConnPoint)
        {
            double minDist = double.PositiveInfinity;
            double dist;
            ConnectionPoint tempConnPoint;
            srcConnPoint = null;
            destConnPoint = null;
            foreach (ConnectionPoint connPoint in srcConnPoints)
            {
                tempConnPoint = GetClosestConnectionPoint(connPoint, destConnPoints, out dist);
                if (dist < minDist)
                {
                    minDist = dist;
                    srcConnPoint = connPoint;
                    destConnPoint = tempConnPoint;
                }
            }
            Debug.Assert(srcConnPoint != null, "No ConnectionPoint found");
            Debug.Assert(destConnPoint != null, "No ConnectionPoint found");
        }


        internal static List<ConnectionPoint> GetEmptyConnectionPoints(UIElement designer)
        {
            List<ConnectionPoint> connectionPoints = FlowDesigner.GetConnectionPoints(designer);
            if (connectionPoints != null)
            {
                return new List<ConnectionPoint>(connectionPoints.Where<ConnectionPoint>(
                    (p) => { return p.AttachedConnectors == null || p.AttachedConnectors.Count == 0; }));
            }
            return new List<ConnectionPoint>();
        }

        internal static List<Connector> GetAttachedConnectors(UIElement shape)
        {
            HashSet<Connector> attachedConnectors = new HashSet<Connector>();
            List<ConnectionPoint> allConnectionPoints = GetConnectionPoints(shape);
            if (allConnectionPoints != null)
            {
                foreach (ConnectionPoint connPoint in allConnectionPoints)
                {
                    if (connPoint != null)
                    {
                        foreach (Connector connector in connPoint.AttachedConnectors)
                        {
                            attachedConnectors.Add(connector);
                        }
                    }
                }
            }
            return attachedConnectors.ToList<Connector>();
        }
        #endregion

        // Returns true if visual is on the visual tree for point p relative to the reference.
        static bool IsVisualHit(UIElement visual, UIElement reference, Point point)
        {
            bool visualIsHit = false;
            HitTestResult result = VisualTreeHelper.HitTest(reference, point);
            if (result != null)
            {
                DependencyObject obj = result.VisualHit;
                while (obj != null)
                {
                    if (visual.Equals(obj))
                    {
                        visualIsHit = true;
                        break;
                    }
                    obj = VisualTreeHelper.GetParent(obj);
                }
            }
            return visualIsHit;
        }


        #region StateContainerGridEventHandlers

        void OnStateContainerGridMouseLeave(object sender, MouseEventArgs e)
        {
           bool endLinkCreation = !IsVisualHit(sender as UIElement, sender as UIElement, e.GetPosition(sender as IInputElement));
            if (endLinkCreation)
            {
                RemoveAdorner(this.panel, typeof(ConnectorCreationAdorner));
                this.activeSrcConnectionPoint = null;
            }
        }

        void OnStateContainerGridMouseMove(object sender, MouseEventArgs e)
        {
           if (this.activeSrcConnectionPoint != null)
            {
                Point[] points = ConnectorRouter.Route(this.panel, this.activeSrcConnectionPoint, e.GetPosition(this.panel));
                List<Point> segments = new List<Point>(points);
                // Remove the previous adorner.
                RemoveAdorner(this.panel, typeof(ConnectorCreationAdorner));
                // Add new adorner.
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.panel);
                Debug.Assert(adornerLayer != null, "Adorner Layer does not exist");
                ConnectorCreationAdorner newAdorner = new ConnectorCreationAdorner(this.panel, segments);
                adornerLayer.Add(newAdorner);
                e.Handled = true;
            }
        }

        void OnStateContainerGridPreviewMouseMove(object sender, MouseEventArgs e)
        {
            // Creating connector
            if (this.activeSrcConnectionPoint != null)
            {
                AutoScrollHelper.AutoScroll(e, this);
            }
            // Reconnecting connector
            else if (this.panel.connectorEditor != null && (this.panel.connectorEditor.IsConnectorEndBeingMoved || this.panel.connectorEditor.IsConnectorStartBeingMoved))
            {
                AutoScrollHelper.AutoScroll(e, this);
            }
        }

        void OnStateContainerGridPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            UIElement destElement = (e.OriginalSource as IStateDesigner)  as UIElement;
            if (destElement == null) destElement = Utility.GetVisualAncestor<IStateDesigner>(e.OriginalSource as DependencyObject) as UIElement;
            if (destElement != null)// && destElement is IStateDesigner)
            {
                if (this.activeSrcConnectionPoint != null)
                {
                    ConnectorCreationResult result = CreateConnectorGesture(this.activeSrcConnectionPoint, destElement, null, false);
                    if (result != ConnectorCreationResult.Success)
                    {
                        FlowDesigner.ReportConnectorCreationError(result);
                    }
                }
                RemoveAdorner(destElement, typeof(ConnectionPointsAdorner));
            }
            if (this.activeSrcConnectionPoint != null)
            {
                this.activeSrcConnectionPoint = null;
                RemoveAdorner(this.panel, typeof(ConnectorCreationAdorner));
            }
        }

       

        void OnStateContainerGridDrop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            Object droppedObject = DragDropHelper.GetDroppedObject(this, e, Context);
            // Marking the event as being handled. In whichever case we want to route the event, it will be unmarked explicitly.
            e.Handled = true;
            if (droppedObject != null)
            {
                Point anchorPoint = DragDropHelper.GetDragDropAnchorPoint(e);
                ModelItem droppedModelItem = droppedObject as ModelItem;
                FlowDesigner srcContainer = DragDropHelper.GetCompositeView(e) as FlowDesigner;

                if (droppedModelItem != null && srcContainer != null && srcContainer.Equals(this))
                {
                    // Internal move
                    PerformInternalMove(this.modelItemToUIElement[droppedModelItem] as WorkflowViewElement, e.GetPosition(this.panel), anchorPoint);
                }
                else
                {
                    // External model Item drop
                    if (droppedModelItem != null)
                    {
                        this.ModelItem.Properties[ChildPropertyName].Collection.Add(droppedModelItem);
                    }
                    // Toolbox drop.
                    else
                    {
                        if (droppedObject is Activity)
                        {
                            //  ((State)droppedObject).DisplayName = SR.DefaultStateDisplayName;
                            Activity theActivity = droppedObject as Activity;
                            droppedObject = new ActionNode() { DisplayName = theActivity.DisplayName, InternalState = theActivity, StateId = Guid.NewGuid().ToString() };
                            //droppedObject = newdroppedObject;
                            droppedModelItem = this.ModelItem.Properties[ChildPropertyName].Collection.Add(droppedObject);
                        }
                        else
                        {
                            ((State)droppedObject).DisplayName= ((State)droppedObject).GetDefaultDisplayName();
                            droppedModelItem = this.ModelItem.Properties[ChildPropertyName].Collection.Add(droppedObject);
                        }
                        /* else if (droppedObject.GetType() == typeof(FinalState))
                         {
                             droppedObject = new State() { DisplayName = SR.DefaultFinalStateDisplayName, IsFinal = true };
                             droppedModelItem = this.ModelItem.Properties[ChildStatesPropertyName].Collection.Add(droppedObject);
                         }*/
                    }
                    if (droppedModelItem != null)
                    {
                        WorkflowViewElement view = droppedModelItem.View as WorkflowViewElement;
                        Debug.Assert(view != null, "Designer for dropped ModelItem should already have been created.");
                        // If drag anchor point is beyond the size of the shape being dropped, 
                        if (anchorPoint.X > view.DesiredSize.Width || anchorPoint.Y > view.DesiredSize.Height)
                        {
                            anchorPoint = new Point(-1, -1);
                        }
                        Point shapeLocation = FlowDesigner.SnapVisualToGrid(view, e.GetPosition(this.panel), anchorPoint);
                        object viewState = this.ViewStateService.RetrieveViewState(droppedModelItem, ShapeLocationViewStateKey);
                        if (viewState != null)
                        {
                            Point oldLocation = (Point)viewState;
                            oldLocation = srcContainer.panel.GetLocationRelativeToOutmostPanel(oldLocation);
                            Point newLocation = this.panel.GetLocationRelativeToOutmostPanel(shapeLocation);
                            // To make sure the connectors are still connected to the connection points
                            OffsetConnectorViewState(droppedModelItem, oldLocation, newLocation);
                        }
                        this.StoreShapeLocationViewState(droppedModelItem, shapeLocation);
                        DragDropHelper.SetDragDropCompletedEffects(e, DragDropEffects.Move);
                        this.Dispatcher.BeginInvoke(
                            new Action(() =>
                            {
                                Selection.SelectOnly(this.Context, droppedModelItem);
                                Keyboard.Focus((IInputElement)droppedModelItem.View);
                            }),
                            DispatcherPriority.ApplicationIdle);
                    }
                }
            }
        }

        void OnStateContainerGridDragEnter(object sender, DragEventArgs e)
        {
            OnStateContainerGridDrag(sender, e);
        }

        void OnStateContainerGridDragOver(object sender, DragEventArgs e)
        {
            OnStateContainerGridDrag(sender, e);
        }

        void OnStateContainerGridDrag(object sender, DragEventArgs e)
        {
            if (!e.Handled)
            {
                // Don't allow dropping on a connector. Otherwise the state will be dropped into the root state machine,
                // since all connectors belong to the root state container designer.
                if (Utility.GetVisualAncestor<Connector>(VisualTreeHelper.HitTest(this, e.GetPosition(this)).VisualHit) != null)
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                    return;
                }


                if (DragDropHelper.AllowDrop(
                    e.Data,
                    this.Context,
                    typeof(Activity), typeof(State), typeof(ActionNode), typeof(MenuScreen)))
                {
                    e.Effects |= DragDropEffects.Move;
                    e.Handled = true;
                }
                base.OnDragEnter(e);

                //ModelItem modelItem = e.Data.GetData(DragDropHelper.ModelItemDataFormat) as ModelItem;
                ////if (modelItem != null && !StateContainerEditor.AreInSameStateMachine(modelItem, this.ModelItem))
                ////{
                ////    // Don't allow dragging a state into a different state machine.
                ////    e.Effects = DragDropEffects.None;
                ////}
                ////else if (modelItem != null && modelItem.ItemType == typeof(State) && this.ModelItem.ItemType == typeof(State) &&
                ////    (bool)modelItem.Properties[IStateDesigner.IsFinalPropertyName].Value.GetCurrentValue())
                ////{
                ////    // Don't allow drag a final state into a state
                ////    e.Effects = DragDropEffects.None;
                ////}
                ////else if ((this.ModelItem.ItemType == typeof(State) && DragDropHelper.AllowDrop(e.Data, this.Context, typeof(State))) ||
                ////    (this.ModelItem.ItemType == typeof(StateMachine) && DragDropHelper.AllowDrop(e.Data, this.Context, typeof(State), typeof(FinalState))))
                ////{
                //    e.Effects |= DragDropEffects.Move;
                ////}
                ////else
                ////{
                ////    e.Effects = DragDropEffects.None;
                ////}
                //e.Handled = true;
            }
        }

        void KeyboardMove(Key key)
        {
            WorkflowViewElement shapeToMove = (WorkflowViewElement)this.modelItemToUIElement[this.Context.Items.GetValue<Selection>().PrimarySelection];
            Point currentLocation = FreeFormPanel.GetLocation(shapeToMove);
            switch (key)
            {
                case Key.Down:
                    PerformInternalMove(shapeToMove, new Point(currentLocation.X, currentLocation.Y + FlowDesigner.GridSize), new Point(0, 0));
                    break;
                case Key.Up:
                    PerformInternalMove(shapeToMove, new Point(currentLocation.X, currentLocation.Y - FlowDesigner.GridSize), new Point(0, 0));
                    break;
                case Key.Right:
                    PerformInternalMove(shapeToMove, new Point(currentLocation.X + FlowDesigner.GridSize, currentLocation.Y), new Point(0, 0));
                    break;
                case Key.Left:
                    PerformInternalMove(shapeToMove, new Point(currentLocation.X - FlowDesigner.GridSize, currentLocation.Y), new Point(0, 0));
                    break;
                default:
                    Debug.Assert(false, "Invalid case");
                    break;
            }
        }

        void OnStateContainerGridKeyDown(object sender, KeyEventArgs e)
        {
            Selection currentSelection = this.Context.Items.GetValue<Selection>();
            if (e.Key == Key.Delete && this.selectedConnector != null)
            {
                ModelItem primarySelection = currentSelection.PrimarySelection;
                //Delete connector
                ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(this.selectedConnector);
                if (object.Equals(primarySelection, connectorModelItem) ||
                    // Delete initial link
                    primarySelection == null && connectorModelItem != null && connectorModelItem.ItemType != typeof(Transition))
                {
                    this.DeleteConnectorModelItem(this.selectedConnector);
                    e.Handled = true;
                }
            }
            else if ((new List<Key> { Key.Left, Key.Right, Key.Up, Key.Down }).Contains(e.Key)
                && currentSelection.SelectionCount == 1
                && this.modelItemToUIElement.ContainsKey(currentSelection.PrimarySelection))
            {
                this.KeyboardMove(e.Key);
                e.Handled = true;
            }
        }

        internal void DeleteConnectorModelItem(Connector connector)
        {
            ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(connector);
            if (connectorModelItem.ItemType == typeof(Transition))
            {
                FlowDesigner.GetParentStateModelItemForTransition(connectorModelItem).Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Remove(connectorModelItem);
            }
            // Connector from initial node
            else if (connectorModelItem.ItemType == typeof(Flow))
            {
                using (EditingScope es = (EditingScope)this.ModelItem.BeginEdit("ClearInitialState"))
                {
                    connectorModelItem.Properties[FlowDesigner.InitialNodePropertyName].SetValue(null);
                    es.Complete();
                }
            }
        }

        void OnStateContainerGridPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.selectedConnector = null;
        }

        #endregion


        #region StateVisuals

        UIElement ProcessAndGetModelView(ModelItem model)
        {
            UIElement element;
            if (!this.modelItemToUIElement.TryGetValue(model, out element))
            {
                // Use VirtualizedContainerService for ModelItem.Focus to work.
                element = this.Context.Services.GetService<VirtualizedContainerService>().GetViewElement(model, this);

                SetChildViewSize((ContentControl)element, model);

                element.MouseEnter += new MouseEventHandler(OnChildElementMouseEnter);
                element.MouseLeave += new MouseEventHandler(OnChildElementMouseLeave);
                ((FrameworkElement)element).SizeChanged += new SizeChangedEventHandler(OnChildElementSizeChanged);

                this.modelItemToUIElement.Add(model, element);

                this.PopulateConnectionPoints(element);

                object locationOfShape = this.ViewStateService.RetrieveViewState(model, ShapeLocationViewStateKey);
                object sizeOfShape = this.ViewStateService.RetrieveViewState(model, ShapeSizeViewStateKey);
                if (locationOfShape != null)
                {
                    Point locationPt = (Point)locationOfShape;
                    FreeFormPanel.SetLocation(element, locationPt);
                    this.shapeLocations.Add(locationPt);
                }
                if (sizeOfShape != null)
                {
                    FreeFormPanel.SetChildSize(element, (Size)sizeOfShape);
                }
            }
            return element;
        }

        void AddStateVisuals(IList<ModelItem> modelItemCollection)
        {
            List<UIElement> viewsAdded = new List<UIElement>();
            foreach (ModelItem modelItem in modelItemCollection)
            {
                UIElement view = null;
                if (!this.modelItemToUIElement.ContainsKey(modelItem))
                {
                     view =ProcessAndGetModelView(modelItem);
                }
                else if (!this.panel.Children.Contains(this.modelItemToUIElement[modelItem]))
                {
                    view = this.modelItemToUIElement[modelItem];
                }
                if (view != null)
                {
                    this.panel.Children.Add(view);
                    
                    //// We need to wait until after the state visuals are added and displayed.
                    //ModelItem md = modelItem;
                    //view.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
                    //{
                    //    List<ModelItem> transitions = new List<ModelItem>();
                    //    transitions.AddRange(md.Properties[StateDesignerConstants.TransitionsPropertyName].Collection);
                    //    this.AddTransitionVisuals(transitions);

                    //    if (this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].Value == md)
                    //    {
                    //        this.AddInitialNodeConnector();
                    //    }
                    //}));

                }
            }

            foreach (ModelItem modelItem in modelItemCollection)
            {
                List<ModelItem> transitions = new List<ModelItem>();
                transitions.AddRange(modelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection);
                this.AddTransitionVisuals(transitions);

                if (this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].Value == modelItem)
                {
                    this.AddInitialNodeConnector();
                }
            }  
        }

        void RemoveStateVisual(WorkflowViewElement removedNodeDesigner)
        {
            HashSet<Connector> connectorsToDelete = new HashSet<Connector>();
            ModelService modelService = this.Context.Services.GetService<ModelService>();
            List<UIElement> removedNodeDesigners = new List<UIElement>();
            removedNodeDesigners.Add(removedNodeDesigner);
            //removedNodeDesigners.AddRange(GetAllChildNodeDesigners(removedNodeDesigner));

            FlowDesigner outmostEditor = this;
            foreach (UIElement designer in removedNodeDesigners)
            {
                if (outmostEditor.activeSrcConnectionPoint != null)
                {
                    List<ConnectionPoint> connectionPoints = GetConnectionPoints(designer);
                    if (connectionPoints.Contains(outmostEditor.activeSrcConnectionPoint))
                    {
                        outmostEditor.activeSrcConnectionPoint = null;
                        RemoveAdorner(outmostEditor.panel, typeof(ConnectorCreationAdorner));
                    }
                }
                if (outmostEditor.lastConnectionPointMouseUpElement == designer)
                {
                    outmostEditor.lastConnectionPointMouseUpElement = null;
                }
                connectorsToDelete.UnionWith(GetAttachedConnectors(designer));
            }

            // Remove any connector visuals attached to this shape. This is required for the scenarios as follows:
            // Copy and paste two connected States into StateMachine and undo the paste. 
            // The Link is not removed as a model change. Hence the connector visual will remain dangling on the designer.
            foreach (Connector connector in connectorsToDelete)
            {
                this.RemoveConnectorOnOutmostEditor(connector);
            }

            this.modelItemToUIElement.Remove(removedNodeDesigner.ModelItem);
            removedNodeDesigner.MouseEnter -= new MouseEventHandler(OnChildElementMouseEnter);
            removedNodeDesigner.MouseLeave -= new MouseEventHandler(OnChildElementMouseLeave);
            ((FrameworkElement)removedNodeDesigner).SizeChanged -= new SizeChangedEventHandler(OnChildElementSizeChanged);

            this.panel.Children.Remove(removedNodeDesigner);

            // deselect removed item
            if (this.Context != null)
            {
                HashSet<ModelItem> selectedItems = new HashSet<ModelItem>(this.Context.Items.GetValue<Selection>().SelectedObjects);
                if (selectedItems.Contains(removedNodeDesigner.ModelItem))
                {
                    Selection.Toggle(this.Context, removedNodeDesigner.ModelItem);
                }
            }

            object locationOfShape = this.ViewStateService.RetrieveViewState(removedNodeDesigner.ModelItem, FlowDesigner.ShapeLocationViewStateKey);
            if (locationOfShape != null)
            {
                this.shapeLocations.Remove((Point)locationOfShape);
            }
        }

        #endregion


        #region FreeFormPanelEventHandlers

        void OnFreeFormPanelLocationChanged(object sender, LocationChangedEventArgs e)
        {
            Debug.Assert(sender is UIElement, "Sender should be of type UIElement");
            Connector movedConnector = sender as Connector;
            if (movedConnector != null)
            {
                //ViewState is undoable only when a user gesture moves a connector. If the FreeFormPanel routes a connector,
                //the change is not undoable.
                bool isUndoableViewState = false;
                ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(movedConnector);
                PointCollection existingViewState = this.ViewStateService.RetrieveViewState(connectorModelItem, ConnectorLocationViewStateKey) as PointCollection;
                if (existingViewState != null && existingViewState.Count > 0 && movedConnector.Points.Count > 0
                    && existingViewState[0].Equals(movedConnector.Points[0]) && existingViewState[existingViewState.Count - 1].Equals(movedConnector.Points[movedConnector.Points.Count - 1]))
                {
                    isUndoableViewState = true;
                }
                StoreConnectorLocationViewState(movedConnector, isUndoableViewState);
            }
            else
            {
                //This is called only when a shape without ViewState is auto-layout'd by the FreeFormPanel.
                WorkflowViewElement view = sender as WorkflowViewElement;
                if (view != null)
                {
                    StoreShapeLocationViewState(view, e.NewLocation);
                }
            }
        }

        void UpdateStateMachineOnConnectorMoved(ConnectionPoint knownConnectionPoint, Point newPoint, Connector movedConnector, bool isConnectorStartMoved)
        {
            HitTestResult hitTestResult = VisualTreeHelper.HitTest(this.panel, newPoint);
            if (hitTestResult == null)
            {
                return;
            }

            UIElement newViewElement = null;
            ConnectionPoint newConnectionPoint = null;

            //The case where the Connector is dropped on a ConnectionPoint.
            if (this.lastConnectionPointMouseUpElement != null)
            {
                newConnectionPoint = FlowDesigner.ConnectionPointHitTest(this.lastConnectionPointMouseUpElement, newPoint, ConnectionPointType.Default);
                if (newConnectionPoint != null)
                {
                    newViewElement = this.lastConnectionPointMouseUpElement;
                }
                this.lastConnectionPointMouseUpElement = null;
            }

            //The case where the link is dropped on a shape.
            if (newViewElement == null)
            {
                newViewElement = Utility.GetVisualAncestor<IStateDesigner>(hitTestResult.VisualHit) as UIElement;
            }

            if (newViewElement != null)
            {
                if (this.panel.IsAncestorOf(newViewElement))
                {
                    using (EditingScope es = (EditingScope)this.ModelItem.BeginEdit("MoveLink"))
                    {
                        // Remove the old connector ModelItem
                        this.DeleteConnectorModelItem(movedConnector);
                        // Create new connector
                        ConnectorCreationResult result = ConnectorCreationResult.OtherFailure;
                        if (!isConnectorStartMoved)
                        {
                            if (newConnectionPoint == null)
                            {
                                result = CreateConnectorGesture(knownConnectionPoint, newViewElement, movedConnector, false);
                            }
                            else
                            {
                                result = CreateConnectorGesture(knownConnectionPoint, newConnectionPoint, movedConnector, false);
                            }
                        }
                        else
                        {
                            // Don't allow moving the start of the initial node connector to a state
                            if (!(newViewElement is IStateDesigner && FlowDesigner.IsConnectorFromInitialNode(movedConnector)))
                            {
                                if (newConnectionPoint == null)
                                {
                                    result = CreateConnectorGesture(newViewElement, knownConnectionPoint, movedConnector, true);
                                }
                                else
                                {
                                    result = CreateConnectorGesture(newConnectionPoint, knownConnectionPoint, movedConnector, true);
                                }
                            }
                        }

                        if (result == ConnectorCreationResult.Success)
                        {
                            es.Complete();
                        }
                        else
                        {
                            FlowDesigner.ReportConnectorCreationError(result);
                            es.Revert();
                        }
                    }
                }
            }
        }

        void OnFreeFormPanelConnectorMoved(object sender, ConnectorMovedEventArgs e)
        {
            Connector movedConnector = sender as Connector;
            if (movedConnector != null)
            {
               // Debug.Assert(e.NewConnectorLocation.Count > 0, "Invalid connector editor");
                if (e.NewConnectorLocation[0].Equals(movedConnector.Points[0]))
                {
                    // destination moved
                    ConnectionPoint srcConnPoint = FreeFormPanel.GetSourceConnectionPoint(movedConnector);
                    Point destPoint = e.NewConnectorLocation[e.NewConnectorLocation.Count - 1];
                    UpdateStateMachineOnConnectorMoved(srcConnPoint, destPoint, movedConnector, false);
                }
                else
                {
                    // source moved
                    ConnectionPoint destConnPoint = FreeFormPanel.GetDestinationConnectionPoint(movedConnector);
                    UpdateStateMachineOnConnectorMoved(destConnPoint, e.NewConnectorLocation[0], movedConnector, true);
                }
            }
        }

        // This is to keep this.selectedConnector up to date.
        // Cases included: 1. create a connector, select it and undo, 2. move a connector from one shape to another.
        void OnFreeFormPanelLayoutUpdated(object sender, EventArgs e)
        {
            if (this.selectedConnector != null && !this.panel.Children.Contains(this.selectedConnector))
            {
                this.ClearSelectedConnector();
            }
        }

        void OnFreeFormPanelRequiredSizeChanged(object sender, RequiredSizeChangedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (e.NewRequiredSize.Width > this.Width)
                {
                    this.ViewStateService.StoreViewState(this.ModelItem, FlowDesigner.WidthViewStateKey, e.NewRequiredSize.Width);
                }
                if (e.NewRequiredSize.Height > this.Height)
                {
                    this.ViewStateService.StoreViewState(this.ModelItem, FlowDesigner.HeightViewStateKey, e.NewRequiredSize.Height);
                }
            }));
        }

        #endregion


        #region ICompositeView Members

        public static readonly DependencyProperty DroppingTypeResolvingOptionsProperty =
                 DependencyProperty.Register("DroppingTypeResolvingOptions", typeof(TypeResolvingOptions), typeof(FlowDesigner));

        public TypeResolvingOptions DroppingTypeResolvingOptions
        {
            get { return (TypeResolvingOptions)GetValue(DroppingTypeResolvingOptionsProperty); }
            set { SetValue(DroppingTypeResolvingOptionsProperty, value); }
        }

        public bool IsDefaultContainer
        {
            get { return true; }
        }

        public void OnItemMoved(ModelItem modelItem)
        {
            Debug.Assert(this.modelItemToUIElement.ContainsKey(modelItem), "Moved item does not exist.");
            this.DoDeleteItems(new List<ModelItem> { modelItem }, false);
        }

        public object OnItemsCopied(List<ModelItem> itemsToCopy)
        {
            // Save the locations of copied items relative to the outmost editor to the metadata.
            // The metadata will be used to translate the location view states of pasted items to the pasting target.
            PointCollection metaData = new PointCollection();
            foreach (ModelItem modelItem in itemsToCopy)
            {
                object viewState = this.ViewStateService.RetrieveViewState(modelItem, ShapeLocationViewStateKey);
                Point location = (Point)viewState;
                //FlowDesigner parentDesigner = Utility.GetVisualAncestor<FlowDesigner>(modelItem.View);
                location = this.panel.GetLocationRelativeToOutmostPanel(location);
                metaData.Add(location);
            }
            return metaData;
        }

        public object OnItemsCut(List<ModelItem> itemsToCut)
        {
            object metaData = OnItemsCopied(itemsToCut);
            this.OnItemsDelete(itemsToCut);
            return metaData;
        }

        public void OnItemsDelete(List<ModelItem> itemsToDelete)
        {
            DoDeleteItems(itemsToDelete, true);
        }

        void DoDeleteItems(List<ModelItem> itemsToDelete, bool removeIncomingConnectors)
        {
            HashSet<Connector> connectorsToDelete = new HashSet<Connector>();
            List<ModelItem> allStateModelItemsToDelete = new List<ModelItem>();
            IEnumerable<ModelItem> selectedStateModelItems = this.Context.Items.GetValue<Selection>().SelectedObjects;
                //Where<ModelItem>((p) => { return p.ItemType == typeof(State); });

            foreach (ModelItem stateModelItem in itemsToDelete)
            {
                allStateModelItemsToDelete.Add(stateModelItem);
            }

            foreach (ModelItem modelItem in allStateModelItemsToDelete)
            {
                // We only need to delete incoming connectors to the states to be deleted; outgoing connectors will be deleted
                // automatically when the containing state is deleted.
                List<Connector> incomingConnectors = FlowDesigner.GetIncomingConnectors((UIElement)modelItem.View);
                foreach (Connector connector in incomingConnectors)
                {
                    ModelItem transitionModelItem = FlowDesigner.GetConnectorModelItem(connector);
                    // If the transition is contained by the states to delete, we don't bother to delete it separately.
                    if (!FlowDesigner.IsTransitionModelItemContainedByStateModelItems(transitionModelItem, selectedStateModelItems))
                    {
                        connectorsToDelete.Add(connector);
                    }
                }
            }

            // If we don't need to remove incoming connectors, we still remove the transitions but then add them back later.
            // This is in order to create an undo unit that contains the change notifications needed to make undo/redo work correctly.
            foreach (Connector connector in connectorsToDelete)
            {
                ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(connector);
                if (removeIncomingConnectors || connectorModelItem.ItemType == typeof(Transition))
                {
                    this.DeleteConnectorModelItem(connector);
                }
            }
            if (!removeIncomingConnectors)
            {
                foreach (Connector connector in connectorsToDelete)
                {
                    ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(connector);
                    if (connectorModelItem.ItemType == typeof(Transition))
                    {
                        FlowDesigner.GetParentStateModelItemForTransition(connectorModelItem).Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Add(connectorModelItem);
                    }
                }
            }

            if (null != itemsToDelete)
            {
                itemsToDelete.ForEach(p => this.DeleteState(p, removeIncomingConnectors));
            }
        }

        // Test if the transition is contained by any of the states or their descendants
        static bool IsTransitionModelItemContainedByStateModelItems(ModelItem transitionModelItem, IEnumerable<ModelItem> stateModelItems)
        {
            foreach (ModelItem stateModelItem in stateModelItems)
            {
                if (stateModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Contains(transitionModelItem))
                {
                    return true;
                }
            }
            return false;
        }

        static List<Connector> GetIncomingConnectors(UIElement shape)
        {
            List<Connector> incomingConnectors = new List<Connector>();
            List<ConnectionPoint> allConnectionPoints = GetConnectionPoints(shape);
            foreach (ConnectionPoint connPoint in allConnectionPoints)
            {
                if (connPoint != null)
                {
                    incomingConnectors.AddRange(connPoint.AttachedConnectors.Where(p => FreeFormPanel.GetDestinationConnectionPoint(p).Equals(connPoint)));
                }
            }
            return incomingConnectors;
        }

        void DeleteState(ModelItem stateModelItem, bool clearInitialState)
        {
            this.ModelItem.Properties[ChildPropertyName].Collection.Remove(stateModelItem);
            if (clearInitialState && stateModelItem == this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].Value)
            {
                this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].SetValue(null);
            }
        }

        public bool CanPasteItems(List<object> itemsToPaste)
        {
            if (itemsToPaste != null)
            {
                return true;
            }
            return false;
        }

        public void OnItemsPasted(List<object> itemsToPaste, List<object> metaData, Point pastePoint, WorkflowViewElement pastePointReference)
        {
            List<ModelItem> modelItemsPasted = new List<ModelItem>();
            List<State> states = new List<State>();
            foreach (object obj in itemsToPaste)
            {
                State state;
                if (obj is Activity)
                {
                    Activity theActivity = obj as Activity;
                    state = new ActionNode() { DisplayName = theActivity.DisplayName, InternalState = theActivity, StateId=Guid.NewGuid().ToString()  };
                }
                else
                {
                    state = (State)obj;
                    if (state.DisplayName == null)
                    {
                        state.DisplayName = (obj as State).GetDefaultDisplayName();
                    }
                    state.StateId = Guid.NewGuid().ToString();
                }
                states.Add(state);
            }

            RemoveDanglingTransitions(states);
            foreach (object state in itemsToPaste)
            {
                ModelItem stateModelItem = this.ModelItem.Properties[ChildPropertyName].Collection.Add(state);
                modelItemsPasted.Add(stateModelItem);
            }
            if (modelItemsPasted.Count > 0)
            {
                // translate location view states to be in the coordinate system of the pasting target             
                this.UpdateLocationViewStatesByMetaData(modelItemsPasted, metaData);
                if (pastePoint.X > 0 && pastePoint.Y > 0)
                {
                    if (pastePointReference != null)
                    {
                        pastePoint = pastePointReference.TranslatePoint(pastePoint, this.panel);
                        pastePoint.X = pastePoint.X < 0 ? 0 : pastePoint.X;
                        pastePoint.Y = pastePoint.Y < 0 ? 0 : pastePoint.Y;
                    }
                    this.UpdateLocationViewStatesByPoint(modelItemsPasted, pastePoint);
                }
                // If paste point is not available, paste the items to the top left corner.
                else
                {
                    this.UpdateLocationViewStatesToTopLeft(modelItemsPasted);
                }
            }

            this.Context.Items.SetValue(new Selection(modelItemsPasted));
        }

        // Remove dangling transitions that are not pointing to any of the input states or their descendants
        static void RemoveDanglingTransitions(IEnumerable<State> states)
        {
            Queue<State> statesToProcess = new Queue<State>(states);
            while (statesToProcess.Count > 0)
            {
                State state = statesToProcess.Dequeue();

                IEnumerable<Transition> toRemove = state.Transitions.Reverse();
                foreach (Transition transition in toRemove)
                {
                    state.Transitions.Remove(transition);
                }
            }
        }

        void UpdateLocationViewStatesByPoint(List<ModelItem> itemsPasted, Point point)
        {
            Point topLeft = new Point(Double.PositiveInfinity, Double.PositiveInfinity);
            foreach (ModelItem stateModelItem in itemsPasted)
            {
                object viewState = this.ViewStateService.RetrieveViewState(stateModelItem, ShapeLocationViewStateKey);
                if (viewState != null)
                {
                    Point location = (Point)viewState;
                    topLeft.X = topLeft.X > location.X ? location.X : topLeft.X;
                    topLeft.Y = topLeft.Y > location.Y ? location.Y : topLeft.Y;
                }
            }
            OffsetLocationViewStates(new Vector(point.X - topLeft.X, point.Y - topLeft.Y), itemsPasted, null, false);//GetTransitionModelItems(itemsPasted), false);
        }

        void UpdateLocationViewStatesByMetaData(List<ModelItem> itemsPasted, List<object> metaData)
        {
            // If the states are not copied from state machine view (e.g., when the State designer is the breadcrumb root), 
            // there is no meta data
            if (metaData != null && metaData.Count > 0)
            {
                FlowDesigner outmostEditor = this;
                int ii = 0;
                foreach (object data in metaData)
                {
                    PointCollection points = (PointCollection)data;
                    foreach (Point point in points)
                    {
                        // translate location view states to be in the coordinate system of the pasting target
                        this.ViewStateService.StoreViewState(itemsPasted[ii], ShapeLocationViewStateKey, outmostEditor.panel.TranslatePoint(point, this.panel));
                        ++ii;
                    }
                }
                Debug.Assert(itemsPasted.Count == ii, "itemsCopied does not match the metaData.");
            }
        }

        void OffsetLocationViewStates(Vector offsetVector, IEnumerable<ModelItem> stateModelItems, IEnumerable<ModelItem> transitionModelItems, bool enableUndo)
        {
            // Offset view state for states
            if (stateModelItems != null)
            {
                foreach (ModelItem modelItem in stateModelItems)
                {
                    object viewState = this.ViewStateService.RetrieveViewState(modelItem, ShapeLocationViewStateKey);
                    if (viewState != null)
                    {
                        viewState = Point.Add((Point)viewState, offsetVector);
                        if (enableUndo)
                        {
                            this.ViewStateService.StoreViewStateWithUndo(modelItem, ShapeLocationViewStateKey, viewState);
                        }
                        else
                        {
                            this.ViewStateService.StoreViewState(modelItem, ShapeLocationViewStateKey, viewState);
                        }
                    }
                }
            }
            // Offset view state for transitions
            if (transitionModelItems != null)
            {
                foreach (ModelItem modelItem in transitionModelItems)
                {
                    object viewState = this.ViewStateService.RetrieveViewState(modelItem, ConnectorLocationViewStateKey);
                    if (viewState != null)
                    {
                        PointCollection locations = (PointCollection)viewState;
                        PointCollection newLocations = new PointCollection();
                        foreach (Point location in locations)
                        {
                            Point newLocation = Point.Add(location, offsetVector);
                            newLocation.X = newLocation.X < 0 ? 0 : newLocation.X;
                            newLocation.Y = newLocation.Y < 0 ? 0 : newLocation.Y;
                            newLocations.Add(newLocation);
                        }
                        if (enableUndo)
                        {
                            this.ViewStateService.StoreViewStateWithUndo(modelItem, ConnectorLocationViewStateKey, newLocations);
                        }
                        else
                        {
                            this.ViewStateService.StoreViewState(modelItem, ConnectorLocationViewStateKey, newLocations);
                        }
                    }
                }
            }
        }

        void UpdateLocationViewStatesToTopLeft(List<ModelItem> itemsPasted)
        {
            this.UpdateLocationViewStatesByPoint(itemsPasted, new Point(GridSize, GridSize));
            this.UpdateLocationViewStatesToAvoidOverlap(itemsPasted);
        }

        void UpdateLocationViewStatesToAvoidOverlap(List<ModelItem> itemsPasted)
        {
            int offset = 0;
            if (itemsPasted.Count > 0)
            {
                //Check to see if the first element in the input list needs offset. Generalize that information for all ModelItems in the input list.
                object location = this.ViewStateService.RetrieveViewState(itemsPasted[0], ShapeLocationViewStateKey);
                if (location != null)
                {
                    Point locationOfShape = (Point)location;
                    while (this.shapeLocations.Contains(locationOfShape))
                    {
                        offset++;
                        locationOfShape.Offset(FlowDesigner.GridSize, FlowDesigner.GridSize);
                    }
                }
            }
            //Update ViewState according to calculated offset.
            if (offset > 0)
            {
                double offsetValue = FlowDesigner.GridSize * offset;
                OffsetLocationViewStates(new Vector(offsetValue, offsetValue), itemsPasted, null, false);//GetTransitionModelItems(itemsPasted), false);
            }
        }
        #endregion

  
        void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (ModelItem modelItem in e.NewItems)
                {

                    Point p = (Point)(this.ViewStateService.RetrieveViewState(modelItem, ShapeLocationViewStateKey) ?? new Point());

                    var view = ProcessAndGetModelView(modelItem); //Context.Services.GetService<ViewService>().GetView(modelItem);
                    FreeFormPanel.SetLocation((UIElement)view, p);

                    this.panel.Children.Add((UIElement)view);
                    DragDropHelper.SetCompositeView((WorkflowViewElement)view, this);
                }
            }
        }

        void Update(ModelItem canvasActivity)
        {
            this.panel.Children.Clear();
            //this.modelItemToUIElement.Clear();
            foreach (ModelItem modelItem in canvasActivity.Properties[ChildPropertyName].Collection)
            {
                Point p = (Point)(this.ViewStateService.RetrieveViewState(modelItem, ShapeLocationViewStateKey)?? new Point());

                var view = Context.Services.GetService<ViewService>().GetView(modelItem);
                FreeFormPanel.SetLocation((UIElement)view, p);

                this.panel.Children.Add((UIElement)view);
                DragDropHelper.SetCompositeView((WorkflowViewElement)view, this);
            }
        }




        //This snaps the center of the element to grid.
        //Wherever shapeAnchorPoint is valid, it is made co-incident with the drop location.
        static Point SnapVisualToGrid(UIElement element, Point location, Point shapeAnchorPoint)
        {
            Debug.Assert(element != null, "Input UIElement is null");
            element.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            Point oldCenter = location;
            if (shapeAnchorPoint.X < 0 && shapeAnchorPoint.Y < 0)
            {
                //shapeAnchorPoint is set to (-1, -1) in case where it does not make sense (eg. toolbox drop).
                location.X -= InitialFinalWidth / 2;
                location.Y -= InitialFinalHeight / 2;
            }
            else
            {
                location.X -= shapeAnchorPoint.X;
                location.Y -= shapeAnchorPoint.Y;
                oldCenter = new Point(location.X + element.DesiredSize.Width / 2, location.Y + element.DesiredSize.Height / 2);
            }

            Point newCenter = SnapPointToGrid(oldCenter);

            location.Offset(newCenter.X - oldCenter.X, newCenter.Y - oldCenter.Y);

            if (location.X < 0)
            {
                double correction = FlowDesigner.GridSize - ((location.X * (-1)) % FlowDesigner.GridSize);
                location.X = (correction == FlowDesigner.GridSize) ? 0 : correction;
            }
            if (location.Y < 0)
            {
                double correction = FlowDesigner.GridSize - ((location.Y * (-1)) % FlowDesigner.GridSize);
                location.Y = (correction == FlowDesigner.GridSize) ? 0 : correction;
            }
            return location;
        }

        static Point SnapPointToGrid(Point pt)
        {
            pt.X -= pt.X % FlowDesigner.GridSize;
            pt.Y -= pt.Y % FlowDesigner.GridSize;
            pt.X = pt.X < 0 ? 0 : pt.X;
            pt.Y = pt.Y < 0 ? 0 : pt.Y;
            return pt;
        }


       
        static void SetChildViewSize(ContentControl control, ModelItem model)
        {
            control.MinWidth = ChildViewMinWidth;
           
        }


        #region ChildElementEventHandlers

        void OnChildElementMouseEnter(object sender, MouseEventArgs e)
        {
            UIElement senderElement = sender as UIElement;
            if (senderElement != null && !this.IsReadOnly)
            {
                AddConnectionPointsAdorner(senderElement);
            }
        }

        void AddConnectionPointsAdorner(UIElement element)
        {
            bool isSelected = false;
            if (element is WorkflowViewElement)
            {
                isSelected = (((Selection)this.Context.Items.GetValue<Selection>()).SelectedObjects as ICollection<ModelItem>).Contains(((WorkflowViewElement)element).ModelItem);
            }
            ConnectionPointsAdorner connectionPointsAdorner = new ConnectionPointsAdorner(element, ConnectionPointsToShow(element), isSelected);
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(element);
            Debug.Assert(adornerLayer != null, "Cannot get AdornerLayer.");
            adornerLayer.Add(connectionPointsAdorner);
            // The outmostEditor should handle all the connection point related events for all its descendants
           // FlowDesigner outmostEditor = this.GetOutmostFlowDesigner();
            connectionPointsAdorner.MouseDown += new MouseButtonEventHandler(this.OnConnectionPointMouseDown);
            connectionPointsAdorner.MouseUp += new MouseButtonEventHandler(this.OnConnectionPointMouseUp);
            connectionPointsAdorner.MouseLeave += new MouseEventHandler(this.OnConnectionPointMouseLeave);
        }



        void OnChildElementMouseLeave(object sender, MouseEventArgs e)
        {
            bool removeConnectionPointsAdorner = true;
            if (Mouse.DirectlyOver != null)
            {
                removeConnectionPointsAdorner = !typeof(ConnectionPointsAdorner).IsAssignableFrom(Mouse.DirectlyOver.GetType());
            }
            if (removeConnectionPointsAdorner)
            {
                RemoveAdorner(sender as UIElement, typeof(ConnectionPointsAdorner));

               
            }
        }

        void OnChildElementSizeChanged(object sender, SizeChangedEventArgs e)
        {
            WorkflowViewElement view = sender as WorkflowViewElement;
            if (view != null)
            {
                // Such size changed events are a result of changes already committed in the UI. Hence we do not want to react to such view state changes.
                // Using internalViewStateChange flag for that purpose.
                this.internalViewStateChange = true;
                ModelItem storageModelItem = view.ModelItem;
                this.ViewStateService.StoreViewState(storageModelItem, ShapeSizeViewStateKey, ((UIElement)sender).DesiredSize);
                this.internalViewStateChange = false;
            }
        }

        static void RemoveAdorner(UIElement adornedElement, Type adornerType)
        {
            Debug.Assert(adornedElement != null, "Invalid argument");
            Debug.Assert(typeof(Adorner).IsAssignableFrom(adornerType), "Invalid argument");
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            if (adornerLayer != null)
            {
                Adorner[] adorners = adornerLayer.GetAdorners(adornedElement);
                if (adorners != null)
                {
                    foreach (Adorner adorner in adorners)
                    {
                        if (adornerType.IsAssignableFrom(adorner.GetType()))
                        {
                            adornerLayer.Remove(adorner);
                        }
                    }
                }
            }
        }



        List<ConnectionPoint> ConnectionPointsToShow(UIElement element)
        {
            bool isCreatingConnector = this.IsCreatingConnector();
            List<ConnectionPoint> connectionPointsToShow = new List<ConnectionPoint>();
            if (element is InitialNode)
            {
                // Don't allow moving the start of a transition to the initial node.
                if (isCreatingConnector || this.IsMovingStartOfConnectorForTransition())
                {
                    return connectionPointsToShow;
                }
                // Don't allow creating more than one connectors from the initial node.
                if ((FlowDesigner.GetOutgoingConnectors(element).Count > 0) && !this.IsMovingStartOfConnectorFromInitialNode())
                {
                    return connectionPointsToShow;
                }
            }
            else if ((element as WorkflowViewElement).ModelItem != null && typeof(ScreenNode).IsAssignableFrom((element as WorkflowViewElement).ModelItem.ItemType))
            {
                // Don't allow creating more than one connectors from the initial node.
                if (this.IsMovingStartOfConnectorFromInitialNode())
                {
                    return connectionPointsToShow;
                }
            }
            else if (element is IStateDesigner)
            {
                // Don't allow moving the start of the initial node connector to a state
                if (this.IsMovingStartOfConnectorFromInitialNode())
                {
                    return connectionPointsToShow;
                }

                if ((FlowDesigner.GetOutgoingConnectors(element).Count > 1) && this.activeSrcConnectionPoint == null)
                {
                    return connectionPointsToShow;
                }
                
            }

            List<ConnectionPoint> connectionPoints = FlowDesigner.GetConnectionPoints(element);
            if (isCreatingConnector)
            {
                connectionPointsToShow.AddRange(connectionPoints.Where<ConnectionPoint>(
                    (p) => { return p.PointType != ConnectionPointType.Outgoing && p.AttachedConnectors.Count == 0; }));
            }
            else
            {
                connectionPointsToShow.AddRange(connectionPoints.Where<ConnectionPoint>(
                    (p) => { return p.PointType != ConnectionPointType.Incoming && p.AttachedConnectors.Count == 0; }));
            }

            return connectionPointsToShow;
        }

        bool IsCreatingConnector()
        {
            FlowDesigner outmostEditor = this;
            return (outmostEditor.activeSrcConnectionPoint != null || (outmostEditor.panel.connectorEditor != null && outmostEditor.panel.connectorEditor.IsConnectorEndBeingMoved));
        }

        static List<Connector> GetOutgoingConnectors(UIElement shape)
        {
            List<Connector> outgoingConnectors = new List<Connector>();
            List<ConnectionPoint> allConnectionPoints = GetConnectionPoints(shape);
            foreach (ConnectionPoint connPoint in allConnectionPoints)
            {
                if (connPoint != null)
                {
                    outgoingConnectors.AddRange(connPoint.AttachedConnectors.Where(p => FreeFormPanel.GetSourceConnectionPoint(p).Equals(connPoint)));
                }
            }
            return outgoingConnectors;
        }

        #endregion

        bool IsMovingStartOfConnectorFromInitialNode()
        {
            FlowDesigner outmostEditor = this;
            return (outmostEditor.panel.connectorEditor != null && outmostEditor.panel.connectorEditor.IsConnectorStartBeingMoved &&
                outmostEditor.panel.connectorEditor.Connector != null &&
                IsConnectorFromInitialNode(outmostEditor.panel.connectorEditor.Connector));
        }

        bool IsMovingStartOfConnectorForTransition()
        {
            FlowDesigner outmostEditor = this;
            return (outmostEditor.panel.connectorEditor != null && outmostEditor.panel.connectorEditor.IsConnectorStartBeingMoved &&
                outmostEditor.panel.connectorEditor.Connector != null &&
                GetConnectorModelItem(outmostEditor.panel.connectorEditor.Connector).ItemType == typeof(Transition)); ;
        }

        internal static List<ConnectionPoint> GetConnectionPoints(DependencyObject obj)
        {
            return (List<ConnectionPoint>)obj.GetValue(FlowDesigner.ConnectionPointsProperty);
        }

        static void SetConnectionPoints(DependencyObject obj, List<ConnectionPoint> connectionPoints)
        {
            obj.SetValue(FlowDesigner.ConnectionPointsProperty, connectionPoints);
        }

        void PopulateConnectionPoints(UIElement view)
        {
            view.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            List<ConnectionPoint> connectionPoints = new List<ConnectionPoint>();
            if (view is InitialNode)
            {
                connectionPoints.Add(CreateConnectionPoint(view, 0.5, 1.0, EdgeLocation.Bottom, ConnectionPointType.Outgoing));
                connectionPoints.Add(CreateConnectionPoint(view, 0, 0.5, EdgeLocation.Left, ConnectionPointType.Outgoing));
                connectionPoints.Add(CreateConnectionPoint(view, 1.0, 0.5, EdgeLocation.Right, ConnectionPointType.Outgoing));
            }
            else
            {
                SetMultipleIncomingConnectionPoints(view, connectionPoints);

                ModelItem model = (view as WorkflowViewElement).ModelItem;
                if (typeof(ConfirmScreen).IsAssignableFrom(model.ItemType))
                {
                    connectionPoints.Add(CreateConnectionPoint(view, 0, 0.5, EdgeLocation.Left, ConnectionPointType.Outgoing));
                    connectionPoints.Add(CreateConnectionPoint(view, 1.0, 0.5, EdgeLocation.Right, ConnectionPointType.Outgoing)); 
                }
                else if (typeof(MenuScreen).IsAssignableFrom(model.ItemType))
                {
                    double connectionPointNum = 3;
                    double connectionPointRatio = 0.25;
                    for (int ii = 1; ii <= connectionPointNum; ii++)
                    {
                        connectionPoints.Add(CreateConnectionPoint(view, 1, ii * connectionPointRatio, EdgeLocation.Right, ConnectionPointType.Outgoing));
                        connectionPoints.Add(CreateConnectionPoint(view, 0, ii * connectionPointRatio, EdgeLocation.Left, ConnectionPointType.Outgoing));
                    }

                    connectionPointNum = 5;
                    connectionPointRatio = 0.167;
                    for (int ii = 1; ii <= connectionPointNum; ii++)
                    {
                        connectionPoints.Add(CreateConnectionPoint(view, ii * connectionPointRatio, 0, EdgeLocation.Top, ConnectionPointType.Outgoing));
                        connectionPoints.Add(CreateConnectionPoint(view, ii * connectionPointRatio, 1, EdgeLocation.Bottom, ConnectionPointType.Outgoing));
                    }
                }
                else if (typeof(NotifyScreen).IsAssignableFrom(model.ItemType))
                {
                }
                else if (typeof(GotoFlowAction).IsAssignableFrom(model.ItemType))
                {
                }
                else //if (typeof(ActionNode).IsAssignableFrom(model.ItemType))
                {
                    connectionPoints.Add(CreateConnectionPoint(view, 0.5, 0, EdgeLocation.Top, ConnectionPointType.Outgoing));
                    connectionPoints.Add(CreateConnectionPoint(view, 0.5, 1.0, EdgeLocation.Bottom, ConnectionPointType.Outgoing));
                    connectionPoints.Add(CreateConnectionPoint(view, 0, 0.5, EdgeLocation.Left, ConnectionPointType.Outgoing));
                    connectionPoints.Add(CreateConnectionPoint(view, 1.0, 0.5, EdgeLocation.Right, ConnectionPointType.Outgoing));
                }
            }
            FlowDesigner.SetConnectionPoints(view, connectionPoints);
        }

        private void SetMultipleIncomingConnectionPoints(UIElement view, List<ConnectionPoint> connectionPoints)
        {
            ConnectionPointType connectionPointType = ConnectionPointType.Incoming;
            double connectionPointNum = 3;
            double connectionPointRatio = 0.25;

            for (int ii = 1; ii <= connectionPointNum; ii++)
            {
                connectionPoints.Add(CreateConnectionPoint(view, 1, ii * connectionPointRatio, EdgeLocation.Right, connectionPointType));
                connectionPoints.Add(CreateConnectionPoint(view, 0, ii * connectionPointRatio, EdgeLocation.Left, connectionPointType));
            }

            connectionPointNum = 5;
            connectionPointRatio = 0.167;

            for (int ii = 1; ii <= connectionPointNum; ii++)
            {
                connectionPoints.Add(CreateConnectionPoint(view, ii * connectionPointRatio, 0, EdgeLocation.Top, connectionPointType));
                connectionPoints.Add(CreateConnectionPoint(view, ii * connectionPointRatio, 1, EdgeLocation.Bottom, connectionPointType));
            }
        }


        //widthFraction and heightFraction determine the location of the ConnectionPoint on the UIElement.
        ConnectionPoint CreateConnectionPoint(UIElement element, double widthFraction, double heightFraction, EdgeLocation location, ConnectionPointType type)
        {
            ConnectionPoint connectionPoint = new ConnectionPoint();
            connectionPoint.EdgeLocation = location;
            connectionPoint.PointType = type;
            connectionPoint.ParentDesigner = element;
            BindingOperations.SetBinding(connectionPoint, ConnectionPoint.LocationProperty, GetConnectionPointBinding(element as FrameworkElement, widthFraction, heightFraction));
            return connectionPoint;
        }

        MultiBinding GetConnectionPointBinding(FrameworkElement element, double widthFraction, double heightFraction)
        {
            Debug.Assert(element != null, "FrameworkElement is null.");
            MultiBinding bindings = new MultiBinding();
            Binding sizeBinding = new Binding();
            sizeBinding.Source = element;
            sizeBinding.Path = new PropertyPath(FreeFormPanel.ChildSizeProperty);
            Binding locationBinding = new Binding();
            locationBinding.Source = element;
            locationBinding.Path = new PropertyPath(FreeFormPanel.LocationProperty);
            bindings.Bindings.Add(sizeBinding);
            bindings.Bindings.Add(locationBinding);
            bindings.Converter = new ConnectionPointConverter();
            bindings.ConverterParameter = new List<Object> { widthFraction, heightFraction, element.Margin };
            return bindings;
        }

        void PerformInternalMove(WorkflowViewElement movedElement, Point newPoint, Point shapeAnchorPoint)
        {
            using (EditingScope es = (EditingScope)this.ModelItem.BeginEdit("ItemMove"))
            {
                RemoveAdorner(movedElement, typeof(ConnectionPointsAdorner));
                Point newLocation = SnapVisualToGrid(movedElement, newPoint, shapeAnchorPoint);
                object viewState = this.ViewStateService.RetrieveViewState(movedElement.ModelItem, ShapeLocationViewStateKey);
                if (viewState != null)
                {
                    Point oldLocation = (Point)viewState;
                    // To make sure the connectors are still connected to the connection points
                    this.OffsetConnectorViewState(movedElement.ModelItem, oldLocation, newLocation);
                }
                this.StoreShapeLocationViewState(movedElement, newLocation);
                // To make sure the connector changes are undoable
                this.StoreAttachedConnectorViewStates(movedElement);
                es.Complete();
            }
        }
        public void StoreAttachedConnectorViewStates(UIElement element)
        {
            foreach (Connector connector in GetAttachedConnectors(element))
            {
                StoreConnectorLocationViewState(connector, true);
            }
        }

        internal static ModelItem GetConnectorModelItem(DependencyObject obj)
        {
            return (ModelItem)obj.GetValue(FlowDesigner.ConnectorModelItemProperty);
        }

        static void SetConnectorModelItem(DependencyObject obj, ModelItem modelItem)
        {
            obj.SetValue(FlowDesigner.ConnectorModelItemProperty, modelItem);
        }


        void OffsetConnectorViewState(ModelItem stateModelItem, Point oldLocation, Point newLocation)
        {
            this.OffsetLocationViewStates(
                new Vector(newLocation.X - oldLocation.X, newLocation.Y - oldLocation.Y), null,
                GetTransitionModelItems(new List<ModelItem> { stateModelItem }), true);
        }

        static List<ModelItem> GetTransitionModelItems(IEnumerable<ModelItem> stateModelItems)
        {
            List<ModelItem> transitionModelItems = new List<ModelItem>();
            foreach (ModelItem stateModelItem in stateModelItems)
            {
                transitionModelItems.AddRange(stateModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection);
                //transitionModelItems.AddRange(GetTransitionModelItems(stateModelItem.Properties[ChildStatesPropertyName].Collection));
            }
            return transitionModelItems;
        }

        void StoreShapeLocationViewState(WorkflowViewElement view, Point newLocation)
        {
            StoreShapeLocationViewState(view.ModelItem, newLocation);
        }

        void StoreShapeLocationViewState(ModelItem storageModelItem, Point newLocation)
        {
            if (this.ViewStateService.RetrieveViewState(storageModelItem, ShapeLocationViewStateKey) != null)
            {
                this.ViewStateService.StoreViewStateWithUndo(storageModelItem, ShapeLocationViewStateKey, newLocation);
            }
            else
            {
                this.ViewStateService.StoreViewState(storageModelItem, ShapeLocationViewStateKey, newLocation);
            }
        }

        void StoreConnectorLocationViewState(ModelItem connectorModelItem, PointCollection viewState, bool isUndoableViewState)
        {
            if (isUndoableViewState)
            {
                this.ViewStateService.StoreViewStateWithUndo(connectorModelItem, ConnectorLocationViewStateKey, viewState);
            }
            else
            {
                this.ViewStateService.StoreViewState(connectorModelItem, ConnectorLocationViewStateKey, viewState);
            }
        }

        void StoreConnectorLocationViewState(Connector connector, bool isUndoableViewState)
        {
            //This method will be called whenever the FreeFormPanel raises a location changed event on a connector.
            //Such location changed events are a result of changes already committed in the UI. Hence we do not want to react to such view state changes.
            //Using internalViewStateChange flag for that purpose.
            this.internalViewStateChange = true;
            this.StoreConnectorLocationViewState(FlowDesigner.GetConnectorModelItem(connector), connector.Points, isUndoableViewState);
            this.internalViewStateChange = false;
        }



        void OnStateCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            FlowDesigner outmostEditor = this;
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    foreach (ModelItem deleted in e.OldItems)
                    {
                        if (deleted != null)
                        {
                            ModelItemCollection transitions = deleted.Properties[StateDesignerConstants.TransitionsPropertyName].Collection;
                            if (outmostEditor.listenedTransitionCollections.Contains(transitions))
                            {
                                transitions.CollectionChanged -=
                                    new NotifyCollectionChangedEventHandler(outmostEditor.OnTransitionCollectionChanged);
                                outmostEditor.listenedTransitionCollections.Remove(transitions);
                            }

                            if (this.modelItemToUIElement.ContainsKey(deleted))
                            {
                                this.RemoveStateVisual(this.modelItemToUIElement[deleted] as WorkflowViewElement);
                            }
                        }
                    }
                }
            }

            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems != null)
                {
                    foreach (ModelItem added in e.NewItems)
                    {
                        if (added != null)
                        {
                            ModelItemCollection transitions = added.Properties[StateDesignerConstants.TransitionsPropertyName].Collection;
                            if (!outmostEditor.listenedTransitionCollections.Contains(transitions))
                            {
                                transitions.CollectionChanged +=
                                    new NotifyCollectionChangedEventHandler(outmostEditor.OnTransitionCollectionChanged);
                                outmostEditor.listenedTransitionCollections.Add(transitions);
                            }
                            this.AddStateVisuals(new List<ModelItem> { added });
                        }
                    }
                }
            }
        }

        void OnTransitionCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    foreach (ModelItem deleted in e.OldItems)
                    {
                        if (deleted != null)
                        {
                            Connector connector = this.GetConnectorOnOutmostEditor(deleted);
                            if (connector != null)
                            {
                                this.RemoveConnectorOnOutmostEditor(connector);
                            }
                        }
                    }
                }
            }

            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // We have to postpone updating the visual until the editing scope completes because 
                // the connector view state is not available at this moment
                foreach (ModelItem item in e.NewItems)
                {
                    if (!this.transitionModelItemsAdded.Contains(item))
                    {
                        this.transitionModelItemsAdded.Add(item);
                    }
                }
            }
        }

        void OnModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == FlowDesigner.InitialNodePropertyName)
            {
                //Debug.Assert(this.ModelItem.ItemType == typeof(Flow), "Only StateMachine should have initial state");
                this.initialStateChanged = true;
            }
        }

        // All the connectors are directly contained by the outmost editor. This is because connectors can go across states.
        void OnEditingScopeCompleted(object sender, EditingScopeEventArgs e)
        {
            //Debug.Assert(this.IsOutmostNodeContainerEditor(), "Only the outmost editor should listen to the EditingScopeCompleted events of the model tree.");
            if (this.transitionModelItemsAdded.Count > 0)
            {
                // We need to wait until after the state visuals are updated
                this.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
                {
                    foreach (ModelItem transition in this.transitionModelItemsAdded)
                    {
                        ModelItem srcStateModelItem = FlowDesigner.GetParentStateModelItemForTransition(transition);
                        this.AddTransitionVisual(transition);
                    }
                    this.transitionModelItemsAdded.Clear();
                }));
            }
            if (this.initialStateChanged)
            {
                //Debug.Assert(this.ModelItem.ItemType == typeof(Flow), "Only StateMachine should have initial state");
                //Debug.Assert(this.initialNode != null, "Initial node should not be null");

                // Remove the old link
                if (GetAttachedConnectors(this.initialNode).Count > 0)
                {
                    this.RemoveConnectorOnOutmostEditor(GetAttachedConnectors(this.initialNode)[0]);
                }
                // Add the new link if the new initial state is not null
                ModelItem initialStateModelItem = this.ModelItem.Properties[FlowDesigner.InitialNodePropertyName].Value;
                if (initialStateModelItem != null)
                {
                    // We need to wait until after the state visuals are updated
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
                    {
                        this.AddInitialNodeConnector();
                    }));
                }
                this.initialStateChanged = false;
            }
        }

        void AddTransitionVisual(ModelItem transitionModelItem)
        {
           // Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");
            UIElement sourceDesigner = FlowDesigner.GetParentStateModelItemForTransition(transitionModelItem).View as UIElement;
            UIElement destinationDesigner = transitionModelItem.Properties["To"].Value.View as UIElement;
            if (sourceDesigner.IsDescendantOf(this) && destinationDesigner.IsDescendantOf(this))
            {
                this.AddConnector(sourceDesigner, destinationDesigner, transitionModelItem);
            }
        }

        void AddTransitionVisuals(IList<ModelItem> transitionModelItemCollection)
        {
            //Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");
            foreach (ModelItem transitionModelItem in transitionModelItemCollection)
            {
                this.AddTransitionVisual(transitionModelItem);
            }
        }


        void RemoveConnectorOnOutmostEditor(Connector connector)
        {
            ConnectionPoint srcConnectionPoint = FreeFormPanel.GetSourceConnectionPoint(connector);
            ConnectionPoint destConnectionPoint = FreeFormPanel.GetDestinationConnectionPoint(connector);
            // Update ConnectionPoints          
            srcConnectionPoint.AttachedConnectors.Remove(connector);
            destConnectionPoint.AttachedConnectors.Remove(connector);

            FlowDesigner outmostContainer = this;
            outmostContainer.panel.Children.Remove(connector);
            if (outmostContainer.selectedConnector == connector)
            {
                outmostContainer.ClearSelectedConnector();
            }
        }


        void ClearSelectedConnector()
        {
            if (this.panel.connectorEditor != null && this.panel.connectorEditor.Connector == this.selectedConnector)
            {
                this.panel.RemoveConnectorEditor();
            }
            this.selectedConnector = null;
        }


        void OnViewStateChanged(object sender, ViewStateChangedEventArgs e)
        {
            Debug.Assert(e.ParentModelItem != null, "ViewState should be associated with some modelItem");

            if (!this.internalViewStateChange)
            {
                if (e.ParentModelItem == this.ModelItem)
                {
                    if (string.Equals(e.Key, WidthViewStateKey, StringComparison.Ordinal))
                    {
                        double defaultWidth =  DefaultWidth;
                        object widthViewState = this.ViewStateService.RetrieveViewState(this.ModelItem, WidthViewStateKey);
                        this.Width = (widthViewState != null) ? (double)widthViewState : defaultWidth;
                    }
                    else if (string.Equals(e.Key, HeightViewStateKey, StringComparison.Ordinal))
                    {
                        double defaultHeight = DefaultHeight;
                        object heightViewState = this.ViewStateService.RetrieveViewState(this.ModelItem, HeightViewStateKey);
                        this.Height = (heightViewState != null) ? (double)heightViewState : defaultHeight;
                    }
                }

                if (e.Key.Equals(ShapeLocationViewStateKey))
                {
                    if (this.modelItemToUIElement.ContainsKey(e.ParentModelItem))
                    {
                        if (e.NewValue != null)
                        {
                            FreeFormPanel.SetLocation(this.modelItemToUIElement[e.ParentModelItem], (Point)e.NewValue);
                            this.panel.InvalidateMeasure();
                            if (e.OldValue != null)
                            {
                                this.shapeLocations.Remove((Point)e.OldValue);
                            }
                            this.shapeLocations.Add((Point)e.NewValue);
                            // To reroute the links
                            this.panel.InvalidateMeasure();
                        }
                    }
                }

                else if (e.Key.Equals(ShapeSizeViewStateKey))
                {
                    // To reroute the links
                    this.panel.InvalidateMeasure();
                }

                // Only the outmost editor should respond to connector changes because all connectors are
                // only added to the outmost editor
                else if (e.Key.Equals(ConnectorLocationViewStateKey))
                {
                    Connector changedConnector = this.GetConnectorOnOutmostEditor(e.ParentModelItem);
                    if (changedConnector != null)
                    {
                        if (e.NewValue != null)
                        {
                            Debug.Assert(e.NewValue is PointCollection, "e.NewValue is not PointCollection");
                            changedConnector.Points = e.NewValue as PointCollection;
                            this.panel.RemoveConnectorEditor();
                            this.panel.InvalidateMeasure();
                            if (IsConnectorFromInitialNode(changedConnector))
                            {
                                this.initialStateChanged = true;
                            }
                        }
                    }
                }
            }


        }

        Connector GetConnectorOnOutmostEditor(ModelItem connectorModelItem)
        {
            FlowDesigner outmostEditor = this;
            foreach (UIElement element in outmostEditor.panel.Children)
            {
                Connector connector = element as Connector;
                if (connector != null)
                {
                    if (FlowDesigner.GetConnectorModelItem(connector) == connectorModelItem)
                    {
                        return connector;
                    }
                }
            }
            return null;
        }

        static bool IsConnectorFromInitialNode(Connector connector)
        {
            return GetConnectorModelItem(connector).ItemType == typeof(Flow);
        }

        #region ConnectorEventHandlers

        void OnConnectorStartDotMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.activeSrcConnectionPoint = null;
            RemoveAdorner(this.panel, typeof(ConnectorCreationAdorner));
        }

        void OnConnectorStartDotMouseDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject startDot = (DependencyObject)sender;
            Connector connector = Utility.GetVisualAncestor<Connector>(startDot);
            this.activeSrcConnectionPoint = FreeFormPanel.GetSourceConnectionPoint(connector);
            e.Handled = true;
        }

        void OnConnectorMouseDown(object sender, MouseButtonEventArgs e)
        {
            Connector connector = (Connector)sender;
            if (this.panel.Children.Contains(connector))
            {
                this.selectedConnector = connector;
            }
            // In order to not let WorkflowViewElement handle the event, which would cause the
            // ConnectorEditor to be removed.
            e.Handled = true;
        }

        void OnConnectorUnloaded(object sender, RoutedEventArgs e)
        {
            ModelItem primarySelection = this.Context.Items.GetValue<Selection>().PrimarySelection;
            if (object.Equals(primarySelection, FlowDesigner.GetConnectorModelItem(sender as DependencyObject)))
            {
                if (primarySelection != null)
                {
                    Selection.Toggle(this.Context, primarySelection);
                }
            }
        }

        // Marking e.Handled = true to avoid scrolling in large workflows to bring the 
        // area of a connector in the center of the view region.
        void OnConnectorRequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }

        // Marking e.Handled true for the case where a connector is clicked. 
        // This is to prevent WorkflowViewElement class from making StateMachine as the current selection.
        void OnConnectorGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            e.Handled = true;
        }

        void OnConnectorLostFocus(object sender, RoutedEventArgs e)
        {
            Connector connector = e.Source as Connector;

            if (this.panel.connectorEditor != null && connector.Equals(this.panel.connectorEditor.Connector))
            {
                this.panel.RemoveConnectorEditor();
            }
        }

        void OnConnectorGotFocus(object sender, RoutedEventArgs e)
        {
            Connector connector = e.Source as Connector;

            if (this.panel.connectorEditor == null || !connector.Equals(this.panel.connectorEditor.Connector))
            {
                this.panel.RemoveConnectorEditor();
                this.panel.connectorEditor = new ConnectorEditor(this.panel, connector);
            }

            if (this.panel.Children.Contains(connector))
            {
                ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(connector);
                Selection newSelection = new Selection();
                if (connectorModelItem != null && connectorModelItem.ItemType == typeof(Transition))
                {
                    newSelection = new Selection(connectorModelItem);
                }
                this.Context.Items.SetValue(newSelection);
                this.selectedConnector = connector;
                e.Handled = true;
            }
        }

        void OnConnectorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                this.Designer.MakeRootDesigner(FlowDesigner.GetConnectorModelItem(sender as DependencyObject));
                e.Handled = true;
            }
        }

        void OnConnectorMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ModelItem connectorModelItem = FlowDesigner.GetConnectorModelItem(sender as DependencyObject);
            //if (connectorModelItem != null && connectorModelItem.ItemType == typeof(Transition))
            //{
            //    this.DesignerView.MakeRootDesigner(connectorModelItem);
            //    e.Handled = true;
            //}
        }

        void OnConnectorContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            // Disable context menu
            e.Handled = true;
        }

        #endregion

        #region ConnectionPointEventHandlers

        void OnConnectionPointMouseDown(object sender, MouseButtonEventArgs e)
        {
            UIElement srcElement = ((Adorner)sender).AdornedElement as UIElement;
            this.activeSrcConnectionPoint = ConnectionPointHitTest(srcElement, e.GetPosition(this.panel), ConnectionPointType.Outgoing);
            e.Handled = true;
        }

        void OnConnectionPointMouseLeave(object sender, MouseEventArgs e)
        {
            UIElement adornedElement = ((Adorner)sender).AdornedElement as UIElement;
            RemoveAdorner(adornedElement, typeof(ConnectionPointsAdorner));
        }

        void OnConnectionPointMouseUp(object sender, MouseButtonEventArgs e)
        {
            UIElement dest = ((Adorner)sender).AdornedElement as UIElement;
            if (this.activeSrcConnectionPoint != null)
            {
                ConnectionPoint destConnectionPoint = ConnectionPointHitTest(dest, e.GetPosition(this.panel), ConnectionPointType.Incoming);
                if (destConnectionPoint != null && !this.activeSrcConnectionPoint.Equals(destConnectionPoint))
                {
                    ConnectorCreationResult result = CreateConnectorGesture(this.activeSrcConnectionPoint, destConnectionPoint, null, false);
                    if (result != ConnectorCreationResult.Success)
                    {
                        FlowDesigner.ReportConnectorCreationError(result);
                    }
                }
                this.activeSrcConnectionPoint = null;
                RemoveAdorner(this.panel, typeof(ConnectorCreationAdorner));
            }
            else
            {
                //This will cause the FreeFormPanel to handle the event and is useful while moving a connector end point.
                this.lastConnectionPointMouseUpElement = dest;
                dest.RaiseEvent(e);
            }
            RemoveAdorner(dest, typeof(ConnectionPointsAdorner));
        }

        #endregion

        // referenceConnector is used when we are re-linking the connector.
        internal ConnectorCreationResult CreateConnectorGesture(ConnectionPoint sourceConnectionPoint, ConnectionPoint destConnectionPoint, Connector referenceConnector, bool isConnectorStartMoved)
        {
            //Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");
            //Debug.Assert(sourceConnectionPoint != null, "sourceConnectionPoint is null.");
            //Debug.Assert(destConnectionPoint != null, "destConnectionPoint is null.");
            ConnectorCreationResult result = ConnectorCreationResult.OtherFailure;
            if (destConnectionPoint.PointType != ConnectionPointType.Outgoing && sourceConnectionPoint.PointType != ConnectionPointType.Incoming)
            {
                if (sourceConnectionPoint.ParentDesigner is InitialNode)
                {
                    ModelItem destModelItem = (destConnectionPoint.ParentDesigner as WorkflowViewElement).ModelItem;
                    using (EditingScope es = (EditingScope)this.ModelItem.BeginEdit("SetInitialState"))
                    {
                        this.FlowModelItem.Properties[FlowDesigner.InitialNodePropertyName].SetValue(destModelItem);
                        PointCollection connectorViewState = new PointCollection(ConnectorRouter.Route(this.panel, sourceConnectionPoint, destConnectionPoint));
                        this.StoreConnectorLocationViewState(this.ModelItem, connectorViewState, true);
                        result = ConnectorCreationResult.Success;
                        es.Complete();
                    }
                }
                else
                {
                    bool sameDestination = false;
                    ModelItem refTransitionModelItem = null;
                    if (referenceConnector != null)
                    {
                        refTransitionModelItem = FlowDesigner.GetConnectorModelItem(referenceConnector);
                        ModelItem destStateModelItem = ((WorkflowViewElement)destConnectionPoint.ParentDesigner).ModelItem;
                        if (refTransitionModelItem != null && refTransitionModelItem.Properties["To"].Value == destStateModelItem) //TransitionDesigner.ToPropertyName
                        {
                            sameDestination = true;
                        }
                    }
                    else
                    {
                        using (EditingScope es = (EditingScope)this.ModelItem.BeginEdit("CreateTransition"))
                        {
                            if (refTransitionModelItem != null)
                            {
                                this.CreateTransition(sourceConnectionPoint, destConnectionPoint, refTransitionModelItem, isConnectorStartMoved);
                            }
                            else
                            {
                                this.CreateTransition(sourceConnectionPoint, destConnectionPoint, null, false);
                            }
                            result = ConnectorCreationResult.Success;
                            es.Complete();
                        }
                    }
                }
              
            }
            return result;
        }

        void CreateTransition(ConnectionPoint sourceConnPoint, ConnectionPoint destConnPoint, ModelItem referenceTransitionModelItem, bool isSourceMoved)
        {
            //Debug.Assert(this.IsOutmostStateContainerEditor(), "Should only be called by the outmost editor.");

            WorkflowViewElement srcDesigner = sourceConnPoint.ParentDesigner as WorkflowViewElement;
            WorkflowViewElement destDesigner = destConnPoint.ParentDesigner as WorkflowViewElement;
            //Debug.Assert(srcDesigner is IStateDesigner && destDesigner is IStateDesigner, "The source and destination designers should both be IStateDesigner");

            ModelItem srcModelItem = srcDesigner.ModelItem;
            ModelItem destModelItem = destDesigner.ModelItem;
            ModelItem transitionModelItem = null;

            // We are moving the connector.
            if (referenceTransitionModelItem != null && referenceTransitionModelItem.ItemType == typeof(Transition))
            {
                transitionModelItem = referenceTransitionModelItem;
                // We are moving the start of the connector. We only preserve the trigger if it is not shared.
                if (isSourceMoved)
                {
                    Transition referenceTransition = referenceTransitionModelItem.GetCurrentValue() as Transition;
                    ModelItem stateModelItem = GetParentStateModelItemForTransition(referenceTransitionModelItem);
                    State state = stateModelItem.GetCurrentValue() as State;
                    bool isTriggerShared = false;
                    foreach (Transition transition in state.Transitions)
                    {
                        if (transition != referenceTransition && transition.Trigger == referenceTransition.Trigger)
                        {
                            isTriggerShared = true;
                            break;
                        }
                    }
                    if (isTriggerShared)
                    {
                        transitionModelItem.Properties["Trigger"].SetValue(null);
                    }
                }
                transitionModelItem.Properties["To"].SetValue(destModelItem);
                srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Add(transitionModelItem);
            }
            // We are creating a new connector. 
            else
            {
                Transition newTransition = new Transition() { DisplayName = string.Empty };
                if (typeof(ConfirmScreen).IsAssignableFrom(srcModelItem.ItemType))
                {
                    newTransition.DisplayName = "yes";
                    if (srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Any(x=>x.Properties["DisplayName"].ComputedValue.ToString() == "yes"))
                    {
                        newTransition.DisplayName = "no";
                    }
                }
                else if (typeof(MenuScreen).IsAssignableFrom(srcModelItem.ItemType))
                {
                    for (int i = 1; i < srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Count + 2; i++)
                    {
                        if (!srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Any(x => x.Properties["DisplayName"].ComputedValue.ToString() == i.ToString()))
                        {
                            newTransition.DisplayName = i.ToString();
                            break;
                        }
                    }
                }
                else if (typeof(ActionNode).IsAssignableFrom(srcModelItem.ItemType))
                {
                    newTransition.DisplayName = "";
                    if (srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Any(x => x.Properties["DisplayName"].ComputedValue.ToString() == ""))
                    {
                        newTransition.DisplayName = "failure";
                    }
                }
                newTransition.To = destModelItem.GetCurrentValue() as State;
                // Assign the shared trigger.
                if (sourceConnPoint.AttachedConnectors.Count > 0)
                {
                    Connector connector = sourceConnPoint.AttachedConnectors[0];
                    Transition existingTransition = FlowDesigner.GetConnectorModelItem(connector).GetCurrentValue() as Transition;
                    newTransition.Trigger = existingTransition.Trigger;
                }
                transitionModelItem = srcModelItem.Properties[StateDesignerConstants.TransitionsPropertyName].Collection.Add(newTransition);
            }
            if (transitionModelItem != null)
            {
                PointCollection connectorViewState = new PointCollection(ConnectorRouter.Route(this.panel, sourceConnPoint, destConnPoint));
                this.StoreConnectorLocationViewState(transitionModelItem, connectorViewState, true);
            }
        }

        internal static ModelItem GetParentStateModelItemForTransition(ModelItem transitionModelItem)
        {
            ModelItem parent = transitionModelItem;
            while (parent != null && parent.ItemType != typeof(State) && !typeof(State).IsAssignableFrom(parent.ItemType))
            {
                parent = parent.Parent;
            }
            return parent;
        }

        internal ConnectorCreationResult CreateConnectorGesture(ConnectionPoint sourceConnectionPoint, UIElement dest, Connector referenceConnector, bool isConnectorStartMoved)
        {
            ConnectionPoint destConnectionPoint = GetClosestDestConnectionPoint(sourceConnectionPoint, dest);
            if (destConnectionPoint != null)
            {
                return CreateConnectorGesture(sourceConnectionPoint, destConnectionPoint, referenceConnector, isConnectorStartMoved);
            }
            return ConnectorCreationResult.OtherFailure;
        }

        internal ConnectorCreationResult CreateConnectorGesture(UIElement source, ConnectionPoint destConnectionPoint, Connector referenceConnector, bool isConnectorStartMoved)
        {
            ConnectionPoint sourceConnectionPoint = GetClosestSrcConnectionPoint(source, destConnectionPoint);
            if (sourceConnectionPoint != null)
            {
                return CreateConnectorGesture(sourceConnectionPoint, destConnectionPoint, referenceConnector, isConnectorStartMoved);
            }
            return ConnectorCreationResult.OtherFailure;
        }

        //This returns the closest non-incoming connectionPoint on source. Return value will be different than destConnectionPoint.
        static ConnectionPoint GetClosestSrcConnectionPoint(UIElement src, ConnectionPoint destConnectionPoint)
        {
            ConnectionPoint srcConnectionPoint = null;
            if (destConnectionPoint.PointType != ConnectionPointType.Outgoing)
            {
                srcConnectionPoint = GetClosestConnectionPointNotOfType(destConnectionPoint, FlowDesigner.GetConnectionPoints(src), ConnectionPointType.Incoming);
            }
            return srcConnectionPoint;
        }

        //This returns the closest non-outgoing connectionPoint on dest. Return value will be different than sourceConnectionPoint.
        static ConnectionPoint GetClosestDestConnectionPoint(ConnectionPoint sourceConnectionPoint, UIElement dest)
        {
            ConnectionPoint destConnectionPoint = null;
            if (sourceConnectionPoint.PointType != ConnectionPointType.Incoming)
            {
                destConnectionPoint = GetClosestConnectionPointNotOfType(sourceConnectionPoint, FlowDesigner.GetConnectionPoints(dest), ConnectionPointType.Outgoing);
            }
            return destConnectionPoint;
        }

        static void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "ErrorMessageBoxTitle", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        static void ReportConnectorCreationError(ConnectorCreationResult result)
        {
            switch (result)
            {
                case ConnectorCreationResult.CannotCreateTransitionToCompositeState:
                    ShowMessageBox("CannotCreateTransitionToCompositeState");
                    break;
                case ConnectorCreationResult.CannotCreateTransitionFromAncestorToDescendant:
                    ShowMessageBox("CannotCreateTransitionFromAncestorToDescendant");
                    break;
                case ConnectorCreationResult.CannotSetCompositeStateAsInitialState:
                    ShowMessageBox("CannotSetCompositeStateAsInitialState");
                    break;
                case ConnectorCreationResult.CannotSetFinalStateAsInitialState:
                    ShowMessageBox("CannotSetFinalStateAsInitialState");
                    break;
                case ConnectorCreationResult.OtherFailure:
                    ShowMessageBox("CannotCreateLink");
                    break;
            }
        }
    }

    internal enum ConnectorCreationResult
    {
        Success,
        CannotCreateTransitionToCompositeState,
        CannotCreateTransitionFromAncestorToDescendant,
        CannotSetCompositeStateAsInitialState,
        CannotSetFinalStateAsInitialState,
        OtherFailure
    }


}
