﻿
namespace BankOneMobile.SessionNodes
{
    using System;
    using System.Windows.Markup;
    using System.ComponentModel;
    using System.Collections.ObjectModel;
    using System.Activities;
    using BankOneMobile.SessionNodes;
    using Microsoft.Activities.Statements;
using Microsoft.Samples.UsingWorkflowItemPresenter;

    /// <summary>
    /// This class represents a State in a StateMachine and it can contain child States.
    /// </summary>
    
    [ContentProperty("InternalState")]
    public abstract class State
    {
        internal protected Flow TheFlow { get; set; }

        public abstract State Next
        {
            get;
            set;
        }

        Collection<Transition> transitions;

        /// <summary>
        /// DisplayName of the State.
        /// </summary>
        public virtual string DisplayName
        {
            get;
            set;
        }

      
        /// <summary>
        /// Transitions collection contains all outgoing Transitions from the State.
        /// </summary>
        [Browsable(false)]
        public virtual Collection<Transition> Transitions
        {
            get
            {
                if (this.transitions == null)
                {
                    this.transitions = new ValidatingCollection<Transition>
                    {

                        // disallow null values
                        OnAddValidationCallback = item =>
                        {
                            if (item == null)
                            {
                                throw new ArgumentNullException("item");
                            }
                        },
                    };
                }
                return this.transitions;
            }
        }

        /// <summary>
        /// Internal activity representation of state.
        /// </summary>
        [Browsable(false)]
        public virtual Activity InternalState
        {
            get;
            set;
        }

        internal bool SingleExecute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            return Execute(context, onCompleted, out nextNode);
        }

        protected abstract bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode);
        /// <summary>
        /// StateId is unique within a StateMachine.
        /// </summary>
        [Browsable(false)]
        public string StateId
        {
            get;
            set;
        }


        /// <summary>
        /// Reached denotes whether state can be reached via transitions.
        /// </summary>
        internal bool Reachable
        {
            get;
            set;
        }

        /// <summary>
        /// PassNumber is used to detect re-visiting when traversing states in StateMachine. 
        /// </summary>
        internal uint PassNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Clear internal state. 
        /// </summary>
        internal void ClearInternalState()
        {
            this.InternalState = null;
        }

        public abstract string GetDefaultDisplayName();
        public abstract void GetChildActivities(NativeActivityMetadata metadata);

    }
}
