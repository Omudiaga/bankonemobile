﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

namespace Microsoft.Activities.Statements.Design
{
    using System;
    using System.Activities.Presentation.Metadata;
    using System.ComponentModel;
    using Microsoft.Activities.Statements;
    using BankOneMobile.SessionNodes;
    using BankOneMobile.SessionNodeDesigners;

    /// <summary>
    /// Contains metadata for the StateMachine related designers.
    /// </summary>
    public class DesignerMetadata : IRegisterMetadata
    {
        /// <summary>
        /// Registers metadata for the StateMachine related designers.
        /// </summary>
        public void Register()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            
            Type stateMachineType = typeof(Flow);
            builder.AddCustomAttributes(stateMachineType, new DesignerAttribute(typeof(FlowDesigner)));
            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
    }
}
