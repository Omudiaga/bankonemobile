﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

namespace BankOneMobile.SessionNodeDesigners
{
    using System.Activities.Presentation.Model;
    using System.Activities.Presentation.View;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    partial class InitialNode
    {
        public InitialNode()
        {
            InitializeComponent();
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            // Disable context menu
            e.Handled = true;
        }
    }
}
