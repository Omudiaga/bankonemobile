﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using BankOneMobile.Services;
using System.Activities.Presentation.Metadata;
using System.ComponentModel;
using BankOneMobile.Core.Contracts;
using System.IO;
using System.Activities.XamlIntegration;
using System.Xaml;
using System.Collections.ObjectModel;
using BankOneMobile.SessionNodes;
using BankOneMobile.Services.WorkflowServices;

namespace BankOneMobile.SessionNodeDesigners.Actions
{
    public class GotoFlowAction : ActionNode
    {
        protected CompletionCallback _onCompleted;

      [Browsable(false)]
        public Guid FlowID
        {
            get
            {
                if (this.InternalState == null) return Guid.Empty;
                return (this.InternalState as GotoFlowActivity).FlowID;
            }
            set
            {
                if (this.InternalState != null)
                {
                    (this.InternalState as GotoFlowActivity).FlowID = value;
                }
            }
        }

      public IDictionary<string, DynamicActivityProperty> Properties { get; set; }
      public IDictionary<string, InArgument<String>> DynProperties { get; set; }

        public GotoFlowAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.DynProperties = new Dictionary<string, InArgument<String>>();
            this.Properties = new Dictionary<string, DynamicActivityProperty>();
            this.InternalState = new GotoFlowActivity() { FlowAction = this };
        }

        static GotoFlowAction()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(GotoFlowAction);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(GotoFlowDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
            
        }

        public override void GetChildActivities(NativeActivityMetadata metadata)
        {
            if (this.InternalState != null) (this.InternalState as GotoFlowActivity).FlowAction = this;
            base.GetChildActivities(metadata);
        }
        //public override void GetChildActivities(NativeActivityMetadata metadata)
        //{
        //    try
        //    {
        //        IFlow theFlow = new FlowSystem().GetFlowBy(FlowID);

        //        if (!String.IsNullOrWhiteSpace(theFlow.XamlDefinition))
        //        {
        //            Activity act = null;
        //            //get the definition of the workflow from the function property
        //            using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(theFlow.XamlDefinition)))
        //            {
        //                act = ActivityXamlServices.Load(new XamlXmlReader(ms, new XamlXmlReaderSettings() { }));
        //            }
        //            this.InternalState = act;
        //        }
        //    }
        //    catch { }
        //    base.GetChildActivities(metadata);
        //}
    }

    public class GotoFlowActivity : NativeActivity
    {
        public GotoFlowAction FlowAction { get; set; }
        public Guid FlowID { get; set; }

        protected override bool CanInduceIdle
        {
            get
            {
                return true;
            }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            if (FlowAction != null)
            {
                foreach (KeyValuePair<string, InArgument<string>> keyVal in FlowAction.DynProperties)
                {
                    RuntimeArgument ra = new RuntimeArgument(keyVal.Key, typeof(string), ArgumentDirection.In);
                    metadata.Bind(keyVal.Value, ra);
                    metadata.AddArgument(ra);
                }
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

            string phoneNo = currentWorkflowInfo.InitiatedBy.StandardPhoneNumber;

            currentWorkflowInfo.InitiatingFlowID = FlowID;

            currentWorkflowInfo.ContextInputs = new Dictionary<string, object>();
            foreach (var keyVal in FlowAction.DynProperties)
            {
                currentWorkflowInfo.ContextInputs.Add(keyVal.Key, keyVal.Value.Get(context));
            }

            currentWorkflowInfo.GotoFlow = true;
            //string phoneNoToUse = phoneNo;

          //  WorkflowCenter.Instance.InitiatingWorkflows.AddOrUpdate(phoneNoToUse, currentWorkflowInfo.TheWorkflow.Id, (x, y) => currentWorkflowInfo.TheWorkflow.Id);

            //var response = new TpaWebService().onSessionStart(currentWorkflowInfo.SessionID, new menuRequestParams()
            //{
            //    msisdn = phoneNo,
            //     optional=currentWorkflowInfo.IncludeXML.ToString(),

            //});

            //currentWorkflowInfo.DisplayMessage = response.ussdMenu;
            //currentWorkflowInfo.XmlDisplayMessage = response.ussdMenu;
            //currentWorkflowInfo.IsFinalScreen = response.shouldClose;
            //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();

            context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, Resume_Action_Bookmark);
        }

        protected void Resume_Action_Bookmark(NativeActivityContext context, Bookmark bookmark, object value)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string response = Convert.ToString(value);
            currentWorkflowInfo.DisplayMessage = null;
            currentWorkflowInfo.XmlDisplayMessage = null;
            //TODO: Check if initiated session is through

           
        }

    }

}
