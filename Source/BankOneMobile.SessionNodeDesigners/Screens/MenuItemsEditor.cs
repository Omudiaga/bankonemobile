﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Presentation.PropertyEditing;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;

namespace BankOneMobile.SessionNodeDesigners.Screens
{
    class MenuItemsEditor : DialogPropertyValueEditor
    {

        public MenuItemsEditor()
        {
            
           this.DialogEditorTemplate = new DataTemplate();
            
           FrameworkElementFactory stack = new FrameworkElementFactory(typeof(StackPanel));

           FrameworkElementFactory textbl = new FrameworkElementFactory(typeof(TextBlock));
           Binding textBlockBinding = new Binding("Value");
           textbl.SetValue(TextBlock.TextProperty, textBlockBinding);
           stack.AppendChild(textbl);

            FrameworkElementFactory textb = new FrameworkElementFactory(typeof(TextBox));
           Binding textBinding = new Binding("Value");
           textb.SetValue(TextBox.TextProperty, textBinding);
           textb.SetValue(TextBox.AcceptsReturnProperty, true);
           textb.SetValue(TextBox.HeightProperty, 100);

           stack.AppendChild(textb);

           this.DialogEditorTemplate.VisualTree = stack;
            

        }
    }
}
