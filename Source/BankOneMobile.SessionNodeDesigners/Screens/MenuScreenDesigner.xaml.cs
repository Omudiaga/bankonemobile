﻿//-------------------------------------------------------------------
// Copyright (c) Microsoft Corporation. All rights reserved
//-------------------------------------------------------------------



using System.Activities.Presentation.Metadata;
using BankOneMobile.SessionNodeDesigners;
using System;
using System.ComponentModel;
using System.Activities.Presentation.PropertyEditing;
using System.Activities.Presentation;
using System.Collections.Generic;
using BankOneMobile.SessionNodeDesigners.Screens;
using System.Collections;
using System.Windows.Data;
using System.Windows.Controls;

namespace Microsoft.Samples.UsingWorkflowItemPresenter
{

    // Interaction logic for SimpleNativeDesigner.xaml
    public partial class MenuScreenDesigner  : IStateDesigner
    {

        internal const string MenuPropertyName = "TheMenu";
        internal const string DisplayPropertyName = "Display";
        public string Display { get; set; }

        public MenuScreenDesigner()
        {
            InitializeComponent();
        }


        private void btnAdd_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MenuManager window = new MenuManager(this.ModelItem, this.DisplayChanged);
            bool? result = window.ShowDialog();
        }

        void DisplayChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateDisplay();
        }

        public void UpdateDisplay()
        {
            try
            {
                TextBlock lblMenu = this.cpMain.ContentTemplate.FindName("lblMenu", this.cpMain) as TextBlock;
                if (lblMenu != null)
                {
                    lblMenu.Text = (this.ModelItem.GetCurrentValue() as ScreenNode).GetDisplay(null);
                }
            }
            catch { }
        }

        private void ActivityDesigner_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            //btnAdd_Click(this, new System.Windows.RoutedEventArgs());
            UpdateDisplay();
            this.ModelItem.PropertyChanged += DisplayChanged;
        }

        private void ActivityDesigner_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            UpdateDisplay();
        }


    }
}
