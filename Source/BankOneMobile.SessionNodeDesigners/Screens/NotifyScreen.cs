﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    public class NotifyScreen : ScreenNode
    {
        private CompletionCallback _onCompleted;
        public InArgument<string> DisplayText
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as CustomScreenActivity).DisplayText;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as CustomScreenActivity).DisplayText = value;
                }
            }
        }
        public string NotificationText { set; get; }

        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(NotificationText);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                }
            }
            else
            {
                result.AppendLine(NotificationText);
            }            
            return result.ToString();
        }

        public override string GetXmlDisplay(NativeActivityContext context)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            {
                if (context != null)
                {
                    node.SetValue(NotificationText);
                }
                else
                {
                    node.SetValue("<Dynamic Text>");
                }
            }
            else
            {
                node.SetValue(NotificationText);
            }
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
            
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public NotifyScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new CustomScreenActivity();
        }

        static NotifyScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(NotifyScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            if (String.IsNullOrEmpty(this.NotificationText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                _onCompleted = onCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;

            nextNode = null;
            return true;
        }

        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.NotificationText = (instance.Activity as CustomScreenActivity).ComputedDisplay;
            State nextNode;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;
            nextNode = null;
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            this.Next = null;

            if (_OnCompleted != null)
            {
                _OnCompleted.Invoke(context, null);
            }
        }
    }

    public class InternalScreen : NativeActivity<string>
    {
        protected override bool CanInduceIdle
        {
            get
            {
                return true;
            }
        }
        protected override void Execute(NativeActivityContext context)
        {
            context.CreateBookmark("Default", Resume_Bookmark);
        }

        void Resume_Bookmark(NativeActivityContext context, Bookmark bookmark, object value)
        {
            
        }
    }
}
