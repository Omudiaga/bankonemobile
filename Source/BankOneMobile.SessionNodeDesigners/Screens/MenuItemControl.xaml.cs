﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Activities.Presentation.Model;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.ComponentModel;

namespace BankOneMobile.SessionNodeDesigners.Screens
{
    /// <summary>
    /// Interaction logic for MenuItemControl.xaml
    /// </summary>
    
    public partial class MenuItemControl : UserControl
    {
        private ModelItem _MenuItem;
        private PropertyChangedEventHandler _propertyChanged;
        public MenuItemControl()
        {
            InitializeComponent();
        }

        public MenuItemControl(ModelItem menuItem, PropertyChangedEventHandler propertyChanged) : this()
        {
            this.DataContext =  _MenuItem = menuItem;
            _propertyChanged = propertyChanged;
        }

        private void ucMenuItem_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Background = new System.Windows.Media.LinearGradientBrush(Colors.WhiteSmoke, Colors.AliceBlue, 90);
        }

        private void ucMenuItem_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Background = Brushes.White;
        }

        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            int oldIndex = this._MenuItem.Parent.Source.Collection.IndexOf(this._MenuItem);
            if (oldIndex > 0)
            {
                this._MenuItem.Properties["Index"].SetValue(oldIndex - 1);
                this._MenuItem.Parent.Source.Collection.Move(oldIndex, oldIndex - 1);
            }
        }

        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            int oldIndex = this._MenuItem.Parent.Source.Collection.IndexOf(this._MenuItem);
            if (oldIndex < this._MenuItem.Parent.Source.Collection.Count - 1)
            {
                this._MenuItem.Properties["Index"].SetValue(oldIndex + 1);
                this._MenuItem.Parent.Source.Collection.Move(oldIndex, oldIndex + 1);
            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            this._MenuItem.Parent.Source.Collection.Remove(this._MenuItem);
        }

        private void ucMenuItem_Loaded(object sender, RoutedEventArgs e)
        {
            this._MenuItem.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(_propertyChanged);
        }

        private void ucMenuItem_Unloaded(object sender, RoutedEventArgs e)
        {
            this._MenuItem.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(_propertyChanged);
        }
    }
}
