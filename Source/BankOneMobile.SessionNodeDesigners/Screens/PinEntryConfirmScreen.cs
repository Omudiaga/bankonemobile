﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.Core.Exceptions;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{

    public class PinEntryConfirmScreen : ConfirmScreen
    {
        private const string DEFAULT_PIN_ENTRY = "";
        private const string DEFAULT_PIN_ENTRY_STATEMENT = "Enter your PIN";
        public string RequestText { set; get; }
        public int PINLength { set; get; }
        public bool IsNumeric { set; get; }
        public bool NoVerification { set; get; }
       

        private CompletionCallback _onCompleted;
        
        
        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            
                result.AppendLine(string.Format("{0} {1} {2}", DEFAULT_PIN_ENTRY, Environment.NewLine,(this.InternalActivity as CustomScreenActivity).ComputedDisplay));
            
            return result.ToString();
        }

        public override string GetXmlDisplay(NativeActivityContext context)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            node.SetValue(string.Format("{0} {1} {2}", DEFAULT_PIN_ENTRY, Environment.NewLine, (this.InternalActivity as CustomScreenActivity).ComputedDisplay));
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", true), 1));
            xmlResponseTree.Add(new XElement("IsNumeric", new XAttribute("IsNumeric", IsNumeric ? true : false), IsNumeric ? 1 : 0));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            node.SetValue(string.Format(textToAdd + "{0} {1} {2}", DEFAULT_PIN_ENTRY, Environment.NewLine, (this.InternalActivity as CustomScreenActivity).ComputedDisplay));
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", true), 1));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public PinEntryConfirmScreen() : base()
        {
           // this.StateId = Guid.NewGuid().ToString();
           // this.InternalActivity = new CustomScreenActivity();
            IsNumeric = true;
            PINLength = 4;
        }

        static PinEntryConfirmScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(PinEntryConfirmScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }


        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            if (response.Trim() == "0")
            {


                Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == "no");
                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_onCompleted != null)
                {
                    _onCompleted.Invoke(context, null);
                }
                return;
            }

            try
            {
                int enteredPin;
                bool validNumber = response == null ? false : Int32.TryParse(response, out enteredPin);
                if (!validNumber && IsNumeric)
                {
                    currentWorkflowInfo.DisplayMessage = "Invalid PIN. Enter numbers only.\r\n\r\n" + this.GetDisplay(context);
                    //currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid PIN. Enter numbers only.<br/>");

                    context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                }
                else if (response != null && response.Trim().Length != PINLength)
                {
                    currentWorkflowInfo.DisplayMessage = string.Format("Invalid PIN. PIN should be {0} {1}.\r\n\r\n", PINLength, IsNumeric ? "Digits" : "Characters", this.GetDisplay(context));
                    currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, string.Format("Invalid PIN. PIN should be {0} {1}.<br/>", PINLength, IsNumeric ? "Digits" : "Characters"));

                    context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                }
                else
                {


                    Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == "yes");
                        if (theTrans != null)
                        {
                            this.Next = theTrans.To;
                        }
                        if (_onCompleted != null)
                        {
                            _onCompleted.Invoke(context, null);
                        }
                    
                }
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException("23", "No Response From HSM");
            }
            catch (CustomHSMException ex)
            {
                throw new FailureException("24", ex.Message);
            }
            
        }




       
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            _onCompleted = onCompleted;
            if (String.IsNullOrEmpty(this.ConfirmationText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            return base.Execute(context, onCompleted, out nextNode);
        }
       
    }


}
