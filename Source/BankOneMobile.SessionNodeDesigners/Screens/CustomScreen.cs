﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    
    public class CustomScreen : ScreenNode
    {
        private CompletionCallback _onCompleted;
        public bool IsPhoneNumber { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsImage { get; set; }
        public bool IsMoney { get; set; }
        public bool IsRecharge { get; set; }
        public bool IsNotification { get; set; }
        public bool IsDateTime { get; set; }
        
        public InArgument<string> DisplayText  
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as CustomScreenActivity).DisplayText;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as CustomScreenActivity).DisplayText = value;
                }
            }
        }
        public string CustomText  { set; get; }
        public bool ShouldMask { get; set; }
        

        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
           if (DisplayText != null)
           {
               if (context != null)
               {
                   result.AppendLine(CustomText);
               }
               else
               {
                   result.AppendLine("<Dynamic Text>");
               }
           }
           else
           {
               result.AppendLine(CustomText);
           }
           return result.ToString();
        }

        public override string GetXmlDisplay(NativeActivityContext context)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            if (DisplayText != null)
            {
                if (context != null)
                {
                    node.SetValue(CustomText);
                }
                else
                {
                    node.SetValue("<Dynamic Text>");
                }
            }
            else
            {
                node.SetValue(CustomText);
            }
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", ShouldMask ? true : false), ShouldMask ? 1 : 0));
            xmlResponseTree.Add(new XElement("IsNumeric", new XAttribute("IsNumeric", IsNumeric ? true : false), IsNumeric ? 1 : 0));
            xmlResponseTree.Add(new XElement("IsImage", new XAttribute("IsImage", IsImage ? true : false), IsImage ? 1 : 0));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            if (DisplayText != null)
            {
                if (context != null)
                {
                    node.SetValue(textToAdd +CustomText);
                }
                else
                {
                    node.SetValue(textToAdd + "<Dynamic Text>");
                }
            }
            else
            {
                node.SetValue(textToAdd + CustomText);
            }
            xmlResponseTree.Add(node);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", ShouldMask ? true : false), ShouldMask ? 1 : 0));
            xmlResponseTree.Add(new XElement("IsNumeric", new XAttribute("IsNumeric", IsNumeric ? true : false), IsNumeric ? 1 : 0));
            xmlResponseTree.Add(new XElement("IsImage", new XAttribute("IsImage", IsImage ? true : false), IsImage ? 1 : 0));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }      

        public CustomScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new CustomScreenActivity();
            //IsNumeric = false;
            //IsPhoneNumber = false;

        }

        static CustomScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(CustomScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
        protected virtual string ProcessResponse(string value)
        {
            if (this.IsMoney)
            {
                decimal val =0M;
                value =  Decimal.TryParse(value, out val)?val.ToString("N"):value; 
            }
            return value;
        }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            if (String.IsNullOrEmpty(this.CustomText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                _onCompleted = onCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            return base.Execute(context, onCompleted, out nextNode);
        }

        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.CustomText = (instance.Activity as CustomScreenActivity).ComputedDisplay;
            if (this.IsMoney)
            {
               // this.CustomText = Convert.ToDecimal(this.CustomText).ToString("N");
            }
            State nextNode;
            base.Execute(context, _onCompleted, out nextNode);
        }


        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            long enteredVal = 0;decimal amt =0M;
            bool validNumber = response == null ? false : Int64.TryParse(response, out enteredVal);
            bool validAmount = Decimal.TryParse(response,out amt);
            DateTime outVal = new DateTime();
            if (this.IsNotification && response != "00")
            {
                currentWorkflowInfo.DisplayMessage = "Enter 00 : main menu.\r\n\r\n" + this.GetDisplay(context);
                //currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Enter 00 : main menu.<br/>");
                // return;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new
                    BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                return;
            }
            if (this.IsNumeric&&!validNumber )
            {
                currentWorkflowInfo.DisplayMessage = "Invalid Response. Enter numbers only.\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid Response. Enter numbers only.<br/>");
               // return;

                 context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                 return;
            }
            if (this.IsRecharge && !MobileAccountSystem.CheckPhoneNumber(response))
            {
                if (response != "1")
                {
                    currentWorkflowInfo.DisplayMessage = "Invalid Response. Enter Phone 1 or Phone Numbers only.\r\n\r\n" + this.GetDisplay(context);
                    currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid Response. Enter Phone 1 or Phone Numbers only.<br/>");
                }
                //return;
                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                
            }
            if (this.IsPhoneNumber && !MobileAccountSystem.CheckPhoneNumber(response))
            {
                currentWorkflowInfo.DisplayMessage = "Invalid Response. Enter Phone Numbers  only.\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid Response. Enter Phone Numbers  only.<br/>");
                //return;
                 context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                 return;
            }
            if (this.IsMoney && !validAmount)
            {
                currentWorkflowInfo.DisplayMessage = "Invalid Response. Enter A valid Amount  only.\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid Response. Enter A valid Amount  only.<br/>");
                //return;
                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                return;
            }
            else if (this.IsMoney && validAmount)
            {
                response = amt.ToString("N");
            }
            if (this.IsDateTime && !DateTime.TryParse(response, out outVal))
            {
                if(!CheckSegmentedDate(response))
                {
                currentWorkflowInfo.DisplayMessage = "Invalid Response. Enter A valid Date   only.\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Invalid Response. Enter A valid Date  only.<br/>");
                //return;
                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
                return;
                }
            }
            Transition theTrans = this.Transitions.FirstOrDefault();
            if (theTrans != null)
            {
                this.Next = theTrans.To;
            }
            if (_OnCompleted != null)
            {
                _OnCompleted.Invoke(context, null);
            }
        }
        //public bool CheckPhoneNumber(string entry)
        //{
        //    string validEntries ="080;070;081;071";
        //    string[] checkEntries =  validEntries.Split(";".ToCharArray());
        //    bool toReturn = true;
        //    long digits = 0;
            
        //    foreach(string ss in checkEntries)
        //    {
        //        if (entry.StartsWith(ss))
        //        {
        //            toReturn = true;
        //            break;
        //        }

        //        toReturn = false;
                
        //    }
        //    if (entry.Length != 11)
        //    {
        //        toReturn = false;
        //    }
        //    if (!Int64.TryParse(entry,out  digits))
        //    {
        //        toReturn = false;
        //    }
        //    return toReturn;
        //}
        public bool CheckSegmentedDate(string entry)
        {
            bool toReturn = false;
            DateTime d = new DateTime();
          
            if (string.IsNullOrEmpty(entry))
            {
                toReturn =  false;
            }

            else if (entry.Length != 8)
            {
                toReturn = false;
            }
            else
            {
                string day = entry.Substring(0, 2);
                string month = entry.Substring(2, 2);
                string year = entry.Substring(4);
                string dateValue = string.Format("{0}-{1}-{2}", month, day, year);
                if (!DateTime.TryParse(dateValue, out d))
                {
                    toReturn = false;
                }
                else
                {
                    toReturn = true;
                }
            }
            return toReturn;
        }

       
    }

    public class CustomScreenActivity : SubActivity
    {
        public InArgument<string> DisplayText
        {
            set;
            get;
        }

        public string ComputedDisplay { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            base.Execute(context);
            if (DisplayText != null) ComputedDisplay = DisplayText.Get(context);
        }
    }

}
