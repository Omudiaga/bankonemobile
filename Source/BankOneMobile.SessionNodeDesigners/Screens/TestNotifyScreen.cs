﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    public class TestNotifyScreen<T> : NotifyScreen
    {
        private CompletionCallback _onCompleted;
        public TestCustomScreenActivity<T> TheTest { get; set; }
        public InArgument<T> TheObject
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as TestCustomScreenActivity<T>).TheObject;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as TestCustomScreenActivity<T>).TheObject = value;
                }
            }
        }
        
     

        public override string GetDisplay(NativeActivityContext context)
        {
            string val;
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(NotificationText);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                }
            }
            else
            {
                result.AppendLine(NotificationText);
            }
            return "This is now Working " + (this.InternalActivity as TestCustomScreenActivity<T>).ObjectName;
        }

        public override string GetXmlDisplay(NativeActivityContext context)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement node = new XElement("Display");
            //if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            //{
            //    if (context != null)
            //    {
            //        node.SetValue(NotificationText);
            //    }
            //    else
            //    {
            //        node.SetValue("<Dynamic Text>");
            //    }
            //}
            //else
            //{
            //    node.SetValue(NotificationText);
            //}
            node.SetValue("This is now Working " + (this.InternalActivity as TestCustomScreenActivity<T>).ObjectName);
            xmlResponseTree.Add(node);
            //xmlResponseTree.Add(new XElement("Password", new XAttribute("Mask", false), 0));
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public TestNotifyScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.TheTest = new TestCustomScreenActivity<T>();
            this.InternalActivity = new TestCustomScreenActivity<T>();
        }

        static TestNotifyScreen()
        {
            
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(NotifyScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            if (String.IsNullOrEmpty(this.NotificationText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                _onCompleted = onCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;

            nextNode = null;
            return true;
        }

        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.NotificationText = (instance.Activity as CustomScreenActivity).ComputedDisplay;
            State nextNode;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;
            nextNode = null;
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            this.Next = null;

            if (_OnCompleted != null)
            {
                _OnCompleted.Invoke(context, null);
            }
        }
       
    }
    public class TestCustomScreenActivity<T> : CustomScreenActivity
    {

        public InArgument<T> TheObject
        {
            set;
            get;
        }
        public T Instance { get; set; }
        public string ObjectName { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            Instance = TheObject.Get(context)==null? (T)Activator.CreateInstance(typeof(T)):TheObject.Get(context);
            ObjectName = Instance.GetType().ToString(); 
            base.Execute(context);
            if (DisplayText != null) ComputedDisplay = DisplayText.Get(context);
        }
    }

   
}
