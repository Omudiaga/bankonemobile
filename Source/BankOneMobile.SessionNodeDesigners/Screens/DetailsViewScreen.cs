﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Reflection;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    public class DetailsViewScreen<T> : NotifyScreen
    {
        private CompletionCallback _onCompleted;
        public InArgument<string> DisplayText
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as DetailsViewScreenActivity<T>).DisplayText;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as CustomScreenActivity).DisplayText = value;
                }
            }
        }
        public InArgument<T> TheObject
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as DetailsViewScreenActivity<T>).TheObject;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as DetailsViewScreenActivity<T>).TheObject = value;
                }
            }
        }
        public string NotificationText { set; get; }

        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            {
                if (context != null)
                {
                    result.AppendLine(NotificationText);
                }
                else
                {
                    result.AppendLine("<Dynamic Text>");
                }
            }
            else
            {
                result.AppendLine(NotificationText);
            }
            T inst = (this.InternalActivity as DetailsViewScreenActivity<T>).Instance;
            var type = typeof(T);
            var properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                IList<CustomAttributeData> custData = property.GetCustomAttributesData();
                
                result.AppendFormat("{0} :{1} {2}", property.Name, property.GetValue(inst, null),System.Environment.NewLine);
            }
            return result.ToString();
        }

        public override string GetXmlDisplay(NativeActivityContext context)
        {
            XElement xmlResponseTree = new XElement("Response");
            XElement displayElement = new XElement("Display");
            //Before Menu
            if (String.IsNullOrEmpty(this.NotificationText) && DisplayText != null)
            {
                if (context != null)
                {
                    displayElement.Add("BeforeMenu", NotificationText);
                }
                else
                {
                    displayElement.Add("BeforeMenu", "<Dynamic Text>");
                }
            }
            else
            {
                displayElement.Add("BeforeMenu", NotificationText);
            }
            //Create Menu Element
            XElement menuElement = new XElement("Menu");
            T inst = (this.InternalActivity as DetailsViewScreenActivity<T>).Instance;
            var type = typeof(T);
            var properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                IList<CustomAttributeData> custData = property.GetCustomAttributesData();

                menuElement.Add("MenuItem", string.Format("{0} :{1}", property.Name, property.GetValue(inst, null)));
            }
            displayElement.Add(menuElement);
            xmlResponseTree.Add(displayElement);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
            
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public DetailsViewScreen()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new DetailsViewScreenActivity<T>();
        }

        static DetailsViewScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(DetailsViewScreenActivity<T>);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            if (String.IsNullOrEmpty(this.NotificationText))
            {
                currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
                _onCompleted = onCompleted;
                context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                nextNode = null;
                return false;
            }

            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;

            nextNode = null;
            return true;
        }

        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            this.NotificationText = (instance.Activity as CustomScreenActivity).ComputedDisplay;
            State nextNode;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.DisplayMessage = this.GetDisplay(context);
            currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context);
            currentWorkflowInfo.IsFinalScreen = true;
            nextNode = null;
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            this.Next = null;

            if (_OnCompleted != null)
            {
                _OnCompleted.Invoke(context, null);
            }
        }
    }

   
    public class DetailsViewScreenActivity<T> : CustomScreenActivity
    {
        public InArgument<T> TheObject
        {
            set;
            get;
        }
        public T Instance { get; set; }
        public string ComputedDisplay { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            Instance = TheObject.Get(context);
            base.Execute(context);
            if (DisplayText != null) ComputedDisplay = DisplayText.Get(context);
        }
    }

}
