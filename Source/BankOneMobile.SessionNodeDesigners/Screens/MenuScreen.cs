﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using System.Xml.Linq;

namespace BankOneMobile.SessionNodeDesigners
{
    
    public class MenuScreen : ScreenNode
    {
        
         public string BeforeMenuText { set; get; }
        
        public string AfterMenuText { set; get; }
        
        [Browsable(false)]
        public List<MenuItem> MenuItems { get; set; }


        public override string GetXmlDisplay(NativeActivityContext context)
        {
            //Create the XML Response Element  - Root node = Response
            XElement xmlResponseTree = new XElement("Response");
            XElement displayElement = new XElement("Display");
            //Before Menu
            if (!String.IsNullOrWhiteSpace(BeforeMenuText))
                displayElement.Add(new XElement("BeforeMenu", BeforeMenuText));
            //Create Menu Element
            XElement menuElement = new XElement("Menu");
            //Populate Menu Items
            foreach (MenuItem item in MenuItems.OrderBy(x => x.Index))
            {
                menuElement.Add(new XElement("MenuItem", new XAttribute("Index", item.Index), item.Text));
            }

            //if (TheFlow != null && TheFlow.InitialNode.StateId != this.StateId)
            //{
            //    menuElement.Add(new XElement("MenuItem", new XAttribute("Index", "00"), "Return to main menu"));
            //}
            //Add Menu element to Display
            displayElement.Add(menuElement);
            //After Menu
            if (!String.IsNullOrWhiteSpace(AfterMenuText))
                displayElement.Add(new XElement("AfterMenu", AfterMenuText));
            //Add Display to Response
            xmlResponseTree.Add(displayElement);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
            
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public override string GetXmlDisplay(NativeActivityContext context, string textToAdd)
        {
            //Create the XML Response Element  - Root node = Response
            XElement xmlResponseTree = new XElement("Response");
            XElement displayElement = new XElement("Display");
            //Before Menu
            if (!String.IsNullOrWhiteSpace(BeforeMenuText))
                displayElement.Add(new XElement("BeforeMenu", textToAdd + BeforeMenuText));
            //Create Menu Element
            XElement menuElement = new XElement("Menu");
            //Populate Menu Items
            foreach (MenuItem item in MenuItems.OrderBy(x => x.Index))
            {
                menuElement.Add(new XElement("MenuItem", new XAttribute("Index", item.Index), item.Text));
            }

            if (TheFlow != null && TheFlow.InitialNode.StateId != this.StateId)
            {
                menuElement.Add(new XElement("MenuItem", new XAttribute("Index", "00"), "Return to main menu"));
            }
            //Add Menu element to Display
            displayElement.Add(menuElement);
            //After Menu
            if (!String.IsNullOrWhiteSpace(AfterMenuText))
                displayElement.Add(new XElement("AfterMenu", AfterMenuText));
            //Add Display to Response
            xmlResponseTree.Add(displayElement);
            xmlResponseTree.Add(new XElement("ShouldMask", new XAttribute("Mask", false), 0));
            
            return xmlResponseTree.ToString(SaveOptions.DisableFormatting);
        }

        public override string GetDisplay(NativeActivityContext context)
        {
            StringBuilder result = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(BeforeMenuText)) result.AppendLine(BeforeMenuText);
            foreach (MenuItem item in MenuItems.OrderBy(x=>x.Index))
            {
                result.AppendFormat("{0}.{1}", item.Index, item.Text);
                result.AppendLine();
            }

            if (TheFlow != null && TheFlow.InitialNode.StateId != this.StateId)
            {
                result.AppendLine();
                result.AppendLine("00.Return to main menu");
            }

            if (!String.IsNullOrWhiteSpace(AfterMenuText)) result.AppendLine(AfterMenuText);
            return result.ToString();
        }

        public MenuScreen()
        {
            MenuItems = new List<MenuItem>();
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new SubActivity();
        }

        static MenuScreen()
        {
            AttributeTableBuilder builder = new AttributeTableBuilder();
            Type type = typeof(MenuScreen);
            builder.AddCustomAttributes(type, new Attribute[] { new DesignerAttribute(typeof(MenuScreenDesigner)) });            
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }

        protected override string ProcessResponse(string value)
        {
            string response = string.Empty;
            int selectedMenu = 0;
            if (value != null)
            {
                if (Int32.TryParse(value, out selectedMenu))
                {
                    if (this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu) != null)
                    {
                        response = this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu).Text;
                    }
                    else
                    {
                        response = value;
                    }
                }

            }
            return response;
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            int selectedMenu = 0;
            if (response != null)
            {
                Int32.TryParse(response, out selectedMenu);
            }
            if (selectedMenu == 0 || selectedMenu > this.MenuItems.Count)
            {
                currentWorkflowInfo.DisplayMessage = "Unknown number. Try again\r\n\r\n" + this.GetDisplay(context);
                //currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown number. Try again<br/>");
                //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                Transition theTrans = this.Transitions.SingleOrDefault(x => x.DisplayName.Trim() == selectedMenu.ToString());
                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }

    }

    public class MenuItem
    {
        public int Index { get; set; }
        public string Text { get; set; }
    }
}
