﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;

namespace BankOneMobile.ThirdPartyCustomActions
{
    //[Designer(typeof(SendSMSActionDesigner))]
    public class LotteryAction : ActionNode
    {
        
        /// <summary>
        /// Ticket Number
        /// </summary>
        public OutArgument<string> TicketNumber
        {
            get
            {
                LotteryActivity activity = this.InternalState as LotteryActivity;
                if (activity != null)
                {
                    return activity.TicketNumber;
                }
                return null;
            }
            set
            {
                LotteryActivity activity = this.InternalState as LotteryActivity;
                if (activity != null)
                {
                    activity.TicketNumber = value;
                }
            }
        }



        public LotteryAction()
        
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new LotteryActivity();
        }

    }

    public class LotteryActivity : CodeActivity
    {
       
       
        /// <summary>
        /// The Lottery Ticket Number
        /// </summary>
         public OutArgument<string> TicketNumber { get; set; }
       
        protected override void Execute(CodeActivityContext context)
        {
            string ticketNumber = new Random().Next(1000000, 9999999).ToString();
            TicketNumber.Set(context, ticketNumber);
           
        }
        
    }
}
