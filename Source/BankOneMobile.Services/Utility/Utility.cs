﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Services.Utility
{
    public static class Utility
    {
        public static void SendErrorMail(string institutionCode, string fromAddress, string toAddress, string title, string body)
        {
            using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
            {
                //string body = string.Format("Successfully created core banking and mobile accounts: {0}: Customer: {1}", mob.MobilePhone, tranType.PhoneNumber);
                client.SendEmail(institutionCode, fromAddress, toAddress, title, body);
                //PANE.ERRORLOG.ErrorLogger.Log(ex);
            }
        }
    }
}
