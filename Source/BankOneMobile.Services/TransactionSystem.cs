﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.SwitchIntegration;
using System.Xml.Linq;
using BankOneMobile.SwitchIntegration.ISO8583.Client.DTO;
using System.ServiceModel;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace BankOneMobile.Services
{
    public class TransactionSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ITransactionRepository _repository = SafeServiceLocator<ITransactionRepository>.GetService();
        public TransactionSystem()
        {
        }
        public TransactionSystem(IDataSource dataSorce, ITransactionRepository repository)
        {
            _theDataSource = dataSorce;
            _repository = repository;
        }
        public TransactionSystem(IDataSource dataSorce)
        {
            _theDataSource = dataSorce;

        }
        public TransactionSystem(ITransactionRepository repository)
        {

            _repository = repository;
        }
        public ITransaction SaveTransaction(Transaction theTransaction)
        {
            theTransaction.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theTransaction);
            _repository.DbContext.CommitTransaction(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public ITransaction UpdateTransaction(Transaction theTransaction)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theTransaction);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public ITransaction GetTransaction(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<ITransaction> GetAllTransactions()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<ITransaction> GetActiveTransactions()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<ITransaction> FindTransactions(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
        public static string RunTransaction(string pin)
        {
            //TODO Do the real Transaction here
            return string.Empty;
        }
        public static string RunTransaction(string pin, string accountNumber)
        {
            //TODO Do the real Transaction here
            return string.Format("{0}", 5000);
        }
        public TransactionResponse RunTransaction(string pin, MobileAccount mob, string from, string to, decimal amount, string sessionID)
        {
            BalanceInquiryTransactionType type = new BalanceInquiryTransactionType();
            return RunTransaction(pin, type, mob, from, to, amount, sessionID);
        }
        public TransactionResponse RunTransaction(string pin, ITransactionType transacType, MobileAccount mob, string from, string to, decimal amount, string sessionID)
        {
            Trace.TraceInformation("Inside Run");
            byte[] encryptedPinBytes = new byte[8];
            if (!string.IsNullOrEmpty(pin))
            {
                ThalesSim.Core.Utility.HexStringToByteArray(pin, ref encryptedPinBytes);
            }
            bool doISO = true;
            Trace.TraceInformation("Pin Done");
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionID);
            _theDataSource = currentWorkflowInfo.DataSource;
            MobileAccount initiatingAccount = null;
            IAgent agent = null;// new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            ISession session = new SessionSystem(currentWorkflowInfo.DataSource).GetBySessionID(sessionID);// Get the  ussd session that was used to orginate the request do we can save it with the transaction
            //if (session == null)
            //{
            //    throw new InvalidSessionIdException("Session ID is invalid");
            //}

            ISOResponse response = null;
            Transaction trans = new Transaction { Date = DateTime.Now, Status = TransactionStatus.Pending, EncryptedPIN = string.IsNullOrEmpty(pin) ? null : encryptedPinBytes, Session = session, From = from, To = to, FromPhoneNumber = mob.MobilePhone, TransactionTypeName = TransactionTypeName.BalanceEnquiry, TheFee = transacType.TheFee };

            if (session != null && session.TheTransactions == null)
            {
                session.TheTransactions = new List<ITransaction>();
                session.TheTransactions.Add(trans);
            }


            transacType.TheTransaction = trans;// create the transaction and tie the transaction and the transaction type to each other
            # region BalanceInquiry

            if (transacType is BalanceInquiryTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                BalanceInquiryTransactionType tranType = transacType as BalanceInquiryTransactionType;
                if (mob.IsAgentAccount && !tranType.IsForThirdParty)
                {
                    //Agent mt = new AgentSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAgentByPhoneNumber(mob.MobilePhone);
                    Agent mt = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (!tranType.IsForThirdParty && mt != null && mt.IsMobileTeller)
                    {
                        trans.From = string.Format("{0}.{1}", "MT", trans.From);
                    }
                }

                trans.TransactionTypeName = TransactionTypeName.BalanceEnquiry;
                trans.FromInstitutionCode = mob.InstitutionCode;
                trans.FromPhoneNumber = mob.MobilePhone;
                tranType.TheMobileAccount = mob;
                trans.Date = DateTime.Now;
                if (tranType.IsForThirdParty)
                {
                    trans.TheService = Service.MobileTellerCustomerBalanceEnquiryMS;
                }
                if (tranType.IsForSelfService)
                {
                    trans.TheService = Service.SelfServiceBalanceEnquiry;
                }
                //trans.TheBankAccount = transacType.
                IDataSource headBlockerDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared);
                new BalanceInquiryTransactionTypeSystem(headBlockerDataSource).SaveBalanceInquiryTransactionType(tranType);
                trans.TransactionTypeID = tranType.ID;
                //  session.PayerAccountNumber = from;
            }
            # endregion
            # region MiniStatement

            else if (transacType is MiniStatementTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                MiniStatementTransactionType tranType = transacType as MiniStatementTransactionType;
                if (mob.IsAgentAccount && !tranType.IsForThirdParty)
                {
                    //Agent mt = new AgentSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetAgentByPhoneNumber(mob.MobilePhone);
                    Agent mt = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (!tranType.IsForThirdParty && mt != null && mt.IsMobileTeller)
                    {
                        trans.From = string.Format("{0}.{1}", "MT", trans.From);
                    }

                }

                trans.TransactionTypeName = TransactionTypeName.MiniStatement;
                trans.FromPhoneNumber = mob.MobilePhone;
                trans.FromInstitutionCode = mob.InstitutionCode;
                tranType.TheMobileAccount = mob;
                trans.Date = DateTime.Now;
                MiniStatementTransactionTypeSystem.SaveMiniStatementTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.MiniStatement;
                trans.TransactionTypeID = tranType.ID;
                if (tranType.IsForThirdParty)
                {
                    trans.TheService = Service.MobileTellerCustomerBalanceEnquiryMS;
                }
                //  session.PayerAccountNumber = from;

            }
            # endregion
            # region PinChange

            else if (transacType is IPinChangeTransactionType)
            {
                trans.FromPhoneNumber = mob.MobilePhone;
                PinChangeTransactionType tranType = transacType as PinChangeTransactionType;
                tranType.TheMobileAccount = mob;
                trans.Date = DateTime.Now;
                PinChangeTransactionTypeSystem.SavePinChangeTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.PINChange;
                trans.TransactionTypeID = tranType.ID;
                byte[] holder = new byte[8];
                trans.FromInstitutionCode = mob.RecievingBankAccount.InstitutionCode;
                ThalesSim.Core.Utility.HexStringToByteArray(tranType.NewPIN, ref holder);
                tranType.EncryptedPin = new byte[48];
                tranType.EncryptedPin[0] = 1;
                holder.CopyTo(tranType.EncryptedPin, 1);
                //session.PayerAccountNumber = from;
            }
            # endregion
            # region SetDefaultAccount

            else if (transacType is ConfigurationTransactionType)
            {
                ConfigurationTransactionType tranType = transacType as ConfigurationTransactionType;
                trans.FromPhoneNumber = mob.MobilePhone;
                tranType.TheMobileAccount = mob;
                tranType.Type = "Set Default Account";
                trans.Date = DateTime.Now;
                trans.FromInstitutionCode = mob.RecievingBankAccount.InstitutionCode;
                ConfigurationTransactionTypeSystem.SaveConfigurationTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.SetDefaultAccount;
                trans.TransactionTypeID = tranType.ID;
                byte[] holder = new byte[8];
                // session.PayerAccountNumber = from;




            }
            # endregion
            # region Registration

            else if (transacType is RegistrationTransactionType)
            {
                Trace.TraceError("Registration-{0} {1}", mob.InstitutionCode, mob.MobilePhone);
                response = new ISOResponse();
                RegistrationTransactionType tranType = transacType as RegistrationTransactionType;
                tranType.TheTransaction = trans;
                tranType.TheMobileAccount = mob;
                try
                {
                    trans.FromPhoneNumber = mob.MobilePhone;
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                        agent = client.GetAgentByPhoneNumber(mob.MobilePhone);
                    }
                    // agent =  new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }


                    // session.PayerAccountNumber = agent.TheAgentAccountNumber;
                    MobileAccount registredAccount = null;
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                        if (tranType.IsEnhanced)
                        {
                            registredAccount = client.CreateEnhancedMobileAccountFromAgent(tranType.LastName,
                                tranType.FirstName, tranType.PhoneNumber, tranType.Gender, tranType.TheProduct as Product,
                                agent.Code, tranType.StarterPackNumber, tranType.IDNumber, tranType.NOKPhone, tranType.NOKName,
                                tranType.Address, tranType.PlaceOfBirth, tranType.ReferralName, tranType.ReferralPhoneNumber,
                                tranType.DateOfBirth, tranType.HasSufficientInfo, tranType.AccountSource.ToString(),
                                tranType.OtherAccountInfo, tranType.Passport);
                        }
                        else
                        {
                        }

                    }
                    // new MobileAccountSystem(_theDataSource).;

                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                    response.ResponseDescription = registredAccount.ReceivingAccountNumber;
                    response.ResponseMessage = registredAccount.ActivationCode;
                    response.Status = TransactionStatus.Successful;

                    if (agent.InstitutionCode == "100127")
                    {
                        Trace.TraceInformation("Save Image Starting No-{0}, Code-{1}", agent.PhoneNumber, agent.InstitutionCode);
                        bool done = new BankoneMobile.AdvansLafayette.Services.ServiceInterfaceSystem().SaveImages(response.ResponseDescription, registredAccount.CustomerID, tranType.Passport);
                        Trace.TraceInformation("Save Image Was Attempted. Status= {0}, Account Number = {1} Customer ID = {2} Passport = {3}", done, response.ResponseDescription, registredAccount.CustomerID, tranType.Passport == null ? "No Image" : tranType.Passport.Count().ToString());
                    }

                    else
                    {
                        Trace.TraceInformation("Save Image By Passed No-{0},Code- {1}", agent.PhoneNumber, agent.InstitutionCode);
                    }
                    //using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    //{
                    //    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    //    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    //    {
                    //        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    //    }
                    //    string body = string.Format("Successfully created core banking and mobile accounts: {0}: Customer: {1}", mob.MobilePhone, tranType.PhoneNumber);
                    //    client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                    //        emails, string.Format("BankOneMobile:{0}", " Successful"), body);
                    //    //PANE.ERRORLOG.ErrorLogger.Log(ex);
                    //}
                }

                catch (AlreadyExistingAccountException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "Accounts already exist.Please try again";
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (AlreadyRegisterdCustomerException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Transaction not permitted to non-agents Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (NoHSMResponseException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("HSM Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyRegisterdCustomerException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyRegisterdCustomerException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyRegisterdCustomerException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "Accounts already exist. Please try again";
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyExistingAccountException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyExistingAccountException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyExistingAccountException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("NoHSMResponseException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("NoHSMResponseException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new NoHSMResponseException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("CoreBankingWebServiceException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("CoreBankingWebServiceException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new CoreBankingWebServiceException(fex.Detail.Message);
                }
                catch (FaultException fex)
                {
                    response.ResponseDescription = fex.Message;
                    response.ResponseMessage = fex.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("FaultException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("FaultException Error from web service{0}\n{1}\nPhone: {2}", fex.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Message), body);
                    }

                    throw;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, tranType.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                    }

                    throw;

                }
                tranType.Date = DateTime.Now;
                new RegistrationTransactionTypeSystem(currentWorkflowInfo.DataSource).SaveRegistrationTransactionType(tranType);
                trans.TransactionTypeName = TransactionTypeName.Registration;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }

            # endregion
            # region Target Savings Registration

            else if (transacType is TargetSavingsRegistrationTransactionType)
            {
                Trace.TraceError("Target Savings Registration-{0} {1}", mob.InstitutionCode, mob.MobilePhone);
                response = new ISOResponse();
                TargetSavingsRegistrationTransactionType tranType = transacType as TargetSavingsRegistrationTransactionType;
                tranType.TheTransaction = trans;
                tranType.TheMobileAccount = mob;
                try
                {
                    string message = string.Empty;
                    trans.FromPhoneNumber = mob.MobilePhone;
                    Trace.TraceError("about to get agent by phonenumber-{0}", mob.MobilePhone);
                    agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone) as Agent;
                    Trace.TraceError("gotten agent. Code:{0}, isActive:{1}", agent.Code, agent.IsActive.ToString());

                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }

                    //ServiceReference1.CommitmentSavingsAccountRequest req = new ServiceReference1.CommitmentSavingsAccountRequest();

                    Trace.TraceInformation("About to create target savings");
                    Trace.TraceInformation("Amount:{0}-StandingOrder:{1}-InsCode:{2}-BetaAcc:{3}-Phone:{4}-ProdCode:{5}-TotalCommAmt{6}-Freq{7}-Tenure{8}", tranType.Amount, tranType.StandingOrder, tranType.TheMobileAccount.InstitutionCode == null ? "null" : tranType.TheMobileAccount.InstitutionCode,
                        tranType.AccountNumber == null ? "null" : tranType.AccountNumber, tranType.PhoneNumber, System.Configuration.ConfigurationManager.AppSettings["TargetSavingsProductCode"] == null ? "null" : System.Configuration.ConfigurationManager.AppSettings["TargetSavingsProductCode"].ToString(),
                        tranType.TotalCommitmentAmount, tranType.Frequency, tranType.Tenure);
                    SwitchServiceRef.CommitmentSavingsAccountRequest accountRequest = new SwitchServiceRef.CommitmentSavingsAccountRequest();
                    accountRequest.AmountToDepositPeriodically = decimal.Parse(tranType.Amount);
                    accountRequest.BookStandingOrder = tranType.StandingOrder == "Yes" ? true : false;
                    accountRequest.InitialDepositAmount = 0;
                    accountRequest.InstitutionCode = tranType.TheMobileAccount.InstitutionCode;
                    accountRequest.LiquidationAccountNumber = tranType.AccountNumber;
                    accountRequest.PhoneNo = tranType.PhoneNumber;
                    accountRequest.Tenure = tranType.TargetSavingsProductTenure;
                    accountRequest.ProductCode = tranType.SelectedProduct.Code;//System.Configuration.ConfigurationManager.AppSettings["TargetSavingsProductCode"];
                    accountRequest.TotalAmountOfCommitment = tranType.TotalCommitmentAmount;
                    accountRequest.IsReapplying = tranType.IsReapplying;
                    accountRequest.CommitmentSavingsAccountNumber = tranType.TargetSavingsAccountNumberForReactivation;
                    accountRequest.RollOverOnMaturation = true;
                    switch (tranType.Frequency)
                    {
                        case "Daily":
                            accountRequest.PaymentFrequency = BankOneMobile.Services.SwitchServiceRef.PaymentFrequency.Daily; break;
                        case "Weekly":
                            accountRequest.PaymentFrequency = BankOneMobile.Services.SwitchServiceRef.PaymentFrequency.Weekly; break;
                        case "Monthly":
                            accountRequest.PaymentFrequency = BankOneMobile.Services.SwitchServiceRef.PaymentFrequency.Monthly; break;
                    }
                    Trace.TraceInformation("finished loading trantype. About to call client.CreateCommitmentSavingsAccount method");
                    SwitchServiceRef.Account account = null;

                    using (SwitchServiceRef.SwitchingServiceClient client = new SwitchServiceRef.SwitchingServiceClient())
                    {
                        try
                        {
                            account = client.CreateCommitmentSavingsAccount(out message, accountRequest);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    Trace.TraceInformation("finished calling client.CreateCommitmentSavingsAccount method: " + message);
                    Trace.TraceInformation("Created Account is: " + account == null ? "null" : "not null");
                    if (account == null)
                    {
                        throw new Exception(message == string.Empty ? "An error occurred. Please try again." : message);
                    }
                    // new MobileAccountSystem(_theDataSource).;

                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                    //response.ResponseDescription = registredAccount.ReceivingAccountNumber;
                    //response.ResponseMessage = registredAccount.ActivationCode;
                    Trace.TraceInformation(string.Format("Commitment account created successfully. Nuban: {0} - Std: {1}", account.NUBAN, account.Number));
                    response.ResponseMessage = account.Number;
                    response.Status = TransactionStatus.Successful;


                    //using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    //{
                    //    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    //    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    //    {
                    //        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    //    }
                    //    string body = string.Format("Successfully created core banking and mobile accounts: {0}: Customer: {1}", mob.MobilePhone, tranType.PhoneNumber);
                    //    client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                    //        emails, string.Format("BankOneMobile:{0}", " Successful"), body);
                    //    //PANE.ERRORLOG.ErrorLogger.Log(ex);
                    //}
                }

                catch (AlreadyExistingAccountException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "Accounts already exist.Please try again";
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (AlreadyRegisterdCustomerException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Transaction not permitted to non-agents Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (NoHSMResponseException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("HSM Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyRegisterdCustomerException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyRegisterdCustomerException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyRegisterdCustomerException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "Accounts already exist. Please try again";
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyExistingAccountException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyExistingAccountException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyExistingAccountException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("NoHSMResponseException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("NoHSMResponseException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new NoHSMResponseException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("CoreBankingWebServiceException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("CoreBankingWebServiceException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new CoreBankingWebServiceException(fex.Detail.Message);
                }
                catch (FaultException fex)
                {
                    response.ResponseDescription = fex.Message;
                    response.ResponseMessage = fex.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("FaultException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("FaultException Error from web service{0}\n{1}\nPhone: {2}", fex.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Message), body);
                    }

                    throw;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, tranType.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                    }

                    throw;

                }
                tranType.Date = DateTime.Now;
                new TargetSavingsRegistrationTransactionTypeSystem(currentWorkflowInfo.DataSource).SaveRegistrationTransactionType(tranType);
                trans.TransactionTypeName = TransactionTypeName.Registration;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }

            # endregion
            # region Target Savings Deposit
            Trace.TraceInformation("Start Target Savings Deposit");
            if (transacType is TargetSavingsDepositTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                string tempAcct = trans.To;
                string tempInstCode = string.Empty;

                if (currentWorkflowInfo == null)//|| !currentWorkflowInfo.HasPreviousWorkflow)
                {
                    throw new BankOneMobile.Core.Exceptions.NonAgentInitiatedTransaction("Transaction was not initated by an agent");//This transaction was not initiated by any agent");
                }

                initiatingAccount = currentWorkflowInfo.InitiatedBy as MobileAccount;
                //string phoneNumber = mob.MobilePhone;
                Trace.TraceInformation("about to get agent");
                agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(initiatingAccount.MobilePhone);
                Trace.TraceInformation("gotten agent");


                if (agent == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                }
                if (!agent.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                }
                Trace.TraceInformation("about to load trantype");
                TargetSavingsDepositTransactionType tranType = transacType as TargetSavingsDepositTransactionType;
                trans.FromPhoneNumber = mob.MobilePhone;
                trans.TransactionTypeName = TransactionTypeName.CashIn;
                tranType.AgentCode = agent.Code;//=// set Agent Code
                //trans.From = trans.To;
                trans.To = tranType.TargetSavingsAccountNumber;
                trans.ToInstitutionCode = agent.MobileAccount.InstitutionCode;
                trans.From = tranType.BetaAccountNumber;
                //trans.TheBankAccount = tranType.TheBankAccount;
                trans.Amount = amount;
                //trans.ToInstitutionCode = tranType.Token;
                //trans.FromInstitutionCode = agent.InstitutionCode;
                trans.Date = DateTime.Now;
                Trace.TraceInformation("finished loading trantype");

                TargetSavingsDepositTransactionTypeSystem.SaveCashInTransactionType(currentWorkflowInfo.DataSource, tranType);
                Trace.TraceInformation("finished saving trantype");
                //trans.TransactionTypeID = tranType.ID;
                // trans.Narration = string.Format("Cash In to Agent ({0}) from {1} to {2}",agent.Code,agent.PhoneNumber, mob.MobilePhone);
                Trace.TraceInformation("About to create narration. corebankingnames");

                string credit = String.Format("Trf from {0} to {1} @ {2} ({3})", tranType.BetaAccountNumber, tranType.TargetSavingsAccountNumber, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                string debit = String.Format("Trf from {0} to {1}", tranType.BetaAccountNumber, tranType.TargetSavingsAccountNumber);//, tranType.TheBankAccount.CoreBankingNames, tranType.TheBankAccount.TheMobileAccount.MobilePhone);
                trans.Narration = String.Format("{0}||{1}", debit, credit);
                trans.ServiceCodes = ServiceCodes.NormalPayment;
                Trace.TraceInformation("Done creating narration");

                if (agent.IsMobileTeller)
                {
                    trans.TheMerchantType = MerchantType.MobileTellerCashIn;
                    trans.TheService = Service.MobileTellerCashIn;
                }
                else
                {
                    trans.TheService = Service.AgentCashIn;
                }
                string message = string.Empty;
                bool success = false;

                Trace.TraceInformation("About to call funds transfer service. insCode:{0}-betaAcc{1}-TargetAcc{2}-amount{3}-refCode{4}-serviceCode{5}-narration{6}",
                    agent.MobileAccount.InstitutionCode == null ? "null" : agent.MobileAccount.InstitutionCode, tranType.BetaAccountNumber,
                    tranType.TargetSavingsAccountNumber, tranType.Amount, trans.ToInstitutionCode == null ? "null" : trans.InstitutionCode,
                    trans.ServiceCodes == null ? "null" : trans.ServiceCodes.ToString(), trans.Narration == null ? "null" : trans.Narration);

                FundsTransferServiceRef.Response rp = new FundsTransferServiceRef.Response();
                using (FundsTransferServiceRef.FundsTransferServiceClient client = new FundsTransferServiceRef.FundsTransferServiceClient())
                {
                    rp = client.TransferFund(agent.MobileAccount.InstitutionCode, tranType.BetaAccountNumber, tranType.TargetSavingsAccountNumber, tranType.Amount, agent.MobileAccount.InstitutionCode, System.Configuration.ConfigurationManager.AppSettings["ServiceAndChannelAccessCode"], trans.Narration);
                }
                Trace.TraceInformation("finished calling service with message: success = " + rp.IsSuccessful);
                if (!rp.IsSuccessful)
                {
                    throw new Exception(rp.ResponseMessage);
                }
                //response.ResponseDescription = success == true ? "Success" : "Fail";
                response = new ISOResponse();
                response.ResponseMessage = "00";
                response.Status = TransactionStatus.Successful;
                trans.Status = TransactionStatus.Successful;
                trans.StatusDetails = "Transaction Successful";
                doISO = false;
                //trans.ServiceCodes=Iso
                //trans.ServiceCodes = ISOServiceCodes.
                // session.PayerAccountNumber = agent.TheAgentAccount.BankAccount;
            }
            Trace.TraceInformation("end of region");
            # endregion
            # region Recharge

            if (transacType is RechargeTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                RechargeTransactionType tranType = transacType as RechargeTransactionType;
                // tranType.TheBankAccount = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(trans.From,trans.FromInstitutionCode);
                if (tranType.TheBankAccount.BankStatus != BankAccountStatus.Active)
                {
                    throw new OnlyDepositsAllowsException("Only Deposits Are Allowed On this Account");
                }


                trans.Amount = amount;
                trans.TransactionTypeName = TransactionTypeName.Recharge;
                trans.From = tranType.TheBankAccount.BankAccount;
                trans.FromPhoneNumber = mob.MobilePhone;
                trans.FromInstitutionCode = tranType.TheBankAccount.InstitutionCode;
                trans.Date = DateTime.Now;
                //  trans.ToInstitutionCode = new SystemConfigSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetDefaulParentInstitutionCode();
                tranType.TheMobileAccount = mob;
                RechargeTransactionTypeSystem.SaveRechargeTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeID = tranType.ID;
                trans.Narration = string.Format("Mobile Airtime Purchase For {0}", tranType.Beneficiary);
                trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;
                trans.TheMerchantType = MerchantType.Recharge;
                // session.PayerAccountNumber = from;

            }
            # endregion
            # region Bills Payment

            if (transacType is BillsPaymentTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                BillsPaymentTransactionType tranType = transacType as BillsPaymentTransactionType;
                //tranType.TheBankAccount = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(trans.From,trans.FromInstitutionCode);
                if (tranType.TheBankAccount.BankStatus != BankAccountStatus.Active)
                {
                    throw new OnlyDepositsAllowsException("Only Deposits Are Allowed On this Account");
                }


                trans.Amount = amount;
                trans.TransactionTypeName = TransactionTypeName.BillsPayment;
                trans.FromPhoneNumber = mob.MobilePhone;
                trans.FromInstitutionCode = tranType.TheBankAccount.InstitutionCode;
                trans.From = from;
                trans.ToInstitutionCode = trans.FromInstitutionCode; //new SystemConfigSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetDefaulParentInstitutionCode();
                tranType.TheMobileAccount = mob;
                trans.Date = DateTime.Now;
                BillsPaymentTransactionTypeSystem.SaveBillsPaymentTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeID = tranType.ID;
                trans.Narration = string.Format("Payment for {0} to {1}", tranType.PaymentItemName, tranType.MerchantName);
                trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;
                trans.TheMerchantType = MerchantType.BillsPayment;
                // session.PayerAccountNumber = from;
            }
            # endregion
            # region Commercial Bank Funds Transfer

            if (transacType is CommercialBankFundsTransferTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Inside Commercial Bank Type"));
                CommercialBankFundsTransferTransactionType tranType = transacType as CommercialBankFundsTransferTransactionType;
                trans.FromInstitutionCode = mob.InstitutionCode;
                //tranType.TheBankAccount = new MobileAccountSystem().GetBankAccountByAcccountNo(trans.From,trans.FromInstitutionCode);
                if (tranType.TheBankAccount.BankStatus != BankAccountStatus.Active)
                {
                    throw new OnlyDepositsAllowsException("Only Deposits Are Allowed On this Account");
                }

                trans.Amount = amount;
                trans.TransactionTypeName = TransactionTypeName.FundsTransferCommercialBank;
                trans.FromPhoneNumber = mob.MobilePhone;

                trans.From = from;

                trans.ToInstitutionCode = new SystemConfigSystem(currentWorkflowInfo.DataSource).GetDefaulParentInstitutionCode();

                tranType.TheMobileAccount = mob;
                trans.Date = DateTime.Now;
                trans.Narration = string.Format("Funds  Transfer to {0},{1}", tranType.BeneficiaryLastName, tranType.BeneficiaryOtherNames);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to Save"));
                CommercialBankFundsTransferTransactionTypeSystem.SaveCommercialBankFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeID = tranType.ID;
                trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;
                trans.TheMerchantType = MerchantType.CustomerCommercialBankTransfer;
                //trans.TheMerchantType=ISOServiceCodes.
                //session.PayerAccountNumber = from;

            }
            # endregion
            # region CashIn
            Trace.TraceInformation("Start Cash In");
            if (transacType is CashInTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                string tempAcct = trans.To;
                string tempInstCode = string.Empty;

                if (currentWorkflowInfo == null)//|| !currentWorkflowInfo.HasPreviousWorkflow)
                {
                    throw new BankOneMobile.Core.Exceptions.NonAgentInitiatedTransaction("Transaction was not initated by an agent");//This transaction was not initiated by any agent");
                }

                initiatingAccount = currentWorkflowInfo.InitiatedBy as MobileAccount;
                //string phoneNumber = mob.MobilePhone;

                agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(initiatingAccount.MobilePhone);


                /// check if the agent code entered by the customer is correct
                if (agent.Code != (transacType as CashInTransactionType).AgentCode)
                {
                    throw new BankOneMobile.Core.Exceptions.InvalidAgentCodeException("Invalid Agent Code");
                }

                if (agent == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                }
                if (!agent.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                }

                if (mob.MobilePhone == initiatingAccount.MobilePhone)
                {
                    //trans.NotValidatePin = true;
                }

                CashInTransactionType tranType = transacType as CashInTransactionType;
                trans.FromPhoneNumber = mob.MobilePhone;
                trans.TransactionTypeName = TransactionTypeName.CashIn;
                tranType.AgentCode = agent.Code;//=// set Agent Code
                trans.From = trans.To;
                trans.To = agent.IsMobileTeller ? string.Format("MT.{0}", from) : from;
                trans.ToInstitutionCode = agent.InstitutionCode;
                trans.From = tempAcct;
                trans.FromInstitutionCode = tranType.Token;
                //trans.TheBankAccount = tranType.TheBankAccount;
                trans.Amount = amount;
                //trans.ToInstitutionCode = tranType.Token;
                //trans.FromInstitutionCode = agent.InstitutionCode;
                trans.Date = DateTime.Now;

                CashInTransactionTypeSystem.SaveCashInTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeID = tranType.ID;
                // trans.Narration = string.Format("Cash In to Agent ({0}) from {1} to {2}",agent.Code,agent.PhoneNumber, mob.MobilePhone);

                string credit = agent.InstitutionCode == "5" ? String.Format("Cash Dep via BETA Friend/{0} {1}",agent.LastName,agent.OtherNames) : String.Format("Cash Deposit @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                string debit = agent.InstitutionCode == "5" ? String.Format("Cash Dep by BETA Friend/{0} {1}", agent.LastName, agent.OtherNames) : String.Format("Cash Deposit by {0}, ({1}) ", tranType.TheBankAccount.CoreBankingNames, tranType.TheBankAccount.TheMobileAccount.MobilePhone);
                trans.Narration = String.Format("{0}||{1}", debit, credit);
                trans.ServiceCodes = ServiceCodes.NormalPayment;

                if (agent.IsMobileTeller)
                {
                    trans.TheMerchantType = MerchantType.MobileTellerCashIn;
                    trans.TheService = Service.MobileTellerCashIn;
                }
                else
                {
                    trans.TheService = Service.AgentCashIn;
                }

                //trans.ServiceCodes=Iso
                //trans.ServiceCodes = ISOServiceCodes.
                // session.PayerAccountNumber = agent.TheAgentAccount.BankAccount;


            }
            # endregion
            # region Cashout
            if (transacType is CashOutTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                if (mob.AccountRestriction == AccountRestriction.OnlyCashIn)
                {
                    throw new OnlyDepositsAllowsException("Only Deposits Are Allowed On this Account");
                }
                if (currentWorkflowInfo == null)//|| !currentWorkflowInfo.HasPreviousWorkflow)
                {
                    throw new BankOneMobile.Core.Exceptions.NonAgentInitiatedTransaction("This transaction was not inititated by an agent");//This transaction was not initiated by any agent");
                }

                initiatingAccount = currentWorkflowInfo.InitiatedBy as MobileAccount;
                //string phoneNumber = mob.MobilePhone;

                agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(initiatingAccount.MobilePhone);

                ///// check if the agent code entered by the customer is correct
                //if (agent.Code != (transacType as CashInTransactionType).AgentCode)
                //{
                //    throw new BankOneMobile.Core.Exceptions.InvalidAgentCodeException();
                //}

                if (agent == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                }
                if (!agent.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                }
                if (!DoPinVerficaction(agent.MobileAccount.MobilePhone, pin))
                {
                    throw new InvalidPinException("Invalid PIN");
                }

                CashOutTransactionType tranType = transacType as CashOutTransactionType;
                trans.TransactionTypeName = TransactionTypeName.CashOut;
                tranType.AgentCode = agent.Code;//=// set Agent Code
                trans.To = agent.IsMobileTeller ? string.Format("MT.{0}", agent.TheAgentAccount.BankAccount) : agent.TheAgentAccount.BankAccount;
                trans.From = agent.IsMobileTeller ? from : tranType.CustomerAccount;
                string frm = string.Format("From is {0}", trans.From);
                Trace.TraceInformation(frm);
                trans.Amount = amount;
                trans.ToInstitutionCode = tranType.Token;
                trans.FromInstitutionCode = agent.InstitutionCode;
                trans.Date = DateTime.Now;
                trans.FromPhoneNumber = tranType.CustomerPhoneNumber;
                // trans.Narration = string.Format("Cash Withdrawal from Agent ({0}) from  {1} to  {2}",agent.Code,trans.From,trans.To);
                string debit = agent.InstitutionCode == "100040" ? String.Format("Cash withdrawal from BETA Friend/{0} {1}", agent.LastName, agent.OtherNames) : String.Format("Cash Withdrawal From Agent ({0})", agent.Code);
                string credit = agent.InstitutionCode == "100040" ? String.Format("Cash withdrawal by BETA Friend/{0} {1}", agent.LastName, agent.OtherNames) : String.Format("Cash Out by {0}, {1} ({2}) ", mob.LastName, mob.OtherNames, mob.MobilePhone);
                trans.Narration = String.Format("{0}||{1}", debit, credit);
                CashOutTransactionTypeSystem.SaveCashOutTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeID = tranType.ID;
                trans.ServiceCodes = ServiceCodes.NormalPayment;
                if (agent.IsMobileTeller)
                {
                    trans.TheMerchantType = MerchantType.MobileTellerCashOut;
                    trans.TheService = Service.MobileTellerCashOut;
                }
                else
                {
                    trans.TheService = Service.AgentCashOut;
                }
                // session.PayerAccountNumber = agent.TheAgentAccount.BankAccount; 
                ;

            }
            # endregion
            # region Funds Transfer Cash In
            FundsTransferCashInTransactionType ftCashIn = transacType as FundsTransferCashInTransactionType;
            if (transacType is FundsTransferCashInTransactionType)
            {
                if (!(transacType as FundsTransferCashInTransactionType).FromSelfService)
                {
                    if (currentWorkflowInfo == null)//|| !currentWorkflowInfo.HasPreviousWorkflow)
                    {
                        throw new BankOneMobile.Core.Exceptions.NonAgentInitiatedTransaction("Transaction was not initated by an agent");//This transaction was not initiated by any agent");
                    }

                    initiatingAccount = currentWorkflowInfo.InitiatedBy as MobileAccount;
                    //string phoneNumber = mob.MobilePhone;

                    agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(initiatingAccount.MobilePhone);

                    /// check if the agent code entered by the customer is correct
                    if (agent.Code != ftCashIn.AgentCode)
                    {
                        throw new BankOneMobile.Core.Exceptions.InvalidAgentCodeException("Invalid Agent Code");
                    }

                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }

                }
                if (ftCashIn.IsCash)// if its cash
                {
                    trans.From = agent.TheAgentAccount.BankAccount;// the agent has collected cash and should be debited
                    trans.FromInstitutionCode = agent.InstitutionCode;
                    trans.FromPhoneNumber = mob.MobilePhone;

                }
                else
                {
                    trans.From = from;
                    trans.FromInstitutionCode = mob.InstitutionCode; //new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(ftCashIn.DepositorPhoneNumber).InstitutionCode;
                    trans.FromPhoneNumber = mob.MobilePhone;
                }
                trans.TransactionTypeName = TransactionTypeName.FundsTransferCashIn;


                trans.Amount = amount;
                //trans.ToInstitutionCode = ftCashIn.Token;
                // trans.FromInstitutionCode = agent.InstitutionCode;
                trans.Narration = string.Format("Funds Transfer From {0} to {1}", trans.From, (transacType as FundsTransferCashInTransactionType).BeneficiaryPhoneNumber);
                new FundsTransferCashInTransactionTypeSystem().SaveFundsTransferCashInTransactionType(currentWorkflowInfo.DataSource, ftCashIn);
                trans.Date = DateTime.Now;
                trans.TransactionTypeID = ftCashIn.ID;
                trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;
                if (agent.IsMobileTeller)
                {
                    trans.TheMerchantType = MerchantType.MobileTellerCashIn;
                }
                else
                {
                    trans.TheMerchantType = MerchantType.NullMerchantType;
                }
                //session.PayerAccountNumber = trans.From;
            }
            # endregion
            # region Funds Transfer Cash Out
            if (transacType is FundsTransferCashOutTransactionType)
            {
                trans.FromPhoneNumber = mob.MobilePhone;
                if (currentWorkflowInfo == null)// || !currentWorkflowInfo.HasPreviousWorkflow)
                {
                    //  throw new BankOneMobile.Core.Exceptions.NonAgentInitiatedTransaction("Transaction was not initated by an agent");//This transaction was not initiated by any agent");
                    agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgent(currentWorkflowInfo.InitiatedBy.RegistrarID);
                }


                //string phoneNumber = mob.MobilePhone;



                if (agent == null)
                {
                    initiatingAccount = currentWorkflowInfo.InitiatedBy as MobileAccount;
                    agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(initiatingAccount.MobilePhone);
                }
                FundsTransferCashOutTransactionType ftCashOut = transacType as FundsTransferCashOutTransactionType;
                /// check if the agent code entered by the customer is correct


                if (agent == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                }
                if (!agent.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                }



                trans.Amount = (transacType as FundsTransferCashOutTransactionType).Amount * 100;// d
                trans.To = agent.TheAgentAccount.BankAccount;// the agent has given cash and should be credited  from the suspense
                trans.ToInstitutionCode = agent.InstitutionCode;
                trans.From = mob.BankAccounts[0].BankAccount;
                trans.FromInstitutionCode = mob.InstitutionCode;


                trans.TransactionTypeName = TransactionTypeName.FundsTransferCashOut;

                trans.Date = DateTime.Now;
                IList<IFundsTransferCashInTransactionType> cashIns = (transacType as FundsTransferCashOutTransactionType).TheCashIns;
                StringBuilder builder = new StringBuilder();
                foreach (IFundsTransferCashInTransactionType cs in cashIns)
                {
                    builder.AppendFormat("{0},{1}", cs.DepositorName, ",");

                }

                trans.Narration = string.Format("Funds Transfer from {0} to {1}", builder.ToString().TrimEnd(",".ToCharArray()), trans.To);
                FundsTransferCashOutTransactionTypeSystem.UpdateFundsTransferCashOutTransactionType(currentWorkflowInfo.DataSource, ftCashOut);
                trans.ServiceCodes = ServiceCodes.FromServiceAccountPayment;
                trans.TheMerchantType = MerchantType.ServiceAccounTransferToCustomer;
                trans.TransactionTypeID = ftCashOut.ID;
                //  session.PayerAccountNumber = agent.TheAgentAccount.BankAccount;
            }
            # endregion
            # region LocaFundsTransfer
            if (transacType is LocalFundsTransferTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                if (mob.AccountRestriction == AccountRestriction.OnlyCashIn)
                {
                    throw new OnlyDepositsAllowsException("Only Deposits Are Allowed On this Account");
                }
                LocalFundsTransferTransactionType tranType = transacType as LocalFundsTransferTransactionType;
                MobileAccount toM = null;
                trans.TransactionTypeName = TransactionTypeName.LocalFundsTransfer;
                if (tranType.isPhoneNumber)
                {
                    toM = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetByPhoneNumber(to);

                    trans.To = toM.RecievingBankAccount == null ? toM.BankAccounts[0].BankAccount : toM.RecievingBankAccount.BankAccount;
                    tranType.ToInstCode = toM.RecievingBankAccount == null ? toM.BankAccounts[0].InstitutionCode : toM.RecievingBankAccount.InstitutionCode;
                    tranType.ToAccount = toM.MobilePhone;

                }
                else
                {
                    LinkingBankAccount iMob = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(to, mob.InstitutionCode);
                    toM = iMob == null ? null : iMob.TheMobileAccount as MobileAccount;

                    tranType.ToAccount = toM == null ? " " : toM.MobilePhone;
                    //tranType.ToInstCode = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetBankAccountByAcccountNo(to, mob.InstitutionCode).InstitutionCode;
                }

                trans.From = from;
                trans.Amount = amount;
                trans.FromInstitutionCode = mob.InstitutionCode;
                trans.FromPhoneNumber = mob.MobilePhone;

                trans.ToInstitutionCode = tranType.ToInstCode;
                trans.Date = DateTime.Now;
                // trans.Narration = string.Format("Funds Transfer From {0} to {1}",trans.From,trans.To);
                string credit = String.Format("Transfer From {0},{1} ({2})", mob.LastName, mob.OtherNames, mob.MobilePhone);
                string debit = String.Format("Transfer To {0}", tranType.ReceivingAccountName);
                trans.Narration = String.Format("{0}||{1}", debit, credit);
                try
                {
                    new LocalFundsTransferTransactionTypeSystem(currentWorkflowInfo.DataSource).SaveLocalFundsTransferTransactionType(tranType);
                }
                catch
                {
                }
                trans.TransactionTypeID = tranType.ID;
                trans.ServiceCodes = ServiceCodes.NormalPayment;
                trans.TheMerchantType = MerchantType.NullMerchantType;
                trans.TheService = Service.MobileLocalFundsTransfer;
                // session.PayerAccountNumber = from;

            }
            //_repository.DbContext.Close(currentWorkflowInfo.DataSource);
            # endregion
            # region DBN Loan Request

            if (transacType is LoanRequestTransactionType)
            {
                Trace.TraceError("Loan Request-{0} {1}", mob.InstitutionCode, mob.MobilePhone);
                response = new ISOResponse();
                LoanRequestTransactionType tranType = transacType as LoanRequestTransactionType;
                tranType.TheTransaction = trans;
                tranType.TheMobileAccount = mob;
                try
                {
                    trans.FromPhoneNumber = mob.MobilePhone;

                    //using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    //{
                    //    agent = client.GetAgentByPhoneNumber(mob.MobilePhone);
                    //}
                    agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }


                    // session.PayerAccountNumber = agent.TheAgentAccountNumber;

                    string loanRequestXML = "<LoanRequest>"
                                                + "<CustomerAccountNumber>" + tranType.AccountNumber + "</CustomerAccountNumber>"
                                                + "<PhoneNumber>" + tranType.PhoneNumber + "</PhoneNumber>"
                                                + "<Amount>" + tranType.LoanAmount + "</Amount>"
                                                + "<Tenure>" + tranType.Tenure + "</Tenure>"
                                                + "<LoanProductID>" + tranType.LoanProductID + "</LoanProductID>"
                                                + "<InstitutionCode>" + tranType.InstitutionCode + "</InstitutionCode>"
                                                + "<AgentPerceptionOfCustomer>" + tranType.BetaFriendPerception + "</AgentPerceptionOfCustomer>"
                                            + "</LoanRequest>";
                    // Create a request using a URL that can receive a post. 
                    WebRequest request = null;
                    if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsTestMode"]))
                        request = WebRequest.Create("http://localhost/CreditAssessment/services/LoanRequest.aspx");
                    else
                        request = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/LoanRequest.aspx");
                    //WebRequest request = WebRequest.Create("http://localhost/CreditAssessment/services/LoanRequest.aspx");
                    //WebRequest request = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/LoanRequest.aspx");
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    string postData = loanRequestXML;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    //request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;
                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    WebResponse webResponse = request.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream = webResponse.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();

                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                    dataStream.Close();
                    webResponse.Close();
                    //Console.Read();
                    if (string.IsNullOrWhiteSpace(responseFromServer))
                        throw new InvalidServerResponseForLoanRequestException("Error processing Loan Request");

                    XElement xml;
                    try
                    {
                        xml = XElement.Parse(responseFromServer);
                    }
                    catch
                    {
                        xml = XElement.Parse(responseFromServer.Replace("&nbsp;", string.Empty));
                    }
                    if (xml.Name.NamespaceName.Contains("http://schemas.microsoft.com/")) //"http://schemas.microsoft.com/2003/10/Serialization/"
                    {
                        xml = XElement.Parse(xml.Value);
                    }


                    if (xml.Name != "LoanRequestResponse")
                    {
                        throw new InvalidServerResponseForLoanRequestException("Error processing Loan Request");
                    }

                    XElement relevantXml = TransformXML(xml, new[] { "Status", "Error" });
                    var status = relevantXml.Element("Status").Value;
                    var error = relevantXml.Element("Error").Value;

                    if (status != "0")
                    {
                        throw new FailedLoanRequestException(error);
                    }
                    else
                    {
                        tranType.TheTransaction.Status = TransactionStatus.Successful;
                        response.Status = TransactionStatus.Successful;
                    }
                }

                catch (InvalidServerResponseForLoanRequestException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error processing Loan Request. The response from server was not in the right format");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }

                catch (FailedLoanRequestException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error processing Loan Request. The response from server returned an error");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }

                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Transaction not permitted to non-agents Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (NoHSMResponseException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("HSM Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }

                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("NoHSMResponseException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("NoHSMResponseException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new NoHSMResponseException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("CoreBankingWebServiceException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("CoreBankingWebServiceException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new CoreBankingWebServiceException(fex.Detail.Message);
                }
                catch (FaultException fex)
                {
                    response.ResponseDescription = fex.Message;
                    response.ResponseMessage = fex.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("FaultException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("FaultException Error from web service{0}\n{1}\nPhone: {2}", fex.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Message), body);
                    }

                    throw;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, tranType.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                    }

                    throw;

                }
                tranType.Date = DateTime.Now;
                new LoanRequestTransactionTypeSystem(currentWorkflowInfo.DataSource).SaveLoanRequestTransactionType(tranType);
                trans.TransactionTypeName = TransactionTypeName.Registration;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }

            # endregion
            #region DBN Funds Transfer
            if (transacType is DBNFundsTransferTransactionType)
            {
                Trace.TraceError("DBN Funds Transfer-{0} {1}", mob.InstitutionCode, mob.MobilePhone);
                response = new ISOResponse();
                DBNFundsTransferTransactionType tranType = transacType as DBNFundsTransferTransactionType;
                tranType.TheTransaction = trans;
                try
                {
                    trans.FromPhoneNumber = mob.MobilePhone;
                    agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }

                    tranType.AgentPhoneNumber = mob.MobilePhone;

                    if (tranType.DBNFundsTransferType == DBNFundsTransferTransactionTypeName.BetaToBeta)
                    {
                        Trace.TraceInformation("Type is BetaToBeta");
                        Random rnd = new Random();
                        string randomNumber = rnd.Next(0, 999999).ToString("000000");
                        string dt = DateTime.Now.ToString("yyMMddHHmmss");
                        string trxReference = "SLF" + dt + randomNumber;
                        tranType.PaymentReference = trxReference;
                        new DBNFundsTransferTransactionTypeSystem().SaveDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                        BankOneMobile.Services.FundsTransferServiceRef.Response rp = new BankOneMobile.Services.FundsTransferServiceRef.Response();
                        try
                        {
                            using (BankOneMobile.Services.FundsTransferServiceRef.FundsTransferServiceClient client = new BankOneMobile.Services.FundsTransferServiceRef.FundsTransferServiceClient())
                            {
                                rp = client.TransferFund(tranType.DestinationInstitutionCode, tranType.OriginatorAccountNumber,
                                    tranType.BeneficiaryAccountNumber, tranType.Amount, tranType.PaymentReference,
                                    System.Configuration.ConfigurationManager.AppSettings["ServiceAndChannelAccessCode"], tranType.Narration);
                            }
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(String.Format("Error trying to debit customer with ref {0}. {1}", tranType.PaymentReference, ex.Message));

                            #region send error mail
                            try
                            {
                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                {
                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                    string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                    client.SendEmail("100040", "donotreply@mybankone.com",
                                        emails, string.Format("BankOneMobile- SLF FT SwitchingService Error:{0}", ex.Message), body);
                                }
                            }
                            catch (Exception emailException)
                            {
                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                            }
                            #endregion

                            Trace.TraceError(String.Format("Now requerying transaction: {0}", tranType.PaymentReference));
                            try
                            {
                                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                {
                                    SwitchServiceRef.Response theResponse = client.RequeryTransactionStatus(mob.InstitutionCode, tranType.PaymentReference);
                                    rp.IsSuccessful = theResponse.IsSuccessful;
                                    rp.ResponseMessage = theResponse.ResponseMessage;
                                }
                                Trace.TraceError(String.Format("Done requerying transaction: {0}. status: {1}", tranType.PaymentReference, rp.IsSuccessful));
                            }
                            catch (Exception iex)
                            {
                                Trace.TraceError(String.Format("Web Service Error in Requery: {0}", iex.Message));
                                #region send error mail
                                try
                                {
                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                    {
                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                        client.SendEmail("100040", "donotreply@mybankone.com",
                                            emails, string.Format("BankOneMobile-SLF FT SwitchingService Trx Requery Error:{0}", ex.Message), body);
                                    }
                                }
                                catch (Exception emailException)
                                {
                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                }
                                #endregion
                            }
                        }
                        Trace.TraceInformation("finished calling service for dbn beta to beta transfer with message: success = " + rp.IsSuccessful);
                        if (!rp.IsSuccessful)
                        {
                            tranType.ResponseCode = "06";
                            tranType.ResponseDate = DateTime.Now;
                            tranType.TheTransaction.Status = TransactionStatus.Failed;
                            response.Status = TransactionStatus.Failed;
                            response.ResponseMessage = rp.ResponseMessage;
                            //throw new Exception(rp.ResponseMessage);
                        }
                        else
                        {
                            tranType.ResponseCode = "00";
                            tranType.ResponseDate = DateTime.Now;
                            tranType.TheTransaction.Status = TransactionStatus.Successful;
                            response.Status = TransactionStatus.Successful;
                        }
                    }
                    else if (tranType.DBNFundsTransferType == DBNFundsTransferTransactionTypeName.BetaToDiamond)
                    {
                        Trace.TraceInformation("Type is BetaToDiamond");
                        Random rnd = new Random();
                        string randomNumber = rnd.Next(0, 999999).ToString("000000");
                        string dt = DateTime.Now.ToString("yyMMddHHmmss");
                        string trxReference = "TDB" + dt + randomNumber;
                        tranType.PaymentReference = trxReference;
                        //1. We debit the customer's beta account
                        Trace.TraceInformation(string.Format("About to debit customer. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                        var rp = new BankOneMobile.Services.SwitchServiceRef.Response();
                        try
                        {
                            using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                            {
                                rp = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, tranType.PaymentReference, tranType.Narration, false, false);
                            }
                        }
                        catch(Exception ex)
                        {
                            Trace.TraceError(String.Format("Error trying to debit customer with ref {0}. {1}",tranType.PaymentReference, ex.Message));

                            #region send error mail
                            try
                            {
                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                {
                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                    string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                    client.SendEmail("100040", "donotreply@mybankone.com",
                                        emails, string.Format("BankOneMobile-TDB FT SwitchingService Error:{0}", ex.Message), body);
                                }
                            }
                            catch (Exception emailException)
                            {
                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                            }
                            #endregion

                            Trace.TraceError(String.Format("Now requerying transaction: {0}",tranType.PaymentReference));
                            try
                            {
                                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                {
                                    rp = client.RequeryTransactionStatus(mob.InstitutionCode, tranType.PaymentReference);
                                }
                                Trace.TraceError(String.Format("Done requerying transaction: {0}. status: {1}", tranType.PaymentReference,rp.IsSuccessful));
                            }
                            catch(Exception iex)
                            {
                                Trace.TraceError(String.Format("Web Service Error in Requery: {0}", iex.Message));
                                #region send error mail
                                try
                                {
                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                    {
                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                        client.SendEmail("100040", "donotreply@mybankone.com",
                                            emails, string.Format("BankOneMobile-TDB FT SwitchingService Trx Requery Error:{0}", ex.Message), body);
                                    }
                                }
                                catch (Exception emailException)
                                {
                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                }
                                #endregion
                            }
                        }
                        Trace.TraceInformation(string.Format("Done debitin customer. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber,rp.IsSuccessful));
                        if (rp.IsSuccessful)
                        {
                            tranType.TransferStatus = FundsTransferStatus.Pending;
                            new DBNFundsTransferTransactionTypeSystem().SaveDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                            var dbnResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                            //2. We try and transfer funds to Diamond bank
                            try
                            {
                                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                {
                                    dbnResponse = client.DiamondBankOneTransferFunds(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.BeneficiaryAccountNumber, tranType.BeneficiaryAccountName, tranType.Amount, tranType.PaymentReference, tranType.Narration);
                                }
                            }
                            catch(Exception ex)
                            {
                                Trace.TraceError(String.Format("Error trying to transfer funds to DBN. {0}",ex.Message));
                                #region send error mail
                                try
                                {
                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                    {
                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                        client.SendEmail("100040", "donotreply@mybankone.com",
                                            emails, string.Format("BankOneMobile-DBN FT WebService/SwitchingService Error:{0}", ex.Message), body);
                                    }
                                }
                                catch (Exception emailException)
                                {
                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                }
                                #endregion
                            }
                            Trace.TraceInformation(string.Format("finished calling diamond bank service with message: success = {0}. Message is {1} ", dbnResponse.IsSuccessful, dbnResponse.ResponseMessage));
                            if (!dbnResponse.IsSuccessful)
                            {
                                string requeryResponse = string.Empty;
                                try
                                {
                                    requeryResponse = RequeryDBNTransferTransaction(tranType.PaymentReference);
                                }
                                catch(Exception ex)
                                {
                                    Trace.TraceError(String.Format("DBN Transfer Requery Error: {0}",ex.Message));
                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                            client.SendEmail("100040", "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-DBN RT Transfer Requery Error:{0}", ex.Message), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                }
                                if (String.IsNullOrEmpty(requeryResponse))
                                {
                                    #region Reversal
                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of funds Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, "R" + tranType.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(String.Format("Switching Service FT Reversal Error: {0}", ex.Message));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-DBN/Switching Service FT Reversal Error:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                    Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                    tranType.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                    tranType.TransferStatus = FundsTransferStatus.Failed;
                                    tranType.ResponseCode = "06";
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Failed;
                                    response.Status = TransactionStatus.Failed;
                                    response.ResponseMessage = string.IsNullOrEmpty(rp.ResponseMessage) ? "Transfer not successful!" : rp.ResponseMessage;
                                    //throw new Exception(rp.ResponseMessage);
                                    #endregion
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "pending".ToLower())
                                {
                                    response.Status = TransactionStatus.Successful;
                                    tranType.TransferStatus = FundsTransferStatus.Pending;
                                    new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                                    TransactionResponse res = new TransactionResponse { ResponseDescription = response.ResponseDescription, ResponseMessage = response.ResponseMessage, Status = response.Status.ToString() };

                                    return res;
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "failed".ToLower())
                                {
                                    #region Reversal
                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of funds Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, "R" + tranType.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceError(String.Format("Switching Service FT Reversal Error: {0}",ex.Message));
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-DBN/Switching Service FT Reversal Error:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                    Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                    tranType.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                    tranType.TransferStatus = FundsTransferStatus.Failed;
                                    tranType.ResponseCode = "06";
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Failed;
                                    response.Status = TransactionStatus.Failed;
                                    response.ResponseMessage = string.IsNullOrEmpty(rp.ResponseMessage) ? "Transfer not successful!" : rp.ResponseMessage;
                                    //throw new Exception(rp.ResponseMessage);
                                    #endregion
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "success".ToLower())
                                {
                                    tranType.TransferStatus = FundsTransferStatus.Successful;
                                    tranType.ResponseCode = "00";
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                                    response.Status = TransactionStatus.Successful;
                                }
                            }
                            else
                            {
                                tranType.TransferStatus = FundsTransferStatus.Successful;
                                tranType.ResponseCode = "00";
                                tranType.ResponseDate = DateTime.Now;
                                tranType.TheTransaction.Status = TransactionStatus.Successful;
                                response.Status = TransactionStatus.Successful;
                            }
                        }
                        else
                        {
                            response.Status = TransactionStatus.Failed;
                            response.ResponseMessage = rp.ResponseMessage;
                        }
                    }

                    else if (tranType.DBNFundsTransferType == DBNFundsTransferTransactionTypeName.BetaToOtherBanks)
                    {
                        Trace.TraceInformation("Type is BetaToOtherBanks");
                        Random rnd = new Random();
                        string randomNumber = rnd.Next(0, 999999).ToString("000000");
                        string dt = DateTime.Now.ToString("yyMMddHHmmss");
                        string trxReference = "TEB" + dt + randomNumber;
                        tranType.PaymentReference = trxReference;

                        //1. We debit the customer's beta account
                        Trace.TraceInformation(string.Format("About to debit customer. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                        var rp = new BankOneMobile.Services.SwitchServiceRef.Response();
                        try
                        {
                            using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                            {
                                rp = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, tranType.PaymentReference, tranType.Narration, false, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(String.Format("Error trying to debit customer with ref {0}. {1}", tranType.PaymentReference, ex.Message));

                            #region send error mail
                            try
                            {
                                using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                {
                                    string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                    string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                    client.SendEmail("100040", "donotreply@mybankone.com",
                                        emails, string.Format("BankOneMobile-TEB FT SwitchingService Error:{0}", ex.Message), body);
                                }
                            }
                            catch (Exception emailException)
                            {
                                System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                            }
                            #endregion

                            Trace.TraceError(String.Format("Now requerying transaction: {0}", tranType.PaymentReference));
                            try
                            {
                                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                {
                                    rp = client.RequeryTransactionStatus(mob.InstitutionCode, tranType.PaymentReference);
                                }
                                Trace.TraceError(String.Format("Done requerying transaction: {0}. status: {1}", tranType.PaymentReference, rp.IsSuccessful));
                            }
                            catch (Exception iex)
                            {
                                Trace.TraceError(String.Format("Web Service Error in Requery: {0}", iex.Message));
                                #region send error mail
                                try
                                {
                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                    {
                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                        client.SendEmail("100040", "donotreply@mybankone.com",
                                            emails, string.Format("BankOneMobile-TEB FT SwitchingService Trx Requery Error:{0}", ex.Message), body);
                                    }
                                }
                                catch (Exception emailException)
                                {
                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                }
                                #endregion
                            }
                        }
                        Trace.TraceInformation(string.Format("Done debiting customer. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, rp.IsSuccessful));
                        if (rp.IsSuccessful)
                        {
                            tranType.TransferStatus = FundsTransferStatus.Pending;
                            new DBNFundsTransferTransactionTypeSystem().SaveDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                            //2. We try and transfer funds to Destination bank

                            string xmlRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
                                                            <FTSingleCreditRequest>
	                                                            <SessionID>{0}</SessionID>
	                                                            <NameEnquiryRef>{1}</NameEnquiryRef>
	                                                            <DestinationInstitutionCode>{2}</DestinationInstitutionCode>
	                                                            <ChannelCode>2</ChannelCode>
	                                                            <BeneficiaryAccountName>{3}</BeneficiaryAccountName>
	                                                            <BeneficiaryAccountNumber>{4}</BeneficiaryAccountNumber>
                                                                <BeneficiaryBankVerificationNumber>{5}</BeneficiaryBankVerificationNumber>
	                                                            <BeneficiaryKYCLevel>{6}</BeneficiaryKYCLevel>
	                                                            <OriginatorAccountName>{7}</OriginatorAccountName>
	                                                            <OriginatorAccountNumber>{8}</OriginatorAccountNumber>
	                                                            <OriginatorBankVerificationNumber>{9}</OriginatorBankVerificationNumber>
	                                                            <OriginatorKYCLevel>{10}</OriginatorKYCLevel>
	                                                            <TransactionLocation></TransactionLocation>
                                                                <Narration>{11}</Narration>
                                                                <PaymentReference>{12}</PaymentReference>
	                                                            <Amount>{13}</Amount>
                                                            </FTSingleCreditRequest>

                                                 ", tranType.SessionID, tranType.NameEnquirySessionID, tranType.DestinationInstitutionCode, tranType.BeneficiaryAccountName,
                                                  tranType.BeneficiaryAccountNumber, tranType.BeneficiaryBankVerificationNumber, tranType.BeneficiaryKYCLevel,
                                                  tranType.OriginatorAccountName, tranType.OriginatorAccountNumber, tranType.OriginatorBankVerificationNumber,
                                                  tranType.OriginatorKYCLevel, tranType.Narration, tranType.PaymentReference, tranType.Amount);

                            Trace.TraceInformation(string.Format("Request XML: {0}", xmlRequest));

                            object[] trxResponse = null;
                            Trace.TraceInformation("Transfer to other banks. Service call begins.");
                            try
                            {
                                using (var client = new Services.NibssServiceReference.localnibbsServiceSoapClient())
                                {
                                    trxResponse = client.executeCreditTransferThruNibbs(xmlRequest);
                                }
                            }
                            catch(Exception ex)
                            {
                                Trace.TraceError(String.Format("NIBSS Service FT Error: {0}", ex.Message));
                                #region send error mail
                                try
                                {
                                    using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                    {
                                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                        string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                        client.SendEmail("100040", "donotreply@mybankone.com",
                                            emails, string.Format("BankOneMobile-DBN/NIBSS Service FT Error:{0}", ex.Message), body);
                                    }
                                }
                                catch (Exception emailException)
                                {
                                    System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                }
                                #endregion
                            }
                            Trace.TraceInformation(string.Format("Response: {0}", trxResponse == null ? "Null" : "Not Null"));
                            Trace.TraceInformation("Transfer to other banks. Service Call Ends.");

                            if (trxResponse == null)
                            {
                                #region Reversal
                                //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                string narration = String.Format("Reversal of Interbank Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                                var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                try
                                {
                                    using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                    {
                                        reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, "R" + tranType.PaymentReference, narration, true, true);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceError(String.Format("OT-Switching Service FT Reversal Error: {0}", ex.Message));
                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                            client.SendEmail("100040", "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-DBN-OT/Switching Service FT Reversal Error:{0}", ex.Message), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                }
                                Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                tranType.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                tranType.TransferStatus = FundsTransferStatus.Failed;
                                tranType.ResponseCode = "No Response";
                                tranType.ResponseDate = DateTime.Now;
                                tranType.TheTransaction.Status = TransactionStatus.Failed;
                                response.Status = TransactionStatus.Failed;

                                response.ResponseMessage = "Transfer not completed at Destination Bank!";
                                //throw new Exception(rp.ResponseMessage);
                                #endregion
                                new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                                throw new Exception("No response received from NIBBS");
                            }
                            string responseCode = trxResponse[0].ToString();
                            string responseXml = trxResponse[1].ToString();
                            if (responseCode == "00")
                            {
                                XmlDocument xmlDoc = new XmlDocument();
                                try
                                {
                                    xmlDoc.LoadXml(responseXml);
                                }
                                catch (Exception)
                                {
                                    Trace.TraceInformation("Invalid response from Diamond Bank web service for transfer to otherbanks. Was not able to load the XML into an XMLDocument");
                                    //Since this is an Error from Diamond Bank, we do not know whether this transaction was successful on NIBBS
                                    //hence, we return this transaction as successful to the agent and let the windows service requery to get the actual status
                                    response.Status = TransactionStatus.Successful;
                                    tranType.TransferStatus = FundsTransferStatus.Pending;
                                    new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                                    TransactionResponse res = new TransactionResponse { ResponseDescription = response.ResponseDescription, ResponseMessage = response.ResponseMessage, Status = response.Status.ToString() };

                                    return res;
                                    //throw new Exception("Invalid response from Diamond Bank.");
                                }

                                string theResponseCode = xmlDoc.SelectSingleNode("/FTSingleCreditResponse/ResponseCode").InnerText;
                                if (theResponseCode != "00")
                                {
                                    #region Reversal
                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of Interbank Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, "R" + tranType.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n", ex.Message);
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-DBN/OtherBanks Transfer Reversal web service Error from DBN:{0}", ex.Message), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                    Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                    tranType.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                    tranType.TransferStatus = FundsTransferStatus.Failed;
                                    tranType.ResponseCode = theResponseCode;
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Failed;
                                    response.Status = TransactionStatus.Failed;

                                    ResponseCodes theResponse = (ResponseCodes)Enum.Parse(typeof(ResponseCodes), theResponseCode);
                                    string msg = PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(theResponse.ToString());
                                    response.ResponseMessage = string.IsNullOrEmpty(msg) ? "Transfer not completed at Destination Bank!" : msg;
                                    //throw new Exception(rp.ResponseMessage);
                                    #endregion
                                }
                                else
                                {
                                    tranType.TransferStatus = FundsTransferStatus.Successful;
                                    tranType.ResponseCode = theResponseCode;
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                                    response.Status = TransactionStatus.Successful;
                                }
                            }
                            else
                            {
                                string errorMessage = string.Empty;
                                string requeryResponse = string.Empty;
                                try
                                {
                                    requeryResponse = RequeryOtherBanksTransferTransaction("999063", tranType.SessionID, out errorMessage);
                                }
                                catch(Exception ex)
                                {
                                    Trace.TraceError(String.Format("DBN/OtherBanks Transfer Requery Error: {0}", ex.Message));
                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("{0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                            client.SendEmail("100040", "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-DBN/OtherBanks Transfer Requery Error:{0}", ex.Message), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                }
                                if (String.IsNullOrEmpty(requeryResponse))
                                {
                                    //Since this is an Error from Diamond Bank, we do not know whether this transaction was successful on NIBBS
                                    //hence, we return this transaction as successful to the agent and let the windows service requery to get the actual status
                                    Trace.TraceError(String.Format("DBN/OtherBanks Transfer Requery Error - No response from DBN: {0}", errorMessage));
                                    tranType.TheTransaction.Status = TransactionStatus.Pending;
                                    response.Status = TransactionStatus.Successful;
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() != "-96" && requeryResponse.ToLower() != "00")
                                {
                                    #region Reversal
                                    //since transfer was not successful, we reverse the amount initailly debited from the cusotmers account
                                    string narration = String.Format("Reversal of Interbank Transfer @ {0} ({1})", agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                                    Trace.TraceInformation(string.Format("About to reverse funds. Amount = {0}. Account is {1} ", tranType.Amount, tranType.OriginatorAccountNumber));
                                    var reversalResponse = new BankOneMobile.Services.SwitchServiceRef.Response();
                                    try
                                    {
                                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                                        {
                                            reversalResponse = client.DiamondBankOneTransferFundsCustomerAccountDebitOrCredit(mob.InstitutionCode, tranType.OriginatorAccountNumber, tranType.Amount, tranType.Fee, tranType.VATInPercentage, "R" + tranType.PaymentReference, narration, true, true);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        #region send error mail
                                        try
                                        {
                                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                            {
                                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                                string body = string.Format("{0}\n", ex.Message);
                                                client.SendEmail("100040", "donotreply@mybankone.com",
                                                    emails, string.Format("BankOneMobile-DBN/OtherBanks Transfer Reversal web service Error from DBN:{0}", errorMessage), body);
                                            }
                                        }
                                        catch (Exception emailException)
                                        {
                                            System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                        }
                                        #endregion
                                    }
                                    Trace.TraceInformation(string.Format("Done reversing funds. Amount = {0}. Account is {1}. Status: {2} ", tranType.Amount, tranType.OriginatorAccountNumber, reversalResponse.IsSuccessful));
                                    tranType.ReversalStatus = reversalResponse.IsSuccessful ? DBNReversalStatus.Successful : DBNReversalStatus.NotSuccessful;

                                    tranType.TransferStatus = FundsTransferStatus.Failed;
                                    tranType.ResponseCode = requeryResponse.ToLower();
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Failed;
                                    response.Status = TransactionStatus.Failed;

                                    ResponseCodes theResponse = (ResponseCodes)Enum.Parse(typeof(ResponseCodes), requeryResponse.ToLower());
                                    string msg = PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(theResponse.ToString());
                                    response.ResponseMessage = string.IsNullOrEmpty(msg) ? "Transfer not completed at Destination Bank!" : msg;
                                    //throw new Exception(rp.ResponseMessage);
                                    #endregion
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "00")
                                {
                                    tranType.TransferStatus = FundsTransferStatus.Successful;
                                    tranType.ResponseCode = requeryResponse;
                                    tranType.ResponseDate = DateTime.Now;
                                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                                    response.Status = TransactionStatus.Successful;
                                }
                                else if (!String.IsNullOrEmpty(requeryResponse) && requeryResponse.ToLower() == "-96")
                                {
                                    //Since this is an Error from Diamond Bank, we do not know whether this transaction was successful on NIBBS
                                    //hence, we return this transaction as successful to the agent and let the windows service requery to get the actual status
                                    Trace.TraceError(String.Format("DBN/OtherBanks Transfer Requery Error from DBN: {0}", errorMessage));
                                    tranType.TheTransaction.Status = TransactionStatus.Pending;
                                    response.Status = TransactionStatus.Successful;

                                    #region send error mail
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("{0}\n", errorMessage);
                                            client.SendEmail("100040", "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile-DBN/OtherBanks Transfer Requery Error from DBN:{0}", errorMessage), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceError(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            response.Status = TransactionStatus.Failed;
                            response.ResponseMessage = rp.ResponseMessage;
                        }
                    }
                    else if (tranType.DBNFundsTransferType == DBNFundsTransferTransactionTypeName.NonAccountHolder)
                    {
                        Trace.TraceInformation("Type is NonAccountHolder");
                        string token = tranType.SessionID;
                        string recordType = tranType.IsCustomerSending ? "Debit" : "Credit";
                        string xmlRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
                                                            <FundsTransferRequest>
	                                                            <InstitutionCode>{0}</InstitutionCode>
	                                                            <AgentCode>{1}</AgentCode>
	                                                            <SourceAccountNumber>{2}</SourceAccountNumber>
	                                                            <ReferenceNumber>{3}</ReferenceNumber>
	                                                            <Amount>{4}</Amount>
	                                                            <Narration>{5}</Narration>
	                                                            <RecordType>{6}</RecordType>
                                                            </FundsTransferRequest>

                                                 ", mob.InstitutionCode, (agent as Agent).AccountNumber,tranType.OriginatorAccountNumber, tranType.PaymentReference,
                                                  tranType.Amount,tranType.Narration,recordType);
                        //1. We debit the agent's till account
                        Trace.TraceInformation(string.Format("About to {0} agents till. Amount = {1}.", recordType, tranType.Amount));
                        //new DBNFundsTransferTransactionTypeSystem().SaveDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                        string rp = string.Empty;
                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                        {
                            rp = client.BankOneFundsTransferToNonAccountHolder(xmlRequest);
                        }
                        Trace.TraceInformation(string.Format("Done debiting agent till. Response = {0}", rp));
                        XmlDocument xmlDoc = new XmlDocument();
                        try
                        {
                            xmlDoc.LoadXml(rp);
                        }
                        catch (Exception ex)
                        {
                            response.Status = TransactionStatus.Failed;
                            response.ResponseMessage = ex.Message;
                        }
                        bool status = bool.Parse(xmlDoc.SelectSingleNode("/FundsTransferResponse/Status").InnerText);
                        string statusMessage = xmlDoc.SelectSingleNode("/FundsTransferResponse/StatusMessage").InnerText;
                        if (!status)
                        {
                            if(!tranType.IsCustomerSending)
                            {
                                tranType.StatusMessage = DBNNonAccountCashCollectionStatus.NotSuccessful;
                                tranType.ResponseDate = DateTime.Now;
                                tranType.ResponseCode = "Pending";
                                tranType.TheTransaction.Status = TransactionStatus.Failed;
                                response.Status = TransactionStatus.Failed;
                                response.ResponseMessage = statusMessage;
                                new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                            }
                            else
                            {
                                //tranType.ResponseCode = "Pending";
                                tranType.TheTransaction.Status = TransactionStatus.Failed;
                                response.Status = TransactionStatus.Failed;
                                response.ResponseMessage = statusMessage;
                            }
                        }
                        else
                        {
                            if (!tranType.IsCustomerSending)
                            {
                                tranType.StatusMessage = DBNNonAccountCashCollectionStatus.Successful;
                                tranType.ResponseCode = "00";
                                tranType.ResponseDate = DateTime.Now;
                                tranType.TheTransaction.Status = TransactionStatus.Successful;
                                response.Status = TransactionStatus.Successful;
                                new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                            }
                            else
                            {
                                tranType.ResponseCode = "Pending";
                                tranType.ResponseDate = DateTime.Now;
                                tranType.TheTransaction.Status = TransactionStatus.Successful;
                                response.Status = TransactionStatus.Successful;
                                new DBNFundsTransferTransactionTypeSystem().SaveDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                            }
                        }
                    }
                }

                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Transaction not permitted to non-agents Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (NoHSMResponseException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("HSM Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }

                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("NoHSMResponseException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("NoHSMResponseException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, mob.MobilePhone);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new NoHSMResponseException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("CoreBankingWebServiceException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("CoreBankingWebServiceException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, mob.MobilePhone);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new CoreBankingWebServiceException(fex.Detail.Message);
                }
                catch (FaultException fex)
                {
                    response.ResponseDescription = fex.Message;
                    response.ResponseMessage = fex.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("FaultException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("FaultException Error from web service{0}\n{1}\nPhone: {2}", fex.Message, agent.PhoneNumber, mob.MobilePhone);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Message), body);
                    }

                    throw;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, mob.MobilePhone, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                    }

                    throw;

                }
                if (tranType.DBNFundsTransferType != DBNFundsTransferTransactionTypeName.NonAccountHolder) new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(currentWorkflowInfo.DataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.FundsTransferLocal;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }

            #endregion
            # region DO Actual ISO Transaction
            try
            {
                //if (!string.IsNullOrEmpty(trans.ToInstitutionCode)&&(trans.FromInstitutionCode != trans.ToInstitutionCode))
                //{
                //string message = String.Format("To Inst Code= {0}, From InstCode={1}",trans.ToInstitutionCode,trans.FromInstitutionCode);
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
                //IList<IInstitution> inst = new InstitutionSystem(currentWorkflowInfo.DataSource).GetActiveInstitutions(_theDataSource);
                //Dictionary<string, IInstitution> insts = inst.ToDictionary(x => x.Code);
                ////if (!insts[trans.FromInstitutionCode].AllowInterMFBTrans ||! insts[trans.ToInstitutionCode].AllowInterMFBTrans)
                //{
                //    throw new TransactionNotSupportedByInstitution("Inter-MFB Transactions are not supported by your Institution");
                //}
                //}


                if (doISO)
                {

                    response = ProcessISOTransaction.DoISOTransactions(trans, transacType);

                    if (response.Status != TransactionStatus.Successful)
                    {
                        trans.Status = response.Status;
                        transacType.TheTransaction.Status = response.Status;
                        Trace.TraceInformation("Status  For {0} is {1}", transacType.TheTransaction.STAN, response.Status);

                        trans.StatusDetails = response.ResponseMessage;

                        //throw new FailedTransactionException(response.ResponseMessage);

                    }
                    else
                    {
                        trans.Status = TransactionStatus.Successful;
                        trans.StatusDetails = "Trasaction Successful";
                    }
                }

            }

            catch (Exception ex)
            {


                if (trans.Status != TransactionStatus.Reversed)
                {
                    trans.Status = TransactionStatus.Failed;
                }
                trans.SwitchTransactionTime = DateTime.Now;
                trans.StatusDetails = ex.Message;

                throw;
            }
            finally
            {
                //if (session.TheTransactions == null)
                // {
                //     session.TheTransactions = new List<ITransaction>();
                // }
                //     session.TheTransactions.Add(trans);//
                try
                {
                    Trace.TraceInformation("Before Update Trans is {0}, TransacType is {1}", trans.STAN, transacType.TheTransaction.Status);
                    transacType = _repository.UpdateTransactionType(currentWorkflowInfo.DataSource, transacType);

                    UpdateTransaction(trans);
                    Trace.TraceInformation("After  Update Trans is {0} , TransacType is {1}", trans.STAN, transacType.TheTransaction.Status);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When updating transaction.");
                    //System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                }
            }

            # endregion

            if (trans.Status != TransactionStatus.Successful)
            {
                throw new FailedTransactionException(response.ResponseMessage);
            }
            TransactionResponse toReturn = new TransactionResponse { ResponseDescription = response.ResponseDescription, ResponseMessage = response.ResponseMessage, Status = response.Status.ToString() };

            return toReturn;
        }

        public string RequeryDBNTransferTransaction (string transactionReference)
        {
            string requeryResponse = string.Empty;
            Trace.TraceInformation(String.Format("About to get {0} status from diamond bank.", transactionReference));

            try
            {
                BypassCertificateError();
                using (DiamondBankOneConnectRef.diamondbankoneConnectSoapClient diamondBankClient = new DiamondBankOneConnectRef.diamondbankoneConnectSoapClient())
                {
                    requeryResponse = diamondBankClient.requeryTransaction(transactionReference);
                }
            }
            catch(Exception)
            {
                throw;
            }
            Trace.TraceInformation(String.Format("Gotten {0} status from diamond bank [{1}]", transactionReference, requeryResponse));
            return requeryResponse;
            //return "pending";
        }

        public string RequeryOtherBanksTransferTransaction(string sourceInstitutionCode, string sessionID, out string errorMessage)
        {
            object[] response = null;
            errorMessage = string.Empty;
            string xmlRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
                                                            <TSQuerySingleRequest>
	                                                            <SourceInstitutionCode>{0}</SourceInstitutionCode>
	                                                            <ChannelCode>2</ChannelCode>
                                                                <SessionID>{1}</SessionID>
                                                            </TSQuerySingleRequest>
                                                 ", sourceInstitutionCode, sessionID);

            Trace.TraceInformation(String.Format("About to get {0} status from NIBSS through diamond bank.", sessionID));

            try
            {
                using (var client = new BankOneMobile.Services.NibssServiceReference.localnibbsServiceSoapClient())
                {
                    response = client.executeStatusEnquiryThruNibbs(xmlRequest);
                }
            }
            catch(Exception)
            {
                throw;
            }
            Trace.TraceInformation(string.Format("Response: {0}", response == null ? "Null" : "Not Null"));
            Trace.TraceInformation("Other banks Trx Requery Service Call Ends.");

            if (response == null)
            {
                throw new Exception("Trx Requery: No response from NIBBS through Diamond Bank");
            }
            string responseCode = response[0].ToString();
            string responseXml = response[1].ToString();
            if (responseCode == "00")
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(responseXml);
                }
                catch (Exception)
                {
                    Trace.TraceInformation("Invalid response from Diamond Bank web service for transfer to otherbanks. Was not able to load the XML into an XMLDocument");
                    throw new Exception("Invalid response from NIBSS through Diamond Bank.");
                }

                string theResponseCode = xmlDoc.SelectSingleNode("/TSQuerySingleResponse/ResponseCode").InnerText;
                return theResponseCode;
            }
            else
            {
                errorMessage = responseXml;
                return responseCode;
            }
            //return "-96";
        }

        public static void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback +=

                delegate(
                    Object sender1,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
        }
        public TransactionResponse RunLafayetteTransaction(string pin, ITransactionType transacType, MobileAccount mob, string from, string to, decimal amount, string sessionID)
        {
            string msg = string.Format("Inside Create Laf Acc from Agent, Product is  Code:{0}", (transacType as RegistrationTransactionType).IDTypeCode);
            Trace.TraceInformation(msg);
            byte[] encryptedPinBytes = new byte[8];
            if (!string.IsNullOrEmpty(pin))
            {
                ThalesSim.Core.Utility.HexStringToByteArray(pin, ref encryptedPinBytes);
            }
            bool doISO = true;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBySessionID(sessionID);
            _theDataSource = currentWorkflowInfo.DataSource;
            MobileAccount initiatingAccount = null;
            IAgent agent = null;// new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            ISession session = new SessionSystem(currentWorkflowInfo.DataSource).GetBySessionID(sessionID);// Get the  ussd session that was used to orginate the request do we can save it with the transaction
            //if (session == null)
            //{
            //    throw new InvalidSessionIdException("Session ID is invalid");
            //}

            ISOResponse response = null;
            Transaction trans = new Transaction { Date = DateTime.Now, Status = TransactionStatus.Pending, EncryptedPIN = string.IsNullOrEmpty(pin) ? null : encryptedPinBytes, Session = session, From = from, To = to, FromPhoneNumber = mob.MobilePhone, TransactionTypeName = TransactionTypeName.BalanceEnquiry, TheFee = transacType.TheFee };

            if (session != null && session.TheTransactions == null)
            {
                session.TheTransactions = new List<ITransaction>();
                session.TheTransactions.Add(trans);
            }

            transacType.TheTransaction = trans;// create the transaction and tie the transaction and the transaction type to each other

            # region Registration

            if (transacType is RegistrationTransactionType)
            {
                response = new ISOResponse();
                RegistrationTransactionType tranType = transacType as RegistrationTransactionType;
                tranType.TheTransaction = trans;
                tranType.TheMobileAccount = mob;
                try
                {
                    trans.FromPhoneNumber = mob.MobilePhone;
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                        agent = client.GetAgentByPhoneNumber(mob.MobilePhone);
                    }
                    // agent =  new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }


                    // session.PayerAccountNumber = agent.TheAgentAccountNumber;
                    LafayettMobileAccountService.MobileAccount registredAccount = null;
                    //using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    //{
                    //    if (tranType.IsEnhanced)
                    //    {
                    //        registredAccount = client.CreateEnhancedMobileAccountFromAgent(tranType.LastName,
                    //            tranType.FirstName, tranType.PhoneNumber, tranType.Gender, tranType.TheProduct as Product,
                    //            agent.Code, tranType.StarterPackNumber, tranType.IDNumber, tranType.NOKPhone, tranType.NOKName,
                    //            tranType.Address, tranType.PlaceOfBirth, tranType.ReferralName, tranType.ReferralPhoneNumber,
                    //            tranType.DateOfBirth, tranType.HasSufficientInfo, tranType.AccountSource.ToString(),
                    //            tranType.OtherAccountInfo, tranType.Passport);
                    //    }


                    //}  e
                    Trace.TraceInformation("TS System is " + tranType.FirstName);
                    Trace.TraceInformation("Agent is to be gotten " + tranType.FirstName);
                    Trace.TraceInformation("Agent Account is " + agent == null ? "Agent Null" : agent.LastName);
                    //Trace.TraceInformation("Agent Account is " + agent.TheAgentAccount==null?"Null":agent.TheAgentAccount.BankAccount);             

                    if (agent.TheAgentAccount == null)
                    {
                        agent.TheAgentAccount = mob.RecievingBankAccount;
                        Trace.TraceInformation("Agent is Null");
                    }
                    else
                    {
                        Trace.TraceInformation("Agent is Null -{0}", agent.TheAgentAccount.BankAccount);
                    }


                    using (LafayettMobileAccountService.MobileAccountServiceClient cl = new LafayettMobileAccountService.MobileAccountServiceClient())
                    {
                        Trace.TraceInformation("Within  -{0}", tranType.IDTypeCode);
                        registredAccount = cl.CreateLafayetteMobileAccountFromAgent(tranType.LastName,
                                tranType.FirstName, tranType.PhoneNumber, tranType.Gender,
                                agent.Code, tranType.ProductCode, tranType.PlaceOfBirth, tranType.DateOfBirth, tranType.IDNumber, tranType.NOKPhone, tranType.NOKName,
                                tranType.Address, tranType.ReferralName, tranType.IDTypeCode,
                                 tranType.HasSufficientInfo, tranType.AccountSource.ToString(),
                                tranType.OtherAccountInfo, tranType.Passport, tranType.Country, tranType.State, tranType.Territory, tranType.District, tranType.IDExpiryDate, tranType.IDIssuedDate);
                    }


                    //NB!!! Referal Phone Number holds the Lafayette ID Type Code
                    // new MobileAccountSystem(_theDataSource).;

                    tranType.TheTransaction.Status = TransactionStatus.Successful;
                    response.ResponseDescription = registredAccount.ReceivingAccountNumber;
                    response.ResponseMessage = registredAccount.ActivationCode;
                    response.Status = TransactionStatus.Successful;

                    Trace.TraceInformation("Save Image Starting No-{0}, Code-{1}", agent.PhoneNumber, agent.InstitutionCode);
                    bool done = new BankoneMobile.AdvansLafayette.Services.ServiceInterfaceSystem().SaveImages(response.ResponseDescription, registredAccount.CustomerID, tranType.Passport);
                    Trace.TraceInformation("Save Image Was Attempted. Status= {0}, Account Number = {1} Customer ID = {2} Passport = {3}", done, response.ResponseDescription, registredAccount.CustomerID, tranType.Passport == null ? "No Image" : tranType.Passport.Count().ToString());


                    //using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    //{
                    //    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    //    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    //    {
                    //        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    //    }
                    //    string body = string.Format("Successfully created core banking and mobile accounts: {0}: Customer: {1}", mob.MobilePhone, tranType.PhoneNumber);
                    //    client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                    //        emails, string.Format("BankOneMobile:{0}", " Successful"), body);
                    //    //PANE.ERRORLOG.ErrorLogger.Log(ex);
                    //}
                }

                catch (AlreadyExistingAccountException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "Accounts already exist.Please try again";
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (AlreadyRegisterdCustomerException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;
                    throw;
                }
                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Transaction not permitted to non-agents Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (NoHSMResponseException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("HSM Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyRegisterdCustomerException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyRegisterdCustomerException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyRegisterdCustomerException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = "Accounts already exist. Please try again";
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("AlreadyExistingAccountException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("AlreadyExistingAccountException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new AlreadyExistingAccountException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("NoHSMResponseException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("NoHSMResponseException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new NoHSMResponseException(fex.Detail.Message);
                }
                catch (FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException> fex)
                {
                    response.ResponseDescription = fex.Detail.Message;
                    response.ResponseMessage = fex.Detail.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("CoreBankingWebServiceException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Detail.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("CoreBankingWebServiceException Error from web service{0}\n{1}\nPhone: {2}", fex.Detail.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Detail.Message), body);
                    }

                    throw new CoreBankingWebServiceException(fex.Detail.Message);
                }
                catch (FaultException fex)
                {
                    response.ResponseDescription = fex.Message;
                    response.ResponseMessage = fex.Message;
                    response.Status = TransactionStatus.Failed;

                    System.Diagnostics.Trace.TraceError("FaultException Error from web service. When running transaction.");
                    System.Diagnostics.Trace.TraceError(fex.Message);

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("FaultException Error from web service{0}\n{1}\nPhone: {2}", fex.Message, agent.PhoneNumber, tranType.PhoneNumber);
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", fex.Message), body);
                    }

                    throw;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                    System.Diagnostics.Trace.TraceError("Error in Transaction system. When running transaction.");
                    System.Diagnostics.Trace.TraceError("Exception Type: {0}", ex.GetType().Name);
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.Source);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                    {
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, tranType.PhoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                        client.SendEmail(string.IsNullOrEmpty(mob.InstitutionCode) ? "BankOneMobile" : mob.InstitutionCode, "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                    }

                    throw;

                }
                tranType.Date = DateTime.Now;
                new RegistrationTransactionTypeSystem(currentWorkflowInfo.DataSource).SaveRegistrationTransactionType(tranType);
                trans.TransactionTypeName = TransactionTypeName.Registration;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }

            # endregion




            TransactionResponse toReturn = new TransactionResponse { ResponseDescription = response.ResponseDescription, ResponseMessage = response.ResponseMessage, Status = response.Status.ToString() };

            return toReturn;
        }

        public TransactionResponse GetISWReferences(string transactionReference)
        {
            TransactionResponse toReturn = new TransactionResponse(); string responseXML = string.Empty; string responseCode = string.Empty;
            string transRef = string.Empty; string value = string.Empty;
            try
            {
                //  using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
                //  {
                //       responseXML= client.ConfirmPaymentMessage(transactionReference);
                //  }
            }
            catch
            {
                throw new NoISWGatewayResponse("ISW Gateway Unavailable");
            }
            responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");

            if (!string.IsNullOrEmpty(responseXML))
            {
                XDocument document = XDocument.Parse(responseXML);


                foreach (var doc in document.Descendants("ResponseData"))
                {
                    responseCode = doc.Element("ResponseCode") != null ? doc.Element("ResponseCode").Value : responseCode;

                    transRef = doc.Element("TransactionRef") != null ? doc.Element("TransactionRef").Value : responseCode;
                    value = doc.Element("RechargePIN") != null ? doc.Element("RechargePIN").Value : responseCode;

                    // do whatever you want to do with those items of information now
                }
            }
            transRef = new Random().Next(10000, 99999).ToString();
            toReturn.STAN = transRef;
            toReturn.ResponseMessage = value;
            //TODO call the right web service
            return toReturn;
        }

        private bool DoPinVerficaction(string phoneNumber, string encryptedPIN)
        {
            //TODO Do real PIN Verification
            return true;
        }




        public static string EncryptPin(string pin)
        {
            // TODO Do real encryption
            return pin;
        }

        public bool DoPINVerfication(string phoneNumber, string encryptedPIN)
        {
            //TODO actual pin verification
            return true;
        }

        public TransactionResponse DoAccountActivation(IPinChangeTransactionType tranType)
        {
            TransactionResponse transResponse = new TransactionResponse();
            MobileAccount mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(tranType.TheMobileAccount.MobilePhone);
            if (mob.ActivationCode != tranType.OldPIN)
            {
                transResponse.Status = TransactionStatus.Failed.ToString();
                transResponse.ResponseMessage = "Invalid Activation Code";
                transResponse.ResponseDescription = "Invalid Activation Code";
                return transResponse;
            }
            Transaction trans = new Transaction();
            trans.FromPhoneNumber = tranType.TheMobileAccount.MobilePhone;
            trans.Date = DateTime.Now;
            trans.Amount = 0.0M;
            byte[] holder = new byte[8];
            ThalesSim.Core.Utility.HexStringToByteArray(tranType.OldPIN, ref holder);
            trans.EncryptedPIN = holder;
            trans.TransactionTypeName = TransactionTypeName.AccountActivation;

            ISOResponse response = ProcessISOTransaction.DoISOTransactions(trans, tranType);
            //if (response.Status == TransactionStatus.Successful)
            //{
            tranType.TheMobileAccount.MobileAccountStatus = MobileAccountStatus.Active;
            new MobileAccountSystem(_theDataSource).UpdateMobileAccount(tranType.TheMobileAccount as MobileAccount);
            // }
            transResponse.ResponseMessage = "Account Verification Succesfull"; transResponse.ResponseDescription = "Account Verification Succesfull"; transResponse.Status = TransactionStatus.Successful.ToString();
            //return new TransactionResponse{ ResponseMessage=response.ResponseMessage, ResponseDescription=response.ResponseDescription, Status=response.Status.ToString()};
            //return new TransactionResponse { ResponseMessage = "Account Succesfully Activated", ResponseDescription = "Account Succesfully Activated", Status = "Succesfully" };
            return transResponse;
        }
        public TransactionResponse TestRunTransaction(string pin, ITransactionType transacType, MobileAccount mob, string from, string to, decimal amount, string sessionID)
        {
            bool doISO = true;
            ISession session = new SessionSystem(_theDataSource).GetBySessionID(sessionID);// Get the  ussd session that was used to orginate the request do we can save it with the transaction
            if (session == null)
            {
                throw new InvalidSessionIdException("Session ID is invalid");
            }
            ISOResponse response = null;
            Transaction trans = new Transaction { Date = DateTime.Now, Status = TransactionStatus.Pending, Session = session, From = from, To = to, FromPhoneNumber = mob.MobilePhone, TransactionTypeName = TransactionTypeName.BalanceEnquiry };
            transacType.TheTransaction = trans;// create the transaction and tie the transaction and the transaction type to each other
            # region BalanceInquiry

            if (transacType is BalanceInquiryTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                BalanceInquiryTransactionType tranType = transacType as BalanceInquiryTransactionType;
                trans.TransactionTypeName = TransactionTypeName.BalanceEnquiry;
                tranType.TheMobileAccount = mob;
                new BalanceInquiryTransactionTypeSystem(_theDataSource).SaveBalanceInquiryTransactionType(tranType);
                trans.TransactionTypeID = tranType.ID;

            }
            # endregion
            # region MiniStatement

            else if (transacType is MiniStatementTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                MiniStatementTransactionType tranType = transacType as MiniStatementTransactionType;
                trans.TransactionTypeName = TransactionTypeName.MiniStatement;
                tranType.TheMobileAccount = mob;
                MiniStatementTransactionTypeSystem.SaveMiniStatementTransactionType(_theDataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.MiniStatement;
                trans.TransactionTypeID = tranType.ID;

            }
            # endregion
            # region PinChange

            else if (transacType is IPinChangeTransactionType)
            {
                PinChangeTransactionType tranType = transacType as PinChangeTransactionType;
                tranType.TheMobileAccount = mob;
                PinChangeTransactionTypeSystem.SavePinChangeTransactionType(_theDataSource, tranType);
                trans.TransactionTypeName = TransactionTypeName.PINChange;
                trans.TransactionTypeID = tranType.ID;
            }
            # endregion
            # region Registration

            else if (transacType is RegistrationTransactionType)
            {
                response = new ISOResponse();
                RegistrationTransactionType tranType = transacType as RegistrationTransactionType;
                tranType.TheMobileAccount = mob;
                try
                {
                    IAgent agent = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                    if (agent == null)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
                    }
                    if (!agent.IsActive)
                    {
                        throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
                    }
                    if (!DoPinVerficaction(agent.MobileAccount.MobilePhone, pin))
                    {
                        throw new InvalidPinException("Invalid PIN");
                    }
                    MobileAccount registredAccount = new MobileAccountSystem(_theDataSource).CreateMobileAccountFromAgent(tranType.LastName, tranType.FirstName, tranType.PhoneNumber, tranType.TheGender.ToString(), tranType.TheProduct as Product, agent as Agent, false);
                    response.ResponseDescription = "Account Succesfully created";
                    response.ResponseMessage = string.Format("Account Succesfully created. <br> The activation Code is {0}.<br> Please help the customer to complete activation", registredAccount.ActivationCode);
                    response.Status = TransactionStatus.Successful;
                }
                catch (AlreadyExistingAccountException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "Accounts already exist.Please try again";
                    response.Status = TransactionStatus.Failed;
                }
                catch (AlreadyRegisterdCustomerException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = "This phone  Number has already been regsitered on BankOne!";//todo Please confirm what to do
                    response.Status = TransactionStatus.Failed;
                }
                catch (TransactionNotPermittedToNonAgentsException ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;
                }
                catch (Exception ex)
                {
                    response.ResponseDescription = ex.Message;
                    response.ResponseMessage = ex.Message;
                    response.Status = TransactionStatus.Failed;

                }
                new RegistrationTransactionTypeSystem(_theDataSource).SaveRegistrationTransactionType(tranType);
                trans.TransactionTypeName = TransactionTypeName.Registration;
                trans.TransactionTypeID = tranType.ID;
                doISO = false;

            }
            if (doISO)
            {
                TransactionResponse rsp = new ProcessISOTransaction().SendISO(trans.From, trans.To, trans.Amount, trans.FromPhoneNumber);//(trans);// Now send the transaction to where the ISO will be generated and sent
                trans.STAN = rsp.STAN;
                trans.Status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), rsp.Status);
                UpdateTransaction(trans);
                return rsp;

            }
            # endregion
            # region LocaFundsTransfer
            if (transacType is LocalFundsTransferTransactionType)// save the transaction type based on what type of transaction Type it is
            {
                LocalFundsTransferTransactionType tranType = transacType as LocalFundsTransferTransactionType;
                trans.TransactionTypeName = TransactionTypeName.LocalFundsTransfer;

                new LocalFundsTransferTransactionTypeSystem(_theDataSource).SaveLocalFundsTransferTransactionType(tranType);
                trans.TransactionTypeID = tranType.ID;

            }
            # endregion
            trans.Status = response.Status;//
            if (session.TheTransactions == null)
            {
                session.TheTransactions = new List<ITransaction>();
            }
            session.TheTransactions.Add(trans);// = trans;

            UpdateTransaction(trans);// update the transaction to this new status of succesfulll or failed
            TransactionResponse toReturn = new TransactionResponse { ResponseDescription = response.ResponseDescription, ResponseMessage = response.ResponseMessage, Status = response.Status.ToString() };
            //for mat the response in the way the work flow guys can understnad            
            //_repository.DbContext.Close(_theDataSource);
            return toReturn;
        }


        public bool VerifyPin(MobileAccount mob, string pinOffset)
        {
            return mob.PINOffset == pinOffset;
        }

        public bool ChangPin(MobileAccount mob, string oldPin, string newpin)
        {
            if (mob.PINOffset != oldPin)
            {
                throw new InvalidPinException("Invalid PIN");
            }
            mob.PINOffset = newpin;
            new MobileAccountSystem(_theDataSource).UpdateMobileAccount(mob);
            //_repository.DbContext.Close(_theDataSource);
            return true;
        }
        public string GenerateTransactionRef()
        {
            lock (this)
            {
                DateTime todaysDate = DateTime.Now;
                string transactionReference = string.Format("{0:yyyyMMddHHmmssff}", todaysDate);
                return transactionReference;// +"T";
            }
        }
        public string DoLinkedAccountEnquiry(IMobileAccount mob, byte[] encryptedPin)
        {
            TransactionResponse resp = new TransactionResponse();
            ITransaction trans = new Transaction();
            trans.From = mob.RecievingBankAccount.BankAccount;
            trans.FromPhoneNumber = mob.MobilePhone;
            trans.FromInstitutionCode = mob.RecievingBankAccount.InstitutionCode;
            trans.EncryptedPIN = encryptedPin;
            AccountEnquiryResponse rsp = new ProcessISOTransaction().DoAccountEnquiry(trans);

            if (rsp.ResponseCode == "00")
            {
                return "00";
            }
            else
            {
                return rsp.ResponseDescription;
            }

        }
        //For PIN-less Balance Enquiry
        public BalanceEnquiryResponse DoPinlessBalanceEnquiry(ILinkingBankAccount account, string mobilePhone)
        {
            ITransaction trans = new Transaction();
            trans.From = account.BankAccount;
            trans.FromPhoneNumber = mobilePhone;
            trans.FromInstitutionCode = account.InstitutionCode;
            //Set PIN as null.
            string pin = HSMCenter.GeneratePinBlock(mobilePhone, "0000", AppZone.HsmInterface.PinBlockFormats.ANSI);
            byte[] pinBytes = new byte[8];
            ThalesSim.Core.Utility.HexStringToByteArray(pin, ref pinBytes);
            trans.EncryptedPIN = pinBytes;
            //Set Transaction Service
            trans.TheService = Service.UssdPinlessBilling;
            BalanceEnquiryResponse rsp = new ProcessISOTransaction().DoBalanceEnquiry(trans);

            if (rsp.ResponseCode == "00")
            {
                return rsp;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        ///For PIN-less Customer Billing for ussd session
        /// </summary>
        /// <param name="account">The bankone customer account to be debited</param>
        /// <param name="toAccountNumber">the account number of the AppZone income account to be credited</param>
        /// <param name="mobilePhone">the customer's mobile phone number</param>
        /// <param name="amount">the debit amount</param>
        /// <returns>The response code(00 -> successful, else response description)</returns>
        public string DoPinlessCustomerDebit(ILinkingBankAccount account, string toAccountNumber, string mobilePhone, decimal amount)
        {
            ITransaction trans = new Transaction();
            //From
            trans.From = account.BankAccount;
            trans.FromPhoneNumber = mobilePhone;
            trans.FromInstitutionCode = trans.ToInstitutionCode = account.InstitutionCode;
            //To
            trans.To = toAccountNumber;
            ///Set PIN as null.
            string pin = HSMCenter.GeneratePinBlock(mobilePhone, "0000", AppZone.HsmInterface.PinBlockFormats.ANSI);
            byte[] pinBytes = new byte[8];
            ThalesSim.Core.Utility.HexStringToByteArray(pin, ref pinBytes);
            trans.EncryptedPIN = pinBytes;
            //Set Transaction Service
            trans.TheService = Service.UssdPinlessBilling;
            trans.Amount = amount;
            trans.Narration = string.Format("Debit for USSD session - PhoneNumber:{0}", mobilePhone);
            PinlessDebitResponse rsp = new ProcessISOTransaction().DoPinlessDebit(trans);

            if (rsp.ResponseCode == "00")
            {
                return "00";
            }
            else
            {
                return rsp.ResponseDescription;
            }
        }
        /// <summary>
        ///  Do Pin-less debit on mobile teller for ussd session
        /// </summary>
        /// <param name="account">The teller's linked account</param>
        /// <param name="mobilePhone">The teller's mobile phone number</param>
        /// <param name="amount">The debit amount</param>
        /// <returns></returns>
        public string DoPinlessMobileTellerDebit(ILinkingBankAccount account, string mobilePhone, decimal amount)
        {
            ITransaction trans = new Transaction();
            trans.FromPhoneNumber = mobilePhone;
            trans.FromInstitutionCode = trans.ToInstitutionCode = account.InstitutionCode;
            //Put anything as ToAccount
            //trans.To = account.BankAccount;
            trans.To = account.InstitutionCode;
            //Set PIN as null.
            trans.EncryptedPIN = new byte[8];
            //Set Transaction Service
            trans.TheService = Service.UssdPinlessBilling;
            trans.Amount = amount;
            trans.Narration = string.Format("Debit for mobile teller USSD session - PhoneNumber:{0}", mobilePhone);
            PinlessDebitResponse rsp = new ProcessISOTransaction().DoPinlessDebit(trans);

            if (rsp.ResponseCode == "00")
            {
                return "00";
            }
            else
            {
                return rsp.ResponseDescription;
            }
        }
        public List<ITransaction> Search(string phoneNum, string institutionCode, string transactionType, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ITransaction> tr = new List<ITransaction>();






            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                TransactionStatus stat = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
                dic.Add("Status", stat);

            }
            if (!string.IsNullOrEmpty(phoneNum) && !string.IsNullOrEmpty(phoneNum.Trim()))
            {

                dic.Add("FromPhoneNumber", phoneNum);

            }
            if (!string.IsNullOrEmpty(transactionType) && !string.IsNullOrEmpty(transactionType.Trim()))
            {
                TransactionTypeName type = (TransactionTypeName)Enum.Parse(typeof(TransactionTypeName), transactionType);
                dic.Add("TransactionTypeName", type);

            }

            //tr = _repository.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = _repository.FindTransactionsWithPaging(_theDataSource, institutionCode, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);
            //_repository.DbContext.Close(_theDataSource);
            return tr;

        }

        public string LinkCardAccountNumber(string instCode, string accountNumber, string serialNumber, string tellerCode)
        {
            string toReturn = string.Empty;
            IssuanceServiceRef.BankOneCardIssuanceClient client = new IssuanceServiceRef.BankOneCardIssuanceClient();
            toReturn = client.LinkCardAccount(serialNumber, accountNumber, instCode, tellerCode);
            ICustomerCreationLog customerCreationLog = new CustomerCreationLog();
            customerCreationLog = new CustomerCreationLogSystem().GetByAccountNumber(accountNumber);
            customerCreationLog.State = "Card Linking was successful. End of Flow. Account number:" + accountNumber + ", Card serial number is: " + serialNumber;



            if (!toReturn.StartsWith("00"))
            {
                throw new ApplicationException(toReturn.Split("-".ToCharArray())[1]);
            }
            client.Close();
            return toReturn;
        }
        public string ValidateCardAccount(string serialNumber, string tellerCode)
        {
            string toReturn = string.Empty;
            IssuanceServiceRef.BankOneCardIssuanceClient client = new IssuanceServiceRef.BankOneCardIssuanceClient();
            toReturn = client.ValidateAccount(tellerCode, serialNumber);
            if (!toReturn.StartsWith("00"))
            {
                throw new ApplicationException(toReturn);
            }
            client.Close();
            return toReturn;
        }
        public bool RegisterLafayetteCustomer(string insCode, string phoneNo, string acctNo, out string description)
        {
            bool toReturn = false;


            LafRef5.SwitchingServiceClient CL = new LafRef5.SwitchingServiceClient();
            try
            {
                toReturn = CL.RegisterMobileUser(insCode, phoneNo, acctNo, "Mobile");

                Trace.TraceInformation("Succesfully Sent Request -{0} {1} {2}", phoneNo, acctNo, toReturn);
                description = "Succesfull";

            }
            catch (Exception ex)
            {
                string errorMsg = String.Format("Error Registering Customer - {0}, Inner Message Is {1}, Inner Stack Trace is {2}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.StackTrace);
                Trace.TraceInformation(errorMsg);
                toReturn = false;
                description = ex.Message;
                throw new BankOneMobile.Core.Exceptions.CoreBankingWebServiceException(ex.Message);
            }
            finally
            {
                CL.Close();
            }
            return toReturn;

        }


        public string AutoRegisterLafayetteCustomer(string cardPan, DateTime cardExpDate, string acctNo)
        {
            string instCode = "100127";
            string response = string.Empty;
            MobileAccount mob = new MobileAccount { MobilePhone = cardPan };

            Transaction trans = new Transaction { From = acctNo, FromInstitutionCode = instCode, Date = cardExpDate, FromPhoneNumber = cardPan, InstitutionCode = instCode };
            IConfigurationTransactionType config = new ConfigurationTransactionType { TheMobileAccount = mob, TheTransaction = trans };
            try
            {
                response = ProcessISOTransaction.AutoRegisterLafayetteCustomer(trans, config);
            }
            catch (NoIssuerResponseException)
            {
                response = "Issuer Unavailable";
            }
            catch (NoSwitchResponseException)
            {
                response = "Switch Unavailable";
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error in Lafayette Auto Reg");
                Trace.TraceInformation(ex.Message);
                response = "System Error";
            }
            return response;
        }
        XElement TransformXML(XElement incompleteXML, string[] relevantXmlNodes)
        {
            XElement transformedXML = new XElement(incompleteXML.Name);

            foreach (string node in relevantXmlNodes)
            {
                try
                {
                    transformedXML.Add(new XElement(node));
                    if (incompleteXML.Element(node).HasElements)
                    {
                        transformedXML.SetElementValue(node, incompleteXML.Element(node));
                    }
                    else
                    {
                        transformedXML.SetElementValue(node, incompleteXML.Element(node).Value);
                    }
                }
                catch
                {
                    transformedXML.SetElementValue(node, string.Empty);
                }
            }
            return transformedXML;
        }
    }
}
