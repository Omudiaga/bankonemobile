﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class MobileRequestSystem
    {
         private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
         private IMobileRequestRepository _repository = SafeServiceLocator<IMobileRequestRepository>.GetService();

       public MobileRequestSystem()
       {

       }

       public MobileRequestSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }

      
        public IMobileRequest Save(IMobileRequest request)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, request);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IMobileRequest Update(IMobileRequest request)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, request);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public IMobileRequest Get(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }   

        public List<IMobileRequest> FindRequestsBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

    }
}
