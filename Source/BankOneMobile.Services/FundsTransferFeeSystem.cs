﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Services.Utility;

namespace BankOneMobile.Services
{
    public class FundsTransferFeeSystem
    {
        private string ENABLE_DISABLE_FUNDS_TRANSFER_FEE = "EnableDisableFundsTransferFee";

         private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
         private IFundsTransferFeeRepository _repository = SafeServiceLocator<IFundsTransferFeeRepository>.GetService();

       public FundsTransferFeeSystem()
       {

       }

       public FundsTransferFeeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }


       public IFundsTransferFee SaveFee(IFundsTransferFee theFee)
        {
            theFee.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theFee);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public FundsTransferFee UpdateFee(IFundsTransferFee theFee)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theFee);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return (FundsTransferFee)result;

        }

       public IFundsTransferFee GetFeeByID(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }



       public List<IFundsTransferFee> GetAllFees()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

       public List<IFundsTransferFee> GetAllFeesByInstitutionCode(string institutionCode)
       {
           var result = _repository.GetAll(_theDataSource);
           //_repository.DbContext.Close(_theDataSource);
           return result;

       }

       public List<IFundsTransferFee> FindFeesBy(IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(_theDataSource, nameValuePairs);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

       public IList<IFundsTransferFee> GetAllActiveFees(string institutionCode)
        {
            var result = _repository.GetActiveFees(_theDataSource, institutionCode);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
       public IFundsTransferFee GetByName(string institutionCode, TransactionTypeName name)
        {
            var result = _repository.GetByName(_theDataSource, institutionCode, name);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }

       public Object EnableDisable(FundsTransferFee fundsTransferFee)
       {

           fundsTransferFee.IsActive = !fundsTransferFee.IsActive;
           return this.UpdateFee(fundsTransferFee);
          // return ApprovalUtil.UpdateOrLogForApproval<FundsTransferFee>(MfbData.MFBCode, this.UpdateFee, fundsTransferFee, fundsTransferFee.TransactionTypeName.ToString(), this.RefreshFundsTransferFee, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, ENABLE_DISABLE_FUNDS_TRANSFER_FEE);
       }

       public FundsTransferFee RefreshFundsTransferFee(FundsTransferFee fundsTransferFee, PANE.Framework.NHibernateManager.Configuration.DatabaseSource dbSource)
       {
           return GetFeeByID(fundsTransferFee.ID) as FundsTransferFee;
       }
       public IFundsTransferFee GetDefault()
        {
            return new FundsTransferFee { Amount = 100 }; 
        }
    }
}
