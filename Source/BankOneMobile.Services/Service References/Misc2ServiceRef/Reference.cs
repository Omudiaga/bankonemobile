﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankOneMobile.Services.Misc2ServiceRef {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Misc2ServiceRef.IMisc2Service")]
    public interface IMisc2Service {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/AccountOfficerIsValid", ReplyAction="http://tempuri.org/IMisc2Service/AccountOfficerIsValidResponse")]
        bool AccountOfficerIsValid(string mfbCode, string staffID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/AccountOfficerName", ReplyAction="http://tempuri.org/IMisc2Service/AccountOfficerNameResponse")]
        string AccountOfficerName(string mfbCode, string staffID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/GetCurrentFinancialDate", ReplyAction="http://tempuri.org/IMisc2Service/GetCurrentFinancialDateResponse")]
        System.DateTime GetCurrentFinancialDate(string mfbCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/IncreaseSMSSentCounterByOne", ReplyAction="http://tempuri.org/IMisc2Service/IncreaseSMSSentCounterByOneResponse")]
        bool IncreaseSMSSentCounterByOne(string mfbCode, string[] accountNos);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/IncreaseSMSSentCounterByOneWithIDs", ReplyAction="http://tempuri.org/IMisc2Service/IncreaseSMSSentCounterByOneWithIDsResponse")]
        bool IncreaseSMSSentCounterByOneWithIDs(string mfbCode, long[] accountNos);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/UserRoleHasNewPendingPostingApprovals", ReplyAction="http://tempuri.org/IMisc2Service/UserRoleHasNewPendingPostingApprovalsResponse")]
        bool UserRoleHasNewPendingPostingApprovals(out string approvalPageLink, string mfbCode, System.DateTime newestCheckDate, long userRoleID, long branchID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMisc2Service/UserHasNewSuccesfulPostingApprovals", ReplyAction="http://tempuri.org/IMisc2Service/UserHasNewSuccesfulPostingApprovalsResponse")]
        bool UserHasNewSuccesfulPostingApprovals(out string approvalPageLink, string mfbCode, System.DateTime newestCheckDate, long userID, long userRoleID, long branchID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMisc2ServiceChannel : BankOneMobile.Services.Misc2ServiceRef.IMisc2Service, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class Misc2ServiceClient : System.ServiceModel.ClientBase<BankOneMobile.Services.Misc2ServiceRef.IMisc2Service>, BankOneMobile.Services.Misc2ServiceRef.IMisc2Service {
        
        public Misc2ServiceClient() {
        }
        
        public Misc2ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public Misc2ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Misc2ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Misc2ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool AccountOfficerIsValid(string mfbCode, string staffID) {
            return base.Channel.AccountOfficerIsValid(mfbCode, staffID);
        }
        
        public string AccountOfficerName(string mfbCode, string staffID) {
            return base.Channel.AccountOfficerName(mfbCode, staffID);
        }
        
        public System.DateTime GetCurrentFinancialDate(string mfbCode) {
            return base.Channel.GetCurrentFinancialDate(mfbCode);
        }
        
        public bool IncreaseSMSSentCounterByOne(string mfbCode, string[] accountNos) {
            return base.Channel.IncreaseSMSSentCounterByOne(mfbCode, accountNos);
        }
        
        public bool IncreaseSMSSentCounterByOneWithIDs(string mfbCode, long[] accountNos) {
            return base.Channel.IncreaseSMSSentCounterByOneWithIDs(mfbCode, accountNos);
        }
        
        public bool UserRoleHasNewPendingPostingApprovals(out string approvalPageLink, string mfbCode, System.DateTime newestCheckDate, long userRoleID, long branchID) {
            return base.Channel.UserRoleHasNewPendingPostingApprovals(out approvalPageLink, mfbCode, newestCheckDate, userRoleID, branchID);
        }
        
        public bool UserHasNewSuccesfulPostingApprovals(out string approvalPageLink, string mfbCode, System.DateTime newestCheckDate, long userID, long userRoleID, long branchID) {
            return base.Channel.UserHasNewSuccesfulPostingApprovals(out approvalPageLink, mfbCode, newestCheckDate, userID, userRoleID, branchID);
        }
    }
}
