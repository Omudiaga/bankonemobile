﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class LocalFundsTransferTransactionTypeSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ILocalFundsTransferTransactionTypeRepository _repository = SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService();
       public LocalFundsTransferTransactionTypeSystem()
       {

       }

       public LocalFundsTransferTransactionTypeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        public  ILocalFundsTransferTransactionType SaveLocalFundsTransferTransactionType(LocalFundsTransferTransactionType theLocalFundsTransferTransactionType)
        {
            theLocalFundsTransferTransactionType.IsActive = false;
            var repo = SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(_theDataSource);
            var result = repo.Save(_theDataSource, theLocalFundsTransferTransactionType);
            repo.DbContext.CommitTransaction(_theDataSource);
            return result;

        }
      
        public  ILocalFundsTransferTransactionType UpdateLocalFundsTransferTransactionType(LocalFundsTransferTransactionType theLocalFundsTransferTransactionType)
        {
            var repo = SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(_theDataSource);
            var result = repo.Merge(_theDataSource, theLocalFundsTransferTransactionType);
            repo.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  ILocalFundsTransferTransactionType GetLocalFundsTransferTransactionType(long id)
        {
            var result = SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService().Get(_theDataSource, id);
            //SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService().DbContext.CommitChanges(_theDataSource);
            return result;

        }

        public  List<ILocalFundsTransferTransactionType> GetAllLocalFundsTransferTransactionTypes()
        {
            var result = SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService().GetAll(_theDataSource);
            //SafeServiceLocator<ILocalFundsTransferTransactionTypeRepository>.GetService().DbContext.CommitChanges(_theDataSource);
            return result;

        }

        public  List<ILocalFundsTransferTransactionType> GetActiveLocalFundsTransferTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public  List<ILocalFundsTransferTransactionType> FindLocalFundsTransferTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            return result;
        }


        public List<LocalFundsTransferTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string agentCode, string reciever,string toInstCode, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<LocalFundsTransferTransactionType> tr = new List<LocalFundsTransferTransactionType>();

            TransactionStatus? transStatus = null;
            if (!string.IsNullOrEmpty(status))
            {
                transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
            }
            if (!string.IsNullOrEmpty(agentCode) && !string.IsNullOrEmpty(agentCode.Trim()))
            {

                dic.Add("AgentCode", agentCode);

            }
            if (!string.IsNullOrEmpty(reciever) && !string.IsNullOrEmpty(reciever.Trim()))
            {

                dic.Add("ToAccount", reciever);

            }
            if (!string.IsNullOrEmpty(toInstCode) && !string.IsNullOrEmpty(toInstCode.Trim()))
            {

                dic.Add("ToInstCode", toInstCode);

            }


            //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = _repository.FindTransactionsWithPaging(dataSource,transStatus,institutionCode, phoneNum,reciever, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;

        }
    }
}
