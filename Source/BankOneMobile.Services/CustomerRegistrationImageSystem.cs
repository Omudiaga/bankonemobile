﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class CustomerRegistrationImageSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ICustomerRegistrationImageRepository _repository = SafeServiceLocator<ICustomerRegistrationImageRepository>.GetService();
       public CustomerRegistrationImageSystem()
       {

       }

       public CustomerRegistrationImageSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
       public ICustomerRegistrationImage SaveCustomerRegistrationImage(CustomerRegistrationImage theCustomerRegistration)
        {
            //theCustomerRegistration.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theCustomerRegistration);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ICustomerRegistrationImage UpdateCustomerRegistrationImage(CustomerRegistrationImage theCustomerRegistration)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theCustomerRegistration);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ICustomerRegistrationImage GetCustomerRegistrationImage(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

       public List<ICustomerRegistrationImage> GetAllCustomerRegistrationImages()
        {
            var result = _repository.GetAll(_theDataSource);
            return result;

        }
    }
}
