﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class BalanceInquiryTransactionTypeSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IBalanceInquiryTransactionTypeRepository _repository = SafeServiceLocator<IBalanceInquiryTransactionTypeRepository>.GetService();
       public BalanceInquiryTransactionTypeSystem()
       {

       }

       public BalanceInquiryTransactionTypeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        public  IBalanceInquiryTransactionType SaveBalanceInquiryTransactionType(BalanceInquiryTransactionType theBalanceInquiryTransactionType)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            theBalanceInquiryTransactionType.IsActive = false;
            var result = _repository.Save(_theDataSource, theBalanceInquiryTransactionType);
            //_repository.DbContext.CommitChanges(_theDataSource);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  IBalanceInquiryTransactionType UpdateBalanceInquiryTransactionType(BalanceInquiryTransactionType theBalanceInquiryTransactionType)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theBalanceInquiryTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

        public  IBalanceInquiryTransactionType GetBalanceInquiryTransactionType(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

        public  List<IBalanceInquiryTransactionType> GetAllBalanceInquiryTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            return result;

        }

        public  List<IBalanceInquiryTransactionType> GetActiveBalanceInquiryTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public  List<IBalanceInquiryTransactionType> FindBalanceInquiryTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            return result;
        }
        
        
    }
}
