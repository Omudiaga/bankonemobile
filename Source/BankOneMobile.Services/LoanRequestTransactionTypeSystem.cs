﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class LoanRequestTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ILoanRequestTransactionTypeRepository _repository = SafeServiceLocator<ILoanRequestTransactionTypeRepository>.GetService();
       public LoanRequestTransactionTypeSystem()
       {

       }

       public LoanRequestTransactionTypeSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
       public ILoanRequestTransactionType SaveLoanRequestTransactionType(LoanRequestTransactionType theLoanRequestTransactionType)
        {
            theLoanRequestTransactionType.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theLoanRequestTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ILoanRequestTransactionType UpdateLoanRequestTransactionType(LoanRequestTransactionType theLoanRequestTransactionType)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theLoanRequestTransactionType);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ILoanRequestTransactionType GetLoanRequestTransactionType(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

       public List<ILoanRequestTransactionType> GetAllLoanRequestTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            return result;

        }

       public List<ILoanRequestTransactionType> GetActiveLoanRequestTransactionTypes()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

       public List<ILoanRequestTransactionType> FindLoanRequestTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            return result;
        }

       public List<ILoanRequestTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            var tr = new List<ILoanRequestTransactionType>();

            tr = _repository.FindTransactionsWithPaging(dataSource, institutionCode, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;
        }
    }
}
