﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;

namespace BankOneMobile.Services
{
    public class UssdBillingConfigurationSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IUssdBillingConfigurationRepository _repository = SafeServiceLocator<IUssdBillingConfigurationRepository>.GetService();

        public UssdBillingConfigurationSystem()
        {

        }

        public UssdBillingConfigurationSystem(IDataSource dataSource)
        {
            _theDataSource = dataSource;
        }

        public IUssdBillingConfiguration Save(UssdBillingConfiguration config)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, config);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
        }

        public IUssdBillingConfiguration Update(UssdBillingConfiguration config)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, config);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
        }

        public IUssdBillingConfiguration SaveOrUpdate(UssdBillingConfiguration config)
        {
            var result = (config.ID == 0) ? Save(config) : Update(config);
            return result;
        }

        public IUssdBillingConfiguration Get(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }

        public List<IUssdBillingConfiguration> GetAll()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }

        public List<IUssdBillingConfiguration> Search(string name, int startIndex, int maxSize, out int total)
        {
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            List<IUssdBillingConfiguration> configurations = new List<IUssdBillingConfiguration>();

            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(name.Trim()))
            {
                dictionary.Add("Name", name);
            }
            configurations = FindUssdBillingConfigurations(dictionary, startIndex, maxSize, out total);
            return configurations;
        }

        private List<IUssdBillingConfiguration> FindUssdBillingConfigurations(IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            var result = _repository.FindUssdBillingConfigurationsWithPaging(_theDataSource, propertyValuePairs, startIndex, maxSize, out total);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
    }
}
