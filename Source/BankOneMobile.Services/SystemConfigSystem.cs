﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using System.Data.SqlTypes;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using System.Configuration;

namespace BankOneMobile.Services
{
  public  class SystemConfigSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
      private ISystemConfigRepository _repository = SafeServiceLocator<ISystemConfigRepository>.GetService();
        public SystemConfigSystem()
        {

        }
        public SystemConfigSystem(IDataSource dataSorce, ISystemConfigRepository repository)
        {
            _theDataSource = dataSorce;
            _repository = repository;
        }
        public SystemConfigSystem(IDataSource dataSorce)
        {
            _theDataSource = dataSorce;
            
        }
        public SystemConfigSystem(ISystemConfigRepository repository)
        {
            
            _repository = repository;
        }

        public SystemConfig GetConfig()
        {
          return   _repository.GetConfig(_theDataSource) as SystemConfig;
        }
        public ISystemConfig UpdateSystemConfig(SystemConfig config)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, config);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

      public IKey GetPEK()
      {
          return SafeServiceLocator<IKeyRepository>.GetService().GetPEK(_theDataSource);
      }


      public string GetDecryptedPEK()
      {
          
          string zmkComp1 = BankOneMobile.Services.Configuration.ConfigurationManager.ZMKComponent1;
          string zmkComp2 = BankOneMobile.Services.Configuration.ConfigurationManager.ZMKComponent2;
          string zmkClear = ThalesSim.Core.Utility.XORHexStringsFull(zmkComp1, zmkComp2);
          
          string key = (GetPEK() as Key).ValueUnderParent; string encryptionKey = string.Empty;


          // ThalesSim.Core.Utility.ByteArrayToHexString(response.SessionKeyBytes, ref hexValue);
          //now we need to take the first 32 characters gotten from  the response and decrypt it using the the clear zmk which in itself is the xor value of the components
          //ThalesSim.Core.Utility.
          encryptionKey = ThalesSim.Core.Cryptography.TripleDES.TripleDESDecrypt(
          new ThalesSim.Core.Cryptography.HexKey(zmkClear),
          key.Substring(1,32)//se only the first 32 digits
          );
          //_repository.DbContext.Close(_theDataSource);
          string message = String.Format("Clear PEK is {0}", encryptionKey);
          PANE.ERRORLOG.ErrorLogger.Log(new Exception(encryptionKey));
          return encryptionKey;
          
      }

      public static string GetDecryptedPEKForTest(string key)
      {

          string zmkComp1 = BankOneMobile.Services.Configuration.ConfigurationManager.ZMKComponent1;
          string zmkComp2 = BankOneMobile.Services.Configuration.ConfigurationManager.ZMKComponent2;
          string zmkClear = ThalesSim.Core.Utility.XORHexStringsFull(zmkComp1, zmkComp2);



      //string key = (GetPEK() as Key).ValueUnderParent; 
          string encryptionKey = string.Empty;    // ThalesSim.Core.Utility.ByteArrayToHexString(response.SessionKeyBytes, ref hexValue);
          //now we need to take the first 32 characters gotten from  the response and decrypt it using the the clear zmk which in itself is the xor value of the components
          //ThalesSim.Core.Utility.
          encryptionKey = ThalesSim.Core.Cryptography.TripleDES.TripleDESDecrypt(
          new ThalesSim.Core.Cryptography.HexKey(zmkClear),
          key.Substring(1, 32)//se only the first 32 digits
          );
          //_repository.DbContext.Close(_theDataSource);
          string message = String.Format("Clear PEK is {0}", encryptionKey);
          PANE.ERRORLOG.ErrorLogger.Log(new Exception(encryptionKey));
          return encryptionKey;

      }


      public string GetPVK()
      {
          string toReturn = string.Empty;
          toReturn=  GetConfig().PVK;
          //_repository.DbContext.Close(_theDataSource);
          return toReturn;
      }
      public  IKey GetTMK()
      {
          IKey toReturn = null;
          toReturn =  SafeServiceLocator<IKeyRepository>.GetService().GetTMK(_theDataSource);  
          //_repository.DbContext.Close(_theDataSource);
          return toReturn;
         
      }


      public string GetDefaulParentInstitutionCode()
      {
          string toReturn = "10011";
          //TODO get the right institution
          return toReturn;
      }
    }
}
