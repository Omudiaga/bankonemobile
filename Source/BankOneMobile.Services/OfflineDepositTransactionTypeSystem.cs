﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class OfflineDepositTransactionTypeSystem
    {

        public IOfflineDepositTransactionType Save(IDataSource dataSource, OfflineDepositTransactionType offlineDeposit)
        {
            var repo = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, offlineDeposit);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IOfflineDepositTransactionType Update(IDataSource dataSource, OfflineDepositTransactionType offlineDeposit)
        {
            var repo = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, offlineDeposit);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IOfflineDepositTransactionType GetByID(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;
        }

        public List<IOfflineDepositTransactionType> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService().GetByInstitutionCode(dataSource, institutionCode);
            return result.ToList();

        }

        public List<IOfflineDepositTransactionType> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService().GetByAgentPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result.ToList();

        }

        public IOfflineDepositTransactionType GetUnsuccessfulOfflineDepositBySessionID(IDataSource dataSource, string sessionID)
        {
            var result = SafeServiceLocator<IOfflineDepositTransactionTypeRepository>.GetService().GetUnsuccessfulOfflineDepositBySessionID(dataSource, sessionID);
            return result;

        }
    }
}
