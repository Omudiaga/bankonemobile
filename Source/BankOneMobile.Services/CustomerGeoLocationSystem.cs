﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class CustomerGeoLocationSystem
    {

        public ICustomerGeoLocation Save(IDataSource dataSource, CustomerGeoLocation theCustomerGeoLocation)
        {
            var repo = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theCustomerGeoLocation);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public ICustomerGeoLocation Update(IDataSource dataSource, CustomerGeoLocation theCustomerGeoLocation)
        {
            var repo = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theCustomerGeoLocation);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public ICustomerGeoLocation GetByID(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService().Get(dataSource, id);
            return result;
        }

        public List<ICustomerGeoLocation> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService().GetByInstitutionCode(dataSource, institutionCode);
            return result.ToList();

        }

        public List<ICustomerGeoLocation> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService().GetByAgentPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result.ToList();
        }
        public ICustomerGeoLocation GetByCustomerPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = SafeServiceLocator<ICustomerGeoLocationRepository>.GetService().GetByCustomerPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result;
        }
    }
}
