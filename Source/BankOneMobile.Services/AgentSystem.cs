﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using System.Data.SqlTypes;
using BankOneMobile.Core.Helpers.Enums;
using PANE.Framework.Utility;
using BankOneMobile.Services.Utility;
using System.Web.Security;
using PANE.Framework.Functions.DTO;
using System.Diagnostics;
using BankOneMobile.Services.WorkflowServices;

namespace BankOneMobile.Services
{
    [ApprovableClass(typeof(Agent))]
  public  class AgentSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IAgentRepository _repository = SafeServiceLocator<IAgentRepository>.GetService();
        private string ADD_EDIT_AGENT = "AddEditAgents";
        private string ADD_EDIT_MOBILE_TELLERS = "AddEditMobileTellers";
        private string ENABLE_DISABLE_MOBILE_TELLERS = "EnableDisableMobileTellers";
        private string ENABLE_DISABLE_AGENT = "EnableDisableAgents";

        private string setUpMessage = "You may Download the BankOne Mobile application from http://www.mybankone.com:9090/JavaApplication/BankOneMobileClient.jar Your Verification Code is {0}";
        private string activationCodeMessage = "An account has been created for you on BankOne. Please use this  Activation Code ({0}) to activate the acct.Welcome to BankOne";
      public AgentSystem()
       {

       }

      public AgentSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
      public virtual bool CheckAgentAccountOnCoreBanking(string accountNumber,string institutionCode)
      {
          bool toReturn = false;
           
           try
           {
               using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
               {

                   toReturn = client.VerifyMobileTellerOrAgent(institutionCode, accountNumber,CoreBankingService.MobileTellerOrAgentVerificationType.MobileTellerVerification);
               }
           }
           catch
           {
               throw new CoreBankingWebServiceException("Core Banking Service is Down");
           }
           return toReturn;
      }
      
        private   Agent SaveAgent(Agent theAgent)
        {
          //  theAgent.InstitutionCode = GetInstitutionCode();
            Trace.TraceInformation("Agent Code is {0}", theAgent.InstitutionCode);
            IInstitution inst = new InstitutionSystem(_theDataSource).GetByCode(_theDataSource, theAgent.InstitutionCode);

            if (inst == null)
            {
                throw new InvalidInstitutionException(string.Format("institution Code ({0}) Has Not Been Setup on Mobile",theAgent.InstitutionCode));
            }
            Trace.TraceInformation("Gotten Institution");
            ILinkingBankAccount existingBankAcct = new MobileAccountSystem(_theDataSource).GetBankAccountByAcccountNo(theAgent.AccountNumber, theAgent.InstitutionCode);
            if (existingBankAcct != null)
            {
                throw new AlreadyExistingAccountException("Account Number already Exists");
            }
           //if(!CheckAgentAccountOnCoreBanking(theAgent.AccountNumber,theAgent.InstitutionCode))
           //{
           //     throw new InvalidAgentCodeException("Agent Account Number is Invalid on Core Banking");
           //}
            MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, theAgent.PhoneNumber) as MobileAccount;
            if (existingAcct != null&&CheckAgentByPhoneNumber(theAgent.PhoneNumber) != null)
            {
                
                    throw new AlreadyRegisterdCustomerException("This Phone Number Has already been registered to an Agent");
                
            }
         
          //if( new MobileAccountSystem().
            theAgent.IsActive = false;
            theAgent.Code = GetAgentCode();//autogenerate the Agent Code


            MobileAccount mobileAcc= existingAcct == null ? new MobileAccount() : existingAcct;// create the mobile account
            if (existingAcct == null)
            {
                mobileAcc.InstitutionCode = theAgent.InstitutionCode;// = GetInstitutionCode();
               
                mobileAcc.LastName = theAgent.LastName;
                mobileAcc.OtherNames = theAgent.OtherNames;
                mobileAcc.OpenedBy = AccountRegistrarType.Branch;
                mobileAcc.RegistrarID = 1;//TODO  set the ID of the branch of the institution
                mobileAcc.MobilePhone = theAgent.PhoneNumber;
                mobileAcc.StandardPhoneNumber = "+234" + mobileAcc.MobilePhone.TrimStart("0".ToCharArray());

                mobileAcc.IsAgentAccount = true;
                mobileAcc.DateCreated = DateTime.Now;
                mobileAcc.DateActivated = (DateTime)SqlDateTime.Null;
                mobileAcc.ActivationCode = new MobileAccountSystem(_theDataSource).GenerateActivationCode();
                mobileAcc.MobileAccountStatus = MobileAccountStatus.New; 
                Trace.TraceInformation("About to Generate Offset");
               
                    mobileAcc.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mobileAcc.MobilePhone, mobileAcc.ActivationCode);
               
                Trace.TraceInformation("Succeasfully Generated Offset {0}",mobileAcc.PIN);
               // mobileAcc.PIN = GetAgentDefaultPin();

                LinkingBankAccount linkingBankAcct = new LinkingBankAccount { TheMobileAccount = mobileAcc, CustomerID = theAgent.AddressLocation, InstitutionCode = theAgent.InstitutionCode, BankAccount = theAgent.AccountNumber, InstitutionName = theAgent.InstitutionName, TheGender = theAgent.TheGender, ProductCode = theAgent.ProductCode, ProductName = theAgent.ProductName };
                Trace.TraceInformation("Created Account");
                linkingBankAcct.InstitutionName = inst.ShortName;
                // create the agents agent account

                mobileAcc.BankAccounts = new List<ILinkingBankAccount>();
                mobileAcc.BankAccounts.Add(linkingBankAcct);
                mobileAcc.RecievingBankAccount = linkingBankAcct;

                theAgent.MobileAccount = mobileAcc;
                theAgent.TheAgentAccount = linkingBankAcct;
                Trace.TraceInformation("Finished wit this guy");
                

            }
            else
            {
                LinkingBankAccount linkingBankAcct = new LinkingBankAccount { TheMobileAccount = mobileAcc, CustomerID = theAgent.AddressLocation, InstitutionCode = theAgent.InstitutionCode, InstitutionName = theAgent.InstitutionName, ProductCode = theAgent.ProductCode, ProductName = theAgent.ProductName, BankAccount = theAgent.AccountNumber, TheGender = theAgent.TheGender };// create the agents agent account
                linkingBankAcct.InstitutionName = inst.ShortName;
                mobileAcc.IsAgentAccount = true;
                if (mobileAcc.BankAccounts == null)
                {
                    mobileAcc.BankAccounts = new List<ILinkingBankAccount>();
                }
                mobileAcc.BankAccounts.Add(linkingBankAcct);
               // existingAcct.RecievingBankAccount = linkingBankAcct;

                theAgent.MobileAccount = mobileAcc;
                theAgent.TheAgentAccount = linkingBankAcct;
                theAgent.MobileAccount.MobileAccountStatus = MobileAccountStatus.New;
               // var res = _repository.Merge(_theDataSource, theAgent);
                //_repository.DbContext.CommitChanges(_theDataSource);
                //_repository.DbContext.Close(_theDataSource);
                
                
                
            }

            
            //var result = _repository.Save(_theDataSource, theAgent);
            //_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            theAgent.MobileAccount.ActivateViaJava = theAgent.ActivateViaJava;
            theAgent.MobileAccount.ActivateViaUSSD = theAgent.ActivateViaUSSD;
            Trace.TraceInformation("About to Return the agent");
            return theAgent;
            

        }
        public Agent SaveAgentExposedForCreditClubWebService(Agent agent)
        {
            return SaveAgent(agent);
        }

      public Object Insert(Agent agent)
      {
          
          ILinkingBankAccount existingBankAcct = new MobileAccountSystem(_theDataSource).GetBankAccountByAcccountNo(agent.AccountNumber, agent.InstitutionCode);
          if (existingBankAcct != null)
          {
              throw new AlreadyExistingAccountException("Account Number already Exists");
          }
        
          MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, agent.PhoneNumber) as MobileAccount;
          if (existingAcct != null && CheckAgentByPhoneNumber(agent.PhoneNumber) != null)
          {
            Exception ex = new AlreadyRegisterdCustomerException("This Phone Number Has already been registered to an Agent"); 
                new PANE.ERRORLOG.Error().LogToFile(ex);

                throw ex;

          }
          new PANE.ERRORLOG.Error().LogToFile(new Exception("About TO SAVE aGENT "));
          agent = SaveAgent(agent);
          new PANE.ERRORLOG.Error().LogToFile(new Exception("Actually sent the SMS"));
          
         return ApprovalUtil.SaveOrLogForApproval<Agent>(MfbData.MFBCode, SaveOrUpdate, agent, string.Format("{0},{1}", agent.LastName, agent.OtherNames),agent.IsMobileTeller?ADD_EDIT_MOBILE_TELLERS: ADD_EDIT_AGENT);
         
       // SaveAgent(agent);
       //return SaveOrUpdate(agent);
      }

      public Agent RefreshAgent(Agent agent, PANE.Framework.NHibernateManager.Configuration.DatabaseSource dbSource)
      {
          return GetAgent(agent.ID) as Agent;
      }
      public Object EnableDisable(Agent agent)
      {

          agent.IsActive = agent.IsActive ? false : true;
          if (agent.IsActive)
          {
              //agent.MobileAccount.MobileAccountStatus = MobileAccountStatus.Active;
          }
          //agent.MobileAccount.MobileAccountStatus =agent.IsActive? MobileAccountStatus.Active : MobileAccountStatus.InActive;

          return ApprovalUtil.UpdateOrLogForApproval<Agent>(MfbData.MFBCode,this.UpdateAgent,agent, agent.LastName, this.RefreshAgent,PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core,agent.IsMobileTeller?ADD_EDIT_MOBILE_TELLERS: ENABLE_DISABLE_AGENT);
          //return ApprovalUtil.DeleteOrLogForApproval<Agent>(MfbData.MFBCode, this.UpdateAgent, agent, agent.LastName, ENABLE_DISABLE_AGENT);
         

      }
      public Object Update(Agent agent)
      {

         return ApprovalUtil.UpdateOrLogForApproval<Agent>(MfbData.MFBCode, this.UpdateAgent, agent, agent.LastName, this.RefreshAgent, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, ADD_EDIT_AGENT);
         // return UpdateAgent(agent);

      }

       
        [ApprovableMethod(PANE.Framework.Utility.Action.UPDATE)]
        private Agent UpdateAgent(Agent theAgent)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            MobileAccount existingAcct = _repository.GetMobileAccountByPhoneNumber(_theDataSource, theAgent.PhoneNumber) as MobileAccount;
            if (existingAcct != null && theAgent.MobileAccount.ID != existingAcct.ID)
            {
                throw new ApplicationException("Mobile Number already exists");
            }
            var result = _repository.Update(_theDataSource, theAgent);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result as Agent;
            

        }


        public IAgent GetAgent(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;
            
        }

        public IAgent GetByCode(string code)
        {
            return _repository.GetByCode(_theDataSource, code);
        }
        public List<IAgent> GetAllAgents()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public List<IAgent> GetAllAgentsByInstitutionCode(string institutionCode)
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result.Where(x => x.InstitutionCode == institutionCode).ToList();

        }
        

        public List<IAgent> GetActiveAgents()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }

        public List<IAgent> FindAgents(IDictionary<string, object> propertyValuePairs,string instCode,int startIndex,int maxSize, out int total)
        {

            var result = _repository.FindAgentsWithPaging(_theDataSource,instCode, propertyValuePairs, startIndex, maxSize, out total);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public Agent EnableDisableAgent(Agent theAgent)
        {
            if (theAgent.IsActive)
            {
                theAgent.IsActive = false;
            }
            else
            {
                theAgent.IsActive = true;
            }
            MergeAgent(theAgent);
            //UpdateAgent(theAgent);
            return theAgent;
        }
        public List<IAgent> Search(string lastname,string institutionCode, string othernames,string code, string phoneNumber,string addressLocation, string status,bool? mobileTeller,string acctNo, long branchID,int startIndex,int maxSize,out int total)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<IAgent> agents = new List<IAgent>();

            if (!string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(lastname.Trim()))
            {
                dic.Add("LastName", lastname);

            }
            if (!string.IsNullOrEmpty(acctNo) && !string.IsNullOrEmpty(acctNo.Trim()))
            {
                dic.Add("AccountNumber", acctNo);

            }

            if (!string.IsNullOrEmpty(othernames) && !string.IsNullOrEmpty(othernames.Trim()))
            {
                dic.Add("OtherNames", othernames);

            }

            if (!string.IsNullOrEmpty(phoneNumber) && !string.IsNullOrEmpty(phoneNumber.Trim()))
            {
                 dic.Add("PhoneNumber", phoneNumber);

            }
            if (!string.IsNullOrEmpty(addressLocation) && !string.IsNullOrEmpty(addressLocation.Trim()))
            {
                dic.Add("AddressLocation", addressLocation);

            }
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(code.Trim()))
            {
                dic.Add("Code", code);
            }
            if (branchID > 0)
            {
                dic.Add("BranchID", branchID);
            }
            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                bool isActive = Convert.ToBoolean(status);
                dic.Add("IsActive", isActive);

            }
            //FunctionsMembershipUser user = Membership.GetUser() as FunctionsMembershipUser;
            //if (user != null && user.UserDetails.Role.UserCategory == UserCategory.Mfb)
            //{
            //    string instCode = GetInstitutionCode();
            //    
            //}
           // string instCode = new MobileAccountSystem().GetLoggedOnUserInstCode();
            //if (!string.IsNullOrEmpty(institutionCode) && !string.IsNullOrEmpty(institutionCode.Trim()))
            //{
            //    dic.Add("InstitutionCode", institutionCode);
            //}
            if (mobileTeller.HasValue)
            {
                dic.Add("IsMobileTeller", mobileTeller.Value);
            }
            //m().FindAgent//dic,startIndex,maxSize, out  total);
         // agents =   new, out System()
          agents =   FindAgents(dic,institutionCode, startIndex, maxSize, out  total);
          return agents;

        }
        public Agent MergeAgent( Agent agent)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
             _repository.Merge(_theDataSource, agent);
             _repository.DbContext.CommitTransaction(_theDataSource);
            return agent;
        }

        public string GetAgentCode()
        {
           // Agent agent = _repository.GetLastSaveAgent();
            int number = _repository.GetAll(_theDataSource).Count;
            
            number++;
            return number.ToString().PadLeft(5, '0');
            
        }


      public IList<IAgent> GetAgentsWithoutAccounts()
      {
          IList<IAgent> theAgents = _repository.GetUnlinked(_theDataSource);
          return theAgents;
          
      }

      public void CreateAgentAccounts(List<IAgent> list)
      {
          //TODO Implement actual creation of accounts
          foreach (IAgent iagent in list)
          {
              Agent agent = iagent as Agent;

              agent.TheAgentAccount.BankAccount = GetAccount(agent);
              agent.Linked = true;
              UpdateAgent(agent);
          }
      }

      public  string GetAccount(Agent agent)
      {
          Random rand = new Random();
         return  rand.Next(900000, 9000000).ToString().PadLeft(8);
          //TODO call the right web service
      }

      public  Agent GetAgentByPhoneNumber(string phoneNumber)
      {
          //MobileAccount mob = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
          MobileAccount mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(phoneNumber);
       if (mob == null)
       {
           throw new InvalidPhoneNumberException("Invalid Phone Number");
       }
       if (!mob.IsAgentAccount)
       {
           throw new TransactionNotPermittedToNonAgentsException("No Permission for Non Agent Accounts");
       }
       else
       {
           Agent agent = _repository.GetAgentByPhoneNumber(_theDataSource,phoneNumber) as Agent;

           //_repository.DbContext.Close(_theDataSource);
           return agent;
       }


      }
     
      public IAgent CheckAgentByPhoneNumber(string phoneNumber)
      {
          MobileAccount mob = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
          if (mob == null)
          {
              return null;
          }
          
          else
          {
              Agent agent = _repository.GetAgentByPhoneNumber(_theDataSource, phoneNumber) as Agent;

              //_repository.DbContext.Close(_theDataSource);
              return agent;
          }


      }
      public string GetAgentDefaultPin()
      {
          return "0000";
      }


      public Agent GetAgentFromCoreBanking(string acctNo, string institutionCode,bool isMobileTeller)
      {
          Agent agent = new Agent(); bool validAct = false;
         // string institutionCode = new MobileAccountSystem().GetInstitutionCode();
          IList<CoreBankingService.Account> accts = null; CoreBankingService.Customer customer = null;
          CoreBankingService.Account acct = null;
          try
          {
              
              using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
              {
                  
                  try
                  {
                      validAct = client.VerifyMobileTellerOrAgent(institutionCode, acctNo,isMobileTeller?CoreBankingService.MobileTellerOrAgentVerificationType.MobileTellerVerification:CoreBankingService.MobileTellerOrAgentVerificationType.AgentVerification);
                  }
                  catch(Exception ex)
                  {
                      throw new CoreBankingWebServiceException(String.Format("Error On Core Banking -{0}", ex.Message));
                  }
                  
                  if (!validAct)
                  {
                      throw new InvalidAccountNumberException(string.Format("{0} is Invalid",isMobileTeller?"Staff ID":"Account Number"));
                  }
                  if (!isMobileTeller)
                  {
                      try
                      {
                          acct = client.GetAccountByAccountNo(institutionCode, acctNo);
                          if (acct == null)
                          {
                              throw new InvalidAccountNumberException("Invalid Account Number Entry");
                          }

                          customer = client.GetCustomer(institutionCode, acct.CustomerID);

                      }

                      catch (Exception ex)
                      {
                          new PANE.ERRORLOG.Error().LogToFile(ex);
                          throw new CoreBankingWebServiceException(String.Format("Error On Core Banking -{0}", ex.Message));
                      }
                  }
                  else
                  {
                      customer = client.GetAccountOfficer(institutionCode, acctNo);
                  }
                  
                  if (customer == null)
                  {
                       throw new InvalidCustomerIDException("Invalid Customer on Core Banking");
                  }
                 

                  agent.LastName = customer.LastName;
                  agent.OtherNames = customer.OtherNames;
                  agent.PersonalPhoneNumber = customer.PhoneNo;
                  agent.PhoneNumber = customer.PhoneNo;
                  
                  agent.AccountNumber = acctNo;
                  agent.ProductCode = acct==null?string.Empty:acct.ProductCode;
                  agent.ProductName = acct == null ? string.Empty : acct.ProductName;
                  agent.TheGender=(Gender)Enum.Parse(typeof(Gender), customer.Gender.ToString());
                  agent.AddressLocation=customer.ID;
                  agent.BranchID = customer.BranchID;
                  //agent.ProductName=acct.Pr
                  
                  

              }
          }

          catch (InvalidAccountNumberException)
          {
              throw;
          }
          catch (InvalidCustomerIDException)
          {
              throw;
          }
          catch (CoreBankingWebServiceException)
          {
              throw ;
          }
          catch
          {
              new CoreBankingWebServiceException("No Response From Core Banking Web Service");
          }
         return agent;
      }
        [ApprovableMethod(PANE.Framework.Utility.Action.INSERT)]
      private Agent SaveOrUpdate(Agent ag)
      {
          _repository.DbContext.BeginTransaction(_theDataSource);
          if (ag.IsMobileTeller)
          {
              ag.TheAgentAccount.CustomerID = ag.TheAgentAccount.BankAccount;
          }
          if (GetAgent(ag.ID)==null)
          {
              ag.Code = GetAgentCode();
              _repository.Save(_theDataSource, ag);
              
              
          }
          else
          {
              _repository.Merge(_theDataSource, ag);
              
          }

          if (ag.ActivateViaUSSD)
          {
              //Send just activation...No verification
              string code = new Random().Next(1000000000, int.MaxValue).ToString();
              string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
              ag.MobileAccount.CustomerID = encryptedCode;
              ag.MobileAccount.ActivationCode = code;

              _repository.Merge(_theDataSource, ag);

              //string appMessage = string.Format(setUpMessage, code);
              string activationMessage = string.Format(activationCodeMessage, ag.MobileAccount.ActivationCode);
              //bool sentVerification = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(appMessage, ag.PhoneNumber, ag.InstitutionCode, ag.TheAgentAccount.BankAccount);
              bool sentActivation = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(activationMessage, ag.PhoneNumber, ag.InstitutionCode, ag.TheAgentAccount.BankAccount);
              ag.Linked = sentActivation;

          }
          else if (ag.ActivateViaJava)
          {
              //Send activation and verification messages
              string code = new Random().Next(1000000000, int.MaxValue).ToString();
              string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
              ag.MobileAccount.CustomerID = encryptedCode;
              ag.MobileAccount.ProductCode = code;
              ag.MobileAccount.ActivationCode = code;
              
              _repository.Merge(_theDataSource, ag);
              
              string appMessage = string.Format(setUpMessage,code);
              //string activationMessage = string.Format(activationCodeMessage,ag.MobileAccount.ActivationCode);
              bool sentVerification = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(appMessage, ag.PhoneNumber,ag.InstitutionCode,ag.TheAgentAccount.BankAccount);
              //bool sentActivation = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(activationMessage, ag.PhoneNumber,ag.InstitutionCode,ag.TheAgentAccount.BankAccount);
              ag.Linked = sentVerification;
              
          }
          else
          {
              
          }
          _repository.DbContext.CommitTransaction(_theDataSource);

          using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
          {
              client.UpdatePhoneNoByAccountByAccountOfficerCode(ag.InstitutionCode, ag.AccountNumber,ag.PhoneNumber);

          }
          return ag;
      }

        public Agent SaveOrUpdateAgentViaWebService(Agent ag)
        {
            return SaveOrUpdate(ag);
        }





        public List<Agent> GetByInstitutionCode(string isntCode,bool isMobileTeller)
        {
           List<Agent> theAgents =   _repository.GetAgentByInstitutionCode(_theDataSource, isntCode,isMobileTeller) ;
           return theAgents;
        }

        public Agent GetAgentByCode( string mobileTellerCode)
        {
            IAgent ag = _repository.GetByCode(_theDataSource, mobileTellerCode);
            return ag as Agent;
        }
    }
}
