﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using System.Diagnostics;

namespace BankOneMobile.Services
{
   public class ThirdPartyMobileProfileSystem
    {
       private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
       private IThirdPartyMobileProfileRepository _repository = SafeServiceLocator<IThirdPartyMobileProfileRepository>.GetService();
      // var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared);

      public ThirdPartyMobileProfileSystem()
       {

       }
      public ThirdPartyMobileProfileSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
      public IThirdPartyMobileProfile Insert(IThirdPartyMobileProfile MobileProfile, out string displayMessage)
       {
           displayMessage = string.Empty;
           try
           {
               _repository.DbContext.BeginTransaction(_theDataSource);
               var result = _repository.Save(_theDataSource, MobileProfile);
               _repository.DbContext.CommitTransaction(_theDataSource);
               displayMessage = "Successfully Saved";
               return result;
           }
           catch(Exception ex)
           {
               _repository.DbContext.RollbackTransaction(_theDataSource);
               PANE.ERRORLOG.ErrorLogger.Log(ex);
               displayMessage = ex.Message;
               return null;
           }
       }
      public IThirdPartyMobileProfile EnableMobileProfile(IThirdPartyMobileProfile MobileProfile)
       {
           var MP = MobileProfile as ThirdPartyMobileProfile;
           MP.Status = "Active";
           return Update(MP);
       }
      public IThirdPartyMobileProfile DisableMobileProfile(IThirdPartyMobileProfile MobileProfile)
      {
          var MP = MobileProfile as ThirdPartyMobileProfile;
          MP.Status = "InActive";
          return Update(MP);
      }
      public IThirdPartyMobileProfile Update(IThirdPartyMobileProfile MobileProfile)
       {
           try
           {
               _repository.DbContext.BeginTransaction(_theDataSource);
               var result = _repository.Update(_theDataSource, MobileProfile);
               _repository.DbContext.CommitTransaction(_theDataSource);
               return result;
           }
           catch (Exception ex)
           {
               _repository.DbContext.RollbackTransaction(_theDataSource);
               PANE.ERRORLOG.ErrorLogger.Log(ex);
               //displayMessage = ex.Message;
               return null;
           }
       }
      public IThirdPartyMobileProfile GetByAccountNumber(string AccountNo)
      {
          return _repository.GetByAccountNumber(_theDataSource, AccountNo);
      }
      public IThirdPartyMobileProfile GetByAccountNumberAndInstitutionCode(string AccountNo, string InstitutionCode)
      {
          return _repository.GetByAccountNumberAndInstitutionCode(_theDataSource, AccountNo, InstitutionCode);
      }
      public IThirdPartyMobileProfile GetByPhoneNumber(string PhoneNo)
       {
           return _repository.GetByPhoneNumber(_theDataSource, PhoneNo);
       }
      public IList<IThirdPartyMobileProfile> GetByLastName(string LastName)
       {
           return _repository.GetByLastName(_theDataSource, LastName);
       }
      public IList<IThirdPartyMobileProfile> GetByOtherNames(string OtherName)
       {
           return _repository.GetByOtherNames(_theDataSource, OtherName);
       }
      public IList<IThirdPartyMobileProfile> GetByInstitutionCode(string InstitutionCode)
      {
          return _repository.GetByOtherNames(_theDataSource, InstitutionCode);
      }
      public IList<IThirdPartyMobileProfile> GetAllMobileProfiles(IDataSource dataSource)
      {
          return _repository.GetAllMobileProfiles(dataSource);
      }
      public IList<IThirdPartyMobileProfile> GetAllMobileProfiles()
      {
          return _repository.GetAllMobileProfiles(_theDataSource);
      }
      public IList<ThirdPartyMobileProfile> GetAllTheMobileProfiles()
      {
          return _repository.GetAllTheMobileProfiles(_theDataSource);
      }
      public IList<IThirdPartyMobileProfile> GetMobileProfilesByStatus(string status)
      {
          return _repository.GetMobileProfilesByStatus(_theDataSource, status);
      }
      public IList<IThirdPartyMobileProfile> Search(string accountNumber, string phoneNumber, string lastname, string status, string InstitutionCode, int startIndex, int maxSize, out int total)
       {
           return _repository.FindWithPaging(_theDataSource, accountNumber, phoneNumber, lastname, status, InstitutionCode, startIndex, maxSize, out total);
       }

      public class Feedback
      {
          public string AccountStatus { get; set; }
          public bool Status { get; set; }
          public string AccountNumber { get; set; }
      }

      public class AccountDetails
      {
          public string PhoneNumber { get; set; }
          public string Lastname { get; set; }
          public string OtherNames { get; set; }
          public string Address { get; set; }
          public string Gender { get; set; }
          public string DateOfBirth_Format_ddMMyyyy { get; set; }
          public string InstitutionCode { get; set; }
          public Byte[] Passport { get; set; }
      }

      public Feedback CreateAccount(AccountDetails prospectAccount)
      {
          // var requestObject = System.Web.HttpContext.Current;
          Feedback FD = new Feedback();
          try
          {
              CoreBanking.Account Account = null;
              CoreBanking.AccountBalance bal = null;
              CoreBanking.Customer cust = null;
             // BranchService.Branch branch = null;
              DateTime DOB = DateTime.ParseExact(prospectAccount.DateOfBirth_Format_ddMMyyyy, "ddMMyyyy", new System.Globalization.CultureInfo("en-GB", true));
              using (CoreBanking.SwitchingServiceClient thirdParty = new CoreBanking.SwitchingServiceClient())
              {
                  var objInfo = CoreBanking.AccountInformationSource.Others;
                  var objGender = CoreBanking.Gender.Male;
                  string Prod = System.Configuration.ConfigurationManager.AppSettings["ProductCode"];
                  string AccountOfficer = System.Configuration.ConfigurationManager.AppSettings["AccountOfficerCode"];
                  if (!(string.IsNullOrEmpty(prospectAccount.Gender)))
                  {
                      objGender = prospectAccount.Gender.ToUpper() == "MALE" ? CoreBanking.Gender.Male : BankOneMobile.Services.CoreBanking.Gender.Female;
                  }

                   Account = thirdParty.CreateAccount(
                      prospectAccount.InstitutionCode, prospectAccount.Lastname, prospectAccount.OtherNames, prospectAccount.PhoneNumber,
                      objGender, Prod, AccountOfficer, "", DOB, prospectAccount.Address, "", "", "", "", "", true, objInfo, "", prospectAccount.Passport);
                  
                  //msg = Request.CreateResponse(HttpStatusCode.Created, FD);
              }
              FD.AccountStatus = Account.Status.ToString();
              FD.AccountNumber = Account.NUBAN;
              FD.Status = FD.AccountNumber != null ? true : false;
              
              try // After the Above is successful, Now save the Mobile Profile
              {
                  ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                  ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                  string responseMessage = "";
                  MobileProfile.AccountNumber = FD.AccountNumber;
                  MobileProfile.Status = "Active";
                  MobileProfile.Gender = prospectAccount.Gender;
                  MobileProfile.PhoneNumber = prospectAccount.PhoneNumber;
                  MobileProfile.OtherName = prospectAccount.OtherNames;
                  MobileProfile.LastName = prospectAccount.Lastname;
                  MobileProfile.InstitutionCode = prospectAccount.InstitutionCode;

                  Logic.Insert(MobileProfile, out responseMessage);

              }
              catch (Exception ex)
              {
                  Trace.TraceInformation("Error Creating ThirdParty profile" + ex.Message);
                  System.Diagnostics.Trace.TraceInformation("An Error occured while saving Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                  throw;
              }


          }
          catch (Exception ex)
          {
              System.Diagnostics.Trace.TraceInformation("An Error Occurred Creating Account on CoreBanking");
              //msg = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, ex.Message);
          }

          return FD;
      }
      public Feedback EnableExistingAccountByAccountNumber(string AccountNumber, string InstitutionCode)
      {

          Feedback FD = new Feedback();
          try
          {
              CoreBanking.Account Account = null;
              CoreBanking.AccountBalance bal = null;
              CoreBanking.Customer customer = null;

              using (CoreBanking.SwitchingServiceClient thirdParty = new CoreBanking.SwitchingServiceClient())
              {

                  //Do not forget to Log for Approval
                  Account = thirdParty.GetAccountByAccountNo(InstitutionCode, AccountNumber);
                  customer = thirdParty.GetCustomer(InstitutionCode, Account.CustomerID);
              }
                  FD.AccountNumber = Account.NUBAN;
                  FD.AccountStatus = Account.Status.ToString();
                  FD.Status = FD.AccountNumber != null ? true : false;


              try // After the Above is successful, Now save the Mobile Profile
                  {
                      ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                      ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                      if (!String.IsNullOrEmpty(Account.PhoneNo))
                      {
                          string responseMessage = "";
                          MobileProfile.AccountNumber = AccountNumber;
                          MobileProfile.Status = "Pending Approval";
                          MobileProfile.Gender = customer.Gender.ToString();
                          MobileProfile.PhoneNumber = Account.PhoneNo;
                          MobileProfile.OtherName = customer.FirstName + " " + customer.OtherNames;
                          MobileProfile.LastName = customer.LastName;
                          MobileProfile.InstitutionCode = InstitutionCode;
                          Logic.Insert(MobileProfile, out responseMessage);
                      }
                      else
                      {
                          FD.AccountNumber = Account.NUBAN;
                          FD.AccountStatus = "Customer Does not have phone Number";
                          FD.Status =  false;
                          Trace.TraceInformation("An Error Occured, Customer does not have Phone Number on Core Banking");
                      }
                  
                  }
                  catch (Exception ex)
                  {
                      Trace.TraceInformation("An Error occured while saving Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
                      throw;
                  }
              
          }
          catch (Exception ex)
          {
              Trace.TraceInformation(ex.Message);
          }

          return FD;
      }

      #region Enable Account by PhoneNumber
      /**
      public Feedback EnableExistingAccountByPhoneNumber(string PhoneNumber, string InstitutionCode)
      {

          Feedback FD = new Feedback();
          try
          {
              CoreBanking.Account[] Account = null;
              CoreBanking.AccountBalance bal = null;
              CoreBanking.Customer customer = null;

              using (CoreBanking.SwitchingServiceClient thirdParty = new CoreBanking.SwitchingServiceClient())
              {
                  //Do not forget to Log for Approval
                  Account = thirdParty.GetAccountsByPhoneNo(InstitutionCode, PhoneNumber);
                  customer = thirdParty.GetCustomer(InstitutionCode, Account[0].CustomerID);
              }
                  FD.AccountNumber = Account[0].NUBAN;
                  FD.AccountStatus = Account[0].Status.ToString();
                  FD.Status = FD.AccountNumber != null ? true : false;


                  try // After the Above is successful, Now save the Mobile Profile
                  {
                      ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
                      ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
                      string responseMessage = "";
                      MobileProfile.AccountNumber = FD.AccountNumber;
                      MobileProfile.Status = "Pending Approval";
                      MobileProfile.Gender = customer.Gender.ToString();
                      MobileProfile.PhoneNumber = PhoneNumber;
                      MobileProfile.OtherName = customer.FirstName + " " + customer.OtherNames;
                      MobileProfile.LastName = customer.LastName;

                      Logic.Insert(MobileProfile, out responseMessage);

                  }
                  catch (Exception ex)
                  {
                      Trace.TraceInformation(
                          "An Error occured while saving Third Mobile Profile using Phone number but CoreBanking Account was Created, Inner Message is: " +
                          ex.Message);
                      throw;
                  }
              
          }
          catch (Exception ex)
          {
              Trace.TraceInformation(ex.Message);
          }

          return FD;
      }
**/

      #endregion
      public bool ConfirmAccountStatusOnCoreBanking(string PhoneNumber, string InstitutionCode)
      {
          var rst = false;
          var FD = new Feedback();
          try
          {
              CoreBanking.Account[] Account = null;
              CoreBanking.AccountBalance bal = null;
              CoreBanking.Customer customer = null;

              using (CoreBanking.SwitchingServiceClient thirdParty = new CoreBanking.SwitchingServiceClient())
              {
                  //Do not forget to Log for Approval
                  Account = thirdParty.GetAccountsByPhoneNo(InstitutionCode, PhoneNumber);
              }
                  FD.AccountNumber = Account[0].NUBAN;
                  FD.AccountStatus = Account[0].Status.ToString();
                  FD.Status = FD.AccountNumber != null ? true : false;

                  if (Account[0].Status.ToString() == "Active")
                  {
                      rst = true;
                  }

              
          }
          catch (Exception ex)
          {

              rst = false;
          }

          return rst;
      }

      public static bool ConfirmAccountExistOnCoreBanking(string PhoneNumber, string InstitutionCode)
      {
          var rst = false;
          var FD = new Feedback();
          try
          {
              CoreBanking.Account[] Account = null;
              CoreBanking.AccountBalance bal = null;
              CoreBanking.Customer customer = null;

              using (CoreBanking.SwitchingServiceClient thirdParty = new CoreBanking.SwitchingServiceClient())
              {
                  //Do not forget to Log for Approval
                  Account = thirdParty.GetAccountsByPhoneNo(InstitutionCode, PhoneNumber);
              }
              

              if (Account[0] != null)
              {
                  FD.AccountNumber = Account[0].NUBAN;
                  FD.AccountStatus = Account[0].Status.ToString();
                  FD.Status = FD.AccountNumber != null ? true : false;
                  rst = true;
              }


          }
          catch (Exception ex)
          {

             // rst = false;
          }

          return rst;
      }

      public  bool ConfirmAccountStatusOnMobileProfile(string AccountNo, string InstitutiionCode)
      {
          bool status = false;
          try
          {
              ThirdPartyMobileProfileSystem Logic = new ThirdPartyMobileProfileSystem();
              ThirdPartyMobileProfile MobileProfile = new ThirdPartyMobileProfile();
              var Mp = Logic.GetByAccountNumberAndInstitutionCode(AccountNo,InstitutiionCode);
              var stat = false;
              if (!(String.IsNullOrEmpty(Mp.Status)))
              {
                  if (Mp.Status == "Active")
                  {
                      status = true;
                  }
              }
          }
          catch (Exception ex)
          {
              System.Diagnostics.Trace.TraceInformation("An Error occured while verifying status of account on Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
              throw;
          }
          return status;
      }

      public bool CheckBothAccountStatus(string AccountNo, string InstitutiionCode)
      {
          var status = false;
          try
          {
              var Logic = new ThirdPartyMobileProfileSystem();
              var MobileProfile = new ThirdPartyMobileProfile();
              var Mp = Logic.GetByAccountNumberAndInstitutionCode(AccountNo,InstitutiionCode);
              var CoreB = ConfirmAccountStatusOnCoreBanking(Mp.PhoneNumber, InstitutiionCode);
              var Mobile = ConfirmAccountStatusOnMobileProfile(Mp.AccountNumber, InstitutiionCode);
              if (Mobile == true && CoreB == true)
              {
                  status = true;
              }
          }
          catch (Exception ex)
          {
              System.Diagnostics.Trace.TraceInformation("An Error occured while verifying status of account on Third Mobile Profile but CoreBanking Account was Created, Inner Message is: " + ex.Message);
              throw;
          }
          return status;
      }

    
      //public static List<IThirdPartyMobileProfile> GetAllMobileProfiles()
      //{
      //    List<IThirdPartyMobileProfile> MobileProfiles = new List<IThirdPartyMobileProfile>();

      //    //using (InstitutionServiceRef.InstitutionServiceClient theClient = new InstitutionServiceRef.InstitutionServiceClient())
      //    //{
      //    //   List<InstitutionServiceRef.Institution> institutionsObj =  theClient.GetAll();

      //    //   if (institutionsObj != null && institutionsObj.Count > 0)
      //    //   {
      //    //       foreach(var institutionObj in institutionsObj)
      //    //       {
      //    //           IThirdPartyMobileProfile theInstitution = new ThirdPartyMobileProfile();
      //    //           theInstitution.Code = institutionObj.Code;
      //    //           theInstitution.Name = institutionObj.Name;

      //    //           institutions.Add(theInstitution);
      //    //       }

      //    //   }

      //    // }

      //    return MobileProfiles;

      //}
  
    }
}
