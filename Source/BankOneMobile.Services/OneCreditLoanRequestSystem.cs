﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class OneCreditLoanRequestSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IOneCreditLoanRequestRepository _repository = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService();

        public OneCreditLoanRequestSystem()
        {

        }
        public OneCreditLoanRequestSystem(IDataSource dataSource)
        {
            _theDataSource = dataSource;
        }

        public IOneCreditLoanRequest SaveOneCreditLoanRequestSystem(IDataSource dataSource, OneCreditLoanRequest request)
        {
            var repo = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, request);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IOneCreditLoanRequest UpdateOneCreditLoanRequestSystem(IDataSource dataSource, OneCreditLoanRequest request)
        {
            var repo = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, request);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IOneCreditLoanRequest GetOneCreditLoanRequestByID(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService().Get(dataSource, id);
            return result;
        }

        public List<IOneCreditLoanRequest> GetOneCreditLoanRequestByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService().GetByInstitutionCode(dataSource, institutionCode);
            return result.ToList();

        }

        public List<IOneCreditLoanRequest> GetOneCreditLoanRequestByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = SafeServiceLocator<IOneCreditLoanRequestRepository>.GetService().GetByAgentPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result.ToList();

        }

        public List<IOneCreditLoanRequest> GetPendingLoanRequests()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x =>
                x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Pending).ToList();
            return result;
        }
    }
}
