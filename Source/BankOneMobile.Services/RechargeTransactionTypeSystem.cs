﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class RechargeTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IRechargeTransactionTypeRepository _repository = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService();
        public List<RechargeTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string merchantName, string transactionRefernce, string paymentReference, string beneficiary, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<RechargeTransactionType> tr = new List<RechargeTransactionType>();

            TransactionStatus? transStatus = null;
            if (!string.IsNullOrEmpty(status))
            {
                transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
            }
            if (!string.IsNullOrEmpty(transactionRefernce) && !string.IsNullOrEmpty(transactionRefernce.Trim()))
            {

                dic.Add("TransactionReference", transactionRefernce);

            }
            if (!string.IsNullOrEmpty(paymentReference) && !string.IsNullOrEmpty(paymentReference.Trim()))
            {

                dic.Add("PaymentReference", paymentReference);

            }
            if (!string.IsNullOrEmpty(beneficiary) && !string.IsNullOrEmpty(beneficiary.Trim()))
            {

                dic.Add("Beneficiary", beneficiary);

            }
            if (!string.IsNullOrEmpty(merchantName) && !string.IsNullOrEmpty(merchantName.Trim()))
            {

                dic.Add("MerchantName", merchantName);

            }


            //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = _repository.FindTransactionsWithPaging(dataSource, institutionCode, transStatus, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);

            // //_repository.DbContext.Close(_theDataSource);
            return tr;

        }

        public static IRechargeTransactionType SaveRechargeTransactionType(IDataSource dataSource, RechargeTransactionType theRechargeTransactionType)
        {
            theRechargeTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theRechargeTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IRechargeTransactionType UpdateRechargeTransactionType(IDataSource dataSource, RechargeTransactionType theRechargeTransactionType)
        {
            var repo = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theRechargeTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IRechargeTransactionType GetRechargeTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IRechargeTransactionType> GetAllRechargeTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IRechargeTransactionType> GetActiveRechargeTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IRechargeTransactionType> FindRechargeTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IRechargeTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }

       
    }
}
