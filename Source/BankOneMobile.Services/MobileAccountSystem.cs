﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Data.SqlTypes;
using BankOneMobile.Core.Exceptions;
using System.Net.Sockets;
using BankOneMobile.Services.Utility;
using PANE.Framework.Utility;
using System.Web.Security;
using PANE.Framework.Functions.DTO;
using System.Diagnostics;
using BankOneMobile.SwitchIntegration.ISO8583.Client.DTO;
using BankoneMobile.AdvansLafayette.Core.Implementation;

namespace BankOneMobile.Services
{
    [ApprovableClass(typeof(MobileAccount))]
  public  class MobileAccountSystem
    {
      private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
      private IMobileAccountRepository _repository = SafeServiceLocator<IMobileAccountRepository>.GetService();
      private string ADD_EDIT_MOBILEACCOUNT = "AddEditMobileAccounts";
      private string ENABLE_DISABLE_MOBILEACCOUNT = "EnableDisableMobileAccounts";
      private string HOTLIST_MOBILEACCOUNT = "HotlistMobileAccount";
      private string RESET_MOBILEACCOUNT_PIN = "ResetMobileAccountPins";
      private string RESET_MOBILEACCOUNT_PIN_TRIES = "ResetMobileAccountPinTries";
      private string setUpMessage = "You may Download the BankOne Mobile application. For Android phones go to http://goo.gl/PbaM7, For blackberry phones go to http://goo.gl/NoxKP, For java phones go to http://goo.gl/q9hmX. Your Verification Code is {0}";
      private string setUpMesageDiamondBank = "To download the BankOne Mobile application, please visit; http://goo.gl/PbaM7 for Android phones.Verification Code-{0}";
      //private string activationCodeMessage = "Your BankOne Mobile Wallet has been temporarily disabled and your pin reset. You may re-activate it with this code: [{0}]";
      private string activationCodeMessage = "Your BankOne Mobile Wallet PIN has been reset. Kindly launch the mobile application, enter this code: [{0}], and save your new mobile PIN";
      private string activationCodeMessageDiamondBank = "Your BankOne Mobile Wallet PIN has been reset. Launch the mobile application and enter this code: [{0}]. Save your new PIN. For enquires SMS BETA to 30811";
      private string newActivationCodeMessage = "A BankOne Mobile Wallet has been created for you.You may activate it with this code: [{0}].Welcome to BankOne";

        public MobileAccountSystem()
       {

       }
        public MobileAccountSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
        public  IMobileAccount SaveMobileAccount(MobileAccount theMobileAccount)
        {
            theMobileAccount.IsActive = false;
            theMobileAccount.MobileAccountStatus = MobileAccountStatus.New;
            theMobileAccount.DateCreated = DateTime.Now;
            theMobileAccount.DateActivated = (DateTime)SqlDateTime.Null;
            return SaveToDatabase(theMobileAccount);
        }
        public IMobileAccount SaveMobileAccount(MobileAccount theMobileAccount, bool isFundsTransfer)
        {
            theMobileAccount.IsActive = false;
            theMobileAccount.MobileAccountStatus = isFundsTransfer ? MobileAccountStatus.PendingFundsTransfer : MobileAccountStatus.New;
            theMobileAccount.DateCreated = DateTime.Now;
            theMobileAccount.DateActivated = (DateTime)SqlDateTime.Null;
            return SaveToDatabase(theMobileAccount);
        }
        public IMobileAccount SaveMobileAccountFromField(MobileAccount theMobileAccount)
        {
            theMobileAccount.IsActive = false;
            theMobileAccount.MobileAccountStatus =  MobileAccountStatus.Active;
            theMobileAccount.DateCreated = DateTime.Now;
            theMobileAccount.DateActivated = (DateTime)SqlDateTime.Null;

            return SaveToDatabase(theMobileAccount);

        }

        public IMobileAccount SaveToDatabase(MobileAccount theMobileAccount)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theMobileAccount);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;
        }

        public  IMobileAccount UpdateMobileAccount(MobileAccount theMobileAccount)
        {
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Update(_theDataSource, theMobileAccount);
             _repository.DbContext.CommitTransaction(_theDataSource);
            //_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            
            return result;

        }
        [ApprovableMethod(PANE.Framework.Utility.Action.UPDATE)]
        private  MobileAccount Update(MobileAccount theMobileAccount)
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("In Update"));
            if (theMobileAccount.MobileAccountStatus == MobileAccountStatus.AwaitingPinReset)
            {
                theMobileAccount = ResetAccountPIN(theMobileAccount);
                bool sent = SendInstitutionSMS(string.Format(theMobileAccount.InstitutionCode == "100040" ? activationCodeMessageDiamondBank : activationCodeMessage, theMobileAccount.ActivationCode), theMobileAccount.MobilePhone, theMobileAccount.PINOffset, theMobileAccount.ProductName);//pin offset holds the current instittions institution Code product name holds the current Institution's bank account and 
            }
            if (theMobileAccount.MobileAccountStatus == MobileAccountStatus.HotListed)
            {
                //When hotlisting a mobile account
                //Male all its linked bank accounts inactive
                //So they can be linked to other mobile accounts...
                foreach (var bankAcct in theMobileAccount.BankAccounts)
                {
                    bankAcct.BankStatus = BankAccountStatus.InActive;
                }
            }
            //_repository.DbContext.BeginTransaction(_theDataSource);
             theMobileAccount= UpdateMobileAccount(theMobileAccount) as MobileAccount;
             //_repository.DbContext.CommitChanges(_theDataSource);
             PANE.ERRORLOG.ErrorLogger.Log(new Exception("Done Updating"));
             return theMobileAccount;

        }
        public IMobileAccount UpdateMobileAccountWithoutCommitting(MobileAccount theMobileAccount)
        {
            var result = _repository.Update(_theDataSource, theMobileAccount);

            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public  IMobileAccount GetMobileAccount(long id)
        {
            
            var result = _repository.Get(_theDataSource, id);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public  List<IMobileAccount> GetAllMobileAccounts()
        {
            var result = _repository.GetAll(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public  List<IMobileAccount> GetActiveMobileAccounts()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            //_repository.DbContext.Close(_theDataSource);
            return result;

        }
        public  List<IMobileAccount> FindMobileAccounts(IDictionary<string, object> propertyValuePairs,string instId,int startIndex,int maxSize,out int total )
        {
            //var result = _repository.Search(_theDataSource, propertyValuePairs)
            Trace.TraceInformation("Inside Second Sytem");
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(" Inside Second System"));
           var result =  _repository.FindWithPaging(_theDataSource,instId, propertyValuePairs, startIndex, maxSize, out total);
           //if (_theDataSource == DataSourceFactory.GetDataSource(DataCategory.Shared))
           //{
               //_repository.DbContext.Close(_theDataSource);
           //}
            return result;
        } 
       
        //public IList<ILinkingBankAccount> GetAccountsForCustomer(string acctNumber, string phoneNumber)
        //{
        //    IList<string> acctNumbers = GetCBAccounts(acctNumber);
        //    IList<ILinkingBankAccount> toReturn = new List<ILinkingBankAccount>();
        //    if (acctNumbers.Count == 0)
        //    {
        //        throw new ApplicationException("Invalid Account Number");
        //    }
        //    foreach (string acctNo in acctNumbers)// create a linking bank account entity for the mobile account and map the accounts
        //    {
        //        LinkingBankAccount lba = new LinkingBankAccount();
                
        //        lba.IsActiveOnInstitution = true;
        //        lba.BankAccount = acctNo;
        //        toReturn.Add(lba);

        //    }
        //    return toReturn;

        //}
        public MobileAccount CreateMobileAccount(MobileAccount mob,IList<ILinkingBankAccount> lbc)
        {
           
            MobileAccount existingAcct = GetByPhoneNumber(mob.MobilePhone); bool newAccount = false;
            
            if (existingAcct != null)
            {
                existingAcct.InstitutionCode = mob.InstitutionCode;
                mob = existingAcct;
                
                
            }
            else
            {                
                newAccount = true;
                mob.IsAgentAccount = false;
                
                mob.OpenedBy = AccountRegistrarType.Branch;
               // mob.RegistrarID = GetLoggedOnUserID();
                
                mob.MobileAccountStatus = MobileAccountStatus.New;
                mob.ActivationCode = GenerateActivationCode();
                bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
                Trace.TraceInformation("About to Create -4");
                mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());
                Trace.TraceInformation("About to Create");           
            }
           // mob.MobilePhone = phoneNumber;
            
           
            //TODO save branch code
            if (mob.BankAccounts == null)
            {
                mob.BankAccounts = new List<ILinkingBankAccount>();
            }
            if (mob.SelectedBankAccounts == null)
            {
                mob.SelectedBankAccounts = new List<ILinkingBankAccount>() as IList<ILinkingBankAccount>;
            }
          foreach(LinkingBankAccount acc in lbc)
            {
                Trace.TraceInformation("About to Create -7",mob.InstitutionCode);
                IInstitution inst = new InstitutionSystem().GetByCode(_theDataSource, mob.InstitutionCode);
                Trace.TraceInformation("About to Create -8 {0}", mob.InstitutionCode);
                acc.InstitutionCode = mob.InstitutionCode;//set the right Institution Code

                acc.InstitutionName = inst.ShortName;
              ILinkingBankAccount tempAc = GetBankAccoutnsByAcctNumber(acc.BankAccount, acc.InstitutionCode);
                if (tempAc != null&&tempAc.BankStatus!=BankAccountStatus.InActive)
                {
                    throw new AlreadyRegisterdCustomerException(string.Format("The  account {0}  has already been registerd on Bank One Mobile",acc.BankAccount));
                }
               
                acc.TheMobileAccount=mob;
               
                mob.BankAccounts.Add(acc);
               
                
                //mob.LastName = acc.TheMobileAccount.LastName;
                //mob.OtherNames = acc.TheMobileAccount.OtherNames;
            }
          if (mob.RecievingBankAccount == null)
          {
              mob.RecievingBankAccount = mob.BankAccounts[0];
          }


          if (newAccount)
          {
              mob.RecievingBankAccount = mob.BankAccounts[0];
              mob.DateCreated = DateTime.Now;
              mob.DateActivated = (DateTime)SqlDateTime.Null;
              
              //SaveMobileAccount(mob);
          }
          else
          {
             // UpdateMobileAccount(mob);
          }
          
           // //_repository.DbContext.Close(_theDataSource);
            return mob;

        }
        public MobileAccount CreateMobileAccountByAgent(MobileAccount mob, IList<ILinkingBankAccount> lbc, Agent ag)
        {

            MobileAccount existingAcct = GetByPhoneNumber(mob.MobilePhone); bool newAccount = false;
            if (existingAcct != null)
            {
                mob = existingAcct;


            }
            else
            {
                newAccount = true;
                mob.IsAgentAccount = false;
                mob.OpenedBy = AccountRegistrarType.Agent;
                mob.InstitutionCode = ag.InstitutionCode;
                // mob.RegistrarID = GetLoggedOnUserID();

                mob.MobileAccountStatus = MobileAccountStatus.AwaitingApproval;
                mob.ActivationCode = GenerateActivationCode();
                bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
                mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());



            }
            // mob.MobilePhone = phoneNumber;


            //TODO save branch code
            if (mob.BankAccounts == null)
            {
                mob.BankAccounts = new List<ILinkingBankAccount>();
            }
            if (mob.SelectedBankAccounts == null)
            {

                mob.SelectedBankAccounts = new List<ILinkingBankAccount>() as IList<ILinkingBankAccount>;
            }
            //foreach (LinkingBankAccount acc in lbc)
            //{
            //    IInstitution inst = new InstitutionSystem().GetByCode(_theDataSource, mob.InstitutionCode);
            //    acc.InstitutionCode = mob.InstitutionCode;//set the right Institution Code

            //    acc.InstitutionName = inst.ShortName;
            //    if (GetBankAccoutnsByAcctNumber(acc.BankAccount, acc.InstitutionCode) != null)
            //    {
            //        throw new AlreadyRegisterdCustomerException(string.Format("The  number ={0}  has already been registerd on Bank One Mobile", acc.BankAccount));
            //    }
            //    acc.TheMobileAccount = mob;
            lbc[0].TheMobileAccount = mob;
            lbc[0].InstitutionName = ag.TheAgentAccount.InstitutionName;
            lbc[0].InstitutionCode=  ag.InstitutionCode;
            lbc[0].BankStatus = BankAccountStatus.AwaitingApproval;
            mob.RegistrarCode = ag.Code;
            mob.RegistrarID = ag.ID;

            mob.BankAccounts.Add(lbc[0]);
                //mob.LastName = acc.TheMobileAccount.LastName;
                //mob.OtherNames = acc.TheMobileAccount.OtherNames;
           // }
            if (mob.RecievingBankAccount == null)
            {
                mob.RecievingBankAccount = mob.BankAccounts[0];
            }


            if (newAccount)
            {
                mob.RecievingBankAccount = mob.BankAccounts[0];
                mob.DateCreated = DateTime.Now;
                mob.DateActivated = (DateTime)SqlDateTime.Null;

                //SaveMobileAccount(mob);
            }
            else
            {
                // UpdateMobileAccount(mob);
            }

            // //_repository.DbContext.Close(_theDataSource);
            return mob;

        }

        public MobileAccount  GetMobileAccountFromCoreBanking(string customerId, string institutionCode)
        {
            string instName = new InstitutionSystem().GetByCode(_theDataSource, institutionCode).ShortName;
            MobileAccount toReturn = new MobileAccount();
            IList<CoreBankingService.Account> accts = null; CoreBankingService.Customer customer = null;
            CoreBankingService.Account acct = null;
            try
            {

                using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
                {

                   
                    
                   
                        try
                        {
                            customer= client.GetCustomer(institutionCode, customerId);
                            if (customer == null)
                            {
                                throw new InvalidCustomerIDException("Invalid Customer ID");
                            }
                            accts = client.GetAccounts(institutionCode,customerId);
                            if (accts == null)
                            {

                                throw new InvalidCustomerIDException("No Accounts configured for this customer");
                            }

                            

                        }

                        catch (Exception ex)
                        {
                            throw new CoreBankingWebServiceException(String.Format("Error On Core Banking -{0}", ex.Message));
                        }





                       
                        toReturn.LastName = customer.LastName;
                        toReturn.OtherNames = customer.OtherNames;
                        toReturn.MobilePhone = customer.PhoneNo;
                    Gender gend =Gender.Female;
                    
                        Enum.TryParse(customer.Gender.ToString(), out gend);
                    
                    toReturn.TheGender = gend;
                    toReturn.BankAccounts = new List<ILinkingBankAccount>();
                    foreach (CoreBankingService.Account acc in accts)
                    {
                        toReturn.BankAccounts.Add(new LinkingBankAccount { BankStatus=BankAccountStatus.Active,InstitutionCode= institutionCode,CoreBankingNames= acc.Name, BankAccount = acc.Number,  Name = acc.Name, ProductCode = acc.ProductCode, ProductName=acc.ProductName, TheGender=gend, CustomerID=customerId });
                    }
                    


                }
            }

            catch (InvalidAccountNumberException)
            {
                throw;
            }
            catch (InvalidCustomerIDException)
            {
                throw;
            }
            catch (CoreBankingWebServiceException)
            {
                throw;
            }
            catch
            {
                new CoreBankingWebServiceException("No Response From Core Banking Web Service");
            }
            return toReturn;
        }
        public MobileAccount GetMobileAccountFromCoreBanking(string instCode,CoreBankingService.Account acc, CoreBankingService.Customer cust)
        {
            string instName = new InstitutionSystem().GetByCode(_theDataSource, instCode).ShortName;
            MobileAccount toReturn = new MobileAccount();
            try
            {
                    toReturn.LastName = cust.LastName;
                    toReturn.OtherNames = cust.OtherNames;
                    toReturn.MobilePhone = cust.PhoneNo;
                    Gender gend = Gender.Female;

                    Enum.TryParse(cust.Gender.ToString(), out gend);

                    toReturn.TheGender = gend;
                    toReturn.BankAccounts = new List<ILinkingBankAccount>();

                    toReturn.BankAccounts.Add(new LinkingBankAccount { CoreBankingNames = acc.Name, InstitutionCode = instCode, BankAccount = acc.Number, Name = acc.Name, ProductCode = acc.ProductCode, ProductName = acc.ProductName, TheGender = gend, CustomerID = acc.CustomerID });                                               
            }

            catch (InvalidAccountNumberException)
            {
                throw;
            }
            catch (InvalidCustomerIDException)
            {
                throw;
            }
            catch (CoreBankingWebServiceException)
            {
                throw;
            }
            catch
            {
                new CoreBankingWebServiceException("No Response From Core Banking Web Service");
            }
            return toReturn;
        }
        
        public  string GenerateActivationCode()
        {
            //TODO Create Algorithm for activation code
            return new Random().Next(1001,9999).ToString();
        }
        public IList<ILinkingBankAccount> GetCBAccounts(string customerID,MobileAccount mob)
        {
            if (mob == null)
            {
                mob = new MobileAccount();
            }
           
              
              CoreBankingService.Customer cust = null;
              IList<CoreBankingService.Account> accts = null;
              try
              {
                  using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
                  {
                      cust = client.GetCustomer(mob.InstitutionCode, customerID);
                      accts = client.GetAccounts(mob.InstitutionCode, customerID);
                  }
              }
              catch
              {
                  throw new CoreBankingWebServiceException("Cannot Connect to Core Banking");
              }
                  if (cust == null || accts == null)
                  {
                      throw new InvalidCustomerIDException("Invalid Customer ID");
                  }
                  else if (accts.Count == 0)
                  {
                      throw new InvalidCustomerIDException("This Customer has no Accounts on Core Banking");
                  }
             
              mob.LastName = cust.LastName;
              mob.OtherNames = cust.OtherNames;
              mob.TheGender = (Gender) Enum.Parse(typeof(Gender), cust.Gender.ToString());
              mob.CustomerID = customerID;
              mob.MobilePhone=cust.PhoneNo;
              
              
             
            
            IList<ILinkingBankAccount> toReturn = new List<ILinkingBankAccount>();
            foreach (CoreBankingService.Account acc in accts)
            {
            
            LinkingBankAccount lbc = new LinkingBankAccount();
                
            lbc.BankAccount = acc.Number;
            lbc.InstitutionCode = mob.InstitutionCode;
            lbc.TheMobileAccount = mob;
            lbc.CustomerID = customerID;
                //lbc.ProductName= cust.
            //lbc.ProductCode;
            toReturn.Add(lbc);
            
            }
            return toReturn;

        }
      //[ApprovableMethod(PANE.Framework.Utility.Action.UPDATE)]
      //  private  MobileAccount EnableDisableMobileAccount(MobileAccount mobile)
      //  {
      //  //    if (mobile.IsActive)
      //  //    {
      //  //        mobile.IsActive = false;
      //  //    }
      //  //    else
      //  //    {
      //  //        mobile.IsActive = true;
      //  //    }

      //      //if (mobile.MobileAccountStatus==MobileAccountStatus.Active)
      //      //{
      //      //    mobile.MobileAccountStatus= MobileAccountStatus.InActive;
      //      //}
      //      //else if (mobile.MobileAccountStatus == MobileAccountStatus.InActive)
      //      //{
      //      //    mobile.MobileAccountStatus = MobileAccountStatus.Active;
      //      //}
      //      //else if (mobile.MobileAccountStatus == MobileAccountStatus.HotListed)
      //      //{
      //      //    mobile.MobileAccountStatus = MobileAccountStatus.Active;
      //      //}
      //      //else
      //      //{
      //      //    throw new Exception("Account Status Must be change from the user's Mobile Device");
      //      //}
      //      MobileAccount existingAcct = _repository.GetByPhoneNumber(_theDataSource,mobile.MobilePhone) as MobileAccount;
      //      if (existingAcct != null && existingAcct.ID != mobile.ID)
      //      {
      //          throw new ApplicationException("Mobile Number already exists");
      //      }
      //      else  if (existingAcct == null )
      //      {
      //          throw new ApplicationException("Invalid Mobile Account Selected");
      //      }
      //      UpdateMobileAccount(mobile);

      //      return mobile;
      //  }
      public List<IMobileAccount> Search(string phoneNumber,string institutionCode, string isAgentAccount, string lastName,string otherNames, string  registrarCode, string status, string gender,int startIndex, int maxSize, out int total)
        {
           // BankOneMobile.Services.HSMCenter.ResetAllPins();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<IMobileAccount> mob = new List<IMobileAccount>();

            if (!string.IsNullOrEmpty(phoneNumber) && !string.IsNullOrEmpty(phoneNumber.Trim()))
            {
                dic.Add("MobilePhone", phoneNumber);

            }
            if (!string.IsNullOrEmpty(registrarCode) && !string.IsNullOrEmpty(registrarCode.Trim()))
            {
                dic.Add("RegistrarCode", registrarCode);

            }
            if (!string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(lastName.Trim()))
            {
                dic.Add("LastName", lastName);

            }
            if (!string.IsNullOrEmpty(otherNames) && !string.IsNullOrEmpty(otherNames.Trim()))
            {
                dic.Add("OtherNames", otherNames);

            }

           
            
            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                MobileAccountStatus stat =(MobileAccountStatus) Enum.Parse(typeof(MobileAccountStatus), status);
                dic.Add("MobileAccountStatus", stat);

            }
            if (!string.IsNullOrEmpty(gender) && !string.IsNullOrEmpty(gender.Trim()))
            {
                Gender gend = (Gender)Enum.Parse(typeof(Gender), gender);
                dic.Add("TheGender", gend);

            }
            if (!string.IsNullOrEmpty(isAgentAccount) && !string.IsNullOrEmpty(isAgentAccount.Trim()))
            {
                bool isAgent = Convert.ToBoolean(isAgentAccount);
                dic.Add("IsAgentAccount", isAgent);

            }
             //instCode = GetLoggedOnUserInstCode();
            Trace.TraceInformation("Inside Sytem");
            mob =  FindMobileAccounts(dic,institutionCode,startIndex,maxSize,out total);
            //if (_theDataSource == DataSourceFactory.GetDataSource(DataCategory.Shared))
            //{


                //_repository.DbContext.Close(_theDataSource);
            //}

            return mob;

        }
        
        public bool EnableSelectedAcounts(List<IMobileAccount> mobileAccounts)
        {
            if (mobileAccounts.Count == 0)
            {
                return false;
            }
            bool toReturn = false;
            
            try
            {
                foreach (MobileAccount mob in mobileAccounts)
                {
                    mob.MobileAccountStatus = MobileAccountStatus.Active;
                    UpdateMobileAccount(mob);
                    
                }
                toReturn = true;
            }
            catch
            {
            }
            ////_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return toReturn;

        }
        public bool DisableSelectedAcounts(List<IMobileAccount> mobileAccounts)
        {
            //List<MobileAccount> mbA = mobileAccounts.Cast<MobileAccount>().ToList();
            //if (mbA.Count == 0)
            //{
            //    return false;
            //}
            bool toReturn = false;
            try
            {
                foreach (MobileAccount mob in mobileAccounts)
                {
                    mob.MobileAccountStatus = MobileAccountStatus.InActive;
                    UpdateMobileAccount(mob);
                    ////_repository.DbContext.CommitChanges(_theDataSource);
                    
                }
                toReturn = true;
            }
            catch
            {
                
            }
            ////_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            return toReturn;
        }
        
        private string ResetPIN(MobileAccount mob)
        {
           
            mob.ActivationCode = GenerateActivationCode();
            //todo do reset PIN here
            UpdateMobileAccount(mob);
            //_repository.DbContext.Close(_theDataSource);
            return mob.ActivationCode;
        }
        public MobileAccount ResetAccountPIN(MobileAccount mob)
        {
            bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
            mob.ActivationCode = GenerateActivationCode();// Change to Send SMS
            mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
            mob.MobileAccountStatus = MobileAccountStatus.New;
            UpdateMobileAccount(mob);
            //_repository.DbContext.Close(_theDataSource);

            return mob;
        }
        public MobileAccount GetByPhoneNumber(string phoneNumber)
        {
            string message = String.Format("Inside System with Phone Number {0} ", phoneNumber);            
            //new PANE.ERRORLOG.Error().LogToFile(new Exception(message));
            MobileAccount mob  = _repository.GetByPhoneNumber(_theDataSource, phoneNumber) as MobileAccount;
            //new PANE.ERRORLOG.Error().LogToFile(new Exception("Out of System"));
            ////_repository.DbContext.Close(_theDataSource);
            //new PANE.ERRORLOG.Error().LogToFile(new Exception("About to Return Mob"));
            return mob;

        }

        public List<IMobileAccount> GetMobileAccountsByPhoneNumber(string phoneNumber)
        {
            IList<IMobileAccount> mob = _repository.GetMobileAccountsByPhoneNumber(_theDataSource, phoneNumber);
            return mob.ToList();

        }

        public MobileAccount GetByPhoneNumberRawSql(string phoneNumber)
        {
            MobileAccount mob = _repository.GetByPhoneNumberByRawSql(_theDataSource, phoneNumber) as MobileAccount;
            return mob;
        }

        public MobileAccount GetByPhoneNumber(string phoneNumber, string institutionCode)
        {
            MobileAccount mob = _repository.GetByPhoneNumber(_theDataSource, phoneNumber) as MobileAccount;
            if (mob != null && mob.BankAccounts != null)
            {
                //Does the mobile account have any linking bank account with this institution?
                if (!mob.BankAccounts.Any(s => s.InstitutionCode == institutionCode))
                {
                    return null;
                }
            }
            return mob;
        }

        public IMobileAccount GetMobileAccountByPhoneNumber(string institutionCode, string phoneNumber)
        {
            IMobileAccount mob = _repository.GetByPhoneNumber(_theDataSource, institutionCode, phoneNumber);
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            return mob;
        }


        public MobileAccount GetByStandardPhoneNumber(string standardPhoneNumber)
        {             
            MobileAccount mob = _repository.GetByStandardPhoneNumber(_theDataSource, standardPhoneNumber) as MobileAccount;
            ////_repository.DbContext.Close(_theDataSource);
            return mob;

        } 
        public MobileAccount GetByAccountNumber(string accountNo,string instCode)
        {
            ILinkingBankAccount lbc = _repository.GetByAccountNumber(_theDataSource, accountNo, instCode) as ILinkingBankAccount;
            MobileAccount mob = null;
            if (lbc != null)
            {

                mob =  lbc.TheMobileAccount as MobileAccount;
            }
            else
            {
                return null;
            }
            //_repository.DbContext.Close(_theDataSource);
            return mob;

        }
        //public MobileAccount GetByAccountNumber(string accountNo)
        //{
        //    ILinkingBankAccount lbc =  _repository.GetByAccountNumber(_theDataSource, accountNo) as ILinkingBankAccount;
        //    if (lbc != null)
        //    {
        //        //_repository.DbContext.Close(_theDataSource);
        //        return lbc.TheMobileAccount as MobileAccount;
        //    }
        //    else
        //    {
        //        //_repository.DbContext.Close(_theDataSource);
        //        return null;
        //    }

        //}
        public LinkingBankAccount GetBankAccountByAcccountNo(string accountNo, string instCode)
        {
            //Trace.TraceInformation("About to check the account type {0}", accountNo);            
            ILinkingBankAccount lbc = _repository.GetByAccountNumber(_theDataSource, accountNo, instCode) as ILinkingBankAccount;
            //_repository.DbContext.Close(_theDataSource);
            return lbc as LinkingBankAccount;

        }
        public LinkingBankAccount GetBankAccountByAcccountNoForAuthorization(string accountNo, string instCode)
        {
            //Trace.TraceInformation("About to check the account type {0}", accountNo);            
            ILinkingBankAccount lbc = _repository.GetByAccountNumberForAuthorization(_theDataSource, accountNo, instCode) as ILinkingBankAccount;
            //_repository.DbContext.Close(_theDataSource);
            return lbc as LinkingBankAccount;

        }
        //public LinkingBankAccount GetBankAccountByAcccountNo(string accountNo)
        //{
            
        //    ILinkingBankAccount lbc = _repository.GetByAccountNumber(_theDataSource, accountNo) as ILinkingBankAccount;
            
        //    //_repository.DbContext.Close(_theDataSource);
        //    return lbc as LinkingBankAccount;

        //}
        public bool LinkAccounts(string phoneNumber,string activationCode)
        {
            IList<LinkingBankAccount> tempList = new List<LinkingBankAccount>();
            bool toReturn = false;
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            else if (mob.ActivationCode != activationCode)
            {
                throw new InvalidActivationCodeException("Activation Code is Invalid");
            }
            if (mob.BankAccounts == null)
            {
                mob.BankAccounts = new List<ILinkingBankAccount>();
            }
            if (mob.SelectedBankAccounts != null)
            {
                foreach (LinkingBankAccount lbc in mob.SelectedBankAccounts)
                {
                    tempList.Add(lbc);
                }
                mob.SelectedBankAccounts.Clear();
            }
            foreach (LinkingBankAccount lbc in tempList)
            {
                mob.BankAccounts.Add(lbc);
            }
            //_repository.DbContext.BeginTransaction(_theDataSource);
            UpdateMobileAccount(mob);
            //_repository.DbContext.CommitTransaction(_theDataSource);
            ////_repository.DbContext.Close(_theDataSource);
            return toReturn;
        }
        public bool SetRecievingAccount(string phoneNumber, string accountNumber)
        {
            bool toReturn = false;
          
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            else if (mob.BankAccounts != null && !mob.BankAccounts.Select(x => x.BankAccount).Contains(accountNumber))
            {
                throw new InvalidAccountNumberException("This account is not tied to this Mobile Account");
            }
            else if(mob.BankAccounts!=null)
            {
                mob.RecievingBankAccount = mob.BankAccounts.Where(x => x.BankAccount == accountNumber).FirstOrDefault();
                if (mob.RecievingBankAccount==null)
                {
                    throw new InvalidAccountNumberException("This account is not tied to this Mobile Account");
                }
            }
            //_repository.DbContext.BeginTransaction(_theDataSource);            
            UpdateMobileAccount(mob);
            //_repository.DbContext.CommitTransaction(_theDataSource);
            toReturn = true;

            return toReturn;
        }
        public MobileAccount GetByActivationCode(string activationCode)
        {
            MobileAccount mob = _repository.GetByActivationCode(_theDataSource, activationCode) as MobileAccount;
            ////_repository.DbContext.Close(_theDataSource);
            return mob;

        }
        public List<ILinkingBankAccount> GetAccountsForLinking(string phoneNumber)
        {
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            if(mob ==null)
            {
                //_repository.DbContext.Close(_theDataSource);
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            else if (mob.SelectedBankAccounts == null || mob.SelectedBankAccounts.Count == 0)
            {
                //_repository.DbContext.Close(_theDataSource);
                throw new ApplicationException("No Accounts Awaiting Linking");
            }
            else
            {
                //_repository.DbContext.Close(_theDataSource);
                return mob.SelectedBankAccounts.ToList();
            }
        }
        public List<ILinkingBankAccount> GetAccountsForLinkingUsingActivatioNCode(string activationCode)
        {
            MobileAccount mob = GetByPhoneNumber(activationCode);
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidActivationCodeException("Invalid Activation Code");
            }
            else if (mob.SelectedBankAccounts == null || mob.SelectedBankAccounts.Count == 0)
            {
                throw new ApplicationException("No Accounts Awaiting Linking");
            }
            else
            {
                
                return mob.SelectedBankAccounts.ToList();
            }
        }
        public List<ILinkingBankAccount> GetLinkedAccounts(string phoneNumber)
        {
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            else if (mob.BankAccounts == null || mob.BankAccounts.Count == 0)
            {
                throw new ApplicationException("No Linked Accounts");
            }
            else
            {
                return mob.BankAccounts.ToList();
            }
        }
        public LinkingBankAccount GetRecievingAccount(string phoneNumber)
        {
            MobileAccount mob =  GetByPhoneNumber(phoneNumber);
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            return mob.RecievingBankAccount as LinkingBankAccount;
        }

        public LinkingBankAccount GetRecievingAccountByInstitution(string institutionCode, string phoneNumber)
        {
            MobileAccount mob = GetMobileAccountByPhoneNumber(institutionCode, phoneNumber) as MobileAccount;
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            return mob.RecievingBankAccount as LinkingBankAccount;
        }
        public MobileAccount GetMobileAccountByPhoneNumber(string phoneNumber)
        {
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            //_repository.DbContext.Close(_theDataSource);
            if (mob == null)
            {
                throw new InvalidPhoneNumberException("Invalid Phone Number");
            }
            return mob;
        }
        public ILinkingBankAccount GetBankAccoutnsByAcctNumber(string acctNo, string institutionCode)
        {
            var result = _repository.GetByAccountNumber(_theDataSource,acctNo,institutionCode);
            //_repository.DbContext.Close(_theDataSource);
            return result;
        }
        public  MobileAccount CreateMobileAccountFromAgent(string lastName,string firstName, string phoneNumber, string  gender,Product prod,Agent agent,bool isFundsTransfer)
        {
            if(GetByPhoneNumber(phoneNumber)!=null)
            {
                throw new AlreadyRegisterdCustomerException("Phone Number Already registered");
            }
            
            string delimiter = ";";
           

            //if (GetBankAccountByAcccountNo(coreBankingAcct.DisplayMessage, agent.InstitutionCode) != null)
            //{
            //    throw new InvalidAccountNumberException("Account Number Already Exists");
            //}

            MobileAccount mob = new MobileAccount { LastName = lastName, OtherNames = firstName, DateCreated = DateTime.Now,   MobilePhone = phoneNumber, IsAgentAccount = false, OpenedBy = AccountRegistrarType.Agent };
            mob.RegistrarID = agent.ID;
            mob.RegistrarCode = agent.Code;
            mob.RegistrarName = string.Format("{0},{1}",agent.LastName,agent.OtherNames);
            
           
            
            mob.MobileAccountStatus = isFundsTransfer ? MobileAccountStatus.PendingFundsTransfer : MobileAccountStatus.New;
            bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);

            mob.ActivationCode = GenerateActivationCode();
            mob.PIN  = demoMode?mob.ActivationCode:new  HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone,mob.ActivationCode);
            
            //string tempCode = new AppZone.HsmInterface.ThalesHsm().PinGenerator().GenerateRandomPin(mob.MobilePhone.PadRight(12,'0'), 4).EncryptedPin;//Generate encryptedPIN
            //mob.PINOffset = GeneratePINOffsetOnHSM(mob.MobilePhone,   GenerateActivationCode());
           // mob.PIN = mob.ActivationCode;
           //TODO set activation code as PIN
            mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());
            
            mob.InstitutionCode = agent.InstitutionCode;
            
            MobileAccount coreBankingAcct = CreateAccountOnCoreBanking(lastName, firstName, phoneNumber, gender, agent, prod.Code); // Do Actual Core Banking Web Service Call
            string instCode = new InstitutionSystem().GetByCode(_theDataSource,agent.InstitutionCode).ShortName;
            LinkingBankAccount acc = new LinkingBankAccount { BankAccount = coreBankingAcct.DisplayMessage, TheProduct = prod, InstitutionCode = agent.InstitutionCode, TheMobileAccount = mob, Activated = false, CustomerID = coreBankingAcct.CustomerID, InstitutionName = instCode, TheGender = coreBankingAcct.TheGender };

            if (GetByAccountNumber(acc.BankAccount, agent.InstitutionCode) != null)
            {
                throw new AlreadyExistingAccountException("Accounts Already Exist");
            }
            mob.SelectedBankAccounts = new List<ILinkingBankAccount>();
            mob.SelectedBankAccounts.Add(acc);
            mob.RecievingBankAccount = acc;
            mob.TheGender =(Gender) Enum.Parse(typeof(Gender),gender) ;
            mob.CustomerID = coreBankingAcct.CustomerID;
            acc.ProductCode = prod.Code;
            acc.ProductName = prod.Name;
            mob.ReceivingAccountNumber = mob.RecievingBankAccount.BankAccount;
            _repository.DbContext.BeginTransaction(_theDataSource);
            SaveMobileAccount(mob,isFundsTransfer);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return mob;
        }

        public MobileAccount CreateEnhancedMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender,BankOneMobile.Core.Implementations.Product prod, Agent agent, string startPackNo, string idNo, string nokPhone, string nokName, string address, string placeofBirth,DateTime DOB,string referalName,string referalNo,bool hasSufInfo,string accountSource,string otherAccountSource, byte[] passport)
        {
            string instCode=string.Empty;
            var accountCreationLogSystem = new CustomerCreationLogSystem(_theDataSource);
            var institutionName=agent.InstitutionName;
            if(agent!=null)
            {
                institutionName=agent.InstitutionName;

            }

            ICustomerCreationLog creationLog=new CustomerCreationLog();
            try
            {
                 creationLog = accountCreationLogSystem.Save(new CustomerCreationLog()
                {
                    CustomerName = firstName + " " + lastName,
                    Institution = institutionName,
                    State = "About to create Customer",
                    PhoneNumber = phoneNumber,

                });
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(ex.Message);
            }
            try
            {
                if (GetByPhoneNumber(phoneNumber) != null)
                {
                    //check is the phone number is already registered for an account in this institution
                    var mob = GetByPhoneNumber(phoneNumber, agent.InstitutionCode);
                    if (mob == null)
                    {
                        //Does not exist for this institution..So link the new bank account to the existing mobile account
                        creationLog.State = "About to call Create Account Number. Calling CoreBanking";
                        try
                        {
                            accountCreationLogSystem.Update(creationLog);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.TraceError(ex.Message);
                        }
                        // Do Actual Core Banking Web Service Call
                        MobileAccount coreBankingAcct = CreateEnhancedAccountOnCoreBanking(lastName, firstName, phoneNumber, gender,
                            agent, prod.Code, placeofBirth, DOB, idNo, nokPhone, nokName, address, referalName, referalNo,
                            hasSufInfo, accountSource, otherAccountSource, passport);


                        creationLog.State = "Customer account was successfully created. Account number:" + coreBankingAcct.NUBAN + ", Card serial number is: " + agent.Code;
                        creationLog.AccountNumber = coreBankingAcct.NUBAN;

                        try
                        {
                            accountCreationLogSystem.Update(creationLog);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.TraceError(ex.Message);
                        }
                        LinkingBankAccount acc = new LinkingBankAccount
                        {
                            TheProduct = prod,
                            InstitutionCode = agent.InstitutionCode,
                            TheMobileAccount = mob,
                            Activated = false,
                            CustomerID = coreBankingAcct.CustomerID,
                            InstitutionName = instCode,
                            TheGender = coreBankingAcct.TheGender,
                            //Ensure you set institution code before doing this
                            BankAccount = coreBankingAcct.DisplayMessage,
                            //NUBAN = mob.NUBAN,
                            Passport = passport
                        };

                        if (GetByAccountNumber(acc.BankAccount, agent.InstitutionCode) != null)
                        {
                            throw new AlreadyExistingAccountException("Account Already Exists");
                        }

                        creationLog.State = "About to UpdateMobileAccount(Link new bank account). Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;
                        try
                        {
                            accountCreationLogSystem.Update(creationLog);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.TraceError(ex.Message);
                        }
                        mob = CreateMobileAccountByAgent(mob, new List<ILinkingBankAccount> { acc }, agent);
                        mob.MobileAccountStatus = MobileAccountStatus.Active;
                        _repository.DbContext.BeginTransaction(_theDataSource);
                        _repository.Merge(_theDataSource,  mob);
                        _repository.DbContext.CommitTransaction(_theDataSource);
                        creationLog.State = "Mobile Account Creation was successful. Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;
                        try
                        {
                            accountCreationLogSystem.Update(creationLog);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.TraceError(ex.Message);
                        }
                        return mob;
                    }
                    else
                    {
                        //Already exists
                        throw new AlreadyRegisterdCustomerException("Phone Number Already registered");
                    }
                }
                else
                {
                    string delimiter = ";";
                    MobileAccount mob = new MobileAccount { LastName = lastName, OtherNames = firstName, DateCreated = DateTime.Now, MobilePhone = phoneNumber, IsAgentAccount = false, OpenedBy = AccountRegistrarType.Agent };
                    mob.RegistrarID = agent.ID;
                    mob.RegistrarCode = agent.Code;
                    mob.RegistrarName = string.Format("{0},{1}", agent.LastName, agent.OtherNames);
                    creationLog.State = "About to get institution by code";
                    accountCreationLogSystem.Update(creationLog);

                    instCode = new InstitutionSystem().GetByCode(_theDataSource, agent.InstitutionCode).ShortName;

                    // mob.MobileAccountStatus = false ? MobileAccountStatus.PendingFundsTransfer : MobileAccountStatus.New;
                    mob.MobileAccountStatus = MobileAccountStatus.Active;
                    bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);



                    mob.ActivationCode = GenerateActivationCode();
                    try
                    {
                        mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                    }
                    catch
                    {
                        throw new NoHSMResponseException("No Response From HSM");
                    }

                    //string tempCode = new AppZone.HsmInterface.ThalesHsm().PinGenerator().GenerateRandomPin(mob.MobilePhone.PadRight(12,'0'), 4).EncryptedPin;//Generate encryptedPIN
                    //mob.PINOffset = GeneratePINOffsetOnHSM(mob.MobilePhone,   GenerateActivationCode());
                    // mob.PIN = mob.ActivationCode;
                    //TODO set activation code as PIN
                    mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());

                    mob.InstitutionCode = agent.InstitutionCode;

                    creationLog.State = "About to call Create Account Number. Calling CoreBanking";

                    try
                    {
                        accountCreationLogSystem.Update(creationLog);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError(ex.Message);
                    }
                    // Do Actual Core Banking Web Service Call
                    //Todo: Add passport to core banking web service
                    MobileAccount coreBankingAcct = CreateEnhancedAccountOnCoreBanking(lastName, firstName, phoneNumber, gender,
                        agent, prod.Code, placeofBirth, DOB, idNo, nokPhone, nokName, address, referalName, referalNo,
                        hasSufInfo, accountSource, otherAccountSource, passport);


                    creationLog.State = "Customer account was successfully created. Account number:" + coreBankingAcct.NUBAN + ", Card serial number is: " + agent.Code;
                    creationLog.AccountNumber = coreBankingAcct.NUBAN;

                    try
                    {
                        accountCreationLogSystem.Update(creationLog);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError(ex.Message);
                    }
                    LinkingBankAccount acc = new LinkingBankAccount
                    {
                        TheProduct = prod,
                        InstitutionCode = agent.InstitutionCode,
                        TheMobileAccount = mob,
                        Activated = false,
                        CustomerID = coreBankingAcct.CustomerID,
                        InstitutionName = instCode,
                        TheGender = coreBankingAcct.TheGender,
                        //Ensure you set institution code before doing this
                        BankAccount = coreBankingAcct.DisplayMessage,
                        //NUBAN = mob.NUBAN,
                        Passport = passport
                    };

                    if (GetByAccountNumber(acc.BankAccount, agent.InstitutionCode) != null)
                    {
                        throw new AlreadyExistingAccountException("Account Already Exists");
                    }

                    creationLog.State = "About to call SaveMobileAccountFromField. Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;
                    try
                    {
                        accountCreationLogSystem.Update(creationLog);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError(ex.Message);
                    }

                    mob.SelectedBankAccounts = new List<ILinkingBankAccount>();
                    mob.SelectedBankAccounts.Add(acc);
                    mob.RecievingBankAccount = acc;
                    mob.TheGender = (Gender)Enum.Parse(typeof(Gender), gender);
                    mob.CustomerID = coreBankingAcct.CustomerID;
                    acc.ProductCode = prod.Code;
                    acc.ProductName = prod.Name;
                    mob.ReceivingAccountNumber = mob.RecievingBankAccount.BankAccount;
                    _repository.DbContext.BeginTransaction(_theDataSource);
                    SaveMobileAccountFromField(mob);
                    //SaveMobileAccount(mob, false);
                    _repository.DbContext.CommitTransaction(_theDataSource);
                    creationLog.State = "Mobile Account Creation was successful. Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;

                    try
                    {
                        accountCreationLogSystem.Update(creationLog);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError(ex.Message);
                    }

                    return mob;
                }
            }
            catch (AlreadyExistingAccountException ex)
            {
                throw ex;
            }
            catch( AlreadyRegisterdCustomerException ex )
            {
                System.Diagnostics.Trace.TraceError("Caught an error during account creation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Phone number: {0}", phoneNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);

                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    {
                        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    }
                    string body = string.Format("{0}\nPhone number:{1}\n{2}\n{3}", ex.Message, phoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    client.SendEmail(string.IsNullOrEmpty(instCode) ? "BankOneMobile" : instCode, "donotreply@mybankone.com",
                        emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }

                throw ex;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during account creation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Mobile Account: {0} Phone number: {1}", agent.PhoneNumber, phoneNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    {
                        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    }
                    string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, phoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    client.SendEmail(string.IsNullOrEmpty(instCode) ? "BankOneMobile" : instCode, "donotreply@mybankone.com", 
                        emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                //return null;
                throw ex;
            }
        }
        public MobileAccount CreateLafayetteMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender, string prod, string agentCode, string startPackNo, string idNo, string nokPhone, string nokName, string address, string placeofBirth, DateTime DOB, string referalName, string referalNo, bool hasSufInfo, string accountSource, string otherAccountSource, byte[] passport,string country,string state,string territory,string district,DateTime idExpDate,DateTime idIssuedDate)
        {

            Agent agent = new AgentSystem(_theDataSource).GetAgentByCode(agentCode);
            string msg = string.Format("Inside Create Laf Acc from Agent, Product is  Code:{0}", prod);
            Trace.TraceInformation(msg);
            string instCode = string.Empty;
            var accountCreationLogSystem = new CustomerCreationLogSystem(_theDataSource);
            var institutionName = string.Empty;// agent.InstitutionName;
            if (agent != null)
            {
                institutionName = agent.InstitutionName;
                Trace.TraceInformation(" Inside Create Laf Acc From Agent. Agent is Not Null -{0}", agent.LastName);

            }

            else
            {
                Trace.TraceInformation("Agent is Null");

            }
            ICustomerCreationLog creationLog = new CustomerCreationLog();
            try
            {
                creationLog = accountCreationLogSystem.Save(new CustomerCreationLog()
                {
                    CustomerName = firstName + " " + lastName,
                    Institution = institutionName,
                    State = "About to create Customer",
                    PhoneNumber = phoneNumber,

                });
                Trace.TraceInformation(creationLog.State);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(ex.Message);
            }
            try
            {
                if (GetByPhoneNumber(phoneNumber) != null)
                {
                    throw new AlreadyRegisterdCustomerException("Phone Number Already registered");
                }

                string delimiter = ";";

                //if (GetBankAccountByAcccountNo(coreBankingAcct.DisplayMessage, agent.InstitutionCode) != null)
                //{
                //    throw new InvalidAccountNumberException("Account Number Already Exists");
                //}

                MobileAccount mob = new MobileAccount { LastName = lastName, OtherNames = firstName, DateCreated = DateTime.Now, MobilePhone = phoneNumber, IsAgentAccount = false, OpenedBy = AccountRegistrarType.Agent };
                mob.RegistrarID = agent.ID;
                mob.RegistrarCode = agent.Code;
                mob.RegistrarName = string.Format("{0},{1}", agent.LastName, agent.OtherNames);
                creationLog.State = "About to get institution by code";
                Trace.TraceInformation(creationLog.State);
                accountCreationLogSystem.Update(creationLog);

                instCode = new InstitutionSystem().GetByCode(_theDataSource, agent.InstitutionCode).ShortName;

                // mob.MobileAccountStatus = false ? MobileAccountStatus.PendingFundsTransfer : MobileAccountStatus.New;
                mob.MobileAccountStatus = MobileAccountStatus.Active;
                bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);



                mob.ActivationCode = GenerateActivationCode();
                try
                {
                    mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                }
                catch
                {
                    throw new NoHSMResponseException("No Response From HSM");
                }

                //string tempCode = new AppZone.HsmInterface.ThalesHsm().PinGenerator().GenerateRandomPin(mob.MobilePhone.PadRight(12,'0'), 4).EncryptedPin;//Generate encryptedPIN
                //mob.PINOffset = GeneratePINOffsetOnHSM(mob.MobilePhone,   GenerateActivationCode());
                // mob.PIN = mob.ActivationCode;
                //TODO set activation code as PIN
                mob.StandardPhoneNumber = "+234" + mob.MobilePhone.TrimStart("0".ToCharArray());

                mob.InstitutionCode = agent.InstitutionCode;

                creationLog.State = "About to call Create Account Number. Calling CoreBanking";

                try
                {
                    accountCreationLogSystem.Update(creationLog);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                }
                // Do Actual Core Banking Web Service Call
                Trace.TraceInformation("call create on core  " + territory);

                MobileAccount coreBankingAcct = CreateLafayetteAccountOnCoreBanking(lastName, firstName, phoneNumber, gender, agent, prod, placeofBirth, DOB, idNo, nokPhone, nokName, address, referalName, referalNo, hasSufInfo, accountSource, otherAccountSource, passport,country,state,territory,district,idExpDate,idIssuedDate);
                //CreateEnhancedAccountOnCoreBanking(lastName, firstName, phoneNumber, gender,
                //    agent, prod.Code, placeofBirth, DOB, idNo, nokPhone, nokName, address, referalName, referalNo,
                //    hasSufInfo, accountSource, otherAccountSource, passport);


                creationLog.State = "Customer account was successfully created. Account number:" + coreBankingAcct.NUBAN + ", Card serial number is: " + agent.Code;
                creationLog.AccountNumber = coreBankingAcct.NUBAN;

                try
                {
                    accountCreationLogSystem.Update(creationLog);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                }
                LinkingBankAccount acc = new LinkingBankAccount
                {
                    TheProduct = new Product{ Code=prod},
                    InstitutionCode = agent.InstitutionCode,
                    TheMobileAccount = mob,
                    Activated = false,
                    CustomerID = coreBankingAcct.CustomerID,
                    InstitutionName = instCode,
                    TheGender = coreBankingAcct.TheGender,
                    //Ensure you set institution code before doing this
                    BankAccount = coreBankingAcct.DisplayMessage,
                    //NUBAN = mob.NUBAN,
                    Passport = passport
                };

                if (GetByAccountNumber(acc.BankAccount, agent.InstitutionCode) != null)
                {
                    throw new AlreadyExistingAccountException("Accounts Already Exist");
                }

                creationLog.State = "About to call SaveMobileAccountFromField. Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;
                try
                {
                    accountCreationLogSystem.Update(creationLog);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                }

                mob.SelectedBankAccounts = new List<ILinkingBankAccount>();
                mob.SelectedBankAccounts.Add(acc);
                mob.RecievingBankAccount = acc;
                mob.TheGender = (Gender)Enum.Parse(typeof(Gender), gender);
                mob.CustomerID = coreBankingAcct.CustomerID;
                acc.ProductCode = prod;
                acc.ProductName = prod;
                mob.ReceivingAccountNumber = mob.RecievingBankAccount.BankAccount;
                _repository.DbContext.BeginTransaction(_theDataSource);
                SaveMobileAccountFromField(mob);
                //SaveMobileAccount(mob, false);
                _repository.DbContext.CommitTransaction(_theDataSource);
                creationLog.State = "Mobile Account Creation was successful. Account number:" + coreBankingAcct.NUBAN + ", Card Serial Number is: " + agent.Code;

                try
                {
                    accountCreationLogSystem.Update(creationLog);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                }

                return mob;
            }
            catch (AlreadyExistingAccountException ex)
            {
                throw ex;
            }
            catch (AlreadyRegisterdCustomerException ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during account creation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Phone number: {0}", phoneNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);

                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    {
                        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    }
                    string body = string.Format("{0}\nPhone number:{1}\n{2}\n{3}", ex.Message, phoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    client.SendEmail(string.IsNullOrEmpty(instCode) ? "BankOneMobile" : instCode, "donotreply@mybankone.com",
                        emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }

                throw ex;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during account creation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Mobile Account: {0} Phone number: {1}", agent.PhoneNumber, phoneNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");

                using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
                {
                    string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                    {
                        emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                    }
                    string body = string.Format("{0}\n{1}\nPhone: {2}\n{3}\n{4}", ex.Message, agent.PhoneNumber, phoneNumber, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    client.SendEmail(string.IsNullOrEmpty(instCode) ? "BankOneMobile" : instCode, "donotreply@mybankone.com",
                        emails, string.Format("BankOneMobile:{0}", ex.Message), body);
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                //return null;
                throw ex;
            }
        }        


        public MobileAccount SaveMobileAccountFromService(MobileAccount mob)
        {
            //_repository.DbContext.BeginTransaction(_theDataSource);
            SaveMobileAccountFromField(mob);
            //SaveMobileAccount(mob, false);
            //_repository.DbContext.CommitTransaction(_theDataSource);
            return mob;
        }
       

        public  int GetLoggedOnUserID()
        {
            //TODO call Institutin Web Service
            return 1;
        }
        public  MobileAccount CreateAccountOnCoreBanking(string lastName,string firstName, string phoneNumber, string sex, Agent agent,string productCode)
        {
            MobileAccount toReturn = new MobileAccount();
            
            CoreBankingService.Account acc = null;
            
            try
            {
                using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
                {
                    CoreBankingService.Gender gender = (CoreBankingService.Gender)Enum.Parse(typeof(CoreBankingService.Gender), sex);
                    if (agent == null)
                    {
                        throw new Exception("Invalid Agent");
                    }
                    //acc = client.CreateAccount(agent.InstitutionCode, lastName, firstName, phoneNumber, gender, productCode);
                    if (acc == null)
                    {
                        throw new Exception("Account not created on Core Banking");
                    }
                    toReturn.CustomerID = acc.CustomerID;
                    toReturn.TheGender = (Gender)Enum.Parse(typeof(Gender), sex);
                   
                    toReturn.DisplayMessage = acc.Number;//Dont freak out. just used to hold the accNo for now
                }
            }
            catch (Exception ex)
            {
                throw new CoreBankingWebServiceException(string.Format("Error Creating Account on Core Banking-{0}",ex.Message));
            }


            return toReturn;

        }
        public MobileAccount CreateEnhancedAccountOnCoreBanking(string lastName, string firstName, string phoneNumber, string sex, Agent agent, string productCode,string placeOfBirth,DateTime DOB,  string idNo,string nOKPhone,string NOKName, string address,string referalName, string referalPhone,bool hasSufficientSourceInfo,string infoSource, string otherSourcInfo, byte[] passport)
        {
            MobileAccount toReturn = new MobileAccount();

            CoreBankingService.Account acc = null;
            try
            {
                using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
                {
                    //client.GetAccountOfficer("oooo","979").
                    

                    CoreBankingService.Gender gender = (CoreBankingService.Gender)Enum.Parse(typeof(CoreBankingService.Gender), sex);
                    CoreBankingService.AccountInformationSource ifoSource = (CoreBankingService.AccountInformationSource)Enum.Parse(typeof(CoreBankingService.AccountInformationSource),infoSource);
                    AccountSource mobileIfoSource = (AccountSource)Enum.Parse(typeof(AccountSource),infoSource);
                    if (agent == null)
                    {
                        throw new Exception("Invalid Agent");
                    }
                    
                    acc = client.CreateAccount(agent.InstitutionCode, lastName, firstName, phoneNumber, gender, productCode,agent.TheAgentAccount.BankAccount,placeOfBirth,DOB,address,idNo,NOKName,nOKPhone,referalName, referalPhone,hasSufficientSourceInfo,ifoSource, otherSourcInfo, passport);
                    
                    if (acc == null)
                    {
                        throw new Exception("Account not created on Core Banking");
                    }
                    toReturn.DateOfBirth = DOB;
                    toReturn.PlaceOfBirth = placeOfBirth;
                    toReturn.NOKName = NOKName;
                    toReturn.NOKPhone=  nOKPhone;
                    toReturn.Address = address;
                    toReturn.ReferalPhone = referalPhone;
                    toReturn.CustomerID = acc.CustomerID;
                    toReturn.TheGender = (Gender)Enum.Parse(typeof(Gender), sex);
                    toReturn.HasSufficientInfo=hasSufficientSourceInfo;
                    toReturn.AccountSource=mobileIfoSource;
                    toReturn.NUBAN = acc.NUBAN;
                    var bankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                    var type = bankAccountHelper.GetInstitutionAccountNumberType(agent.InstitutionCode);
                    //Dont freak out. just used to hold the accNo for now
                    switch (type)
                    {
                        case AccountNumberType.NUBAN:
                            toReturn.DisplayMessage = acc.NUBAN; break;
                        case AccountNumberType.Standard:
                        default:
                            toReturn.DisplayMessage = acc.Number; break;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Core banking web service exception.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw new CoreBankingWebServiceException(string.Format("Error Creating Account on Core Banking-{0}", ex.Message));
            }


            return toReturn;

        }

        public MobileAccount CreateLafayetteAccountOnCoreBanking(string lastName, string firstName, string phoneNumber, string gender, Agent agent, string productCode, string placeOfBirth, DateTime DOB, string idNo, string nOKPhone, string NOKName, string address, string referalName, string referalPhone, bool hasSufficientSourceInfo, string ifoSource, string otherSourcInfo, byte[] passport, string country, string state, string territory, string district, DateTime idExpDate, DateTime idIssuedDate)
        {
            MobileAccount toReturn = new MobileAccount();
            Trace.TraceInformation(" Creat Core. Produtc is {0}", productCode);
            Trace.TraceInformation(" Creat Core. ID TYOE is {0}", referalPhone);
            Trace.TraceInformation("Agent is {0}", agent.Code);

            //LafayetteService.Account acc = null;
            
                 
            try
            {

                using (LafayetteService.SwitchingServiceClient client = new LafayetteService.SwitchingServiceClient())
                {
                

                    //CoreBankingService.Gender gender = (CoreBankingService.Gender)Enum.Parse(typeof(CoreBankingService.Gender), sex);
                    //CoreBankingService.AccountInformationSource ifoSource = (CoreBankingService.AccountInformationSource)Enum.Parse(typeof(CoreBankingService.AccountInformationSource), infoSource);
                    //AccountSource mobileIfoSource = (AccountSource)Enum.Parse(typeof(AccountSource), infoSource);
                    if (agent == null)
                    {
                        throw new Exception("Invalid Agent");
                    }
                    LafRef5.Gender gend =(LafRef5.Gender) Enum.Parse(typeof(LafRef5.Gender), gender);
                    LafRef5.IDType idType = new  LafRef5.IDType{ Code=referalPhone};
                    LafayetteService.AccountInformationSource acif = (LafayetteService.AccountInformationSource)Enum.Parse(typeof(LafayetteService.AccountInformationSource), ifoSource);
                    LafRef5.SwitchingServiceClient lClient = new LafRef5.SwitchingServiceClient();
                    string aCode = "PHOENIX";
                    aCode = string.IsNullOrEmpty(agent.TheAgentAccountNumber) ? "PHOENIX" : agent.TheAgentAccountNumber;



                    Trace.TraceInformation(" Create Account Values Are {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},", agent.InstitutionCode, firstName, lastName, placeOfBirth, DOB.ToString(), address, state, district, territory, country, phoneNumber, gend, Convert.ToInt32(productCode), agent.TheAgentAccount.BankAccount, idType.Code.ToString(), idNo, idExpDate.ToString(), idIssuedDate.ToString());
                    //Create Account Values Are 100127,Jane,Frederick,Ibadan,4/29/1980 12:00:00 AM,17 Iwo Road,Oyo,Iwo,Dugbe,Nigeria,08067551199,Male,211,821-0000001,,65465578,4/29/2018 12:00:00 AM,4/29/2013 12:00:00 AM,	2014-07-10 10:29:49Z

                                                                                                                 //w3wp.exe	Information	0	 Create Account Values Are100127,John Snow,Farida,Iwo,4/29/1980 12:00:00 AM,Iwo Rd,Oyo,disIwo,Terri,Country,08076761100,Female,211,821-0000001,,5456587,4/29/2012 12:00:00 AM,4/29/1980 12:00:00 AM,	2014-07-10 08:36:02Z
                 LafRef5.Account acccount=  lClient.CreateAccount(agent.InstitutionCode, firstName, lastName, placeOfBirth, DOB, address, state, district, territory, country, phoneNumber, gend,Convert.ToInt32(productCode), aCode, idType, idNo, idExpDate, idIssuedDate);
                                                                          
                    //                                                                                                                                                                                                                                                               
                 
                                  //                                                                                                        Values Are 100127,Maureen,Aikhigbe,Lagos,4/29/2006 12:00:00 AM,Oti Road,Yaba,Yaba,Yaba,Nigeria,08067671100,Male,191,PHOENIX,BankOneMobile.Services.LafRef5.IDType,765467766,7/8/2014 2:46:31 PM,7/8/2014 2:46:31 PM,	2014-07-08 13:46:31Z
                  //w3wp.exe	Information	0	                                                                                        

                    








                    //acc = client.CreateAccount(agent.InstitutionCode, lastName, firstName, phoneNumber, gend, productCode, "PHOENIX", placeOfBirth, DOB, address, idNo, NOKName, nOKPhone, referalName, referalPhone, hasSufficientSourceInfo, acif, otherSourcInfo, passport,state,district,territory,country,idExpDate,idIssuedDate);

                 if (acccount == null)
                    {                    
                        throw new Exception("Account not created on Core Banking");
                    }
                    toReturn.DateOfBirth = DOB;
                    toReturn.PlaceOfBirth = placeOfBirth;
                    toReturn.NOKName = NOKName;
                    toReturn.NOKPhone = nOKPhone;
                    toReturn.Address = address;
                    toReturn.ReferalPhone = referalPhone;
                    toReturn.CustomerID = acccount.CustomerID;
                    toReturn.TheGender = (Gender)Enum.Parse(typeof(Gender), gender);
                    toReturn.HasSufficientInfo = hasSufficientSourceInfo;
                    //toReturn.AccountSource = Mobile
                    toReturn.NUBAN = acccount.NUBAN;
                    var bankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                    var type = bankAccountHelper.GetInstitutionAccountNumberType(agent.InstitutionCode);
                    //Dont freak out. just used to hold the accNo for now
                    switch (type)
                    {
                        case AccountNumberType.NUBAN:
                            toReturn.DisplayMessage = acccount.NUBAN; break;
                        case AccountNumberType.Standard:
                        default:
                            toReturn.DisplayMessage = acccount.Number; break;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Core banking web service exception.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw new CoreBankingWebServiceException(string.Format("Error Creating Account on Core Banking-{0}", ex.Message));
            }


            return toReturn;

        }
        public List<CoreIDType> GetLafayetteIDTypes()
        {
            string instCode = "100127";
            List<CoreIDType> toReturn = new List<CoreIDType>();
            LafRef5.SwitchingServiceClient cl = new LafRef5.SwitchingServiceClient();
            LafRef5.IDType[] values = cl.GetIDTypes(instCode);
            cl.Close();
            if (values == null)
                return null;
            foreach (LafRef5.IDType idt in values)
            {
                toReturn.Add(new CoreIDType { Code = idt.Code, Name = idt.Name });
            }
            return toReturn;
        }

        
        # region WebService Methods
        public string ValidateMobileAccount(string phoneNumber, string institutionCode, string accountNumber, string encryptedPinBlock, bool isPhoneNumber, bool verifyPin, out string acctNo)
        {
            Trace.TraceInformation("Do  Validate Mobile Account . Values are::::PhoneNumber: {0} InstitutionCode: {1}Account No {2} Encrypted Pin Block: {3} Is Phone Number; {4}", phoneNumber, institutionCode,accountNumber, encryptedPinBlock, isPhoneNumber);
            //PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("About to validate Account. Encrypted Pin is {0}", encryptedPinBlock)));
            Trace.TraceInformation("Validation Started-{0}",phoneNumber);
           // HSMCenter.ResetAllPins();
            acctNo = string.Empty;
            ILinkingBankAccount bankAccount = null;
            MobileAccount mob = new MobileAccount();
            MobileAccount TestMobileAccount = null;
            try
            {
                
                Trace.TraceInformation("About to Get Config--{0}", phoneNumber);
                SystemConfig config = new SystemConfigSystem(_theDataSource).GetConfig();

                TestMobileAccount = GetMobileAccountByPhoneNumber(institutionCode, phoneNumber) as MobileAccount;
                if (TestMobileAccount == null)
                {
                    mob.DisplayMessage = "E02:Invalid Sending Phone Number";
                    Trace.TraceInformation("Get By Phone Number Returned Null-{0}", phoneNumber);
                    return mob.DisplayMessage;
                }
                else
                {

                    var mobAcc = GetMobileAccountByPhoneNumber(institutionCode, phoneNumber) as MobileAccount;
                    bankAccount = mobAcc == null ? (ILinkingBankAccount)null : mobAcc.BankAccounts.FirstOrDefault(f => f.InstitutionCode == institutionCode);
                    if (bankAccount == null)
                    {
                        Trace.TraceInformation("Get Bank Account Returned Null-{0}", phoneNumber);
                        mob.DisplayMessage = "E02:Invalid Account Number";
                        return mob.DisplayMessage;
                    }
                    
                    accountNumber = acctNo = bankAccount.BankAccount;
                    Trace.TraceInformation("Account Number  Set is {0}-", accountNumber);
                }

                Trace.TraceInformation("Real Check-{0}",accountNumber);
                string val =accountNumber.Split(".".ToCharArray()).Count()>=2?accountNumber.Split(".".ToCharArray())[1]:string.Empty;// check if tis a mobile Teller acct Seperated by a '.'
                Trace.TraceInformation("Val is ",val);
                Trace.TraceInformation("About to Get Bank Account {0}", phoneNumber);
                if (string.IsNullOrEmpty(val) && GetBankAccountByAcccountNoForAuthorization(accountNumber, institutionCode) == null)
                {
                    Trace.TraceInformation("Gotten Bank Account is null{0}", phoneNumber);
                  //if(accountNumber.Split(".".ToCharArray()).Count()>=1)
                  //{
                  //}
                  //  Trace.TraceInformation("Real Check-1");
                    mob.DisplayMessage = "E02:Invalid Account Number";
                    return mob.DisplayMessage;
                }
                else if (!string.IsNullOrEmpty(val) && GetBankAccountByAcccountNoForAuthorization(val, institutionCode) == null)
                {
                    Trace.TraceInformation("Gotten Bank Account is NOT null{0}", phoneNumber);
                    //if (accountNumber.Split(".".ToCharArray()).Count() >= 1)
                    //{
                    //}
                    //Trace.TraceInformation("Real Check-1");
                    mob.DisplayMessage = "E02:Invalid Account Number";
                    return mob.DisplayMessage;
                }
               
                Trace.TraceInformation("Real Check-{0}", "Stage1");
                 if (TestMobileAccount.BankAccounts == null )
                {
                    Trace.TraceInformation("Real Check-2");
                    mob.DisplayMessage = "E03:Mobile Account has no linked accounts";
                    return mob.DisplayMessage;
                }
                //else if ( TestMobileAccount.BankAccounts.Count == 0)
                //{
                //    Trace.TraceInformation("Real Check-2");
                //    mob.DisplayMessage = "E03:Mobile Account has no linked accounts";
                //    return mob.DisplayMessage;
                //}
                 Trace.TraceInformation("Real Check-{0}", "Stage2");
                 if (TestMobileAccount.BankAccounts != null && TestMobileAccount.BankAccounts.Count == 0)
                {
                    Trace.TraceInformation("Real Check-3");
                    mob.DisplayMessage = "E03:Mobile Account has no linked accounts";
                    return mob.DisplayMessage;
                }
                Trace.TraceInformation("Real Check-{0}", "Stage3");
                // if (TestMobileAccount.BankAccounts != null&&!TestMobileAccount.BankAccounts.Select(x => x.BankAccount).Contains(string.IsNullOrEmpty(val)?accountNumber:val))
                //{
                //    Trace.TraceInformation("Real Check-4");
                //    Trace.TraceInformation("mobile is  {0} val is {1} account is {2}", TestMobileAccount.MobilePhone, val, accountNumber);
                //    mob.DisplayMessage = "E04:Account Number not linked to Mobile Account";
                //    return mob.DisplayMessage;
                //}
                //else if (mob.RecievingBankAccount == null)
                //{
                //    mob.DisplayMessage = "E05:Mobile Account does not have default account number";
                //    return mob;
                //}
                 Trace.TraceInformation("Real Check-{0}", "Stage4");
                 if (TestMobileAccount.MobileAccountStatus != MobileAccountStatus.Active)
                {
                    
                    Trace.TraceInformation("Real Check-4");
                    mob.DisplayMessage = "E06:Mobile Account is not active";
                    Trace.TraceInformation("Ending Web Service Call {0}", phoneNumber);
                    return mob.DisplayMessage;
                }
                 Trace.TraceInformation("Real Check-{0}", "Stage5");
                 if (TestMobileAccount.PinTries > config.MaximumPinTries)
                {
                    Trace.TraceInformation("Real Check-5");
                    mob.DisplayMessage = "E07:Mobile Account Locked.Pin Tries Exceeded";
                    Trace.TraceInformation("Ending Web Service Call {0}", phoneNumber);
                    return mob.DisplayMessage;
                }
                 Trace.TraceInformation("Real Check-{0}", "Starting Pin Check-{0}",phoneNumber);
                 if (verifyPin && !new HSMCenter().VerifyPinOnHSM(phoneNumber, TestMobileAccount.PIN, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI))
                { //Remove this Now!! pin check disabled
                    Trace.TraceInformation("Pin Check Done -6-{0}",phoneNumber);
                    TestMobileAccount.PinTries++;
                    //_repository.DbContext.BeginTransaction(_theDataSource);
                    UpdateMobileAccount(TestMobileAccount);
                    //_repository.DbContext.CommitChanges(_theDataSource);
                    mob.DisplayMessage = "E08:Invalid Pin";
                    Trace.TraceInformation("Ending Web Service Call {0}", phoneNumber);
                    return mob.DisplayMessage;
                }
                     
                else
                {
                    Trace.TraceInformation("Pin Check Done-7 -{0}",phoneNumber);
                    
                    mob = TestMobileAccount;
                    TestMobileAccount.DisplayMessage = mob.DisplayMessage = String.Format("{0}:{1}", "00", acctNo);
                    Trace.TraceInformation("Ending Web Service Call {0}",phoneNumber);
                    return TestMobileAccount.DisplayMessage;                  
                    
                }
            }
            catch (NoHSMResponseException ex)
            {
                System.Diagnostics.Trace.TraceError("Caught HSM Response error during mobile account validation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Phone number: {0}......Account: {1}", phoneNumber, accountNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                return string.Format("E200:{0}", "No Response from HSM");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught unknown error during mobile account validation.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(string.Format("Phone number: {0}......Account: {1}", phoneNumber, accountNumber));
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                return string.Format("E201:{0}", ex.Message);
            }


        }

        public string ValidateReciever(string identifier, string institutionCode, bool isPhoneNumber, out string accountNumber)
        {
            MobileAccount toMob = null;

            accountNumber = string.Format("{0}:{1}", "E01:Invalid", isPhoneNumber ? "Invalid Phone Number" : "Invalid Account Number");
            string toReturn = accountNumber;
            if (isPhoneNumber)
            {
                toMob = GetByPhoneNumber(identifier, institutionCode);
                if (toMob != null)
                {
                    var bankAccount = toMob.BankAccounts.FirstOrDefault(k => k.InstitutionCode == institutionCode);
                    if (bankAccount != null)
                    {
                        toReturn = "00";
                        accountNumber = bankAccount.BankAccount;
                    }
                }

            }
            else
            {
                toMob = GetByAccountNumber(identifier, institutionCode);
                if (toMob != null)
                {
                    toReturn = "00";
                    accountNumber = identifier;
                }
                else
                {
                    string newIdentifier = identifier.Substring(3);
                    toMob = GetByAccountNumber(newIdentifier, institutionCode);
                    if (toMob != null)
                    {
                        toReturn = "00";
                        accountNumber = identifier;
                    }
                }
            }
            return toReturn;
        }
        public bool UpdatePinTries(string phoneNumber, bool IsReset)
        {
            bool toReturn = false;
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            if (mob == null)
            {
                return false;
            }

            if (IsReset)
            {
                mob.PinTries = 0;
            }
            else
            {
                mob.PinTries++;
            }
            //_repository.DbContext.BeginTransaction(_theDataSource);
            UpdateMobileAccount(mob);
            //_repository.DbContext.CommitTransaction(_theDataSource);
            toReturn = true;
            return toReturn;
        }
        public bool ChangePin(string phoneNumber, string newPinBlock)
        {
            //MobileAccountSystem mobSys = new MobileAccountSystem(_);
            MobileAccount mob = GetByPhoneNumber(phoneNumber);
            string offset = new HSMCenter().GeneratePinOfserWithPinBlock(phoneNumber, newPinBlock);
            mob.PIN = offset;
            mob.MobileAccountStatus = MobileAccountStatus.Active;
            mob.DateActivated = DateTime.Now;
            //_repository.DbContext.BeginTransaction(_theDataSource);
            UpdateMobileAccount(mob);
            //_repository.DbContext.CommitTransaction(_theDataSource);
            return true;

        }
        # endregion                                                                   <

          
    
        public string ValidateMobileAccount(string phoneNumber, string institutionCode, string encryptedPinBlock, bool isPhoneNumber, bool verifyPin, out string acctNo)
        {
            Trace.TraceInformation("Inside Validate Mobile Account Method. Values are:::: {0} {1} {2} {3} {4}", phoneNumber, institutionCode, encryptedPinBlock, isPhoneNumber, verifyPin);
            acctNo = string.Empty;
            ILinkingBankAccount bankAccount = null;
            MobileAccount mob = new MobileAccount();
            MobileAccount TestMobileAccount = null;
            //if (isPhoneNumber)
            //{                

            //}
            try
            {
                SystemConfig config = new SystemConfigSystem(_theDataSource).GetConfig();
                Trace.TraceInformation("Now Gotten Config");
                TestMobileAccount = GetByPhoneNumber(phoneNumber, institutionCode);
                if (TestMobileAccount == null)
                {
                    mob.DisplayMessage = "E02:Invalid Sending Phone Number";
                    return mob.DisplayMessage;
                }
                else
                {
                    Trace.TraceInformation("Test Mobile Acct Is Not Null");

                    bankAccount = TestMobileAccount.BankAccounts.FirstOrDefault(h => h.InstitutionCode == institutionCode);
                    Trace.TraceInformation("Iteration Done");
                    if (bankAccount == null)
                    {
                        mob.DisplayMessage = "E02:Invalid Sending Phone Number";
                        return mob.DisplayMessage;
                    }
                    acctNo = bankAccount.BankAccount;
                    Trace.TraceInformation("Bank Account Is Not Null");
                    
                }
                
                if (TestMobileAccount.BankAccounts == null || TestMobileAccount.BankAccounts.Count == 0)
                {
                    mob.DisplayMessage = "E03:Mobile Account has no linked accounts";
                    return mob.DisplayMessage;
                }
                
                else if (TestMobileAccount.MobileAccountStatus != MobileAccountStatus.Active)
                {
                    mob.DisplayMessage = "E06:Mobile Account is not active";
                    return mob.DisplayMessage;
                }
                else if (TestMobileAccount.PinTries > config.MaximumPinTries)
                {
                    mob.DisplayMessage = "E07:Mobile Account Locked.Pin Tries Exceeded";
                    return mob.DisplayMessage;
                }
                else if (verifyPin && !new HSMCenter().VerifyPinOnHSM(phoneNumber, TestMobileAccount.PIN, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI))
                {
                    TestMobileAccount.PinTries++;

                    //_repository.DbContext.BeginTransaction(_theDataSource);
                    UpdateMobileAccount(TestMobileAccount);
                    //_repository.DbContext.CommitTransaction(_theDataSource);

                    mob.DisplayMessage = "E08:Invalid Pin";
                    return mob.DisplayMessage;
                }
                else
                {
                    mob = TestMobileAccount;
                    TestMobileAccount.DisplayMessage = mob.DisplayMessage = string.Format("{0}:{1}", "00", acctNo);
                    return TestMobileAccount.DisplayMessage;
                }
            }
            catch (NoHSMResponseException)
            {
                return string.Format("E200:{0}", "No Response from HSM");
            }
            catch (Exception EX)
            {
                return string.Format("E201:{0}", EX.Message);
            }
        }
        //[ApprovableMethod(PANE.Framework.Utility.Action.UPDATE)]
        //private MobileAccount HotListAccount(MobileAccount x)
        //{
        //    x.MobileAccountStatus = MobileAccountStatus.HotListed;
        //    UpdateMobileAccount(x);
        //    //_repository.DbContext.Close(_theDataSource);
        //        return x;
        //}
        //[ApprovableMethod(PANE.Framework.Utility.Action.UPDATE)]
        //private MobileAccount ResetPinTries(MobileAccount x)
        //{
        //    x.PinTries=0;
        //    UpdateMobileAccount(x);
        //    //_repository.DbContext.Close(_theDataSource);
        //    return x;
        //}
        public MobileAccount RefreshMobileAccount(MobileAccount mob, PANE.Framework.NHibernateManager.Configuration.DatabaseSource dbSource)
        {
            return  GetByPhoneNumber(mob.MobilePhone);
        }
        public LinkingBankAccount RefreshBankAccount(LinkingBankAccount lnb, PANE.Framework.NHibernateManager.Configuration.DatabaseSource dbSource)
        {
            return new LinkingBankAccountSystem(_theDataSource).GetByID(_theDataSource, lnb.ID);
        }
        public Object HotList(MobileAccount x)
        {
            x.MobileAccountStatus = MobileAccountStatus.HotListed;
            return ApprovalUtil.UpdateOrLogForApproval<MobileAccount>(MfbData.MFBCode, this.Update, x, x.LastName, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, HOTLIST_MOBILEACCOUNT);
        }
        public Object ResetMobileAccountPinTries(MobileAccount x)
        {
            x.PinTries = 0;
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("ResetMobileAccountPinTries"));
            return ApprovalUtil.UpdateOrLogForApproval<MobileAccount>(MfbData.MFBCode, this.Update, x, x.LastName, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, RESET_MOBILEACCOUNT_PIN_TRIES);
            
        }
        public Object EnableDisable(MobileAccount x,int mobIndex)
        {
            Trace.TraceInformation("Entered Vals",mobIndex);
            if (mobIndex == -1)
            {
                if (x.MobileAccountStatus == MobileAccountStatus.Active)
                {
                    x.MobileAccountStatus = MobileAccountStatus.InActive;
                }
                else
                {
                    x.MobileAccountStatus = MobileAccountStatus.Active;
                }
            }
            else
            {
                Trace.TraceInformation("Getting Vals");
                List<ILinkingBankAccount> vals = new LinkingBankAccountSystem(_theDataSource).FindActive(_theDataSource, x.BankAccounts[mobIndex].InstitutionCode, x.BankAccounts[mobIndex].BankAccount).ToList();
                Trace.TraceInformation("vals is ", x == null ? "Null" : Convert.ToString(x.BankAccounts));
                Trace.TraceInformation("vals is ", vals == null ? "Null" : vals.Count().ToString());
                if (x.BankAccounts[mobIndex].BankStatus ==BankAccountStatus.InActive)
                {
                    Trace.TraceInformation("vals is ", vals == null ? "Null" : vals.Count().ToString());
                    if (vals != null && vals.Where(y => y.BankStatus != BankAccountStatus.InActive).Count() > 0)
                    {

                        throw new Exception("an account with this  Number is already Active or Awaiting Approval.");
                    }
                    x.MobileAccountStatus = MobileAccountStatus.Active;
                    x.BankAccounts[mobIndex].BankStatus = BankAccountStatus.Active;
                }
                else
                {
                    x.BankAccounts[mobIndex].BankStatus = BankAccountStatus.InActive;
                }
            }
            return ApprovalUtil.UpdateOrLogForApproval<MobileAccount>(MfbData.MFBCode, this.Update, x, x.LastName, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, ENABLE_DISABLE_MOBILEACCOUNT);
        }
        public Object EnableDisable(MobileAccount x, int mobIndex, LinkingBankAccount lnk)
        {
            Trace.TraceInformation("Entered Vals", mobIndex);
            if (mobIndex == -1)
            {
                if (x.MobileAccountStatus == MobileAccountStatus.Active)
                {
                    x.MobileAccountStatus = MobileAccountStatus.InActive;
                }
                else
                {
                    x.MobileAccountStatus = MobileAccountStatus.Active;
                }
            }
            else
            {
                Trace.TraceInformation("Getting Vals");
                List<ILinkingBankAccount> vals = new LinkingBankAccountSystem(_theDataSource).FindActive(_theDataSource, lnk.InstitutionCode, lnk.BankAccount).ToList();
                Trace.TraceInformation("vals is ", x == null ? "Null" : Convert.ToString(x.BankAccounts));
                Trace.TraceInformation("vals is ", vals == null ? "Null" : vals.Count().ToString());
                if (x.BankAccounts[mobIndex].BankStatus == BankAccountStatus.InActive)
                {
                    Trace.TraceInformation("vals is ", vals == null ? "Null" : vals.Count().ToString());
                    if (vals != null && vals.Where(y => y.BankStatus != BankAccountStatus.InActive).Count() > 0)
                    {

                        throw new Exception("an account with this  Number is already Active or Awaiting Approval.");
                    }
                    x.MobileAccountStatus = MobileAccountStatus.Active;
                    x.BankAccounts[mobIndex].BankStatus = BankAccountStatus.Active;
                }
                else
                {
                    x.BankAccounts[mobIndex].BankStatus = BankAccountStatus.InActive;
                }
            }
            return ApprovalUtil.UpdateOrLogForApproval<MobileAccount>(MfbData.MFBCode, this.Update, x, x.LastName, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, ENABLE_DISABLE_MOBILEACCOUNT);
        }
       
        
        //public Object EnableDisable(LinkingBankAccount x)
        //{
        //    if (x.BankStatus == BankAccountStatus.Active)
        //    {
        //        x.BankStatus = BankAccountStatus.Active;
        //    }
        //    else
        //    {
        //        x.BankStatus = BankAccountStatus.InActive;
        //    }
        //    return ApprovalUtil.UpdateOrLogForApproval<LinkingBankAccount>(MfbData.MFBCode, this.Update, x, x.CoreBankingNames, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, ENABLE_DISABLE_MOBILEACCOUNT);
        //}
        public Object ResetPin(MobileAccount x)
        {
            var lbc = x.BankAccounts.FirstOrDefault(e => e.InstitutionCode == x.InstitutionCode);
            if (lbc == null)
            {
                Trace.TraceInformation("Error resetting mobile account PIN: The mobile account {0} has no bank accounts with institution {1}."
                    , x.MobilePhone, x.InstitutionCode);
                return false;
            }
            x.ProductName = lbc.BankAccount;
            x.PINOffset = lbc.InstitutionCode;
            x.MobileAccountStatus = MobileAccountStatus.AwaitingPinReset;
            return ApprovalUtil.UpdateOrLogForApproval<MobileAccount>(MfbData.MFBCode, this.Update, x, x.LastName, this.RefreshMobileAccount, PANE.Framework.NHibernateManager.Configuration.DatabaseSource.Core, RESET_MOBILEACCOUNT_PIN);
        }
        public Object Insert(MobileAccount x)
        {
            Trace.TraceInformation("In Insert {0}",x.InstitutionCode);
           x=  CreateMobileAccount(x, x.SelectedBankAccounts);
           Trace.TraceInformation("About to send SMS");
           if (x.ActivateViaUSSD)
           {
               //Send just activation...No verification
               string code = new Random().Next(1000000000, int.MaxValue).ToString();
               string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
               x.CustomerID = encryptedCode;

               //_repository.Merge(_theDataSource, ag);

               //string appMessage = string.Format(setUpMessage, code);
               string activationMessage = string.Format(newActivationCodeMessage, x.ActivationCode);
               //bool sentVerification = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(appMessage, ag.PhoneNumber, ag.InstitutionCode, ag.TheAgentAccount.BankAccount);
               bool sentActivation = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(activationMessage, x.MobilePhone, x.InstitutionCode, x.RecievingBankAccount.BankAccount);
               //ag.Linked = sentActivation;

           }
           else if (x.ActivateViaJava)
           {
               //Send activation and verification messages
               string code = new Random().Next(1000000000, int.MaxValue).ToString();
               string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
               x.CustomerID = encryptedCode;
               x.ProductCode = code;
               x.ActivationCode = code;

               //_repository.Merge(_theDataSource, ag);

               string appMessage = string.Format(x.InstitutionCode == "100040" ? setUpMesageDiamondBank : setUpMessage, code);
               //string activationMessage = string.Format(activationCodeMessage,ag.MobileAccount.ActivationCode);
               bool sentVerification = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(appMessage, x.MobilePhone, x.InstitutionCode, x.RecievingBankAccount.BankAccount);
               //bool sentActivation = new MobileAccountSystem(_theDataSource).SendInstitutionSMS(activationMessage, ag.PhoneNumber,ag.InstitutionCode,ag.TheAgentAccount.BankAccount);
               

           }
           else
           {

           }
           Trace.TraceInformation("After Create");
            return ApprovalUtil.SaveOrLogForApproval<MobileAccount>(MfbData.MFBCode, InsertMobileAccount, x, string.Format("{0},{1}", x.LastName, x.OtherNames), ADD_EDIT_MOBILEACCOUNT);
        }
        public MobileAccount InsertByAgent(MobileAccount mob,Agent ag)
        {
            mob = CreateMobileAccountByAgent(mob, mob.BankAccounts,ag);
            _repository.DbContext.BeginTransaction(_theDataSource);
            SaveOrUpdateByAgent(mob);
            _repository.DbContext.CommitTransaction(_theDataSource);
            using (CoreBankingService.SwitchingServiceClient client = new CoreBankingService.SwitchingServiceClient())
            {
               client.UpdatePhoneNoByCustomerID(mob.BankAccounts[0].InstitutionCode, mob.BankAccounts[0].CustomerID, mob.MobilePhone);             

            }
            return mob;
        }
        [ApprovableMethod(PANE.Framework.Utility.Action.INSERT)]
        private MobileAccount InsertMobileAccount(MobileAccount mob)
        {

             SaveOrUpdate(mob);
             return mob;
        }

        private void SaveOrUpdate(MobileAccount mob)
        {
            Trace.TraceInformation("In Final Insert");

            _repository.DbContext.BeginTransaction(_theDataSource);
            if (GetByPhoneNumber(mob.MobilePhone) == null)
            {
                Trace.TraceInformation("In Final Insert-null");
                bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
                mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                _repository.Save(_theDataSource, mob);
                //STOP sending activation code
                //Send when you confirm the verification code from the application
                //bool sent = SendInstitutionSMS(string.Format(newActivationCodeMessage,mob.ActivationCode),mob.MobilePhone,mob.PINOffset,mob.ProductName);
            }
            else
            {
                Trace.TraceInformation("In Final Insert - not null");
                //STOP sending activation code
                //Send when you confirm the verification code from the application
                //bool sent = SendInstitutionSMS(string.Format(newActivationCodeMessage, mob.ActivationCode), mob.MobilePhone, mob.PINOffset, mob.ProductName);
                _repository.Merge(_theDataSource, mob);
            }
            _repository.DbContext.CommitTransaction(_theDataSource);
             //_repository.DbContext.CommitChanges(_theDataSource);
          //_repository.DbContext.Close(_theDataSource);
        }
        private void SaveOrUpdateByAgent(MobileAccount mob)
        {
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
            MobileAccount oldMob = GetByPhoneNumber(mob.MobilePhone);
            if (oldMob == null)
            {
                bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
                mob.AccountRestriction = AccountRestriction.OnlyCashIn;
                mob.PIN = demoMode ? mob.ActivationCode : new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                _repository.Save(_theDataSource, mob);
            }
            else
            {
                oldMob.BankAccounts.Add(mob.BankAccounts[0]);
                oldMob.AccountRestriction = AccountRestriction.OnlyCashIn;
                _repository.Merge(_theDataSource, mob);
            }
            //_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
            
        }
        public static bool CheckPhoneNumber(string entry)
        {
            string validEntries = "080;070;081;071;090;091";
            string[] checkEntries = validEntries.Split(";".ToCharArray());
            bool toReturn = true;
            long digits = 0;

            foreach (string ss in checkEntries)
            {
                if (entry.StartsWith(ss))
                {
                    toReturn = true;
                    break;
                }

                toReturn = false;

            }
            if (entry.Length != 11)
            {
                toReturn = false;
            }
            if (!Int64.TryParse(entry, out  digits))
            {
                toReturn = false;
            }
            return toReturn;
        }

        //public bool  SendBankOneSMS(string message, string phoneNo)
        //{
        //    string acctNo = "APPZONE-001"; bool toReturn = false;
        //    string refNo = new TransactionSystem().GenerateTransactionRef();
        //    string apzoneInstCode = "000000";
           
        //    using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
        //    {
        //        SMSService.SMS sms = new SMSService.SMS {  AccountNo = acctNo, Body = message, To = phoneNo, ReferenceNo = refNo  };
                     
               
        //             toReturn= client.SendSMSBySwitchCodeSingle(apzoneInstCode, sms);
                    
               
        //    }
        //    return toReturn;
        //}
        public bool SendInstitutionSMS(string message, string phoneNo,string instCode,string acctNo)
        {
             bool toReturn = false;
             string messageForLogging = string.Format("Message is {0} Phone is{1}, instCoe is {2}, acct No is {3}", message, phoneNo, instCode, acctNo);
             PANE.ERRORLOG.ErrorLogger.Log(new Exception(messageForLogging));
            string refNo = new TransactionSystem().GenerateTransactionRef();
            

            using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
            {
                SMSService.SMS sms = new SMSService.SMS {  AccountNo = acctNo, Body = message, To = phoneNo, ReferenceNo = refNo };


                toReturn = client.SendSMSBySwitchCodeSingle(instCode, sms);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(String.Format("{0},{1} {2} SMS sending returned {3}", phoneNo, acctNo, instCode, toReturn)));
            }
            return toReturn;
        }
        public bool GenerateVerificationCode(MobileAccount  mob, string requestingInstitutionCode)
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Inside GenerateVerificationCode method"));
            System.Diagnostics.Trace.TraceInformation("Inside GenerateVerificationCode method");
            string code = new Random().Next(1000000000, int.MaxValue).ToString();
              string encryptedCode = new PANE.Framework.Utility.MD5Password().CreateSecurePassword(code);
              mob.CustomerID = encryptedCode;
              mob.ProductCode = code;
            //Set verified to false so verification will work...
              mob.Verified = false;
              setUpMessage = string.Format(mob.InstitutionCode == "100040" ? setUpMesageDiamondBank : setUpMessage, code);
              _repository.Update(_theDataSource, mob);

              var lbc = mob.BankAccounts.FirstOrDefault(e => e.InstitutionCode == requestingInstitutionCode);
              if (lbc == null)
              {
                  Trace.TraceInformation("Error generating verification Code: The mobile account {0} has no bank accounts with institution {1}."
                      , mob.MobilePhone, requestingInstitutionCode);
                  return false;
              }
              PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to send sms"));
              System.Diagnostics.Trace.TraceInformation("About to send sms");
              bool sent = SendInstitutionSMS(setUpMessage, mob.MobilePhone,lbc.InstitutionCode,lbc.BankAccount);
              UpdateMobileAccount(mob);
              //_repository.DbContext.CommitChanges(_theDataSource);
              //_repository.DbContext.Close(_theDataSource);
          
              return sent; 
         
             
          }
        public bool ApproveLinking(MobileAccount mob)
        {
            bool toReturn = false;
            mob.MobileAccountStatus = MobileAccountStatus.New;

            return toReturn;
        }
        public bool ApproveLinking(LinkingBankAccount lnk,bool isApproved)
        {
            List<ILinkingBankAccount> vals = new LinkingBankAccountSystem(_theDataSource).FindActive(_theDataSource, lnk.InstitutionCode, lnk.BankAccount).ToList();
            if (vals.Where(x => x.BankStatus == BankAccountStatus.Active).Count() > 0)
            {
                throw new Exception("an account with this  Number is already Active");
            }
            
         
            bool toReturn = false;
            lnk.BankStatus = isApproved? BankAccountStatus.Active:BankAccountStatus.InActive;
            if (isApproved&&lnk.TheMobileAccount.MobileAccountStatus==MobileAccountStatus.AwaitingApproval)
            {
               
                        bool sent = SendInstitutionSMS(string.Format(newActivationCodeMessage,lnk.TheMobileAccount.ActivationCode), lnk.TheMobileAccount.MobilePhone, lnk.InstitutionCode, lnk.BankAccount);
                        lnk.TheMobileAccount.MobileAccountStatus = MobileAccountStatus.New;
                       
            }

            bool allSet = true;
            //_theDataSource=DataSourceFactory.GetDataSource(DataCategory.Core);




            UpdateMobileAccount(lnk.TheMobileAccount as MobileAccount);
            //_repository.DbContext.CommitChanges(_theDataSource);
            //_repository.DbContext.Close(_theDataSource);
          
            //foreach (LinkingBankAccount lbc in lnk.TheMobileAccount.BankAccounts)
            //{
            //    if (lbc.BankStatus == BankAccountStatus.AwaitingApproval)
            //    {
            //        allSet = false;
            //    }
            //}

            //if (allSet)
            //{
            //    //lnk.TheMobileAccount.
                
            //}
            //new MobileAccountSystem().UpdateMobileAccount(lnk.TheMobileAccount as MobileAccount);
            return true;
        }

        public bool UpdateAccountToDebitForUSSD(MobileAccount mobileAccount, ILinkingBankAccount debitBankAccount)
        {
            bool result = false;
            if (debitBankAccount != null)
            {
                try
                {
                    mobileAccount.BankAccountToDebit = debitBankAccount;
                    UpdateMobileAccount(mobileAccount);
                    result = true;
                }
                catch
                {

                }
            }
            return result;
        }

        public bool CheckIfBalanceEnoughForUSSD(ILinkingBankAccount account, string mobilePhone)
        {
            bool result = false;
            BalanceEnquiryResponse response = new TransactionSystem(_theDataSource).DoPinlessBalanceEnquiry(account, mobilePhone);
            if (response != null)
            {
                if (response.AvailableBalance.TheBalanceType == SwitchIntegration.ISO8583.DTO.BalanceType.Credit)
                {
                    //Get amount configured
                    List<UssdBillingConfiguration> allConfiguration = new List<UssdBillingConfiguration>
                        (new UssdBillingConfigurationSystem(_theDataSource).GetAll().Cast<UssdBillingConfiguration>());
                    if (allConfiguration == null || allConfiguration.Count == 0)
                    {
                        throw new NoUssdConfigurationException("No configuration found");
                    }
                    UssdBillingConfiguration configuration = allConfiguration.First();
                    decimal amount = configuration.Amount;
                    try
                    {
                        if (Convert.ToDecimal(response.AvailableBalance.Balance) > amount)
                        {
                            result = true;
                        }
                    }
                    catch
                    {

                    }
                }
            }
            return result;
        }

        public static bool DebitAccountForUSSD(MobileAccount account)
        {
            bool result = false;
            //Get amount configured
            using (IDataSource dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                List<UssdBillingConfiguration> allConfiguration = new List<UssdBillingConfiguration>
                    (new UssdBillingConfigurationSystem(dataSource).GetAll().Cast<UssdBillingConfiguration>());
                if (allConfiguration == null || allConfiguration.Count == 0)
                {
                    throw new NoUssdConfigurationException("No configuration found");
                }
                UssdBillingConfiguration configuration = allConfiguration.First();
                decimal amount = configuration.Amount;
                if (account.IsAgentAccount)
                {
                    string response = new TransactionSystem(dataSource)
                        .DoPinlessMobileTellerDebit(account.RecievingBankAccount, account.MobilePhone, amount);
                    result = response == "00";
                }
                else
                {
                    Institution inst = new InstitutionSystem(dataSource).GetByCode(dataSource, account.RecievingBankAccount.InstitutionCode) as Institution;
                    string accountNumber = null;
                    using (BankOneIncomeAccountService.UtilityServiceClient client = new BankOneIncomeAccountService.UtilityServiceClient())
                    {
                        BankOneIncomeAccountService.AppZoneIncomeAccount incomeAccount = client.GetIncomeAccountByInstitutionID(inst.ID);
                        if (incomeAccount != null)
                        {
                            accountNumber = incomeAccount.AccountNumber;
                        }
                    }
                    if (string.IsNullOrEmpty(accountNumber))
                    {
                        result = false;
                    }
                    else
                    {
                        string response = new TransactionSystem(dataSource)
                            .DoPinlessCustomerDebit(account.BankAccountToDebit, accountNumber, account.MobilePhone, amount);
                        result = response == "00";
                    }
                }
            }
            return result;
        }

        public bool LoopThroughLinkedAccounts(string mobilePhone)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(mobilePhone))
            {
                //Get mobile account
                MobileAccount mob = GetByPhoneNumber(mobilePhone);
                if (mob == null)
                {
                    mob = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Auxilliary)).GetByPhoneNumber(mobilePhone);
                }
                if (mob != null && mob.MobileAccountStatus == MobileAccountStatus.Active)
                {
                    if(mob.IsAgentAccount)
                    {
                        return true;
                    }
                    //check default account
                    bool enoughBalance = CheckIfBalanceEnoughForUSSD(mob.RecievingBankAccount, mob.MobilePhone);
                    result = enoughBalance;
                    if (result)
                    {
                        //set account to debit
                        result = UpdateAccountToDebitForUSSD(mob, mob.RecievingBankAccount);
                    }
                    else
                    {
                        //Check all accounts
                        foreach (ILinkingBankAccount account in mob.BankAccounts)
                        {
                            //skip default account
                            if (account.ID == mob.RecievingBankAccount.ID)
                                continue;
                            enoughBalance = CheckIfBalanceEnoughForUSSD(account, mob.MobilePhone);
                            if (enoughBalance)
                            {
                                //set  account to debit and break from loop
                                result = UpdateAccountToDebitForUSSD(mob, account);
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SendActivationCode(MobileAccount mob)
        {
            bool result = false;
            try
            {

                //The folowing 3 lines of code are to update the mobile account with a new activation code because there was a bug that had the verification code in the place of activation code
                mob.ActivationCode = GenerateActivationCode();                
                
                mob.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.ActivationCode);
                new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).UpdateMobileAccount(mob);

                //continue here
                result = SendInstitutionSMS(string.Format(newActivationCodeMessage, mob.ActivationCode), mob.MobilePhone, mob.InstitutionCode, mob.ProductName);
            }
            catch(Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                Trace.TraceError("Error when sending activation code SMS.");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                Trace.TraceError(ex.Source);
                Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
            }
            return result;
        }

        public bool ChangePINFromBranch(string phoneNumber, string institutionCode, string oldPinBlock, string newPinBlock, out string message)
        {
            bool result = false;
            message = "Unable to change PIN, please try again later!";
            string account;
            //Validate old PIN
            try
            {
                message = ValidateMobileAccount(phoneNumber, institutionCode, oldPinBlock, true, true, out account);
                //Check validation result
                if (!message.Contains(':'))
                {
                    Trace.TraceInformation("Pin change: Invalid result from authorisation call: {0}", message);
                }
                else
                {
                    var splitMessage = message.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitMessage.Length != 2)
                    {
                        Trace.TraceInformation("Pin change: Invalid result from authorisation call: {0}", message);
                    }
                    else
                    {
                        //00 - Valid account
                        //E06 - 
                        var acceptableResponses = new string[] { "00", "E06" };
                        //Validation NOT successful
                        if (!acceptableResponses.Contains(splitMessage[0]))
                        {
                            message = string.IsNullOrEmpty(splitMessage[1]) ? splitMessage[0] + ":" + message : splitMessage[1];
                        }
                        //Validation Successful
                        else
                        {
                            if (splitMessage[0] == "E06")
                            {
                                //Account is not active
                                //New status should go - For activation
                                //Other statuses shouldn't
                                var mobileAccount = GetByPhoneNumber(phoneNumber);
                                if (mobileAccount.MobileAccountStatus == MobileAccountStatus.New)
                                {
                                    result = ChangePin(phoneNumber, newPinBlock);
                                }
                            }
                            else
                            {
                                result = ChangePin(phoneNumber, newPinBlock);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during pin change.{0}", phoneNumber);
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
            }
            return result;
        }
    }
}
