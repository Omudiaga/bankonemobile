﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class DBNFundsTransferTransactionTypeSystem
    {
       public IDBNFundsTransferTransactionType SaveDBNFundsTransferTransactionType(IDataSource dataSource, DBNFundsTransferTransactionType theDBNFundsTransferTransactionType)
        {
            theDBNFundsTransferTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theDBNFundsTransferTransactionType);
                repo.DbContext.CommitTransaction(dataSource);
            //SafeServiceLocator<IFundsTransferCashInTransactionTypeRepository>.GetService().DbContext.CommitChanges(dataSource);
            return result;

        }

        public IDBNFundsTransferTransactionType UpdateDBNFundsTransferTransactionType(IDataSource dataSource, DBNFundsTransferTransactionType theDBNFundsTransferTransactionType)
        {
            var repo = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theDBNFundsTransferTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IDBNFundsTransferTransactionType GetDBNFundsTransferTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public List<IDBNFundsTransferTransactionType> GetAllDBNFundsTransferransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public List<IDBNFundsTransferTransactionType> GetActiveDBNFundsTransferTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public List<IDBNFundsTransferTransactionType> FindFundsTransferCashInTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }

        public string GenerateToken()
        {
            return new Random().Next(10000, 99999).ToString();
        }
        public IList<IDBNFundsTransferTransactionType> GetByDestinationInstitutionCode(IDataSource dataSource, string destinationInstitutionCode)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetByDestinationInstitutionCode(dataSource, destinationInstitutionCode);
            return result;

        }

        public IList<IDBNFundsTransferTransactionType> GetByOriginatorAccountNumber(IDataSource dataSource, string originatorAccountNumber)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetByOriginatorAccountNumber(dataSource, originatorAccountNumber);
            return result;

        }

        public IList<IDBNFundsTransferTransactionType> GetByResponseCode(IDataSource dataSource, string responseCode)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetByResponseCode(dataSource, responseCode);
            return result;

        }

        public IList<IDBNFundsTransferTransactionType> GetByBeneficiaryAccountNumber(IDataSource dataSource, string beneficiaryAccountNumber)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetByBeneficiaryAccountNumber(dataSource, beneficiaryAccountNumber);
            return result;

        }

        public IDBNFundsTransferTransactionType GetBySessionID(IDataSource dataSource, string sessionID)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetBySessionID(dataSource, sessionID);
            return result;

        }
        public IDBNFundsTransferTransactionType GetByNameEnquirySessionID(IDataSource dataSource, string nameEnquirySessionID)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetByNameEnquirySessionID(dataSource, nameEnquirySessionID);
            return result;

        }

        public List<IDBNFundsTransferTransactionType> GetPendingTransfersToDiamondBank(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetPendingTransfersToDiamondBank(dataSource, institutionCode);
            return result.ToList();

        }

        public List<IDBNFundsTransferTransactionType> GetPendingTransfersToOtherBanks(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetPendingTransfersToOtherBanks(dataSource);
            return result.ToList();

        }

        public List<IDBNFundsTransferTransactionType> GetFailedReversals(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<IDBNFundsTransferTransactionTypeRepository>.GetService().GetFailedReversals(dataSource, institutionCode);
            return result.ToList();

        }
    }
}
