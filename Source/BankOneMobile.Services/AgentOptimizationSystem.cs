﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class AgentOptimizationSystem
    {

        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private IAgentOptimizationRepository _repository = SafeServiceLocator<IAgentOptimizationRepository>.GetService();
        public AgentOptimizationSystem()
        {

        }

        public AgentOptimizationSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }

        public IAgentOptimization SaveAgentOptimization(IDataSource dataSource, AgentOptimization agentOptimization)
        {
            var repo = SafeServiceLocator<IAgentOptimizationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, agentOptimization);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentOptimization UpdateAgentOptimization(IDataSource dataSource, AgentOptimization agentOptimization)
        {
            var repo = SafeServiceLocator<IAgentOptimizationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, agentOptimization);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentOptimization GetAgentOptimizationByID(IDataSource dataSource, long id)
        {
            var result = _repository.Get(dataSource, id);
            return result;
        }

        public List<AgentOptimization> GetAll(IDataSource dataSource)
        {
            var result = _repository.GetAll(dataSource);
            return result;
        }

        public List<AgentOptimization> GetAgentOptimizationByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            var result = _repository.GetByInstitutionCode(dataSource, institutionCode);
            return result.ToList();

        }

        public List<AgentOptimization> GetAgentOptimizationByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = _repository.GetByAgentPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result.ToList();

        }

        public List<AgentOptimization> Find(IDataSource dataSource, IDictionary<string, Object> nameValuePairs)
        {
            var result = _repository.FindAll(dataSource, nameValuePairs);
            return result;
        }
    }
}
