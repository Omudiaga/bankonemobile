﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class AgentGeoLocationTransactionTypeSystem
    {

        public IAgentGeoLocationTransactionType SaveAgentGeoLocationTransactionType(IDataSource dataSource, AgentGeoLocationTransactionType theAgentGeoLocation)
        {
            var repo = SafeServiceLocator<IAgentGeoLocationTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theAgentGeoLocation);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentGeoLocationTransactionType UpdateAgentGeoLocationTransactionType(IDataSource dataSource, AgentGeoLocationTransactionType theAgentGeoLocation)
        {
            var repo = SafeServiceLocator<IAgentGeoLocationTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theAgentGeoLocation);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentGeoLocationTransactionType GetAgentGeoLocationTransactionTypeByID(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IAgentGeoLocationTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;
        }

        public List<IAgentGeoLocationTransactionType> GetAgentGeoLocationTransactionTypeByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            var result = SafeServiceLocator<IAgentGeoLocationTransactionTypeRepository>.GetService().GetByInstitutionCode(dataSource, institutionCode);
            return result.ToList();

        }

        public List<IAgentGeoLocationTransactionType> GetAgentGeoLocationTransactionTypeByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber)
        {
            var result = SafeServiceLocator<IAgentGeoLocationTransactionTypeRepository>.GetService().GetByAgentPhoneNumber(dataSource, institutionCode, phoneNumber);
            return result.ToList();

        }
    }
}
