﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class FundsTransferCashOutTransactionTypeSystem
    {
      public static IFundsTransferCashOutTransactionType SaveFundsTransferCashOutTransactionType(IDataSource dataSource, FundsTransferCashOutTransactionType theFundsTransferCashOutTransactionType)
        {
            theFundsTransferCashOutTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theFundsTransferCashOutTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public static IFundsTransferCashOutTransactionType UpdateFundsTransferCashOutTransactionType(IDataSource dataSource, FundsTransferCashOutTransactionType theFundsTransferCashOutTransactionType)
        {
            var repo = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theFundsTransferCashOutTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

      public static IFundsTransferCashOutTransactionType GetFundsTransferCashOutTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IFundsTransferCashOutTransactionType> GetAllFundsTransferCashOutTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IFundsTransferCashOutTransactionType> GetActiveFundsTransferCashOutTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IFundsTransferCashOutTransactionType> FindFundsTransferCashOutTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }
        public static IFundsTransferCashOutTransactionType GetByToken(IDataSource dataSource, string token)
        {
            return  SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().GetByToken(dataSource,token);
        }
        public static IFundsTransferCashOutTransactionType GetByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
            return SafeServiceLocator<IFundsTransferCashOutTransactionTypeRepository>.GetService().GetByPhoneNumber(dataSource, phoneNumber);
        }
    }
}
