﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Services
{
  public  class CommercialBankFundsTransferTransactionTypeSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ICommercialBankFundsTransferTransactionTypeRepository _repository = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService();
        public List<ICommercialBankFundsTransferTransactionType> Search(IDataSource dataSource, string phoneNum, string institutionCode, string bankName, string transactionRefernce, string paymentReference,string beneficiaryAcctNo, string beneficiaryPhone,string beneficiaryName, DateTime? dateFrom, DateTime? dateTo, string status, int startIndex, int maxSize, out int total, out decimal totalAmt)
        {
            // BankOneMobile.Services.HSMCenter.ResetAllPins();
            //_theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ICommercialBankFundsTransferTransactionType> tr = new List<ICommercialBankFundsTransferTransactionType>();

            TransactionStatus? transStatus = null;
            if (!string.IsNullOrEmpty(status))
            {
                transStatus = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), status);
            }
            if (!string.IsNullOrEmpty(transactionRefernce) && !string.IsNullOrEmpty(transactionRefernce.Trim()))
            {

                dic.Add("TransactionReference", transactionRefernce);

            }
            if (!string.IsNullOrEmpty(paymentReference) && !string.IsNullOrEmpty(paymentReference.Trim()))
            {

                dic.Add("PaymentReference", paymentReference);

            }
            if (!string.IsNullOrEmpty(beneficiaryPhone) && !string.IsNullOrEmpty(beneficiaryPhone.Trim()))
            {

                dic.Add("BeneficiaryPhoneNumber", beneficiaryPhone);

            }
            if (!string.IsNullOrEmpty(beneficiaryName) && !string.IsNullOrEmpty(beneficiaryName.Trim()))
            {

                dic.Add("BeneficiaryLastName", beneficiaryName);

            }
            if (!string.IsNullOrEmpty(beneficiaryAcctNo) && !string.IsNullOrEmpty(beneficiaryAcctNo.Trim()))
            {

                dic.Add("AccountNumber", beneficiaryAcctNo);

            }
            if (!string.IsNullOrEmpty(bankName) && !string.IsNullOrEmpty(bankName.Trim()))
            {

                dic.Add("BankName", bankName);

            }


            //tr = _reposewitory.FindWithPaging( dic, institutionCode, startIndex, maxSize, out total);
            tr = FindTransactionsWithPaging(dataSource, institutionCode, transStatus, phoneNum,transactionRefernce,bankName, dic, dateFrom, dateTo, startIndex, maxSize, out total, out totalAmt);
             
            // //_repository.DbContext.Close(_theDataSource);
            return tr;

        }
        public static ICommercialBankFundsTransferTransactionType SaveCommercialBankFundsTransferTransactionType(IDataSource dataSource, CommercialBankFundsTransferTransactionType theCommercialBankFundsTransferTransactionType)
        {
            theCommercialBankFundsTransferTransactionType.IsActive = false;
            var repo = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theCommercialBankFundsTransferTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static ICommercialBankFundsTransferTransactionType UpdateCommercialBankFundsTransferTransactionType(IDataSource dataSource, CommercialBankFundsTransferTransactionType theCommercialBankFundsTransferTransactionType)
        {
            var repo = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theCommercialBankFundsTransferTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static CommercialBankFundsTransferTransactionType GetCommercialBankFundsTransferTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService().GetTransactionById(dataSource, id);
            CommercialBankFundsTransferTransactionType toReturn = null;;
            if (result != null)
            {
                toReturn = new CommercialBankFundsTransferTransactionType { ID = result.ID, Institution = result.Institution, MobilePhone = result.MobilePhone, AccountNumber = result.AccountNumber, BeneficiaryLastName = result.BeneficiaryLastName, BankName = result.BankName, BeneficiaryOtherNames = result.BeneficiaryOtherNames, PaymentReference = result.PaymentReference, TransactionReference = result.TransactionReference, TransactionDate = result.TransactionDate, TransactionAmount = result.TransactionAmount, TransactionDetails = result.TransactionDetails, TransactionStatus = result.TransactionStatus };
            }
            return toReturn;

        }

        public static List<ICommercialBankFundsTransferTransactionType> GetAllCommercialBankFundsTransferTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<ICommercialBankFundsTransferTransactionType> GetActiveCommercialBankFundsTransferTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<ICommercialBankFundsTransferTransactionType> FindCommercialBankFundsTransferTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }

        public  List<BankOneMobile.Core.Contracts.ICommercialBankFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, string transRef,string bankName, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount)
        {
            var result = SafeServiceLocator<ICommercialBankFundsTransferTransactionTypeRepository>.GetService().FindTransactionsWithPaging(dataSource, InstitutionID, status, phoneNumber,transRef,bankName, propertyValuePairs, dateFrom, dateTo, startIndex, maxSize, out total, out amount);
            return result;
        }


        public CommercialBankFundsTransferTransactionType ConfirmTransaction(IDataSource source,CommercialBankFundsTransferTransactionType x, out string status, out string description )
        {
            x.TheTransaction = new TransactionSystem(source).GetTransaction(x.TransactionID);
            string trxRef = string.Empty;
            QueryServiceRef.ServiceClient client = new QueryServiceRef.ServiceClient();
            try
            {

                bool succesful = client.QueryAllTransactionTypesWithRawReference(out trxRef, x.Institution, x.TransactionReference, true);
                if (succesful)
                {
                    x.TheTransaction.Status = TransactionStatus.Successful;
                    status = "Successful";
                    x.TheTransaction.StatusDetails = "Successful";
                    description = "Successful";
                    x.PaymentReference = trxRef;

                }
                else
                {
                    x.TheTransaction.Status = x.TheTransaction.Status==TransactionStatus.Pending?  TransactionStatus.FailedNotReversed:TransactionStatus.Failed;
                    status = "Failed";
                    x.TheTransaction.StatusDetails = trxRef;
                    description = trxRef;

                }
                x = UpdateCommercialBankFundsTransferTransactionType(source, x) as CommercialBankFundsTransferTransactionType;
            }
            
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                client.Close();
            }
            
        
            return x;
        }
    }
}
