﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class CustomerRegistrationSystem
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        private ICustomerRegistrationRepository _repository = SafeServiceLocator<ICustomerRegistrationRepository>.GetService();

        private Object thisLock = new Object();
       public CustomerRegistrationSystem()
       {

       }

       public CustomerRegistrationSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
       public ICustomerRegistration SaveCustomerRegistration(CustomerRegistration theCustomerRegistration)
        {
            //theCustomerRegistration.IsActive = false;
            _repository.DbContext.BeginTransaction(_theDataSource);
            var result = _repository.Save(_theDataSource, theCustomerRegistration);
            _repository.DbContext.CommitTransaction(_theDataSource);
            return result;

        }

       public ICustomerRegistration UpdateCustomerRegistration(CustomerRegistration theCustomerRegistration)
        {
            //lock (thisLock)
            //{
                _repository.DbContext.BeginTransaction(_theDataSource);
                var result = _repository.Update(_theDataSource, theCustomerRegistration);
                _repository.DbContext.CommitTransaction(_theDataSource);
                return result;
           //}
        }

       public ICustomerRegistration GetCustomerRegistration(long id)
        {
            var result = _repository.Get(_theDataSource, id);
            return result;

        }

       public List<ICustomerRegistration> GetAllCustomerRegistrations()
        {
            var result = _repository.GetAll(_theDataSource);
            return result;

        }

       public List<ICustomerRegistration> GetPendingCustomerRegistrations()
        {
            var result = _repository.GetAll(_theDataSource);
            result = result.Where(x => 
                x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Pending ||
               (x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Failed &&
               x.CommitPoint == Core.Helpers.Enums.CommitPoint.BankOneMobile)).ToList();
            return result;
        }

       public List<ICustomerRegistration> GetPendingSMSes()
       {
           var result = _repository.GetAll(_theDataSource);
           result = result.Where(x =>
               x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Successful &&
              x.IsSmsSent == false).ToList();
           return result;
       }

       public List<ICustomerRegistration> GetPendingSMSesByRawSql()
       {
           var result = _repository.GetPendingSMSesByRawSql(_theDataSource);
           return result;
       }

       public List<ICustomerRegistration> GetFailedCustomerRegistrations()
       {
           var result = _repository.GetAll(_theDataSource);
           result = result.Where(x => x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Failed).ToList();
           return result;
       }
       public List<ICustomerRegistration> GetSuccessfulCustomerRegistrationsWhoseImagesAreNotSynced()
       {
           var result = _repository.GetAll(_theDataSource);
           result = result.Where(x => x.Status == BankOneMobile.Core.Helpers.Enums.RegistrationStatus.Successful
               && x.IsImageSync == false).ToList();
           return result;
       }

       public ICustomerRegistration GetCustomerRegistrationByPhoneNumber(string institutionCode, string phoneNumber)
       {
           var result = _repository.GetCustomerRegistrationByPhoneNumber(_theDataSource, institutionCode, phoneNumber);
           return result;
       }
       public ICustomerRegistration GetCustomerRegistrationBySerialNumber(string serialNumber)
       {
           var result = _repository.GetCustomerRegistrationByCardSerialNumber(_theDataSource, serialNumber);
           return result;
       }

       public List<ICustomerRegistration> FindRegistrationTransactionTypes(IDictionary<string, object> propertyValuePairs)
        {
            var result = _repository.Search(_theDataSource, propertyValuePairs);
            return result;
        }

       public List<ICustomerRegistration> Search(IDataSource dataSource, string phoneNum, string institutionCode, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            var tr = new List<ICustomerRegistration>();

            tr = _repository.FindTransactionsWithPaging(dataSource, institutionCode, phoneNum, dic, dateFrom, dateTo, startIndex, maxSize, out total);
            return tr;
        }
    }
}
