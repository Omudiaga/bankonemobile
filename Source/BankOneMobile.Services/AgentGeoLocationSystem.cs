﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
    public class AgentGeoLocationSystem
    {

        public IAgentGeoLocation SaveAgentGeoLocation(IDataSource dataSource, AgentGeoLocation theAgentGeoLocation)
        {
            var repo = SafeServiceLocator<IAgentGeoLocationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theAgentGeoLocation);
                repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentGeoLocation UpdateAgentGeoLocation(IDataSource dataSource, AgentGeoLocation theAgentGeoLocation)
        {
            var repo = SafeServiceLocator<IAgentGeoLocationRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Update(dataSource, theAgentGeoLocation);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public IAgentGeoLocation GetAgentGeoLocationByID(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IAgentGeoLocationRepository>.GetService().Get(dataSource, id);
            return result;
        }

        public List<IAgentGeoLocation> GetAgentGeoLocationByInstitutionCode(IDataSource dataSource,string institutionCode)
        {
            var result = SafeServiceLocator<IAgentGeoLocationRepository>.GetService().GetByInstitutionCode(dataSource,institutionCode);
            return result.ToList();

        }

        public List<IAgentGeoLocation> GetAgentGeoLocationByAgentPhoneNumber(IDataSource dataSource,string institutionCode,string phoneNumber)
        {
            var result = SafeServiceLocator<IAgentGeoLocationRepository>.GetService().GetByAgentPhoneNumber(dataSource,institutionCode,phoneNumber);
            return result.ToList();

        }
    }
}
