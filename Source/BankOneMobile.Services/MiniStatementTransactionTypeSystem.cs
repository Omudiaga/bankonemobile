﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Services
{
  public  class MiniStatementTransactionTypeSystem
    {
        public static IMiniStatementTransactionType SaveMiniStatementTransactionType(IDataSource dataSource, MiniStatementTransactionType theMiniStatementTransactionType)
        {
            theMiniStatementTransactionType.IsActive = false;
            var repo = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService();
            repo.DbContext.BeginTransaction(dataSource);
            var result = repo.Save(dataSource, theMiniStatementTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IMiniStatementTransactionType UpdateMiniStatementTransactionType(IDataSource dataSource, MiniStatementTransactionType theMiniStatementTransactionType)
        {
            var repo = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService();
            repo.DbContext.CommitTransaction(dataSource);
            var result = repo.Update(dataSource, theMiniStatementTransactionType);
            repo.DbContext.CommitTransaction(dataSource);
            return result;

        }

        public static IMiniStatementTransactionType GetMiniStatementTransactionType(IDataSource dataSource, long id)
        {
            var result = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService().Get(dataSource, id);
            return result;

        }

        public static List<IMiniStatementTransactionType> GetAllMiniStatementTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService().GetAll(dataSource);
            return result;

        }

        public static List<IMiniStatementTransactionType> GetActiveMiniStatementTransactionTypes(IDataSource dataSource)
        {
            var result = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService().GetAll(dataSource);
            result = result.Where(x => x.IsActive == true).ToList();
            return result;

        }

        public static List<IMiniStatementTransactionType> FindMiniStatementTransactionTypes(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            var result = SafeServiceLocator<IMiniStatementTransactionTypeRepository>.GetService().Search(dataSource, propertyValuePairs);
            return result;
        }
    }
}
