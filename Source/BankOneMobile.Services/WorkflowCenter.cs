﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Web;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Activities.XamlIntegration;
using System.IO;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Diagnostics;

namespace BankOneMobile.Services
{
    public class WorkflowCenter
    {
        private static volatile WorkflowCenter instance = null;
        private static object syncRoot = new object();
        public System.Collections.Concurrent.ConcurrentDictionary<Guid, WorkflowInfo> WorkflowInfos { get; set; }

        public System.Collections.Concurrent.ConcurrentQueue<WorkflowInfo> EndedWorkflowInfos { get; set; }

        public System.Collections.Concurrent.ConcurrentDictionary<string, Guid> SessionWorkflows { get; set; }

        public System.Collections.Concurrent.ConcurrentDictionary<string, Guid> InitiatingWorkflows { get; set; }

        // make the default constructor private, so that no can directly create it.
        private WorkflowCenter()
        {
            WorkflowInfos = new System.Collections.Concurrent.ConcurrentDictionary<Guid, WorkflowInfo>();
            SessionWorkflows = new System.Collections.Concurrent.ConcurrentDictionary<string, Guid>();
            InitiatingWorkflows = new System.Collections.Concurrent.ConcurrentDictionary<string, Guid>();
            EndedWorkflowInfos = new System.Collections.Concurrent.ConcurrentQueue<WorkflowInfo>();
        }

        // public property that can only get the single instance of this class.
        public static WorkflowCenter Instance
        {
            get
            {
                // only create a new instance if one doesn't already exist.
                if (instance == null)
                {
                    // use this lock to ensure that only one thread is access
                    // this block of code at once.
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new WorkflowCenter();
                        }
                    }
                }
                // return instance where it was just created or already existed.
                return instance;
            }
        }

        public WorkflowInfo SetCurrentWorkflow(string sessionId, IWFActivity theFunction, WorkflowApplication theWorkflowApp, WorkflowInfo thePreviousWorkflow, WorkflowInfo currentWorkflowInfo = null)
        {
            if (currentWorkflowInfo == null)
            {
                var reuseWorkflowInfo = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ReUseWorkflow"]);
                if (reuseWorkflowInfo && EndedWorkflowInfos.TryDequeue(out currentWorkflowInfo))
                {
                    currentWorkflowInfo.ReUse(sessionId, theFunction, theWorkflowApp, thePreviousWorkflow);
                }
                else
                {
                    currentWorkflowInfo = new WorkflowInfo(sessionId, theFunction, theWorkflowApp, thePreviousWorkflow);

                }
            }

            WorkflowInfos[theWorkflowApp.Id] = currentWorkflowInfo;
            SessionWorkflows[sessionId] = theWorkflowApp.Id;

            currentWorkflowInfo.TheWorkflow.Idle = (x) =>
            {
                WorkflowInfo workflowInfo = GetWorkflowBy(x.InstanceId);
                workflowInfo.IsIdle = true;
            };
            currentWorkflowInfo.TheWorkflow.Completed = (x) =>
            {
                if (x.CompletionState != ActivityInstanceState.Canceled)
                {
                    WorkflowInfo workflowInfo = GetWorkflowBy(x.InstanceId);
                    if (x.CompletionState == ActivityInstanceState.Faulted)
                    {
                        workflowInfo.IsFaulted = true;
                        workflowInfo.TheError = x.TerminationException;
                        workflowInfo.AbortWorkflow();
                    }
                    else
                    {
                        workflowInfo.IsCompleted = true;
                        workflowInfo.CompleteWorkflow();
                        //SafeServiceLocator<IDbContext>.GetService().CommitChanges(DataSourceFactory.GetDataSource(DataCategory.Shared));
                    }
                }
                else
                {
                    WorkflowInfo workflowInfo = GetWorkflowBy(x.InstanceId);
                    workflowInfo.CancelWorkflow();
                }
            };

            currentWorkflowInfo.TheWorkflow.PersistableIdle = new Func<WorkflowApplicationIdleEventArgs, PersistableIdleAction>((x) =>
            {
                return PersistableIdleAction.Unload;
            });

            return currentWorkflowInfo;
        }

        public WorkflowInfo GetWorkflowBy(Guid workflowInstanceId)
        {
            WorkflowInfo result = null;
            if (WorkflowInfos.ContainsKey(workflowInstanceId))
            {
                result = WorkflowInfos[workflowInstanceId];
            }
            return result;
        }

        public WorkflowInfo GetWorkflowBySessionID(string sessionId)
        {
            WorkflowInfo result = null;
            if (SessionWorkflows.ContainsKey(sessionId))
            {
                Guid workflowId = SessionWorkflows[sessionId];
                result = GetWorkflowBy(workflowId);
            }
            return result;
        }

        public WorkflowInfo GetInitiatingWorkflowByPhoneNo(string phoneNo)
        {
            WorkflowInfo result = null;
            if (InitiatingWorkflows.ContainsKey(phoneNo))
            {
                Guid workflowId = InitiatingWorkflows[phoneNo];
                result = GetWorkflowBy(workflowId);
            }
            return result;
        }

        public bool RemoveWorkflow(Guid workflowInstanceId)
        {
            WorkflowInfo theWorkflowInfo;
            bool result = WorkflowInfos.TryRemove(workflowInstanceId, out theWorkflowInfo);
            if (result)
            {
                //theWorkflowInfo.TheWorkflow.Abort("Workflow removed");
                try
                {
                    Trace.TraceInformation("Aborting Workflow- {0}", workflowInstanceId);
                    theWorkflowInfo.TheWorkflow.Abort();

                    //Remove Previous workflows
                    if (theWorkflowInfo.ThePreviousWorkflow != null && theWorkflowInfo.ThePreviousWorkflow.TheWorkflow != null)
                    {
                        RemoveWorkflow(theWorkflowInfo.ThePreviousWorkflow.TheWorkflow.Id);
                    }

                     theWorkflowInfo.Dispose();
                     var reuseWorkflowInfo = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ReUseWorkflow"]);
                     if (reuseWorkflowInfo)
                     {
                         EndedWorkflowInfos.Enqueue(theWorkflowInfo);
                     }
                     else
                     {
                         theWorkflowInfo = null;
                     }
                }
                catch (WorkflowApplicationAbortedException ex)
                {
                    Trace.TraceInformation("Caught WorkflowApplicationAbortedException");
                }
            }
            return result;
        }

        public bool RemoveWorkflow(string sessionId)
        {
            Guid theWorkflowId;
            bool result = SessionWorkflows.TryRemove(sessionId, out theWorkflowId);
            if (result)
            {
                RemoveWorkflow(theWorkflowId);
            }
            return result;
        }

    }

    public class WorkflowInfo : IDisposable
    {
        public void Dispose()
        {
            ThePreviousWorkflow = null;
            TheFunction = null;
            TheTenant = null;
            TheError = null;
            ContextInputs = null;

            OnCompleted = null;
            OnAborted = null;
            OnCancelled = null;
            OnIdle = null;

            Outputs = null;
            if (TheWorkflow != null)
            {
                try
                {
                    TheWorkflow.Abort();
                }
                catch { }
                TheWorkflow = null;
            }
            InitiatedBy = null;
            GeoLocation = null;
            AttachedBookmarkCallback = null;
            AttachedFaultCallback = null;
            AttachedCompletedCallback = null;
            GotoFlow = false;
            IsFinalScreen = false;
            SessionID = null;
            InitiatingFlowID = null;
            XmlDisplayMessage = null;
            DisplayMessage = null;
        }

        public IDictionary<string, string> Outputs { get; set; }
        public WorkflowApplication TheWorkflow { get; set; }
        public IMobileAccount InitiatedBy { get; set; }
        public string GeoLocation { get; set; }
        public string BookmarkToResume { get; set; }
        public BookmarkCallback AttachedBookmarkCallback { get; set; }
        public FaultCallback AttachedFaultCallback { get; set; }
        public CompletionCallback AttachedCompletedCallback { get; set; }
        public bool IsIdle{ get; set; }
        public bool IsCompleted { get; set; }
        public bool IsFaulted { get; set; }
        public bool IncludeXML { get; set; }
        public bool GotoFlow { get; set; }
        public bool ChildTransactionSuccessful { get; set; }
        public bool ChildTransactionCompleted { get; set; }
        public string FailureReason { get; set; }

        public event EventHandler OnCompleted;
        public event EventHandler OnAborted;
        public event EventHandler OnCancelled;
        public event EventHandler OnIdle;

        public bool HasPreviousWorkflow
        {
            get
            {
                return ThePreviousWorkflow != null;
            }
        }
        public WorkflowInfo ThePreviousWorkflow { get; set; }
        public IWFActivity TheFunction { get; set; }
        public IInstitution TheTenant { get; set; }
        public Exception TheError { get; set; }
        public IDictionary<string, object> ContextInputs { get; set; }

      
        public BankOneMobile.Data.Contracts.IDataSource DataSource
        {
            get
            {
                BankOneMobile.Data.Contracts.IDataSource dataSource = DataSourceFactory.GetDataSource(DataCategory.Shared, TheWorkflow.Id);
                if (TheTenant != null)
                {
                    dataSource = DataSourceFactory.GetDataSource(DataCategory.Tenant, TheTenant, TheWorkflow.Id);
                }
                return dataSource;
            }
        }

        public void AbortWorkflow()
        {
            if (OnAborted != null)
            {
                OnAborted(this, new EventArgs());
            }
        }

        public void CancelWorkflow()
        {
            if (OnCancelled != null)
            {
                OnCancelled(this, new EventArgs());
            }
        }

        public bool IsFinalScreen { get; set; }

        public void CompleteWorkflow()
        {
          
            if (OnCompleted != null)
            {
                OnCompleted(this, new EventArgs());
            }
        }

        public Guid? InitiatingFlowID { get; set; }

        public WorkflowInfo(string sessionId, IWFActivity theFunction, WorkflowApplication theWorkflow, WorkflowInfo thePreviousWorkflow)
        {
            SessionID = sessionId;
            Outputs = new Dictionary<string, string>();
            TheWorkflow = theWorkflow;
            ThePreviousWorkflow = thePreviousWorkflow;            
            IsIdle = false;
            IsCompleted = false;
            IsFaulted = false;
            TheFunction = theFunction;
            GotoFlow = false;
            ContextInputs = new Dictionary<string, object>();
        }

        public void ReUse(string sessionId, IWFActivity theFunction, WorkflowApplication theWorkflow, WorkflowInfo thePreviousWorkflow)
        {
            SessionID = sessionId;
            Outputs = new Dictionary<string, string>();
            TheWorkflow = theWorkflow;
            ThePreviousWorkflow = thePreviousWorkflow;
            IsIdle = false;
            IsCompleted = false;
            IsFaulted = false;
            TheFunction = theFunction;
            GotoFlow = false;
            ContextInputs = new Dictionary<string, object>();
        }

        public string SessionID
        {
            get;
            set;
        }

        public string DisplayMessage
        {
            get;
            set;
        }

        public string XmlDisplayMessage
        {
            get;
            set;
        }
      
        public string Start()
        {
            
            this.TheWorkflow.Run();
            
            
            return this.IncludeXML ? this.ProcessXML() : this.Process();
            //return this.Process();
            //return this.ProcessXML();
        }

        public string Resume(string outputs)
        {
            this.IsIdle = false;
            this.TheWorkflow.ResumeBookmark(this.BookmarkToResume, outputs);
            
            
            return this.IncludeXML ? this.ProcessXML() : this.Process();
            //return this.ProcessXML();
        }

        private string Process()
        {
            while (!this.IsIdle && !this.IsCompleted && !this.IsFaulted)
            {
                Thread.Sleep(100);
            }

            if (this.IsIdle)
            {
                if (!String.IsNullOrWhiteSpace(this.DisplayMessage))
                {
                    return this.DisplayMessage;
                }   
            }
            else if (this.IsFaulted)
            {
                this.IsFinalScreen = true;
                return string.Format("Error processing flow: {0}", this.TheError.Message);
            }
            else if (this.IsCompleted)
            {
                this.IsFinalScreen = true;
                if (this.HasPreviousWorkflow)
                {
                    this.ThePreviousWorkflow.InitiatingFlowID = null;
                    return string.Format("{0}", this.DisplayMessage);
                }
                else
                {
                    return string.Format("{0}", this.DisplayMessage);
                }
            }
            return string.Empty;
        }

        private string ProcessXML()
        {
            while (!this.IsIdle && !this.IsCompleted && !this.IsFaulted)
            {
                Thread.Sleep(100);
            }

            if (this.IsIdle)
            {
                if (!String.IsNullOrWhiteSpace(this.XmlDisplayMessage))
                {
                    return this.XmlDisplayMessage;
                }
            }
            else if (this.IsFaulted)
            {
                this.IsFinalScreen = true;
                return string.Format("Error processing flow: {0}", this.TheError.Message);
            }
            else if (this.IsCompleted)
            {
                this.IsFinalScreen = true;
                if (this.HasPreviousWorkflow)
                {
                    this.ThePreviousWorkflow.InitiatingFlowID = null;
                    return string.Format("{0}", this.XmlDisplayMessage);
                }
                else
                {
                    return string.Format("{0}", this.XmlDisplayMessage);
                }
            }
            return string.Empty;
        }

       
    }
}
