﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using SOA.Framework.FunctionHelper;
using SOA.Framework.AS;
using SOA.Framework.FunctionHelper.DAO;
using PANE.Framework.Utility;
using PANE.Framework.DTO;
using PANE.Framework.NHibernateManager.Configuration;

using PANE.Framework.AuditTrail.DTO;
using PANE.Framework.AuditTrail.Attributes;
using BankOneMobile.Core.Implementations;
using PANE.Framework.Functions.DTO;

namespace BankOneMobile.Services.Utility
{
    public delegate T InsertOrUpdateAction<T>(T x);
    public delegate object InsertOrUpdateAction2<T>(T x);
    public delegate T RetrieveAction<T>(string mfbCode, long id);
    public delegate bool PreValidateAction<T>(T x, bool isUpdate);
    public delegate bool PreLogForApprovalAction<T>(T x);
    public delegate T RefreshAction<T>(T x, DatabaseSource dbSource);


    public static class ApprovalUtil
    {

        public static string APP_ENDPOINT = "BankOneMobile_Approval";
        public static string APP_NAME = "Mobile";

        /// <summary>
        /// Used to either update an object straight or Log for Approval.
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="updateMethod">The update method to perform if the current object has no approval enabled for it. </param>
        /// <param name="objectToUpdate">The object to update.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="retrieveMethod">The retrieve method to perform on an object if the object is to be logged for approval.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object UpdateOrLogForApproval<T>(string mfbCode, InsertOrUpdateAction<T> updateMethod, T objectToUpdate, string objectName, RefreshAction<T> refreshMethod, DatabaseSource dbSource, string functionName)
        {
            return UpdateOrLogForApproval<T>(mfbCode, null, updateMethod, objectToUpdate, objectName, refreshMethod, dbSource, functionName);
        }

        /// <summary>
        /// Used to either save an object straight or Log for Approval. 
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="insertMethod">The insert method to perform if the current object has no approval enabled for it. </param>
        /// <param name="insertObject">The object to insert.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object SaveOrLogForApproval<T>(string mfbCode, InsertOrUpdateAction<T> insertMethod, T insertObject, string objectName, string functionName)
        {
            return SaveOrLogForApproval<T>(mfbCode, null, insertMethod, insertObject, objectName, functionName);
        }

        public static object DoActionOrLogForApproval<T>(string mfbCode, PreLogForApprovalAction<T> preLogForApprovalAction, InsertOrUpdateAction2<T> insertMethod, T insertObject, string objectName, string functionName)
        {
            new PANE.ERRORLOG.Error().LogToFile(new Exception("Starting of Approval"));
            //Retrieve the Current User and the FunctionItem first.
            SOA.Framework.MS.MembershipUserItem user = (Membership.GetUser() as FunctionsMembershipUser).UserDetails as SOA.Framework.MS.MembershipUserItem;
            
            
            if (user == null)
            { return null; }

            IAuditable auditable = insertObject as IAuditable;
            if (auditable != null)
            {
                auditable.AuditableUser = user as IUser;
            }
            FunctionItem functionItem = FunctionItemDAO.RetrieveByRoleName(functionName, (PANE.Framework.Functions.DTO.UserCategory)user.Role.UserCategory);

            //Check approval config. whether or not this object needs to be approved.
            //using (ApprovalConfigServiceClient client = new ApprovalConfigServiceClient())
            ApprovalConfigServiceClient client = null;
            try
            {
                client = new ApprovalConfigServiceClient();
                bool isApprovable = client.RetrieveByMakerRoleName(mfbCode, functionName, functionItem.ID, ApprovalUtil.APP_NAME);
                if (!isApprovable)
                {
                    //Save straight-away.
                    return insertMethod.Invoke(insertObject);
                }
                else
                {
                    if (preLogForApprovalAction != null)
                    {
                        preLogForApprovalAction.Invoke(insertObject);
                    }

                    //Create Approval log.
                    
                    return client.CreateAddApproval(mfbCode, GetDataTypeName(insertObject), objectName, (insertObject as BankOneMobile.Core.Contracts.IEntity).ID, functionItem.ID,
                       BinarySerializer.SerializeObject(insertObject), ApprovalUtil.APP_NAME, ApprovalUtil.APP_ENDPOINT, user.ID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                
                client.Close();
            }
        }



        /// <summary>
        /// Used to either save an object straight or Log for Approval. 
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="preDeleteAction">The action to be performed if there is any thing to be validated before deleting.</param>
        /// <param name="insertObject">The object to insert.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object DeleteOrLogForApproval<T>(string mfbCode, InsertOrUpdateAction<T> deleteMethod, T objectToDelete, string objectName, string functionName)
        {
            return DeleteOrLogForApproval<T>(mfbCode, null, deleteMethod, objectToDelete, objectName, functionName);
        }

        /// <summary>
        /// Used to either save an object straight or Log for Approval. 
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="preSaveAction">The action to be performed if there is any thing to be validated before saving.</param>
        /// <param name="insertMethod">The insert method to perform if the current object has no approval enabled for it. </param>
        /// <param name="insertObject">The object to insert.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object SaveOrLogForApproval<T>(string mfbCode, PreValidateAction<T> preSaveAction, InsertOrUpdateAction<T> insertMethod, T insertObject, string objectName, string functionName)
        {
            if (preSaveAction != null)
            {
                preSaveAction.Invoke(insertObject, false);
            }
            SOA.Framework.MS.MembershipUserItem user = null;
            //Retrieve the Current User and the FunctionItem first.
            try
            {
               user  = (Membership.GetUser() as FunctionsMembershipUser).UserDetails as SOA.Framework.MS.MembershipUserItem;
            }
            catch
            {
            }
            if (user == null)
            { return null; }

            IAuditable auditable = insertObject as IAuditable;
            if (auditable != null)
            {
                auditable.AuditableUser = user as IUser;
            }
            FunctionItem functionItem = FunctionItemDAO.RetrieveByRoleName(functionName, (PANE.Framework.Functions.DTO.UserCategory)user.Role.UserCategory);

            //Check approval config. whether or not this object needs to be approved.
            //using (ApprovalConfigServiceClient client = new ApprovalConfigServiceClient())
            ApprovalConfigServiceClient client = null;
            try
            {
                client = new ApprovalConfigServiceClient();
                bool isApprovable = client.RetrieveByMakerRoleName(mfbCode, functionName, functionItem.ID, ApprovalUtil.APP_NAME);
                if (!isApprovable)
                {
                    //Save straight-away.
                    return insertMethod.Invoke(insertObject);
                }
                else
                {
                    //Create Approval log.
                    return client.CreateAddApproval(mfbCode, GetDataTypeName(insertObject), objectName, (insertObject as Entity).ID, functionItem.ID,
                        BinarySerializer.SerializeObject(insertObject), ApprovalUtil.APP_NAME, ApprovalUtil.APP_ENDPOINT, user.ID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                client.Close();
            }
        }


        /// <summary>
        /// Used to either update an object straight or Log for Approval.
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="preSaveAction">The action to be performed if there is any thing to be validated before updating.</param>
        /// <param name="updateMethod">The update method to perform if the current object has no approval enabled for it. </param>
        /// <param name="objectToUpdate">The object to update.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="refreshMethod">The refresh method to perform on an object to get its previous state so it can be logged for approval.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object UpdateOrLogForApproval<T>(string mfbCode, PreValidateAction<T> preUpdateAction, InsertOrUpdateAction<T> updateMethod, T objectToUpdate, string objectName, RefreshAction<T> refreshMethod, DatabaseSource dbSource, string functionName)
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("UpdateOrLogForApproval"));
            if (preUpdateAction != null)
            {
                preUpdateAction(objectToUpdate, true);
            }

            //Retrieve the Current User and the FunctionItem first.
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Get User"));
            SOA.Framework.MS.MembershipUserItem user = (Membership.GetUser() as FunctionsMembershipUser).UserDetails as SOA.Framework.MS.MembershipUserItem;
            if (user == null)
            { return null; }

            IAuditable auditable = objectToUpdate as IAuditable;
            if (auditable != null)
            {
                auditable.AuditableUser = user as IUser;
            }
            FunctionItem functionItem = FunctionItemDAO.RetrieveByRoleName(functionName, (PANE.Framework.Functions.DTO.UserCategory)user.Role.UserCategory);

            //Check approval config. whether or not this object needs to be approved.
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Approval Config"));
            using (ApprovalConfigServiceClient client = new ApprovalConfigServiceClient())
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Approval Config Client"));
                bool isApprovable = client.RetrieveByMakerRoleName(mfbCode, functionName, functionItem.ID, ApprovalUtil.APP_NAME);
                if (!isApprovable)
                {
                    //Save straight-away.
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to Invoke"));
                    return updateMethod.Invoke(objectToUpdate);
                }
                else
                {
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception("Save For Approval"));
                    byte[] newObject = BinarySerializer.SerializeObject(objectToUpdate);


                    long objectID = (objectToUpdate as Entity).ID;
                    //Get the object just before any update was performed on it.
                    //T objectBeforeUpdate = refreshMethod.Invoke(mfbCode, objectID, dbSource);
                     objectToUpdate = refreshMethod.Invoke(objectToUpdate, dbSource);
                    //Create Approval log.
                     PANE.ERRORLOG.ErrorLogger.Log(new Exception("Create Approval"));
                    return client.CreateEditApproval(mfbCode, GetDataTypeName(objectToUpdate), objectName, objectID, functionItem.ID,
                        BinarySerializer.SerializeObject(objectToUpdate), newObject, ApprovalUtil.APP_NAME, ApprovalUtil.APP_ENDPOINT, user.ID);
                }
            }
        }

        /// <summary>
        /// Used to either save an object straight or Log for Approval. 
        /// </summary>
        /// <typeparam name="T">The object.</typeparam>
        /// <param name="mfbCode">The mfb Code of the current MFB.</param>
        /// <param name="preDeleteAction">The action to be performed if there is any thing to be validated before deleting.</param>
        /// <param name="insertMethod">The insert method to perform if the current object has no approval enabled for it. </param>
        /// <param name="insertObject">The object to insert.</param>
        /// <param name="objectName">The name of the object e.g. obj.Name.</param>
        /// <param name="functionName">The name of the function. e.g. AddEditUser</param>
        /// <returns><see cref="T"/>, if not saved for approval, else an approval object.</returns>
        public static object DeleteOrLogForApproval<T>(string mfbCode, PreValidateAction<T> preDeleteAction, InsertOrUpdateAction<T> deleteMethod, T deleteObject, string objectName, string functionName)
        {
            if (preDeleteAction != null)
            {
                preDeleteAction.Invoke(deleteObject, false);
            }

            //Retrieve the Current User and the FunctionItem first.
            SOA.Framework.MS.MembershipUserItem user = (Membership.GetUser() as FunctionsMembershipUser).UserDetails as SOA.Framework.MS.MembershipUserItem;
            if (user == null)
            { return null; }

            IAuditable auditable = deleteObject as IAuditable;
            if (auditable != null)
            {
                auditable.AuditableUser = user as IUser;
            }
            FunctionItem functionItem = FunctionItemDAO.RetrieveByRoleName(functionName, (PANE.Framework.Functions.DTO.UserCategory)user.Role.UserCategory);

            //Check approval config. whether or not this object needs to be approved.
            using (ApprovalConfigServiceClient client = new ApprovalConfigServiceClient())
            {
                bool isApprovable = client.RetrieveByMakerRoleName(mfbCode, functionName, functionItem.ID, ApprovalUtil.APP_NAME);
                if (!isApprovable)
                {
                    //Save straight-away.
                    return deleteMethod.Invoke(deleteObject);
                }
                else
                {
                    //Create Approval log.
                    return client.CreateDeleteApproval(mfbCode, GetDataTypeName(deleteObject), objectName, (deleteObject as   Entity).ID, functionItem.ID,
                       BinarySerializer.SerializeObject(deleteObject), ApprovalUtil.APP_NAME, ApprovalUtil.APP_ENDPOINT, user.ID);
                }
            }
        }

        private static string GetDataTypeName(object obj)
        {
            string dataType = ((TrailableAttribute)obj.GetType().GetCustomAttributes(typeof(TrailableAttribute), false)[0]).LoggedName;
            if (String.IsNullOrEmpty(dataType)) dataType = obj.GetType().Name;
            dataType = PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(dataType);

            return dataType;
        }


    }
}