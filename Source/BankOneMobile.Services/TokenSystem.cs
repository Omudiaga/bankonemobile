﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers;
using System.Data;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.Services
{
   public class TokenSystem
    {
       private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
       private ITokenRepository _repository = SafeServiceLocator<ITokenRepository>.GetService();

     public TokenSystem()
       {

       }

     public TokenSystem(IDataSource dataSource)
       {
           _theDataSource = dataSource;
       }
     public IList<IToken> GetToken (IDataSource dataSource,string phoneNumber,string tokenValue)
     {
         IList<IToken> token = null;
         token = _repository.GetByValueAndPhoneNumber(dataSource,phoneNumber,tokenValue);
         return token;
     }
     public IToken SaveToken(IToken token)
     {
         token.IsConsumed = false;
         token.DateConsumed = (DateTime)System.Data.SqlTypes.SqlDateTime.Null;
         token.DateGenerated = DateTime.Now;
         _repository.DbContext.BeginTransaction(_theDataSource);
         var result = _repository.Save(_theDataSource, token);
         _repository.DbContext.CommitTransaction(_theDataSource);
         return result;

     }
     public IToken CosumeToken(IToken token)
     {
         token.IsConsumed = true;
         token.DateConsumed = DateTime.Now;
         var result = UpdateSession(token as Token);
         return result;

     }

     public IToken UpdateSession(Token token)
     {
         _repository.DbContext.BeginTransaction(_theDataSource);
         var result = _repository.Update(_theDataSource, token);
         _repository.DbContext.CommitTransaction(_theDataSource);
         return result;

     }
       
     public IToken GenerateToken(string  mob, string  inst)
     {
         IToken toReturn = new Token();
         toReturn.TokenValue = GenerateToken();
         //IMobileAccount mobile = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(mob);
         IMobileAccount mobile = new MobileAccountSystem(_theDataSource).GetMobileAccountByPhoneNumber(inst, mob);

         if (mobile == null)
         {
             throw new InvalidPhoneNumberException("Invalid Phone Number");
         }
         IInstitution institution = new InstitutionSystem().GetByCode(_theDataSource, inst);
         if (inst == null)
         {
             throw new InvalidInstitutionException("Instution Code is Invalid");
         }
         toReturn.PhoneNumber = mobile.StandardPhoneNumber;
         toReturn.TheMobileAccount = mobile;
         toReturn.TheInstitution = institution;
         SaveToken(toReturn);

         return toReturn;
     }
     public IToken GenerateNonAccountHolderToken(string mobile, string inst)
     {
         IToken toReturn = new Token();
         toReturn.TokenValue = GenerateToken();
         IInstitution institution = new InstitutionSystem().GetByCode(_theDataSource, inst);
         if (institution == null)
         {
             throw new InvalidInstitutionException("Instution Code is Invalid");
         }
         toReturn.PhoneNumber = "+234" + mobile.TrimStart("0".ToCharArray());
         toReturn.TheInstitution = institution;
         SaveToken(toReturn);

         return toReturn;
     }

     //public bool ValidateToken(string institutionCode, string mobile, string tokenValue)
     //{
     //    bool toReturn = false;
     //    IToken token = null;

     //    string message = String.Format("ValidateToken- MObile PhONE -{0}, tOKEN-{1}", mobile, tokenValue);
     //    IMobileAccount mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(mobile);
     //    PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
     //    if (mob == null)
     //    {
     //        throw new InvalidPhoneNumberException("Invalid Phone Number");
     //    }
     //    IList<IToken> tk = GetToken(_theDataSource, mob.StandardPhoneNumber, tokenValue);
     //    if (tk != null && tk.Count() > 0)
     //    {
     //        token = tk.Where(x => !x.IsExpired).FirstOrDefault();// Get all tokens for that phone number and token value that havent been consumed and then locate the one that hasnt expired

     //        if (token == null)
     //        {
     //            throw new TokenExpiredException("Token has Expired");

     //        }
     //        else
     //        {
     //            new TokenSystem(_theDataSource).CosumeToken(token);
     //            toReturn = true;
     //        }
     //    }
     //    else
     //    {
     //        throw new InvalidTokenException("Invalid Token");
     //    }
     //    return toReturn;
     //}
     public bool ValidateToken(string institutionCode, string  mobile, string tokenValue)
     {
         bool toReturn = false;
         IToken token = null;

         string message = String.Format("ValidateToken- MObile PhONE -{0}, tOKEN-{1}", mobile, tokenValue);
         //IMobileAccount mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(mobile);
         IMobileAccount mob = new MobileAccountSystem(_theDataSource).GetMobileAccountByPhoneNumber(institutionCode, mobile);
         PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
         if (mob == null)
         {
             throw new InvalidPhoneNumberException("Invalid Phone Number");
         }
         IList<IToken> tk = GetToken(_theDataSource, mob.StandardPhoneNumber, tokenValue);
         if (tk != null&&tk.Count()>0)
         {
             token = tk.Where(x => !x.IsExpired).FirstOrDefault();// Get all tokens for that phone number and token value that havent been consumed and then locate the one that hasnt expired

             if (token == null)
             {
                 throw new TokenExpiredException("Token has Expired");             
                 
             }
             else
             {
                 new TokenSystem(_theDataSource).CosumeToken(token);
                 toReturn = true;
             }
         }
         else 
         {
             throw new InvalidTokenException("Invalid Token");
         }
         return toReturn;
     }
     public bool ValidateNonAccountHolderToken(string mobile, string tokenValue)
     {
         bool toReturn = false;
         IToken token = null;

         string message = String.Format("ValidateNonAccountHolderTokenMethod- MObile PhONE -{0}, tOKEN-{1}", mobile, tokenValue);
         
         PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
         IList<IToken> tk = GetToken(_theDataSource, "+234" + mobile.TrimStart("0".ToCharArray()), tokenValue);
         if (tk != null && tk.Count() > 0)
         {
             token = tk.Where(x => !x.IsExpired).FirstOrDefault();// Get all tokens for that phone number and token value that havent been consumed and then locate the one that hasnt expired

             if (token == null)
             {
                 throw new TokenExpiredException("Token has Expired");

             }
             else
             {
                 new TokenSystem(_theDataSource).CosumeToken(token);
                 toReturn = true;
             }
         }
         else
         {
             throw new InvalidTokenException("Invalid Token");
         }
         return toReturn;
     }

     private string GenerateToken()
     {
         string toReturn = string.Empty;
         Random rand = new Random();
         lock (this)
         {
             toReturn = rand.Next(10000, 99999).ToString();
         }
         return toReturn;
     }
     public string GenerateTokenServiceMethod(string  phoneNumber, string  instCode)
     {
         string response = string.Empty;
         using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
         {
             response = client.GenerateToken(phoneNumber, instCode);
         }
         return response;
     }
     public string GenerateNonAccountHolderTokenServiceMethod(string phoneNumber, string instCode)
     {
         string response = string.Empty;
         using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
         {
             response = client.GenerateNonAccountHolderToken(phoneNumber, instCode);
         }
         return response;
     }
     public bool ValidateTokenMethod(string institutionCode, string  phoneNumber, string enteredToken)
     {
         string message = String.Format("ValidateTokenMethod MObile PhONE -{0}, tOKEN-{1}, InstitutionCode- {2}", phoneNumber, enteredToken, institutionCode);
         bool response = false;
         using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
         {
           
             response = client.ValidateToken(institutionCode,phoneNumber, enteredToken);
         }
         return response;
     }
     public bool ValidateNonAccountHolderTokenMethod(string phoneNumber, string enteredToken)
     {
         string message = String.Format("ValidateNonAccountHolderTokenMethod MObile PhONE -{0}, tOKEN-{1}", phoneNumber, enteredToken);
         bool response = false;
         using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
         {

             response = client.ValidateNonAccountHolderToken(phoneNumber, enteredToken);
         }
         return response;
     }
 

       
    }

}
