using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace BankOneMobile.Services.Configuration
{
    public class ConfigurationManager
    {
        private static NameValueCollection USSDSessionConfig
        {
            get
            {
                return System.Configuration.ConfigurationManager.GetSection("USSDSessionConfig") as NameValueCollection;
            }
        }
        private static NameValueCollection HSMConfig
        {
            get
            {
                return System.Configuration.ConfigurationManager.GetSection("HSMConfig") as NameValueCollection;
            }
        }
        public static string NoOfRetries
        {
            get
            {
                return HSMConfig["NoOfRetries"];
            }

        }
        public static string HSMServer
        {
            get
            {
                return HSMConfig["HSMServer0"];
            }

        }
        public static string HSMServerN(int n)
        {
             string key = string.Format("HSMServer{0}", n);
           
            return HSMConfig[key];

        }

        public static string ZMKComponent1
        {
            get
            {
               return HSMConfig["ZMKComponent1"];
            }
        }
        public static string ZMKComponent2
        {
            get
            {
               return HSMConfig["ZMKComponent2"];
            }
        }
        public static string HSMPort
        {
            get
            {
                return HSMConfig["HSMPort0"];
            }
        }

        public static string HSMPortN(int n)
        {
            string key = string.Format("HSMPort{0}", n);
           
            return HSMConfig[key];
           
        }


        public static string HSMHeaderLength
        {
            get
            {
                return HSMConfig["HSMHeaderLength"];
            }
        }
        public static double USSDSessionInterval
        {
            get
            {
                return Convert.ToDouble(USSDSessionConfig["SessionInterval"]);
            }
        }


        public static decimal  AmountPerSession
        {
            get
            {
                return Convert.ToDecimal(USSDSessionConfig["AmountPerSession"]);
            }
        }
       

    }
}
