﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using BankOneMobile.SwitchIntegration.ISO8583.Client.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Messages;
using System.Net.Sockets;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services.Configuration;
using System.Diagnostics;
using BankOneMobile.Core.Contracts;


namespace BankOneMobile.Services
{
   public class HSMCenter
    {

       public static string GeneratePinBlock(string mobilePhone, string pin, AppZone.HsmInterface.PinBlockFormats format)
       {
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               string stringFormat = "00";
               switch (format)
               {
                   case AppZone.HsmInterface.PinBlockFormats.ANSI:
                       stringFormat = "01";
                       break;


               }
               //
               mobilePhone = mobilePhone.PadLeft(12, '0');
               string encryptionKey = new SystemConfigSystem(dataSource).GetDecryptedPEK(); // RetrievePinEncryptionKey();
               string toReturn = string.Empty;
               string pinLength = pin.Length.ToString();
               string clearPinValue = "0" + pinLength + pin + "FFFFFFFFFF";

               string clearPhone = "0000" + mobilePhone;



               //toReturn = (Convert.ToInt32(clearPinValue) ^ Convert.ToInt32(clearPhone)).ToString();
               toReturn = ThalesSim.Core.Utility.XORHexStringsFull(clearPinValue, clearPhone);
               //Trace.TraceInformation(toReturn);
               toReturn = ThalesSim.Core.Cryptography.TripleDES.TripleDESEncrypt(
              new ThalesSim.Core.Cryptography.HexKey(encryptionKey.Substring(0, 32)),
              toReturn);

               Trace.TraceInformation(mobilePhone + ";" + encryptionKey + ";" + pin + ";" + toReturn);
               return toReturn;
           }
       }
       
      /* public static string Generate48BitPinBlock(string mobilePhone, string pin, AppZone.HsmInterface.PinBlockFormats format)
       {
           string stringFormat = "00";
           switch (format)
           {
               case AppZone.HsmInterface.PinBlockFormats.ANSI:
                   stringFormat = "01";
                   break;


           }
           mobilePhone = mobilePhone.PadLeft(12, '0');
           string encryptionKey = new SystemConfigSystem().GetDecryptedPEK(); // RetrievePinEncryptionKey();
           string toReturn = string.Empty;
           string pinLength = pin.Length.ToString();
           string clearPinValue = ("0"+"0" + pinLength + pin).PadRight(48,'F');

           string clearPhone = "0000" + mobilePhone;



           //toReturn = (Convert.ToInt32(clearPinValue) ^ Convert.ToInt32(clearPhone)).ToString();
           toReturn = ThalesSim.Core.Utility.XORHexStringsFull(clearPinValue, clearPhone);
           toReturn = ThalesSim.Core.Cryptography.TripleDES.TripleDESEncrypt(
          new ThalesSim.Core.Cryptography.HexKey(encryptionKey.Substring(0, 32)),
          toReturn);
           return toReturn;
       }*/
       public static void InitialiseHSM ()
       {
           AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
           using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
           {

               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);

               try
               {
                   theHsm.Setup(hsmServer, port, headerLength);
               }
               catch (SocketException)
               {
                   throw new NoHSMResponseException("HSM Unavailable");
               }
               string zmkComp1 = ConfigurationManager.ZMKComponent1;
               string zmkComp2 = ConfigurationManager.ZMKComponent2;
               string zmkClear = ThalesSim.Core.Utility.XORHexStringsFull(zmkComp1, zmkComp2);
               Key pek = new SystemConfigSystem(source).GetPEK() as Key;
               string key = pek.ValueUnderParent; string encryptionKey = string.Empty;


               // ThalesSim.Core.Utility.ByteArrayToHexString(response.SessionKeyBytes, ref hexValue);
               //now we need to take the first 32 characters gotten from  the response and decrypt it using the the clear zmk which in itself is the xor value of the components
               //ThalesSim.Core.Utility.
               encryptionKey = ThalesSim.Core.Cryptography.TripleDES.TripleDESDecrypt(
               new ThalesSim.Core.Cryptography.HexKey(zmkClear),
               key.Substring(0, 32)// use only the first 32 digits
               );


               SystemConfig config = new SystemConfigSystem(source).GetConfig();
               config.PEK = encryptionKey;
               new SystemConfigSystem(source).UpdateSystemConfig(config);
           }
       }

       
       public static  string RetrievePinEncryptionKey()
       {
           using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
           {
               return new SystemConfigSystem(source).GetConfig().PEK;
           }
       }
       public string RetrievePinVerificationKey()
       {
           using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
           {
               return new SystemConfigSystem(source).GetConfig().PVK;
           }
       }
       public bool VerifyPinOnHSM(string phoneNumber, string offset, string encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats pinFormat)
       {
           string hsmConfigDetails = string.Empty;
           int maxRetryCount = Convert.ToInt32(Configuration.ConfigurationManager.NoOfRetries);
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               Trace.TraceInformation("Entered HSM ");
               bool toReturn = false;
               phoneNumber = phoneNumber.PadLeft(12, '0');
               SystemConfigSystem sys = new SystemConfigSystem(dataSource);
               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();

               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);


               theHsm.Setup(hsmServer, port, headerLength);
               hsmConfigDetails = string.Format("Server-{0} ;Port-{1}",hsmServer,port);

               Trace.TraceInformation("Getting PEK");

               string PEK = sys.GetPEK().ValueUnderLMK;
               Trace.TraceInformation("Got PEK Value");
               Trace.TraceInformation("Getting PVK");
               string PVK = sys.GetPVK();
               Trace.TraceInformation("Gotten PVK value");
               string pinValidationData = "N00000000000";
               int count = 0; string rsp = string.Empty;
               try
               {
                   Trace.TraceInformation(PEK + ";" + PVK + ";" + PEK + ";" + encryptedPinBlock.ToString() + ";" + pinFormat.ToString() + ";" + offset + ";" + phoneNumber);
                   
                   theHsm.PinVerifier().VerifyPIN(true, PEK, PVK, encryptedPinBlock, pinFormat, 4, phoneNumber, pinValidationData, offset);
                   Trace.TraceInformation("Finished HSM PIN verify"+ hsmConfigDetails);
                   toReturn = true;
               }
               catch (SocketException ex)
               {
                   count++;

                   /* We figured we ddint need to notify each time a HSM request failed. We would howevere notify if all Failed.
                   //Trace.TraceInformation("Error in HSM -{0}:{1}", ex.Message,hsmConfigDetails);
                   //string mailTitle = string.Format("The HSM {0} Gone Down. Please Restart Now",hsmConfigDetails);
                   //string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure",hsmConfigDetails);
                   //SendErrorMail(body, mailTitle);
                   */
                   
                    while (count < maxRetryCount)
                    {                        
                        bool verified = ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count, out rsp);
                        if (verified)
                        {
                            return true;
                        }
                        else if (rsp == "01")
                        {
                            return false;
                        }
                        else
                        {
                            count++;
                        }

                    }
                    Trace.TraceInformation("Giving Up. After Trying {0} times. The last HSM Server Used was-{1}", count, hsmConfigDetails);
                    
                  string  mailTitle = "SOS!!TANSACTIONS ARE FAILING NOW AS THERE ARE NO AVAILABLE HSMs TO HANDLE REQUESTS.";
                   string body = "ALL AVAILABLE HSMs NEED TO BE RESTARTED RIGHT NOW!!";
                    SendErrorMail(body, mailTitle);

                    throw new CustomHSMException(ex.Message);
               }
               catch (Exception ex)
               {
                   if (ex.Message == "01")
                   {
                       return false;
                   }
                   count++;
                   /* We figured we ddint need to notify each time a HSM request failed. We would howevere notify if all Failed.
                   Trace.TraceInformation("Error in HSM -{0}:{1}", ex.Message,hsmConfigDetails);
                   string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails, count);
                   SendErrorMail(body, mailTitle);
                   */
                   while (count < maxRetryCount)
                   {
                       bool verified = ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count, out rsp);
                       if (verified)
                       {
                           return true;
                       }
                       else if (rsp == "01")
                       {
                           return false;
                       }
                       else
                       {
                           count++;
                       }

                   }
                   Trace.TraceInformation("Giving Up. After Trying {0} times. The last HSM Server Used was-{1}",count,  hsmConfigDetails);
                   
                 string  mailTitle = "SOS!! TANSACTIONS ARE FAILING NOW AS THERE ARE NO AVAILABLE HSMs TO HANDLE REQUESTS.";
                 string  body = "ALL AVAILABLE HSMs NEED TO BE RESTARTED RIGHT NOW!!";
                   SendErrorMail(body, mailTitle);
                   throw new CustomHSMException(ex.Message);

               }
               finally
               {
               }
               return toReturn;
           }
       }

       public bool ReVerifyPinOnHSM(string phoneNumber, string offset, string encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats pinFormat,int count, out string response)
       {
           string responseCode = string.Empty;
           string hsmConfigDetails = string.Empty;
           
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               Trace.TraceInformation("Entered HSM");
               bool toReturn = false;
               phoneNumber = phoneNumber.PadLeft(12, '0');
               SystemConfigSystem sys = new SystemConfigSystem(dataSource);
               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();

               string hsmServer = Configuration.ConfigurationManager.HSMServerN(count);
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPortN(count));
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);

               if (string.IsNullOrEmpty(hsmServer)||string.IsNullOrEmpty(Convert.ToString(port)))
               {
                   throw new CustomHSMException(string.Format("No HSM Config for this config number{0}", count));
               }


               theHsm.Setup(hsmServer, port, headerLength);
               hsmConfigDetails = string.Format("Server-{0} ;Port-{1}", hsmServer, port);

               Trace.TraceInformation("Getting PEK");

               string PEK = sys.GetPEK().ValueUnderLMK;
               Trace.TraceInformation("Got PEK Value");
               Trace.TraceInformation("Getting PVK");
               string PVK = sys.GetPVK();
               Trace.TraceInformation("Gotten PVK value");
               string pinValidationData = "N00000000000";

               try
               {
                   Trace.TraceInformation(PEK + ";" + PVK + ";" + PEK + ";" + encryptedPinBlock.ToString() + ";" + pinFormat.ToString() + ";" + offset + ";" + phoneNumber);
                   theHsm.PinVerifier().VerifyPIN(true, PEK, PVK, encryptedPinBlock, pinFormat, 4, phoneNumber, pinValidationData, offset);
                   Trace.TraceInformation("Finished HSM PIN verify" + hsmConfigDetails);
                   response = "00";
                   toReturn = true;
               }
               catch (SocketException ex)
               {
                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails,count);
                   count++;
                   //if (count < maxRetryCount)
                   //{
                   //    ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count);
                   //}
                   //throw new NoHSMResponseException("HSM Unvailable");
                   responseCode = "02";
                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails, count);
                   string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails,count);
                  // SendErrorMail(body, mailTitle);


                   
               }
               catch (Exception ex)
               {
                   if (ex.Message == "01")
                   {

                       responseCode = "01";
                       return false;
                   }
                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails,count);
                   
                   //if (count < maxRetryCount)
                   //{
                   //   toReturn = ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count);
                       
                   //}
                   responseCode = "02";
                   if (toReturn)
                   {
                       responseCode = "00";
                       return true;
                   }
                   else
                   {
                      // throw new NoHSMResponseException(ex.Message);
                       responseCode = "02";
                   }
                   

               }
               finally
               {
                   response = responseCode;
               }
               return toReturn;
           }
       }
       public bool VerifyPinOnHSMForTest(string phoneNumber, string offset, string encryptedPinBlock, string PVK, string PEK, AppZone.HsmInterface.PinBlockFormats pinFormat)
       {
            Trace.TraceInformation("Entered HSM");
               bool toReturn = false;
               phoneNumber = phoneNumber.PadLeft(12, '0');
               
               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();

               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);


               theHsm.Setup(hsmServer, port, headerLength);
               Trace.TraceInformation("Getting PEK");

               
               Trace.TraceInformation("Got PEK Value");
               Trace.TraceInformation("Getting PVK");
               
               Trace.TraceInformation("Gotten PVK value");
               string pinValidationData = "N00000000000";

               try
               {
                   Trace.TraceInformation(PEK + ";" + PVK + ";" + PEK + ";" + encryptedPinBlock.ToString() + ";" + pinFormat.ToString() + ";" + offset + ";" + phoneNumber);
                   theHsm.PinVerifier().VerifyPIN(true, PEK, PVK, encryptedPinBlock, pinFormat, 4, phoneNumber, pinValidationData, offset);
                   Trace.TraceInformation("Finished HSM PIN verify");
                   toReturn = true;
               }
               catch (SocketException ex)
               {
                   Trace.TraceInformation("Error in HSM -{0}", ex.Message);
                   throw new NoHSMResponseException("HSM Unvailable: " + ex.Message);
               }
               catch (Exception ex)
               {
                   if (ex.Message == "01")
                   {
                       return false;
                   }
                   Trace.TraceInformation("Error in HSM -{0}", ex.Message);
                   throw new CustomHSMException(ex.Message);

               }
               finally
               {
                   //if (theHsm != null)
                   //{
                   //    theHsm.Close();
                   //}
               }
               return toReturn;
           
       }
       public string GeneratePINOffsetOnHSM(string phoneNumber,string clearPin)
       {
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               MobileAccountSystem sys = new MobileAccountSystem(dataSource);

               //IList<IMobileAccount> mobs =sys .GetAllMobileAccounts();
               //foreach (MobileAccount mob in mobs)
               //{
               //    if (!string.IsNullOrEmpty(mob.MobilePhone))
               //    {
               //        //if (string.IsNullOrEmpty(mob.PIN))
               //        //{

               //            mob.PIN = "0000";
               //       // }

               //        //if (!mob.PIN.ToLower().EndsWith("f"))
               //        //{
               //            mob.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.PIN);
               //            sys.UpdateMobileAccount(mob);
               //       // }
               //    }
               //}
               int maxRetryCount = Convert.ToInt32(Configuration.ConfigurationManager.NoOfRetries);
               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
               AppZone.HsmInterface.IGeneratePinOffsetResponse offsetResponse = null;
               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);
               theHsm.Setup(hsmServer, port, headerLength);
               string hsmConfigDetails = string.Format("Server-{0} ;Port-{1}",hsmServer,port);
               Trace.TraceInformation("About to encrypt clear pin-{0}",hsmConfigDetails);
               

               string pinValidationData = "N00000000000";

               //TODO Do actual HSM Call


               string toReturn = string.Empty;
               string pinVerificationKey = new SystemConfigSystem(dataSource).GetConfig().PVK;
               Trace.TraceInformation("Gotten PVK:{0}", pinVerificationKey);
               int count = 0; string rsp = string.Empty;
               try
               {
                   string encryptedPIN = theHsm.PinGenerator().EncryptClearPin(clearPin, phoneNumber).EncryptedPin;
                   Trace.TraceInformation("Abt toget PVK");
                   offsetResponse = theHsm.PinGenerator().GeneratePinOffset(pinVerificationKey, encryptedPIN, 4, phoneNumber, pinValidationData);

               }
               catch (Exception  ex)
               {
                   Trace.TraceInformation("Error in HSM -{0}:{1}", ex.Message, hsmConfigDetails);
                   //string mailTitle = string.Format("The HSM {0} Gone Down. Please Restart Now", hsmConfigDetails);
                   //string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure", hsmConfigDetails);
                   //SendErrorMail(body, mailTitle);
                   count++;
                   Trace.TraceInformation("Error in HSM -{0}:{1}", ex.Message, hsmConfigDetails);
                   while (count < maxRetryCount)
                   {
                       string generated = ReGeneratePINOffsetOnHSM(phoneNumber,clearPin,count,out  rsp);// ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count, out rsp);
                       if (rsp!="01")
                       {
                           return generated;
                       }
                      
                       else
                       {
                           count++;
                       }

                   }
                   Trace.TraceInformation("Giving Up. After Trying {0} times. The last HSM Server Used was-{1}", count, hsmConfigDetails);
                  string mailTitle = "SOS!! TRANSACTIONS ARE FAILING NOW AS THERE ARE NO AVAILABLE HSMs TO HANDLE REQUESTS.";
                  string body = "ALL AVAILABLE HSMs NEED TO BE RESTARTED RIGHT NOW!!";
                   SendErrorMail(body, mailTitle);

                   throw new CustomHSMException(ex.Message);
                   
               }
               //catch (Exception EX)
               //{
               //    Trace.TraceInformation(EX.Message);
               //}


               Trace.TraceInformation("gOTTEN OFFEST", offsetResponse.Offset);



               toReturn = offsetResponse.Offset;
               //AppZone.HsmInterface.PinBlockFormats.

               //toReturn = new Random().Next(1000, 9999).ToString();
               return toReturn;
           }
       }

       public string ReGeneratePINOffsetOnHSM(string phoneNumber, string clearPin, int count, out string response)
       {
           string responseCode = string.Empty;
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               MobileAccountSystem sys = new MobileAccountSystem(dataSource);

               //IList<IMobileAccount> mobs =sys .GetAllMobileAccounts();
               //foreach (MobileAccount mob in mobs)
               //{
               //    if (!string.IsNullOrEmpty(mob.MobilePhone))
               //    {
               //        //if (string.IsNullOrEmpty(mob.PIN))
               //        //{

               //            mob.PIN = "0000";
               //       // }

               //        //if (!mob.PIN.ToLower().EndsWith("f"))
               //        //{
               //            mob.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.PIN);
               //            sys.UpdateMobileAccount(mob);
               //       // }
               //    }
               //}

               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
               AppZone.HsmInterface.IGeneratePinOffsetResponse offsetResponse = null;
               string hsmServer = Configuration.ConfigurationManager.HSMServerN(count);
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPortN(count));
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);
               theHsm.Setup(hsmServer, port, headerLength);
               string hsmConfigDetails = string.Format("Server-{0} ;Port-{1}", hsmServer, port);
               Trace.TraceInformation("About to encrypt clear pin");
               string encryptedPIN = theHsm.PinGenerator().EncryptClearPin(clearPin, phoneNumber).EncryptedPin;
               Trace.TraceInformation("Abt toget PVK");

               string pinValidationData = "N00000000000";

               //TODO Do actual HSM Call


               string toReturn = string.Empty;
               string pinVerificationKey = new SystemConfigSystem(dataSource).GetConfig().PVK;
               Trace.TraceInformation("Gotten PVK:{0}", pinVerificationKey);
               try
               {
                   offsetResponse = theHsm.PinGenerator().GeneratePinOffset(pinVerificationKey, encryptedPIN, 4, phoneNumber, pinValidationData);
                   Trace.TraceInformation("Finished PIN Offset Generation" + hsmConfigDetails);
                   responseCode = "00";

               }
               catch (SocketException ex)
               {
                   count++;
                   response = "01";
                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails, count);
                   string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails, count);
                  // SendErrorMail(body, mailTitle);
                   return response;
                   //throw new NoHSMResponseException("HSM Unavailable");

               }
               catch (Exception ex)
               {
                   //Trace.TraceInformation(EX.Message);
                   
                   response = "01";
                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails, count);
                   //string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   //string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails, count);
                   //SendErrorMail(body, mailTitle);
                   return response;
               }

               finally
               {
                   response = responseCode;
               }
               Trace.TraceInformation("gOTTEN OFFEST", offsetResponse.Offset);



               toReturn = offsetResponse.Offset;
               //AppZone.HsmInterface.PinBlockFormats.

               //toReturn = new Random().Next(1000, 9999).ToString();
               return toReturn;


           }
       }
       public string GeneratePINOffsetOnHSMForTest(string phoneNumber, string clearPin,  string pinVerificationKey)
       {
           

               //IList<IMobileAccount> mobs =sys .GetAllMobileAccounts();
               //foreach (MobileAccount mob in mobs)
               //{
               //    if (!string.IsNullOrEmpty(mob.MobilePhone))
               //    {
               //        //if (string.IsNullOrEmpty(mob.PIN))
               //        //{

               //            mob.PIN = "0000";
               //       // }

               //        //if (!mob.PIN.ToLower().EndsWith("f"))
               //        //{
               //            mob.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, mob.PIN);
               //            sys.UpdateMobileAccount(mob);
               //       // }
               //    }
               //}

               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
               AppZone.HsmInterface.IGeneratePinOffsetResponse offsetResponse = null;
               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);
               theHsm.Setup(hsmServer, port, headerLength);
               Trace.TraceInformation("About to encrypt clear pin");
               string encryptedPIN = theHsm.PinGenerator().EncryptClearPin(clearPin, phoneNumber).EncryptedPin;
               Trace.TraceInformation("Abt toget PVK");

               string pinValidationData = "N00000000000";

               //TODO Do actual HSM Call


               string toReturn = string.Empty;
//               string pinVerificationKey = new SystemConfigSystem(dataSource).GetConfig().PVK;
               Trace.TraceInformation("Gotten PVK:{0}", pinVerificationKey);
               try
               {
                   offsetResponse = theHsm.PinGenerator().GeneratePinOffset(pinVerificationKey, encryptedPIN, 4, phoneNumber, pinValidationData);

               }
               catch (SocketException)
               {
                   throw new NoHSMResponseException("HSM Unavailable");
               }
               catch (Exception EX)
               {
                   Trace.TraceInformation(EX.Message);
               }


               Trace.TraceInformation("gOTTEN OFFEST", offsetResponse.Offset);



               toReturn = offsetResponse.Offset;
               //AppZone.HsmInterface.PinBlockFormats.

               //toReturn = new Random().Next(1000, 9999).ToString();
               return toReturn;
           
       }
       public string GeneratePinOfserWithPinBlock(string phoneNumber, string encryptedPIN)
       {
           
           Trace.TraceInformation(string.Format("In Pin Change-{0}",phoneNumber));
           string hsmConfigDetails = string.Empty;
           int maxRetryCount = Convert.ToInt32(Configuration.ConfigurationManager.NoOfRetries);

           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               SystemConfigSystem sys = new SystemConfigSystem(dataSource);
               string PEK = sys.GetPEK().ValueUnderLMK;
               string pinVerificationKey = sys.GetConfig().PVK;

               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
               AppZone.HsmInterface.IGeneratePinOffsetResponse offsetResponse = null;
               string hsmServer = Configuration.ConfigurationManager.HSMServer;
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPort);
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);
               hsmConfigDetails = string.Format("Server-{0} ;Port-{1}", hsmServer, port);
               Trace.TraceInformation(hsmConfigDetails);
               theHsm.Setup(hsmServer, port, headerLength);
               Trace.TraceInformation("Done Setup");


               //TODO Do actual HSM Call


               string toReturn = string.Empty;

               int count = 0; string rsp = string.Empty;


               try
               {
                   Trace.TraceInformation("Start Generate-{0} Encrypted Pin is {1} PEK is {2}", phoneNumber, encryptedPIN, PEK);
                   encryptedPIN = theHsm.PinTranslator().TranslateFromPinEncryptionKeyToStorageKey(true, PEK, encryptedPIN, AppZone.HsmInterface.PinBlockFormats.ANSI, phoneNumber).EncryptedPin;
                   string pinValidationData = "N00000000000";
                   Trace.TraceInformation("Actually Generate-{0} Encryption Pin is {1} Pin Verification Key is {2}", phoneNumber, encryptedPIN, pinVerificationKey);
                   offsetResponse = theHsm.PinGenerator().GeneratePinOffset(pinVerificationKey, encryptedPIN, 4, phoneNumber, pinValidationData);

               }
               catch (Exception ex)
               {

                   count++;
                   Trace.TraceInformation("Error in HSM -{0}:{1}", ex.Message, hsmConfigDetails);
                   string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails, count);
                   SendErrorMail(body, mailTitle);
                   while (count < maxRetryCount)
                   {
                       Trace.TraceInformation("Trying in Generate {0} Time", count);
                       string generated = ReGeneratePinOfserWithPinBlock(phoneNumber, encryptedPIN, count, out rsp);
                       if (rsp == "00")
                       {                                            
                           return generated;
                       }

                       else
                       {
                           count++;
                       }

                   }
                   Trace.TraceInformation("Giving Up. After Trying {0} times. The last HSM Server Used was-{1}", count, hsmConfigDetails);
                   mailTitle = "SOS!! TANSACTIONS ARE FAILING NOW AS THERE ARE NO AVAILABLE HSMs TO HANDLE REQUESTS.";
                   body = "ALL AVAILABLE HSMs NEED TO BE RESTARTED RIGHT NOW!!";
                   SendErrorMail(body, mailTitle);
                   throw new NoHSMResponseException("HSM Unavailable");
               }







               toReturn = offsetResponse.Offset;
               //AppZone.HsmInterface.PinBlockFormats.

               //toReturn = new Random().Next(1000, 9999).ToString();
               return toReturn;
           }
       }
       public string ReGeneratePinOfserWithPinBlock(string phoneNumber, string encryptedPIN, int count, out string response)
       {
           string responseCode = string.Empty;
           string hsmConfigDetails = string.Empty;
           int maxRetryCount = Convert.ToInt32(Configuration.ConfigurationManager.NoOfRetries);
           using (var dataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               Trace.TraceInformation(string.Format("In Regenerat Pin offset with pin block {0} times", count));
               SystemConfigSystem sys = new SystemConfigSystem(dataSource);
               string PEK = sys.GetPEK().ValueUnderLMK;
               string pinVerificationKey = sys.GetConfig().PVK;

               AppZone.HsmInterface.IHsm theHsm = new AppZone.HsmInterface.ThalesHsm();
               AppZone.HsmInterface.IGeneratePinOffsetResponse offsetResponse = null;
               string hsmServer = Configuration.ConfigurationManager.HSMServerN(count);
               int port = Convert.ToInt32(Configuration.ConfigurationManager.HSMPortN(count));
               int headerLength = Convert.ToInt32(Configuration.ConfigurationManager.HSMHeaderLength);
               theHsm.Setup(hsmServer, port, headerLength);
               hsmConfigDetails = string.Format("Server-{0} ;Port-{1}", hsmServer, port);
               Trace.TraceInformation(String.Format("Trying Load Balanced Regeneration  {0} Time(s) with details:{1}", count, hsmConfigDetails));
               
               string pinValidationData = "N00000000000";
               //TODO Do actual HSM Call


               string toReturn = string.Empty;


               try
               {
                   encryptedPIN = theHsm.PinTranslator().TranslateFromPinEncryptionKeyToStorageKey(true, PEK, encryptedPIN, AppZone.HsmInterface.PinBlockFormats.ANSI, phoneNumber).EncryptedPin;
                   offsetResponse = theHsm.PinGenerator().GeneratePinOffset(pinVerificationKey, encryptedPIN, 4, phoneNumber, pinValidationData);
                   Trace.TraceInformation("Finished HSM Pin Generation {0}-{1}" ,hsmConfigDetails,offsetResponse==null?"Offset Response is null":offsetResponse.Offset);
                   responseCode = "00";

               }
               catch (Exception ex)
               {

                   Trace.TraceInformation("Error in HSM -{0}:{1}. This has been tried {2} times", ex.Message, hsmConfigDetails, count);
                   string mailTitle = "A Particular HSM Has Gone Down. Please Restart Now";
                   string body = string.Format("The HSM ({0}) has gone down and needs to be restarted NOW! to prevent transaction failure. This is HSM Number {1} to go down", hsmConfigDetails, count);
                  // SendErrorMail(body, mailTitle);
                   count++;
                   //if (count < maxRetryCount)
                   //{
                   //    ReVerifyPinOnHSM(phoneNumber, offset, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI, count);
                   //}
                   //throw new NoHSMResponseException("HSM Unvailable");
                   responseCode = "02";
                   Trace.TraceInformation("Response Code Set");
                   
               }


               finally
               {
                   
                   response = responseCode;
                   Trace.TraceInformation(string.Format("About to Set Response Code -{0}", response));    
               }


               toReturn = offsetResponse == null ? response : offsetResponse.Offset;
               Trace.TraceInformation(string.Format("About To Return Value - {0}",toReturn));
               //AppZone.HsmInterface.PinBlockFormats.

               //toReturn = new Random().Next(1000, 9999).ToString();
               return toReturn;
           }
       }

       public static  bool  ResetAllPins()
       {
           try
           {
               using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
               {
                   MobileAccountSystem sys = new MobileAccountSystem(source);

                   IList<IMobileAccount> mobs = sys.GetAllMobileAccounts();
                   //List<IMobileAccount> mobsToUse = mobs.Except(mobs.Where(x=>x.PIN.EndsWith("F")).ToList()).ToList();

                   int i = 0;

                   foreach (MobileAccount mob in mobs)
                   {
                       if (!string.IsNullOrEmpty(mob.MobilePhone))
                       {
                           //if (string.IsNullOrEmpty(mob.PIN))
                           //{

                           // mob.PIN = "0000";
                           // }

                           //if (!mob.PIN.ToLower().EndsWith("f"))
                           //{
                           Trace.TraceInformation("Updating-{0}", mob.MobilePhone);
                           mob.PIN = new HSMCenter().GeneratePINOffsetOnHSM(mob.MobilePhone, "0000");
                           Trace.TraceInformation("Updated-{0}", mob.PIN);
                           sys.UpdateMobileAccount(mob);
                           i++;


                           // }
                       }
                   }

                   return true;
               }
           }
         catch  (Exception ex)
           {
               Trace.TraceInformation("HSM eRROR -{0}", ex.Message);
               throw;
              // return false;
           }

       }

       public static string GeneratePinBlockForHSMTest(string mobilePhone, string pin, AppZone.HsmInterface.PinBlockFormats format,  string valueUnderParent)
       {
           
              
               mobilePhone = mobilePhone.PadLeft(12, '0');
              string encryptionKey =  SystemConfigSystem.GetDecryptedPEKForTest(valueUnderParent); // RetrievePinEncryptionKey();
               string toReturn = string.Empty;
               string pinLength = pin.Length.ToString();
               string clearPinValue = "0" + pinLength + pin + "FFFFFFFFFF";

               string clearPhone = "0000" + mobilePhone;



               //toReturn = (Convert.ToInt32(clearPinValue) ^ Convert.ToInt32(clearPhone)).ToString();
               toReturn = ThalesSim.Core.Utility.XORHexStringsFull(clearPinValue, clearPhone);
               //Trace.TraceInformation(toReturn);
               toReturn = ThalesSim.Core.Cryptography.TripleDES.TripleDESEncrypt(
              new ThalesSim.Core.Cryptography.HexKey(encryptionKey.Substring(0, 32)),
              toReturn);

               Trace.TraceInformation(mobilePhone + ";" + encryptionKey + ";" + pin + ";" + toReturn);
               return toReturn;
           
       }

       public bool SendErrorMail(string body,string title)
       {
           bool toReturn = false;
           using (SMSService.MessagingServiceClient client = new SMSService.MessagingServiceClient())
           {
               string emails = string.Empty;
               if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
               {
                   emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
               }
               try
               {
                   toReturn = client.SendEmail("BankOneMobile", "donotreply@mybankone.com", emails, title, body);
               }
               catch(Exception ex)
               {
                   Trace.TraceInformation("Error Sending Mail -{0}",ex.Message);
               }
              return toReturn; 

           }
       }


    }
}
