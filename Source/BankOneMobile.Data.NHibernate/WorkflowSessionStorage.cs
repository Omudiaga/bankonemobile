﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Implementations;
using NHibernate;
using BankOneMobile.Services;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;

namespace BankOneMobile.Data.NHibernate
{
    public class WorkflowSessionStorage : ISessionStorage
    {
        #region ISessionStorage Members
        private ISession _session;
        private WorkflowInfo _workflowInfo;

        public WorkflowSessionStorage(IDataSource dataSource)
        {
            Check.Require(dataSource != null, "DataSource should exist for a session to be created");

            _workflowInfo = WorkflowCenter.Instance.GetWorkflowBy(dataSource.WorkflowId);
            if (_workflowInfo != null)
            {
                _workflowInfo.OnCompleted += new EventHandler(_workflowInfo_OnCompleted);
                _workflowInfo.OnAborted += new EventHandler(_workflowInfo_OnAborted);
                _workflowInfo.OnCancelled += new EventHandler(_workflowInfo_OnAborted);
            }
        }
        void _workflowInfo_OnAborted(object sender, EventArgs e)
        {
            if (_session != null)
            {
                if (_session.Transaction != null && _session.Transaction.IsActive && !_session.Transaction.WasRolledBack)
                {
                    _session.Transaction.Rollback();
                }
                _session.Close();
                _session = null;
            }
        }

        void _workflowInfo_OnCompleted(object sender, EventArgs e)
        {
            if (_session != null)
            {
                if (_session.Transaction != null && _session.Transaction.IsActive && !_session.Transaction.WasCommitted)
                {
                    _session.Transaction.Commit();
                }
                _session.Flush();
                _session.Close();
                _session = null;
            }
        }

        public global::NHibernate.ISession Session
        {
            get
            {
                return _session;
            }
            set
            {
                _session = value;
            }
        }

        public WorkflowInfo TheWorkflowInfo
        {
            get
            {
                return _workflowInfo;
            }
        }
        #endregion
    }
}
