﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using NHibernate.Criterion;
using NHibernate;
using BankOneMobile.Core.Helpers;
using System.Collections;
using System.Diagnostics;

namespace BankOneMobile.Data.NHibernate
{
   
    /// <summary>
    /// Since nearly all of the domain objects you create will have a type of long ID, this 
    /// most freqently used base GenericDao leverages this assumption.  If you want an entity
    /// with a type other than int, such as string, then use 
    /// <see cref="GenericDaoWithTypedId{T, IdT}" />.
    /// </summary>
    public class Repository<T> : RepositoryWithTypedId<T, long>, IRepository<T> { }

    [Serializable]
    public class Repository :  IRepository
    {
        protected virtual ISession Session(IDataSource dataSource)
        {
            return NHibernateSession.Current(dataSource);
        }

        public virtual IDbContext DbContext
        {
            get
            {
                return BankOneMobile.Data.NHibernate.DbContext.Instance;
            }
        }


        #region IRepositoryWithTypedId<object,long> Members

        public object Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public List<object> GetAll(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public List<object> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public object FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public object Update(IDataSource dataSource, object entity)
        {
            throw new NotImplementedException();
        }

        public object Merge(IDataSource dataSource, object entity)
        {
            throw new NotImplementedException();
        }

        public object Save(IDataSource dataSource, object entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(IDataSource dataSource, object entity)
        {
            throw new NotImplementedException();
        }

        public object ExecuteSqlQuery(IDataSource dataSource, string query)
        {
            return Session(dataSource).CreateSQLQuery(query).List();
        }

        public void UpdateSqlQuery(IDataSource dataSource, string query)
        {
            Session(dataSource).CreateSQLQuery(query).ExecuteUpdate();
        }

        #endregion

        #region IRepositoryWithTypedId<object,long> Members


        public List<object> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IRepositoryWithTypedId<object,long> Members


        public List<object> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    /// <summary>
    /// Provides a fully loaded DAO which may be created in a few ways including:
    /// * Direct instantiation; e.g., new GenericDao<Customer, string>
    /// * Spring configuration; e.g., <object id="CustomerDao" type="SharpArch.Data.NHibernateSupport.GenericDao&lt;CustomerAlias, string>, SharpArch.Data" autowire="byName" />
    /// </summary>
    public class RepositoryWithTypedId<T, IdT> : IRepositoryWithTypedId<T, IdT>
    {
        protected virtual ISession Session(IDataSource dataSource) 
        {
            return NHibernateSession.Current(dataSource); 
        }

        public virtual IDbContext DbContext {
            get {
                return BankOneMobile.Data.NHibernate.DbContext.Instance;
            }
        }

        public virtual T Get(IDataSource dataSource, IdT id) {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));
            criteria.Add(Restrictions.IdEq(id));
            lock (Session(dataSource))
            {
                var result = criteria.UniqueResult<T>();
                return result;

            }
            
        }

        public virtual List<T> GetAll(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));
            lock (Session(dataSource))
            {                
                var results = criteria.List<T>() as List<T>;
                return results;
            }
        }

        public virtual List<T> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs) {
            Check.Require(propertyValuePairs != null ,
                "propertyValuePairs was null or empty; " +
                "it has to have at least one property/value pair in it");

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));

            foreach (string key in propertyValuePairs.Keys) {
                if (propertyValuePairs[key] != null) {
                    criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                }
                else {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            lock (Session(dataSource))
            {
                var results = criteria.List<T>() as List<T>;
                return results;
            }
        }

        public virtual List<T> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            Check.Require(propertyValuePairs != null,
                "propertyValuePairs was null or empty; " +
                "it has to have at least one property/value pair in it");

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            lock (Session(dataSource))
            {
                var results = criteria.List<T>() as List<T>;
                return results;
            }
        }
        public virtual List<T> FindWithPaging(IDataSource dataSource,Order order, IDictionary<string, object> propertyValuePairs,int startIndex,int maxSize,out int total)
        {
            Check.Require(propertyValuePairs != null,
                "propertyValuePairs was null or empty; " +
                "it has to have at least one property/value pair in it");

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            lock (Session(dataSource))
            {
               // var results = criteria.List<T>() as List<T>;
               var  results = RetrieveUsingPaging<T>(dataSource, criteria,order, startIndex, maxSize, out  total) ;
                return results;

                
            }
        }
        public virtual List<T> FindWithPaging(IDataSource dataSource,  IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            Check.Require(propertyValuePairs != null,
                "propertyValuePairs was null or empty; " +
                "it has to have at least one property/value pair in it");

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(T));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<T>(dataSource, criteria, startIndex, maxSize, out  total);
                return results;


            }
        }
     
        


        public virtual T FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            List<T> foundList = FindAll(dataSource, propertyValuePairs);

            if (foundList.Count > 1) {
                throw new NonUniqueResultException(foundList.Count);
            }
            else if (foundList.Count == 1) {
                return foundList[0];
            }

            return default(T);
        }

        public virtual void Delete(IDataSource dataSource, T entity)
        {
            lock (Session(dataSource))
            {
                Session(dataSource).Delete(entity);
            }
        }

        /// <summary>
        /// Although SaveOrUpdate _can_ be invoked to update an object with an assigned ID, you are 
        /// hereby forced instead to use Save/Update for better clarity.
        /// </summary>
        public virtual T SaveOrUpdate(IDataSource dataSource, T entity)
        {
            /*Check.Require(!(entity is IHasAssignedId<IdT>),
                "For better clarity and reliability, Entities with an assigned ID must call Save or Update");*/
            lock (Session(dataSource))
            {
                Session(dataSource).SaveOrUpdate(entity);
            }
            return entity;
        }
        public List<T> RetrieveUsingPaging<T>(IDataSource dataSource, ICriteria theCriteria, Order order, int startIndex, int maxRows, out int totalCount)
        {

           // IList vals = theCriteria.List();
           
            ICriteria countCriteria = CriteriaTransformer.Clone(theCriteria).SetProjection(Projections.RowCount());
            ICriteria listCriteria = CriteriaTransformer.Clone(theCriteria).SetFirstResult(startIndex).SetMaxResults(maxRows);
            IList allResults = null;
           listCriteria.AddOrder(order);
           //System.Diagnostics.Trace.TraceInformation("Transaction Report Query Started:{0}", DateTime.Now);
           try
           {
                allResults = Session(dataSource).CreateMultiCriteria().Add<T>(listCriteria).Add(countCriteria).List();
           }
           catch (Exception ex)
           {
               Trace.TraceInformation("Error Getting Agents From DB-{0}", ex.Message);
           }
            //System.Diagnostics.Trace.TraceInformation("Transaction Report Query Ended:{0}", DateTime.Now);

            totalCount = Convert.ToInt32(((IList)allResults[1])[0]);
            return allResults[0] as List<T>;
        }
        public List<T> RetrieveUsingPaging<T>(IDataSource dataSource, ICriteria theCriteria, Order order, int startIndex, int maxRows, out int totalCount, out decimal sum)
        {

            // IList vals = theCriteria.List();
            IList allResults = null;
            ICriteria countCriteria = CriteriaTransformer.Clone(theCriteria).SetProjection(Projections.RowCount());
            ICriteria listCriteria = CriteriaTransformer.Clone(theCriteria).SetFirstResult(startIndex).SetMaxResults(maxRows);

            listCriteria.AddOrder(order);
            try
            {
                 allResults = Session(dataSource).CreateMultiCriteria().Add<T>(listCriteria).Add(countCriteria).List();
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error Retreiving Data From DB-{0} {1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
            }
            totalCount = Convert.ToInt32(((IList)allResults[1])[0]);
            sum = Convert.ToDecimal(totalCount);
            return allResults[0] as List<T>;
        }

       
        public List<T> RetrieveUsingPaging<T>(IDataSource dataSource, ICriteria theCriteria, int startIndex, int maxRows, out int totalCount)
        {
            IList allResults = null;
            ICriteria countCriteria = CriteriaTransformer.Clone(theCriteria).SetProjection(Projections.RowCount());
            ICriteria listCriteria = CriteriaTransformer.Clone(theCriteria).SetFirstResult(startIndex).SetMaxResults(maxRows);


            try
            {

                allResults = Session(dataSource).CreateMultiCriteria().Add<T>(listCriteria).Add(countCriteria).List();
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error Retreiving Data From DB-{0} {1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
            }
            totalCount = Convert.ToInt32(((IList)allResults[1])[0]);
            return allResults[0] as List<T>;
        }
        public List<T> RetrieveUsingPagingAndOrder<T>(IDataSource dataSource, ICriteria theCriteria, int startIndex, int maxRows, out int totalCount)

        {
                         IList allResults = null;
            ICriteria countCriteria = CriteriaTransformer.Clone(theCriteria).SetProjection(Projections.RowCount());
            ICriteria listCriteria = CriteriaTransformer.Clone(theCriteria).SetFirstResult(startIndex).SetMaxResults(maxRows);
            try
            {
                allResults = Session(dataSource).CreateMultiCriteria().Add<T>(listCriteria).Add(countCriteria).List();
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error Retreiving Data From DB-{0} {1}",ex.Message, ex.InnerException==null?"No Inner Exception":ex.InnerException.Message);
            }

            totalCount = Convert.ToInt32(((IList)allResults[1])[0]);
            return allResults[0] as List<T>;
        }

        #region IRepositoryWithTypedId<T,IdT> Members


        public virtual T Update(IDataSource dataSource, T entity)
        {
            lock (Session(dataSource))
            {
                Session(dataSource).Merge(entity);
            }
            return entity;
        }

        public virtual T Merge(IDataSource dataSource, T entity)
        {
            lock (Session(dataSource))
            {
                Session(dataSource).Merge(entity);
            }
            return entity;
        }

        public virtual T Save(IDataSource dataSource, T entity)
        {
            lock (Session(dataSource))
            {
                Session(dataSource).Save(entity);
            }
            return entity;
        }

        #endregion

        #region IRepositoryWithTypedId<T,IdT> Members


        public object ExecuteSqlQuery(IDataSource dataSource, string query)
        {
            lock (Session(dataSource))
            {
                var results = Session(dataSource).CreateSQLQuery(query).List();
                return results;
            }
        }

        public void UpdateSqlQuery(IDataSource dataSource, string query)
        {
            lock (Session(dataSource))
            {
                Session(dataSource).CreateSQLQuery(query).ExecuteUpdate();
            }
        }

        #endregion
    }

}

