﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CustomerCreationLogRepository:Repository<ICustomerCreationLog> , ICustomerCreationLogRepository 
    {

        public ICustomerCreationLog GetByAccountNumber(IDataSource dataSource, string accountNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerCreationLog));
            criteria.Add(Restrictions.Eq("AccountNumber", accountNumber));
            return criteria.UniqueResult<ICustomerCreationLog>(); 
        }
        
    }
}
