﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CustomerRegistrationRepository : Repository<ICustomerRegistration>, ICustomerRegistrationRepository
    {
        public List<ICustomerRegistration> FindTransactionsWithPaging(IDataSource dataSource, string institutionCode, 
            string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerRegistration));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(institutionCode))
            {
                criteria.Add(Expression.Eq("InstitutionCode", institutionCode));
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                criteria.Add(Expression.Eq("PhoneNumber", phoneNumber));
            }
            if (dateFrom.HasValue)
            {
                criteria.Add(Expression.Ge("Date", dateFrom.Value));
            }
            if (dateTo.HasValue)
            {
                criteria.Add(Expression.Le("Date", dateTo.Value));
            }
            Order order = Order.Desc("ID");
            //ICriteria sumCriteria = cr
            lock (Session(dataSource))
            {

                var results = RetrieveUsingPaging<ICustomerRegistration>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;

            }
        }

        public ICustomerRegistration GetCustomerRegistrationByPhoneNumber(IDataSource dataSource,string institutionCode, string phoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerRegistration));
            criteria.Add(Expression.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Eq("PhoneNumber", phoneNumber));
            return criteria.UniqueResult<ICustomerRegistration>();
        }

        public ICustomerRegistration GetCustomerRegistrationByCardSerialNumber(IDataSource dataSource,string serialNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerRegistration));
            criteria.Add(Expression.Eq("CardSerialNumber", serialNumber));
            //criteria.Add(Expression.Eq("PhoneNumber", phoneNumber));
            return criteria.UniqueResult<ICustomerRegistration>();
        }

        public List<ICustomerRegistration> GetPendingSMSesByRawSql(IDataSource dataSource)
        {
            var query = Session(dataSource).CreateSQLQuery("select * from CustomerRegistrations where Status=:status and IsSmsSent=:smsSent")
                .SetResultTransformer(Transformers.AliasToBean<CustomerRegistration>())
                .SetString("status", "successful")
                .SetString("smsSent", "false");
            return query.List<ICustomerRegistration>().ToList();
        }
    }
}
