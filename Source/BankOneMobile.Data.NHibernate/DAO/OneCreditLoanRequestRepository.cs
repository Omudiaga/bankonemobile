﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.DAO
{

    public class OneCreditLoanRequestRepository : Repository<IOneCreditLoanRequest>, IOneCreditLoanRequestRepository
    {


        public IList<IOneCreditLoanRequest> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOneCreditLoanRequest));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<IOneCreditLoanRequest>();
        }

        public IList<IOneCreditLoanRequest> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOneCreditLoanRequest));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IOneCreditLoanRequest>();
        }


        public IList<IOneCreditLoanRequest> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOneCreditLoanRequest));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateLogged", startDate, endDate));
            return criteria.List<IOneCreditLoanRequest>();
        }
    }
}
