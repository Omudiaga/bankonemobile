﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class HostRepository : Repository<IHost>, IHostRepository
    {
    }
}
