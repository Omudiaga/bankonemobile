﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class FundsTransferCashOutTransactionTypeRepository : Repository<IFundsTransferCashOutTransactionType>, IFundsTransferCashOutTransactionTypeRepository
    {

        

        public IFundsTransferCashOutTransactionType GetByToken(IDataSource dataSource, string token)
        {

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferCashOutTransactionType));
            criteria.Add(Restrictions.Eq("Token", token));
            return criteria.UniqueResult<IFundsTransferCashOutTransactionType>();
        
        }

        public IFundsTransferCashOutTransactionType GetByPhoneNumber(IDataSource dataSource, string phoneNumber)
        {
              ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferCashOutTransactionType));
              criteria.Add(Restrictions.Eq("BeneficiaryPhoneNumber", phoneNumber));
              return criteria.UniqueResult<IFundsTransferCashOutTransactionType>();
        }

        
    }
}
