﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CustomerRegistrationImageRepository : Repository<ICustomerRegistrationImage>, ICustomerRegistrationImageRepository
    {
    }
}
