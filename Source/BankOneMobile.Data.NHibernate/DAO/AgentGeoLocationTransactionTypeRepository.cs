﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class AgentGeoLocationTransactionTypeRepository : Repository<IAgentGeoLocationTransactionType>, IAgentGeoLocationTransactionTypeRepository
    {


        public IList<IAgentGeoLocationTransactionType> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocationTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<IAgentGeoLocationTransactionType>();           
        }

        public IList<IAgentGeoLocationTransactionType> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocationTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IAgentGeoLocationTransactionType>();
        }


        public IList<IAgentGeoLocationTransactionType> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocationTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateLogged", startDate,endDate));
            return criteria.List<IAgentGeoLocationTransactionType>();
        }
    }
}
