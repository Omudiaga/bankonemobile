﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class KeyRepository : Repository<BankOneMobile.Core.Contracts.IKey>, IKeyRepository
    {


       


        

      
       public IKey GetPEK(IDataSource dataSource)
       {
           ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.IKey));
           criteria.Add(Restrictions.Eq("Type", "TerminalPinKey"));
           criteria.SetMaxResults(1);
           BankOneMobile.Core.Contracts.IKey toReturn = criteria.UniqueResult<BankOneMobile.Core.Contracts.IKey>();
           return toReturn;
       }
       public IKey GetTMK(IDataSource dataSource)
       {
           ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.IKey));
           criteria.Add(Restrictions.Eq("Type", "TerminalMasterKey"));
           criteria.SetMaxResults(1);
           BankOneMobile.Core.Contracts.IKey toReturn = criteria.UniqueResult<BankOneMobile.Core.Contracts.IKey>();
           return toReturn;
       }

        
    }
}
