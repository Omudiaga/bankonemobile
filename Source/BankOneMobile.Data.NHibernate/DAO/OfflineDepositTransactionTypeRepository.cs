﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class OfflineDepositTransactionTypeRepository : Repository<IOfflineDepositTransactionType>, IOfflineDepositTransactionTypeRepository
    {


        public IList<IOfflineDepositTransactionType> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOfflineDepositTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<IOfflineDepositTransactionType>();           
        }

        public IList<IOfflineDepositTransactionType> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOfflineDepositTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IOfflineDepositTransactionType>();
        }

        public IOfflineDepositTransactionType GetUnsuccessfulOfflineDepositBySessionID(IDataSource dataSource, string sessionID)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOfflineDepositTransactionType));
            criteria.Add(Restrictions.Eq("SessionID", sessionID));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", TransactionStatus.Successful)));
            return criteria.UniqueResult<IOfflineDepositTransactionType>();
        }


        public IList<IOfflineDepositTransactionType> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IOfflineDepositTransactionType));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateLogged", startDate,endDate));
            return criteria.List<IOfflineDepositTransactionType>();
        }
    }
}
