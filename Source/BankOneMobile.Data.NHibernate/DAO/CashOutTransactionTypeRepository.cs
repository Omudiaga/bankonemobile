﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Helpers.Enums;
using System.Collections;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CashOutTransactionTypeRepository : Repository<ICashOutTransactionType>, ICashOutTransactionTypeRepository
    {
        public virtual List<BankOneMobile.Core.Implementations.CashOutTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, 
            TransactionStatus? status, string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount, out int totalSuccessful)
        {

            amount = 0;
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Implementations.CashOutTransactionType));


            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            //if (!string.IsNullOrEmpty(InstitutionID) || !string.IsNullOrEmpty(phoneNumber) || dateFrom.HasValue || dateTo.HasValue || status.HasValue)
            //{
                criteria.CreateCriteria("TheTransaction", "tr");
                //sumCriteria = criteria.SetProjection(Projections.Sum("Amount"));
            //}


            if (!string.IsNullOrEmpty(InstitutionID))
            {
                criteria.Add(Expression.Eq("tr.FromInstitutionCode", InstitutionID));
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                criteria.Add(Expression.Eq("tr.FromPhoneNumber", phoneNumber));
            }
            if (dateFrom.HasValue)
            {
                criteria.Add(Expression.Ge("tr.Date", dateFrom.Value));
            }
            if (dateTo.HasValue)
            {
                criteria.Add(Expression.Le("tr.Date", dateTo.Value));
            }
            if (status.HasValue)
            {
                criteria.Add(Expression.Eq("tr.Status", status));
            }
            Order order = Order.Desc("ID");
            //ICriteria sumCriteria = cr
            lock (Session(dataSource))
            {
                var results = RetrieveUsingPaging<BankOneMobile.Core.Implementations.CashOutTransactionType>(dataSource, criteria, order, startIndex, maxSize, out  total);
                var successCriteria = criteria.Add(Expression.Eq("tr.Status", TransactionStatus.Pending))
                    .SetProjection(Projections.RowCount(), Projections.Alias(Projections.Sum("tr.Amount"), "TotalSuccessful"));
                IList projectionResults = successCriteria.List();
                amount = Convert.ToDecimal(((IList)projectionResults[0])[1]); totalSuccessful = Convert.ToInt32(((IList)projectionResults[0])[0]);
                return results;

            }
        }
    }
}
