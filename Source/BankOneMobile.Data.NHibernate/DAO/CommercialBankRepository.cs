﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CommercialBankRepository : Repository<ICommercialBankRepository>, ICommercialBankRepository
    {
        public ICommercialBank GetByCode(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICommercialBank));
            criteria.Add(Restrictions.Eq("Code", code));
            return criteria.UniqueResult<ICommercialBank>();
        }
        public ICommercialBank GetByName(IDataSource dataSource, string name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICommercialBank));
            criteria.Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<ICommercialBank>();
        }


        public IList<ICommercialBank> GetAllActive(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICommercialBank));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<ICommercialBank>();
        }




        #region ICommercialBankRepository Members


        public IList<ICommercialBank> GetActiveCommercialBanks(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICommercialBank));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<ICommercialBank>();
        }

        #endregion

        #region IRepositoryWithTypedId<ICommercialBank,long> Members

        public new ICommercialBank Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public new List<ICommercialBank> GetAll(IDataSource dataSource)
        {
            return GetActiveCommercialBanks(dataSource).ToList();
        }

        public new List<ICommercialBank> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new List<ICommercialBank> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new ICommercialBank FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public ICommercialBank Update(IDataSource dataSource, ICommercialBank entity)
        {
            throw new NotImplementedException();
        }

        public ICommercialBank Merge(IDataSource dataSource, ICommercialBank entity)
        {
            throw new NotImplementedException();
        }

        public ICommercialBank Save(IDataSource dataSource, ICommercialBank entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(IDataSource dataSource, ICommercialBank entity)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IRepositoryWithTypedId<ICommercialBank,long> Members


        public new List<ICommercialBank> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
