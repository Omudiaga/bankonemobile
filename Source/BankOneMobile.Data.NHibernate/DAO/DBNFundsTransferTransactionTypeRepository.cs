﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class DBNFundsTransferTransactionTypeRepository : Repository<IDBNFundsTransferTransactionType>, IDBNFundsTransferTransactionTypeRepository
    {


        public IDBNFundsTransferTransactionType GetBySessionID(IDataSource dataSource, string sessionID)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("SessionID", sessionID));
            return criteria.UniqueResult<IDBNFundsTransferTransactionType>();
            
        }

        public IDBNFundsTransferTransactionType GetByNameEnquirySessionID(IDataSource dataSource, string nameEnquirySessionID)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("NameEnquirySessionID", nameEnquirySessionID));
            return criteria.UniqueResult<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetByDestinationInstitutionCode(IDataSource dataSource, string destinationInstitutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("DestinationInstitutionCode", destinationInstitutionCode));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetByOriginatorAccountNumber(IDataSource dataSource, string originatorAccountNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("OriginatorAccountNumber", originatorAccountNumber));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }
        public IList<IDBNFundsTransferTransactionType> GetByResponseCode(IDataSource dataSource, string responseCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("GetByResponseCode", responseCode));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetByBeneficiaryAccountNumber(IDataSource dataSource, string beneficiaryAccountNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("BeneficiaryAccountNumber", beneficiaryAccountNumber));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetPendingTransfersToDiamondBank(IDataSource dataSource, string diamondBankInstitutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("DestinationInstitutionCode", diamondBankInstitutionCode));
            criteria.Add(Restrictions.Eq("TransferStatus", FundsTransferStatus.Pending));
            criteria.Add(Restrictions.Eq("DBNFundsTransferType", DBNFundsTransferTransactionTypeName.BetaToDiamond));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetFailedReversals(IDataSource dataSource, string diamondBankInstitutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("DestinationInstitutionCode", diamondBankInstitutionCode));
            criteria.Add(Restrictions.Eq("TransferStatus", FundsTransferStatus.Failed));
            criteria.Add(Restrictions.Eq("ReversalStatus", DBNReversalStatus.NotSuccessful));
            criteria.Add(Restrictions.Eq("DBNFundsTransferType", DBNFundsTransferTransactionTypeName.BetaToDiamond));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }

        public IList<IDBNFundsTransferTransactionType> GetPendingTransfersToOtherBanks(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IDBNFundsTransferTransactionType));
            criteria.Add(Restrictions.Eq("TransferStatus", FundsTransferStatus.Pending));
            criteria.Add(Restrictions.Eq("DBNFundsTransferType", DBNFundsTransferTransactionTypeName.BetaToOtherBanks));
            return criteria.List<IDBNFundsTransferTransactionType>();
        }
        
    }
}
