﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class ProductRepository : Repository<IProductRepository>, IProductRepository
    {
        public IProduct GetByCode(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IProduct));
            criteria.Add(Restrictions.Eq("Code", code));
            return criteria.UniqueResult<IProduct>();
        }
        public IProduct GetByName(IDataSource dataSource, string name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IProduct));
            criteria.Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<IProduct>();
        }


        public IList< IProduct> GetAllActive(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IProduct));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<IProduct>();
        }
        IList<IProduct> GetActiveProductsByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IProduct));
            criteria.Add(Restrictions.Eq("IsActive", true));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IProduct>();
        }

        #region IProductRepository Members


        public IList<IProduct> GetActiveProducts(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IRepositoryWithTypedId<IProduct,long> Members

        public new IProduct Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public new List<IProduct> GetAll(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public new List<IProduct> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new List<IProduct> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new IProduct FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public IProduct Update(IDataSource dataSource, IProduct entity)
        {
            throw new NotImplementedException();
        }

        public IProduct Merge(IDataSource dataSource, IProduct entity)
        {
            throw new NotImplementedException();
        }

        public IProduct Save(IDataSource dataSource, IProduct entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(IDataSource dataSource, IProduct entity)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IProductRepository Members


        IList<IProduct> IProductRepository.GetActiveProductsByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IProduct));
            criteria.Add(Restrictions.Eq("IsActive", true));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IProduct>();
        }

        #endregion

        #region IRepositoryWithTypedId<IProduct,long> Members


        public new List<IProduct> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
