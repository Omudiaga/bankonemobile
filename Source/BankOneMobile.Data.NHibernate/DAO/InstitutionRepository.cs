﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;
using System.Diagnostics;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class InstitutionRepository : Repository<IInstitutionRepository>, IInstitutionRepository
    {
        public IInstitution GetByCode(IDataSource dataSource, string code)
        {
            global::NHibernate.ISession sess = Session(dataSource);
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            criteria.Add(Restrictions.Eq("Code", code));
            return criteria.UniqueResult<IInstitution>();
        }


        public IInstitution GetByName(IDataSource dataSource, string name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            criteria.Add(Restrictions.Eq("Name", name));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.UniqueResult<IInstitution>();
        }
        public IInstitution GetByShortName(IDataSource dataSource, string shortName)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            criteria.Add(Restrictions.Eq("ShortName", shortName));
            return criteria.UniqueResult<IInstitution>();
        }
        public IList <IInstitution> GetAllInstitutions(IDataSource dataSource)
        {
            Trace.TraceInformation("Inside dAta for Institutions");
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            
            try
            {
                Trace.TraceInformation("Inside dAta for Institutions. Getting Inst");
                IList<IInstitution> institutions = null;
                institutions= criteria.List<IInstitution>();
                Trace.TraceInformation("Institutions Gotten -{0}", institutions==null? 0: institutions.Count);
                return institutions;
            }
            catch   (Exception ex)
            {
                Trace.TraceInformation("Error Getting Institutions-{0},{1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
                throw;
            }
        }

        //public IAppZoneIncomeAccount GetAppZoneIncomeAccount(IDataSource dataSource, long institutionID)
        //{
        //    string query = string.Format("SELECT ID, InstitutionID, AccountNumber FROM AppZoneIncomeAccounts where InstitutionID={0}", institutionID);
        //    ISQLQuery sqlQuery = Session(dataSource).CreateSQLQuery(query);
        //    sqlQuery  = sqlQuery.SetResultTransformer(new global::NHibernate.Transform.AliasToBeanResultTransformer(typeof(IAppZoneIncomeAccount))) as ISQLQuery;
        //    return sqlQuery.UniqueResult<IAppZoneIncomeAccount>();            
        //}

        public List<IInstitution> FindWithPaging(IDataSource dataSource, string name, string code, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
            Order order = Order.Desc("ID");
            //ICriteria sumCriteria = cr
            lock (Session(dataSource))
            {
                var results = RetrieveUsingPaging<IInstitution>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;
            }
        }

        #region IInstitutionRepository Members


        public IList<IInstitution> GetActiveInstitutions(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<IInstitution>();
        }

        #endregion

        #region IRepositoryWithTypedId<IInstitution,long> Members

        public new IInstitution Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public new List<IInstitution> GetAll(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public new List<IInstitution> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new List<IInstitution> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new IInstitution FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public IInstitution Update(IDataSource dataSource, IInstitution entity)
        {
            throw new NotImplementedException();
        }

        public IInstitution Merge(IDataSource dataSource, IInstitution entity)
        {
            throw new NotImplementedException();
        }

        public IInstitution Save(IDataSource dataSource, IInstitution entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(IDataSource dataSource, IInstitution entity)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IInstitutionRepository Members


        

        #endregion

        #region IRepositoryWithTypedId<IInstitution,long> Members


        public new List<IInstitution> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
