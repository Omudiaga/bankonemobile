﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class TokenRepository : Repository<IToken>, ITokenRepository
    {


        



        public IList<IToken> GetByValueAndPhoneNumber(IDataSource dataSource, string phoneNumber, string token)
        {
            global::NHibernate.ISession sess = Session(dataSource);
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IToken));
            criteria.Add(Restrictions.Eq("PhoneNumber", phoneNumber));
            criteria.Add(Restrictions.Eq("TokenValue", token));
            criteria.Add(Restrictions.Eq("IsConsumed", false));
            return criteria.List<IToken>();
        }

        public new IToken Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

      
    }
}
