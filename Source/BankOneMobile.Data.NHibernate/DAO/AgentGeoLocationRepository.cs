﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class AgentGeoLocationRepository : Repository<IAgentGeoLocation>, IAgentGeoLocationRepository
    {


        public IList<IAgentGeoLocation> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<IAgentGeoLocation>();           
        }

        public IList<IAgentGeoLocation> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IAgentGeoLocation>();
        }


        public IList<IAgentGeoLocation> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateLogged", startDate,endDate));
            return criteria.List<IAgentGeoLocation>();
        }
    }
}
