﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class ConfigurationTransactionTypeRepository : Repository<IConfigurationTransactionType>, IConfigurationTransactionTypeRepository
    {
    }
}
