﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;
using System.Diagnostics;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class ThirdPartyMobileProfileRepository : Repository<IThirdPartyMobileProfile>, IThirdPartyMobileProfileRepository
    {
        public IThirdPartyMobileProfile GetByAccountNumber(IDataSource dataSource, string AccountNo)
        {
            //global::NHibernate.ISession sess = Session(dataSource);
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("AccountNumber", AccountNo));
            return criteria.UniqueResult<IThirdPartyMobileProfile>();
        }
        public IThirdPartyMobileProfile GetByAccountNumberAndInstitutionCode(IDataSource dataSource, string AccountNo, string InstitutionCode)
        {
            //global::NHibernate.ISession sess = Session(dataSource);
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            if (!string.IsNullOrEmpty(AccountNo) && !string.IsNullOrEmpty(AccountNo.Trim()))
            {
                criteria.Add(Restrictions.Eq("AccountNumber", AccountNo));

            }
            if (!string.IsNullOrEmpty(InstitutionCode) && !string.IsNullOrEmpty(InstitutionCode.Trim()))
            {
                criteria.Add(Restrictions.Like("InstitutionCode", InstitutionCode, MatchMode.Anywhere));

            }
            
            return criteria.UniqueResult<IThirdPartyMobileProfile>();
        }
        public IThirdPartyMobileProfile GetByPhoneNumber(IDataSource dataSource, string PhoneNo)
        {
            global::NHibernate.ISession sess = Session(dataSource);
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("PhoneNumber", PhoneNo));
            return criteria.UniqueResult<IThirdPartyMobileProfile>();
        }
        public IList<IThirdPartyMobileProfile> GetByLastName(IDataSource dataSource, string LastName)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("LastName", LastName));
            return criteria.List<IThirdPartyMobileProfile>();
        }
        public IList<IThirdPartyMobileProfile> GetByOtherNames(IDataSource dataSource, string OtherName)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("OtherName", OtherName));
            return criteria.List<IThirdPartyMobileProfile>();
        }
        public IList<IThirdPartyMobileProfile> GetByInstitutionCode(IDataSource dataSource, string InstitutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("InstitutionCode", InstitutionCode));
            return criteria.List<IThirdPartyMobileProfile>();
        }
        public IList<IThirdPartyMobileProfile> GetMobileProfilesByStatus(IDataSource dataSource, string status)
        {
            Trace.TraceInformation("Inside data for Third Party Mobile Profiles");
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            criteria.Add(Restrictions.Eq("Status",status));
            try
            {
                Trace.TraceInformation("Inside data for Third Party Mobile Profiles. Getting Third Party Mobile Profiles");
                IList<IThirdPartyMobileProfile> MobileProfiles = null;
                MobileProfiles = criteria.List<IThirdPartyMobileProfile>();
                Trace.TraceInformation("Profiles Gotten -{0}", MobileProfiles==null? 0: MobileProfiles.Count);
                return MobileProfiles;
            }
            catch   (Exception ex)
            {
                Trace.TraceInformation("Error Getting Third Party Mobile Profiles-{0},{1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
                throw;
            }
        }
        public IList<IThirdPartyMobileProfile> GetAllMobileProfiles(IDataSource dataSource)
        {
            Trace.TraceInformation("Inside data for Third Party Mobile Profiles");
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));
            try
            {
                Trace.TraceInformation("Inside data for Third Party Mobile Profiles. Getting Third Party Mobile Profiles");
                IList<IThirdPartyMobileProfile> MobileProfiles = null;
                MobileProfiles = criteria.List<IThirdPartyMobileProfile>();
                Trace.TraceInformation("Profiles Gotten -{0}", MobileProfiles==null? 0: MobileProfiles.Count);
                return MobileProfiles;
            }
            catch   (Exception ex)
            {
                Trace.TraceInformation("Error Getting Third Party Mobile Profiles-{0},{1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
                throw;
            }
        }
        public IList<ThirdPartyMobileProfile> GetAllTheMobileProfiles(IDataSource dataSource)
        {
            Trace.TraceInformation("Inside data for Third Party Mobile Profiles");
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ThirdPartyMobileProfile));
            try
            {
                Trace.TraceInformation("Inside data for Third Party Mobile Profiles. Getting Third Party Mobile Profiles");
                IList<ThirdPartyMobileProfile> MobileProfiles = null;
                MobileProfiles = criteria.List<ThirdPartyMobileProfile>();
                Trace.TraceInformation("Profiles Gotten -{0}", MobileProfiles == null ? 0 : MobileProfiles.Count);
                return MobileProfiles;
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error Getting Third Party Mobile Profiles-{0},{1}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message);
                throw;
            }
        }
        public List<IThirdPartyMobileProfile> FindWithPaging(IDataSource dataSource, string accountNumber, string phoneNumber, string lastname, string status, string InstitutionCode, int startIndex, int maxSize, out int total)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IThirdPartyMobileProfile));

            
            if (!string.IsNullOrEmpty(accountNumber) && !string.IsNullOrEmpty(accountNumber.Trim()))
            {
                criteria.Add(Restrictions.Like("AccountNumber", accountNumber, MatchMode.Anywhere));

            }
            if (!string.IsNullOrEmpty(phoneNumber) && !string.IsNullOrEmpty(phoneNumber.Trim()))
            {
                criteria.Add(Restrictions.Like("PhoneNumber", phoneNumber, MatchMode.Anywhere));

            }
            if (!string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(lastname.Trim()))
            {
                criteria.Add(Restrictions.Like("LastName", lastname, MatchMode.Anywhere));

            }
            if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            {
                criteria.Add(Restrictions.Like("Status", status, MatchMode.Anywhere));

            }
            if (!string.IsNullOrEmpty(InstitutionCode) && !string.IsNullOrEmpty(status.Trim()))
            {
                criteria.Add(Restrictions.Like("InstitutionCode", InstitutionCode, MatchMode.Exact));

            }


            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                var results = RetrieveUsingPaging<IThirdPartyMobileProfile>(dataSource, criteria, order, startIndex, maxSize, out  total);
                return results;
            }
        }


        #region IRepositoryWithTypedId<IThirdPartyMobileProfile,long> Members

        public new IThirdPartyMobileProfile Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public new List<IThirdPartyMobileProfile> GetAll(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        //public new List<IThirdPartyMobileProfile> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        //{
        //    throw new NotImplementedException();
        //}

        //public new List<IThirdPartyMobileProfile> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        //{
        //    throw new NotImplementedException();
        //}

        //public new IThirdPartyMobileProfile FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        //{
        //    throw new NotImplementedException();
        //}

        //public IThirdPartyMobileProfile Update(IDataSource dataSource, IThirdPartyMobileProfile entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public IThirdPartyMobileProfile Merge(IDataSource dataSource, IThirdPartyMobileProfile entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public IThirdPartyMobileProfile Save(IDataSource dataSource, IThirdPartyMobileProfile entity)
        //{
        //    return  base.Save(dataSource, entity);
        //}

        //public void Delete(IDataSource dataSource, IThirdPartyMobileProfile entity)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region IThirdPartyMobileProfileRepository Members




        #endregion

        #region IRepositoryWithTypedId<IThirdPartyMobileProfile,long> Members


        public new List<IThirdPartyMobileProfile> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
