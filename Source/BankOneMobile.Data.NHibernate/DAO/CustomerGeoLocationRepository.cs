﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CustomerGeoLocationRepository : Repository<ICustomerGeoLocation>, ICustomerGeoLocationRepository
    {


        public IList<ICustomerGeoLocation> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<ICustomerGeoLocation>();           
        }

        public ICustomerGeoLocation GetByCustomerPhoneNumber(IDataSource dataSource, string institutionCode, string customerPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("CustomerPhoneNumber", customerPhoneNumber));
            return criteria.UniqueResult<ICustomerGeoLocation>();
        }

        public IList<ICustomerGeoLocation> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<ICustomerGeoLocation>();
        }


        public IList<ICustomerGeoLocation> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ICustomerGeoLocation));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateLogged", startDate,endDate));
            return criteria.List<ICustomerGeoLocation>();
        }
    }
}
