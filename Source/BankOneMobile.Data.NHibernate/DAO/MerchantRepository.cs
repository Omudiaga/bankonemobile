﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class MerchantRepository : Repository<IMerchantRepository>, IMerchantRepository
    {
        public IMerchant GetByCode(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMerchant));
            criteria.Add(Restrictions.Eq("Code", code));
            return criteria.UniqueResult<IMerchant>();
        }
        public IMerchant GetByName(IDataSource dataSource, string name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMerchant));
            criteria.Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<IMerchant>();
        }


        public IList< IMerchant> GetAllActive(IDataSource dataSource, string code)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMerchant));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<IMerchant>();
        }
        

        #region IMerchantRepository Members


        public IList<IMerchant> GetActiveMerchants(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IRepositoryWithTypedId<IMerchant,long> Members

        public new IMerchant Get(IDataSource dataSource, long id)
        {
            throw new NotImplementedException();
        }

        public new List<IMerchant> GetAll(IDataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public new List<IMerchant> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new List<IMerchant> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public new IMerchant FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs)
        {
            throw new NotImplementedException();
        }

        public IMerchant Update(IDataSource dataSource, IMerchant entity)
        {
            throw new NotImplementedException();
        }

        public IMerchant Merge(IDataSource dataSource, IMerchant entity)
        {
            throw new NotImplementedException();
        }

        public IMerchant Save(IDataSource dataSource, IMerchant entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(IDataSource dataSource, IMerchant entity)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IMerchantRepository Members


        IList<IMerchant> IMerchantRepository.GetActiveMerchantsByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IMerchant));
            criteria.Add(Restrictions.Eq("IsActive", true));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<IMerchant>();
        }

        #endregion

        #region IRepositoryWithTypedId<IMerchant,long> Members


        public new List<IMerchant> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
