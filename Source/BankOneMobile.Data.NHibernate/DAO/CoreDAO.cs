﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class CoreDAO<T> : Repository<T>, ICoreDAO<T> where T : class, IEntity
    {
        public string InstitutionCode { get; set; }
        public void SqlBulkInsert(IList<T> items, string tableName, List<string> columnsToExclude = null, bool closeConnection = false)
        {
            if (items.Count == 0) return;

            var session = Session(DataSourceFactory.GetDataSource(DataCategory.Core));
            //var connection = session.Connection;

            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.KeepIdentity & SqlBulkCopyOptions.KeepNulls, transaction))
                {
                    bulkCopy.BatchSize = 100;
                    bulkCopy.DestinationTableName = tableName;
                    try
                    {
                        var dt = items.AsDataTable();
                        foreach (DataColumn column in dt.Columns)
                        {
                            if (columnsToExclude != null && columnsToExclude.Contains(column.ColumnName)) continue;
                            bulkCopy.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                        }
                        bulkCopy.WriteToServer(dt);
                        bulkCopy.Close();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        connection.Close();
                    }
                }
                if (closeConnection)
                {
                    connection.Close();
                    connection.Dispose();
                }

                transaction.Commit();
            }
        }
        IList RetriveUsingDirectQuery(string query)
        {
            var session = Session(DataSourceFactory.GetDataSource(DataCategory.Core));
            IList toReturn;
            using (var trx = session.Transaction)
            {
                toReturn = session.CreateSQLQuery(query).List();//.ExecuteUpdate();  
            }
            return toReturn;
        }
    }

    public static class IEnumerableExtensions
    {
        public static DataTable AsDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            //foreach (T item in data)
            //{
            //    DataRow row = table.NewRow();
            //    foreach (PropertyDescriptor prop in properties)
            //        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            //    table.Rows.Add(row);
            //}
            PropertyInfo[] props = typeof(T).GetProperties();

            //Fill in the rows
            for (int i = 0; i < data.Count(); i++)
            {
                var row = table.NewRow();
                for (int j = 0; j < props.Length; j++)
                {
                    var prop = props[j];
                    if (prop.Name == "ID") continue;
                    if (!table.Columns.Contains(prop.Name))
                    {
                        if (prop.PropertyType.IsClass)
                        {
                            var innerProps = prop.PropertyType.GetProperties();
                            if (innerProps.Any(x => x.Name == "ID")) // Handle properties mapped as Reference 
                            {
                                string columnName = string.Format("{0}ID", prop.Name.StartsWith("The") ? prop.Name.Remove(0, 3) : prop.Name);
                                if (!table.Columns.Contains(columnName)) continue;

                                object theRefValue;
                                try
                                {
                                    dynamic propValue = prop.GetValue(data[i], null);
                                    theRefValue = propValue.ID;
                                }
                                catch (TargetException)
                                {
                                    theRefValue = null;
                                }
                                catch (RuntimeBinderException)
                                {
                                    theRefValue = null;
                                }
                                catch (Exception) { throw; }
                                if (theRefValue == null)
                                {
                                    row[columnName] = System.DBNull.Value;
                                    continue;
                                }
                                row[columnName] = theRefValue;
                            }
                            else //Handling Compositely mapped Class properties
                            {
                                for (int k = 0; k < innerProps.Length; k++)
                                {
                                    var innerProp = innerProps[k];
                                    if (!table.Columns.Contains(innerProp.Name)) continue;

                                    object theInnerValue;
                                    try
                                    {
                                        theInnerValue = innerProp.GetValue(prop.GetValue(data[i], null), null);
                                    }
                                    catch (TargetException)
                                    {
                                        theInnerValue = null;
                                    }
                                    catch (Exception) { throw; }
                                    if (theInnerValue == null)
                                    {
                                        row[innerProp.Name] = System.DBNull.Value;
                                        continue;
                                    }
                                    row[innerProp.Name] = theInnerValue;
                                }
                            }
                            continue;
                        }
                        continue;
                    }

                    object theValue;
                    try
                    {
                        theValue = prop.GetValue(data[i], null);
                    }
                    catch (TargetException)
                    {
                        theValue = null;
                    }
                    catch (Exception) { throw; }
                    if (theValue == null)
                    {
                        row[prop.Name] = System.DBNull.Value;
                        continue;
                    }
                    row[prop.Name] = theValue;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
