﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Helpers.Enums;
using System.Collections;
using BankOneMobile.Data.Implementations;



namespace BankOneMobile.Data.NHibernate.DAO
{
    public class LinkingBankAccountRepository : Repository<ILinkingBankAccount>, ILinkingBankAccountRepository
    {
        public virtual List<ILinkingBankAccount> FindPageAndOrder(IDataSource dataSource,ICriteria criteria, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            

            // = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                      
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
           
          // criteria.AddOrder(OrderEx.Asc<ILinkingBankAccount>(x=>x.ID));
            Order order = Order.Desc("ID");
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<ILinkingBankAccount>(dataSource, criteria,order, startIndex, maxSize, out  total);
                return results;
            }
        }
       
        public virtual List<ILinkingBankAccount> Find(IDataSource dataSource,string acctNo,string status,string isAgentAccount,string phoneNumber,string productCode,string gender,  string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total)
        {
            //Get the institution
            IInstitution institution = null;
            if (!string.IsNullOrEmpty(InstitutionID))
            {
                total = 0;
                var institutionCriteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
                institutionCriteria.Add(Restrictions.Eq("Code", InstitutionID));
                institution = institutionCriteria.UniqueResult<IInstitution>();
                if (institution == null) return null;
            }

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));
            if (!string.IsNullOrEmpty(InstitutionID))
            {
                criteria.Add(Expression.Eq("InstitutionCode", InstitutionID));
            }
            if (!string.IsNullOrEmpty(status))
            {
                Enum st = (BankOneMobile.Core.Helpers.Enums.BankAccountStatus)Enum.Parse(typeof(BankOneMobile.Core.Helpers.Enums.BankAccountStatus), status);
                criteria.Add(Expression.Eq("BankStatus", st));
            }
            if (!string.IsNullOrEmpty(acctNo))
            {
                if (institution != null || institution.TheAccountNumberType == AccountNumberType.NUBAN)
                {
                    criteria.Add(Expression.Like("NUBAN", acctNo, MatchMode.Anywhere));
                }
                else
                {
                    criteria.Add(Expression.Like("StandardAccount", acctNo, MatchMode.Anywhere));
                }
            }
            if (!string.IsNullOrEmpty(productCode))
            {
                criteria.Add(Expression.Eq("ProductCode", productCode));
            }
            if (!string.IsNullOrEmpty(gender))
            {
              Gender  gend = (Gender)Enum.Parse(typeof(Gender), gender);                
                criteria.Add(Expression.Eq("TheGender", gend));
            }
            criteria = criteria.CreateCriteria("TheMobileAccount");
            if (!string.IsNullOrEmpty(isAgentAccount) && !string.IsNullOrEmpty(isAgentAccount.Trim()))
            {
                bool isAgent = Convert.ToBoolean(isAgentAccount);
                criteria.Add(Expression.Eq("IsAgentAccount", isAgent));

            }
            //if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
            //{
            //    MobileAccountStatus stat = (MobileAccountStatus)Enum.Parse(typeof(MobileAccountStatus), status);
            //    criteria.Add(Expression.Eq("MobileAccountStatus", stat));

            //}
            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                criteria.Add(Expression.Like("MobilePhone",phoneNumber,MatchMode.Anywhere));
               // criteria.SetResultTransformer(new global::NHibernate.Transform.DistinctRootEntityResultTransformer());
            }
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = FindPageAndOrder(dataSource, criteria, InstitutionID, propertyValuePairs, startIndex, maxSize, out  total);
                return results;
            }
        }
        public virtual ArrayList GetProducts(IDataSource dataSource, string instCode)
        {
            ArrayList toReturn = new ArrayList();
            //IDataSource dataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            string query =string.Format("Select Distinct ProductCode,ProductName from LinkingBankAccounts where InstitutionCode = {0} ",instCode);
            Session(dataSource).CreateSQLQuery(query).List(toReturn);
            return toReturn;
        }
        public virtual List<ILinkingBankAccount> FindActive(IDataSource dataSource,string instCode, string acctNo)
        {
            //Get the institution
            IInstitution institution = null;
            if (!string.IsNullOrEmpty(instCode))
            {
                var institutionCriteria = Session(dataSource).CreateCriteria(typeof(IInstitution));
                institutionCriteria.Add(Restrictions.Eq("Code", instCode));
                institution = institutionCriteria.UniqueResult<IInstitution>();
                if (institution == null) return null;
            }

            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(ILinkingBankAccount));

            criteria.Add(Expression.Eq("InstitutionCode", instCode));



            if (!string.IsNullOrEmpty(acctNo))
            {
                if (institution != null || institution.TheAccountNumberType == AccountNumberType.NUBAN)
                {
                    criteria.Add(Expression.Like("NUBAN", acctNo, MatchMode.Anywhere));
                }
                else
                {
                    criteria.Add(Expression.Like("StandardAccount", acctNo, MatchMode.Anywhere));
                }
            }
           
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = criteria.List <ILinkingBankAccount>().ToList();
                return results;
            }
        }
    }
}
