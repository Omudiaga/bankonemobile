﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Helpers.Enums;


namespace BankOneMobile.Data.NHibernate.DAO
{
    public class FundsTransferFeeRepository : Repository<IFundsTransferFee>, IFundsTransferFeeRepository 
    {
        public IFundsTransferFee GetByName(IDataSource dataSource,string institutionCode, TransactionTypeName name)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferFee));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("TransactionTypeName", name));
            return criteria.UniqueResult<IFundsTransferFee>();
        }
        public IFundsTransferFee GetByID(IDataSource dataSource, long id)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferFee));
            criteria.Add(Restrictions.Eq("ID", id));
            return criteria.UniqueResult<IFundsTransferFee>();
        }
        public IList<IFundsTransferFee> GetActiveFees(IDataSource dataSource,string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFundsTransferFee));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("IsActive", true));
            return criteria.List<IFundsTransferFee>();
        }
    }
}
