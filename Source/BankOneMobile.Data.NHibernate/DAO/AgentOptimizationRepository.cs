﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.DAO
{

    public abstract class GenericAgentOptimizationRepository<T> : CoreDAO<T>, IGenericAgentOptimizationRepository<T> where T : GenericAgentOptimization
    {
        public IList<T> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentOptimization));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Restrictions.Eq("AgentPhoneNumber", agentPhoneNumber));
            return criteria.List<T>();
        }

        public IList<T> GetByInstitutionCode(IDataSource dataSource, string institutionCode)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentOptimization));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            return criteria.List<T>();
        }


        public IList<T> GetByDateGenerated(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IAgentOptimization));
            criteria.Add(Restrictions.Eq("InstitutionCode", institutionCode));
            criteria.Add(Expression.Between("DateGenerated", startDate, endDate));
            return criteria.List<T>();
        }
    }
    public class AgentOptimizationRepository : GenericAgentOptimizationRepository<AgentOptimization>, IAgentOptimizationRepository
    {
    
    }
}
