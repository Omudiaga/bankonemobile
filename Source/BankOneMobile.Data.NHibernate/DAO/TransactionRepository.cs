﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using NHibernate;
using NHibernate.Criterion;

namespace BankOneMobile.Data.NHibernate.DAO
{
    public class TransactionRepository : Repository<BankOneMobile.Core.Contracts.ITransaction>, ITransactionRepository
    {
       

        public ITransactionType UpdateTransactionType(IDataSource dataSource, ITransactionType transactionType)
        {
            if (transactionType is IFundsTransferCashInTransactionType)
            {
                new FundsTransferCashInTransactionTypeRepository().Update(dataSource,transactionType as IFundsTransferCashInTransactionType);
            }
            else  if (transactionType is ILocalFundsTransferTransactionType)
            {
                new LocalFundsTransferTransactionTypeRepository().Update(dataSource, transactionType as ILocalFundsTransferTransactionType);
            }
            else if (transactionType is IFundsTransferCashOutTransactionType)
            {
                new FundsTransferCashOutTransactionTypeRepository().Update(dataSource, transactionType as IFundsTransferCashOutTransactionType);
            }
            else  if (transactionType is ICommercialBankFundsTransferTransactionType)
            {
                new CommercialBankFundsTransferTransactionTypeRepository().Update(dataSource, transactionType as ICommercialBankFundsTransferTransactionType);
            }
            else if (transactionType is IPinChangeTransactionType)
            {
                new PinChangeTransactionTypeRepository().Update(dataSource, transactionType as IPinChangeTransactionType);
            }
            else if (transactionType is IBalanceInquiryTransactionType)
            {
                new BalanceInquiryTransactionTypeRepository().Update(dataSource, transactionType as IBalanceInquiryTransactionType);
            }
            else if (transactionType is ICashInTransactionType)
            {
                new CashInTransactionTypeRepository().Update(dataSource, transactionType as CashInTransactionType);
            }
            else if (transactionType is ICashOutTransactionType)
            {
                new CashOutTransactionTypeRepository().Update(dataSource, transactionType as CashOutTransactionType);
            }
            else if (transactionType is IMiniStatementTransactionType)
            {
                new MiniStatementTransactionTypeRepository().Update(dataSource, transactionType as MiniStatementTransactionType);
            }

            else if (transactionType is IBillsPaymentTransactionType)
            {
                new BillsPaymentTransactionTypeRepository().Update(dataSource, transactionType as BillsPaymentTransactionType);
            }
            return transactionType;
        }
        public virtual List<BankOneMobile.Core.Contracts.ITransaction> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs,DateTime? dateFrom,DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount)
        {
            amount = 0M;
           
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(BankOneMobile.Core.Contracts.ITransaction));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    if (propertyValuePairs[key].GetType().Equals(typeof(String)))
                    {
                        criteria.Add(Expression.Like(key, Convert.ToString(propertyValuePairs[key]), MatchMode.Anywhere));
                    }
                    else
                    {
                        criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                    }
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }
            if (!string.IsNullOrEmpty(InstitutionID))
            {
                criteria.Add(Expression.Eq("FromInstitutionCode", InstitutionID));
            }
            if (dateFrom.HasValue)
            {
                criteria.Add(Expression.Ge("Date", dateFrom.Value));
            }
            if (dateTo.HasValue)
            {
                criteria.Add(Expression.Le("Date", dateTo.Value.AddDays(1)));
            }
            criteria.SetTimeout(120);
            Order order = Order.Desc("Date");
            lock (Session(dataSource))
            {
                // var results = criteria.List<T>() as List<T>;
                var results = RetrieveUsingPaging<BankOneMobile.Core.Contracts.ITransaction>(dataSource, criteria,order, startIndex, maxSize, out  total);
                return results;
            }
        }

        
    }
}
