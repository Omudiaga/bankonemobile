﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Contracts;
using System.Diagnostics;


namespace BankOneMobile.Data.NHibernate.DAO
{
    public class FlowRepository : Repository<IFlow> , IFlowRepository
    {

    
        
        #region IFlowRepository Members

        public IFlow GetStartupFlow(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow)).SetMaxResults(1);
            return criteria.UniqueResult<IFlow>();
        }


        public IFlow GetStartupFlow(IDataSource dataSource, bool isAgent)
        {
            Trace.TraceInformation("looking for flow");
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow))
                .Add(Restrictions.Eq("IsMainFlow",true)).Add(Restrictions.Eq("IsAgent",isAgent)).SetMaxResults(1);
            return criteria.UniqueResult<IFlow>();
        }
        public IFlow GetStartupFlow(IDataSource dataSource, bool isAgent,bool isStartUp)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow))
                .Add(Restrictions.Eq("IsMainFlow", true)).Add(Restrictions.Eq("IsAgent", isAgent));
            if (isStartUp)
            {
                criteria.Add(Restrictions.Eq("IsAccountActivation", true));
            }
            criteria.SetMaxResults(1);
            return criteria.UniqueResult<IFlow>();
        }
        public IFlow GetStartupFlowForFundsTransferCustomers(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow))
                .Add(Restrictions.Eq("IsFundsTransfer", true));
            
            criteria.SetMaxResults(1);
            return criteria.UniqueResult<IFlow>();
        }
        

        public IFlow GetByUniqueId(IDataSource dataSource, Guid uniqueId)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow));
            criteria.Add(Restrictions.Eq("UniqueId", uniqueId));
            return criteria.UniqueResult<IFlow>();
        }

        #endregion

       


        public IFlow GetActivationFlow(IDataSource dataSource)
        {
            ICriteria criteria = Session(dataSource).CreateCriteria(typeof(IFlow))
                .Add(Restrictions.Eq("IsMainFlow", true)).Add(Restrictions.Eq("IsAccountActivation",true)).SetMaxResults(1);
            return criteria.UniqueResult<IFlow>();
        }



        
    }
}
