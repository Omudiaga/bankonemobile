﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.NHibernate.Web
{
    /// <summary>
    /// Attribute for specifying a controller action should be executed under a NHibernate Transaction
    /// </summary>
    public class TransactionAttribute //: ActionFilterAttribute
    {
        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{

        //    //Begin the transaction just before the action executes
        //    //NHibernateSession.Current.BeginTransaction();
        //}

        //public override void OnActionExecuted(ActionExecutedContext filterContext)
        //{

        //    //Commit the transaction after the action has executed, 
        //    //as long as it's still active and no exception has been thrown
        //    /* if (filterContext.Exception == null && NHibernateSession.Current.Transaction.IsActive) {
        //         NHibernateSession.Current.Transaction.Commit();
        //     }*/
        //}
    }
}
