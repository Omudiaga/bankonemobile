﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Mapping;

namespace BankOneMobile.Data.NHibernate.FluentNHibernate.Conventions
{
    /// <summary>
    /// Fluent NHibernate Convention for Handling References 
    /// <remarks>
    /// Removes the "The" and adds "ID" to the end of the Property Name of the Entity
    /// </remarks>
    /// <example>
    /// TheFunction --> FunctionID
    /// TheUserRole --> UserRoleID
    /// </example>
    /// </summary>
    public class ReferencesConvention : IReferenceConvention
    {
        #region IConvention<IManyToOneInspector,IManyToOneInstance> Members

        public void Apply(global::FluentNHibernate.Conventions.Instances.IManyToOneInstance instance)
        {
            instance.Column((instance.Property.Name.StartsWith("The") ? instance.Property.Name.Remove(0, 3) : instance.Property.Name) + "ID");
            //instance.Not.LazyLoad();
        }

        #endregion
    }
}
