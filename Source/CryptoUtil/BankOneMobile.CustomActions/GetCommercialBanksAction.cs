﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetCommercialBanksDesigner))]
    public class GetCommercialBanksAction : ActionNode
    {
      
       

        /// <summary>
        /// List of CommercialBanks
        /// </summary>
        public OutArgument<IList<CommercialBank>> CommercialBanks
        {
            get
            {
                GetCommercialBanksActivity activity = this.InternalState as GetCommercialBanksActivity;
                if (activity != null)
                {
                    return activity.CommercialBanks;
                }
                return null;
            }
            set
            {
                GetCommercialBanksActivity activity = this.InternalState as GetCommercialBanksActivity;
                if (activity != null)
                {
                    activity.CommercialBanks = value;
                }
            }
        }

        public GetCommercialBanksAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetCommercialBanksActivity();
        }

    }

    public class GetCommercialBanksActivity : CodeActivity
    {
        
        
        public OutArgument<IList<CommercialBank>> CommercialBanks { get; set; }

        protected override void Execute(CodeActivityContext context)
        {



            var currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string instName = currentWorkflowInfo.InitiatedBy.BankAccountToDebit.InstitutionName;
         IList<CommercialBank> commercialBanks =    new CommercialBankSystem(currentWorkflowInfo.DataSource).GetActiveCommercialBanks().OrderBy(x => x.ShortName).Cast<CommercialBank>().ToList();
         
            
            commercialBanks.Where(x => x.Code == "000").FirstOrDefault().ShortName = instName;
             
         CommercialBanks.Set(context, commercialBanks);
            

           

        }
    }
}
