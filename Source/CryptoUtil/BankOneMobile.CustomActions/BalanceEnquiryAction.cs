﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(BalanceEnquiryDesigner))]
    public class BalanceEnquiryAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                 
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        public InArgument<string> CustomerAccountNumber
        {
            get
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {

                    return activity.CustomerAccountNumber;
                }
                return null;
            }
            set
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    activity.CustomerAccountNumber = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Balance
        {
            get
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    return activity.Balance;
                }
                return null;
            }
            set
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    activity.Balance = value;
                }
            }
        }
        /// <summary>
        /// Available Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> AvailableBalance
        {
            get
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    return activity.AvailableBalance;
                }
                return null;
            }
            set
            {
                BalanceEnquiryActivity activity = this.InternalState as BalanceEnquiryActivity;
                if (activity != null)
                {
                    activity.AvailableBalance = value;
                }
            }
        }

        public BalanceEnquiryAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new BalanceEnquiryActivity();
        }

    }

    public class BalanceEnquiryActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        public InArgument<string> CustomerAccountNumber { get; set; }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        /// 
        public OutArgument<string> AvailableBalance { get; set; }
        /// <summary>
        /// Available Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Balance { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
             pin  =  HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
             Institution inst = null; 
          //   pin = ThalesSim.Core.Utility.HexStringToByteArray(pin,);
            string accountNumber = AccountNumber.Get(context);
            string custAcct = CustomerAccountNumber.Get(context);
            IBalanceInquiryTransactionType tranType = new BalanceInquiryTransactionType();

            if (!string.IsNullOrEmpty(custAcct))
            {
                tranType.IsForThirdParty = true;
                accountNumber = custAcct;
            }


            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                if (vals.Count > 0)
                {
                    try
                    {
                        Agent ag;
                        if (vals.Count() > 1)
                        {
                            inst = new InstitutionSystem(currentWorkflowInfo.DataSource).GetByShortName(currentWorkflowInfo.DataSource, vals[1]) as Institution;
                            instCode = inst.Code;
                            mob.InstitutionCode=instCode;
                            //Check if agent
                            if (!mob.IsAgentAccount)
                            {
                                // if not agent
                                //Then its customer doing self service
                                //Use self service serviceCode
                                tranType.IsForSelfService = true;
                            }
                            else
                            {
                                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                                // If agent
                                //check if agent accoun number is selected acct no
                                if (accountNumber == ag.TheAgentAccount.BankAccount)
                                {
                                    // if yes use teller service code
                                    //service code we have been using all this while
                                }
                                else
                                {
                                    //then its agent doing self service.
                                    // else use self service ServiceCode
                                    tranType.IsForSelfService = true;
                                }
                            }
                        }
                        else
                        {
                            ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                            if (ag == null)
                            {
                                throw new TransactionNotPermittedToNonAgentsException("Non -Agent Initiated Trnsactions");
                            }
                            instCode = ag.TheAgentAccount.InstitutionCode;
                            mob.InstitutionCode = instCode;
                        }
                    }
                    catch (InvalidInstitutionException)
                    {
                        throw new FailureException("51", "Your Institution is not set up for Mobile Transactions");
                    }
                }
                else
                {
                   // inst= 
                }
                mob.InstitutionCode = instCode;
            }
            
            
            
            TransactionResponse response = null;       

            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //CRIMINAL.....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            Balance.Set(context, response.ResponseMessage);//set ledger balance 
            AvailableBalance.Set(context, response.ResponseDescription);//Set available balance
            
        
        }
    }
}
