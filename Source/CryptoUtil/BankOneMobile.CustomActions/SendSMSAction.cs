﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(SendSMSActionDesigner))]
    public class SendSMSAction : ActionNode
    {
        /// <summary>
        /// Test Value
        /// </summary>
        public InArgument<string> Message
        {
            get
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    return activity.Message;
                }
                return null;
            }
            set
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    activity.Message = value;
                }
            }
        }
        /// <summary>
        /// value to be compared against
        /// </summary>
        public InArgument<string> PhoneNumber
        {
            get
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// Account Number to debit for the amount of the sms
        /// </summary>
        public InArgument<string> BillableAccoutNumber
        {
            get
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    return activity.BillableAccountNumber;
                }
                return null;
            }
            set
            {
                SendSMSActivity activity = this.InternalState as SendSMSActivity;
                if (activity != null)
                {
                    activity.BillableAccountNumber = value;
                }
            }
        }
       
       

        public SendSMSAction ()
        
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SendSMSActivity();
        }

    }

    public class SendSMSActivity : CodeActivity
    {
        /// <summary>
        ///     Message to be sent in the SMS
        /// </summary>

        
        public InArgument<string> Message { get; set; }
       
        /// <summary>
        /// value to be tested against
        /// </summary>
         public InArgument<string> PhoneNumber { get; set; }
         /// <summary>
         /// Account Number to Bill
         public InArgument<string> BillableAccountNumber { get; set; }
       
        protected override void Execute(CodeActivityContext context)
        {
            string message = Message.Get(context);
            string phoneNo = PhoneNumber.Get(context);
            string accountNumber = BillableAccountNumber.Get(context);
            
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
                
              MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
              string instCode = string.Empty;
              if (accountNumber == null)
              {
                  accountNumber = mob.BankAccounts[0].BankAccount;
                  instCode = mob.BankAccounts[0].InstitutionCode;

              }
              else
              {
                  List<String> vals = accountNumber.Split(':').ToList();
                  accountNumber = vals[0];
                  instCode = vals[1];
                  mob.InstitutionCode = instCode;
              }
            if(string.IsNullOrEmpty(message))
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.NOSMSMessageSpecified), Utilities.GetDescription(ErrorCodes.NOSMSMessageSpecified));
            }
            if (!string.IsNullOrEmpty(phoneNo) && !MobileAccountSystem.CheckPhoneNumber(phoneNo))
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidPhoneNumber), Utilities.GetDescription(ErrorCodes.InvalidPhoneNumber));
            }
            else if(string.IsNullOrEmpty(phoneNo))
            {
               
                phoneNo = mob.MobilePhone;

            }
            try
            {
                SendSMS(instCode, message, phoneNo, accountNumber);
            }
            catch 
            {
                throw;// new FailureException(Utilities.GetCode(ErrorCodes.ErrorSendingSMS), Utilities.GetDescription(ErrorCodes.ErrorSendingSMS));
            }
           
        }

        private void SendSMS(string instCode, string message, string phoneNo, string acctNo)
        {
            string refNo = new TransactionSystem().GenerateTransactionRef();
            if (instCode == "10011")
            {
                instCode = "000000"; // TEST MFB ON LIVE HAS THIS CODE AND WE WILL NEED TO SEND TEST SMS
            }

            bool sent = new MobileAccountSystem().SendInstitutionSMS(message, phoneNo,instCode,acctNo);
            if (!sent)
            {
                string val = string.Format("Phone No: {0}, InstCode :{1} AccountNo: {2}, Ref No :{3}", phoneNo, instCode, acctNo,refNo);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(val));
                throw  new FailureException(Utilities.GetCode(ErrorCodes.ErrorSendingSMS), Utilities.GetDescription(ErrorCodes.ErrorSendingSMS));
            }

            
           
      
        }
    }
}
