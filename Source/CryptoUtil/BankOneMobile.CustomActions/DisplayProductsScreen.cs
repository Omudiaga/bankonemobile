﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{

    public class DisplayProductScreen : MenuScreen
    {
        protected CompletionCallback _OnFullCompleted;

        public OutArgument<string> OutputProductName
        {
            get
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;

                if (activity != null)
                {
                    return activity.OutPutProductName;

                }
                return null;
            }
            set
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;
                if (activity != null)
                {
                    activity.OutPutProductName = value;
                }
            }


        }
        public OutArgument<Product> OutputProduct
        {
            get
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;

                if (activity != null)
                {
                    return activity.OutputProduct;

                }
                return null;
            }
            set
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;
                if (activity != null)
                {
                    activity.OutputProduct = value;
                }
            }


        }
        public InArgument<IList<Product>> InPutProducts
        {
            get
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;

                if (activity != null)
                {
                    return activity.InputProducts;

                }
                return null;
            }
            set
            {
                GetProductActivity activity = this.DisplayActivity as GetProductActivity;
                if (activity != null)
                {
                    activity.InputProducts = value;
                }
            }


        }
        private Dictionary<int, Product> DictProducts;
        private List<Product> Products;
        private GetProductActivity DisplayActivity { get; set; }
        public DisplayProductScreen()
        {
            DictProducts = new Dictionary<int, Product>();
            DisplayProductActivity dis = new DisplayProductActivity();
            Products = new List<Product>();
            dis.SelectedProduct = new Product();
            this.InternalActivity = dis;
            GetProductActivity getProd = new GetProductActivity();
            getProd.TheProducts = new List<Product>();
            getProd.SelectedProduct = new Product();

            this.DisplayActivity = getProd;

        }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            ;
            //this.DisplayActivity.InputItems = this.InputItems;// = this.InternalActivity;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedCompletedCallback = OnDisplayActivityCompleted;
            context.ScheduleActivity(this.DisplayActivity, this.TheFlow.Resume_Flow_Completed);
            _OnFullCompleted = onCompleted;
            nextNode = null;
            return false;
        }

        internal void OnDisplayActivityCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            int i = 1;
            this.MenuItems.Clear();
            Products = (this.DisplayActivity as GetProductActivity).TheProducts.ToList();
         
            foreach (Product obj in Products)
            {
                this.MenuItems.Add(new MenuItem() { Text = obj.Name, Index = i });
                DictProducts.Add(i, obj);
                i++;
            }

            State nextNode = null;
            base.Execute(context, _OnFullCompleted, out nextNode);
        }
        public override void GetChildActivities(NativeActivityMetadata metadata)
        {


            metadata.AddChild(this.DisplayActivity);

            base.GetChildActivities(metadata);
        }
        

        protected override string ProcessResponse(string value)
        {
            string response = string.Empty;
            int selectedMenu = 0;
            if (value != null)
            {
                if (Int32.TryParse(value, out selectedMenu))
                {
                    if (this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu) != null)
                    {
                        response = this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu).Text;
                        (this.InternalActivity as DisplayProductActivity).SelectedProduct = this.DictProducts[selectedMenu];
                    }
                    else
                    {
                        //this.SelectedProduct = this.DictProducts[selectedMenu];
                        
                        response = value;
                    }
                }

            }
            return response;
        }


        
    }
    public class DisplayProductActivity : SubActivity
    {
        public Product SelectedProduct;
        public InArgument<IList<Product>> InputProducts { get; set; }
        public OutArgument<string> OutPutProductName { get; set; }
        public OutArgument<Product> OutputProduct
        {
            get;
            set;
        }
        protected override void Execute(CodeActivityContext context)
        {
            //context.SetValue(Response, TheResponse);
            context.SetValue(OutputProduct, this.SelectedProduct);
            base.Execute(context);
        }

    }
    public class GetProductActivity : SubActivity
    {
        public Product SelectedProduct;
        public InArgument<IList<Product>> InputProducts { get; set; }
        public OutArgument<string > OutPutProductName { get; set; }
        public OutArgument<Product> OutputProduct { get; set; }
        public IList<Product> TheProducts { get; set; }
        
        protected override void Execute(CodeActivityContext context)
        {
            TheProducts = InputProducts.Get(context);
            // TheProducts = InputProducts.Get(context);
            ////context.SetValue(Response, TheResponse);
            //context.SetValue(OutputProduct, this.SelectedProduct);
            base.Execute(context);
        }

    }
}
