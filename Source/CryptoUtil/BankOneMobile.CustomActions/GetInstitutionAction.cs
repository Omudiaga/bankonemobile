﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(BalanceEnquiryDesigner))]
    public class GetInstitutionAction : ActionNode
    {
        
        

        /// <summary>
        /// Produts for agent's institution
        /// </summary>
        public OutArgument<List<string>> Products
        {
            get
            {
                GetInstitutionActivity activity = this.InternalState as GetInstitutionActivity;
                if (activity != null)
                {
                    return activity.Products;
                }
                return null;
            }
            set
            {
                GetInstitutionActivity activity = this.InternalState as GetInstitutionActivity;
                if (activity != null)
                {
                    activity.Products = value;
                }
            }
        }

        public GetInstitutionAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetInstitutionActivity();
        }

    }

    public class GetInstitutionActivity : CodeActivity
    {
       

        /// <summary>
        /// Products for the agent's institution
        /// </summary>
        public OutArgument<List<string>> Products { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            

            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            //ITransactionType tranType = new BalanceInquiryTransactionType() ;

            //TransactionResponse response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            //decimal returnValue = Convert.ToDecimal(response.ResponseMessage);
            //Balance.Set(context,string.Format("{0:N}", returnValue));

        }
    }
}
