﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.CustomActions
{
    public enum ErrorCodes
    {
       PhoneNumberNotRegisterd = 01,
       PhoneNumberAlreadyRegistered = 02,
        InvalidPin =03,
        PinEntriesDoNotMatch = 04,
        IssuerOrSwitchUnavailable =05,
        TransactionNotPermittedToNonAgents=06,
        PhoneNumberNotActiveOnBankOne=07,
        InvalidAgentCode=08,
        InvalidActivationCode=09,
        InvalidFundsTransferCode=10,
        NoPendingFundsTransferForThisPhoneNumber=11,
        FundsTransferError=12,
        TransactionFailed=13,
        SystemError = 14,
        TransactionNotInitiatedByAgent=15,
        UnableToConnectToCoreBanking=16,
        CustomerIsInactive=17,
        FundsTransferHasAlreadyBeenCashed=18,
        AccountActivationFailed=19,
        SwitchUnavailable=20,
        IssuerUnavailable=21,
        InvalidAccountNumber =22,
        HSMUnavailable=23,
        ISWGatewayUnavailable=24,        
        HSMError=25,
        AccountCreatedNoFundsTransfer = 26,
        InvalidPhoneNumber=27,
        NOSMSMessageSpecified=28,
        ErrorSendingSMS=29,
        SessionInitiationFailed =30,
        AccountNumberAlreadyExists=31,
        OnlyDepositTransactionsAreAllowedOnThisAccount=31,
        InterMFBTransactionsNotSupportedByInstitution=32,
        AmountIsAbovePostingLimt=32
        

        
    }

}
