using System;
using System.Diagnostics;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using System.Text;

namespace BankOneMobile.CustomActions
{
    public class Utilities
    {
        public static string PluralOf(string text)
        {
            var pluralString = text;
            var lastCharacter = pluralString.Substring(pluralString.Length - 1).ToLower();

            // y�s become ies (such as Category to Categories)
            if (string.Equals(lastCharacter, "y", StringComparison.InvariantCultureIgnoreCase) && !pluralString.EndsWith("ay", StringComparison.InvariantCultureIgnoreCase))
            {
                pluralString = pluralString.Remove(pluralString.Length - 1);
                pluralString += "ie";
            }

            // ch�s become ches (such as Pirch to Pirches)
            if (string.Equals(pluralString.Substring(pluralString.Length - 2), "ch",
                              StringComparison.InvariantCultureIgnoreCase))
            {
                pluralString += "e";
            }

            switch (lastCharacter)
            {
                case "s":
                    return pluralString + "es";

                default:
                    return pluralString + "s";

            }

        }

        public static string SpaceWords(string words)
        {
            StringBuilder result = new StringBuilder();
            foreach (char letter in words.ToCharArray())
            {
                if (char.IsUpper(letter))
                {
                    result.Append(' ');
                }
                result.Append(letter);
            }
            return result.Replace('_', ' ').ToString();
        }

        public static string GetCode(ErrorCodes code)
        {
            return ((int)code).ToString().PadLeft(2, '0');
        }
        public static string GetDescription(ErrorCodes code)
        {
            return SpaceWords(code.ToString());
        }

       
# region commented Code
        /*
        public string  EncryptPinBlock(string pan, string pin, string format, string key)
        {

    
    var bloc = GeneratePinBlock(pan, pin, format);
   
    var tripledes = false;
    if (key.Length >= 32)
    {
        tripledes = true;
        //bloc = bloc + "0808080808080808"; 
    }

  
  string hexbloc = Convert.ToInt32(bloc).ToString("X");
  string pinblock = hexbloc.PadLeft(16, '0'); //format pin block
  
    //pinblock = "This is the message to encrypt, it will be padded.";
  string   key1 =Convert.ToInt32(key.Substring(0, 16)).ToString("X"); //format key 1
    //key1 = "0x" + key.substring(0, 16);

    //encrypt pinblock using key 1
            //string cipher1 = ThalesSim.Core.Cryptography.DES.
    cipher1 = des(key1, pinblock, 1, 0);

    if (tripledes) {
        key2 = hexToString("0x" + key.substring(16, 32)); //format key 2
        //key2 = "0x" + key.substring(16, 32); //format key 2
        cipher2 = des(key2, cipher1, 0, 0); //decrypt
        cipher1 = des(key1, cipher2, 1, 0); //encrypt
    }

    var output = stringToHex(cipher1);
    return output.replace("0x", "");
}
        */
# endregion
    }
}
