﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetPaymentItemsDesigner))]
    public class GetPaymentItemsAction : ActionNode
    {
      
       

        public OutArgument<IList<PaymentItem>> PaymentItems
        {
            get
            {
                GetPaymentItemsActivity activity = this.InternalState as GetPaymentItemsActivity;
                if (activity != null)
                {
                    return activity.PaymentItems ;
                }
                return null;
            }
            set
            {
                GetPaymentItemsActivity activity = this.InternalState as GetPaymentItemsActivity;
                if (activity != null)
                {
                    activity.PaymentItems = value;
                }
            }
        }
        public InArgument<Merchant> TheMerchant
        {
            get
            {
                GetPaymentItemsActivity activity = this.InternalState as GetPaymentItemsActivity;
                if (activity != null)
                {
                    return activity.TheMerchant;
                }
                return null;
            }
            set
            {
                GetPaymentItemsActivity activity = this.InternalState as GetPaymentItemsActivity;
                if (activity != null)
                {
                    activity.TheMerchant = value;
                }
            }
        }

        public GetPaymentItemsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetPaymentItemsActivity();
        }

    }

    public class GetPaymentItemsActivity : CodeActivity
    {


        public OutArgument<IList<PaymentItem>> PaymentItems { get; set; }
        public InArgument<Merchant> TheMerchant { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            Merchant merch = TheMerchant.Get(context);IList<PaymentItem> items = null;
            try
            {
                items = new MerchantSystem().GetPaymentItemsByMerchant(merch);
            }
            catch (FaultException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), Utilities.GetDescription(ErrorCodes.ISWGatewayUnavailable));
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), "Error on ISW Gateway");
            }
            
            PaymentItems.Set(context, items);
            


        }
    }
}
