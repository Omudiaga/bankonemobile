﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using System.Diagnostics;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CashInAgentDesigner))]
    public class CashInAgentAction : ActionNode
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        public OutArgument<string> TransactionSuccessful
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.TransactionSuccesful;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.TransactionSuccesful = value;
                }
            }
        }
        public InArgument<string> PIN
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhone_Account
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerPhone_Account;
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.CustomerPhone_Account = value;
                }
            }
        }
        /// <summary>
        ///Agent Code
        /// </summary>
        public InArgument<string> AgentCode
        {
            get
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    return activity.AgentCode;
                        
                }
                return null;
            }
            set
            {
                CashInAgentActivity activity = this.InternalState as CashInAgentActivity;
                if (activity != null)
                {
                    activity.AgentCode = value;
                }
            }
        }
       
       
       

        public CashInAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CashInAgentActivity();
        }

    }

    public class CashInAgentActivity : CodeActivity
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// Agent Code
        /// </summary>
        public InArgument<string> AgentCode { get; set; }
        
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhone_Account { get; set; }
        public InArgument<string> PIN{get; set; }
        public OutArgument<string> TransactionSuccesful { get; set; }
       

        
        
        
        protected override void Execute(CodeActivityContext context)
        {

            
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string notification = Notification.Get(context);

            string customerPhone_Account = CustomerPhone_Account.Get(context);
            
            string agentCode = AgentCode.Get(context);
            string amount = Amount.Get(context);
            string pin=PIN.Get(context);
            string realResponse = string.Empty;
            
            //try
            //{
            //    pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            //}
            //catch (NoHSMResponseException)
            //{
            //    throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            //}
            //catch
            //{
            //    throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            //}
            Trace.TraceInformation("Gotten Data");
            ICashInTransactionType tranType = new CashInTransactionType();
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            string instCode = string.Empty;
            if (customerPhone_Account == null)
            {
                customerPhone_Account = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;
                Trace.TraceInformation("Cust Phone acct was null but im out");

            }
            else
            {
                List<String> vals = customerPhone_Account.Split(':').ToList();
                customerPhone_Account = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;
                Trace.TraceInformation("Cust Phone acct was  NOT null but im out");
            }
            Agent theAgent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(phoneNumber) as Agent;
            Trace.TraceInformation("Got Agent");
            tranType.Amount = Convert.ToDecimal(amount);
            tranType.AgentCode = agentCode;
            tranType.Token = instCode;// just save it her cos we need it
            TransactionResponse response = new TransactionResponse();

            try
            {
                Trace.TraceInformation("About to Run");
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, theAgent.TheAgentAccount.BankAccount, customerPhone_Account, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                if (response.Status == TransactionStatus.Successful.ToString())
                {
                    realResponse = "true";
                }
               
               
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents);
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
                
            }
            catch (InvalidAgentCodeException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.InvalidAgentCode);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.SwitchUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.IssuerUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                realResponse=ex.Message;
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                realResponse="System Error";
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {

                TransactionSuccesful.Set(context,realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP...... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }



           //tring result = TransactionSystem.RunTransaction(pin);
            //context.SetValue(Notification,response.ResponseMessage);
            //string result = TransactionSystem.RunTransaction(pin);

            

        }
    }

}
