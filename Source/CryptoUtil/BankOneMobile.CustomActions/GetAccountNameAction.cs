﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetAccountNameDesigner))]
    public class GetAccountNameAction : ActionNode
    {
        
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// bool is phone number
        /// </summary>
        public InArgument<bool> IsPhoneNumber
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.IsPhoneNumber;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumber = value;
                }
            }
        }
        public InArgument<bool> IsOnCoreBanking
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.IsOnCoreBanking;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.IsOnCoreBanking = value;
                }
            }
        }
        public InArgument<bool> ForLoggedOnCustomer
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.IsForLoggedOnCustomer;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.IsForLoggedOnCustomer = value;
                }
            }
        }

        /// <summary>
        /// account Name
        /// </summary>
        public OutArgument<string> AccountName {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.AccountName;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.AccountName = value;
                }
            }
        }
        public OutArgument<string> CoreBankingPhoneNumber
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.CoreBankingPhoneNumber;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.CoreBankingPhoneNumber = value;
                }
            }
        }
        public OutArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.CoreBankingCustomer;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.CoreBankingCustomer = value;
                }
            }
        }
        public OutArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount
        {
            get
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    return activity.CoreBankingAccount;
                }
                return null;
            }
            set
            {
                GetAccountNameActivity activity = this.InternalState as GetAccountNameActivity;
                if (activity != null)
                {
                    activity.CoreBankingAccount = value;
                }
            }
        }

        public GetAccountNameAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetAccountNameActivity();
        }

    }

    public class GetAccountNameActivity : CodeActivity
    {
        /// <summary>
        /// Used to check if its being used for the customer doing the transaction
        /// </summary>
        public InArgument<bool> IsForLoggedOnCustomer { get; set; }
        /// <summary>
        /// Used to check if the validation is from Core Banking or not
        /// </summary>
        public InArgument<bool> IsOnCoreBanking { get; set; }
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        /// <summary>
        /// Type OF ID i.e. account Number or Phone Number
        /// </summary>
        public InArgument<bool> IsPhoneNumber { get; set; }
        public InArgument<bool> IsForSelfService { get; set; }
        /// <summary>
        /// Recharge PIN 
        /// </summary>
        public OutArgument<string> AccountName { get; set; }

        /// <summary>
        /// Phone Number On Core Banking
        /// </summary>
        public OutArgument<string> CoreBankingPhoneNumber { get; set; }
        /// <summary>
        /// Core Banking Customer
        /// </summary>
        public OutArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer { get; set; }
        /// <summary>
        /// Core Banking Account
        /// </summary>
        public OutArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount { get; set; }


        protected override void Execute(CodeActivityContext context)
        {

            string phoneNumber = PhoneNumber.Get(context);
            string accountNumber = AccountName.Get(context);
            bool isPhoneNumber = IsPhoneNumber.Get(context);
            bool isForLoggedOnCustomer = IsForLoggedOnCustomer.Get(context);
            bool isOnCoreBanking = IsOnCoreBanking.Get(context);
            bool isForSelfService = IsForSelfService.Get(context);
            MobileAccount mobile = null;
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
             ag=   new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
             instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }

            if (isForLoggedOnCustomer)
            {
                string custName = mob.LastName + " " + mob.OtherNames;
                context.SetValue(AccountName, custName);
                return;
            }
            if (!isOnCoreBanking)
            {

                if (isPhoneNumber)
                {
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                        mobile = client.GetMobileAccountByPhoneNumberAndInstitution(phoneNumber, instCode); //new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
                        //AccountName.Set(context, string.Format("{0}:{1}", mobile.LastName, mobile.OtherNames));
                    }
                    if (mobile == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

                    }
                    else
                    {
                    }
                }
                else
                {
                    LinkingBankAccount bankAccount = null;
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                      //  mobile = client.GetMobileAccountBy(phoneNumber);


                        bankAccount = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(phoneNumber,instCode); //client.GetAccountByAccountNumber(phoneNumber) as LinkingBankAccount;
                        mobile = bankAccount==null? null:bankAccount.TheMobileAccount as MobileAccount;
                    }
                    // new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetBankAccountByAcccountNo(phoneNumber, mob.InstitutionCode);
                    if (mobile == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAccountNumber), Utilities.GetDescription(ErrorCodes.InvalidAccountNumber));
                    }
                    //mobile  =bankAccount.TheMobileAccount as MobileAccount;
                }
            }

            else
            {
                using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new BankOneMobile.Services.CoreBankingService.SwitchingServiceClient())
                {
                    BankOneMobile.Services.CoreBankingService.Account ac = client.GetAccountByAccountNo(instCode,phoneNumber);
                    if (ac == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAccountNumber), Utilities.GetDescription(ErrorCodes.InvalidAccountNumber));
                    }
                    else
                    {
                        
                        BankOneMobile.Services.CoreBankingService.Customer cust = client.GetCustomer(instCode,ac.CustomerID);
                        string accName = string.Format("{0},{1}", cust.LastName, cust.OtherNames);
                        AccountName.Set(context, accName);
                        context.SetValue<BankOneMobile.Services.CoreBankingService.Customer>(CoreBankingCustomer, cust);
                        context.SetValue<BankOneMobile.Services.CoreBankingService.Account>(CoreBankingAccount,ac);
                        //CoreBankingAccount.Set(context, ac);
                        //CoreBankingCustomer.Set(context,cust);
                        CoreBankingPhoneNumber.Set(context, cust.PhoneNo);
                        return;
                    }
                }
            }
            string name = mobile.LastName + " " + mobile.OtherNames;
            context.SetValue(AccountName, name);
            //TODO: Do Balance Enquiry
            //string rechargePInCode = TransactionSyst    unTransaction(pin);
             //RechargePIN.Set(context,rechargePInCode);

            

        }
    }

}
