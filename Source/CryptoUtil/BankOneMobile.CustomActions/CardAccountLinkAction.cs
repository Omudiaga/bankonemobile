﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.CustomActions
{

    [Designer(typeof(AccountActivationDesigner))]
    public class CardAccountLinkAction : ActionNode
    {
        
        public InArgument<string> SerialNumber {
            get
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    return activity.SerialNumber;
                }
                return null;
            }
            set
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    activity.SerialNumber = value;
                }
            }
        }

        
        public InArgument<string> CardPAN
        {
            get
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    return activity.CardPAN;
                }
                return null;
            }
            set
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    activity.CardPAN = value;
                }
            }
        }
        public InArgument<string> AccountNumber
        {
            get
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        public InArgument<bool> IsSerialNumberBased
        {
            get
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    return activity.IsSerialNumberBased;
                }
                return null;
            }
            set
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    activity.IsSerialNumberBased = value;
                }
            }
        }
        public OutArgument<string> Response
        {
            get
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    return activity.Response;
                }
                return null;
            }
            set
            {
                CardAccountLinkActionActivity activity = this.InternalState as CardAccountLinkActionActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }

        public CardAccountLinkAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CardAccountLinkActionActivity();
        }

    }

    public class CardAccountLinkActionActivity : CodeActivity
    {
        
        public InArgument<string> SerialNumber { get; set; }
       
        public InArgument<string> AccountNumber { get; set; }
       
        public InArgument<string> CardPAN { get; set; }
        /// <summary>
        /// if true,  Serial Number is the basis for linking not Card PAN 
        /// </summary>
        public InArgument<bool> IsSerialNumberBased { get; set; }
        public OutArgument<string > Response { get; set; }
        
        

        protected override void Execute(CodeActivityContext context)
        {
            string serialNo = SerialNumber.Get(context);
            string accountNo = AccountNumber.Get(context);
            string cardPan = CardPAN.Get(context);
            bool isSerialNoBased = IsSerialNumberBased.Get(context);

            

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            if (mob.MobileAccountStatus == MobileAccountStatus.AwaitingApproval)
            {
                throw new FailureException("01","Account Linking has not yet been approved");
            }
            string phoneNumber = mob.MobilePhone;     
            
           LinkCardToAccount(isSerialNoBased? serialNo:cardPan,isSerialNoBased,accountNo);
           
            Response.Set(context, "success");

            

        }
        public void  LinkCardToAccount(string identifier, bool isSerialNumber, string acctNo)
        {
            string toReturn = String.Empty;
            


            try
            {
                //new TransactionSystem().RunTransaction(activationCode, tranType, mob, trans.From, string.Empty, 0.0M, sessionID);
                
            }
            
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            finally
            {


            }

            }
        
    }


}
