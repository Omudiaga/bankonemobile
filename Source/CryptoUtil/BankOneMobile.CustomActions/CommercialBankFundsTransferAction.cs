﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Data.Implementations;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CommercialBankFundsTransferDesigner))]
    public class CommercialBankFundsTransferAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// Account Tpe: Savings or Current
        /// </summary>
        public InArgument<AccountMode> AccountType
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.AccountType;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.AccountType = value;
                }
            }
        }

        /// <summary>
        /// AccoutnNumber
        /// </summary>
        public InArgument<string> BankName
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.BankName;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.BankName = value;
                }
            }
        }

        
        /// <summary>
        ///  Account Number to recieve transfer
        /// </summary>
        public InArgument<string> ToAccountNumber
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.ToAccountNumber;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.ToAccountNumber = value;
                }
            }
        }
        /// <summary>
        ///  Account Name to recieve transfer
        /// </summary>
        public InArgument<string> AccountName
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.AccountName;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.AccountName = value;
                }
            }
        }
        /// <summary>
        ///  Account Number to recieve transfer
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        ///  Transfer Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }

        /// <summary>
        /// Notification of success / failure
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        /// <summary>
        /// reference code 4 success
        /// </summary>
        public OutArgument<string> FailureReference
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.FailureReference;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.FailureReference = value;
                }
            }
        }
        /// <summary>
        /// reference code 4 failure
        /// </summary>
        public OutArgument<string> SuccessReference
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.SuccessReference;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.SuccessReference = value;
                }
            }
        }
        public InArgument<CommercialBank> SelectedBank
        {
            get
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    return activity.SelectedBank;
                }
                return null;
            }
            set
            {
                CommercialBankFundsTransferActivity activity = this.InternalState as CommercialBankFundsTransferActivity;
                if (activity != null)
                {
                    activity.SelectedBank = value;
                }
            }
        }
       

        public CommercialBankFundsTransferAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CommercialBankFundsTransferActivity();
        }

    }

    public class CommercialBankFundsTransferActivity : CodeActivity
    {
        /// <summary>
        ///Notification of success / failure
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>        
        public InArgument<string> AccountNumber { get; set; }
        /// <summary>
        /// Amount to transfer
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Commercial Bank Account Number to recieve the transfer
        /// </summary>
        public InArgument<string> ToAccountNumber { get; set; }
        /// <summary>
        /// Commercial Bank Name to recieve transfer 
        /// </summary>
        public InArgument<string> BankName { get; set; }
        /// <summary>
        /// Name of the Account
        /// </summary>
        public InArgument<string> AccountName { get; set; }
        /// <summary>
        /// reference code should action succeed
        /// </summary>
        public OutArgument<string> SuccessReference { get; set; }
        /// <summary>
        /// reference code should action fail
        /// </summary>
        public OutArgument<string> FailureReference { get; set; }
        /// <summary>
        /// The Selected Bank
        /// </summary>
        public InArgument<CommercialBank> SelectedBank { get; set; }
        /// <summary>
        /// The Account Type Selected
        /// </summary>
        public InArgument<AccountMode> AccountType { set; get; }
        

        

        protected override void Execute(CodeActivityContext context)
        {
            string selectedBank = BankName.Get(context);
            AccountMode AccountType=this.AccountType.Get(context);
            string pin = PIN.Get(context);
            string toAccount = ToAccountNumber.Get(context);
            string accountNumber = AccountNumber.Get(context);
            CommercialBank bank = SelectedBank.Get(context);
            // new CommercialBank { Code = "044", Name = "Access Bank" };//SelectedBank.Get(context);
            string bankName = bank.Name;
            string bankCode = bank.Code;
            string amount = Amount.Get(context);
            string accountName = AccountName.Get(context);
            string beneficiaryLastName = string.Empty;
            string beneficiaryOtherName = string.Empty;
            if (accountName.Contains(" "))
            {
                beneficiaryLastName = accountName.Substring(0, accountName.IndexOf(" "));
                beneficiaryOtherName = accountName.Substring(accountName.IndexOf(" "));
            }
            else
            {
                beneficiaryLastName = accountName;
                beneficiaryOtherName = accountName;
            }

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;
            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;
                mob.InstitutionCode = instCode;
            }
            
            ICommercialBankFundsTransferTransactionType tranType = new CommercialBankFundsTransferTransactionType();
            tranType.TheMobileAccount = mob;
            tranType.AccountNumber = toAccount;
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            tranType.Amount = Convert.ToDecimal(amount)*100;

            tranType.TheBankAccount = tranType.TheMobileAccount.BankAccounts.Where(x => x.BankAccount == accountNumber).FirstOrDefault() as LinkingBankAccount;
            tranType.BankName = bank.Name;
            tranType.BankCode = bank.Code;
            tranType.TransactionReference = new BankOneMobile.Services.TransactionSystem().GenerateTransactionRef().Substring(8);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Transaction ref is "+tranType.TransactionReference));
            

            tranType.BeneficiaryLastName = beneficiaryLastName;
            tranType.BeneficiaryOtherNames = beneficiaryOtherName;
            tranType.TheAccountType = AccountType;
           
           

            //TODO: Do Balance Enquiry
            TransactionResponse response = new TransactionResponse();

            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob,accountNumber,accountNumber, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (InvalidAgentCodeException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (NoISWGatewayResponse)
            {
                string errorDesc = "No Response from ISW Gateway";
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), errorDesc);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                
                //STOP.... THIS IS CRIMINAL....
                //nkOneMobile.Data.NHibernate.DbContext.Instance.Terminate();
                SuccessReference.Set(context, tranType.PaymentReference);
                SuccessReference.Set(context, tranType.TransactionReference);

            }


            

        }
    }

}
