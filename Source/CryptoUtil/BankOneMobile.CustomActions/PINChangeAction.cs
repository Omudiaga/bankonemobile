﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Data.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(PinChangeDesigner))]
    public class PINChangeAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// New PIN entered the first Time
        /// </summary>
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> NewPIN1
        {
            get
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    return activity.NewPIN1;
                }
                return null;
            }
            set
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    activity.NewPIN1 = value;
                }
            }
        }

        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        public InArgument<string> NewPIN2
        {
            get
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    return activity.NewPIN2;
                }
                return null;
            }
            set
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    activity.NewPIN2 = value;
                }
            }
        }
        public OutArgument<string> Response
        {
            get
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    return activity.Response;
                }
                return null;
            }
            set
            {
                PINChangeActivity activity = this.InternalState as PINChangeActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }

        public PINChangeAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new PINChangeActivity();
        }

    }

    public class PINChangeActivity : CodeActivity
    {
        /// <summary>
        /// User Old PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }
        /// <summary>
        /// New PIN Entered the first Time
        /// </summary>
        public InArgument<string> NewPIN1 { get; set; }
        /// <summary>
        /// New PIN entered the second time
        /// </summary>
        public InArgument<string> NewPIN2 { get; set; }
        /// <summary>
        /// Succesful or Failed Response
        /// </summary>
        public OutArgument<string> Response { get; set; }
        
        

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source = DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
            string customerPhoneNumber = CustomerPhoneNumber.Get(context);
            try
            {
                pin = HSMCenter.GeneratePinBlock(mob.MobilePhone, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string pin1 = NewPIN1.Get(context);
            string pin2 = NewPIN2.Get(context);

            if (pin1 != pin2)
            {
                //string message = string.Format("Values are pin1 -{0}, pin2-{1}", pin1,pin2);
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
                throw new FailureException(Utilities.GetCode(ErrorCodes.PinEntriesDoNotMatch), Utilities.GetDescription(ErrorCodes.PinEntriesDoNotMatch));
                
            }

            
            bool demoMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HSMDemoMode"]);
            if (!string.IsNullOrEmpty(customerPhoneNumber))
            {
                mob = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetByPhoneNumber(customerPhoneNumber);
                if (mob == null)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidPhoneNumber), Utilities.GetDescription(ErrorCodes.InvalidPhoneNumber));
                }
            }
            if (demoMode)
            {
                mob.PIN = pin1;
                mob.MobileAccountStatus = MobileAccountStatus.Active;

                try
                {
                    new MobileAccountSystem(currentWorkflowInfo.DataSource).UpdateMobileAccount(mob);
                }
                catch
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), Utilities.GetDescription(ErrorCodes.SystemError));
                }
            }
            else
            {

                ITransaction trans = new Transaction();
                IPinChangeTransactionType tranType = new PinChangeTransactionType();
                
               // ta.EncryptedPIN = pin1;
                tranType.NewPIN = HSMCenter.GeneratePinBlock(mob.MobilePhone,pin1, AppZone.HsmInterface.PinBlockFormats.ANSI);
               
                trans.FromInstitutionCode = mob.InstitutionCode;
                trans.From = mob.BankAccounts[0].BankAccount; //mob.RecievingBankAccount.BankAccount;
                try
                {
                    new TransactionSystem().RunTransaction(pin,tranType, mob, trans.From, string.Empty, 0.0M, sessionID);
                   // (pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                }
                catch (NoSwitchResponseException)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
                }
                catch (NoIssuerResponseException)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
                }
                catch (FailedTransactionException ex)
                {
                    throw new FailureException("13", ex.Message);
                }
                catch (InvalidPhoneNumberException)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

                }
                catch (NoHSMResponseException)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
                }
                catch (Exception ex)
                {
                    new PANE.ERRORLOG.Error().LogToFile(ex);
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                }
                finally
                {


                    if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                    {
                        try
                        {
                            new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                        }
                        catch
                        {
                        }

                    }
                    //STOP.....THIS IS CRIMINAL...
                    //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                }
            }

            

        }
    }


}
