﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CashOutDesigner))]
    public class CashOutAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        ///Selected  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> WithdrawalLocation
        {
            get
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    return activity.WithdrawalLocation;
                }
                return null;
            }
            set
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    activity.WithdrawalLocation = value;
                }
            }
        }
        /// <summary>
        ///Entered Amount 
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }

        /// <summary>
        /// Generated Toke
        /// </summary>
        public OutArgument<string> Token {
            get
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    return activity.Token;
                }
                return null;
            }
            set
            {
                CashOutActivity activity = this.InternalState as CashOutActivity;
                if (activity != null)
                {
                    activity.Token = value;
                }
            }
        }

        public CashOutAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CashOutActivity();
        }

    }

    public class CashOutActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }


        /// <summary>
        /// Generated Token for transaction
        /// </summary>
        public OutArgument<string> Token { get; set; }
        /// <summary>
        /// Cash Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// ATM/Bank Branch
        /// </summary>
        public InArgument<string> WithdrawalLocation { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            string pin = PIN.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string withdrawalLocation = WithdrawalLocation.Get(context);
            string token = Token.Get(context);
            string amount = Amount.Get(context);

            //TODO: Do Balance Enquiry
            string result = TransactionSystem.RunTransaction(pin);

            Token.Set(context, result);

        }
    }

}
