﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;
using System.Diagnostics;

namespace BankOneMobile.CustomActions
{

    // InstCode //Phone No, //Account No // Service Register
    [Designer(typeof(BalanceEnquiryDesigner))]
    public class RegisterLafayetteCustomerOnOrbitAction : ActionNode
    {
        
       
        public InArgument<string>PhoneNumber
        {
            get
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public InArgument<string> AccountNumber
        {
            get
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

       
        public OutArgument<string> Registered
        {
            get
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    return activity.Registered;
                }
                return null;
            }
            set
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    activity.Registered = value;
                }
            }
        }

        public OutArgument<string> ResponseDescription
        {
            get
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    return activity.ResponseDescription;
                }
                return null;
            }
            set
            {
                RegisterLafayetteCustomerOnOrbitActivity activity = this.InternalState as RegisterLafayetteCustomerOnOrbitActivity;
                if (activity != null)
                {
                    activity.ResponseDescription = value;
                }
            }
        }
        
        

        public RegisterLafayetteCustomerOnOrbitAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new RegisterLafayetteCustomerOnOrbitActivity();
        }

    }

    public class RegisterLafayetteCustomerOnOrbitActivity : CodeActivity
    {
       
        public InArgument<string> AccountNumber { get; set; }
        public InArgument<string> PhoneNumber { get; set; }        
        public OutArgument<string> Registered { get; set; }
        public OutArgument<string> ResponseDescription { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
           WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;     
                  bool registered = false; 
                  bool registerUserSpecified = false;
            string reason= string.Empty;
            string phoneNumber = PhoneNumber.Get(context);
            string acctNo = AccountNumber.Get(context);
            LafyetteCustReg.SwitchingService serv = new LafyetteCustReg.SwitchingService();
                                

            try
            {                    
                string toSend = string.Format("About to Send Details -Agent{0}, Phone Number - {1}, Account Number {2}", mob.MobilePhone, phoneNumber, acctNo);
                Trace.TraceInformation(toSend);
                registered = new TransactionSystem().RegisterLafayetteCustomer(mob.InstitutionCode, mob.MobilePhone, acctNo, out reason);
                serv.RegisterMobileUser(mob.InstitutionCode, phoneNumber, acctNo, "Mobile", out  registered, out registerUserSpecified);
                Trace.TraceInformation("xxx Done with registration {0} {1}",phoneNumber,registered);

            }
            catch (Exception ex)
            {
                string errorMsg = String.Format("Error Saving Flow - {0}, Inner Message Is {1}, Inner Stack Trace is {2}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.StackTrace);
                Trace.TraceInformation(errorMsg);
            }
            
                 
           Registered.Set(context, registered.ToString());
           ResponseDescription.Set(context, registered.ToString());
            
        
        }  
 

    }
}
