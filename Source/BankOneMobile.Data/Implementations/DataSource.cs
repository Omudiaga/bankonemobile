﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers;
using System.Diagnostics;

namespace BankOneMobile.Data.Implementations
{
    public class DataSource : IDataSource
    {
        internal DataSource(DataCategory type)
        {
            if (type == DataCategory.Tenant)
            {
                throw new ArgumentException("A Tenant is required for DataSourceType Tenant", "type");
            }
            Type = type;
        }

        internal DataSource(DataCategory type, string sessionKey) : this(type)
        {
            _SessionKey = sessionKey;
        }

        internal DataSource(DataCategory type, IInstitution theTenant)
        {
            if (type != DataCategory.Tenant && theTenant != null)
            {
                throw new ArgumentException("A Tenant can only be specified for DataSourceType Tenant", "theTenant");
            }
            if (type == DataCategory.Tenant && theTenant == null)
            {
                throw new ArgumentNullException("theTenant", "A Valid Tenant must be specified for DataSourceType Tenant");
            }
            Type = type;
            TheTenant = theTenant;
        }


        internal DataSource(DataCategory type, Guid workflowId)
            : this(type)
        {
            if (type == DataCategory.Tenant)
            {
                throw new ArgumentNullException("theTenant", "A Valid Tenant must be specified for DataSourceType Tenant");
            }
            WorkflowId = workflowId;
        }


        internal DataSource(DataCategory type, IInstitution theTenant, Guid workflowId)
            : this(type, theTenant)
        {
            WorkflowId = workflowId;
        }



        public DataCategory Type { get; protected set; }
        public IInstitution TheTenant { get; protected set; }
        public Guid WorkflowId { get; protected set; }

        public string FactoryKey
        {

            get
            {
                string factoryKey = this.Type.ToString();
                if (this.Type == DataCategory.Tenant)
                {
                    factoryKey = string.Format("BankOneMobile_{0}_{1}", factoryKey, TheTenant.Code);
                }
                return factoryKey;
            }
        }

        public string SessionKey
        {
            get
            {
                return _SessionKey;
            }
        }

        public string StorageKey
        {
            get
            {
                string storageKey = this.Type.ToString();
                if (!string.IsNullOrEmpty(_SessionKey))
                {
                    storageKey += string.Format("_{0}", _SessionKey);
                }
                return storageKey;
            }
        }

        private string _SessionKey;

        public override int GetHashCode()
        {
            return (this.FactoryKey + this.SessionKey).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj != null)
            {
                IDataSource comp = (IDataSource)obj;
                if (comp != null)
                {
                    if (comp.FactoryKey == this.FactoryKey && comp.SessionKey == this.SessionKey)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        private IDbContext _dbContext = SafeServiceLocator<IDbContext>.GetService();
        public void Dispose()
        {
            //Trace.TraceInformation("About to dispose datasource: {0}", StorageKey);
            _dbContext.Close(this);
            //Trace.TraceInformation("Disposed datasource: {0}", StorageKey);
        }
    }


    public enum DataCategory
    {
        Core,
        Shared,
        Tenant,
        Auxilliary
    }
}
