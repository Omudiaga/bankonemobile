﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IDBNFundsTransferTransactionTypeRepository : IRepository<IDBNFundsTransferTransactionType>
    {
        IDBNFundsTransferTransactionType GetBySessionID(IDataSource dataSource, string sessionID);
        IDBNFundsTransferTransactionType GetByNameEnquirySessionID(IDataSource dataSource, string nameEnquirySessionID);
        IList<IDBNFundsTransferTransactionType> GetByDestinationInstitutionCode(IDataSource dataSource, string destinationInstitutionCode);
        IList<IDBNFundsTransferTransactionType> GetByOriginatorAccountNumber(IDataSource dataSource, string originatorAccountNumber);
        IList<IDBNFundsTransferTransactionType> GetByBeneficiaryAccountNumber(IDataSource dataSource, string beneficiaryAccountNumber);
        IList<IDBNFundsTransferTransactionType> GetByResponseCode(IDataSource dataSource, string responseCode);
        IList<IDBNFundsTransferTransactionType> GetPendingTransfersToDiamondBank(IDataSource dataSource, string diamondBankInstitutionCode);
        IList<IDBNFundsTransferTransactionType> GetPendingTransfersToOtherBanks(IDataSource dataSource);
        IList<IDBNFundsTransferTransactionType> GetFailedReversals(IDataSource dataSource, string diamondBankInstitutionCode);
    }
}
