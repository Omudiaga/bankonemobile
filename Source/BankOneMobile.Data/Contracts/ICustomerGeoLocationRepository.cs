﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface ICustomerGeoLocationRepository : IRepository<ICustomerGeoLocation>
    {
        IList<ICustomerGeoLocation> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        ICustomerGeoLocation GetByCustomerPhoneNumber(IDataSource dataSource, string institutionCode, string customerPhoneNumber);
        IList<ICustomerGeoLocation> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IList<ICustomerGeoLocation> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
