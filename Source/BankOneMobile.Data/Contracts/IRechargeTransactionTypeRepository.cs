﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface IRechargeTransactionTypeRepository : IRepository<IRechargeTransactionType>
    {
        List<BankOneMobile.Core.Implementations.RechargeTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);
    }
}
