﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface IAgentOptimizationRepository : IGenericAgentOptimizationRepository<AgentOptimization>
    {
    }

    public interface IGenericAgentOptimizationRepository<T> : IRepository<T> where T : GenericAgentOptimization
    {
        IList<T> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        IList<T> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IList<T> GetByDateGenerated(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
