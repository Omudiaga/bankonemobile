﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IFeeTransactionTypeRepository : IRepository<IFeeTransactionType> 
    {
         IFeeTransactionType GetByName(IDataSource dataSource, string name);
       
         IFeeTransactionType GetByID(IDataSource dataSource, long id);
        
         IList<IFeeTransactionType> GetActiveFeeTransactionTypes(IDataSource dataSource);
        
    }
}
