﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface IInstitutionRepository : IRepository<IInstitution>
    {

        IInstitution GetByCode(IDataSource dataSource, string code);
        IInstitution GetByName(IDataSource dataSource, string code);
        IInstitution GetByShortName(IDataSource dataSource, string shortName);

        IList<IInstitution> GetActiveInstitutions(IDataSource dataSource);
        IList<IInstitution> GetAllInstitutions(IDataSource dataSource);
        //IAppZoneIncomeAccount GetAppZoneIncomeAccount(IDataSource dataSource);


        List<IInstitution> FindWithPaging(IDataSource dataSource, string name, string code, int startIndex, int maxSize, out int total);
    }
}
