﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IUssdBillingConfigurationRepository : IRepository<IUssdBillingConfiguration>
    {
        List<IUssdBillingConfiguration> FindUssdBillingConfigurationsWithPaging(IDataSource _theDataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);
    }
}
