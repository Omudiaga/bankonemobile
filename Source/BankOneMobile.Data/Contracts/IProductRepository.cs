﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface IProductRepository : IRepository<IProduct>
    {

        IProduct GetByCode(IDataSource dataSource, string code);
        IProduct GetByName(IDataSource dataSource, string code);
     
       IList<IProduct> GetActiveProducts(IDataSource dataSource);
       IList<IProduct> GetActiveProductsByInstitutionCode(IDataSource dataSource,string institutionCode);
       
    }
}
