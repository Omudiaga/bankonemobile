﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface IMerchantRepository : IRepository<IMerchant>
    {

        IMerchant GetByCode(IDataSource dataSource, string code);
        IMerchant GetByName(IDataSource dataSource, string code);
     
       IList<IMerchant> GetActiveMerchants(IDataSource dataSource);
       IList<IMerchant> GetActiveMerchantsByInstitutionCode(IDataSource dataSource,string institutionCode);
       
    }
}
