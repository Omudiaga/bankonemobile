﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IOneCreditLoanRequestRepository : IRepository<IOneCreditLoanRequest>
    {
        IList<IOneCreditLoanRequest> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        IList<IOneCreditLoanRequest> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IList<IOneCreditLoanRequest> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
