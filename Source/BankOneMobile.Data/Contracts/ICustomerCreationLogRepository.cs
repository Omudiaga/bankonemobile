﻿using BankOneMobile.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.Contracts
{
    public interface ICustomerCreationLogRepository:IRepository<ICustomerCreationLog>
    {
        ICustomerCreationLog GetByAccountNumber(IDataSource _theDataSource, string accountNumber);
    }
}
