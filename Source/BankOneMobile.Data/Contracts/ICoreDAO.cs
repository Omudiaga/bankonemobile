﻿using BankOneMobile.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Data.Contracts
{
    public interface ICoreDAO<T, idT> : IRepositoryWithTypedId<T, idT> where T : class, IEntity<idT>
    {
        string InstitutionCode { get; set; }
        void SqlBulkInsert(IList<T> items, string tableName, List<string> columnsToExclude = null, bool closeConnection = false);
    }
    public interface ICoreDAO<T> : ICoreDAO<T, long> where T : class, IEntity
    {

    }
}
