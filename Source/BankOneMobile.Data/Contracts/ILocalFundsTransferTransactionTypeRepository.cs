﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface ILocalFundsTransferTransactionTypeRepository : IRepository<ILocalFundsTransferTransactionType>
    {
         List<BankOneMobile.Core.Implementations.LocalFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, TransactionStatus? status, string InstitutionID, string phoneNumber, string reciever, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);
    }
}
