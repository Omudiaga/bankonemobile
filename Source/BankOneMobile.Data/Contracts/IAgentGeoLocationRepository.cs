﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IAgentGeoLocationRepository : IRepository<IAgentGeoLocation>
    {
        IList<IAgentGeoLocation> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        IList<IAgentGeoLocation> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IList<IAgentGeoLocation> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
