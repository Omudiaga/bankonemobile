﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IOfflineDepositTransactionTypeRepository : IRepository<IOfflineDepositTransactionType>
    {
        IList<IOfflineDepositTransactionType> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        IList<IOfflineDepositTransactionType> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IOfflineDepositTransactionType GetUnsuccessfulOfflineDepositBySessionID(IDataSource dataSource, string sessionID);
        IList<IOfflineDepositTransactionType> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
