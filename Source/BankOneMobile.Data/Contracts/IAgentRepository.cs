﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
  public  interface IAgentRepository : IRepository<IAgent>
    {

      IAgent GetByCode(IDataSource dataSource, string code);
      IMobileAccount GetMobileAccountByPhoneNumber(IDataSource dataSource, string phoneNumber);
       IList<IAgent> GetUnlinked(IDataSource dataSource);
       IAgent GetAgentByPhoneNumber(IDataSource dataSource, string code);
       List<IAgent> FindAgentsWithPaging(IDataSource dataSource, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);

       List<Agent> GetAgentByInstitutionCode(IDataSource _theDataSource, string isntCode, bool isMobileTeller);
    }
}
