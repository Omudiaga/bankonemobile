﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using System.Collections;

namespace BankOneMobile.Data.Contracts
{
    public interface ILinkingBankAccountRepository : IRepository<ILinkingBankAccount>
    {
         List<ILinkingBankAccount> Find(IDataSource dataSource,string acctNo,string status,string isAgentAcct, string phoneNumber,string productCode,string gender, string InstitutionID, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);
         ArrayList GetProducts(IDataSource dataSource, string instCode);
         List<ILinkingBankAccount> FindActive(IDataSource dataSource, string instCode, string acctNo);
    }
}
