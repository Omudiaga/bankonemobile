﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.Contracts
{
    public interface IThirdPartyMobileProfileRepository : IRepository<IThirdPartyMobileProfile>
    {

        IThirdPartyMobileProfile GetByAccountNumber(IDataSource dataSource, string AccountNo);
        IThirdPartyMobileProfile GetByAccountNumberAndInstitutionCode(IDataSource dataSource, string AccountNo, string InstitutionCode);
        IThirdPartyMobileProfile GetByPhoneNumber(IDataSource dataSource, string PhoneNo);
        IList<IThirdPartyMobileProfile> GetByLastName(IDataSource dataSource, string LastName);
        IList<IThirdPartyMobileProfile> GetByOtherNames(IDataSource dataSource, string OtherName);
        IList<IThirdPartyMobileProfile> GetByInstitutionCode(IDataSource dataSource, string InstitutionCode);
        IList<IThirdPartyMobileProfile> GetMobileProfilesByStatus(IDataSource dataSource, string status);
        IList<IThirdPartyMobileProfile> GetAllMobileProfiles(IDataSource dataSource);

        IList<ThirdPartyMobileProfile> GetAllTheMobileProfiles(IDataSource dataSource);

        List<IThirdPartyMobileProfile> FindWithPaging(IDataSource dataSource, string accountNumber, string phoneNumber, string lastname, string status, string InstitutionCode, int startIndex, int maxSize, out int total);
    }
}
