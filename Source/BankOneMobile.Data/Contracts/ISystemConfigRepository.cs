﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface ISystemConfigRepository : IRepository<ISystemConfig>
    {
        ISystemConfig GetConfig(IDataSource dataSource);
        
    }
}
