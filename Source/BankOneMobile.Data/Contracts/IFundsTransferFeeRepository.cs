﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface IFundsTransferFeeRepository : IRepository<IFundsTransferFee> 
    {
        IFundsTransferFee GetByName(IDataSource dataSource, string institutionCode, TransactionTypeName name);

        IFundsTransferFee GetByID(IDataSource dataSource, long id);

        IList<IFundsTransferFee> GetActiveFees(IDataSource dataSource, string institutionCode);
        
    }
}
