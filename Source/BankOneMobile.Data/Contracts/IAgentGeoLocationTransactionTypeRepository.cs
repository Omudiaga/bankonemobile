﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface IAgentGeoLocationTransactionTypeRepository : IRepository<IAgentGeoLocationTransactionType>
    {
        IList<IAgentGeoLocationTransactionType> GetByAgentPhoneNumber(IDataSource dataSource, string institutionCode, string agentPhoneNumber);
        IList<IAgentGeoLocationTransactionType> GetByInstitutionCode(IDataSource dataSource, string institutionCode);
        IList<IAgentGeoLocationTransactionType> GetByDateLogged(IDataSource dataSource, string institutionCode, DateTime startDate, DateTime endDate);
    }
}
