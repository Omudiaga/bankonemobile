﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
   public interface ILinkingCardRepository : IRepository<ILinkingCard>
    {
    }
}
