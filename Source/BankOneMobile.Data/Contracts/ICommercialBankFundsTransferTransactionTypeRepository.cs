﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface ICommercialBankFundsTransferTransactionTypeRepository : IRepository<ICommercialBankFundsTransferTransactionType>
    {
        List<BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);
        List<BankOneMobile.Core.Contracts.ICommercialBankFundsTransferTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, string transRef,string bankName, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);
        BankOneMobile.Core.Implementations.InterbankTransaction GetTransactionById(IDataSource dataSource, long ID);
    }
}
