﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface ICustomerRegistrationRepository : IRepository<ICustomerRegistration>
    {
        List<ICustomerRegistration> FindTransactionsWithPaging(IDataSource _theDataSource, string institutionCode, string phoneNum, IDictionary<string, object> dic, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total);
        ICustomerRegistration GetCustomerRegistrationByPhoneNumber(IDataSource dataSource, string institutionCode, string phoneNumber);
        ICustomerRegistration GetCustomerRegistrationByCardSerialNumber(IDataSource dataSource, string serialNumber);
        List<ICustomerRegistration> GetPendingSMSesByRawSql(IDataSource dataSource);
    }
}
