﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface ICashInTransactionTypeRepository : IRepository<ICashInTransactionType>
    {
        List<BankOneMobile.Core.Implementations.CashInTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber,string custPhoneNo, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount);

    }
}
