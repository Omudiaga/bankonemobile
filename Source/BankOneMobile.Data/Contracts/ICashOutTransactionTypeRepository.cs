﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Data.Contracts
{
    public interface ICashOutTransactionTypeRepository : IRepository<ICashOutTransactionType>
    {
        List<BankOneMobile.Core.Implementations.CashOutTransactionType> FindTransactionsWithPaging(IDataSource dataSource, string InstitutionID, TransactionStatus? status, string phoneNumber, IDictionary<string, object> propertyValuePairs, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total, out decimal amount, out int totalSuccess);
    }
}
