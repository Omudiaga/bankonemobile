﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.Contracts
{
    public interface ITargetSavingsRegistrationTransactionTypeRepository : IRepository<ITargetSavingsRegistrationTransactionType>
    {
        List<ITargetSavingsRegistrationTransactionType> FindTransactionsWithPaging(IDataSource _theDataSource, string institutionCode, string phoneNum, IDictionary<string, object> dic, DateTime? dateFrom, DateTime? dateTo, int startIndex, int maxSize, out int total);
    }
}
