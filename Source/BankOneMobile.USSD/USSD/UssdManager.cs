﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.InfoBip.USSD
{
    public class UssdManager
    {
        string _applicationId = InfoBipConfiguration.UssdApplicationId;
        string _userName = InfoBipConfiguration.UssdUsername;
        string _password = InfoBipConfiguration.UssdPassword;

        InfobipUssdGateway.UssdGwAccessWsClient asyncClient;

        public void StartSession(string phoneNo, string initialMessage)
        {
            using (var client = new InfobipUssdGateway.UssdGwAccessWsClient())
            {
                client.startSession(_applicationId, new InfobipUssdGateway.ussdGwAccessParams()
                {
                    username = _userName,
                    authKey = _password,
                    destinationAddress = phoneNo,
                    ussdCode= "123",
                    text = initialMessage,
                });
            }
        }

        public void StartSessionAsync(string phoneNo, string initialMessage)
        {
            asyncClient = new InfobipUssdGateway.UssdGwAccessWsClient();
            asyncClient.BeginstartSession(_applicationId, new InfobipUssdGateway.ussdGwAccessParams()
                {
                    username = _userName,
                    authKey = _password,
                    destinationAddress = phoneNo,
                    ussdCode = "123",
                    text = initialMessage,
                }, SessionCallback, null);
        }

        public void SessionCallback(IAsyncResult ar)
        {
            try
            {
                if (ar.IsCompleted)
                {
                    asyncClient.EndstartSession(ar);
                }
            }
            catch {  }
            finally
            {
                asyncClient.Close();
            }
        }


    }
}
