﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace BankOneMobile.InfoBip.SMS
{
    public class SmsClient
    {
         string _urlSms = InfoBipConfiguration.SmsUrl; // QueryString path
         string _user = InfoBipConfiguration.SmsUsername;    // Username, e.g.: "xxxx"
         string _password = InfoBipConfiguration.SmsPassword; // Password, e.g.: "xxxxx" 

        //public string SendSMS(string sender, string destinationPhoneNo, string message)
        //{
        //    string Msender = sender;    // Sender, e.g.: "Friend" 

        //    string smstext = message;   // Message text, e.g.: "Message"

        //    string isFlash = "0"; // Message type

        //    //if (CB_Flash.Checked.ToString().Equals("True"))
        //    //    isFlash = "1";

        //    string gsm = destinationPhoneNo; // Destination number, e.g.: "38598514674"

        //    Uri uri = new Uri(_urlSms);

        //    string data = "user=" + _user + "&password=" + _password + "&sender=" + Msender + "&SMSText=" + smstext + "&IsFlash=" + isFlash + "&GSM=" + gsm + "";

        //    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);

        //    request.Method = WebRequestMethods.Http.Post;

        //    request.ContentLength = data.Length;

        //    request.ContentType = "application/x-www-form-urlencoded";

        //    StreamWriter writer = new StreamWriter(request.GetRequestStream());

        //    writer.Write(data);

        //    writer.Close();

        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

        //    StreamReader reader = new StreamReader(response.GetResponseStream());

        //    string tmp = reader.ReadToEnd();

        //    response.Close();

        //    return tmp;

        //}

         public string SendSMS(string sender, string destinationPhoneNo, string message)
         {
             string result = string.Empty;
             try
             {
                 using (var client = new BankOneMessagingService.MessagingServiceClient())
                 {
                     var sms = new BankOneMessagingService.SMS();
                     sms.Body = message;
                     //sms.DateCreated = DateTime.Now;
                     sms.To = destinationPhoneNo;
                     if (destinationPhoneNo.StartsWith("+234"))
                     {
                         sms.To = destinationPhoneNo.Replace("+234", "");
                     }
                     //sms.InstitutionName = sender;
                     sms.ReferenceNo = "";// ":MTO:";
                     sms.AccountNo = "test";
                     result = client.SendSMSByCodeSingle("999999", sms) ? "Success" : "Failed";
                 }
             }
             catch(Exception ex)
             {
                 result = ex.Message;
             }
             return result;
         }

    }
}
