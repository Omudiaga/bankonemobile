﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BillerCategory", Namespace="http://schemas.datacontract.org/2004/07/Integration.Core.Data")]
    [System.SerializableAttribute()]
    public partial class BillerCategory : BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerCategoryIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.Biller[] BillsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerCategoryID {
            get {
                return this.BillerCategoryIDField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerCategoryIDField, value) != true)) {
                    this.BillerCategoryIDField = value;
                    this.RaisePropertyChanged("BillerCategoryID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.Biller[] Bills {
            get {
                return this.BillsField;
            }
            set {
                if ((object.ReferenceEquals(this.BillsField, value) != true)) {
                    this.BillsField = value;
                    this.RaisePropertyChanged("Bills");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataObject", Namespace="http://schemas.datacontract.org/2004/07/Integration.Core.Base")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.Biller))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.PaymentItem))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.BillerCategory))]
    public partial class DataObject : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsActiveField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsActive {
            get {
                return this.IsActiveField;
            }
            set {
                if ((this.IsActiveField.Equals(value) != true)) {
                    this.IsActiveField = value;
                    this.RaisePropertyChanged("IsActive");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Biller", Namespace="http://schemas.datacontract.org/2004/07/Integration.Core.Data")]
    [System.SerializableAttribute()]
    public partial class Biller : BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerCategoryIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CurrencyCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CurrencySymbolField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CustomSectionUrlField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CustomerField1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CustomerField2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LogoUrlField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NarrationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string QuickTellerSiteUrlNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ShortNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SupportEmailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SurchargeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UrlField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerCategoryID {
            get {
                return this.BillerCategoryIDField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerCategoryIDField, value) != true)) {
                    this.BillerCategoryIDField = value;
                    this.RaisePropertyChanged("BillerCategoryID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerId {
            get {
                return this.BillerIdField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerIdField, value) != true)) {
                    this.BillerIdField = value;
                    this.RaisePropertyChanged("BillerId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CurrencyCode {
            get {
                return this.CurrencyCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.CurrencyCodeField, value) != true)) {
                    this.CurrencyCodeField = value;
                    this.RaisePropertyChanged("CurrencyCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CurrencySymbol {
            get {
                return this.CurrencySymbolField;
            }
            set {
                if ((object.ReferenceEquals(this.CurrencySymbolField, value) != true)) {
                    this.CurrencySymbolField = value;
                    this.RaisePropertyChanged("CurrencySymbol");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CustomSectionUrl {
            get {
                return this.CustomSectionUrlField;
            }
            set {
                if ((object.ReferenceEquals(this.CustomSectionUrlField, value) != true)) {
                    this.CustomSectionUrlField = value;
                    this.RaisePropertyChanged("CustomSectionUrl");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CustomerField1 {
            get {
                return this.CustomerField1Field;
            }
            set {
                if ((object.ReferenceEquals(this.CustomerField1Field, value) != true)) {
                    this.CustomerField1Field = value;
                    this.RaisePropertyChanged("CustomerField1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CustomerField2 {
            get {
                return this.CustomerField2Field;
            }
            set {
                if ((object.ReferenceEquals(this.CustomerField2Field, value) != true)) {
                    this.CustomerField2Field = value;
                    this.RaisePropertyChanged("CustomerField2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LogoUrl {
            get {
                return this.LogoUrlField;
            }
            set {
                if ((object.ReferenceEquals(this.LogoUrlField, value) != true)) {
                    this.LogoUrlField = value;
                    this.RaisePropertyChanged("LogoUrl");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Narration {
            get {
                return this.NarrationField;
            }
            set {
                if ((object.ReferenceEquals(this.NarrationField, value) != true)) {
                    this.NarrationField = value;
                    this.RaisePropertyChanged("Narration");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string QuickTellerSiteUrlName {
            get {
                return this.QuickTellerSiteUrlNameField;
            }
            set {
                if ((object.ReferenceEquals(this.QuickTellerSiteUrlNameField, value) != true)) {
                    this.QuickTellerSiteUrlNameField = value;
                    this.RaisePropertyChanged("QuickTellerSiteUrlName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ShortName {
            get {
                return this.ShortNameField;
            }
            set {
                if ((object.ReferenceEquals(this.ShortNameField, value) != true)) {
                    this.ShortNameField = value;
                    this.RaisePropertyChanged("ShortName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SupportEmail {
            get {
                return this.SupportEmailField;
            }
            set {
                if ((object.ReferenceEquals(this.SupportEmailField, value) != true)) {
                    this.SupportEmailField = value;
                    this.RaisePropertyChanged("SupportEmail");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Surcharge {
            get {
                return this.SurchargeField;
            }
            set {
                if ((object.ReferenceEquals(this.SurchargeField, value) != true)) {
                    this.SurchargeField = value;
                    this.RaisePropertyChanged("Surcharge");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Url {
            get {
                return this.UrlField;
            }
            set {
                if ((object.ReferenceEquals(this.UrlField, value) != true)) {
                    this.UrlField = value;
                    this.RaisePropertyChanged("Url");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PaymentItem", Namespace="http://schemas.datacontract.org/2004/07/Integration.Core.Data")]
    [System.SerializableAttribute()]
    public partial class PaymentItem : BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerCategoryIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BillerTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CustomerIDNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PaymentItemIDField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Amount {
            get {
                return this.AmountField;
            }
            set {
                if ((object.ReferenceEquals(this.AmountField, value) != true)) {
                    this.AmountField = value;
                    this.RaisePropertyChanged("Amount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerCategoryId {
            get {
                return this.BillerCategoryIdField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerCategoryIdField, value) != true)) {
                    this.BillerCategoryIdField = value;
                    this.RaisePropertyChanged("BillerCategoryId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerId {
            get {
                return this.BillerIdField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerIdField, value) != true)) {
                    this.BillerIdField = value;
                    this.RaisePropertyChanged("BillerId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BillerType {
            get {
                return this.BillerTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.BillerTypeField, value) != true)) {
                    this.BillerTypeField = value;
                    this.RaisePropertyChanged("BillerType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Code {
            get {
                return this.CodeField;
            }
            set {
                if ((object.ReferenceEquals(this.CodeField, value) != true)) {
                    this.CodeField = value;
                    this.RaisePropertyChanged("Code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CustomerIDName {
            get {
                return this.CustomerIDNameField;
            }
            set {
                if ((object.ReferenceEquals(this.CustomerIDNameField, value) != true)) {
                    this.CustomerIDNameField = value;
                    this.RaisePropertyChanged("CustomerIDName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PaymentItemID {
            get {
                return this.PaymentItemIDField;
            }
            set {
                if ((object.ReferenceEquals(this.PaymentItemIDField, value) != true)) {
                    this.PaymentItemIDField = value;
                    this.RaisePropertyChanged("PaymentItemID");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="BankOneGatewayServiceRef.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetBillerCategories", ReplyAction="http://tempuri.org/IService/GetBillerCategoriesResponse")]
        BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.BillerCategory[] GetBillerCategories();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetBillers", ReplyAction="http://tempuri.org/IService/GetBillersResponse")]
        BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.Biller[] GetBillers(string billerCategoryID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetBillPaymentItems", ReplyAction="http://tempuri.org/IService/GetBillPaymentItemsResponse")]
        BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.PaymentItem[] GetBillPaymentItems(string billerID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/ConfirmPaymentMessage", ReplyAction="http://tempuri.org/IService/ConfirmPaymentMessageResponse")]
        string ConfirmPaymentMessage(string requestReference);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/QueryTransaction", ReplyAction="http://tempuri.org/IService/QueryTransactionResponse")]
        string QueryTransaction(string xmlParams);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/QueryTransactionWithRawReference", ReplyAction="http://tempuri.org/IService/QueryTransactionWithRawReferenceResponse")]
        bool QueryTransactionWithRawReference(out string responseDescOrPaymentRef, string instCode, string rawRequestReference);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/QueryAllTransactionTypesWithRawReference", ReplyAction="http://tempuri.org/IService/QueryAllTransactionTypesWithRawReferenceResponse")]
        bool QueryAllTransactionTypesWithRawReference(out string responseDescOrPaymentRef, string instCode, string rawRequestReference, bool isTransfer);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.IService>, BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.BillerCategory[] GetBillerCategories() {
            return base.Channel.GetBillerCategories();
        }
        
        public BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.Biller[] GetBillers(string billerCategoryID) {
            return base.Channel.GetBillers(billerCategoryID);
        }
        
        public BankOneMobile.SwitchIntegration.BankOneGatewayServiceRef.PaymentItem[] GetBillPaymentItems(string billerID) {
            return base.Channel.GetBillPaymentItems(billerID);
        }
        
        public string ConfirmPaymentMessage(string requestReference) {
            return base.Channel.ConfirmPaymentMessage(requestReference);
        }
        
        public string QueryTransaction(string xmlParams) {
            return base.Channel.QueryTransaction(xmlParams);
        }
        
        public bool QueryTransactionWithRawReference(out string responseDescOrPaymentRef, string instCode, string rawRequestReference) {
            return base.Channel.QueryTransactionWithRawReference(out responseDescOrPaymentRef, instCode, rawRequestReference);
        }
        
        public bool QueryAllTransactionTypesWithRawReference(out string responseDescOrPaymentRef, string instCode, string rawRequestReference, bool isTransfer) {
            return base.Channel.QueryAllTransactionTypesWithRawReference(out responseDescOrPaymentRef, instCode, rawRequestReference, isTransfer);
        }
    }
}
