﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.Client.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Messages;
using BankOneMobile.Core.Contracts;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using Trx.Messaging.FlowControl;
using AppZoneSwitch.Services;
using AppZoneSwitch.Core.Contracts;
using AppZoneSwitch.Services.Contracts;
using AppZoneSwitch.Services.Implementations;
using System.Threading;
using System.Data.SqlTypes;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.Core.Exceptions;
using System.Diagnostics;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
using System.Data;
using BankOneMobile.Data.Implementations;





namespace BankOneMobile.SwitchIntegration
{
   public  class ProcessISOTransaction
    {
       System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
       string terminalOwner = "4BANKMOB";
       public const int  NORMAL_TIME_WAIT = 30;
       public const int LONG_TIME_WAIT = 60;
       
       public  Payment CreatePayment(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           Account fromAccount = new Account(trans.From, AccountType.Default);
           Account toAccount = new Account(trans.To, AccountType.Default);
           Amount amount = new Amount(Convert.ToInt64(trans.Amount),"566",AmountType.Cash);
           
           
           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber,trans.EncryptedPIN);// trans.EncryptedptedPin  o
          

           Payment toReturn = new Payment(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, amount, mob, trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), trans.NotValidatePin, trans.Narration,trans.ServiceCodes,null,trans.TheMerchantType,trans.TheService,trans.DisplayMessage);
           return toReturn;
       }
       //public Purchase CreatePurchase(BankOneMobile.Core.Contracts.ITransaction trans,string xmlData)
       //{
       //    Account fromAccount = new Account(trans.From, AccountType.Default);
       //    Account toAccount = new Account(trans.To, AccountType.Default);
       //    Amount amount = new Amount(Convert.ToInt64(trans.Amount), "566", AmountType.Cash);


       //    MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin

       //    Purchase toReturn = new Purchase(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, amount, mob, trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), false,xmlData,false,trans.Narration,trans.TheFee,trans.TheMerchantType,trans.TheService);
       //    return toReturn;
       //}
       public Purchase CreatePurchase(BankOneMobile.Core.Contracts.ITransaction trans, string xmlData,bool isBillsPayment)
       {
           Account fromAccount = new Account(trans.From, AccountType.Default);
           Account toAccount = new Account(trans.To, AccountType.Default);
           Amount amount = new Amount(Convert.ToInt64(trans.Amount), "566", AmountType.Cash);


           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin

           Purchase toReturn = new Purchase(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, amount, mob, trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), false, xmlData, isBillsPayment, trans.Narration,trans.TheFee.Amount*100,trans.TheMerchantType,trans.TheService);
           return toReturn;
       }
       public FundsTransfer CreateFundsTransfer(BankOneMobile.Core.Contracts.ITransaction trans, string xmlData,ISOServiceCodes isoServ)
       {
           Account fromAccount = new Account(trans.From, AccountType.Default);
           Account toAccount = new Account(trans.To, AccountType.Default);
           Amount amount = new Amount(Convert.ToInt64(trans.Amount), "566", AmountType.Cash);



           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin

           FundsTransfer toReturn = new FundsTransfer(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, amount, mob, trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), false, xmlData,trans.Narration,trans.TheFee.Amount*100,isoServ,trans.TheMerchantType);
           return toReturn;
       }
           
       public ServicePayment CreateServicePayment(BankOneMobile.Core.Contracts.ITransaction trans,ServiceCodes code,ISOServiceCodes isoServ)
       {
           Account fromAccount = new Account(trans.From, AccountType.Default);
           Account toAccount = new Account(trans.To, AccountType.Default);
           Amount amount = new Amount(Convert.ToInt64(trans.Amount), "566", AmountType.Cash);


           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin

           ServicePayment toReturn = new ServicePayment(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, code, amount, mob, trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), false, trans.Narration,trans.TheFee.Amount*100,isoServ,trans.TheMerchantType,trans.TheService);
           return toReturn;
       }

       public  BalanceEnquiry CreateBalanceEnquiry(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           
           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin
           if (trans.TheService == Service.MobileTellerCustomerBalanceEnquiryMS || trans.TheService == Service.SelfServiceBalanceEnquiry)
           {
               mob.ServiceCode = Convert.ToString((int)trans.TheService);
           }
        
           Account fromAccount = new Account(trans.From,AccountType.Default);

           return new BalanceEnquiry(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, mob, trans.FromInstitutionCode, trans.ID.ToString(), false, trans.Narration);
       }
       public AccountEnquiry CreateAccountEnquiry(BankOneMobile.Core.Contracts.ITransaction trans)
       {

           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);// trans.EncryptedptedPin
           Account fromAccount = new Account(trans.From, AccountType.Default);
           if (trans.TheService == Service.MobileTellerCustomerBalanceEnquiryMS)
           {
               mob.ServiceCode = Convert.ToString((int)trans.TheService);
           }
           return new AccountEnquiry(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, mob, trans.FromInstitutionCode, trans.ID.ToString(), false, trans.Narration);
       }
       public PinlessDebit CreatePinlessDebit(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);
           Account fromAccount = new Account(trans.From, AccountType.Default);
           Account toAccount = new Account(trans.To, AccountType.Default);
           Amount amount = new Amount(Convert.ToInt64(trans.Amount), "566", AmountType.Cash);

           return new PinlessDebit(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, toAccount, amount, mob, 
               trans.FromInstitutionCode, trans.ToInstitutionCode, trans.ID.ToString(), false, trans.Narration, null, trans.TheService);
        }
      public ChangePIN CreateChangePIN(BankOneMobile.Core.Contracts.ITransaction trans, IPinChangeTransactionType pinChange)
       {

           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);//.EncryptedptedPin
           Account fromAccount = new Account(trans.From, AccountType.Default);
           
//         return new ChangePIN(terminalOwner, terminalOwner), fromAccount, mob, trans.FromInstitutionCode, trans.ID.ToString(), false);
           ChangePIN toReturn = new ChangePIN(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, mob, pinChange.EncryptedPin, trans.ID.ToString(), false,trans.FromInstitutionCode);
          return toReturn;
             
       }
      public ConfigurationMessage CreateConfigurationMessage(BankOneMobile.Core.Contracts.ITransaction trans, IConfigurationTransactionType config)
      {

          MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);//.EncryptedptedPin
          Account fromAccount = new Account(trans.From, AccountType.Default);

          //         return new ChangePIN(terminalOwner, terminalOwner), fromAccount, mob, trans.FromInstitutionCode, trans.ID.ToString(), false);
          ConfigurationMessage toReturn = new ConfigurationMessage(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, mob, trans.ID.ToString(), false,trans.FromInstitutionCode);
          return toReturn;

      }
      public Byte[] GeneratePinBlock(string encryptedPin)
      {
          return Encoding.Unicode.GetBytes(encryptedPin);
      }
      
       public  BankOneMobile.SwitchIntegration.ISO8583.Client.Messages.MiniStatement CreateMiniStatement(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           MobileAccountDetails mob = new MobileAccountDetails(trans.FromPhoneNumber, trans.EncryptedPIN);
           Account fromAccount = new Account(trans.From, AccountType.Default);
           if (trans.TheService == Service.MobileTellerCustomerBalanceEnquiryMS)
           {
               mob.ServiceCode = Convert.ToString((int)trans.TheService);
           }
           return new MiniStatement(new TerminalOwner(terminalOwner, terminalOwner), fromAccount, mob, trans.FromInstitutionCode, trans.ID.ToString(), false, trans.Narration);
       }
       public  BalanceEnquiryResponse DoBalanceEnquiry(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           BalanceEnquiryResponse toReturn = null;
           
           BalanceEnquiry tr = CreateBalanceEnquiry(trans);
           //TODO  do the real thing here to send the transaction

             //Trx.Messaging.Iso8583.Iso8583Message isoResponse  = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30) ;
             //toReturn = new BalanceEnquiryResponse(isoResponse);
           if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
           {
               tr.Fields.Remove(94);
               tr.Fields.Add(94, "1091");
           }
          Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if(RoutingSystem.NodePeers==null||RoutingSystem.NodePeers.Count==0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
          if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
          {

              throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
          }

           //toReturn = new BalanceEnquiryResponse(new Trx.Messaging.Message());
           //toReturn.ResponseCode = "00";
          //using (ISOInterfaceService.ISOConnectionServiceClient client = new ISOInterfaceService.ISOConnectionServiceClient())
          //{
          //    isoResponse = client.SendActualISO(tr);
          //}
          //using (ISOConnection.ISOInterfaceWebServiceClient client = new ISOConnection.ISOInterfaceWebServiceClient())
          //{
          //    isoResponse = client.SendActualISO(tr);
          //}
          IAction payload = new ProcessorAction();
          isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);         
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
          if (isoResponse == null)
          {
              throw new NoIssuerResponseException("No Response From Core Banking");
          }
          toReturn = new BalanceEnquiryResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;


           
       }
       public AccountEnquiryResponse DoAccountEnquiry(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           AccountEnquiryResponse toReturn = null;

           AccountEnquiry tr = CreateAccountEnquiry(trans);
           //TODO  do the real thing here to send the transaction

           //Trx.Messaging.Iso8583.Iso8583Message isoResponse  = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30) ;
           //toReturn = new BalanceEnquiryResponse(isoResponse);
           if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
           {
               tr.Fields.Remove(94);
               tr.Fields.Add(94, "1089");
           }
           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }

           //toReturn = new BalanceEnquiryResponse(new Trx.Messaging.Message());
           //toReturn.ResponseCode = "00";
           //using (ISOInterfaceService.ISOConnectionServiceClient client = new ISOInterfaceService.ISOConnectionServiceClient())
           //{
           //    isoResponse = client.SendActualISO(tr);
           //}
           //using (ISOConnection.ISOInterfaceWebServiceClient client = new ISOConnection.ISOInterfaceWebServiceClient())
           //{
           //    isoResponse = client.SendActualISO(tr);
           //}
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);  
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse == null)
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           toReturn = new AccountEnquiryResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;
       }
       public PinlessDebitResponse DoPinlessDebit(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           PinlessDebitResponse toReturn = null;

           PinlessDebit tr = CreatePinlessDebit(trans);
           if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
           {
               tr.Fields.Remove(94);
               tr.Fields.Add(94, "1091");
           }
           //TODO  do the real thing here to send the transaction

           //Trx.Messaging.Iso8583.Iso8583Message isoResponse  = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30) ;
           //toReturn = new BalanceEnquiryResponse(isoResponse);

           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }

           //toReturn = new BalanceEnquiryResponse(new Trx.Messaging.Message());
           //toReturn.ResponseCode = "00";
           //using (ISOInterfaceService.ISOConnectionServiceClient client = new ISOInterfaceService.ISOConnectionServiceClient())
           //{
           //    isoResponse = client.SendActualISO(tr);
           //}
           //using (ISOConnection.ISOInterfaceWebServiceClient client = new ISOConnection.ISOInterfaceWebServiceClient())
           //{
           //    isoResponse = client.SendActualISO(tr);
           //}
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse == null)
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           toReturn = new PinlessDebitResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;
       }
       public ChangePINResponse DoChangePin(BankOneMobile.Core.Contracts.ITransaction trans,IPinChangeTransactionType transacType)
       {
           ChangePINResponse toReturn = null;

           ChangePIN tr = CreateChangePIN(trans,transacType);
           if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
           {
               tr.Fields.Remove(94);
               tr.Fields.Add(94, "1089");
           }
           //TODO  do the real thing here to send the transaction

             //Trx.Messaging.Iso8583.Iso8583Message isoResponse  = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30) ;
             //toReturn = new BalanceEnquiryResponse(isoResponse);

          Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
          IAction payload = new ProcessorAction();
          isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);         
          if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
          {
              throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
          }
          if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
          {

              throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
          }

           //toReturn = new BalanceEnquiryResponse(new Trx.Messaging.Message());
           //toReturn.ResponseCode = "00";
          //using (ISOInterfaceService.ISOConnectionServiceClient client = new ISOInterfaceService.ISOConnectionServiceClient())
          //{
          //    isoResponse = client.SendActualISO(tr);
          //}
          //using (ISOConnection.ISOInterfaceWebServiceClient client = new ISOConnection.ISOInterfaceWebServiceClient())
          //{
          //    isoResponse = client.SendActualISO(tr);
          //}
        //  isoResponse  = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30);        
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
          if (isoResponse == null)
          {
              throw new NoIssuerResponseException("No Response From Core Banking");
          }
          toReturn = new ChangePINResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;


           
       }
       public ConfigurationResponse DoConfiguration(BankOneMobile.Core.Contracts.ITransaction trans, IConfigurationTransactionType transacType)
        {
            ConfigurationResponse toReturn = null;

            ConfigurationMessage tr = CreateConfigurationMessage (trans, transacType);
            if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
            {
                tr.Fields.Remove(94);
                tr.Fields.Add(94, "1089");
            }

            Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
            if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
            {
                throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
            }
            if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
            {

                throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
            }

            IAction payload = new ProcessorAction();
            isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);        
           // isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30);
            //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
            if (isoResponse == null)
            {
                throw new NoIssuerResponseException("No Response From Core Banking");
            }
            toReturn = new ConfigurationResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
            if (toReturn.ResponseCode == "91")
            {
                throw new NoIssuerResponseException("No Response From Core Banking");
            }
            return toReturn;



        }

       public ConfigurationResponse DoLafayetteReg(BankOneMobile.Core.Contracts.ITransaction trans, IConfigurationTransactionType transactTtype)
       {
           ConfigurationResponse toReturn = null;


           ConfigurationMessage tr = CreateConfigurationMessage(trans, transactTtype);
           tr.Fields.Add(100, "100127");
           tr.Fields.Add(3, "322000");
           tr.Fields.Add(94, "1090");
           tr.Fields.Add(42, "ATM");
           tr.Fields.Add(41, "1AUTOREG");
           tr.Fields.Add(123, "511201203344002");
           tr.Fields.Add(14, string.Format("{0}{1}",trans.Date.Year.ToString().Substring(2),trans.Date.Month.ToString().PadLeft(2,'0')));
           tr.Fields.Remove(127);
           tr.Fields.Remove(49);
           if (tr.Fields.Contains(43))
           {
               tr.Fields.Remove(43);
           }

           tr.Fields.Remove(26);
           tr.Fields.Remove(25);
           tr.Fields.Remove(22);
           tr.Fields.Remove(14);
           tr.Fields.Remove(4);
           tr.Fields.Remove(13);
           tr.Fields.Remove(37);

           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }

           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);
           // isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30);
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse == null)
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           toReturn = new ConfigurationResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;



       }


       public  MiniStatementResponse DoMiniStatement(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           MiniStatementResponse toReturn = null;
           //TODO  do the real thing here
           MiniStatement tr = CreateMiniStatement(trans as BankOneMobile.Core.Contracts.ITransaction);
           if (tr.Fields[100] != null && Convert.ToString(tr.Fields[100].Value) == "100127")
           {
               tr.Fields.Remove(94);
               tr.Fields.Add(94, "1091");
           }

           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);        
          // isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(),60);
           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse == null)
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           toReturn = new MiniStatementResponse(isoResponse,payload.SourceTrans,payload.OriginalTrans);
           if (toReturn.ResponseCode == "91")
           {
               throw new NoIssuerResponseException("No Response From Core Banking");
           }
           return toReturn;
       }
       public  PaymentResponse DoPayment(BankOneMobile.Core.Contracts.ITransaction trans)
       {
           
            PaymentResponse toReturn = null;
            Payment tr = CreatePayment(trans  as BankOneMobile.Core.Contracts.ITransaction);
            
            
           
           
            
           if(tr.Fields[100]!=null&&Convert.ToString(tr.Fields[100].Value) == "100127")
           {
              
               if (tr.Fields[94]!=null&&Convert.ToString(tr.Fields[94].Value) == "1001")
               {
                   

                   tr.Fields.Remove(94);
                   tr.Fields.Add(94, "1091");
                   tr.Fields.Remove(3);
                   tr.Fields.Add(3, "210000");

               }
               else if (tr.Fields[94] != null && Convert.ToString(tr.Fields[94].Value) == "1002")
               {
                  
                   tr.Fields.Remove(94);
                   tr.Fields.Add(94, "1091");
                   tr.Fields.Remove(3);
                   tr.Fields.Add(3, "010000");
                   tr.Fields.Remove(102);
                   tr.Fields.Add(102,trans.From);
                   Trace.TraceInformation("From:{0}, To:{1}, Phone No{2}",trans.From,trans.To, trans.FromPhoneNumber);

               }
              
           }
            
           
            
            Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
            if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
            {
                throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
            }
            if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
            {

                throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
            }
           
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, RoutingSystem.ActiveNodes.First().RequestTimeout);  
            
            //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
            if (isoResponse != null)
            {
                toReturn = new PaymentResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
                    
                if (toReturn.ResponseCode == "91")
                {
                    throw new NoIssuerResponseException("No Response From Core Banking");
                }
            }

            else
            {
                throw new NoIssuerResponseException("No Response From Issuer");
            }
            return toReturn;
           
       }
       //public PurchaseResponse DoPurchase(BankOneMobile.Core.Contracts.ITransaction trans,string xmlData)
       //{
       //    PurchaseResponse toReturn = null;
       //    Purchase tr = CreatePurchase(trans as BankOneMobile.Core.Contracts.ITransaction,xmlData);
       //    Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
       //    if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
       //    {
       //        throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
       //    }
       //    if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
       //    {

       //        throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
       //    }
       //    IAction payload = new ProcessorAction();
       //    isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload ,NORMAL_TIME_WAIT);

       //    //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
       //    if (isoResponse != null)
       //    {
       //        toReturn = new PurchaseResponse(isoResponse);
       //        if (toReturn.ResponseCode == "91")
       //        {
       //            throw new NoIssuerResponseException("No Response From Core Banking");
       //        }
       //    }

       //    else
       //    {
       //        throw new NoIssuerResponseException("No Response From Issuer");
       //    }
       //    toReturn.TheOriginalTransaction = payload.OriginalTrans;
       //    toReturn.TheTransaction = payload.SourceTrans;
       //    return toReturn;

       //}
       public PurchaseResponse DoPurchase(BankOneMobile.Core.Contracts.ITransaction trans, string xmlData,bool isBillsPayment)
       {
           PurchaseResponse toReturn = null;
           Purchase tr = CreatePurchase(trans as BankOneMobile.Core.Contracts.ITransaction, xmlData,isBillsPayment);
           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, LONG_TIME_WAIT);

           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse != null)
           {
               toReturn = new PurchaseResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
               if (toReturn.ResponseCode == "91")
               {
                   throw new NoIssuerResponseException("No Response From Core Banking");
                  // throw new NoISWGatewayResponse("No Response from ISWGateway");
               }
           }

           else
           {
               //throw new NoIssuerResponseException("No Response From Issuer");
               throw new NoISWGatewayResponse("No Response from ISWGateway");
           }
           toReturn.TheTransaction = payload.SourceTrans;
           toReturn.TheOriginalTransaction = payload.OriginalTrans;
           return toReturn;

       }
       public FundsTransferResponse DoFundsTransfer(BankOneMobile.Core.Contracts.ITransaction trans, string xmlData, ISOServiceCodes isoServ)
       {
           FundsTransferResponse toReturn = null;
           FundsTransfer tr = CreateFundsTransfer(trans as BankOneMobile.Core.Contracts.ITransaction, xmlData,isoServ);
           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           IAction payload = new ProcessorAction();
           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, 30);

           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse != null)
           {
               toReturn = new FundsTransferResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
               if (toReturn.ResponseCode == "91")
               {
                   throw new NoISWGatewayResponse("No Response From ISW Gateway");
               }
           }

           else
           {
               throw new NoISWGatewayResponse("No Response From ISW Gateway");
           }
           toReturn.TheTransaction = payload.SourceTrans;
           toReturn.TheOriginalTransaction = payload.OriginalTrans;
           return toReturn;

       }
       public PaymentResponse DoServiceAccountPayment(BankOneMobile.Core.Contracts.ITransaction trans, ServiceCodes code,ISOServiceCodes isoServ)
       {
           PaymentResponse toReturn = null;
           ServicePayment tr = CreateServicePayment(trans as BankOneMobile.Core.Contracts.ITransaction, code, isoServ);
           Trx.Messaging.Iso8583.Iso8583Message isoResponse = null;
           if (RoutingSystem.NodePeers == null || RoutingSystem.NodePeers.Count == 0)
           {
               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           if (!RoutingSystem.NodePeers.First().Value.IsConnected)//
           {

               throw new BankOneMobile.Core.Exceptions.NoSwitchResponseException("Switch Unavailable");
           }
           IAction payload = new ProcessorAction();

           isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr,  payload, NORMAL_TIME_WAIT);
        
           

           //toReturn.LedgerBalance = new Amount(new Random().Next(1000, 10000), "566", string.Empty);
           if (isoResponse != null)
           {
               toReturn = new PaymentResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
               if (toReturn.ResponseCode == "91")
               {
                   throw new NoIssuerResponseException("No Response From Core Banking");
               }
           }

           else
           {
               throw new NoIssuerResponseException("No Response From Issuer");
           }
           toReturn.TheTransaction = payload.SourceTrans;
           toReturn.TheOriginalTransaction = payload.OriginalTrans;
           return toReturn;

       }
       public  MessageResponse DoPinReset(BankOneMobile.Core.Contracts.ITransaction trans, IPinResetTransactionType transacType)
       {
           ChangePINResponse toReturn = null;
           //DO Pin Reset here
           return toReturn;
       }
       private MessageResponse DoLocalFundsTransfer(Core.Contracts.ITransaction trans)
       {
           PaymentResponse toReturn = null;

           Payment tr = CreatePayment(trans);
           //TODO  do the real thing here to send the transaction
        IAction payload = new ProcessorAction();
        if (RoutingSystem.ActiveNodes == null)
        {
            throw new NoSwitchResponseException("No Response From Switch");
        }
        else if(RoutingSystem.ActiveNodes.Count==0)
        {
            throw new NoSwitchResponseException("No Response From Switch");
        }
       Trx.Messaging.Iso8583.Iso8583Message    isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, payload, NORMAL_TIME_WAIT);
           // isoResponse = RoutingSystem.ActiveNodes.First().SendSync(tr, new ProcessorAction(), 30);
            if(isoResponse==null)
            {
                throw new NoIssuerResponseException("No Response From Issuer");
            }
            toReturn = new PaymentResponse(isoResponse, payload.SourceTrans, payload.OriginalTrans);
           toReturn.TheOriginalTransaction = payload.OriginalTrans;

          
           return toReturn;
       }

      



       //public ChangePINResponse DoPinChange(BankOneMobile.Core.Contracts.ITransaction trans,IPinChangeTransactionType transacType)
       //{
       //    ChangePINResponse toReturn = null;//new ChangePINResponse(;
       //   //TODO DO Real ISO Connection here
       //    return toReturn;
       //}
       public static ISOResponse DoISOTransactions(BankOneMobile.Core.Contracts.ITransaction trans, BankOneMobile.Core.Contracts.ITransactionType transacType)
       {
           ISOResponse toReturn = new ISOResponse();
           MessageResponse response = null;
           
           # region BalanceInquiry
           
           if (transacType is IBalanceInquiryTransactionType)
           {
              
                   response = new ProcessISOTransaction().DoBalanceEnquiry(trans);
               
             
               BalanceEnquiryResponse br =  response as BalanceEnquiryResponse;
               if (br.ResponseCode == "00")
               {
                   toReturn.ResponseDescription = (br.AvailableBalance.Balance / 100).ToString("N");
                   toReturn.ResponseMessage = (br.LedgerBalance.Balance / 100).ToString("N");
                   toReturn.Status = TransactionStatus.Successful;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
               }
               else
               {
                   toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   toReturn.ResponseMessage = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
              
           }
           # endregion
           # region Recharge

           else if (transacType is IRechargeTransactionType)
           {
               IRechargeTransactionType ttType = transacType as RechargeTransactionType;
               string rechargeXml = "<Transaction><TransactionType>00</TransactionType><Amount>XXAMOUNTXX</Amount><BillerID>XXBILLERIDXX</BillerID><PaymentItemID>XXPAYMENTITEMCODEXX</PaymentItemID><CustomerEmail>omudiaga@appzonegroup.com</CustomerEmail> <CustomerId>XXCUSTOMERIDXX</CustomerId> <TerminalId>3YYY0001</TerminalId><RequestReference>XXREQUESTREFERENCEXX</RequestReference></Transaction>";
               rechargeXml = rechargeXml.Replace("XXAMOUNTXX", ((long)ttType.Amount).ToString());
               rechargeXml = rechargeXml.Replace("XXREQUESTREFERENCEXX", ttType.TransactionReference);
               rechargeXml = rechargeXml.Replace("XXBILLERIDXX", ttType.TheMerchant.Code);
               rechargeXml = rechargeXml.Replace("XXCUSTOMERIDXX", ttType.Beneficiary);
               rechargeXml = rechargeXml.Replace("XXPAYMENTITEMCODEXX", ttType.PaymentItemCode);
               trans.TheService = Service.CustomerRecharge;

               trans.DisplayMessage = rechargeXml;
               trans.TheService = Service.CustomerRecharge;
               trans.TheMerchantType = MerchantType.NullMerchantType;
               trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;

               response = //new ProcessISOTransaction().DoPurchase(trans, billsXml);
                new ProcessISOTransaction().DoPayment(trans);// Send the Message to Core banking



               PaymentResponse br = response as PaymentResponse;
               # region debit leg  Succesfull on Core Banking
               if (br.ResponseCode == "00")// If succesfull then send to ISW Gateway
               {

                   ConfigureResponse(toReturn, br.ResponseCode, br.ResponseDescription, TransactionStatus.Successful, transacType);
                   trans.TheService = Service.CustomerRechargeCompletion;
                   trans.TheMerchantType = MerchantType.NullMerchantType;
                   # region commented
                   //toReturn.ResponseDescription = br.ResponseDescription;
                   //toReturn.ResponseMessage = br.ResponseDescription;
                   //toReturn.Status = TransactionStatus.Successful;
                   //transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   //transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   # endregion
                   try
                   {
                       trans.TheService = Service.CustomerRechargeCompletion;

                       response = new ProcessISOTransaction().DoPayment(trans);
                       response = response as PaymentResponse;
                       ttType.PaymentReference = (response as PaymentResponse).TransactionReference;
                   }
                   # region No Response from Switch Or Error
                   catch (NoIssuerResponseException)
                   {

                       LogReversal(br);
                       ConfigureResponse(toReturn, String.Format("ISW Gateway Unavailable"), "No Response From ISW Gateway", TransactionStatus.Reversed, transacType);
                       TransactionResponse iswResponse = ProcessISOTransaction.GetISWReferences(ttType.TransactionReference);//Confirm real trans status

                       ttType.PaymentReference = iswResponse.STAN;
                       
                       if (iswResponse.TransactionStatus == TransactionStatus.Successful)
                       {
                           ConfigureResponse(toReturn, "Transaction Successful", "Transaction Successful", TransactionStatus.Successful, ttType);                          

                           return toReturn;
                       }
                       else //Reverse the Core banking transaction
                       {


                           ConfigureResponse(toReturn, iswResponse.ResponseDescription, iswResponse.ResponseDescription, TransactionStatus.Reversed, ttType);
                           transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                           throw new FailedTransactionException(iswResponse.ResponseDescription);
                       }
                       
                   }
                   catch (Exception ex)
                   {
                       LogReversal(br);
                       ConfigureResponse(toReturn, String.Format("Errot on ISW Gateway-{0}", ex.Message), "No Response From ISW Gateway", TransactionStatus.Reversed, transacType);
                       throw;
                   }
                   # endregion
                   # region There was Response from Switch
                   # region ISW Gateway Response was Succeasful
                   PaymentResponse ft = response as PaymentResponse;
                   if (ft.ResponseCode == "00")// If succesfull then send response as succesfull
                   {
                       ConfigureResponse(toReturn, "Transaction Succesfull", "Transaction Successful", TransactionStatus.Successful, ttType);
                       ttType.ISWSTAN = ft.SystemsTraceAuditNumber;
                       ttType.PaymentReference = ft.TransactionReference;
                       return toReturn;


                   }
                   # endregion
                   # region ISW Gateway Response was Not Successful and not "Node Unavailable"

                   if (ft.ResponseCode != "91")// If not  succesfull  and not 91
                   {
                       LogReversal(br);
                       ConfigureResponse(toReturn, "Transaction Failed", ft.ResponseDescription, TransactionStatus.Reversed, ttType);
                       ttType.ISWSTAN = ft.SystemsTraceAuditNumber;
                       throw new FailedTransactionException(ft.ResponseDescription);




                   }
                   # endregion
                   # region Response was Node Unavailable
                   else if (ft.ResponseCode == "91")// If No Response
                   {
                       TransactionResponse iswResponse = ProcessISOTransaction.GetISWReferences(ft.TransactionReference);//Confirm real trans status
                       ttType.ISWSTAN = ft.SystemsTraceAuditNumber;
                       ttType.PaymentReference = iswResponse.STAN;
                       ft.ResponseCode = iswResponse.Status;
                       if (iswResponse.TransactionStatus == TransactionStatus.Successful)
                       {
                           ConfigureResponse(toReturn, "Transaction Successful", "Transaction Successful", TransactionStatus.Successful, ttType);
                           ft.ResponseCode = iswResponse.Status;
                           return toReturn;
                       }
                       else //Reverse the Core banking transaction
                       {
                           ft.ResponseCode = iswResponse.Status;
                          // LogReversal(br);
                           ft.MainResponseDescription = "The status of your transaction could not be determined. If you dont receive value,please contact you financial institution";
                           ConfigureResponse(toReturn, ft.ResponseDescription, ft.ResponseDescription, TransactionStatus.Reversed, ttType);
                           transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                           throw new FailedTransactionException(ft.ResponseDescription);
                       }

                   }
                   # region Redundant Commented  Code
                   else// Reverse the Core Banking Transaction
                   {

                       toReturn.ResponseDescription = ft.ResponseDescription;
                       toReturn.ResponseMessage = ft.ResponseDescription;
                       toReturn.Status = TransactionStatus.Failed;
                       //transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                       transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       //TODO Log  Reversal
                   }
                   // transacType.DisplayMessage = br.ValuePurchased;
                   # endregion
                   #endregion
                   # endregion


               }
               # endregion
               # region Debit leg on core banking failed
               else
               {
                   response = response as PaymentResponse;
                   toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
               # endregion


           }//Commercial Bank Transfer Ends here
           # endregion
           # region Bills Payment 

           else if (transacType is IBillsPaymentTransactionType)
           {
               //IBillsPaymentTransactionType bill = transacType as BillsPaymentTransactionType;
               //string billsXml = "<Transaction><TransactionType>51</TransactionType><Amount></Amount><BillerID>XXBILLERIDXX</BillerID><PaymentItemID>XXPAYMENTITEMIDXX</PaymentItemID><CustomerEmail>omudiaga@appzonegroup.com</CustomerEmail> <CustomerId>XXCUSTOMERIDXX</CustomerId> <TerminalId>3YYY0001</TerminalId><RequestReference>XXREQUESTREFERENCEXX</RequestReference></Transaction>";
               IBillsPaymentTransactionType bill = transacType as BillsPaymentTransactionType;
               string billsXml = "<Transaction><TransactionType>51</TransactionType><Amount>XXAMOUNTXX</Amount><BillerID>XXBILLERIDXX</BillerID><PaymentItemID>XXPAYMENTITEMIDXX</PaymentItemID><CustomerEmail>omudiaga@appzonegroup.com</CustomerEmail><CustomerMobile>08078786565</CustomerMobile><CustomerId>XXCUSTOMERIDXX</CustomerId><TerminalId>3YYY0001</TerminalId><RequestReference>XXREQUESTREFERENCEXX</RequestReference></Transaction>";
               billsXml = billsXml.Replace("XXAMOUNTXX",((long) bill.Amount).ToString());
               billsXml = billsXml.Replace("XXREQUESTREFERENCEXX",bill.TransactionReference);
               billsXml = billsXml.Replace("XXPAYMENTITEMIDXX", bill.ThePaymentItem.Code);
               billsXml = billsXml.Replace("XXBILLERIDXX",bill.TheMerchant.Code);
               billsXml = billsXml.Replace("XXCUSTOMERIDXX", bill.CustomerID);
               billsXml = billsXml.Replace("XXCUSTOMERMOBILEXX", bill.TheMobileAccount.MobilePhone);
               trans.TheService = Service.CustomerBillsPayment;

               trans.DisplayMessage = billsXml;
               trans.TheService = Service.CustomerBillsPayment;
               trans.TheMerchantType = MerchantType.NullMerchantType;
               trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;

               response = //new ProcessISOTransaction().DoPurchase(trans, billsXml);
                new ProcessISOTransaction().DoPayment(trans);// Send the Message to Core banking



               PaymentResponse br = response as PaymentResponse;
               # region debit leg  Succesfull on Core Banking
               if (br.ResponseCode == "00")// If succesfull then send to ISW Gateway
               {

                   ConfigureResponse(toReturn, br.ResponseCode, br.ResponseDescription, TransactionStatus.Successful, transacType);
                   trans.TheService = Service.CustomerBillsPaymentCompletion;
                   trans.TheMerchantType = MerchantType.NullMerchantType;
                   # region commented
                   //toReturn.ResponseDescription = br.ResponseDescription;
                   //toReturn.ResponseMessage = br.ResponseDescription;
                   //toReturn.Status = TransactionStatus.Successful;
                   //transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   //transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   # endregion
                   try
                   {
                       trans.TheService = Service.CustomerBillsPaymentCompletion;

                       response = new ProcessISOTransaction().DoPayment(trans);
                       response = response as PaymentResponse;
                   }
                   # region No Response from Switch Or Error
                   catch (NoIssuerResponseException)
                   {

                      // LogReversal(br);
                       string message = "The status of your transaction could not be determined. If you dont receive value,please contact you financial institution";
                       ConfigureResponse(toReturn, String.Format(message), message, TransactionStatus.Reversed, transacType);
                       throw new NoISWGatewayResponse(message);
                   }
                   catch (Exception ex)
                   {
                       LogReversal(br);
                       ConfigureResponse(toReturn, String.Format("Errot on ISW Gateway-{0}", ex.Message), "No Response From ISW Gateway", TransactionStatus.Reversed, transacType);
                       throw;
                   }
                   # endregion
                   # region There was Response from Switch
                   # region ISW Gateway Response was Succeasful
                   PaymentResponse ft = response as PaymentResponse;
                   if (ft.ResponseCode == "00")// If succesfull then send response as succesfull
                   {
                       ConfigureResponse(toReturn, "Transaction Succesfull", "Transaction Successful", TransactionStatus.Successful, bill);
                      bill .ISWSTAN = ft.SystemsTraceAuditNumber;
                      bill.PaymentReference = ft.TransactionReference;
                       return toReturn;


                   }
                   # endregion
                   # region ISW Gateway Response was Not Successful and not "Node Unavailable"

                   if (ft.ResponseCode != "91")// If not  succesfull  and not 91
                   {
                       LogReversal(br);
                       ConfigureResponse(toReturn, "Transaction Failed", ft.ResponseDescription, TransactionStatus.Reversed, bill);
                       bill.ISWSTAN = ft.SystemsTraceAuditNumber;
                       throw new FailedTransactionException(ft.ResponseDescription);




                   }
                   # endregion
                   # region Response was Node Unavailable
                   else if (ft.ResponseCode == "91")// If No Response
                   {
                       TransactionResponse iswResponse = ProcessISOTransaction.GetISWReferences(ft.TransactionReference);//Confirm real trans status
                       bill.ISWSTAN = ft.SystemsTraceAuditNumber;
                       bill.PaymentReference = iswResponse.STAN;
                       ft.ResponseCode = iswResponse.Status;
                       if (iswResponse.TransactionStatus == TransactionStatus.Successful)
                       {
                           ConfigureResponse(toReturn, "Transaction Successful", "Transaction Successful", TransactionStatus.Successful, bill);
                           ft.ResponseCode = iswResponse.Status;
                           
                           return toReturn;
                       }
                       else //Reverse the Core banking transaction
                       {
                           ft.ResponseCode = iswResponse.Status;
                           LogReversal(br);
                           ConfigureResponse(toReturn, ft.ResponseDescription, ft.ResponseDescription, TransactionStatus.Reversed, bill);
                           transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                           throw new FailedTransactionException(ft.ResponseDescription);
                       }

                   }
                   # region Redundant Commented  Code
                   else// Reverse the Core Banking Transaction
                   {

                       toReturn.ResponseDescription = ft.ResponseDescription;
                       toReturn.ResponseMessage = ft.ResponseDescription;
                       toReturn.Status = TransactionStatus.Failed;
                       //transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                       transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       //TODO Log  Reversal
                   }
                   // transacType.DisplayMessage = br.ValuePurchased;
                   # endregion
                   #endregion
                   # endregion

               }
               # endregion
               # region Debit leg on core banking failed
               else
               {
                   response = response as PaymentResponse;
                   toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
               # endregion


           }//Commercial Bank Transfer Ends here
           # endregion
           # region Commercial Bank Funds Transfer

           else if (transacType is ICommercialBankFundsTransferTransactionType)
           {
               //IBillsPaymentTransactionType bill = transacType as BillsPaymentTransactionType;
               //string billsXml = "<Transaction><TransactionType>51</TransactionType><Amount></Amount><BillerID>XXBILLERIDXX</BillerID><PaymentItemID>XXPAYMENTITEMIDXX</PaymentItemID><CustomerEmail>omudiaga@appzonegroup.com</CustomerEmail> <CustomerId>XXCUSTOMERIDXX</CustomerId> <TerminalId>3YYY0001</TerminalId><RequestReference>XXREQUESTREFERENCEXX</RequestReference></Transaction>";
               string requestXML = "<?xml version='1.0' encoding='utf-8' ?>";
               requestXML = "<Transaction><TransactionType>50</TransactionType><InitiatingEntityCode></InitiatingEntityCode><MAC>9F4E4F53C57BE63E1F08D8F07A7BC1A9461E4A7D5304043DAA1EF54BD727B6CDE148F4FBFC5E2AD8C4A60F78DFA76304DE671FBEB70657B1628F14B6B6BAA5E</MAC><TransferCode>XXTRANSFERCODEXX</TransferCode><Sender><Lastname>XXSENDERLASTNAMEXX</Lastname><Othernames>XXSENDEROTHERNAMESXX</Othernames><Email>omudiaga@appzonegroup.com</Email><Phone>XXSENDERPHONENUMBERXX</Phone></Sender><Beneficiary><Lastname>XXBENEFICIARYLASTNAMEXX</Lastname><Othernames>XXBENEFICIARYOTHERNAMESXX</Othernames><Email>fred@yahoo.com</Email><Phone>08078786565</Phone></Beneficiary><Initiation><Amount>XXAMOUNTXX</Amount><Channel>7</Channel><PaymentMethodCode>CA</PaymentMethodCode><CurrencyCode>566</CurrencyCode><Processor><Lastname></Lastname><Othernames></Othernames></Processor><DepositSlip></DepositSlip></Initiation><Termination><PaymentMethodCode>AC</PaymentMethodCode><Amount>XXAMOUNTXX</Amount><CurrencyCode>566</CurrencyCode><CountryCode>NG</CountryCode><EntityCode>XXTERMINATIONENTITYCODEXX</EntityCode><AccountReceivable><AccountNumber>XXBENEFICIARYACCOUNTNUMBERXX</AccountNumber><AccountType>XXACCOUNTTYPEXX</AccountType></AccountReceivable></Termination><RequestReference>XXREQUESTREFERENCEXX</RequestReference></Transaction>";

               ICommercialBankFundsTransferTransactionType ttType = transacType as ICommercialBankFundsTransferTransactionType;
          
               requestXML = requestXML.Replace("XXAMOUNTXX", ((long)ttType.Amount).ToString());
               requestXML = requestXML.Replace("XXTRANSFERCODEXX", ttType.TransactionReference);
               requestXML = requestXML.Replace("XXREQUESTREFERENCEXX", ttType.TransactionReference);
               requestXML = requestXML.Replace("XXSENDERLASTNAMEXX", ttType.TheMobileAccount.LastName);
               requestXML = requestXML.Replace("XXSENDEROTHERNAMESXX", ttType.TheMobileAccount.LastName);
               requestXML = requestXML.Replace("XXBENEFICIARYLASTNAMEXX", ttType.BeneficiaryLastName);
               requestXML = requestXML.Replace("XXBENEFICIARYOTHERNAMESXX", ttType.BeneficiaryOtherNames); //   
               requestXML = requestXML.Replace("XXBENEFICIARYPHONENUMBERXX", ttType.BeneficiaryPhoneNumber); // 
               requestXML = requestXML.Replace("XXBENEFICIARYACCOUNTNUMBERXX", ttType.AccountNumber);
               requestXML = requestXML.Replace("XXTERMINATIONENTITYCODEXX", ttType.BankCode);
               requestXML = requestXML.Replace("XXSENDERPHONENUMBERXX", ttType.TheMobileAccount.MobilePhone);
               if ((int)ttType.TheAccountType == 0)
               {
                   ttType.TheAccountType = AccountMode.Savings;
               }
               requestXML = requestXML.Replace("XXACCOUNTTYPEXX", ((int)ttType.TheAccountType).ToString());

               trans.DisplayMessage = requestXML;
               trans.TheService = Service.OffNetTransfer;
               trans.TheMerchantType = MerchantType.NullMerchantType;
               trans.ServiceCodes = ServiceCodes.ToServiceAccountPayment;
               
                   response = //new ProcessISOTransaction().DoPurchase(trans, billsXml);
                    new ProcessISOTransaction().DoPayment(trans);// Send the Message to Core banking
               
               

               PaymentResponse br = response as PaymentResponse;
            # region debit leg  Succesfull on Core Banking
               if (br.ResponseCode == "00")// If succesfull then send to ISW Gateway
               {
           
                   ConfigureResponse(toReturn, br.ResponseCode, br.ResponseDescription, TransactionStatus.Successful, transacType);
                   trans.TheService = Service.OffNetTransferCompletion;
                   trans.TheMerchantType = MerchantType.NullMerchantType;
                   
                   try
                   {
                       trans.TheService = Service.OffNetTransferCompletion;                      
                       
                       response = new ProcessISOTransaction().DoPayment(trans);
                       response = response as PaymentResponse;
                   }
                   # region No Response from Switch Or Error
                   catch (NoIssuerResponseException)
                   {
                       //string message = "The status of your transaction could not be determined. If you dont receive value,please contact you financial institution";
                       //ConfigureResponse(toReturn, String.Format(message), message, TransactionStatus.Reversed, transacType);
                       //throw new NoISWGatewayResponse(message);


                       Trace.TraceInformation("Could Not Send Because The Connection to the ISW Gateway was down", (transacType as ICommercialBankFundsTransferTransactionType).TransactionReference);
                       response.ResponseCode = "91";// if u ddnt get a response then allow control to drop to section of code that treates a response that came as 91                    
                         
                       //LogReversal(br);
                       //ConfigureResponse(toReturn, String.Format("ISW Gateway Unavailable"), "No Response From ISW Gateway", TransactionStatus.Reversed, transacType);
                       //throw new NoISWGatewayResponse("ISW Gateway Unavailable");
                   }
                   catch (Exception ex)
                   {                       
                       LogReversal(br);
                       ConfigureResponse(toReturn, String.Format("Error on ISW Gateway-{0}", ex.Message), "No Response From ISW Gateway", TransactionStatus.Reversed, transacType);                      
                       throw;
                   }
                   # endregion
                   # region There was Response from Switch
                   # region ISW Gateway Response was Succeasful
                   PaymentResponse ft = response as PaymentResponse;
                   if (ft.ResponseCode == "00")// If succesfull then send response as succesfull
                   {
                       ConfigureResponse(toReturn, "Transaction Succesfull", "Transaction Successful", TransactionStatus.Successful, ttType);
                       ttType.ISWSTAN = ft.SystemsTraceAuditNumber;
                       ttType.PaymentReference = ft.TransactionReference;
                       return toReturn;
                       

                   }
                  # endregion
                    
                   # region Response was Node Unavailable
                   else if (ft.ResponseCode == "91")// If No Response
                   {
                       ICommercialBankFundsTransferTransactionType ftTrans = transacType as ICommercialBankFundsTransferTransactionType;
                       Trace.TraceInformation("Node Unavailable Section-{0}",ft.SystemsTraceAuditNumber);
                       //TransactionResponse iswResponse = ProcessISOTransaction.GetISWReferences(ft.TransactionReference);//Confirm real trans status
                       TransactionResponse confirmResponse = ConfirmTransactionFromQuickTeller(ftTrans.TransactionReference, ftTrans.TheTransaction.FromInstitutionCode, true);// ProcessISOTransaction.GetISWReferences(ft.TransactionReference);//Confirm real trans status
                       if (confirmResponse.Status == "Failed")
                       {
                           Trace.TraceInformation("91 Response -{0} {1} {2}", ftTrans.TransactionReference, confirmResponse.ResponseDescription, confirmResponse.Status);
                           LogReversal(br);
                           //ConfigureResponse(toReturn, "Transaction Failed", ft.ResponseDescription, TransactionStatus.Reversed, bill);
                           Trace.TraceInformation(
                               "Reversal Logged. Transfer Transaction reference({0}) response from ISW, Response Code: {1}, ResponseDescription {2} : {3}"
                               , ftTrans.TransactionReference, ft.ResponseCode, ft.ResponseDescription,
                               ft.MainResponseDescription);
                           ftTrans.ISWSTAN = ft.SystemsTraceAuditNumber;
                           trans.Status = TransactionStatus.Reversed;
                           toReturn.Status = TransactionStatus.Reversed;
                           toReturn.ResponseDescription = confirmResponse.ResponseDescription;
                           toReturn.ResponseMessage = confirmResponse.ResponseDescription;


                       }
                       else if (confirmResponse.Status == "Pending")
                       {
                           Trace.TraceInformation("91  Response -{0} {1} {2}", ftTrans.TransactionReference, "Pending", confirmResponse.Status);
                           //ConfigureResponse(toReturn, "Transaction Succesfull", "Transaction Successful", TransactionStatus.Successful, bill);
                           Trace.TraceInformation("Transfer Transaction reference({0}) response from ISW, response code: {1}", ftTrans.TransactionReference, "Pending");
                           ftTrans.ISWSTAN = ft.SystemsTraceAuditNumber;
                           ftTrans.PaymentReference = ft.TransactionReference;
                           trans.Status = TransactionStatus.Pending;
                              
                           toReturn.Status=TransactionStatus.Pending;
                           toReturn.ResponseDescription = confirmResponse.ResponseDescription;
                           toReturn.ResponseMessage = confirmResponse.ResponseDescription;          
                           
                           return toReturn;
                       }
                       else
                       {
                           //ConfigureResponse(toReturn, "Transaction Succesfull", "Transaction Successful", TransactionStatus.Successful, bill);
                           Trace.TraceInformation("Transfer Transaction reference({0}) response from ISW, response code: {1}", ftTrans.TransactionReference, "00");
                           Trace.TraceInformation("Succesfull  Response -{0} {1} {2}",ft.TransactionReference, trans.Status, trans.Status);
                           ftTrans.ISWSTAN = ft.SystemsTraceAuditNumber;
                           ftTrans.PaymentReference = ft.TransactionReference;
                           trans.Status = TransactionStatus.Successful;
                           trans.StatusDetails = "Transaction Successful";
                           confirmResponse.Status = "Successful";
                           toReturn.ResponseDescription = confirmResponse.ResponseDescription;
                           toReturn.ResponseMessage = confirmResponse.ResponseDescription;
                           return toReturn;
                       }

                       //ttType.ISWSTAN = ft.SystemsTraceAuditNumber;                       
                       //ttType.PaymentReference = iswResponse.STAN;
                       //ft.ResponseCode = iswResponse.Status;                   
                       //if (iswResponse.TransactionStatus == TransactionStatus.Successful)
                       //{
                       //    ConfigureResponse(toReturn,"Transaction Successful","Transaction Successful", TransactionStatus.Successful, ttType); 
                       //    ft.ResponseCode = iswResponse.Status;
                       //    return toReturn;
                       //}
                       //else //Reverse the Core banking transaction
                       //{
                       //    ft.ResponseCode = iswResponse.Status;
                       //    LogReversal(br);                          
                       //    ConfigureResponse(toReturn, ft.ResponseDescription, ft.ResponseDescription,TransactionStatus.Reversed,ttType);                      
                       //    transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       //    throw new FailedTransactionException(ft.ResponseDescription);
                       //}

                   }
                                                                          
                   else// Reverse the Core Banking Transaction usually 06 Response
                   {
                       ICommercialBankFundsTransferTransactionType ftTrans = transacType as ICommercialBankFundsTransferTransactionType;

                       Trace.TraceInformation("Unsusuccesful Response -Code:{0} Description:{1} MainCode:{2}", ftTrans.TransactionReference, ft.ResponseCode,ft.ResponseDescription,ft.MainResponseCode,ft.MainResponseDescription);
                       LogReversal(br);
                       //ConfigureResponse(toReturn, "Transaction Failed", ft.ResponseDescription, TransactionStatus.Reversed, bill);
                       Trace.TraceInformation(
                           "Reversal Logged. Transfer Transaction reference({0}) response from ISW, Response Code: {1}, ResponseDescription {2} : {3}"
                           , ftTrans.TransactionReference, ft.ResponseCode, ft.ResponseDescription,
                           ft.MainResponseDescription);
                       ftTrans.ISWSTAN = ft.SystemsTraceAuditNumber;
                       //trans.Status = TransactionStatus.Reversed;
                       //toReturn.Status = TransactionStatus.Reversed;
                       //toReturn.ResponseDescription = confirmResponse.ResponseDescription;
                       //toReturn.ResponseMessage = confirmResponse.ResponseDescription;



                       toReturn.ResponseDescription = ft.ResponseDescription;
                       toReturn.ResponseMessage = ft.ResponseDescription;
                       toReturn.Status = TransactionStatus.Failed;
                       //transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                       transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       //TODO Log  Reversal
                   }
                   // transacType.DisplayMessage = br.ValuePurchased;
                   
                   #endregion
                   # endregion

               }
           # endregion
            # region Debit leg on core banking failed
               else
               {
                   response = response as PaymentResponse;
                   toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
               # endregion

           
           }//Commercial Bank Transfer Ends here
           # endregion
           # region MiniStatement

           else  if (transacType is IMiniStatementTransactionType)
           {

               response = new ProcessISOTransaction().DoMiniStatement(trans);


               MiniStatementResponse br = response as MiniStatementResponse;
               if (br.ResponseCode == "00")
               {
                   StringBuilder builder = new StringBuilder(); int count = 0;
                   //toReturn.ResponseDescription = (br.AvailableBalance.Balance / 100).ToString("N");
                   //toReturn.ResponseMessage = (br.LedgerBalance.Balance / 100).ToString("N");
                  toReturn.Status = TransactionStatus.Successful;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       IList<TransactionHistory> theTransactions = new List<TransactionHistory>();
                   if (br.Transactions.Count > 0)
                   {
                       foreach (BankOneMobile.SwitchIntegration.ISO8583.DTO.Transaction isoTrans in br.Transactions)
                       {
                           TransactionHistory history = new TransactionHistory { TransactionType = isoTrans.Type.ToString(), Date = isoTrans.Date, Amount = isoTrans.TheAmount.Balance, Status = "Succesfull" };
                           TransactionDirection dir;
                         bool casted =  Enum.TryParse<TransactionDirection>(history.TransactionType, out  dir);
                           history.Direction = !casted ? TransactionDirection.Credit : dir;

                           theTransactions.Add(history);
                           count++;
                           builder.AppendFormat("{0} {1}", history.ToString(), Environment.NewLine);
                       }
                   }
                   else
                   {
                       builder.Append("There are no transaction  in your history");
                   }
                   (transacType as IMiniStatementTransactionType).TheTransactions=theTransactions;
                   toReturn.ResponseMessage = toReturn.ResponseDescription = builder.ToString();
               }
               else
               {
                   toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   toReturn.ResponseMessage = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }

           }
           # endregion           
           # region Pin Change
           else if (transacType is IPinChangeTransactionType)
           {
               response = new ProcessISOTransaction().DoChangePin(trans,transacType as  IPinChangeTransactionType);


               ChangePINResponse br = response as ChangePINResponse;
               if (br.ResponseCode == "00")
               {
                   //toReturn.ResponseDescription = (br.AvailableBalance.Balance / 100).ToString("N");
                   //toReturn.ResponseMessage = (br.LedgerBalance.Balance / 100).ToString("N");
                   toReturn.Status = TransactionStatus.Successful;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
               }
               else
               {
                   //toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   //toReturn.ResponseMessage = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
           }
           # endregion
           # region Set Default Account
           else if (transacType is IConfigurationTransactionType)
           {
               response = new ProcessISOTransaction().DoConfiguration(trans, transacType as IConfigurationTransactionType);


               ConfigurationResponse br = response as ConfigurationResponse;
               if (br.ResponseCode == "00")
               {
                   //toReturn.ResponseDescription = (br.AvailableBalance.Balance / 100).ToString("N");
                   //toReturn.ResponseMessage = (br.LedgerBalance.Balance / 100).ToString("N");
                   toReturn.Status = TransactionStatus.Successful;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
               }
               else
               {
                   //toReturn.ResponseDescription = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   //toReturn.ResponseMessage = br == null ? "Issuer Or Switch Unavailable" : br.ResponseDescription;
                   transacType.TheTransaction.STAN = br.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = br.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(br.ResponseDescription);
               }
           }
           # endregion
           # region Pin Reset
           else if (transacType is IPinResetTransactionType)
           {
               response = new ProcessISOTransaction().DoPinReset(trans,transacType as IPinResetTransactionType);
               toReturn.ResponseMessage = (response as ChangePINResponse).ResponseDescription;
               toReturn.Status = response.ResponseCode == "00" ? TransactionStatus.Successful : TransactionStatus.Failed;
               toReturn.ResponseDescription = response.ResponseDescription;
           }
           # endregion
           # region Registration
           else if (transacType is IRegistrationTransactionType)
           {

           }
           # endregion
           # region Local Funds Transfer
           else if (transacType is ILocalFundsTransferTransactionType)
           {
               
               PaymentResponse rsp = new ProcessISOTransaction().DoLocalFundsTransfer(trans) as PaymentResponse;             
          
               if (rsp.ResponseCode == "00")
               {
                   toReturn.Status = TransactionStatus.Successful;
                   toReturn.ResponseDescription = rsp.ResponseDescription;
                   toReturn.ResponseMessage = rsp.ResponseDescription;
                   transacType.TheTransaction.STAN = rsp.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = rsp.TransDateTime == null ? (DateTime)SqlDateTime.Null : rsp.TransDateTime;


               }
               else
               {
                   toReturn.Status = TransactionStatus.Failed;
                   toReturn.ResponseDescription = response == null ? "Issuer Or Switch Unavailable" : rsp.ResponseDescription;
                   toReturn.ResponseMessage = response == null ? "Issuer Or Switch Unavailable" : rsp.ResponseDescription;
                   transacType.TheTransaction.STAN = rsp.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = rsp.TransDateTime == null ? (DateTime)SqlDateTime.Null : rsp.TransDateTime;
                   throw new FailedTransactionException(rsp.ResponseDescription);
               }
               
           }
# endregion
           # region payment Guys

           else if ((transacType is CashInTransactionType || transacType is CashOutTransactionType))
           {
               try
               {
                   response = new ProcessISOTransaction().DoPayment(trans);
                   
               }
               catch
               {
                   trans.Status = TransactionStatus.Failed;
                   throw;
               }
               PaymentResponse payment = response as PaymentResponse;
               if (payment.ResponseCode == "00")
               {
                   toReturn.Status = TransactionStatus.Successful;
                   toReturn.ResponseDescription = response.ResponseDescription;
                   toReturn.ResponseMessage = response.ResponseDescription;
                   transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : payment.TransDateTime;

                   
               }
               else
               {
                   toReturn.Status = TransactionStatus.Failed;
                   toReturn.ResponseDescription = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                   toReturn.ResponseMessage = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                   transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(response.ResponseDescription);
               }
               
           }
           # endregion
           # region Service Account Payment Guys
           # region funds transfer cash out
           else if (transacType is FundsTransferCashOutTransactionType)
           {
               FundsTransferCashOutTransactionType theCashOut = transacType as FundsTransferCashOutTransactionType;
               //First and foremost move the money to the customers acct from the service acct
               
               
                   BankOneMobile.Core.Implementations.Transaction firstTransaction = new BankOneMobile.Core.Implementations.Transaction { Amount = theCashOut.OriginalAmount * 100, FromPhoneNumber = trans.FromPhoneNumber, From = string.Empty, To = trans.From, ToInstitutionCode = trans.FromInstitutionCode, FromInstitutionCode = string.Empty };// debit the service account credit the customer
                   try
                   {
                       response = new ProcessISOTransaction().DoServiceAccountPayment(firstTransaction, ServiceCodes.FromServiceAccountPayment,ISOServiceCodes.CustomerPayment);// run the transaction
                   }
                   catch(Exception ex)// means the first transaction failed.No need to move forward
                   {
                       throw new AccountCreatedNoFundsTranferException(ex.Message);
                   }
               
                   if (response != null && response.ResponseCode == "00")// if the first transaction succeded proceed to next
                   {
                       toReturn.Status = TransactionStatus.Successful;
                       //(transacType as FundsTransferCashOutTransactionType).TheCashIn.IsConsumed = true;
                       if (theCashOut.IsCash)// if its cash, move the requested  money to the agents account else leave it there
                       {
                           try
                           {
                               trans.Narration = string.Format("Cash Withdrawal Through Agent ({0}) from {1} to {2}", (transacType as FundsTransferCashOutTransactionType).AgentCode,trans.From,trans.To);
                               response = new ProcessISOTransaction().DoPayment(trans);
                           }
                           
                           catch(Exception ex)
                           {
                               throw new FundsTransferNoCashOutException(String.Format("{0}. Reason-{1}","Funds have been transferred but the requested cash out failed",ex.Message)) ;
                           }

                           if (response.ResponseCode == "00")
                           {
                               toReturn.Status = TransactionStatus.Successful;
                               toReturn.ResponseMessage = response.ResponseDescription;
                               toReturn.ResponseDescription = response.ResponseDescription;
                               toReturn.ResponseMessage = response.ResponseDescription;
                               transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                               transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                           }

                           else
                           {
                               toReturn.Status = TransactionStatus.Failed;
                               toReturn.ResponseDescription = response.ResponseDescription;
                               toReturn.ResponseMessage = response.ResponseDescription;
                               transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                               transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                               throw new FundsTransferNoCashOutException("Funds have been transferred but the requested cash out failed");
                           }
                          
                       }
                       
                       toReturn.ResponseDescription = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                       toReturn.ResponseMessage = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                       transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                       transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                       return toReturn;
                   }
                   else if (response != null)
                   {
                           toReturn.Status = TransactionStatus.Failed;
                               toReturn.ResponseMessage = response.ResponseDescription;
                               toReturn.ResponseDescription = response.ResponseDescription;
                               toReturn.ResponseMessage = response.ResponseDescription;
                               transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                               transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   }
                   
           }
# endregion
           # region Funds Transfer Cash In
           else if (transacType is IFundsTransferCashInTransactionType)
           {
               FundsTransferCashInTransactionType theCashOut = transacType as FundsTransferCashInTransactionType;
               
               
               response = new ProcessISOTransaction().DoServiceAccountPayment(trans, ServiceCodes.ToServiceAccountPayment,ISOServiceCodes.CustomerPayment);// run the transaction
               if (response!=null&&response.ResponseCode == "00")
               {
                   toReturn.Status = TransactionStatus.Successful;
                   toReturn.ResponseMessage = response.ResponseDescription;
                   toReturn.ResponseDescription = response.ResponseDescription;
                   toReturn.ResponseMessage = response.ResponseDescription;
                   transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
               }

               else
               {
                   toReturn.Status = TransactionStatus.Failed;
                   toReturn.ResponseDescription = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                   toReturn.ResponseMessage = response == null ? "Issuer Or Switch Unavailable" : response.ResponseDescription;
                   transacType.TheTransaction.STAN = response.SystemsTraceAuditNumber;
                   transacType.TheTransaction.SwitchTransactionTime = response.TransDateTime == null ? (DateTime)SqlDateTime.Null : response.TransDateTime;
                   throw new FailedTransactionException(response.ResponseDescription);
               }
               

           }
           # endregion
           # endregion

           //return new ISOResponse{ ResponseDescription=response.ResponseDescription, Status= response.ResponseCode=="00"?TransactionStatus.Successful:TransactionStatus.Failed, ResponseMessage=string.Format("{0} {1}",trans.TransactionTypeName,Status.ToString())});
           
           return toReturn;
           
       }

       


       //public static bool SendISOMessage(this INode theNode, Trx.Messaging.Message theMessage, IAction payload, int timeOut)
       //{
       //    bool toReturn = false;
       //    PeerRequest request = new PeerRequest(RoutingSystem.NodePeers[theNode.Name], theMessage);
       //    request.Payload = payload;
       //    request.Send(timeOut * 1000);
       //    return toReturn;
       //}

       public static  Trx.Messaging.Iso8583.Iso8583Message SendActualISO(MobileBasedMessage message)
       {
           Trx.Messaging.Iso8583.Iso8583Message isoResponse = RoutingSystem.ActiveNodes.First().SendSync(message, new ProcessorAction(), 30);
           return isoResponse;
       }


      




       public TransactionResponse SendISO(string from, string toAcct, decimal amount, string phone)
       {
           TransactionResponse rsp = null;
           using (ISOSender.ISOConnectionServiceClient client = new ISOSender.ISOConnectionServiceClient())
           {
              rsp =  client.SendActualISOMessage(from, toAcct, amount, phone);
           }
           return rsp;
       }

       public static  TransactionResponse GetISWReferences(string transactionReference)
       {
           TransactionResponse toReturn = new TransactionResponse(); string responseXML = string.Empty; string responseCode = string.Empty;
           string transRef = string.Empty; string value = string.Empty;
           try
           {
               using (ISWGatewayService.ServiceClient client = new ISWGatewayService.ServiceClient())
               {
                   
                   responseXML = client.ConfirmPaymentMessage(transactionReference);
               }
           }
           catch
           {
               throw new NoISWGatewayWebServiceResponseException("ISW Gateway Web Service Unavailable");
           }
           responseXML = responseXML.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");

           if (!string.IsNullOrEmpty(responseXML))
           {
               XDocument document = XDocument.Parse(responseXML);


               foreach (var doc in document.Descendants("ResponseData"))
               {
                   responseCode = doc.Element("ResponseCode") != null ? doc.Element("ResponseCode").Value : responseCode;

                   transRef = doc.Element("TransactionRef") != null ? doc.Element("TransactionRef").Value : responseCode;
                   value = doc.Element("RechargePIN") != null ? doc.Element("RechargePIN").Value : responseCode;

                   // do whatever you want to do with those items of information now
               }
           }
           
           toReturn.STAN = transRef;
           toReturn.ResponseMessage = value;
           toReturn.Status = responseCode;

           if (toReturn.Status == "9000")
           {
               toReturn.TransactionStatus = TransactionStatus.Successful;
           }
           return toReturn;
       }
       public static  void LogReversal(MessageResponse br)
       {
           INode thisNode = AppZoneSwitch.Services.RoutingSystem.ActiveNodes[0];
           
           AppZoneSwitch.Services.ReversalSystem.LogReversal(thisNode, br.TheOriginalTransaction);
       }
       public static  ISOResponse ConfigureResponse(ISOResponse iso, string responseMessage, string responseDscription,TransactionStatus status,BankOneMobile.Core.Contracts.ITransactionType type)
       {
           iso.ResponseMessage = responseMessage;
           iso.ResponseDescription = responseDscription;
           iso.Status = status;
           type.TheTransaction.Status = status;
           return iso;
       }
       public static TransactionResponse ConfirmTransactionFromQuickTeller(string reference, string instCode, bool isTransfer)
       {
           Trace.TraceInformation("Calling Confirm Service. Ref is {0}, Code is {1}, Is Transfer is {2}", reference, instCode, isTransfer);
           TransactionResponse toReturn = new TransactionResponse();
           BankOneGatewayServiceRef.ServiceClient client = null;
           string responseDesc = string.Empty; string paymentRef = string.Empty;
           bool successful = false;
           try
           {
              client= new BankOneGatewayServiceRef.ServiceClient();
               successful = client.QueryAllTransactionTypesWithRawReference(out responseDesc, instCode, reference, isTransfer);
               Trace.TraceInformation("Trx Status Succesfully Called For. Ref is {0}, Code is {1}, Is Transfer is {2}, Status is {3}", reference, instCode, isTransfer,successful);
           }
           catch (Exception ex)
           {
               Trace.TraceInformation("Trx Status Exception Thrown For. Ref is {0}, Code is {1}, Is Transfer is {2}", reference, instCode, isTransfer);
               Trace.TraceInformation(ex.Message);
               toReturn.Status = "Pending";               
               toReturn.ResponseDescription = "Transaction Status Cannot be Determined at this point.Please Contact your administrator";
               return toReturn;

           }
           if (successful)
           {
               Trace.TraceInformation("Transaction Was Successfu - Ref:{0},paymentRef {1}", reference, responseDesc);
               toReturn.Status = "Successful";
               toReturn.ResponseDescription = "Transaction Successful";
               
               //toReturn.ResponseDescription = responseDesc;
           }
           else
           {
               Trace.TraceInformation("Trx Status Succesfully Called and confirmed as failed For. Ref is {0}, Code is {1}, Is Transfer is {2}, Reason is {3}", reference, instCode, isTransfer, responseDesc);
               toReturn.Status = "Failed";
               toReturn.ResponseDescription = responseDesc == "Data not found" ? "Unable To Connect To Interswitch" : responseDesc;
              // toReturn.ResponseDescription = responseDesc;
           }

           return toReturn;
       }

       public static string GetQuickTellerResponse(string theRequestReference)
       {
           Trace.TraceInformation("Querying QuickTeller for response.");
           XDocument xDoc = new XDocument(new XDeclaration("1.0", "utf-8", "true"));
           XElement root = new XElement("RequestDetails",
                               new XElement("RequestReference", theRequestReference));
           xDoc.Add(root);
           string xmlParams = xDoc.ToString();

           string responseXml;
           using (BankOneGatewayServiceRef.ServiceClient client = new BankOneGatewayServiceRef.ServiceClient())
           {
               try
               {
                   responseXml = client.QueryTransaction(xmlParams);
               }
               catch (Exception ex)
               {
                   Trace.TraceError(ex.Message);
                   throw new NoISWGatewayResponse("QuickTeller Web Service Unavailable");
               }
           }
           Trace.TraceInformation("Querying QuickTeller for response Successful.");
           return responseXml;
       }



       public static string AutoRegisterLafayetteCustomer(BankOneMobile.Core.Contracts.ITransaction trans,IConfigurationTransactionType transacType)
       {
           ConfigurationResponse response = new ProcessISOTransaction().DoLafayetteReg(trans, transacType);
           return response.ResponseDescription;

       }
    }
} 
