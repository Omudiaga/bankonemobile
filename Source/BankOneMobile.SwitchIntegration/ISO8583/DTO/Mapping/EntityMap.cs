﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.Mappings
{
    public class EntityMap<T> : ClassMap<T> where T : IEntity
    {
        public EntityMap()
        {
            Id(x => x.ID)
                .GeneratedBy.Native();
        }
    }
}
