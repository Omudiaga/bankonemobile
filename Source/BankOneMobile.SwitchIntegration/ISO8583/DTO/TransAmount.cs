﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Implementations;

namespace BankOneMobile.SwitchIntegration.ISO8583.DTO
{
    public class TransAmount : Entity
    {
        public string Amount { get; set; }
        public string Sign { get; set; }
        public string CurrencyCode { get; set; }
    }
}
