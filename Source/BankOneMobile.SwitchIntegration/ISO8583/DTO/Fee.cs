﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BankOneMobile.SwitchIntegration.ISO8583.DTO
{
    public class Fee : TransAmount
    {
        public string FeeDescription { get; set; }
    }
}
