using System;
using System.Collections.Generic;
using System.Text;
using Trx.Messaging.Iso8583;
using Trx.Messaging;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public abstract class AuthorizationMessage : MobileBasedMessage
    {

        public AuthorizationMessage(TerminalOwner terminalOwner, string transactionID, TransactionType transType, MobileAccountDetails theMobileAccount, string fromAccountType, string toAccountType, Amount transferAmount, bool isRepeat)
            : base(100, terminalOwner, theMobileAccount, transactionID, isRepeat)
        {
            this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)transType).PadLeft(2,'0'), fromAccountType, toAccountType));
            this.Fields.Add(FieldNos.F4_TransAmount, transferAmount.Balance.ToString());

            this.Fields.Add(FieldNos.F49_TransCurrencyCode, transferAmount.CurrencyCode);
        }


    }
}
