using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class MiniStatement : FinancialMessage
    {
        public MiniStatement(TerminalOwner terminalOwner, Account acct, MobileAccountDetails theMobileAccount, string institutionCode, string transactionID, bool isRepeat, string narration)
            : base(terminalOwner, transactionID, TransactionType.MiniStatementInquiry, theMobileAccount, institutionCode, acct.Type, "00", new Amount(0, "566", AmountType.AvailableBalance), isRepeat,narration)
        {
            this.Fields.Add(FieldNos.F102_Account1, acct.Number);
            if (!String.IsNullOrEmpty(theMobileAccount.ServiceCode))
            {

                this.Fields.Add(FieldNos.F94_ServiceCode, theMobileAccount.ServiceCode.PadLeft(4, '0'));
            }

        }
    }
}
