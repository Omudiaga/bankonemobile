using System;
using System.Collections.Generic;
using System.Text;
using Trx.Messaging.Iso8583;
using Trx.Messaging;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;
using System.Runtime.Serialization;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    [Serializable]
    public abstract class FinancialMessage : MobileBasedMessage
    {

        public FinancialMessage(TerminalOwner terminalOwner, string transactionID, TransactionType transType, MobileAccountDetails theMobileAccount, string fromInstitutionCode, string fromAccountType, string toAccountType, Amount transferAmount, bool isRepeat,string narration)
            : base(200, terminalOwner, theMobileAccount, transactionID, isRepeat)
        {
            this.Fields.Add(FieldNos.F3_ProcCode, string.Format("{0}{1}{2}", Convert.ToString((int)transType), fromAccountType, toAccountType).PadLeft(6,'0'));
            this.Fields.Add(FieldNos.F4_TransAmount, transferAmount.Balance.ToString());

            this.Fields.Add(FieldNos.F49_TransCurrencyCode, transferAmount.CurrencyCode);

            this.Fields.Add(100, fromInstitutionCode);

            Trx.Messaging.Message msg = new Trx.Messaging.Message();
            msg.Fields.Add(19, "Appzone Group Ltd");
            if (isRepeat)
            {
                msg.Fields.Add(20, "DV");
            }
           
            this.Fields.Add(127, msg);
            if (narration != null)
            {
                string realNarration = narration.Length <= 100 ? narration : narration.Substring(0, 99);
                this.Fields.Add(FieldNos.F104_TransactionDescription, realNarration);
            }
            
        }


    }
}
