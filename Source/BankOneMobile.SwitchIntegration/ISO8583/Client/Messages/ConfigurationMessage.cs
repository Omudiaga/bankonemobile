using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Configuration;
using BankOneMobile.SwitchIntegration.Utility;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.Messages
{
    public class ConfigurationMessage : AdministrationMessage
    {
        public ConfigurationMessage(TerminalOwner terminalOwner, Account acct, MobileAccountDetails theMobileAccount,  string transactionID, bool isRepeat,string instCode)
            : base(terminalOwner, transactionID, TransactionType.SetDefaultAccount, theMobileAccount,acct.Type, AccountType.Default, isRepeat, instCode)
        {
            this.Fields.Add(FieldNos.F102_Account1, acct.Number);
            

        }
    }
}
