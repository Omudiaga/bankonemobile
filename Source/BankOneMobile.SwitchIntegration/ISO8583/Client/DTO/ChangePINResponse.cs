using System;
using System.Collections.Generic;
using System.Text;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    public class ChangePINResponse : MessageResponse
    {
        public ChangePINResponse(Trx.Messaging.Message responseMessage, AppZoneSwitch.Core.Contracts.ITransaction sourceTrans, AppZoneSwitch.Core.Contracts.ITransaction originalTrans)
            : base(responseMessage, sourceTrans, originalTrans)
        {
           
        }
    }
}
