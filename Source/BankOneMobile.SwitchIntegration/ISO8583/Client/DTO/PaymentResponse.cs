using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.Client.Utility;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using BankOneMobile.SwitchIntegration.ISO8583.Utility;
using System.Runtime.Serialization;
using System.Xml.Linq;
using AppZoneSwitch.Core.Contracts;
using System.Diagnostics;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    [DataContract]
    public class PaymentResponse : MessageResponse
    {
        public PaymentResponse(Trx.Messaging.Message responseMessage,ITransaction sourceTrans,ITransaction originalTrans)
            : base(responseMessage,sourceTrans,originalTrans)
        {
            
            if (responseMessage.Fields.Contains(4))
            {
                this._amount = new Amount(Convert.ToInt64(responseMessage.Fields[4].Value), responseMessage.Fields[49].Value.ToString(), AmountType.Approved);
            }
            if (responseMessage.Fields[127]!=null&&responseMessage.Fields[127].Value != null)
            {
                Trx.Messaging.Message msg = responseMessage.Fields[127].Value as Trx.Messaging.Message;
                if (msg.Fields[22] != null)
                {
                    string responseXML = msg.Fields[22].Value.ToString();
                    int val = responseXML.IndexOf('<');
                    responseXML = responseXML.Substring(val);
                    if (!string.IsNullOrEmpty(responseXML))
                    {
                        XDocument document = XDocument.Parse(responseXML);


                        foreach (var doc in document.Descendants("Response"))
                        {


                            this.TransactionReference = doc.Element("TransactionRef") != null ? doc.Element("TransactionRef").Value : string.Empty;
                            this.MainResponseDescription = doc.Element("ResponseDescription") != null ? doc.Element("ResponseDescription").Value : string.Empty;
                            this.MainResponseCode = doc.Element("ResponseCode") != null ? doc.Element("ResponseCode").Value : string.Empty;

                            // do whatever you want to do with those items of information now
                        }
                    }
                }

            }
            if (this.MainResponseCode == "90091")
            {
                this.ResponseCode = "91";
            }
        }

        private Amount _amount;
        private string _transactionReference;
        public Amount Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string TransactionReference
        {
            get { return _transactionReference; }
            set { _transactionReference = value; }
        }
    }
}
