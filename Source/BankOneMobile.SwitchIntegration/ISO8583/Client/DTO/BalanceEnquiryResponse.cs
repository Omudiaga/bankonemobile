using System;
using System.Collections.Generic;
using System.Text;
using BankOneMobile.SwitchIntegration.ISO8583.DTO;
using System.Runtime.Serialization;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    [DataContract]
    public class BalanceEnquiryResponse : MessageResponse
    {
        private string _AccountName;    
        private Account _AccountDetails;
        private Amount _AvailableBalance;
        private Amount _LedgerBalance;

        public BalanceEnquiryResponse(Trx.Messaging.Message responseMessage, AppZoneSwitch.Core.Contracts.ITransaction sourceTrans, AppZoneSwitch.Core.Contracts.ITransaction originalTrans)
            : base(responseMessage, sourceTrans, originalTrans)
        {
            if (responseMessage.Fields.Contains(54))
            {
                string amt = responseMessage.Fields[54].Value.ToString();
                if(amt.IndexOf('.')>=0)
                {

                    amt = amt.Remove(amt.IndexOf('.'));
                }
                string realAmount = amt.Substring(0,20);//.Substring(0, 20);
                string ledgerAmt = amt.Substring(20, 20); 

               //realAmount = realAmount.Remove(realAmount.IndexOf('.'));
               //realAmount = realAmount.Remove(0,8);
                this._LedgerBalance = new Amount(realAmount);
                this._AvailableBalance = new Amount(ledgerAmt);
            }
            //if (responseMessage.Fields.Contains(102) && responseMessage.Fields.Contains(3))
            //{
            //    //this._AccountDetails = new Account(responseMessage.Fields[102].Value.ToString(), responseMessage.Fields[3].Value.ToString().Substring(2,2));
            //}
            
            
        }

        [DataMember]
        public Amount AvailableBalance
        {
            get
            {
                return this._AvailableBalance;
            }
            set
            {
                this._AvailableBalance = value;
            }
        }

        [DataMember]
        public Account AccountDetails
        {
            get
            {
                return this._AccountDetails;
            }
            set
            {
                this._AccountDetails = value;
            }
        }

        [DataMember]
        public string AccountName
        {
            get
            {
                return this._AccountName;
            }
            set
            {
                this._AccountName = value;
            }
        }

        [DataMember]
        public Amount LedgerBalance
        {
            get
            {
                return this._LedgerBalance;
            }
            set
            {
                this._LedgerBalance = value;
            }
        }
    }
}
