using System;
using System.Collections.Generic;
using System.Text;

namespace BankOneMobile.SwitchIntegration.ISO8583.Client.DTO
{
    public class ConfigurationResponse : MessageResponse
    {
        public ConfigurationResponse(Trx.Messaging.Message responseMessage, AppZoneSwitch.Core.Contracts.ITransaction sourceTrans, AppZoneSwitch.Core.Contracts.ITransaction originalTrans)
            : base(responseMessage, sourceTrans, originalTrans)
        {
           
        }
    }
}
