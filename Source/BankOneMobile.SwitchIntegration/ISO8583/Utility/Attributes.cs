﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BankOneMobile.SwitchIntegration.Utility
{
    /// <summary>
    /// This indicates that a class can be approved.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public sealed class  ApprovableClassAttribute :  Attribute
    {
        /// <summary>
        /// Constructor for specifying the type this attribute should be applied to.
        /// </summary>
        /// <param name="name">A friendly name of this class.</param>
        /// <param name="type">The type of the class.</param>
        public ApprovableClassAttribute(string name, Type type)
        {
            this.Name = name;
            this.Type = type;
        }

        /// <summary>
        /// Constructor for specifying the type this attribute should be applied to.
        /// </summary>
        /// <param name="type">The type of the class.</param>
        public ApprovableClassAttribute(Type type)
            : this(type.Name, type)
        {
        }

        public string Name { get; set; }

        public Type Type { get; set; }

    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class XMLNameAttribute : Attribute
    {
        /// <summary>
        /// Constructor for specifying the type this attribute should be applied to.
        /// </summary>
        /// <param name="name">A friendly name of this class.</param>
        public XMLNameAttribute(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }


    }

    /// <summary>
    /// Used by every method to specify the action to be taken on that method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public sealed class ApprovableMethodAttribute: Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">The name of the method.</param>
        /// <param name="action">The <see cref="Action"/> to be taken.</param>
        public ApprovableMethodAttribute(string name, Action action)
            : this(action)
        {
            this.Name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">The <see cref="Action"/> to be taken.</param>
        public ApprovableMethodAttribute(Action action)
        {
            this.Action = action;
        }

        public string Name { get; set; }

        public Action Action { get; set; }
    }

    public enum Action
    {
        INSERT,
        UPDATE,
        DELETE,
        ENABLEDISABLE
      
    }

    
}
