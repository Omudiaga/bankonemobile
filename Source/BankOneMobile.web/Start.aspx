﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void btnStart_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txtPhoneNo.Text))
        {
            new BankOneMobile.InfoBip.USSD.UssdManager().StartSession(txtPhoneNo.Text, "Testing our service");
            Response.Write("Session started!");
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Phone No:"></asp:Label>
        <asp:TextBox ID="txtPhoneNo" runat="server" Text="+2348034730365"></asp:TextBox>
        <asp:Button ID="btnStart" runat="server" onclick="btnStart_Click" 
            Text="Start Session" />
    
    </div>

    </form>
</body>
</html>
