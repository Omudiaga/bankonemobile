﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.CustomActions;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Data.Contracts;
using System.Diagnostics;
using BankOneMobile.Core.Helpers;

[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class BankOneMobileService : IBankOneMobileService
{
    private IAgentGeoLocationRepository _repository = SafeServiceLocator<IAgentGeoLocationRepository>.GetService();
    private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
    public bool SaveAgentGeoLocation(string agentPhoneNumber, string latitude, string longitude, out string errorMessage)
    {
        Agent agent = null;
        if (string.IsNullOrWhiteSpace(agentPhoneNumber))
        {
            errorMessage = "The Agent's Phone Number is Required";
            return false;
        }
        if (string.IsNullOrWhiteSpace(latitude))
        {
            errorMessage = "Latitude is Required";
            return false;
        }
        if (string.IsNullOrWhiteSpace(longitude))
        {
            errorMessage = "Longitude is Required";
            return false;
        }
        try
        {
            if (!MobileAccountSystem.CheckPhoneNumber(agentPhoneNumber))
            {
                errorMessage = string.Format("The Phone Number ({0}) is Invalid", agentPhoneNumber);
                return false;
            }
            agent = new AgentSystem().GetAgentByPhoneNumber(agentPhoneNumber);
            if (agent == null)
            {
                errorMessage = string.Format("The Phone Number {0} is not tied to any Agent", agentPhoneNumber);
                return false;
            }
            if (string.IsNullOrEmpty(agent.InstitutionCode))
            {
                errorMessage = "This Agent has no Institution Code";
                return false;
            }

            AgentGeoLocation agentGeoLocationToSave = new AgentGeoLocation
            {
                InstitutionCode = agent.InstitutionCode,
                AgentPhoneNumber = agentPhoneNumber,
                Latitude = latitude,
                Longitude = longitude,
                DateLogged = DateTime.Now
            };
            new AgentGeoLocationSystem().SaveAgentGeoLocation(_theDataSource, agentGeoLocationToSave);
            errorMessage = string.Empty;
        }

        catch (Exception ex)
        {
            errorMessage = ex.Message;
            return false;
        }
        return true;
    }
}