﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services.CoreBankingService;

[ServiceContract]
public interface IBankOneMobileService
{
    [OperationContract]
    bool SaveAgentGeoLocation(string agentPhoneNumber, string latitude, string longitude, out string errorMessage);
}