﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services.CoreBankingService;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMobileAccountService" in both code and config file together.
[ServiceContract]
public interface IMobileAccountService
{

    [OperationContract]
    bool SetRecievingAccount(string phoneNumber, string accountNumber);     
    [OperationContract]
    IList<LinkingBankAccount> GetAccountsForLinking(string phoneNumber);
    [OperationContract]
    IList<LinkingBankAccount> GetAccountsForLinkingWithActivationCode(string activationCode);
    [OperationContract]
    IList<LinkingBankAccount> GetLinkedAccounts(string phoneNumber);
    [OperationContract]
    LinkingBankAccount GetReceivingAccount(string phoneNumber);
    [OperationContract]
    MobileAccount GetMobileAccountByPhoneNumber(string phoneNumber);
    [OperationContract]
    MobileAccount GetMobileAccountByPhoneNumberAndInstitution(string phoneNumber, string institutionCode);
    [OperationContract]
    bool UpdatePinTries(string phoneNumber, bool IsReset);
    [OperationContract]
    MobileAccount ValidateMobileAccount(string phoneNumber, string accountNumber, string encryptedPinBlock,bool isPhoneNumber,out string acctNo);
    [OperationContract]
    MobileAccount  CreateMobileAccountFromAgent(string lastName,string firstName, string phoneNumber,string gender,BankOneMobile.Core.Implementations.Product prod,string  agentCode,bool isFundsTransfer);
    [OperationContract]
    bool ValidateReciever(string identifier, string institutionCode, bool isPhoneNumber, out string accountNumber);
    [OperationContract]
    Agent GetAgentByPhoneNumber(string phoneNumber);
    [OperationContract]
    LinkingBankAccount GetAccountByAccountNumber(string acctNumber, string insCode);
    [OperationContract]
       MobileAccount GetMobileAccountBy(string identifier, string instCode);
    [OperationContract]
    bool ChangePin(string phoneNumber, string newPinBlock);

    [FaultContract(typeof(BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException))]
    [FaultContract(typeof(BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException))]
    [FaultContract(typeof(BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException))]
    [FaultContract(typeof(BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException))]
    [OperationContract]
    MobileAccount CreateEnhancedMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender, BankOneMobile.Core.Implementations.Product prod, string agentCode, string startPackNo, string idNo, string nokPhone, string nokName, string address, string plceofBirth, string referalName, string referalNo, DateTime DateOfBirth, bool hasInfo, string accountSource, string otherAcctInfo, byte[] passport);   
     [OperationContract]
    MobileAccount CreateLafayetteMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender, string agentCode, string prod, string plceofBirth, DateTime DateOfBirth, string idNo, string nokPhone, string nokName, string address, string referalName, string referalPhone, bool hasSufficientSourceInfo, string infoSource, string otherSourcInfo, byte[] passport, string country, string state, string territory, string district, DateTime idExpDate, DateTime idIssuedDate);   
    [OperationContract]
     bool ValidateToken(string institutionCode, string mobilePhone, string enteredToken);
    [OperationContract]
    bool ValidateNonAccountHolderToken(string mobilePhone, string enteredToken);
    [OperationContract]
    bool ValidateDiamondBankNonAccountHolderToken(string originatorPhoneNumber, string beneficiaryPhoneNumber, decimal amountInNaira,
        string enteredToken, out string errorMessage, out long transactionID);
    [OperationContract]
    bool UpdateDiamondBankNonAccountHolderTransferStatus(long transactionID, out string errorMessage);


    [OperationContract]
    string GenerateToken(string  phoneNo, string  instCode);
    [OperationContract]
    string GenerateNonAccountHolderToken(string mobilePhone, string instCode);
    [OperationContract]
    Agent ValidateAgent( string mobileTellerCode);
    [OperationContract]
     Agent SaveAgent(Agent ag, out string errorMessage);

    [OperationContract]
    long PendingAccountsCount();

    [OperationContract]
    long FailedAccountsCount();

}
