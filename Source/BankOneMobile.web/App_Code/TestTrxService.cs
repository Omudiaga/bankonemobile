﻿using BankOneMobile.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TestTrxService" in code, svc and config file together.
[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class TestTrxService : ITestTrxService
{
	public void DoWork()
	{
        string actNo = "";
        BankOneMobile.Core.Implementations.MobileAccount mob = new BankOneMobile.Core.Implementations.MobileAccount { MobilePhone = "08034730365" };
        BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType tType = new BankOneMobile.Core.Implementations.CommercialBankFundsTransferTransactionType { AccountNumber = "", AgentCode = "001", Amount = 500, BankCode = "058", BankName = "GTB", BeneficiaryLastName = "", BeneficiaryOtherNames = "", BeneficiaryPhoneNumber = "", PaymentReference = "89765409", TheBankAccount = new BankOneMobile.Core.Implementations.LinkingBankAccount { BankAccount = "989800980" }, TheMobileAccount = mob, Token = "98767", TransactionReference = "9875786879" };
        string pin = "9898";
        new TransactionSystem().RunTransaction(pin, tType, mob, actNo, "", 500, new Guid().ToString());
	}
}
