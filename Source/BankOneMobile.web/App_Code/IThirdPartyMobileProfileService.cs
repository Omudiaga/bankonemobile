﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Services;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IThirdPartyMobileProfileService" in both code and config file together.
[ServiceContract]
public interface IThirdPartyMobileProfileService
{
    [OperationContract]
    bool CreateAccount(string PhoneNumber, string Lastname, string OtherNames, string Address, string Gender,
        string DateOfBirth_Format_ddMMyyyy, string InstitutionCode, out string accountStatus, out string accountNumber,
        byte[] Passport = null);

    [OperationContract]
    ThirdPartyMobileProfileSystem.Feedback InCreateAccount(ThirdPartyMobileProfileSystem.AccountDetails prospectAccount);


    [OperationContract]
    ThirdPartyMobileProfileSystem.Feedback EnableExistingAccountByAccountNumber(string AccountNumber,string InstitutionCode);

    /**
    [OperationContract]
    ThirdPartyMobileProfileSystem.Feedback EnableExistingAccountByPhoneNumber(string PhoneNumber, string InstitutionCode);
    **/

    [OperationContract]
    bool ConfirmAccountStatusOnCoreBanking(string PhoneNumber, string InstitutionCode);

    [OperationContract]
    bool ConfirmAccountStatusOnCoreBankingUsingAccount(string AccountNumber, string InstitutionCode);

    [OperationContract]
    bool ConfirmAccountStatusOnMobileProfile(string AccountNo, string InstitutionCode);

    [OperationContract]
    bool CheckBothAccountStatus(string AccountNo, string InstitutionCode);




}
