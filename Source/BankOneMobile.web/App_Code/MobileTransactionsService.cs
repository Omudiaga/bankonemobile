﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MobileTransactionsService" in code, svc and config file together.
//[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class MobileTransactionsService : IMobileTransactionsService
{
	public void DoWork()
	{
	}

    


    public IList<BankOneMobile.Core.Implementations.Session> GetDailySessions(DateTime dt)
    {
        IList<Session> toReturn = null;
        toReturn = new SessionSystem().GetSessionsForADay(dt);
        return toReturn;
    }

    
}
