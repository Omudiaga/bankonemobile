﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Utriilities
/// </summary>
public class Utilities
{
	public Utilities()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private const string SS_MFBCODE = "::SS_INSTITUTION_CODE::";
    //[System.Diagnostics.DebuggerStepThroughAttribute]
    public static string MFBCode
    {
        get
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                HttpContext.Current.Session[SS_MFBCODE] = HttpContext.Current.User.Identity.Name.Split(':')[1];
            }

            return Convert.ToString(HttpContext.Current.Session[SS_MFBCODE]);
        }
        set
        {
            HttpContext.Current.Session[SS_MFBCODE] = value;
        }
    }

    
}