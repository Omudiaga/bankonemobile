﻿using BankOneMobile.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LafayetteAutoReg" in code, svc and config file together.
public class LafayetteAutoReg : ILafayetteAutoReg
{
	public string  AutoRegisterCustomer( string cardPan, string acctNo,DateTime expDate)
	{
        string toRetrn = string.Empty;
        if (string.IsNullOrEmpty(cardPan))
        {
            return "No  Card PAN was supplied";
        }
        if (cardPan.Length <= 15)
        {
            return string.Format("Card PAN must be at least 16 Characters -{0} is only {1} characters", cardPan, cardPan.Length);
        }
        toRetrn = new TransactionSystem().AutoRegisterLafayetteCustomer(cardPan, expDate, acctNo);
        return toRetrn;
	}
}
