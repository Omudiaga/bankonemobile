﻿using BankOneMobile.Core.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AgentGeoLocationModel
/// </summary>
public class AgentGeoLocationModel
{
    public AgentGeoLocationModel()
        {
            //DateLogged= new DateTime(1900, 1, 1);
        }
        public string InstitutionCode { get; set; }
        public string AgentName { get; set; }
        public string AgentPhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string TransactionType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string DateLogged { get; set; }
        public long RegionID { get; set; }
        public long BranchID { get; set; }
        public string FirstBandInterval { get; set; }
        public string SecondBandInterval { get; set; }
        public string StartDate { get; set; }

}