﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportingService" in code, svc and config file together.
//[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class ReportingService : IReportingService
{
	

   


    public IList<Session> GetUSSDSessionsBetweenDates(DateTime fromDate, DateTime toDate)
    {
        IList<Session> toReturn = new List<Session>();
        toReturn = new SessionSystem().GetSessionBetweenDates(fromDate, toDate);
        return toReturn;

    }

    
}
