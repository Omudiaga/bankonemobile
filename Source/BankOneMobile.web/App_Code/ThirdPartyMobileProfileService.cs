﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Services;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services.CoreBanking;


[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ThirdPartyMobileProfileService" in code, svc and config file together.
public class ThirdPartyMobileProfileService : IThirdPartyMobileProfileService
{
	 public ThirdPartyMobileProfileService()
    {
        
	     
    }

     public bool CreateAccount(string PhoneNumber, string Lastname, string OtherNames, string Address, string Gender, string DateOfBirth_Format_ddMMyyyy, string InstitutionCode, out string accountStatus, out string accountNumber, byte[] Passport = null)
     {
         bool result = false;
            ThirdPartyMobileProfileSystem.AccountDetails newAccount = new ThirdPartyMobileProfileSystem.AccountDetails()
            {
                PhoneNumber = PhoneNumber,
                Lastname =Lastname,
                OtherNames=OtherNames,
                Address=Address,
                Gender = Gender,
                DateOfBirth_Format_ddMMyyyy =DateOfBirth_Format_ddMMyyyy,
                InstitutionCode = InstitutionCode,
                Passport=Passport
            };
            var accountStatusOnCorebanking = ThirdPartyMobileProfileSystem.ConfirmAccountExistOnCoreBanking(PhoneNumber, InstitutionCode);
            if (accountStatusOnCorebanking == true)
            {
                accountNumber = "0000000000";
                accountStatus = "Cannot Create Because Account Already Exist";
                
            }
            else
            {
                var fd = InCreateAccount(newAccount);
                accountNumber = fd.AccountNumber;
                accountStatus = fd.AccountStatus;
                if (fd.Status == true)
                {
                     result=true;
                }
            }
         return result;
;
     }

     public ThirdPartyMobileProfileSystem.Feedback InCreateAccount(ThirdPartyMobileProfileSystem.AccountDetails prospectAccount)
        {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return tSystem.CreateAccount(prospectAccount);
        }

     public ThirdPartyMobileProfileSystem.Feedback EnableExistingAccountByAccountNumber(string AccountNumber, string InstitutionCode)
        {

            var tSystem = new ThirdPartyMobileProfileSystem();
            return tSystem.EnableExistingAccountByAccountNumber(AccountNumber,InstitutionCode);
        }
    /**
     public ThirdPartyMobileProfileSystem.Feedback EnableExistingAccountByPhoneNumber(string PhoneNumber, string InstitutionCode)
     {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return tSystem.EnableExistingAccountByPhoneNumber(PhoneNumber, InstitutionCode);
     }
    **/
     public bool ConfirmAccountStatusOnCoreBanking(string PhoneNumber, string InstitutionCode)
     {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return tSystem.ConfirmAccountStatusOnCoreBanking(PhoneNumber, InstitutionCode);
     }
     public bool ConfirmAccountStatusOnCoreBankingUsingAccount(string AccountNumber, string InstitutionCode)
     {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return true;// tSystem.ConfirmAccountStatusOnCoreBankingByAccountNo(AccountNumber, InstitutionCode);
     }

     public bool ConfirmAccountStatusOnMobileProfile(string AccountNo, string InstitutionCode)
     {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return tSystem.ConfirmAccountStatusOnMobileProfile(AccountNo, InstitutionCode);
     }

     public bool CheckBothAccountStatus(string AccountNo, string InstitutionCode)
     {
         var tSystem = new ThirdPartyMobileProfileSystem();
         return tSystem.CheckBothAccountStatus(AccountNo, InstitutionCode);
     }



}
