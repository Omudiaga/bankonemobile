﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Services;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.CustomActions;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Data.Contracts;
using System.Diagnostics;
using BankOneMobile.Core.Helpers;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MobileAccountService" in code, svc and config file together.
[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class MobileAccountService : IMobileAccountService
{
    //IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared, Guid.NewGuid());
    //MobileAccountSystem sys = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared, _uniqueID));
    private IAgentRepository _repository = SafeServiceLocator<IAgentRepository>.GetService();
    private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
    public string  LinkAccounts(string phoneNumber,string activationCode)
    {
        string toReturn = string.Empty;
        bool linked = false;
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                linked = new MobileAccountSystem(_theDataSource).LinkAccounts(phoneNumber, activationCode);
            }
            toReturn = "E00; Account Succesfully Linked";
        }
        catch (InvalidPhoneNumberException)
        {
            toReturn = "E01;Phone Number is Invalid";
        }
        catch (InvalidActivationCodeException)
        {
            toReturn = "E02; Activation Code is Invalid";
        }
        catch (Exception ex)
        {
            toReturn =string.Format("E03; System Error-{0}",ex.Message);
        }
       
     
        return toReturn;
    }
    public bool SetRecievingAccount(string phoneNumber, string accountNumber)
    {
        bool result = false;
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            result = new MobileAccountSystem(_theDataSource).SetRecievingAccount(phoneNumber, accountNumber);
        }
        return result;
    }
    public IList<LinkingBankAccount> GetAccountsForLinking(string phoneNumber)
    {
        IList<ILinkingBankAccount> toReturn = new List<ILinkingBankAccount>();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new MobileAccountSystem(_theDataSource).GetAccountsForLinking(phoneNumber);
            }
        }
        catch(InvalidPhoneNumberException)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = "E01; Invalid PhoneNumber" });
        }
        catch (ApplicationException ex)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = string.Format("E01;{0}",ex.Message) });
        }
        return toReturn as IList<LinkingBankAccount>;
    }
    public IList<LinkingBankAccount> GetAccountsForLinkingWithActivationCode(string activationCode)
    {
        IList<ILinkingBankAccount> toReturn = new List<ILinkingBankAccount>();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new MobileAccountSystem(_theDataSource).GetAccountsForLinking(activationCode);
            }
        }
        catch (InvalidActivationCodeException)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = "E01; Invalid PhoneNumber" });
        }
        catch (ApplicationException ex)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = string.Format("E01;{0}", ex.Message) });
        }
        return toReturn as IList<LinkingBankAccount>;
    }
    public IList<LinkingBankAccount> GetLinkedAccounts(string phoneNumber)
    {
        IList<ILinkingBankAccount> toReturn = new List<ILinkingBankAccount>();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new MobileAccountSystem(_theDataSource).GetAccountsForLinking(phoneNumber);
            }
        }
        catch (InvalidPhoneNumberException)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = "E01; Invalid PhoneNumber" });
        }
        catch (ApplicationException ex)
        {
            toReturn.Add(new LinkingBankAccount { BankAccount = string.Format("E01;{0}", ex.Message) });
        }
        return toReturn as IList<LinkingBankAccount>;
    }
    public LinkingBankAccount GetReceivingAccount(string phoneNumber)
    {
        ILinkingBankAccount toReturn = new LinkingBankAccount();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new MobileAccountSystem(_theDataSource).GetRecievingAccount(phoneNumber);
            }
        }
        catch (InvalidPhoneNumberException)
        {
            toReturn = new LinkingBankAccount { BankAccount = "E01; Invalid PhoneNumber" };
        }
        catch (ApplicationException ex)
        {
            toReturn = new LinkingBankAccount { BankAccount = string.Format("E01;{0}", ex.Message) };
        }
        return toReturn as LinkingBankAccount;
    }
    public LinkingBankAccount GetAccountByAccountNumber(string acctNumber, string instCode)
    {
        ILinkingBankAccount toReturn = new LinkingBankAccount();
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            toReturn = new MobileAccountSystem(_theDataSource).GetBankAccountByAcccountNo(acctNumber, instCode);
        }
        toReturn.TheMobileAccount = null;
        return toReturn as LinkingBankAccount;
    }
    public MobileAccount GetMobileAccountByPhoneNumber(string phoneNumber)
    {
        MobileAccount mob = new MobileAccount();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(phoneNumber);
            }
        }
        catch (InvalidPhoneNumberException)
        {
            mob = null;
        }
        return mob;

    }
    public MobileAccount GetMobileAccountByPhoneNumberAndInstitution(string phoneNumber, string institutionCode)
    {
        MobileAccount mob = new MobileAccount();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                mob = new MobileAccountSystem(_theDataSource).GetByPhoneNumber(phoneNumber, institutionCode);
            }
        }
        catch (InvalidPhoneNumberException)
        {
            mob = null;
        }
        return mob;

    }
    public bool UpdatePinTries(string phoneNumber, bool IsReset)
    {
        bool toReturn = false;
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            var system = new MobileAccountSystem(_theDataSource);
            MobileAccount mob = system.GetByPhoneNumber(phoneNumber);

            if (mob == null)
            {
                return false;
            }

            if (IsReset)
            {
                mob.PinTries = 0;
            }
            else
            {
                mob.PinTries++;
            }
            system.UpdateMobileAccount(mob);
        }
        toReturn = true;
        return toReturn;
    }
    public MobileAccount ValidateMobileAccount(string phoneNumber, string accountNumber, string encryptedPinBlock,bool isPhoneNumber,out string acctNo)
    {
        acctNo = string.Empty;
        MobileAccount mob = new MobileAccount();
        MobileAccount TestMobileAccount = null;
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            var system = new MobileAccountSystem(_theDataSource);
            if (isPhoneNumber)
            {
                MobileAccount phoneAccountTestMobileAccount =
                 system.GetByPhoneNumber(phoneNumber);
                if (phoneAccountTestMobileAccount == null)
                {
                    mob.DisplayMessage = "E02:Invalid Sending Phone Number";
                    return mob;
                }
                else
                {
                    accountNumber = phoneAccountTestMobileAccount.BankAccounts[0].BankAccount;
                    acctNo = accountNumber;
                }

            }
            try
            {
                SystemConfig config = new SystemConfigSystem(_theDataSource).GetConfig();


                TestMobileAccount = system.GetByPhoneNumber(phoneNumber);

                if (TestMobileAccount == null)
                {
                    mob.DisplayMessage = "E01:Invalid Phone Number";
                    return mob;
                }
                if (system.GetBankAccountByAcccountNo(accountNumber, TestMobileAccount.RecievingBankAccount.InstitutionCode) == null)
                {
                    mob.DisplayMessage = "E02:Invalid Account Number";
                    return mob;
                }
                else if (TestMobileAccount.BankAccounts == null || TestMobileAccount.BankAccounts.Count == 0)
                {
                    mob.DisplayMessage = "E03:Mobile Account has no linked accounts";
                    return mob;
                }
                else if (!TestMobileAccount.BankAccounts.Select(x => x.BankAccount).Contains(accountNumber))
                {
                    mob.DisplayMessage = "E04:Account Number not linked to Mobile Account";
                    return mob;
                }
                //else if (mob.RecievingBankAccount == null)
                //{
                //    mob.DisplayMessage = "E05:Mobile Account does not have default account number";
                //    return mob;
                //}
                else if (TestMobileAccount.MobileAccountStatus != MobileAccountStatus.Active)
                {
                    mob.DisplayMessage = "E06:Mobile Account is not active";
                    return mob;
                }
                else if (TestMobileAccount.PinTries > config.MaximumPinTries)
                {
                    mob.DisplayMessage = "E07:Mobile Account Locked.Pin Tries Exceeded";
                    return mob;
                }
                else if (!new HSMCenter().VerifyPinOnHSM(phoneNumber, TestMobileAccount.PIN, encryptedPinBlock, AppZone.HsmInterface.PinBlockFormats.ANSI))
                {
                    mob.DisplayMessage = "E08:Invalid Pin";
                    return mob;
                }
                else
                {
                    if (TestMobileAccount.PinTries > 3)

                        mob = TestMobileAccount;
                    mob.DisplayMessage = String.Format("{0}:{1}", "00", TestMobileAccount.RecievingBankAccount == null ? TestMobileAccount.BankAccounts[0].BankAccount : TestMobileAccount.RecievingBankAccount.BankAccount);
                    acctNo = TestMobileAccount.BankAccounts[0].BankAccount;
                    TestMobileAccount.DisplayMessage = "00:" + acctNo;
                    return TestMobileAccount;
                }
            }
            catch (Exception EX)
            {
                return new MobileAccount { DisplayMessage = EX.Message };
            }
        }
    }
    public  MobileAccount CreateMobileAccountFromAgent(string lastName,string firstName, string phoneNumber, string  gender,Product prod,string  agentCode,bool isFundsTransfer)
    {
        MobileAccount toReturn = new MobileAccount();
        //using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        //{
        //    Agent agent = new AgentSystem(_theDataSource).GetByCode(agentCode) as Agent;
        //     toReturn= new MobileAccountSystem().CreateMobileAccountFromAgent(lastName,firstName,phoneNumber,gender,prod,agent,isFundsTransfer);        
        //}
        return toReturn;
    }
    public MobileAccount CreateEnhancedMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender, Product prod, string agentCode,string startPackNo,string idNo,string nokPhone,string nokName,string address, string plceofBirth,string referalName,string referalNo,DateTime DateOfBirth, bool hasInfo,string accountSource,string otherAcctInfo, byte[] passport)
    {
        MobileAccount toReturn = new MobileAccount();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                Agent agent = new AgentSystem(_theDataSource).GetByCode(agentCode) as Agent;
                toReturn = new MobileAccountSystem(_theDataSource).CreateEnhancedMobileAccountFromAgent(lastName, firstName, phoneNumber, gender, prod, agent, startPackNo, idNo, nokPhone, nokName, address, plceofBirth, DateOfBirth, referalName, referalNo, hasInfo, accountSource, otherAcctInfo, passport);
            }
        }
        catch (AlreadyExistingAccountException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException>(
                new BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (AlreadyRegisterdCustomerException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException>(
                new BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (NoHSMResponseException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException>(
                new BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (CoreBankingWebServiceException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException>(
                new BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException(ex.Message)
                , new FaultReason(ex.Message));
        }
        return toReturn;
    }
    public Agent GetAgentByPhoneNumber(string phoneNumber)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            Agent ag = new AgentSystem(_theDataSource).GetAgentByPhoneNumber(phoneNumber);
            return ag;
        }
    }
    public  bool ValidateReciever(string identifier, string institutionCode,bool isPhoneNumber, out string accountNumber)
    {
        MobileAccount toMob = null;
        bool toReturn = false;
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            var system = new MobileAccountSystem(_theDataSource);
            accountNumber = string.Format("{0}:{1}", "E01:Invalid", isPhoneNumber ? "Invalid Phone Number" : "Invalid Account Number");
            if (isPhoneNumber)
            {
                toMob = system.GetByPhoneNumber(identifier, institutionCode);
                if (toMob != null)
                {
                    var bankAccount = toMob.BankAccounts.FirstOrDefault(l => l.InstitutionCode == institutionCode);
                    if (bankAccount != null)
                    {
                        toReturn = true;
                        accountNumber = bankAccount.BankAccount;
                    }
                }

            }
            else
            {
                toMob = system.GetByAccountNumber(identifier, institutionCode);
                if (toMob != null)
                {
                    toReturn = true;
                    accountNumber = identifier;
                }
            }
        }
        return toReturn;
    }
  
    public MobileAccount GetMobileAccountBy(string identifier, string instCode)
    {
        ILinkingBankAccount toReturn = new LinkingBankAccount();
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            toReturn = new MobileAccountSystem(_theDataSource).GetBankAccountByAcccountNo(identifier, instCode);
        }
        return toReturn!=null?toReturn.TheMobileAccount as MobileAccount:null;
    }
    public bool ChangePin(string phoneNumber, string newPinBlock)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            return new MobileAccountSystem(_theDataSource).ChangePin(phoneNumber, newPinBlock);
        }
    }
     public bool ValidateToken(string institutionCode, string  mobilePhone, string enteredToken)
    {
        bool  toReturn = false;
//        string message = String.Format("MObile PhONE -{0}, tOKEN-{1}",mobilePhone,enteredToken);
      //  PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new TokenSystem(_theDataSource).ValidateToken(institutionCode, mobilePhone, enteredToken);
            }
        }
        catch(Exception ex)
        {
            throw new InvalidTokenException(ex.Message);
        }
        return toReturn;
    }
    public bool ValidateNonAccountHolderToken(string mobilePhone, string enteredToken)
    {
        bool toReturn = false;
        //        string message = String.Format("MObile PhONE -{0}, tOKEN-{1}",mobilePhone,enteredToken);
        //  PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                toReturn = new TokenSystem(_theDataSource).ValidateNonAccountHolderToken(mobilePhone, enteredToken);
            }
        }
        catch (Exception ex)
        {
            throw new InvalidTokenException(ex.Message);
        }
        return toReturn;
    }

    public bool ValidateDiamondBankNonAccountHolderToken(string originatorAccountNumber, string beneficiaryPhoneNumber,decimal amountInNaira, 
        string enteredToken, out string errorMessage, out long transactionID)
    {
        PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to validate dbn non account holder token. Sender: {0}|Receiver: {1}|amount: {2}|Token: {3}", originatorAccountNumber, beneficiaryPhoneNumber, amountInNaira, enteredToken)));
        List<IDBNFundsTransferTransactionType> tranType = null;
        Dictionary<string, object> propertyValuePairs = new Dictionary<string, object>();
        //propertyValuePairs.Add("AgentPhoneNumber", mob.MobilePhone);
        propertyValuePairs.Add("OriginatorAccountNumber", originatorAccountNumber);
        propertyValuePairs.Add("BeneficiaryAccountNumber", beneficiaryPhoneNumber);
        propertyValuePairs.Add("SessionID", enteredToken);
        propertyValuePairs.Add("ResponseCode", "Pending");
        propertyValuePairs.Add("Amount", amountInNaira);
        propertyValuePairs.Add("DBNFundsTransferType", DBNFundsTransferTransactionTypeName.NonAccountHolder);
        try
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to find dbn non account holder transaction.")));
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                tranType = new DBNFundsTransferTransactionTypeSystem().FindFundsTransferCashInTransactionTypes(_theDataSource, propertyValuePairs);
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Found dbn non account holder transaction.....{0} found",tranType.Count)));
            if (tranType == null || tranType.Count == 0)
            {
                errorMessage = string.Format("Mobile: There is no pending transfer from {0} to {1} with the entered secret token and amount: {2}", originatorAccountNumber, beneficiaryPhoneNumber, amountInNaira);
                transactionID = 0;
                return false;
            }
            else
            {
                errorMessage = string.Empty;
                transactionID = tranType.FirstOrDefault().ID;
                return true;
            }                
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Exception trying to validate dbn non account holder transaction token. {0}",ex.Message)));
            errorMessage = ex.Message;
            transactionID = 0;
            return false;
        }
            
    }

    public bool UpdateDiamondBankNonAccountHolderTransferStatus(long transactionID, out string errorMessage)
    {
       if(transactionID <= 0)
       {
           errorMessage = "Mobile: Transaction ID is required";
           return false;
       }
       PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to update dbn non account holder transaction with ID: {0}", transactionID)));
       try
       {
           IDBNFundsTransferTransactionType tranType = null;
           using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               tranType = new DBNFundsTransferTransactionTypeSystem().GetDBNFundsTransferTransactionType(_theDataSource, transactionID);
           }
           if (tranType == null)
           {
               errorMessage = "Mobile: Invalid transaction ID";
               return false;
           }
           tranType.StatusMessage = DBNNonAccountCashCollectionStatus.Successful;
           tranType.ResponseCode = "00";
           tranType.ResponseDate = DateTime.Now;
           //tranType.TheTransaction.Status = TransactionStatus.Successful;
           PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to update dbn non account holder transaction with ID: {0}", transactionID)));
           using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
           {
               new DBNFundsTransferTransactionTypeSystem().UpdateDBNFundsTransferTransactionType(_theDataSource, tranType as DBNFundsTransferTransactionType);
           }
           PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Done updating dbn non account holder transaction with ID: {0}", transactionID)));
           errorMessage = string.Empty;
           return true;  
       }
       catch (Exception ex)
       {
           PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Exception when trying to update dbn non account holder transaction with ID: {0}|{1}", transactionID,ex.ToString())));
           errorMessage = string.Format("Mobile: {0}", ex.Message);
           return false;
       }
    }
    public string GenerateToken(string  mobilePhone, string  instCode)
    {
        string toReturn = string.Empty;
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                IToken token = new TokenSystem(_theDataSource).GenerateToken(mobilePhone, instCode);
                toReturn = token.TokenValue;
            }
        }
        catch (Exception ex)
        {
            toReturn = ex.Message;
        }
        return toReturn;
    }
    public string GenerateNonAccountHolderToken(string mobilePhone, string instCode)
    {
        string toReturn = string.Empty;
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                IToken token = new TokenSystem(_theDataSource).GenerateNonAccountHolderToken(mobilePhone, instCode);
                toReturn = token.TokenValue;
            }
        }
        catch (Exception ex)
        {
            toReturn = ex.Message;
        }
        return toReturn;
    }
    public Agent ValidateAgent(string mobileTellerCode)
    {
        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
        {
            Agent ag = new AgentSystem(_theDataSource).GetAgentByCode(mobileTellerCode);
            return ag;
        }
    }

    public Agent SaveAgent(Agent ag, out string errorMessage)
    {

       
        Agent toReturn = null;
        errorMessage = string.Empty;
        if (ag == null)
        {
            errorMessage = "Agent Is Null";
            return toReturn;
        }
         if (string.IsNullOrEmpty(ag.AccountNumber))
        {
            errorMessage = "Agent Account Number Is Compulsory";
            return toReturn;
        }
        if (string.IsNullOrEmpty(ag.InstitutionCode))
        {
            errorMessage = "Agent Institution Code Is Compulsory";
            return toReturn;
        }
        //if (string.IsNullOrEmpty(ag.PersonalPhoneNumber))
        // {
        //     errorMessage = "Personal Phone Number Is Compulsory";
        //     return toReturn;
        // }
        if (string.IsNullOrEmpty(ag.PhoneNumber))
        {
            errorMessage = "Phone Number Is Compulsory";
            return toReturn;
        }
        if (ag.TheGender==null)
        {
            errorMessage = "Gender Is Compulsory";
            return toReturn;
        }
        if (!MobileAccountSystem.CheckPhoneNumber(ag.PhoneNumber))
        {
            errorMessage= string.Format("The Phone Number ({0}) is Invalid", ag.PhoneNumber);
        }
        
        try
        {
            Agent agentToSave = new AgentSystem(_theDataSource).SaveAgentExposedForCreditClubWebService(ag);
            toReturn = new AgentSystem(_theDataSource).SaveOrUpdateAgentViaWebService(agentToSave);
        }
        
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return toReturn;
    }









    public MobileAccount CreateLafayetteMobileAccountFromAgent(string lastName, string firstName, string phoneNumber, string gender, string agentCode, string prod, string plceofBirth, DateTime DateOfBirth, string idNo, string nokPhone, string nokName, string address, string referalName, string referalPhone, bool hasSufficientSourceInfo, string infoSource, string otherSourcInfo, byte[] passport, string country, string state, string territory, string district, DateTime idExpDate, DateTime idIssuedDate)
    {
        string msg = String.Format("CFL Inside Create Service {0}", prod);
        Trace.TraceInformation(msg);
        Agent agent = null;
        MobileAccount toReturn = new MobileAccount();
        try
        {
            using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
            {
                agent = new AgentSystem(_theDataSource).GetAgentByCode(agentCode);
                string startPackNo = string.Empty;
               // string referalNo = string.Empty;
                //Agent agent = new AgentSystem(_theDataSource).GetByCode(agentCode) as Agent;
                if (agent == null)
                {
                    Trace.TraceInformation("CFL Agent is Null");

                }
                else
                {
                    Trace.TraceInformation("CFL Agent is {0}", agent.LastName);
                }
                Trace.TraceInformation("App code referal phone is {0}", referalPhone);
                toReturn = new MobileAccountSystem(_theDataSource).CreateLafayetteMobileAccountFromAgent(lastName, firstName, phoneNumber, gender,  prod , agent.Code, startPackNo, idNo, nokPhone, nokName, address, plceofBirth, DateOfBirth, referalName, referalPhone, hasSufficientSourceInfo, infoSource, otherSourcInfo, passport, country, state, territory, district, idExpDate, idIssuedDate);// TODO Fix Product


            }
        }
        catch (AlreadyExistingAccountException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException>(
                new BankOneMobile.Core.Exceptions.WebService.AlreadyExistingAccountException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (AlreadyRegisterdCustomerException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException>(
                new BankOneMobile.Core.Exceptions.WebService.AlreadyRegisterdCustomerException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (NoHSMResponseException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException>(
                new BankOneMobile.Core.Exceptions.WebService.NoHSMResponseException(ex.Message)
                , new FaultReason(ex.Message));
        }
        catch (CoreBankingWebServiceException ex)
        {
            throw new FaultException<BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException>(
                new BankOneMobile.Core.Exceptions.WebService.CoreBankingWebServiceException(ex.Message)
                , new FaultReason(ex.Message));
        }
        return toReturn;
    }


    public IAgent CheckAgentByPhoneNumber(string phoneNumber)
    {
        MobileAccount mob = new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
        if (mob == null)
        {
            return null;
        }

        else
        {
            Agent agent = _repository.GetAgentByPhoneNumber(_theDataSource, phoneNumber) as Agent;

            //_repository.DbContext.Close(_theDataSource);
            return agent;
        }


    }

    public long PendingAccountsCount()
    {
        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
        {
            var pendingAccounts = new CustomerRegistrationSystem(source).GetPendingCustomerRegistrations();
            return pendingAccounts.Count;
        }
        
    }

    public long FailedAccountsCount()
    {
        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
        {
            var failedAccounts = new CustomerRegistrationSystem(source).GetFailedCustomerRegistrations();
            return failedAccounts.Count;
        }
    }


}
