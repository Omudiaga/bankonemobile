﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FlowService" in code, svc and config file together.
[System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
public class FlowService : IFlowService
{
    private FlowSystem _flowSystem = new FlowSystem(DataSourceFactory.GetDataSource(DataCategory.Shared));
    #region IFlowService Members


    public long SaveFlow(IFlow theFlow)
    {
        try
        {
            _flowSystem.SaveFlow(theFlow);
        }
        catch  (Exception ex)
        {
            string errorMsg = String.Format("Error Saving Flow - {0}, Inner Message Is {1}, Inner Stack Trace is {2}",ex.Message,ex.InnerException==null?"No Inner Exception":ex.InnerException.Message,ex.InnerException==null?"No Inner Exception":ex.InnerException.StackTrace);
        }
        return theFlow.ID;
    }

    public List<IFlow> GetAllFlows()
    {
        return _flowSystem.GetAllFlows();
    }

    public void UpdateFlow(IFlow theFlow)
    {
        try
        {
            _flowSystem.UpdateFlow(theFlow);
        }
        catch (Exception ex)
        {
            string errorMsg = String.Format("Error Saving Flow - {0}, Inner Message Is {1}, Inner Stack Trace is {2}", ex.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.Message, ex.InnerException == null ? "No Inner Exception" : ex.InnerException.StackTrace);
        }
    }

    public IFlow GetFlowById(long id)
    {
        return _flowSystem.GetFlow(id);
    }

    #endregion
}
