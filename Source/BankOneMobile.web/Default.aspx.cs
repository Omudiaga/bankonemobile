﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using BankOneMobile.CustomerInfo.Interface;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        System.Diagnostics.Trace.TraceInformation("Trace Just Began");
        BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
        // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
        //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
        ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
        ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
        ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
        ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());


        System.Diagnostics.Trace.TraceInformation("Initialised Properties");

        ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
        ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
        ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
        ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
        System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
        BankOneMobile.Extension.ApplicationInitializer.Init();

        AppZoneSwitch.Services.RoutingSystem.SystemError += (o, a) =>
            {
                System.Diagnostics.Trace.TraceError(a.TheException.ToString());
            };
        AppZoneSwitch.Services.RoutingSystem.SystemInitialize += (o, a) =>
         {
             AppZoneSwitch.Services.Implementations.TransactionSplitter.InitializeEngine();
             AppZoneSwitch.Services.ReversalSystem.InitializeEngine();
             AppZoneSwitch.Services.ResyncSystem.InitializeEngine();
             AppZoneSwitch.Services.SwitchConnectionSystem.InitializeEngine();

             //TransactionLogSystem.InitializeEngine();







         };
        AppZoneSwitch.Services.RoutingSystem.InitializeEngine();
        //new PANE.ERRORLOG.Error().LogToFile(new Exception("Init Successfull"));
        System.Diagnostics.Trace.TraceInformation("Init Succesfull");
             
    }
}