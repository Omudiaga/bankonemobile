﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using BankOneMobile.CustomActions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class HttpJavaClient_MobileService : System.Web.UI.Page
{
    string response;
    Guid SessionID;

    private const string SS_OPERATION_SS = "Operation";
    private const string SS_AGENTPHONENUMBER_SS = "Agent Phone Number";
    private const string SS_LONGITUDE_SS = "Longitude";
    private const string SS_LATITUDE_SS = "Latitude";
    private const string SS_CUSTOMER_PHONE_NUMBER_SS = "Customer Phone Number";
    private const string SS_AMOUNT_SS = "Amount";
    private const string SS_AGENT_PIN_SS = "Agent Pin";
    private const string SS_SESSION_ID_SS = "SESSION_ID";

    static Regex MobileCheck = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
    static Regex MobileVersionCheck = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

    public static bool fBrowserIsMobile()
    {
        Debug.Assert(HttpContext.Current != null);

        if (HttpContext.Current.Request != null && HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();

            if (u.Length < 4)
                return false;

            if (MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4)))
                return true;
        }

        return false;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!fBrowserIsMobile())
        {
            //Response.Write("Your device is not allowed the view this site");
            //return;
        }
        //PANE.ERRORLOG.ErrorLogger.Log(new Exception("@@@@@@@@@ Starting Agent Geo Location Now @@@@@@@@@"));
        string operation = Request.Params["OPERATION"];
        string agentPhoneNumber = Request.Params["AGENT_PHONE_NUMBER"];
        string longitude = Request.Params["LONGITUDE"];
        string latitude = Request.Params["LATITUDE"];
        string paddingString = Request.Params["USE_PADDING"];
        string customerPhoneNumber = Request.Params["CUSTOMER_PHONE_NUMBER"];
        string amount = Request.Params["AMOUNT"];
        string pin = Request.Params["AGENT_PIN"];
        string sessionID = Request.Params["SESSION_ID"];

        bool usePadding = string.IsNullOrEmpty(paddingString) ? true : Convert.ToBoolean(paddingString);

        CryptoUtil.CryptoServices crypt = new CryptoUtil.CryptoServices();
        if (!usePadding) crypt = new CryptoUtil.CryptoServices(usePadding);

        if (string.IsNullOrEmpty(operation))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_OPERATION_SS), true);
            return;
        }

        if (string.IsNullOrEmpty(agentPhoneNumber))
        {
            ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_AGENTPHONENUMBER_SS), true);
            return;
        }

        if (string.IsNullOrEmpty(longitude))
        {
            ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_LONGITUDE_SS), true);
            return;
        }

        if (string.IsNullOrEmpty(latitude))
        {
            ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_LATITUDE_SS), true);
            return;
        }

        string mssg = string.Format("Encrypted message received- AgentPhone:{0}|Operation:{1}|Latitude:{2}|Longitude:{3}", agentPhoneNumber, operation, latitude, longitude);
        //PANE.ERRORLOG.ErrorLogger.Log(new Exception(mssg));

        try
        {
            operation = crypt.BCDecrypt(operation);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }

        
        try
        {
            agentPhoneNumber = crypt.BCDecrypt(agentPhoneNumber);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }


        if (operation.ToLower() != "geo_location" && operation.ToLower() != "offline_deposit")
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_OPERATION_SS), true);
            return;
        }

        

        try
        {
            longitude = crypt.BCDecrypt(longitude);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            latitude = crypt.BCDecrypt(latitude);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }



        switch (operation.ToLower())
        {
            //NOW check verification before calling web service
            case "geo_location":
                #region Periodic Agent Geo Location
                
                mssg = string.Format("After decrypting- AgentPhone:{0}|Longitude:{1}|Latitude:{2}", agentPhoneNumber, longitude, latitude);
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(mssg));


                string errorMessage = string.Empty;
                Agent agent = null;
                try
                {
                    if (!MobileAccountSystem.CheckPhoneNumber(agentPhoneNumber))
                    {
                        errorMessage = string.Format("The Phone Number ({0}) is Invalid", agentPhoneNumber);
                        ReturnResponse(errorMessage, true);
                    }
                    agent = new AgentSystem().GetAgentByPhoneNumber(agentPhoneNumber);
                    if (agent == null)
                    {
                        errorMessage = string.Format("The Phone Number {0} is not tied to any Agent", agentPhoneNumber);
                        ReturnResponse(errorMessage, true);
                    }
                    if (string.IsNullOrEmpty(agent.InstitutionCode))
                    {
                        errorMessage = "This Agent has no Institution Code";
                        ReturnResponse(errorMessage, true);
                    }

                    AgentGeoLocation agentGeoLocationToSave = new AgentGeoLocation
                    {
                        InstitutionCode = agent.InstitutionCode,
                        AgentPhoneNumber = agentPhoneNumber,
                        Latitude = latitude,
                        Longitude = longitude,
                        DateLogged = DateTime.Now
                    };
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        new AgentGeoLocationSystem().SaveAgentGeoLocation(source, agentGeoLocationToSave);
                    }
                    ReturnResponse("Success", true);
                    return;
                }

                catch (Exception ex)
                {
                    ReturnResponse(ex.Message, true);
                    return;
                }
                #endregion
            case "offline_deposit":
                #region Offline Deposit
                if (string.IsNullOrEmpty(customerPhoneNumber))
                {
                    ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_CUSTOMER_PHONE_NUMBER_SS), true);
                    return;
                }
                if (string.IsNullOrEmpty(amount))
                {
                    ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_AMOUNT_SS), true);
                    return;
                }
                if (string.IsNullOrEmpty(pin))
                {
                    ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_AGENT_PIN_SS), true);
                    return;
                }
                if (string.IsNullOrEmpty(sessionID))
                {
                    ReturnResponse(string.Format("Invalid Request Parameter -{0}", SS_SESSION_ID_SS), true);
                    return;
                }

                try
                {
                    customerPhoneNumber = crypt.BCDecrypt(customerPhoneNumber);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                try
                {
                    amount = crypt.BCDecrypt(amount);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                try
                {
                    pin = crypt.BCDecrypt(pin);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                try
                {
                    sessionID = crypt.BCDecrypt(sessionID);
                }
                catch (Exception ex)
                {
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                mssg = string.Format("**OFFLINE DEP** After decrypting- AgentPhone:{0}|CustomerPhone:{1}|Amount:{2}|Pin:{3}|Session ID:{4}|Longitude:{5}|Latitude:{6}", agentPhoneNumber, customerPhoneNumber, amount, pin, sessionID, longitude, latitude);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(mssg));

                string instCode = string.Empty;
                MobileAccount mob = new MobileAccountSystem().GetByPhoneNumber(agentPhoneNumber);

                if (mob == null)
                {
                    errorMessage = string.Format("There is no account for this Agent with Phone Number ({0})", agentPhoneNumber);
                    ReturnResponse(errorMessage, true);
                    return;
                }
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Retrieved Agent Mobile Account. MobilePinTries:{0} | PhoneNumber: {1}|{2}|{3}|{4}|{5}|{6}", mob.PinTries,  mob.MobilePhone, mob.ProductCode,mob.ProductName,mob.Verified,mob.LastName,mob.OtherNames)));

                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Mobile account is not null"));
                MobileAccount custMob = new MobileAccountSystem().GetByPhoneNumber(customerPhoneNumber);
                if (custMob == null)
                {
                    errorMessage = string.Format("There is no account for this Customer with Phone Number ({0})", customerPhoneNumber);
                    ReturnResponse(errorMessage, true);
                    return;
                }

                if (custMob.BankAccounts == null)
                {
                    errorMessage = string.Format("There is no linked account for this Customer with Phone Number ({0})", customerPhoneNumber);
                    ReturnResponse(errorMessage, true);
                    return;
                }
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Customer Mobile account is not null"));
                if (custMob.BankAccounts.Count > 1)
                {
                    errorMessage = string.Format("Could not retrieve unique result for Customer with Phone Number ({0})", customerPhoneNumber);
                    ReturnResponse(errorMessage, true);
                    return;
                }
                PANE.ERRORLOG.ErrorLogger.Log(new Exception("Customer Mobile account has 1 linked account"));
                Agent theAgent = new AgentSystem().GetAgentByPhoneNumber(agentPhoneNumber);
                if (theAgent == null)
                {
                    errorMessage = string.Format("This Phone Number ({0}) does not belong to an agent", agentPhoneNumber);
                    ReturnResponse(errorMessage, true);
                    return;
                }
                SystemConfig config = new SystemConfigSystem().GetConfig();

                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("MobilePinTries:{0} | ConfigPinTries:{1} | PhoneNumber: {2}", mob.PinTries, config.MaximumPinTries, mob.MobilePhone)));
                if (mob.PinTries > config.MaximumPinTries)
                {
                    errorMessage = string.Format("Your Mobile Account has been Locked. Pin Tries Exceeded");
                    ReturnResponse(errorMessage, true);
                    return;
                }

                string encrytedBlock = GenerateEncryptedPinBlock2(agentPhoneNumber, pin);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Real Check-{0}", "Starting Pin Check-{0}", agentPhoneNumber)));
                if (!new HSMCenter().VerifyPinOnHSM(agentPhoneNumber, mob.PIN, encrytedBlock, AppZone.HsmInterface.PinBlockFormats.ANSI))
                {
                    mob.PinTries++;
                    new MobileAccountSystem().UpdateMobileAccount(mob);
                    errorMessage = "Invalid Pin";
                    ReturnResponse(errorMessage, true);
                    return;
                }

                string mmg = string.Format("Offline dep values: agent Acc:{0}|customer Acc:{1}|", theAgent.TheAgentAccount.BankAccount, custMob.BankAccounts[0].BankAccount);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(mmg));

                try
                {
                    IOfflineDepositTransactionType offlineDeposit = null;
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        offlineDeposit = new OfflineDepositTransactionTypeSystem().GetUnsuccessfulOfflineDepositBySessionID(source, sessionID);
                    }
                    if (offlineDeposit == null)
                    {
                        offlineDeposit = new OfflineDepositTransactionType
                        {
                            InstitutionCode = mob.InstitutionCode,
                            SessionID = sessionID,
                            AgentPhoneNumber = agentPhoneNumber,
                            Amount = amount,
                            CustomerPhoneNumber = customerPhoneNumber,
                            DateLogged = DateTime.Now,
                            Status = TransactionStatus.Pending
                        };
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                        {
                            offlineDeposit = new OfflineDepositTransactionTypeSystem().Save(source, offlineDeposit as OfflineDepositTransactionType);
                        }
                        if (offlineDeposit == null)
                        {
                            errorMessage = "Could not save transaction";
                            ReturnResponse(errorMessage, true);
                            return;
                        }
                    }



                    string narration = theAgent.InstitutionCode == System.Configuration.ConfigurationManager.AppSettings["DBNInstitutionCodeForNarration"] ? String.Format("Cash Dep via BETA Friend/{0} {1}", theAgent.LastName, theAgent.OtherNames) : String.Format("Cash Deposit @ {0} ({1})", theAgent.IsMobileTeller ? "Mobile Teller" : "Agent", theAgent.LastName + " , " + theAgent.OtherNames);
                    //string debit = theAgent.InstitutionCode == System.Configuration.ConfigurationManager.AppSettings["DBNInstitutionCodeForNarration"] ? String.Format("Cash Dep by BETA Friend/{0} {1}", theAgent.LastName, theAgent.OtherNames) : String.Format("Cash Deposit by {0}, ({1}) ", custMob.BankAccounts[0].CoreBankingNames, custMob.BankAccounts[0].TheMobileAccount.MobilePhone);
                    //string narration = String.Format("{0}||{1}", debit, credit);

                    string xmlRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
                                                            <CashDepositRequest>
	                                                            <InstitutionCode>{0}</InstitutionCode>
	                                                            <AgentCode>{1}</AgentCode>
	                                                            <CustomerAccountNumber>{2}</CustomerAccountNumber>
	                                                            <ReferenceNumber>{3}</ReferenceNumber>
	                                                            <Amount>{4}</Amount>
	                                                            <Narration>{5}</Narration>
                                                            </CashDepositRequest>

                                                 ", mob.InstitutionCode, (theAgent as Agent).AccountNumber, custMob.BankAccounts[0].BankAccount,
                                                  sessionID, amount, narration);
                   string response = string.Empty;
                   try
                   {
                       using (var client = new SwitchingServiceRef.SwitchingServiceClient())
                       {
                           response = client.BankOneCashDepositPostingFromAgent(xmlRequest);
                       }
                   }
                   catch (Exception ex)
                   {
                       PANE.ERRORLOG.ErrorLogger.Log(new Exception("Error from corebanking service"));
                       PANE.ERRORLOG.ErrorLogger.Log(ex);
                       errorMessage = string.Format("An Error Occured");
                       ReturnResponse(errorMessage, true);
                       return;
                   }
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Done with deposit. Response: {0}", response)));
                    XmlDocument xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(response);
                    }
                    catch (Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                        errorMessage = string.Format("Error. Response was not in the correct format");
                        ReturnResponse(errorMessage, true);
                        return;
                    }
                    bool status = bool.Parse(xmlDoc.SelectSingleNode("/CashDepositResponse/Status").InnerText);
                    string statusMessage = xmlDoc.SelectSingleNode("/CashDepositResponse/StatusMessage").InnerText;
                    if(!status)
                    {
                        offlineDeposit.Status = TransactionStatus.Failed;
                        offlineDeposit.DateLogged = DateTime.Now;
                        try
                        {
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                offlineDeposit = new OfflineDepositTransactionTypeSystem().Update(source, offlineDeposit as OfflineDepositTransactionType);
                            }
                        }
                        catch(Exception ex)
                        {
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Error trying to update offline deposit to failed."));
                            PANE.ERRORLOG.ErrorLogger.Log(ex);
                        }
                        errorMessage = string.Format("Deposit Was Not Successful. Reason: {0}", statusMessage);
                        ReturnResponse(errorMessage, true);
                        return;
                    }
                    else
                    {
                        AgentGeoLocationTransactionType agentGeoLocation = new AgentGeoLocationTransactionType
                        {
                            InstitutionCode = mob.InstitutionCode,
                            AgentPhoneNumber = agentPhoneNumber,
                            CustomerPhoneNumber = customerPhoneNumber,
                            TransactionType = TransactionTypeName.OfflineDeposit,
                            Latitude = latitude,
                            Longitude = longitude,
                            DateLogged = DateTime.Now
                        };
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to save offline geo location"));
                        try
                        {
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                new AgentGeoLocationTransactionTypeSystem().SaveAgentGeoLocationTransactionType(source, agentGeoLocation);
                            }
                        }
                        catch(Exception ex)
                        {
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Error trying to save offline deposit geo location."));
                            PANE.ERRORLOG.ErrorLogger.Log(ex);
                        }

                        offlineDeposit.Status = TransactionStatus.Successful;
                        offlineDeposit.DateLogged = DateTime.Now;
                        try
                        {
                            using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                            {
                                offlineDeposit = new OfflineDepositTransactionTypeSystem().Update(source, offlineDeposit as OfflineDepositTransactionType);
                            }
                        }
                        catch (Exception ex)
                        {
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Error trying to update offline deposit to Successful."));
                            PANE.ERRORLOG.ErrorLogger.Log(ex);
                        }

                        errorMessage = "Deposit Successful";
                        ReturnResponse(errorMessage, true);
                        return;
                    
                    }
                }
                catch (Exception ex)
                {
                    string msg = string.Format("Offline Deposit Was Successful but an exception was thrown while saving the offline deposit");
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(mssg));
                    PANE.ERRORLOG.ErrorLogger.Log(ex);
                }
                errorMessage = "Successful";
                ReturnResponse(errorMessage, true);
                return;
                #endregion
        }
    }

    public static string GenerateEncryptedPinBlock2(string mobilePhone, string pin)
    {
        mobilePhone = mobilePhone.PadLeft(12, '0');
        string encryptionKey = System.Configuration.ConfigurationManager.AppSettings["OTP_ENCRYPTION_KEY"];// "268397495B7C61672FF16B2F16973485";
        string toReturn = string.Empty;
        string pinLength = pin.Length.ToString();
        string clearPinValue = "0" + pinLength + pin + "FFFFFFFFFF";

        string clearPhone = "0000" + mobilePhone;
        toReturn = ThalesSim.Core.Utility.XORHexStringsFull(clearPinValue, clearPhone);
        //Trace.TraceInformation(toReturn);
        toReturn = ThalesSim.Core.Cryptography.TripleDES.TripleDESEncrypt(new ThalesSim.Core.Cryptography.HexKey(encryptionKey.Substring(0, 32)), toReturn);
        return toReturn;
    }
    private void ReturnResponse(string response, bool shouldClose)
    {
        //response = "<Response><Display><BeforeMenu>Before</BeforeMenu><Menu><MenuItem Index='1'>First</MenuItem><MenuItem Index='2'>Second</MenuItem></Menu></Display><ShouldMask>0</ShouldMask></Response>";
        //response = response.Replace("\r\n", "").Replace("\n", "");
        string value = "<Response><Menu>$MENU$</Menu><ShouldClose>$SHOULDCLOSE$</ShouldClose></Response>";
        value = value.Replace("$MENU$", response).Replace("$SHOULDCLOSE$", shouldClose ? "1" : "0");
        response = value.Replace("\r\n", "\n");
        response = new CryptoUtil.CryptoServices().BCEncrypt(response);
        Response.ClearHeaders();
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
        Response.ClearContent();
        Response.Write(response);
        if (Response.IsClientConnected)
        {
            Response.Flush();
        }
    }
}