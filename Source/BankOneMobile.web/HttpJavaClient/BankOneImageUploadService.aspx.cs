﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HttpJavaClient_BankOneImageUploadService : System.Web.UI.Page
{
    private const string SS_SESSION_ID_SS = "SESSION_ID";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("beginning of image upload"));
            System.Diagnostics.Trace.TraceInformation("beginning of image upload");
            string sessionID = Request.Params["SESSION_ID"];
            if (string.IsNullOrEmpty(sessionID))
            {
                throw new Exception(string.Format("Invalid Request Parameters -{0}", SS_SESSION_ID_SS));
            }
            HttpPostedFile file = Request.Files[0];
            byte[] imgData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                imgData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("Gotten image bytes. " + imgData.Length));
            System.Diagnostics.Trace.TraceInformation("Gotten image bytes.");
            MemoryStream ms = new MemoryStream(imgData);
            //MemoryStream ms = new MemoryStream(imgData, 0, imgData.Length);
            //ms.Position = 0;
            //ms.Write(imgData, 0, imgData.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to save image"));
            System.Diagnostics.Trace.TraceInformation("About to save image");
            string path = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("ImageUploadPath") ? 
                System.Configuration.ConfigurationManager.AppSettings["ImageUploadPath"] : "C:\\";
            string filename = string.Format(@"{0}{1}.png", path, sessionID.Replace("+", "").Trim());
            if (File.Exists(filename))
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("{0} exists. deleting...", filename)));
                File.Delete(filename);
            }
            image.Save(filename);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(filename));
            System.Diagnostics.Trace.TraceInformation("Image saved.");
            //Response.redi
        }
        catch(Exception ex)
        {

            PANE.ERRORLOG.ErrorLogger.Log(ex);
            System.Diagnostics.Trace.TraceError(ex.Message);
        }
    }
}