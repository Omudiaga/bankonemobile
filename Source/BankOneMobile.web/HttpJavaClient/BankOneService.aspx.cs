﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BankOneMobile.Services;
using BankOneMobile.Services.WorkflowServices;
using System.Text.RegularExpressions;
using System.Diagnostics;
using BankOneMobile.Data.Implementations;

public partial class HttpJavaClient_BankOneService : System.Web.UI.Page
{
    //global::menuRequestParams @params;
    string  response;
    Guid SessionID;

    private const string SS_OPERATION_SS = "Operation";
    private const string SS_MSISDN_SS = "MSISDN";
    private const string SS_TEXT_SS = "Text";
    private const string SS_EXIT_CODE_SS = "EXIT_CODE";
    private const string SS_EXIT_REASON_SS = "EXIT_REASON";
    private const string SS_EXIT_CODE_SPECIFIED_SS = "EXIT_CODE_SPECIFIED";
    private const string SS_SESSION_ID_SS = "SESSION_ID";
    private const string SS_ACTIVATION_CODE_SS = "ACTIVATION_CODE";
    private const string SS_VERIFICATION_CODE_SS = "VERIFICATION_CODE";
    private const string SS_GEO_LOCATION_SS = "GEO_LOCATION";

    static Regex MobileCheck = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
    static Regex MobileVersionCheck = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

    public static bool fBrowserIsMobile()
    {
        Debug.Assert(HttpContext.Current != null);

        if (HttpContext.Current.Request != null && HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();

            if (u.Length < 4)
                return false;

            if (MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4)))
                return true;
        }

        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!fBrowserIsMobile())
        {
            //Response.Write("Your device is not allowed the view this site");
            //return;
        }
        PANE.ERRORLOG.ErrorLogger.Log(new Exception("Startig Now. Just Entered"));
        new PANE.ERRORLOG.Error().LogToFile(new Exception("Startig Now. Just Entered"));
        string operation = Request.Params["OPERATION"];
        string sessionID = Request.Params["SESSION_ID"];
        string reason = Request.Params["EXIT_REASON"];
        string exitCode = Request.Params["EXIT_CODE"];
        string useXML = Request.Params["USE_XML"];
        string paddingString = Request.Params["USE_PADDING"];
        string text = Request.Params["TEXT"];
        string msisdn = Request.Params["MSISDN"];
        string activationCode = Request.Params["ACTIVATION_CODE"];
        string useVerificationCode = Request.Params["USE_VERIFICATION_CODE"];
        string verificationCode = Request.Params["VERIFICATION_CODE"];
        string geoLocation = Request.Params["GEO_LOCATION"];
    
        bool exitCodeSpecified =Convert.ToBoolean(Request.Params["EXIT_CODE_SPECIFIED"]);
        bool useXMLValue = string.IsNullOrEmpty(useXML) ? false : Convert.ToBoolean(Request.Params["USE_XML"]);
        //Determines whether to use padding while decrypting
        //Used for the PhoneGap Android and Blackberry apps
        //Default is true if query parameter is not set
        bool usePadding = string.IsNullOrEmpty(paddingString) ? true : Convert.ToBoolean(paddingString);
        bool useVerificationCodeValue = string.IsNullOrEmpty(useVerificationCode) ? false : Convert.ToBoolean(useVerificationCode);

        string msg = string.Format("operation :{0} ,sess:{1},usexml:{2}, text:{3},msisdn:{4},activationCode:{5},geoLocation:{6}", operation,sessionID,useXML,text,msisdn,activationCode,geoLocation);
       // PANE.ERRORLOG.ErrorLogger.Log(new Exception(msg));
        new PANE.ERRORLOG.Error().LogToFile(new Exception(msg));
        CryptoUtil.CryptoServices crypt = new CryptoUtil.CryptoServices();
        if (!usePadding) crypt = new CryptoUtil.CryptoServices(usePadding);
        //string decMSISDN = crypt.DecryptData(msisdn);
        //string decActivationCode = crypt.DecryptData(activationCode);
        //string decOperation = crypt.DecryptData(operation);
        //string decSessionID = crypt.DecryptData(sessionID);
        

        if (string.IsNullOrEmpty(operation))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_OPERATION_SS),true);
            return;
        }
        //new PANE.ERRORLOG.Error().LogToFile(new Exception(msisdn + "Midway through"));
         if (string.IsNullOrEmpty(sessionID))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_SESSION_ID_SS),true);
            return;
        }
             
        else if (string.IsNullOrEmpty(msisdn))
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_MSISDN_SS),true);
            return;
        }
        try
        {
         operation = crypt.BCDecrypt(operation);
        }
        catch(Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
         try
         {
             text = crypt.BCDecrypt(text);
         }
         catch (Exception ex)
         {
             PANE.ERRORLOG.ErrorLogger.Log(ex);
         }
        try
        {
         activationCode = crypt.BCDecrypt(activationCode);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            sessionID = crypt.BCDecrypt(sessionID);
            
        }

        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            msisdn = crypt.BCDecrypt(msisdn);

        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            verificationCode = crypt.BCDecrypt(verificationCode);

        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
        try
        {
            geoLocation = crypt.BCDecrypt(geoLocation);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }


         if (operation.ToLower() != "start" && operation.ToLower() != "next" && operation.ToLower() != "activation")
        {
            ReturnResponse(string.Format("Invalid Request Parameters -{0}",SS_OPERATION_SS),true);
            return;
        }
         if (useVerificationCodeValue && string.IsNullOrEmpty(verificationCode))
         {
             ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
             return;
         }
         string msgsss = string.Format(" After Decryption ::operation :{0} ,sess:{1},usexml:{2}, text:{3},msisdn:{4},activationCode:{5},geoLocation:{6}", operation, sessionID, useXML, text, msisdn, activationCode,geoLocation);
         PANE.ERRORLOG.ErrorLogger.Log(new Exception(msgsss));
         msisdn = ConvertToStandard(msisdn);
         switch (operation.ToLower())
        {
            //NOW check verification before calling web service
            case "start":
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Got here..onstart")));
                new PANE.ERRORLOG.Error().LogToFile(new Exception(string.Format("Got here..onstart")));
                if (useVerificationCodeValue)
                {
                    bool verified = new TpaWebService().ConfirmActicationCode(msisdn, verificationCode, false);
                    if (!verified)
                    {
                        ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
                        return;
                    }
                }
                BankOneServiceReference.menuResponseParams rr = null;
                using (BankOneServiceReference.UssdThirdPartyWsDemo client = new BankOneServiceReference.UssdThirdPartyWsDemo())
                {
                    client.Timeout = 200000;
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} about to call START BankOneService.{1}", sessionID, DateTime.Now)));
                    try
                    {
                        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                        {
                            var reqSystem = new MobileRequestSystem(_theDataSource);
                            var request = LogRequest(reqSystem, sessionID, operation, text);
                            rr = client.onSessionStart
                                (sessionID.ToString(), new BankOneServiceReference.menuRequestParams()
                                {
                                    msisdn = msisdn,
                                    text = text,
                                    geoLocation = geoLocation,
                                    optional = useXMLValue.ToString()
                                });
                            @response = rr.ussdMenu;
                            if (request != null)
                            {
                                LogResponse(reqSystem, request,@response);
                            }
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} out of START BankOneService.{1}", sessionID, DateTime.Now)));
                        }
                    }
                    catch(Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\n{2}\n{3}\nSessionID:{4}", ex.Message, ex.Source, ex.StackTrace,
                            ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION", sessionID);
                        BankOneMobile.Services.Utility.Utility.SendErrorMail("", "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile session service Error: {0}", ex.Message), body);
                        ReturnResponse("An error occurred.", true);
                        return;
                    }
                }
                ReturnResponse(@response, rr.shouldClose);
                return;


            case "next":
                if (useVerificationCodeValue)
                {
                    bool verified = new TpaWebService().ConfirmActicationCode(msisdn, verificationCode, false);
                    if (!verified)
                    {
                        ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_VERIFICATION_CODE_SS), true);
                        return;
                    }
                }
                if (string.IsNullOrEmpty(text))
                {
                    ReturnResponse(string.Format("Invalid Request Parameters -{0}", SS_TEXT_SS),true);
                    return;
                }
                BankOneServiceReference.menuResponseParams rsp = null;
                using (BankOneServiceReference.UssdThirdPartyWsDemo cl = new BankOneServiceReference.UssdThirdPartyWsDemo())
                {
                    cl.Timeout = 200000;
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} about to call NEXT BankOneService.{1}", sessionID, DateTime.Now)));
                    try
                    {
                        using (var _theDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                        {
                            var reqSystem = new MobileRequestSystem(_theDataSource);
                            var request = LogRequest(reqSystem, sessionID, operation, text);
                            rsp = cl.onUserResponse(sessionID.ToString(), new BankOneServiceReference.menuRequestParams()
                            {
                                msisdn = msisdn,
                                text = text,
                                geoLocation = geoLocation,
                                optional = useXMLValue.ToString()
                            });
                            if (request != null)
                            {
                                LogResponse(reqSystem, request, rsp.ussdMenu);
                            }
                            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("SessionID: {0} out of NEXT BankOneService.{1}", sessionID, DateTime.Now)));
                        }
                    }
                    catch (Exception ex)
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(ex);
                        string emails = "aomogbai@appzonegroup.com;pdureke@appzonegroup.com";
                        if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("SupportEmails"))
                        {
                            emails = System.Configuration.ConfigurationManager.AppSettings["SupportEmails"];
                        }
                        string body = string.Format("{0}\n{1}\n{2}\n{3}\nSessionID:{4}", ex.Message, ex.Source, ex.StackTrace,
                            ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION", sessionID);
                        BankOneMobile.Services.Utility.Utility.SendErrorMail("", "donotreply@mybankone.com",
                            emails, string.Format("BankOneMobile session service Error: {0}", ex.Message), body);
                        ReturnResponse("An error occurred.", true);
                        return;
                    }
                    if (rsp.shouldClose)
                    {
                        cl.onSessionEnd(sessionID, 200, true, "Ended Normally");
                        ReturnResponse(rsp.ussdMenu,rsp.shouldClose);
                        return;
                    }
                    else
                    {
                        ReturnResponse(rsp.ussdMenu, rsp.shouldClose);
                        return;
                    }                   

                  
                    
                }
                
            case "activation":
                {
                    if (string.IsNullOrEmpty(activationCode))
                    {
                        ReturnResponse("Invalid Parameters -Activation Code",true);
                        return;
                    }
                   // new PANE.ERRORLOG.Error().LogToFile(new Exception("msisdn is " + msisdn));
                  bool activated = new TpaWebService().ConfirmActicationCode(msisdn, activationCode, true);
                 // new PANE.ERRORLOG.Error().LogToFile(new Exception("msisdn is " + msisdn));
                    ReturnResponse(activated?"1":"0",true);

                    break;
                }
            default:
                {
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Got here..default")));
                    new PANE.ERRORLOG.Error().LogToFile(new Exception(string.Format("Got here..default")));
                    System.Diagnostics.Trace.TraceInformation("Got here..default");
                    break;
                }
        }
         
    }

    BankOneMobile.Core.Implementations.MobileRequest LogRequest(MobileRequestSystem reqSystem, string sessionID, string operation, string text)
    {
        try
        {
            var request = new BankOneMobile.Core.Implementations.MobileRequest
            {
                SessionID = sessionID,
                RequestTime = DateTime.Now,
                Operation = operation, Request = String.IsNullOrEmpty(text) ?"":text.Length == 4 ? "****" : text
            };
            reqSystem.Save(request);
            return request;
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
            return null;
        }
    }

    void LogResponse(MobileRequestSystem reqSystem, BankOneMobile.Core.Implementations.MobileRequest request, string resp)
    {
        try
        {
            request.RequestResponse = resp;
            request.ResponseTime = DateTime.Now;
            reqSystem.Update(request);
        }
        catch (Exception ex)
        {
            PANE.ERRORLOG.ErrorLogger.Log(ex);
        }
    }

    string ConvertToStandard(string msisdn)
    {        
        if (msisdn.StartsWith("08"))
        {
            msisdn = msisdn.Substring(3);
            msisdn = "+23480" + msisdn;
        }
        if (msisdn.StartsWith("07"))
        {
            msisdn = msisdn.Substring(3);
            msisdn = "+23470" + msisdn;
        }
        if (!msisdn.StartsWith("+"))
        {
            msisdn = "+" + msisdn;
        }
        return msisdn;
    }


    private void ReturnResponse(string  response,bool shouldClose)
    {
        //response = "<Response><Display><BeforeMenu>Before</BeforeMenu><Menu><MenuItem Index='1'>First</MenuItem><MenuItem Index='2'>Second</MenuItem></Menu></Display><ShouldMask>0</ShouldMask></Response>";
        //response = response.Replace("\r\n", "").Replace("\n", "");
        string value = "<Response><Menu>$MENU$</Menu><ShouldClose>$SHOULDCLOSE$</ShouldClose></Response>";
        value = value.Replace("$MENU$", response).Replace("$SHOULDCLOSE$",shouldClose?"1":"0");
      response = value.Replace("\r\n", "\n");
      response = new CryptoUtil.CryptoServices().BCEncrypt(response);      
     Response.ClearHeaders();
     HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
      Response.ClearContent(); 
      Response.Write(response);
      if (Response.IsClientConnected)
      {
          Response.Flush();
      }
    }
}