﻿<%@ WebHandler Language="C#" Class="Imager" %>

using System;
using System.Web;

public class Imager : IHttpHandler {

    

     private const string ACCOUNT_IDENTIFIER = "ACCOUNT_ID";
     private const string IMAGE_TYPE = "IMAGE_TYPE";

     
    

    public void ProcessRequest(HttpContext context)
    {


        string entityID = context.Request.Params[ACCOUNT_IDENTIFIER].ToLower();
        string imageType = context.Request.Params[IMAGE_TYPE].ToLower(); 

        if (String.IsNullOrEmpty(entityID))
        {
           return; 
        }
        if (String.IsNullOrEmpty(imageType))
           
        {
            return;
        }
        else if (imageType.ToLower()!="passport"&&imageType.ToLower()!="signature")
        {
            return;
        }

        byte[] bytes = null;
        using (var source = BankOneMobile.Data.Implementations.DataSourceFactory.GetDataSourceWithUniqueKey
            (BankOneMobile.Data.Implementations.DataCategory.Core))
        {
            bytes = new BankOneMobile.Services.LinkingBankAccountSystem(source).GetAccountImage(entityID, imageType);
        }
            if (bytes != null)
            {
                context.Response.ContentType = "image/jpeg";
                context.Response.OutputStream.Write(bytes, 0, bytes.Length);
                context.Response.Flush();
            }
            else
            {
                return;
            }      
    }



    public bool IsReusable
    {
        get { throw new NotImplementedException(); }
    }
}