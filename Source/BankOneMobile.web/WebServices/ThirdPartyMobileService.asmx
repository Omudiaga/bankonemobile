﻿<%@ WebService Language="C#" Class="ThirdPartyMobileService" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using BankOneMobile.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ThirdPartyMobileService  : System.Web.Services.WebService {

    [WebMethod]
    public bool CreateAccount(string PhoneNumber, string Lastname, string OtherNames, string Address, string Gender, string DateOfBirth_Format_ddMMyyyy, string InstitutionCode, byte[] Passport, out string accountNumber, out string accountStatus)
    {
        bool result = false;
        ThirdPartyMobileProfileSystem.AccountDetails newAccount = new ThirdPartyMobileProfileSystem.AccountDetails()
        {
            PhoneNumber = PhoneNumber,
            Lastname = Lastname,
            OtherNames = OtherNames,
            Address = Address,
            Gender = Gender,
            DateOfBirth_Format_ddMMyyyy = DateOfBirth_Format_ddMMyyyy,
            InstitutionCode = InstitutionCode,
            Passport = Passport
        };
        var accountStatusOnCorebanking = ThirdPartyMobileProfileSystem.ConfirmAccountExistOnCoreBanking(PhoneNumber, InstitutionCode);
         accountNumber = "";
         accountStatus = "";
        if (accountStatusOnCorebanking == true)
        {
            accountNumber = "0000000000";
            accountStatus = "Cannot Create Because Account Already Exist";

        }
        else
        {
            var fd = InCreateAccount(newAccount);
            accountNumber = fd.AccountNumber;
            accountStatus = fd.AccountStatus;
            if (fd.Status == true)
            {
                result = true;
            }
        }
        return result;
        
    }

    public ThirdPartyMobileProfileSystem.Feedback InCreateAccount(ThirdPartyMobileProfileSystem.AccountDetails prospectAccount)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return tSystem.CreateAccount(prospectAccount);
    }

    
    [WebMethod]
    public ThirdPartyMobileProfileSystem.Feedback EnableExistingAccountByAccountNumber(string AccountNumber, string InstitutionCode)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return tSystem.EnableExistingAccountByAccountNumber(AccountNumber, InstitutionCode);
    }
    
    [WebMethod]
    public bool ConfirmAccountStatusOnCoreBanking(string PhoneNumber, string InstitutionCode)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return tSystem.ConfirmAccountStatusOnCoreBanking(PhoneNumber, InstitutionCode);
    }

    [WebMethod]
    public bool ConfirmAccountStatusOnCoreBankingUsingAccount(string AccountNumber, string InstitutionCode)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return true;// tSystem.ConfirmAccountStatusOnCoreBankingByAccountNo(AccountNumber, InstitutionCode);
    }
    
    [WebMethod]
    public bool ConfirmAccountStatusOnMobileProfile(string AccountNo, string InstitutionCode)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return tSystem.ConfirmAccountStatusOnMobileProfile(AccountNo, InstitutionCode);
    }

    [WebMethod]
    public bool CheckBothAccountStatus(string AccountNo, string InstitutionCode)
    {
        var tSystem = new ThirdPartyMobileProfileSystem();
        return tSystem.CheckBothAccountStatus(AccountNo, InstitutionCode);
    }
    
    
}