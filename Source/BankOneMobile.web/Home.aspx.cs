﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coolite.Ext.Web;
using System.Web.Security;

public partial class _Home : System.Web.UI.Page
{
    protected void LogoutUser(object sender, AjaxEventArgs e)
    {
        FormsAuthentication.SignOut();
        Session.Abandon();
        FormsAuthentication.RedirectToLoginPage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       /* if (!Ext.IsAjaxRequest && !Page.IsPostBack)
        {
            FunctionsMembershipUser membershipUser = Membership.GetUser() as FunctionsMembershipUser;
            if(membershipUser != null)
            {
                btnName.Text = membershipUser.UserName.Split(':')[0];
            }
        }*/


        if (!IsPostBack)
        {
            //Default SiteMap Section
            LoadSiteMap(SiteMap.Provider, "House");
            StatusBar2.DefaultText = string.Format("Copyright &copy; {0} {1}. Powered by AppZone. Version {2}", 
                System.Configuration.ConfigurationManager.AppSettings["CopyRightYear"],
                System.Configuration.ConfigurationManager.AppSettings["CopyRightName"], 
                System.Configuration.ConfigurationManager.AppSettings["Version"]);
        }

       // this.ScriptManager1.SetTheme(Coolite.Ext.Web.Theme.Default);
    }

    private void LoadSiteMap(SiteMapProvider provider, string iconName)
    {
        TreePanel tp = new TreePanel();
        tp.SingleExpand = true;
        tp.RootVisible = false;
        tp.Title = "AppZoneSwitch"; //TODO: Remove for providers -  provider.Name;
        tp.Icon = (Icon)Enum.Parse(typeof(Icon), iconName, true);
        tp.Header = true; 
        tp.AutoScroll = true;

        //At this point, the top folders should open up when clicked, while the other ones can cause
        //a page to load.
        tp.Listeners.Click.Handler = /* String.Format("if(node.attributes.href){{e.stopEvent();if(!node.leaf)node.toggle(); if(node.leaf) loadPage(node, '{0}', 'icon-{1}');}}", provider.Name, iconName.ToLower());
        tp.Listeners.ContextMenu.Handler=*/ String.Format("if(!node.leaf)node.toggle();if(node.attributes.href){{e.stopEvent(); if(node.leaf) loadNewPage(node, '{0}', 'icon-{1}');}}", provider.Name, iconName.ToLower());
            /*tp.Listeners.DblClick.Handler =
                String.Format("if(node.attributes.href){{e.stopEvent();if(!node.leaf)node.toggle();if(node.leaf) reLoadPage(node, '{0}', 'icon-{1}');}}",
                              provider.Name, iconName.ToLower());*/
        
        PageTreeLoader ptl = new PageTreeLoader();
        ptl.NodeLoad+=new PageTreeLoader.NodeLoadEventHandler(LoadPages);
        ptl.BaseParams.Add(new Coolite.Ext.Web.Parameter("providerName", provider.Name));
        tp.Loader.Add(ptl);
        SiteMapNode siteNode = provider.RootNode;
        Coolite.Ext.Web.TreeNodeBase root = this.CreateNode(siteNode);
        root.Expanded = true;
        tp.Root.Add(root);
        accSidePanel.Items.Add(tp);
    }

    protected Coolite.Ext.Web.Theme TheTheme
    {
        get
        {
            if (Session[":::Theme:::"] == null)
            {
                Session[":::Theme:::"] = this.ScriptManager1.Theme;
            }
            return (Coolite.Ext.Web.Theme)Session[":::Theme:::"];
        }
        set
        {
            Session[":::Theme:::"] = value;
        }
    }
    protected void theme_click(object sender, AjaxEventArgs e)
    {
        switch (e.ExtraParams["Param"])
        {
            case "Gray":
                TheTheme = Coolite.Ext.Web.Theme.Gray;
                break;
            case "Slate":
                TheTheme = Coolite.Ext.Web.Theme.Slate;
                break;
            case "Default":
                TheTheme = Coolite.Ext.Web.Theme.Default;
                break;
        }
        e.ExtraParamsResponse.Add(new Coolite.Ext.Web.Parameter("success", this.ScriptManager1.GetThemeUrl(TheTheme)));
        //this.ScriptManager1.SetTheme(TheTheme);
    }
    [AjaxMethod]
    public void Notify(string title, string message)
    {
        
        Ext.Net.Notification.Show(new Ext.Net.NotificationConfig
        {
            Title = title,
            Icon =Ext.Net.Icon.Information,
            Draggable = true,
            Modal = false,
            Html = message
        });
    }

    [AjaxMethod]
    public void RedirectToLoginPage()
    {
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    [AjaxMethod]
    public string SetPageTheme(string theme)
    {
        switch (theme)
        {
            case "Gray":
                TheTheme = Coolite.Ext.Web.Theme.Gray;
                break;
            case "Slate":
                TheTheme = Coolite.Ext.Web.Theme.Slate;
                break;
            case "Default":
                TheTheme = Coolite.Ext.Web.Theme.Default;
                break;
        }
        this.ScriptManager1.SetTheme(TheTheme);
        return this.ScriptManager1.GetThemeUrl(TheTheme);
    }

    [AjaxMethod]
    public string ChangeTheme()
    {
        switch (TheTheme)
        {
            case Coolite.Ext.Web.Theme.Slate:
            case Coolite.Ext.Web.Theme.Default:
                TheTheme = Coolite.Ext.Web.Theme.Gray;
                break;
            case Coolite.Ext.Web.Theme.Gray:
                TheTheme = Coolite.Ext.Web.Theme.Slate;
                break;
        }
        this.ScriptManager1.SetTheme(TheTheme);
        return this.ScriptManager1.GetThemeUrl(TheTheme);
    }
    //page tree node loader handler
    protected void LoadPages(object sender, NodeLoadEventArgs e)
    {
        string providerName = e.ExtraParams["providerName"];
        if (!string.IsNullOrEmpty(e.NodeID))
        {
            SiteMapNode siteMapNode = SiteMap.Providers[providerName].FindSiteMapNodeFromKey(e.NodeID);
         
            SiteMapNodeCollection children = siteMapNode.ChildNodes;
            if (children != null && children.Count > 0)
            {
                foreach (SiteMapNode mapNode in children)
                {               
                    //if(new SOA.Framework.SiteMapHelper.ManagedSiteMapProvider().IsAccessibleToUser(System.Web.HttpContext.Current,mapNode))     
                    e.Nodes.Add(this.CreateNodeWithOutChildren(mapNode));
                }
            }
        }
    }

    //dynamic node creation
    private Coolite.Ext.Web.TreeNodeBase CreateNodeWithOutChildren(SiteMapNode siteMapNode)
    {
        Coolite.Ext.Web.TreeNodeBase treeNode;

        if (siteMapNode.ChildNodes != null && siteMapNode.ChildNodes.Count > 0)
        {
            treeNode = new Coolite.Ext.Web.AsyncTreeNode();
        }
        else
        {
            treeNode = new Coolite.Ext.Web.TreeNode();
            treeNode.Leaf = true;
        }

        if (!string.IsNullOrEmpty(siteMapNode.Url))
        {
            treeNode.Href = string.Format("{0};{1}",this.Page.ResolveUrl(siteMapNode.Url), String.IsNullOrEmpty(siteMapNode.ResourceKey)? "-":siteMapNode.ResourceKey);
        }

        treeNode.NodeID = siteMapNode.Key;
        treeNode.Text = siteMapNode.Title;
        treeNode.Qtip = siteMapNode.Description;        
        return treeNode;
    }

    //static node creation with children
    private Coolite.Ext.Web.TreeNode CreateNode(SiteMapNode siteMapNode)
    {
        Coolite.Ext.Web.TreeNode treeNode = new Coolite.Ext.Web.TreeNode();
        if (siteMapNode.ChildNodes != null && siteMapNode.ChildNodes.Count > 0)
        {
            treeNode.Leaf = false;
        }
        else
        {
            treeNode.Leaf = true;
        }

        if (!string.IsNullOrEmpty(siteMapNode.Url))
        {
            treeNode.Href = string.Format("{0};{1}", this.Page.ResolveUrl(siteMapNode.Url), String.IsNullOrEmpty(siteMapNode.ResourceKey) ? "-" : siteMapNode.ResourceKey);
        }
        treeNode.NodeID = siteMapNode.Key;
        treeNode.Text = siteMapNode.Title;
        treeNode.Qtip = siteMapNode.Description;
        if (!string.IsNullOrEmpty(siteMapNode.ResourceKey))
        {
            treeNode.IconFile = string.Format("Icons/{0}", siteMapNode.ResourceKey);
        }
        SiteMapNodeCollection children = siteMapNode.ChildNodes;
        if (children != null && children.Count > 0)
        {
            foreach (SiteMapNode mapNode in siteMapNode.ChildNodes)
            {
                //    if (SiteMap.RootNode == siteMapNode)
                //    {
                //        Accordion1.Items.Add(new Coolite.Ext.Web.Panel(mapNode.Title) { Icon = Icon.New });
                //    }
                treeNode.Nodes.Add(this.CreateNode(mapNode));
            }
        }

        return treeNode;
    }
    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        HttpContext.Current.Session.Abandon();
    }

    protected void ObjectDataSource1_Disposing(object sender, ObjectDataSourceDisposingEventArgs e)
    {
      /*  SOA.Framework.RA.RecentActivityServiceClient client = e.ObjectInstance as SOA.Framework.RA.RecentActivityServiceClient;
        if (client != null)
        {
            client.Close();
        }*/
    }

   
}
