﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Services;
using BankOneMobile.UI.Logic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class AgentLocationManagement_ViewAgentGeoLocation : System.Web.UI.Page
{
   
    private const String MINUTES_HOUR_INTERVAL = "1";
    private const String HOUR_DAY_INTERVAL = "2";
    private const String DAY_WEEK_INTERVAL = "3";
    private const String WEEK_MONTH_INTERVAL = "4";


    public static IDictionary<int, string> GetAll<TEnum>() where TEnum : struct
    {
        var enumerationType = typeof(TEnum);
        
        if (!enumerationType.IsEnum)
            throw new ArgumentException("Enumeration type is expected.");

        var dictionary = new Dictionary<int, string>();

        foreach (int value in Enum.GetValues(enumerationType))
        {
            var name = Enum.GetName(enumerationType, value);
            dictionary.Add(value, name);
        }

        return dictionary;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string institutionCode = BankOneMobile.UI.Logic.UserLogic.MFBCode;
            List<ListItem> list = new List<ListItem>();
            RegionServiceReference.Region[] regions = null;
            using (var client = new RegionServiceReference.RegionServiceClient())
            {
                regions = client.RetrieveAll(institutionCode);
            }
            if (regions != null)
            {
                foreach (var region in regions)
                {
                    list.Add(new ListItem(region.Name.ToUpper(), region.ID.ToString()));
                }
                list = list.OrderBy(a => a.Text).ToList();
                RegionDropDownList.DataSource = list;
                RegionDropDownList.DataTextField = "Text";
                RegionDropDownList.DataValueField = "Value";
                //RegionDropDownList.AutoselectFirstItem = true;

                RegionDropDownList.DataBind();
                RegionDropDownList.Items.Insert(0, new ListItem("-select-",""));
            }

            List<IAgent> agents = new List<IAgent>();
            agents = new AgentSystem().GetAllAgentsByInstitutionCode(institutionCode);
            if (agents != null && agents.Count > 0)
            {
                list = new List<ListItem>();
                foreach (var agent in agents)
                {
                    list.Add(new ListItem(agent.LastName + ", " + agent.OtherNames, agent.PhoneNumber));
                    list = list.OrderBy(a => a.Text).ToList();
                    AgentDropDownList.DataSource = list;
                    AgentDropDownList.DataTextField = "Text";
                    AgentDropDownList.DataValueField = "Value";
                    //AgentDropDownList.AutoselectFirstItem = true;

                    AgentDropDownList.DataBind();
                    AgentDropDownList.Items.Insert(0, new ListItem("-select-",""));
                }
            }
            IDictionary<int, string> geoTypeDictionary = GetAll<GeoType>();
            TypeDropDownList.DataSource = geoTypeDictionary;
            TypeDropDownList.DataTextField = "Value";
            TypeDropDownList.DataValueField = "Key";
            //TypeDropDownList.AutoselectFirstItem = true;
            TypeDropDownList.DataBind();
            TypeDropDownList.Items.Insert(0, new ListItem("-select-",""));

            IDictionary<int, string> transactionTypeDictionary = GetAll<TransactionTypeName>();

            TransactionTypeDropDownList.DataSource = transactionTypeDictionary;
            TransactionTypeDropDownList.DataTextField = "Value";
            TransactionTypeDropDownList.DataValueField = "Key";

            //TransactionTypeDropDownList.AutoselectFirstItem = true;
            TransactionTypeDropDownList.DataBind();
            TransactionTypeDropDownList.Items.Insert(0, new ListItem("-select-",""));

        }
    }

//    private void btnSearch_Click(object sender, EventArgs e)
//    {
//        if (TypeDropDownList.Text == string.Empty)
//        {
//            AgentDropDownList.CssClass = "form-control";
//            TypeDropDownList.CssClass = "form-control-alert";
//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript2", "alert('Please select Geo Type to display');", true);

//            return;
//        }
//        if (BandInternalDropDownList.Text == string.Empty)
//        {
//            AgentDropDownList.CssClass = "form-control";
//            TypeDropDownList.CssClass = "form-control";
//            BandInternalDropDownList.CssClass = "form-control-alert";
//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript2", "alert('Please select band interval');", true);

//            return;
//        }
//        else
//        {
//            TypeDropDownList.CssClass = "form-control";
//            BandInternalDropDownList.CssClass = "form-control";
//            DateTime dateFrom;
//            string regionID = RegionDropDownList.SelectedValue;
//            string branchID = BranchDropDownList.SelectedValue;
//            string agentPhone = AgentDropDownList.SelectedValue;
//            string customerPhone = CustomerPhone.Text;
//            string theType = TypeDropDownList.Text;
//            string band = BandInternalDropDownList.Text;
//            DateTime.TryParse(dtFrom.Text, out dateFrom);
//            string transactionType = TransactionTypeDropDownList.Text;

//            List<AgentGeoLocationModel> agentGeoLocationModel = new List<AgentGeoLocationModel>();
//            GeoType theGeoType = (GeoType)Enum.Parse(typeof(GeoType), theType);

//            switch (theGeoType)
//            {
//                case GeoType.AgentLocation:
//                    {
//                        if (dateFrom == null || dateFrom == default(DateTime))
//                        {
//                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript2", "alert('Please specify a valid date to continue. The \"Date\" field is required');", true);
//                            return;
//                        }

//                        List<IAgentGeoLocation> agentGeoLocations = new List<IAgentGeoLocation>();
//                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
//                        {
//                            agentGeoLocations = new AgentGeoLocationSystem().GetAgentGeoLocationByInstitutionCode(source, institutionCode);
//                        }
//                        if (agentGeoLocations != null && agentGeoLocations.Count > 0)
//                        {
//                            if (dateFrom != null && dateFrom > default(DateTime))
//                            {
//                                agentGeoLocations = agentGeoLocations.Where(x => x.DateLogged >= dateFrom && x.DateLogged <= dateFrom.AddDays(1).AddSeconds(-1)).ToList();
//                            }

//                            if (agentGeoLocations == null || agentGeoLocations.Count == 0)
//                            {
//                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript1", "alert('No data to display');", true);
//                                return;
//                            }

//                            timeMapPanel.Visible = true;
//                            List<IMobileAccount> agentMobileAccounts = new List<IMobileAccount>();
//                            foreach (var phoneNo in agentGeoLocations.Select(x => x.AgentPhoneNumber).ToArray())
//                            {
//                                var mob = new MobileAccountSystem().GetMobileAccountsByPhoneNumber(phoneNo);
//                                if (mob != null && mob.Count > 0) agentMobileAccounts.Add(mob.FirstOrDefault());
//                            }
//                            //CustomerServiceRef.Customer[] agentsFromCoreBanking = null;

//                            using (var client = new CustomerServiceRef.CustomerServiceClient())
//                            {
//                                //agentsFromCoreBanking = client.GetByCustomerIDs(institutionCode, agentMobileAccounts.Select(x => x.CustomerID).Distinct().ToArray());
//                            }

//                            RegionServiceReference.Region[] regions = null;
//                            using (var client = new RegionServiceReference.RegionServiceClient())
//                            {
//                                regions = client.RetrieveAll(institutionCode);
//                            }

//                            BranchServiceReference.Branch[] branches = null;
//                            using (var client = new BranchServiceReference.BranchServiceClient())
//                            {
//                                branches = client.RetrieveAll(institutionCode);
//                            }

//                            List<IAgent> agents = new List<IAgent>();
//                            agents = new AgentSystem().GetAllAgentsByInstitutionCode(institutionCode);

//                            foreach (var agentGeoLocation in agentGeoLocations)
//                            {
//                                string agentName = string.Empty;
//                                if (!string.IsNullOrWhiteSpace(agentGeoLocation.AgentPhoneNumber))
//                                {
//                                    agentName = agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).ToList().Count > 0 ?
//                                        agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).FirstOrDefault().LastName + ", " +
//                                            agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).FirstOrDefault().OtherNames : "Agent";
//                                }


//                                long theBranchID = agents.FirstOrDefault(x => x.PhoneNumber == agentGeoLocation.AgentPhoneNumber) != null ?
//                                                agents.FirstOrDefault(x => x.PhoneNumber == agentGeoLocation.AgentPhoneNumber).BranchID : 0;

//                                long theRegionID = theBranchID == 0 ? 0 : branches.Where(x => x.ID == theBranchID).Select(y => y.RegionID).FirstOrDefault();

//                                agentGeoLocationModel.Add(new AgentGeoLocationModel
//                                {
//                                    InstitutionCode = agentGeoLocation.InstitutionCode,
//                                    AgentName = !string.IsNullOrEmpty(agentName) ? agentName : "Agent",
//                                    AgentPhoneNumber = agentGeoLocation.AgentPhoneNumber,
//                                    DateLogged = agentGeoLocation.DateLogged,
//                                    Latitude = agentGeoLocation.Latitude,
//                                    Longitude = agentGeoLocation.Longitude,
//                                    BranchID = theBranchID,
//                                    RegionID = theRegionID
//                                });
//                            }
//                        }
//                        else
//                        {
//                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript3", "alert('No data to display');", true);
//                        }
//                        break;
//                    }
//                case GeoType.AgentTransactionLocation:
//                    {
//                        if (dateFrom == null || dateFrom == default(DateTime))
//                        {
//                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript2", "alert('Please specify a valid date to continue. The \"Date\" field is required');", true);
//                            return;
//                        }

//                        List<IAgentGeoLocationTransactionType> agentGeoLocations = new List<IAgentGeoLocationTransactionType>();
//                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
//                        {
//                            agentGeoLocations = new AgentGeoLocationTransactionTypeSystem().GetAgentGeoLocationTransactionTypeByInstitutionCode(source, institutionCode);
//                        }
//                        if (agentGeoLocations != null && agentGeoLocations.Count > 0)
//                        {
//                            if (dateFrom != null && dateFrom > default(DateTime))
//                            {
//                                agentGeoLocations = agentGeoLocations.Where(x => x.DateLogged >= dateFrom && x.DateLogged <= dateFrom.AddDays(1).AddSeconds(-1)).ToList();
//                            }

//                            if (agentGeoLocations == null || agentGeoLocations.Count == 0)
//                            {
//                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript1", "alert('No data to display');", true);
//                                return;
//                            }

//                            timeMapPanel.Visible = true;
//                            List<MobileAccount> custMobileAccounts = new List<MobileAccount>();
//                            List<MobileAccount> agentMobileAccounts = new List<MobileAccount>();
//                            foreach (var phoneNo in agentGeoLocations.Select(x => x.CustomerPhoneNumber).ToArray())
//                            {
//                                var mob = new MobileAccountSystem().GetByPhoneNumber(phoneNo);
//                                if (mob != null) custMobileAccounts.Add(mob);
//                            }
//                            foreach (var phoneNo in agentGeoLocations.Select(x => x.AgentPhoneNumber).ToArray())
//                            {
//                                var mob = new MobileAccountSystem().GetByPhoneNumber(phoneNo);
//                                if (mob != null) agentMobileAccounts.Add(mob);
//                            }
//                            CustomerServiceRef.Customer[] customersFromCoreBanking = null;
//                            CustomerServiceRef.Customer[] agentsFromCoreBanking = null;
//                            using (var client = new CustomerServiceRef.CustomerServiceClient())
//                            {
//                                customersFromCoreBanking = client.GetByCustomerIDs(institutionCode, custMobileAccounts.Select(x => x.CustomerID).Distinct().ToArray());
//                                agentsFromCoreBanking = client.GetByCustomerIDs(institutionCode, agentMobileAccounts.Select(x => x.CustomerID).Distinct().ToArray());
//                            }

//                            RegionServiceReference.Region[] regions = null;
//                            using (var client = new RegionServiceReference.RegionServiceClient())
//                            {
//                                regions = client.RetrieveAll(institutionCode);
//                            }

//                            BranchServiceReference.Branch[] branches = null;
//                            using (var client = new BranchServiceReference.BranchServiceClient())
//                            {
//                                branches = client.RetrieveAll(institutionCode);
//                            }


//                            foreach (var agentGeoLocation in agentGeoLocations)
//                            {
//                                string agentName = string.Empty, customerName = string.Empty;
//                                if (!string.IsNullOrWhiteSpace(agentGeoLocation.AgentPhoneNumber))
//                                {
//                                    agentName = agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).ToList().Count > 0 ?
//                                        agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).FirstOrDefault().LastName + ", " +
//                                            agentMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.AgentPhoneNumber).FirstOrDefault().OtherNames : "Agent";
//                                }

//                                if (!string.IsNullOrWhiteSpace(agentGeoLocation.CustomerPhoneNumber))
//                                {
//                                    customerName = custMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.CustomerPhoneNumber).ToList().Count > 0 ?
//                                        custMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.CustomerPhoneNumber).FirstOrDefault().LastName + ", " +
//                                            custMobileAccounts.Where(x => x.MobilePhone == agentGeoLocation.CustomerPhoneNumber).FirstOrDefault().OtherNames : "Customer";
//                                }

//                                long theBranchID = customersFromCoreBanking.Where(x => x.PhoneNo == agentGeoLocation.CustomerPhoneNumber).ToList().Count > 0 ?
//                                        customersFromCoreBanking.Where(x => x.PhoneNo == agentGeoLocation.CustomerPhoneNumber).Select(x => x.BranchID).FirstOrDefault() :
//                                            agentsFromCoreBanking.Where(x => x.PhoneNo == agentGeoLocation.AgentPhoneNumber).ToList().Count > 0 ?
//                                                customersFromCoreBanking.Where(x => x.PhoneNo == agentGeoLocation.CustomerPhoneNumber).Select(x => x.BranchID).FirstOrDefault() : 0;

//                                long theRegionID = theBranchID == 0 ? 0 : branches.Where(x => x.ID == theBranchID).Select(y => y.RegionID).FirstOrDefault();

//                                agentGeoLocationModel.Add(new AgentGeoLocationModel
//                                {
//                                    InstitutionCode = agentGeoLocation.InstitutionCode,
//                                    AgentName = !string.IsNullOrEmpty(agentName) ? agentName : "Agent",
//                                    CustomerName = !string.IsNullOrEmpty(customerName) ? customerName : "Customer",
//                                    AgentPhoneNumber = agentGeoLocation.AgentPhoneNumber,
//                                    CustomerPhoneNumber = agentGeoLocation.CustomerPhoneNumber,
//                                    DateLogged = agentGeoLocation.DateLogged,
//                                    Latitude = agentGeoLocation.Latitude,
//                                    Longitude = agentGeoLocation.Longitude,
//                                    TransactionType = agentGeoLocation.TransactionType.ToString(),
//                                    BranchID = theBranchID,
//                                    RegionID = theRegionID
//                                });
//                            }
//                        }
//                        else
//                        {
//                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript1", "alert('No data to display');", true);
//                        }
//                        break;
//                    }
//            }
//            if (!string.IsNullOrWhiteSpace(regionID))
//            {
//                agentGeoLocationModel = agentGeoLocationModel.Where(x => x.RegionID == long.Parse(regionID)).ToList();
//            }
//            if (!string.IsNullOrWhiteSpace(branchID))
//            {
//                agentGeoLocationModel = agentGeoLocationModel.Where(x => x.BranchID == long.Parse(branchID)).ToList();
//            }
//            if (!string.IsNullOrWhiteSpace(agentPhone))
//            {
//                agentGeoLocationModel = agentGeoLocationModel.Where(x => x.AgentPhoneNumber == agentPhone).ToList();
//            }
//            if (!string.IsNullOrWhiteSpace(customerPhone))
//            {
//                agentGeoLocationModel = agentGeoLocationModel.Where(x => x.CustomerPhoneNumber == customerPhone).ToList();
//            }

//            if (!string.IsNullOrWhiteSpace(transactionType))
//            {
//                agentGeoLocationModel = agentGeoLocationModel.Where(x => x.TransactionType == transactionType).ToList();
//            }
//            if (agentGeoLocationModel.Count == 0)
//            {
//                timeMapPanel.Visible = false;
//                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript2", "alert('No data to display');", true);
//                return;
//            }

//            agentGeoLocationModel = agentGeoLocationModel.OrderBy(x => x.DateLogged).ToList();
//            TotalCountLbl.Text = "Total: " + agentGeoLocationModel.Count;

//            StringBuilder sb = new StringBuilder();
//            Random rand = new Random();
//            Random randHour = new Random();
//            Random randMin = new Random();
//            Random randSec = new Random();
//            sb.Append(@"var tm;
//                        $(function() {
//                            var items = [], x;");
//            foreach (var theLocation in agentGeoLocationModel)
//            {
//                sb.Append(string.Format(@"items.push({{
//                    title:""{0}"",
//                    start:""{1}"",
//                    point: {{
//                        lat:{2},
//                        lon:{3}
//                    }},
//                    options: {{
//                        size:parseInt(Math.random()*5),
//                        awesomeness: parseInt(Math.random()*10),
//                        description: ""{4}""
//                    }}
//                }});", theLocation.AgentName, theLocation.DateLogged.AddHours(1).ToString("yyyy-M-d hh:mm:ss tt"), theLocation.Latitude,
//                     theLocation.Longitude, theGeoType == GeoType.AgentTransactionLocation ? theLocation.TransactionType + "<br/>Time: " + theLocation.DateLogged.ToString("hh:mm:ss tt") : "Movement" + "<br/>Time: " + theLocation.DateLogged.ToString("hh:mm:ss tt")));
//            }

//            //            for (int i = 0; i < 30; i++)
//            //            {
//            //                sb.Append(string.Format(@"items.push({{
//            //                    title:""{0}"",
//            //                    start:""{1}"",
//            //                    point: {{
//            //                        lat:{2},
//            //                        lon:{3}
//            //                    }},
//            //                    options: {{
//            //                        size:parseInt(Math.random()*5),
//            //                        awesomeness: parseInt(Math.random()*10),
//            //                        description: ""{4}""
//            //                    }}
//            //                }});", "Title -" + i, (new DateTime(2015, 10, 21, rand.Next(1, 23), rand.Next(1, 59), 0).ToString("yyyy-M-d hh:mm:ss")), 43.0 + 10 * (float)rand.Next(90, 99) / rand.Next(90, 99),
//            //                   11.0 * (float)rand.Next(90, 99) / rand.Next(90, 99), "Description - " + i));
//            //            }

//            sb.Append(@"var colors = [""BA4848"", ""BA4848"", ""BA4848"", ""BA4848"", ""BA4848""]
//                        for (var size=0; size<5; size++) {
//                            for (var awe=0; awe<colors.length;awe++) {
//                                TimeMap.themes['theme' + size + '-' + awe] = TimeMapTheme.createCircleTheme({
//                                    size: 15,
//                                    color: colors[awe]
//                                });
//                            }
//                        }");

//            string firstBandInterval = string.Empty, secondBandInterval = string.Empty;
//            if (BandInternalDropDownList.SelectedValue == MINUTES_HOUR_INTERVAL)
//            {
//                firstBandInterval = "MINUTE";
//                secondBandInterval = "HOUR";
//            }
//            else if (BandInternalDropDownList.SelectedValue == HOUR_DAY_INTERVAL)
//            {
//                firstBandInterval = "HOUR";
//                secondBandInterval = "DAY";
//            }
//            else if (BandInternalDropDownList.SelectedValue == DAY_WEEK_INTERVAL)
//            {
//                firstBandInterval = "DAY";
//                secondBandInterval = "WEEK";
//            }
//            else if (BandInternalDropDownList.SelectedValue == WEEK_MONTH_INTERVAL)
//            {
//                firstBandInterval = "WEEK";
//                secondBandInterval = "MONTH";
//            }

//            sb.Append(@"tm = TimeMap.init({
//                        mapId: ""map"",               // Id of map div element (required)
//                        timelineId: ""timeline"",     // Id of timeline div element (required)
//                        datasets: [
//                            {
//                                type: ""basic"",
//                                options: {
//                                    items: items,
//                                    infoTemplate: '<b>{{title}}</b><div>Type: {{description}}</div>',
//                                    // use the transformFunction to add the theme before loading
//                                    transformFunction: function(item) {
//                                        item.options.theme = ""theme"" + item.options.size + 
//                                            ""-"" + parseInt(item.options.awesomeness / 2); // range 0-9, colors 0-5
//                                        return item;
//                                    }
//                                }
//                            }
//                        ],");

//            sb.Append(string.Format(@"
//                        bandIntervals: [
//                            Timeline.DateTime.{0}, 
//                            Timeline.DateTime.{1}
//                        ]
//                    }});
//                }});
//                    ", firstBandInterval, secondBandInterval));

//            string script = sb.ToString();

//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MyScript", script, true);

//        }
//    }
    protected void TypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        timeMapPanel.Visible = false;
        if ((GeoType)Enum.Parse(typeof(GeoType), TypeDropDownList.Text) == GeoType.AgentLocation)
        {
            TransactionTypeDropDownList.Enabled = false;
            CustomerPhone.Enabled = false;
        }
        else
        {
            TransactionTypeDropDownList.Enabled = true;
            CustomerPhone.Enabled = true;
        }
    }

    [WebMethod]
    public static List<BranchServiceReference.Branch> GetBranches(long regionId)
    {
        string institutionCode = BankOneMobile.UI.Logic.UserLogic.MFBCode;

        List<BranchServiceReference.Branch> branches = null;
        using (var client = new BranchServiceReference.BranchServiceClient())
        {
            branches = client.RetrieveAll(institutionCode).ToList();
        }
        if (branches != null && branches.Count > 0)
        {
            branches = branches.Where(x => x.RegionID == regionId).ToList();
            if (branches == null || branches.Count == 0)
            {
                throw new Exception("No branches in selected region!");
            }
        }
        else
        {
            throw new Exception("No branches in selected region!");
        }

        return branches;
    }

    [WebMethod]
    public static List<AgentGeoLocationModel> GetLocation(long regionID, long branchID, string agentPhone, string customerPhone, 
        string theType, string band,string dtFrom, string transactionType)
    {
        string institutionCode = BankOneMobile.UI.Logic.UserLogic.MFBCode;
        DateTime dateFrom;
        DateTime.TryParse(dtFrom, out dateFrom);
        GeoType theGeoType = (GeoType)Enum.Parse(typeof(GeoType), theType);

        List<AgentGeoLocationModel> agentGeoLocationModel = new List<AgentGeoLocationModel>();

        if (dateFrom == null || dateFrom == default(DateTime))
        {
            throw new Exception("Please specify a valid date to continue. The \"Date\" field is required!");
        }

        string firstBandInterval = string.Empty, secondBandInterval = string.Empty;
        if (band == MINUTES_HOUR_INTERVAL)
        {
            firstBandInterval = "Timeline.DateTime.MINUTE";
            secondBandInterval = "Timeline.DateTime.HOUR";
        }
        else if (band == HOUR_DAY_INTERVAL)
        {
            firstBandInterval = "Timeline.DateTime.HOUR";
            secondBandInterval = "Timeline.DateTime.DAY";
        }
        else if (band == DAY_WEEK_INTERVAL)
        {
            firstBandInterval = "Timeline.DateTime.DAY";
            secondBandInterval = "Timeline.DateTime.WEEK";
        }
        else if (band == WEEK_MONTH_INTERVAL)
        {
            firstBandInterval = "Timeline.DateTime.WEEK";
            secondBandInterval = "Timeline.DateTime.MONTH";
        }

       
        switch (theGeoType)
        {
            case GeoType.AgentLocation:
                {
                    
                    List<IAgentGeoLocation> agentGeoLocations = new List<IAgentGeoLocation>();
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        agentGeoLocations = new AgentGeoLocationSystem().GetAgentGeoLocationByInstitutionCode(source, institutionCode);
                    }
                    if (agentGeoLocations != null && agentGeoLocations.Count > 0)
                    {
                        RegionServiceReference.Region[] regions = null;
                        using (var client = new RegionServiceReference.RegionServiceClient())
                        {
                            regions = client.RetrieveAll(institutionCode);
                        }

                        BranchServiceReference.Branch[] branches = null;
                        using (var client = new BranchServiceReference.BranchServiceClient())
                        {
                            branches = client.RetrieveAll(institutionCode);
                        }

                        List<IAgent> agents = new List<IAgent>();
                        agents = new AgentSystem().GetAllAgentsByInstitutionCode(institutionCode);

                        if (dateFrom != null && dateFrom > default(DateTime))
                        {
                            agentGeoLocations = agentGeoLocations.Where(x => x.DateLogged >= dateFrom && x.DateLogged <= dateFrom.AddDays(1).AddSeconds(-1)).ToList();
                        }

                        if (agentGeoLocations == null || agentGeoLocations.Count == 0)
                        {
                            throw new Exception("No data to display!");
                        }

                        foreach (var location in agentGeoLocations)
                        {
                            var agent = agents.SingleOrDefault(x=>x.PhoneNumber==location.AgentPhoneNumber);
                            string agentName = agent == null ? "Agent" : agent.LastName + " " + agent.OtherNames;
                            long agentBranch = agent==null? 0 : agent.BranchID;
                            var branch = branches.SingleOrDefault(x=>x.ID==agentBranch);
                            long theRegionID = branch==null? 0 : branch.RegionID;
                            agentGeoLocationModel.Add(new AgentGeoLocationModel
                            {
                                AgentName = agentName,
                                AgentPhoneNumber = location.AgentPhoneNumber,
                                BranchID = agentBranch,
                                RegionID = theRegionID,
                                DateLogged = location.DateLogged.ToString("hh:mm:ss tt"),
                                InstitutionCode = location.InstitutionCode,
                                Latitude = location.Latitude,
                                Longitude = location.Longitude,
                                TransactionType = "Movement",
                                FirstBandInterval = firstBandInterval,
                                SecondBandInterval = secondBandInterval,
                                StartDate = location.DateLogged.AddHours(1).ToString("yyyy-M-d hh:mm:ss tt")
                            });
                        }

                        if (regionID > 0)
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.RegionID == regionID).ToList();
                        }

                        if (branchID > 0)
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.BranchID == branchID).ToList();
                        }

                        if(!string.IsNullOrWhiteSpace(agentPhone))
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.AgentPhoneNumber == agentPhone).ToList();
                        }

                        if(agentGeoLocationModel == null || agentGeoLocationModel.Count == 0)
                        {
                            throw new Exception("No data to display!");
                        }
                    }
                    else
                    {
                        throw new Exception("No data to display!");
                    }
                    break;
                }
            case GeoType.AgentTransactionLocation:
                {
                    List<IAgentGeoLocationTransactionType> agentGeoLocations = new List<IAgentGeoLocationTransactionType>();
                    using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                    {
                        agentGeoLocations = new AgentGeoLocationTransactionTypeSystem().GetAgentGeoLocationTransactionTypeByInstitutionCode(source, institutionCode);
                    }
                    if (agentGeoLocations != null && agentGeoLocations.Count > 0)
                    {
                        RegionServiceReference.Region[] regions = null;
                        using (var client = new RegionServiceReference.RegionServiceClient())
                        {
                            regions = client.RetrieveAll(institutionCode);
                        }

                        BranchServiceReference.Branch[] branches = null;
                        using (var client = new BranchServiceReference.BranchServiceClient())
                        {
                            branches = client.RetrieveAll(institutionCode);
                        }

                        List<IAgent> agents = new List<IAgent>();
                        agents = new AgentSystem().GetAllAgentsByInstitutionCode(institutionCode);

                        var mobileAccounts = new MobileAccountSystem().GetAllMobileAccounts().Where(x=>x.InstitutionCode == institutionCode);

                        if (dateFrom != null && dateFrom > default(DateTime))
                        {
                            agentGeoLocations = agentGeoLocations.Where(x => x.DateLogged >= dateFrom && x.DateLogged <= dateFrom.AddDays(1).AddSeconds(-1)).ToList();
                        }

                        if (agentGeoLocations == null || agentGeoLocations.Count == 0)
                        {
                            throw new Exception("No data to display!");
                        }

                        foreach (var location in agentGeoLocations)
                        {
                            var agent = agents.SingleOrDefault(x => x.PhoneNumber == location.AgentPhoneNumber);
                            string agentName = agent == null ? "Agent" : agent.LastName + " " + agent.OtherNames;
                            var customer = mobileAccounts.SingleOrDefault(x => x.MobilePhone == location.CustomerPhoneNumber);
                            string customerName = customer == null ? "Customer" : customer.LastName + " " + customer.OtherNames;
                            long agentBranch = agent == null ? 0 : agent.BranchID;
                            var branch = branches.SingleOrDefault(x => x.ID == agentBranch);
                            long theRegionID = branch == null ? 0 : branch.RegionID;
                            agentGeoLocationModel.Add(new AgentGeoLocationModel
                            {
                                AgentName = agentName,
                                AgentPhoneNumber = location.AgentPhoneNumber,
                                BranchID = agentBranch,
                                RegionID = theRegionID,
                                DateLogged = location.DateLogged.ToString("hh:mm:ss tt"),
                                InstitutionCode = location.InstitutionCode,
                                Latitude = location.Latitude,
                                Longitude = location.Longitude,
                                TransactionType = location.TransactionType.ToString(),
                                CustomerName = customerName,
                                CustomerPhoneNumber = location.CustomerPhoneNumber,
                                FirstBandInterval = firstBandInterval,
                                SecondBandInterval = secondBandInterval,
                                StartDate = location.DateLogged.AddHours(1).ToString("yyyy-M-d hh:mm:ss tt")
                            });
                        }

                        if (regionID > 0)
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.RegionID == regionID).ToList();
                        }

                        if (branchID > 0)
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.BranchID == branchID).ToList();
                        }

                        if (!string.IsNullOrWhiteSpace(agentPhone))
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.AgentPhoneNumber == agentPhone).ToList();
                        }

                        if (!string.IsNullOrWhiteSpace(customerPhone))
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.CustomerPhoneNumber == customerPhone).ToList();
                        }

                        if (!string.IsNullOrWhiteSpace(transactionType))
                        {
                            agentGeoLocationModel = agentGeoLocationModel.Where(x => x.TransactionType == transactionType).ToList();
                        }

                        if (agentGeoLocationModel == null || agentGeoLocationModel.Count == 0)
                        {
                            throw new Exception("No data to display!");
                        }
                    }
                    else
                    {
                        throw new Exception("No data to display!");
                    } break;
               }
        }

        agentGeoLocationModel = agentGeoLocationModel.OrderBy(x => x.DateLogged).ToList();

        return agentGeoLocationModel;
        
    }
}