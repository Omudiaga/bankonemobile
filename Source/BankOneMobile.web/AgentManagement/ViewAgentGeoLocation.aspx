﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="ViewAgentGeoLocation.aspx.cs" Inherits="AgentLocationManagement_ViewAgentGeoLocation" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Geo Location Tracking System</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAASI0kCI-azC8RgbOZzWc3VRRarOQe_TKf_51Omf6UUSOFm7EABRRhO0PO4nBAO9FCmVDuowVwROLo3w" type="text/javascript"></script>
    <script type="text/javascript" src="../lib/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../lib/mxn/mxn.js?(google)"></script>
    <script type="text/javascript" src="../lib/timeline-2.3.0.js"></script>
    <script src="../lib/timemap.js" type="text/javascript"></script>
    <script src="../lib/circle_icons.js" type="text/javascript"></script>

    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles/jquery.datetimepicker.css" />
    <script src="../lib/js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.combobox.js" type="text/javascript"></script>

    <link href="../Styles/examples.css" type="text/css" rel="stylesheet" />
    <link href="../Styles/bootstrap.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        body {
            margin: 0px;
            padding: 0px;
        }

        /*.container {
            width: 100%;
        }

            .container div {
                width: 100%;
            }*/

        .container .header {
            padding: 4px;
            cursor: pointer;
            font-weight: bold;
            font-size: 11px;
        }

        .container .content {
            /*display: none;
                padding: 5px;*/
        }

        .myclass {
            height: 18px;
            color: rgba(128,128,128,1);
            border-left: 5px solid #6B869F !important;
            padding-left: 3px;
        }

        div#timelinecontainer {
            height: 200px;
            border-left: 1px solid #cbc7c7;
            border-right: 1px solid #cbc7c7;
            border-top: 1px solid #cbc7c7;
        }

        div#mapcontainer {
            /*height:400px;*/
            border-left: 1px solid #cbc7c7;
            border-right: 1px solid #cbc7c7;
            border-bottom: 1px solid #cbc7c7;
        }

        .top {
            background: #6B869F;
            height: 3px;
            -webkit-box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
            -moz-box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
            box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
        }

        .filterTable {
            margin: 0px 5px;
            font-size: 15px;
        }

        .fitContent {
            font-size: 13px;
            width: 1%;
            white-space: nowrap;
            padding: 0px 3px;
        }

        .fillContent {
            width: 250px;
        }

        .myDiv {
            position: relative;
            z-index: 14;
        }

        .pull-right {
            float: right;
            padding-right: 10px;
            font-size: 10px;
            font-weight: bold;
            color: rgba(128,128,128,1);
        }

        #error-div {
            background: #fbb7b7;
            border: 1px solid #ff7575;
            display: none;
            width: 100%;
            margin: 10px auto;
            padding: 6px;
            font-size: 13px;
            border-radius: 5px;
        }

        #map_wrapper {
            height: 100%;
            display: none;
        }

        #map_canvas {
            width: 100%;
            height: 100%;
        }

        .info_content p {
            margin: 0px;
            padding: 0px;
            font-size: 13px;
        }

        .info_content h3 {
            margin: 0px;
            padding: 0px;
            font-size: 14px;
        }

        #legend {
            font-size: 10px;
            font-weight: bold;
            border: 1px solid #ddd;
            margin: 1em;
            padding: 10px;
            background: #fefbfb;
            display: none;
        }

        .legend-inner {
            display: inline;
            height: 5px;
            width: 10px;
            margin: 0px 5px;
            padding: 3px 5px;
            border-radius: 50px;
            color: gray;
        }

        .loader {
            display: none;
            width: 300px;
            margin: 5px auto;
            font-size: 13px;
            color: gray;
        }

        #timeMapPanel {
            margin: 0px 5px;
        }

        #slidenav {
            margin: 0 auto;
            width: auto;
            background: #6B869F;
            height: 30px;
            padding: 5px 10px 15px 10px;
            position: absolute;
            z-index: 1000;
            right: 15px;
            border-bottom: 2px solid rgba(128,128,128,1);
            border-left: 2px solid rgba(128,128,128,1);
            border-right: 2px solid rgba(128,128,128,1);
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            display: none;
            color: white;
            font-size: 13px;
        }

        .header-click {
            margin-bottom: 5px;
            padding: 4px;
            cursor: pointer;
            font-weight: bold;
            font-size: 11px;
        }
    </style>
    <script>
        
       
    </script>
</head>

<body>
    <!-- Preloader -->
    <div class="loading-container">
        <%--<div class="cssload-box-loading"></div>--%>
    </div>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="top"></div>
        <div class="header-click">
            <span><i class='fa fa-minus'></i> Hide Filter</span>
        </div>
        <div class="col-md-12">
            <div class="panel panel-top  panel-danger panel-top-danger">
                <%--<div class="panel-heading un-bold">
                <h5><i class="fa fa-search"></i>&nbsp;&nbsp;Filters</h5>
            </div>--%>
                <div class="panel-body" style="background: #f4f4f4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-align">

                                <div class="form-horizontal">
                                    <div id="error-div" class="error">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Region</label>
                                            <asp:DropDownList ID="RegionDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server" onchange="javascript:GetBranchesInRegion(this.value);"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Agent</label>
                                            <asp:DropDownList ID="AgentDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label">Date</label>
                                            <asp:TextBox ID="dtFrom" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Transaction Type</label>
                                            <asp:DropDownList ID="TransactionTypeDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label">Geo Type</label>
                                            <asp:DropDownList ID="TypeDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Branch</label>
                                            <asp:DropDownList ID="BranchDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Customer Phone Number</label>
                                            <asp:TextBox ID="CustomerPhone" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label">Band</label>
                                            <asp:DropDownList ID="BandInternalDropDownList" CssClass="form-control" ClientIDMode="Static" runat="server">
                                                <asp:ListItem Value="">-select-</asp:ListItem>
                                                <asp:ListItem Value="1">Minute/Hour</asp:ListItem>
                                                <asp:ListItem Value="2">Hour/Day</asp:ListItem>
                                                <asp:ListItem Value="3">Day/Week</asp:ListItem>
                                                <asp:ListItem Value="4">Week/Month</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a class="btn btn-default" href="/Phone/Create" id="btnSearch" runat="server">
                                                <i id="filtersubmit" class="fa fa-search"></i>&nbsp;Search
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <asp:Panel runat="server" ID="timeMapPanel">
                    <div id="legend">
                    </div>
                    <asp:Label ID="TotalCountLbl" runat="server" CssClass="pull-right"></asp:Label>
                    <div id="map_wrapper">
                        <div id="timemap" class="time-map">

                            <div id="timelinecontainer">
                                <div id="timeline"></div>
                            </div>
                            <div id="mapcontainer">
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>



        <div id="slidenav">
        </div>

        <div class="loader" id="loader">
            <table>
                <tr>
                    <td>
                        <img src="../Styles/img/loader.gif" />
                    </td>
                    <td>&nbsp;Processing...  
                    </td>
                </tr>
            </table>
        </div>



    </form>
</body>


</html>
<script src="../lib/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="../lib/js/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    /********************************
    Preloader
    ********************************/
    $(window).load(function () {
        $('.loading-container').fadeOut(1000, function () {
            $(this).remove();
        });
    });
    $('#dtFrom').datetimepicker();
    $('#dtTo').datetimepicker();

    $(".header-click").click(function () {

        $header = $(this);
        //getting the next element
        $content = $(".panel")
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {
            //execute this after slideToggle is done
            if ($content.is(":visible") == false) {
                //$("#timelinecontainer").css("height", "150px");
                $("#mapcontainer").css("height", "600px");
            }
            else {
                //$("#timelinecontainer").css("height", "250px");
                $("#mapcontainer").css("height", "450px");
                $('#slidenav').hide();
            }

            //change text of header based on visibility of content div
            $header.text(function () {
                //change text based on condition
                return $content.is(":visible") ? "Hide Filter" : "Show Filter";
            });
        });

    });

    function GetBranchesInRegion(_RegionId) {
        if (_RegionId == '') {
            $("#BranchDropDownList").html(processmessage).show();
        }
        else {
            var procemessage = "<option value=''> Please wait...</option>";
            $("#BranchDropDownList").html(procemessage).show();
            var url = "ViewAgentGeoLocation.aspx/GetBranches";

            $.ajax({
                type: "POST",
                url: url,
                data: '{regionId: ' + _RegionId + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#BranchDropDownList").empty().append($("").val("[-]").html("Please select"));
                    BindBranchddl(msg.d);
                },
                error: function (xhr, status, Error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#BranchDropDownList").empty().append($("").val("[-]").html("Please select"));
                    $("#BranchDropDownList").append($("<option></option>").val('').html('-select-'));
                    // Display the specific error raised by the server (e.g. not a
                    //   valid value for Int32, or attempted to divide by zero).
                    alert(err.Message);
                }
            });
        }
    }

    function BindBranchddl(msg) {
        $("#BranchDropDownList").append($("<option></option>").val('').html('-select-'));
        $.each(msg, function () {
            $("#BranchDropDownList").append($("<option></option>").val(this['ID']).html(this['Name']));
        });
    }

    var customerArr = [];
    var infoContent = [];

    $('#slidenav').hide();

    $(document).ready(function () {


        //jQuery(function ($) {
        //    // Asynchronously Load the map API 
        //    var script = document.createElement('script');
        //    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
        //    document.body.appendChild(script);
        //});

        $('#btnSearch').click(function () {
            if ($('#dtFrom').val() == '' || $('#dtFrom').val() == '-select-') {
                //alert('Please select a branch!');
                $('#legend').slideUp();
                $('#error-div').slideUp();
                $('#error-div').slideDown('slow');
                $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  The Date Field is required!');
                $('#map_wrapper').fadeOut();
                $('#loader').fadeOut();

                $('#slidenav').hide();

                return false;
            }
            if ($('#BandInternalDropDownList').val() == '' || $('#BandInternalDropDownList').val() == '-select-') {
                //alert('Please select a branch!');
                $('#legend').slideUp();
                $('#error-div').slideUp();
                $('#error-div').slideDown('slow');
                $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  The Band Field is required!');
                $('#map_wrapper').fadeOut();
                $('#loader').fadeOut();

                $('#slidenav').hide();

                return false;
            }
            if ($('#TypeDropDownList').val() == '' || $('#TypeDropDownList').val() == '-select-') {
                //alert('Please select a branch!');
                $('#legend').slideUp();
                $('#error-div').slideUp();
                $('#error-div').slideDown('slow');
                $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  The Geo Type Field is required!');
                $('#map_wrapper').fadeOut();
                $('#loader').fadeOut();

                $('#slidenav').hide();

                return false;
            }
            else {
                $('#loader').fadeOut();
                $('#loader').fadeIn();
                $('#map_wrapper').fadeOut();
                var transactionType;
                var regionID = document.getElementById("RegionDropDownList").value;
                var branchID = document.getElementById("BranchDropDownList").value;
                if (regionID == '') regionID = '0';
                if (branchID == '') branchID = '0';
                var customerPhone = document.getElementById("CustomerPhone").value;
                var agentPhone = document.getElementById("AgentDropDownList").value;
                if (document.getElementById("TransactionTypeDropDownList").value != '')
                    transactionType = $("#TransactionTypeDropDownList option:selected").text();
                else
                    transactionType = document.getElementById("TransactionTypeDropDownList").value;
                var theType = document.getElementById("TypeDropDownList").value;
                var band = document.getElementById("BandInternalDropDownList").value;
                var dtFrom = document.getElementById("dtFrom").value;
                $('#error-div').slideUp();
                $('#map_wrapper').fadeOut();
                //$('#legend').fadeOut();
                $('#slidenav').hide();

                var url = "ViewAgentGeoLocation.aspx/GetLocation";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify({ regionID: regionID, branchID: branchID, customerPhone: customerPhone, agentPhone: agentPhone, transactionType: transactionType, theType: theType, band: band, dtFrom: dtFrom }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $content = $(".panel")
                        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                        $content.slideUp(1500);
                            //$("#timelinecontainer").css("height", "150px");
                            $("#mapcontainer").css("height", "600px");
                        
                        //change text of header based on visibility of content div
                            $(".header-click").text(function () {
                                //change text based on condition
                                return "Show Filter";
                            });

                        $('#map_wrapper').fadeIn();

                        //$('#legend').html(legend);
                        //$('#legend').slideDown('slow');


                        initialize(msg.d);

                        $('#map_wrapper').fadeIn();

                        $('#loader').fadeOut();

                        var markup = "Date: " + dtFrom;
                        if (regionID != 0) markup += " | Region: " + $("#RegionDropDownList option:selected").text();
                        if (branchID != 0) markup += " | Branch: " + $("#BranchDropDownList option:selected").text();
                        if (customerPhone != '' && theType == '2') markup += " | Customer Phone: " + customerPhone;
                        if (agentPhone != 0) markup += " | Agent: " + $("#AgentDropDownList option:selected").text();
                        if (transactionType != '' && theType == '2') markup += " | Transaction Type: " + $("#TransactionTypeDropDownList option:selected").text();
                        markup += " | Geo Type: " + $("#TypeDropDownList option:selected").text();
                        $('#slidenav').html(markup);
                        //$('#slidenav').show();
                        $('#slidenav').animate({
                            top: '0'
                        }, 2000, function () {
                            $(this).slideDown(2000);
                        });

                    },
                    error: function (xhr, err) {
                        var parsedError = JSON.parse(xhr.responseText);
                        //$('#legend').fadeOut();
                        $('#error-div').slideUp();

                        $('#loader').fadeOut();
                        $('#slidenav').hide();
                        $header = $(".header");

                        $content = $(".panel")
                        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                        $content.slideDown(500);
                            //$("#timelinecontainer").css("height", "250px");
                            $("#mapcontainer").css("height", "450px");

                        //getting the next element
                        $content = $header.next();
                        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                        $content.slideDown(500);

                        //change text of header based on visibility of content div
                        $(".header-click").text(function () {
                            //change text based on condition
                            return "Hide Filter";
                        });

                        $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  ' + parsedError.Message);
                        $('#error-div').slideDown('slow');
                    }
                });


            }
            return false;
        });
    });

var tm;
    function initialize(locations) {
        debugger;
        var interval;
        if (document.getElementById("BandInternalDropDownList").value == '1')
            interval = [Timeline.DateTime.MINUTE, Timeline.DateTime.HOUR];
        if (document.getElementById("BandInternalDropDownList").value == '2')
            interval = [Timeline.DateTime.HOUR, Timeline.DateTime.DAY];
        if (document.getElementById("BandInternalDropDownList").value == '3')
            interval = [Timeline.DateTime.DAY, Timeline.DateTime.WEEK];
        if (document.getElementById("BandInternalDropDownList").value == '4')
            interval = [Timeline.DateTime.WEEK, Timeline.DateTime.MONTH];
        
        //$(function () {
        // make some fake data
        var items = [], x;
        for (x = 0; x < locations.length; x++) {
            items.push({
                title: locations[x]['AgentName'],
                start: locations[x]['StartDate'],
                point: {
                    lat: parseFloat(locations[x]['Latitude']),
                    lon: parseFloat(locations[x]['Longitude'])
                },
                options: {
                    size: parseInt(Math.random() * 5),
                    awesomeness: parseInt(Math.random() * 10),
                    description: locations[x]['TransactionType'] + "<br />Time: " + locations[x]['DateLogged']
                }
            })
        }
        // make some themes
        var colors = ["BA4848", "BA4848", "BA4848", "BA4848", "BA4848"]
        for (var size = 0; size < 5; size++) {
            for (var awe = 0; awe < colors.length; awe++) {
                // Create a new theme and add it to the TimeMap.themes namespace
                // (allowing your data to refer to it by string key). You could make 
                // other kinds of themes with a new TimeMapTheme() instead
                // if you wanted - the concept is the same.
                TimeMap.themes['theme' + size + '-' + awe] = TimeMapTheme.createCircleTheme({
                    size: 15,
                    color: colors[awe]
                });
            }
        }

        tm = TimeMap.init({
            mapId: "map",               // Id of map div element (required)
            timelineId: "timeline", 		// Id of timeline div element (required)
            datasets: [
                {
                    type: "basic",
                    options: {
                        mapZoom: 0,
                        items: items,
                        infoTemplate: '<b>{{title}}</b><div>Type: {{description}}</div>',
                        // use the transformFunction to add the theme before loading
                        transformFunction: function (item) {
                            item.options.theme = "theme" + item.options.size +
                                "-" + parseInt(item.options.awesomeness / 2); // range 0-9, colors 0-5
                            return item;
                        }
                    }
                }
            ],
            bandIntervals: interval
        });
        // manipulate the timemap further here if you like
        //});
    }
</script>

