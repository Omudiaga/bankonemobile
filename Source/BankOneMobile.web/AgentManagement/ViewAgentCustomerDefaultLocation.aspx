﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="ViewAgentCustomerDefaultLocation.aspx.cs" Inherits="AgentLocationManagement_ViewAgentCustomerDefaultLocation" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Customer Geo Location Tracking System</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAASI0kCI-azC8RgbOZzWc3VRRarOQe_TKf_51Omf6UUSOFm7EABRRhO0PO4nBAO9FCmVDuowVwROLo3w" type="text/javascript"></script>
    <script type="text/javascript" src="../lib/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../lib/mxn/mxn.js?(google)"></script>

    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles/jquery.datetimepicker.css" />
    <script src="../lib/js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="../lib/js/jquery.ui.combobox.js" type="text/javascript"></script>

    <link href="../Styles/examples.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        body {
            margin: 0px;
            padding: 0px;
        }

        .container {
            width: 100%;
        }

            .container div {
                width: 100%;
            }

            .container .header {
                padding: 4px;
                cursor: pointer;
                font-weight: bold;
                font-size: 11px;
            }

            .container .content {
                /*display: none;
                padding: 5px;*/
            }

        .myclass {
            height: 18px;
            color: rgba(128,128,128,1);
            border-left: 5px solid #6B869F !important;
            padding-left: 3px;
        }


        .top {
            background: #6B869F;
            height: 3px;
            -webkit-box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
            -moz-box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
            box-shadow: 0px 0px 20px 7px rgba(128,128,128,1);
        }

        .filterTable {
            margin: 0px 5px;
            font-size: 15px;
        }

        .fitContent {
            font-size: 13px;
            width: 1%;
            white-space: nowrap;
            padding: 0px 3px;
        }

        .fillContent {
            width: 250px;
        }

        .myDiv {
            position: relative;
            z-index: 14;
        }

        .pull-right {
            float: right;
            padding-right: 10px;
            font-size: 10px;
            font-weight: bold;
            color: rgba(128,128,128,1);
        }

        #error-div {
            background: #fbb7b7;
            border: 1px solid #ff7575;
            display: none;
            width: 95%;
            margin: 10px auto;
            padding: 6px;
            font-size: 13px;
            border-radius: 5px;
        }

        #map_wrapper {
            height: 600px;
            margin: 10px;
            display: none;
        }

        #map_canvas {
            width: 100%;
            height: 100%;
        }

        .info_content p {
            margin: 0px;
            padding: 0px;
            font-size: 13px;
        }

        .info_content h3 {
            margin: 0px;
            padding: 0px;
            font-size: 14px;
        }

        #legend {
            font-size: 10px;
            font-weight: bold;
            border: 1px solid #ddd;
            margin: 1em;
            padding: 10px;
            background: #fefbfb;
            display: none;
        }

        .legend-inner {
            display: inline;
            height: 5px;
            width: 10px;
            margin: 0px 5px;
            padding: 3px 5px;
            border-radius: 50px;
            color: gray;
        }

        .loader {
            display: none;
            width: 300px;
            margin: 5px auto;
            font-size: 13px;
            color: gray;
        }

        #MapPanel {
            margin: 0px 5px;
        }

        #slidenav {
            margin: 0 auto;
            width: 180px;
            background: #6B869F;
            height: 30px;
            padding: 10px;
            position: absolute;
            z-index:1000;
            right:15px;
            border-bottom:2px solid rgba(128,128,128,1);
            border-left:2px solid rgba(128,128,128,1);
            border-right:2px solid rgba(128,128,128,1);
            border-bottom-left-radius:5px;
            border-bottom-right-radius:5px;
            display:none;
            color:white;
            font-size:13px;
        }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div class="loading-container">
        <%--<div class="cssload-wrap">
	<div class="cssload-container">
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
	<span class="cssload-dots"></span>
</div>
</div>--%>
    </div>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="top"></div>
        <div class="container">
            <div class="header">
                <span></span>

            </div>
            <div class="content">
                <div class="myDiv">
                    <div id="error-div" class="error">
                    </div>
                    <fieldset>
                        <legend>Filters</legend>
                        <table class="filterTable">
                            <tr>
                                <td class="fitContent">Branch:
                                </td>
                                <td class="fillContent">
                                    <asp:DropDownList ID="BranchDropDownList" runat="server" CssClass="form-control" Height="23px" ClientIDMode="Static"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="btn btn-success" href="/Phone/Create" id="btnSearch" runat="server">
                                        <i id="filtersubmit" class="fa fa-search"></i>&nbsp;Search
                                    </a>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>

        <div id="slidenav">
            
        </div>

        <div class="loader" id="loader">
            <table>
                <tr>
                    <td>
                        <img src="../Styles/img/loader.gif" />
                    </td>
                    <td>Processing...  
                    </td>
                </tr>
            </table>
        </div>


        <asp:Panel runat="server" ID="MapPanel">
            <div id="legend">
            </div>
            <div id="map_wrapper">
                <div id="map_canvas" class="mapping"></div>
            </div>
        </asp:Panel>
    </form>
</body>


</html>
<script src="../lib/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript">
    /********************************
    Preloader
    ********************************/
    $(window).load(function () {
        $('.loading-container').fadeOut(1000, function () {
            $(this).remove();
        });
    });

    $(".header").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {
            //execute this after slideToggle is done
            if ($content.is(":visible") == false) {
                //$("#timelinecontainer").css("height", "150px");
                $("#map_wrapper").css("height", "600px");
            }
            else {
                //$("#timelinecontainer").css("height", "250px");
                $("#map_wrapper").css("height", "600px");
            }
            //change text of header based on visibility of content div
            $header.text(function () {
                //change text based on condition
                return $content.is(":visible") ? "" : "";
            });
        });

    });

    var customerArr = [];
    var infoContent = [];

    $('#slidenav').hide();


    $(document).ready(function () {


        jQuery(function ($) {
            // Asynchronously Load the map API 
            var script = document.createElement('script');
            script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
            document.body.appendChild(script);
        });

        $('#btnSearch').click(function () {
            if ($('#BranchDropDownList').val() == '' || $('#BranchDropDownList').val() == '-select-') {
                //alert('Please select a branch!');
                $('#legend').slideUp();
                $('#error-div').slideUp();
                $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  Please select a branch!');
                $('#error-div').slideDown('slow');
                $('#map_wrapper').fadeOut();
                $('#loader').fadeOut();

                $('#slidenav').hide();


                //$('#error').html('Please select a branch!').slideDown();
            }
            else {
                $('#loader').fadeOut();
                $('#loader').fadeIn();
                $('#map_wrapper').fadeOut();
                var branchID = document.getElementById("BranchDropDownList").value;
                $('#error-div').slideUp();
                $('#map_wrapper').fadeOut();
                $('#legend').fadeOut();
                $('#slidenav').hide();

                $.ajax({
                    type: "POST",
                    url: "ViewAgentCustomerDefaultLocation.aspx/GetData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ branch: branchID }),
                    success: function (response) {
                        var names = response.d;
                        //alert(names);
                        var parsed = JSON.parse(names);

                        var legend = "";
                        var colors = [];

                        for (var x in parsed) {
                            customerArr.push([parsed[x].CustomerName, parsed[x].Latitude, parsed[x].Longitude, parsed[x].PinColor]);
                            infoContent.push(['<div class="info_content"><h3>' + parsed[x].CustomerName + '</h3><p><b>Agent</b>:' + parsed[x].AgentName + '</p></div>']);
                            //alert(parsed[x].AgentName);


                            if ($.inArray(parsed[x].PinColor, colors) > -1) {

                            }
                            else {
                                legend += '<div class="legend-inner" style="background:#' + parsed[x].PinColor + '"></div>' + parsed[x].AgentName + ' (' + parsed[x].NumberOfAgentCustomers + ')';
                            }
                            colors.push(parsed[x].PinColor);
                        }

                        //for (var x in customerArr) {
                        //    alert(x);
                        //}

                        $('#map_wrapper').fadeIn();

                        $('#legend').html(legend);
                        $('#legend').slideDown('slow');
                        initialize();
                        $('#loader').fadeOut();

                        $('#slidenav').html('<i id="fci" class="fa fa-users"></i>  Total Agents: ' + parsed[0].TotalAgents + '<br/><i id="fci" class="fa fa-users"></i>  Total Customers: ' + parsed[0].TotalCustomers);
                        //$('#slidenav').show();
                        $('#slidenav').animate({
                            top: '0'
                        }, 2000, function () {
                            $(this).slideDown('slow');
                        });

                    },
                    error: function (xhr, err) {
                        var parsedError = JSON.parse(xhr.responseText);
                        $('#legend').fadeOut();
                        $('#error-div').slideUp();
                        $('#error-div').html('<i id="fci" class="fa fa-warning"></i>  ' + parsedError.Message);
                        $('#error-div').slideDown('slow');
                        $('#loader').fadeOut();
                        $('#slidenav').hide();
                    }
                });


            }
            return false;
        });
    });




    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        map.setTilt(45);

        // Multiple Markers
        var markers = customerArr;

        // Info Window Content
        var infoWindowContent = infoContent;

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;




        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));

        // Loop through our array of markers & place each one on the map  
        for (i = 0; i < markers.length; i++) {
            var pinColor = markers[i][3];
            var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34));
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0],
                icon: pinImage,
                shadow: pinShadow
            });

            // Allow each marker to have an info window    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }

        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        //var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
        //    this.setZoom(14);
        //    google.maps.event.removeListener(boundsListener);
        //});

    }

</script>

