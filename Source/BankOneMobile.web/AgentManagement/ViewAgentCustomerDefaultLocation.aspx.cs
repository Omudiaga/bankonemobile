﻿using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Services;
using BankOneMobile.UI.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public partial class AgentLocationManagement_ViewAgentCustomerDefaultLocation : System.Web.UI.Page
{
    


    public static IDictionary<int, string> GetAll<TEnum>() where TEnum : struct
    {
        var enumerationType = typeof(TEnum);

        if (!enumerationType.IsEnum)
            throw new ArgumentException("Enumeration type is expected.");

        var dictionary = new Dictionary<int, string>();

        foreach (int value in Enum.GetValues(enumerationType))
        {
            var name = Enum.GetName(enumerationType, value);
            dictionary.Add(value, name);
        }

        return dictionary;
    }

    public string DropDownSelectedValue
    {
        get { return this.BranchDropDownList.SelectedValue; }
        set { this.BranchDropDownList.SelectedValue = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        btnSearch.ServerClick += new EventHandler(btnSearch_Click);
        if (!IsPostBack)
        {
            string institutionCode = BankOneMobile.UI.Logic.UserLogic.MFBCode;
            List<ListItem> list = new List<ListItem>();
            BranchServiceReference.Branch[] branches = null;
            using (var client = new BranchServiceReference.BranchServiceClient())
            {
                branches = client.RetrieveAll(institutionCode);
            }
            if (branches != null)
            {
                list = new List<ListItem>();
                foreach (var branch in branches)
                {
                    list.Add(new ListItem(branch.Name.ToUpper(), branch.ID.ToString()));
                }
                list = list.OrderBy(a => a.Text).ToList();
                BranchDropDownList.DataSource = list;
                BranchDropDownList.DataTextField = "Text";
                BranchDropDownList.DataValueField = "Value";
                BranchDropDownList.DataBind();

                BranchDropDownList.Items.Insert(0, "-select-");
            }
        }
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetData(string branch)
    {
        //throw new Exception("No Data To Display!");

        List<ICustomerGeoLocation> customerLocations = new List<ICustomerGeoLocation>();
        List<Location> locations = new List<Location>();
        System.Diagnostics.Trace.TraceError(string.Format("About to get customer locations"));
        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
        {
            customerLocations = new CustomerGeoLocationSystem().GetByInstitutionCode(source, BankOneMobile.UI.Logic.UserLogic.MFBCode);
        }
        System.Diagnostics.Trace.TraceError(string.Format("Done getting customer locations. Count: {0}", customerLocations == null ? 0 : customerLocations.Count));
        if (customerLocations != null && customerLocations.Count > 0)
        {
            var customerPhoneNumbers = customerLocations.Select(x => x.CustomerPhoneNumber).ToList();
            var customerIDs = (from p in new MobileAccountSystem().GetAllMobileAccounts() where customerPhoneNumbers.Contains(p.MobilePhone) select p.CustomerID).Distinct().ToList();
            BankOneMobile.Services.SwitchServiceRef.Customer[] customers = null;
            try
            {
                System.Diagnostics.Trace.TraceError(string.Format("About to get customers from corebanking. Count: {0}", customerIDs.Count));
                using (var client = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    customers = client.GetCustomers(BankOneMobile.UI.Logic.UserLogic.MFBCode, customerIDs.ToArray());
                }
                System.Diagnostics.Trace.TraceError(string.Format("Done getting customers from corebanking. Count: {0}", customers.Length));

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(string.Format("Web Service Error while trying to get Customers from CoreBanking:{0}", ex.Message));
                throw new Exception(ex.Message);
            }
            List<CustomerLocationsModel> customersLocationsModel = new List<CustomerLocationsModel>();
            if (customers != null && customers.Length > 0)
            {
                foreach (var location in customerLocations)
                {
                    if (customers.FirstOrDefault(x => x.PhoneNo == location.CustomerPhoneNumber) == null)
                    {
                        continue;
                    }
                    customersLocationsModel.Add(new CustomerLocationsModel
                    {
                        AgentPhoneNumber = location.AgentPhoneNumber,
                        CustomerPhoneNumber = location.CustomerPhoneNumber,
                        BranchID = customers.FirstOrDefault(x => x.PhoneNo == location.CustomerPhoneNumber).BranchID,
                        Latitude = location.Latitude,
                        Longitude = location.Longitude
                    });
                }

                long b = Convert.ToInt64(branch);
                customersLocationsModel = customersLocationsModel.Where(x => x.BranchID == b).ToList();
                if (customersLocationsModel.Count > 0)
                {
                    var groupByAgent = customersLocationsModel.GroupBy(x => x.AgentPhoneNumber).ToDictionary(x => x.Key, x => x.ToList());
                    foreach (var agent in groupByAgent)
                    {
                        var random = new Random();
                        var color = String.Format("{0:X6}", random.Next(0x1000000));
                        foreach (var location in agent.Value)
                        {
                            var custMob = new MobileAccountSystem().GetMobileAccountsByPhoneNumber(location.CustomerPhoneNumber).FirstOrDefault();
                            var agentMob = new MobileAccountSystem().GetMobileAccountsByPhoneNumber(location.AgentPhoneNumber).FirstOrDefault();
                            locations.Add(new Location
                            {
                                AgentName = agentMob == null ? "" : agentMob.LastName + " " + agentMob.OtherNames,
                                CustomerName = custMob == null ? "" : custMob.LastName + " " + custMob.OtherNames,
                                Latitude = location.Latitude,
                                Longitude = location.Longitude,
                                PinColor = color,
                                NumberOfAgentCustomers = agent.Value.Count.ToString(),
                                TotalAgents = groupByAgent.Count.ToString(),
                                TotalCustomers = customerLocations.Count.ToString()
                            });
                        }
                    }
                }
                else
                    throw new Exception("No Data To Display for this Branch!");
            }

        }
        else
            throw new Exception("No Data To Display!");

        var jsonSerialiser = new JavaScriptSerializer();
        var json = jsonSerialiser.Serialize(locations);
        return json;
    }
    public class Location
    {
        public string AgentName { get; set; }
        public string CustomerName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PinColor { get; set; }
        public string NumberOfAgentCustomers { get; set; }
        public string TotalAgents { get; set; }
        public string TotalCustomers { get; set; }
    }

    public class CustomerLocationsModel
    {
        public string AgentPhoneNumber { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public long BranchID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}