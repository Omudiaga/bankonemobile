﻿using BankOneMobile.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankoneMobile.AdvansLafayette.Core
{
    public interface ICoreIDType : IEntity , IEnableDisable
    {
        string Name { get; set; }
        string Code {get;set;}
    }
}
