﻿using BankoneMobile.AdvansLafayette.AdvRef;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BankoneMobile.AdvansLafayette.Services
{
   public  class ServiceInterfaceSystem
    {
       public List<IDType> GetIDTypes()
       {
           List<IDType> toReturn = new List<IDType>();
                   List<AdvRef.IDType> resp = new List<AdvRef.IDType>();
           string instCode ="100127";
           Trace.TraceInformation("starting XXX");
           using (AdvRef.SwitchingServiceClient cl = new AdvRef.SwitchingServiceClient())
           {
              // var res = cl.GetAccountByAccountNo(instCode, instCode);
              // var respon = cl.GetProducts(instCode);
               var response =cl.GetIDTypes(instCode);
               
               

               if (response == null)
               {
                   Trace.TraceInformation("Response Is Null");
                   resp=null;
               }
               else if (response[0] == null)
               {
                   Trace.TraceInformation("First Item Is Null");
                   resp=null;
               }
               else
               {
                   Trace.TraceInformation("First Item Is {0}",response[0].Code);
                   resp = response.ToList();
                   Trace.TraceInformation("After Conversion Reponse Is {0}", resp[0].Code);
                   
               }
               
           }
           foreach (AdvRef.IDType tp in resp)
           {
               toReturn.Add(new IDType { Code = tp.Code, Name = tp.Name });
           }
           Trace.TraceInformation("After Addition Reponse Is {0}", toReturn[0].Code);
           return toReturn;
       }
       public bool SaveImages(string acountNumber, string customerID, byte[] Image)
       {
           
           string instCode = "100127";
           bool toReturn = false;
           Trace.TraceInformation("About to Upload Photo . Inst Code is {0}, Account Number is {1} Customer ID is {2} Image is {3}", instCode,acountNumber,customerID,Image==null?"Image is Null":Convert.ToString(Image.Count()));

           try
           {
               using (AdvRef.SwitchingServiceClient cl = new AdvRef.SwitchingServiceClient())
               {
                   toReturn = cl.UploadCustomerPhoto(instCode, customerID, acountNumber, Image);
                   Trace.TraceInformation("Uploaded Photo. Photo Upload Status is {0}", toReturn);
                   //toReturn = cl.UploadCustomerSignature(instCode, customerID, acountNumber, Image);
                   //Trace.TraceInformation("Signature Upload Status Complete. Signature Upload Status is {0} ", toReturn);
               }
               return toReturn;
           }
           catch (Exception ex)
           {
               Trace.TraceInformation("About to Upload Signature. Photo Upload Status is {0} ", toReturn);
               return false;
           }
       }
    }
}
