﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PANE.Framework.Functions.DAO;
using PANE.Framework.Functions.DTO;

namespace PANE.Framework.Functions
{
    public class FunctionsEngine
    {
        public static List<UserRole> GetRoles(string mfbCode)
        {
            return UserRoleDAO.RetrieveAll(mfbCode);
        }
    }
}
