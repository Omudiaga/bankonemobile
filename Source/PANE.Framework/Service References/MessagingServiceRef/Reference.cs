﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PANE.Framework.MessagingServiceRef {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataObject", Namespace="http://schemas.datacontract.org/2004/07/PANE.Framework.DTO")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PANE.Framework.MessagingServiceRef.Message))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PANE.Framework.MessagingServiceRef.Mail))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PANE.Framework.MessagingServiceRef.SMS))]
    public partial class DataObject : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsDeletedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool LogObjectField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MFBCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool UseAuditTrailField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsDeleted {
            get {
                return this.IsDeletedField;
            }
            set {
                if ((this.IsDeletedField.Equals(value) != true)) {
                    this.IsDeletedField = value;
                    this.RaisePropertyChanged("IsDeleted");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool LogObject {
            get {
                return this.LogObjectField;
            }
            set {
                if ((this.LogObjectField.Equals(value) != true)) {
                    this.LogObjectField = value;
                    this.RaisePropertyChanged("LogObject");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MFBCode {
            get {
                return this.MFBCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.MFBCodeField, value) != true)) {
                    this.MFBCodeField = value;
                    this.RaisePropertyChanged("MFBCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool UseAuditTrail {
            get {
                return this.UseAuditTrailField;
            }
            set {
                if ((this.UseAuditTrailField.Equals(value) != true)) {
                    this.UseAuditTrailField = value;
                    this.RaisePropertyChanged("UseAuditTrail");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Message", Namespace="http://schemas.datacontract.org/2004/07/Messaging.Core")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PANE.Framework.MessagingServiceRef.Mail))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PANE.Framework.MessagingServiceRef.SMS))]
    public partial class Message : PANE.Framework.MessagingServiceRef.DataObject {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AccountNoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BodyField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateCreatedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateSentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string InstitutionNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long MfbIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int NoOfAttemptsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ReferenceNoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PANE.Framework.MessagingServiceRef.MsgStatus StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusMsgField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ToField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AccountNo {
            get {
                return this.AccountNoField;
            }
            set {
                if ((object.ReferenceEquals(this.AccountNoField, value) != true)) {
                    this.AccountNoField = value;
                    this.RaisePropertyChanged("AccountNo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Body {
            get {
                return this.BodyField;
            }
            set {
                if ((object.ReferenceEquals(this.BodyField, value) != true)) {
                    this.BodyField = value;
                    this.RaisePropertyChanged("Body");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateCreated {
            get {
                return this.DateCreatedField;
            }
            set {
                if ((this.DateCreatedField.Equals(value) != true)) {
                    this.DateCreatedField = value;
                    this.RaisePropertyChanged("DateCreated");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateSent {
            get {
                return this.DateSentField;
            }
            set {
                if ((this.DateSentField.Equals(value) != true)) {
                    this.DateSentField = value;
                    this.RaisePropertyChanged("DateSent");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string InstitutionName {
            get {
                return this.InstitutionNameField;
            }
            set {
                if ((object.ReferenceEquals(this.InstitutionNameField, value) != true)) {
                    this.InstitutionNameField = value;
                    this.RaisePropertyChanged("InstitutionName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long MfbID {
            get {
                return this.MfbIDField;
            }
            set {
                if ((this.MfbIDField.Equals(value) != true)) {
                    this.MfbIDField = value;
                    this.RaisePropertyChanged("MfbID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NoOfAttempts {
            get {
                return this.NoOfAttemptsField;
            }
            set {
                if ((this.NoOfAttemptsField.Equals(value) != true)) {
                    this.NoOfAttemptsField = value;
                    this.RaisePropertyChanged("NoOfAttempts");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ReferenceNo {
            get {
                return this.ReferenceNoField;
            }
            set {
                if ((object.ReferenceEquals(this.ReferenceNoField, value) != true)) {
                    this.ReferenceNoField = value;
                    this.RaisePropertyChanged("ReferenceNo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PANE.Framework.MessagingServiceRef.MsgStatus Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StatusMsg {
            get {
                return this.StatusMsgField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusMsgField, value) != true)) {
                    this.StatusMsgField = value;
                    this.RaisePropertyChanged("StatusMsg");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string To {
            get {
                return this.ToField;
            }
            set {
                if ((object.ReferenceEquals(this.ToField, value) != true)) {
                    this.ToField = value;
                    this.RaisePropertyChanged("To");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Mail", Namespace="http://schemas.datacontract.org/2004/07/Messaging.Core")]
    [System.SerializableAttribute()]
    public partial class Mail : PANE.Framework.MessagingServiceRef.Message {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SubjectField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Subject {
            get {
                return this.SubjectField;
            }
            set {
                if ((object.ReferenceEquals(this.SubjectField, value) != true)) {
                    this.SubjectField = value;
                    this.RaisePropertyChanged("Subject");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SMS", Namespace="http://schemas.datacontract.org/2004/07/Messaging.Core")]
    [System.SerializableAttribute()]
    public partial class SMS : PANE.Framework.MessagingServiceRef.Message {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MsgStatus", Namespace="http://schemas.datacontract.org/2004/07/Messaging.Core.Enum")]
    public enum MsgStatus : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Failed = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Pending = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Successful = 2,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MailConfigurationManager", Namespace="http://schemas.datacontract.org/2004/07/Messaging.Extensions")]
    [System.SerializableAttribute()]
    public partial class MailConfigurationManager : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool EnableSSLField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string HostField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MailTemplatePathField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PasswordField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PortField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ServiceIntervalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UseConfigSettingsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UserNameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool EnableSSL {
            get {
                return this.EnableSSLField;
            }
            set {
                if ((this.EnableSSLField.Equals(value) != true)) {
                    this.EnableSSLField = value;
                    this.RaisePropertyChanged("EnableSSL");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Host {
            get {
                return this.HostField;
            }
            set {
                if ((object.ReferenceEquals(this.HostField, value) != true)) {
                    this.HostField = value;
                    this.RaisePropertyChanged("Host");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MailTemplatePath {
            get {
                return this.MailTemplatePathField;
            }
            set {
                if ((object.ReferenceEquals(this.MailTemplatePathField, value) != true)) {
                    this.MailTemplatePathField = value;
                    this.RaisePropertyChanged("MailTemplatePath");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password {
            get {
                return this.PasswordField;
            }
            set {
                if ((object.ReferenceEquals(this.PasswordField, value) != true)) {
                    this.PasswordField = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Port {
            get {
                return this.PortField;
            }
            set {
                if ((object.ReferenceEquals(this.PortField, value) != true)) {
                    this.PortField = value;
                    this.RaisePropertyChanged("Port");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ServiceInterval {
            get {
                return this.ServiceIntervalField;
            }
            set {
                if ((object.ReferenceEquals(this.ServiceIntervalField, value) != true)) {
                    this.ServiceIntervalField = value;
                    this.RaisePropertyChanged("ServiceInterval");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UseConfigSettings {
            get {
                return this.UseConfigSettingsField;
            }
            set {
                if ((object.ReferenceEquals(this.UseConfigSettingsField, value) != true)) {
                    this.UseConfigSettingsField = value;
                    this.RaisePropertyChanged("UseConfigSettings");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserName {
            get {
                return this.UserNameField;
            }
            set {
                if ((object.ReferenceEquals(this.UserNameField, value) != true)) {
                    this.UserNameField = value;
                    this.RaisePropertyChanged("UserName");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MessagingServiceRef.IMessageLoggerService")]
    public interface IMessageLoggerService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMessageLoggerService/DoWork", ReplyAction="http://tempuri.org/IMessageLoggerService/DoWorkResponse")]
        void DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMessageLoggerService/SaveSMSLog", ReplyAction="http://tempuri.org/IMessageLoggerService/SaveSMSLogResponse")]
        void SaveSMSLog(string mfbCode, PANE.Framework.MessagingServiceRef.SMS sms);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMessageLoggerService/SaveMailLog", ReplyAction="http://tempuri.org/IMessageLoggerService/SaveMailLogResponse")]
        void SaveMailLog(string mfbCode, PANE.Framework.MessagingServiceRef.Mail mail, PANE.Framework.MessagingServiceRef.MailConfigurationManager Cfg);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMessageLoggerService/SaveSMSLogByList", ReplyAction="http://tempuri.org/IMessageLoggerService/SaveSMSLogByListResponse")]
        void SaveSMSLogByList(string mfbCode, PANE.Framework.MessagingServiceRef.SMS[] sms);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMessageLoggerService/SaveMailLogByList", ReplyAction="http://tempuri.org/IMessageLoggerService/SaveMailLogByListResponse")]
        void SaveMailLogByList(string mfbCode, PANE.Framework.MessagingServiceRef.Mail[] mail);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMessageLoggerServiceChannel : PANE.Framework.MessagingServiceRef.IMessageLoggerService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MessageLoggerServiceClient : System.ServiceModel.ClientBase<PANE.Framework.MessagingServiceRef.IMessageLoggerService>, PANE.Framework.MessagingServiceRef.IMessageLoggerService {
        
        public MessageLoggerServiceClient() {
        }
        
        public MessageLoggerServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MessageLoggerServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MessageLoggerServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MessageLoggerServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void DoWork() {
            base.Channel.DoWork();
        }
        
        public void SaveSMSLog(string mfbCode, PANE.Framework.MessagingServiceRef.SMS sms) {
            base.Channel.SaveSMSLog(mfbCode, sms);
        }
        
        public void SaveMailLog(string mfbCode, PANE.Framework.MessagingServiceRef.Mail mail, PANE.Framework.MessagingServiceRef.MailConfigurationManager Cfg) {
            base.Channel.SaveMailLog(mfbCode, mail, Cfg);
        }
        
        public void SaveSMSLogByList(string mfbCode, PANE.Framework.MessagingServiceRef.SMS[] sms) {
            base.Channel.SaveSMSLogByList(mfbCode, sms);
        }
        
        public void SaveMailLogByList(string mfbCode, PANE.Framework.MessagingServiceRef.Mail[] mail) {
            base.Channel.SaveMailLogByList(mfbCode, mail);
        }
    }
}
