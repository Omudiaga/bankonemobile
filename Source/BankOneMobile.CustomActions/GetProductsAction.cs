﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetProductsDesigner))]
    public class GetProductsAction : ActionNode
    {
      
       

        /// <summary>
        /// List of Products
        /// </summary>
        public OutArgument<IList<Product>> Products
        {
            get
            {
                GetProductsActivity activity = this.InternalState as GetProductsActivity;
                if (activity != null)
                {
                    return activity.Products;
                }
                return null;
            }
            set
            {
                GetProductsActivity activity = this.InternalState as GetProductsActivity;
                if (activity != null)
                {
                    activity.Products = value;
                }
            }
        }

        public GetProductsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetProductsActivity();
        }

    }

    public class GetProductsActivity : CodeActivity
    {
        
        
        public OutArgument<IList<Product>> Products { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
           

            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            IList<Product> products = null;
            try
            {
                products =
                 new ProductSystem(currentWorkflowInfo.DataSource).GetProductsByAgentPhone(mob.MobilePhone);
            }
            catch (FaultException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), "Core Banking Unavailable");
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), ex.Message);
            }
         Products.Set(context, products);
            

           

        }
    }
}
