﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankoneMobile.AdvansLafayette;
using System.Diagnostics;
using System.Xml;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(AccountToAccountTransferDesigner))]
    public class AccountToAccountTransferAction : ActionNode
    {

        public InArgument<bool> IsDiamondBank
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsDiamondBank;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsDiamondBank = value;
                }
            }
        }
        public InArgument<bool> IsToDiamondBank
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsToDiamondBank;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsToDiamondBank = value;
                }
            }
        }

        public InArgument<bool> IsToOtherBanks
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsToOtherBanks;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsToOtherBanks = value;
                }
            }
        }

        public InArgument<bool> IsIntraBank
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsIntraBank;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsIntraBank = value;
                }
            }
        }

        public InArgument<bool> IsInterBank
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsInterBank;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsInterBank = value;
                }
            }
        }
        public InArgument<bool> IsNonAccountHolder
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsNonAccountHolder;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsNonAccountHolder = value;
                }
            }
        }
        public InArgument<string> AccountNumberFrom
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNumberFrom;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.AccountNumberFrom = value;
                }
            }
        }
        public InArgument<string> AccountNumberTo
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNumberTo;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.AccountNumberTo = value;
                }
            }
        }

        public InArgument<string> AccountNameTo
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNameTo;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.AccountNameTo = value;
                }
            }
        }
        public InArgument<string> Amount
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }

        public InArgument<string> PIN
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        public InArgument<string> DestinationInstitutionCode
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.DestinationInstitutionCode;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.DestinationInstitutionCode = value;
                }
            }
        }

        public InArgument<string> BeneficiaryBankVerificationNumber
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.BeneficiaryBankVerificationNumber;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.BeneficiaryBankVerificationNumber = value;
                }
            }
        }

        public InArgument<string> BeneficiaryKYCLevel
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.BeneficiaryKYCLevel;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.BeneficiaryKYCLevel = value;
                }
            }
        }

        public InArgument<string> AccountNameEnquirySessionID
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNameEnquirySessionID;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.AccountNameEnquirySessionID = value;
                }
            }
        }

        /// <summary>
        /// Contains the Token and Record Type(Debit/Credit) separated by a semi colon.e.g "35467;Credit"
        /// </summary>
        public InArgument<string> TokenAndRecordType
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.TokenAndRecordType;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.TokenAndRecordType = value;
                }
            }
        }

        /// <summary>
        /// Transaction successful
        /// </summary>
        public OutArgument<string> TransactionSuccessful
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.TransactionSuccessful;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.TransactionSuccessful = value;
                }
            }
        }

        public InArgument<DBNFundsTransferTransactionType> TransactionType
        {
            get
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    return activity.TransactionType;
                }
                return null;
            }
            set
            {
                AccountToAccountTransferActivity activity = this.InternalState as AccountToAccountTransferActivity;
                if (activity != null)
                {
                    activity.TransactionType = value;
                }
            }
        }    
       

        public AccountToAccountTransferAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new AccountToAccountTransferActivity();
        }

    }

    public class AccountToAccountTransferActivity : CodeActivity
    {
        public InArgument<bool> IsDiamondBank { get; set; }
        public InArgument<bool> IsToDiamondBank { get; set; }
        public InArgument<bool> IsToOtherBanks { get; set; }
        public InArgument<bool> IsIntraBank { get; set; }
        public InArgument<bool> IsInterBank { get; set; }
        public InArgument<bool> IsNonAccountHolder { get; set; }
        /// <summary>
        /// Beta savings account number
        /// </summary>
        public InArgument<string> AccountNumberFrom { get; set; }
        /// <summary>
        /// Destination Banks account number
        /// </summary>
        public InArgument<string> AccountNumberTo { get; set; }
        public InArgument<string> AccountNameTo { get; set; }
        
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> PIN { get; set; }
        public InArgument<string> Amount { get; set; }
        public InArgument<string> DestinationInstitutionCode { get; set; }
        public InArgument<string> BeneficiaryBankVerificationNumber { get; set; }
        public InArgument<string> BeneficiaryKYCLevel { get; set; }
        public InArgument<string> AccountNameEnquirySessionID { get; set; }
        public InArgument<string> TokenAndRecordType { get; set; }
        public InArgument<DBNFundsTransferTransactionType> TransactionType { get; set; }
        public OutArgument<string> TransactionSuccessful { get; set; }     

        
        
        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            bool isDiamondBank = IsDiamondBank.Get(context);
            bool isIntraBank = IsIntraBank.Get(context);
            bool isInterBank = IsInterBank.Get(context);
            bool isToDiamondBank = IsToDiamondBank.Get(context);
            bool isToOtherBanks = IsToOtherBanks.Get(context);
            bool isNonAccountHolder = IsNonAccountHolder.Get(context);
            string accountNumberFrom = AccountNumberFrom.Get(context);
            string accountNumberTo = AccountNumberTo.Get(context);
            string accountNameTo = AccountNameTo.Get(context);
            string amount = Amount.Get(context);
            string pin = PIN.Get(context);
            string realResponse = string.Empty;

            string instCode = string.Empty;
            Agent agent;
            agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (agent == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Non-Agent Initiated Trnsactions");
            }
            instCode = agent.MobileAccount.InstitutionCode;
            mob.InstitutionCode = instCode;
            //TransactionResponse response = new TransactionResponse();
            decimal amountToTransfer = decimal.Parse(amount);
            try
            {
                if (accountNumberFrom == accountNumberTo)
                {
                    throw new Exception("Cannot transfer funds to the same customer");
                }
                if (isIntraBank && isToDiamondBank && isToOtherBanks || isIntraBank && isToDiamondBank || isToDiamondBank && isToOtherBanks
                    || isIntraBank && isToOtherBanks)
                    throw new Exception("Invalid transfer mode checked in flow");
                if (isDiamondBank)
                {
                    if (isIntraBank)
                    {
                        Trace.TraceInformation("Type is IntraBank");
                        var accountTo = new BankOneMobile.Services.SwitchServiceRef.Account();
                        var accountFrom = new BankOneMobile.Services.SwitchServiceRef.Account();
                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                        {
                            accountTo = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumberTo);
                            accountFrom = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumberFrom);
                        }
                        decimal theAmount = decimal.Parse(amount);
                        IFundsTransferFee theFee = new FundsTransferFeeSystem().GetByName(mob.InstitutionCode, TransactionTypeName.DBNBetaToBetaTransfer);
                        if(theFee == null)
                        {
                            throw new Exception("No fee configured for BETA to BETA transfer");
                        }
                        decimal fee = theFee.Amount;
                        int vat = theFee.VATInPercentage;
                        Random rnd = new Random();
                        string paymentReference = "SLF" + DateTime.Now.ToString("ddMMyyyyhhmmss") + rnd.Next(0, 9999).ToString("0000");
                        string debit = String.Format("Mobile Trf to {0} @ {1} ({2})", accountTo == null ? accountNumberTo : accountTo.Name, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                        string credit = String.Format("Mobile Trf from {0}", accountFrom == null ? accountNumberFrom : accountFrom.Name);//, tranType.TheBankAccount.CoreBankingNames, tranType.TheBankAccount.TheMobileAccount.MobilePhone);
                        string narration = String.Format("{0}||{1}", debit, credit);
                        IDBNFundsTransferTransactionType tranType = new DBNFundsTransferTransactionType();
                        tranType.DBNFundsTransferType = DBNFundsTransferTransactionTypeName.BetaToBeta;
                        tranType.Amount = theAmount;
                        tranType.Fee = fee;
                        tranType.VATInPercentage = vat;
                        tranType.BeneficiaryAccountNumber = accountNumberTo;
                        tranType.Narration = narration;
                        tranType.PaymentReference = paymentReference;
                        tranType.OriginatorAccountNumber = accountNumberFrom;
                        tranType.RequestDate = DateTime.Now;
                        tranType.SessionID = sessionID;
                        tranType.DestinationInstitutionCode = instCode;
                        tranType.BeneficiaryAccountName = accountTo.Name;
                        tranType.OriginatorAccountName = accountFrom.Name;
                        tranType.ReversalStatus = DBNReversalStatus.NotApplicable;
                        TransactionResponse response = new TransactionResponse();

                        try
                        {
                            response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumberFrom, accountNumberTo, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                            if (response.Status == TransactionStatus.Successful.ToString())
                            {
                                realResponse = "true";
                            }
                            else
                            {
                                throw new Exception(response.ResponseMessage);
                            }

                        }
                        catch(Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    if (isToDiamondBank)
                    {
                        Trace.TraceInformation("Type is to diamond Bank");
                        var account = new BankOneMobile.Services.SwitchServiceRef.Account();
                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                        {
                            account = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumberFrom);
                        }
                        decimal theAmount = decimal.Parse(amount);
                       
                        IFundsTransferFee theFee = new FundsTransferFeeSystem().GetByName(mob.InstitutionCode, TransactionTypeName.DBNBetaToDiamondTransfer);
                        if (theFee == null)
                        {
                            throw new Exception("No fee configured for BETA To Diamond Transfer");
                        }

                        decimal fee = theFee.Amount;
                        int vat = theFee.VATInPercentage;
                        string transactionReference = "TDB" + DateTime.Now.ToString("ddMMyyhhmmss") + account.CustomerID;
                        string debit = String.Format("Transfer to {0} @ {1} ({2})", accountNameTo, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                        //string credit = String.Format("InterBank Trf from {0}", account == null ? accountNumberFrom : account.Name);//, tranType.TheBankAccount.CoreBankingNames, tranType.TheBankAccount.TheMobileAccount.MobilePhone);
                        string narration = debit;
                        Trace.TraceInformation("About to call diamond bank service");
                        IDBNFundsTransferTransactionType tranType = new DBNFundsTransferTransactionType();
                        tranType.DBNFundsTransferType = DBNFundsTransferTransactionTypeName.BetaToDiamond;
                        tranType.Amount = theAmount;
                        tranType.Fee = fee;
                        tranType.VATInPercentage = vat;
                        tranType.BeneficiaryAccountNumber = accountNumberTo;
                        tranType.DestinationInstitutionCode = mob.InstitutionCode;
                        tranType.Narration = narration;
                        tranType.OriginatorAccountNumber = accountNumberFrom;
                        tranType.PaymentReference = transactionReference;
                        tranType.RequestDate = DateTime.Now;
                        tranType.SessionID = sessionID;
                        tranType.BeneficiaryAccountName = accountNameTo;
                        tranType.OriginatorAccountName = account.Name;
                        tranType.ReversalStatus = DBNReversalStatus.NotApplicable;

                        TransactionResponse trxResponse = new TransactionResponse();

                        try
                        {
                            trxResponse = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumberFrom, accountNumberTo, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                            if (trxResponse.Status == TransactionStatus.Successful.ToString())
                            {
                                realResponse = "true";
                            }
                            else
                            {
                                throw new Exception(trxResponse.ResponseMessage);
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    if (isToOtherBanks)
                    {
                        Trace.TraceInformation("Type is to other Banks");
                        Trace.TraceInformation("*********Transfer to Other Banks Begins*********");
                        Trace.TraceInformation(string.Format("AccountNumber: {0}", accountNumberTo));
                        //Trace.TraceInformation(string.Format("Destination Inst. Code: {0}", destinationInstitutionCode));

                        string beneficiaryBVN = BeneficiaryBankVerificationNumber.Get(context);
                        string beneficiaryKYCLevel = BeneficiaryKYCLevel.Get(context);
                        string destinationInstCode = DestinationInstitutionCode.Get(context);
                        string accountNameEnqSessionID = AccountNameEnquirySessionID.Get(context);

                        Random rnd = new Random();
                        string randomNumber = rnd.Next(0, 999999).ToString("000000") + rnd.Next(0, 999999).ToString("000000");
                        string dt = DateTime.Now.ToString("yyMMddHHmmss");
                        string sessionId = "999063" + dt + randomNumber;


                        var account = new BankOneMobile.Services.SwitchServiceRef.Account();
                        using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                        {
                            account = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumberFrom);
                        }

                        if (account == null)
                        {
                            throw new Exception(string.Format("Could not retrieve account details for {0}", accountNumberFrom));
                        }
                        decimal theAmount = decimal.Parse(amount);
                        IFundsTransferFee theFee = new FundsTransferFeeSystem().GetByName(mob.InstitutionCode, TransactionTypeName.DBNBetaToOtherBanks);
                        if (theFee == null)
                        {
                            throw new Exception("No fee configured for BETA To Other Banks Transfer");
                        }
                        decimal fee = theFee.Amount;
                        int vat = theFee.VATInPercentage;
                        string narration = string.Format("Interbank Transfer to {0} @ {1} ({2})", accountNameTo, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                        string paymentReference = "TEB" + DateTime.Now.ToString("ddMMyyyyhhmmss") + rnd.Next(0, 9999).ToString("0000");
                        Trace.TraceInformation(string.Format("Session ID: {0}", sessionId));
                        IDBNFundsTransferTransactionType tranType = new DBNFundsTransferTransactionType();
                        tranType.DBNFundsTransferType = DBNFundsTransferTransactionTypeName.BetaToOtherBanks;
                        tranType.Amount = theAmount;
                        tranType.Fee = fee;
                        tranType.VATInPercentage = vat;
                        tranType.BeneficiaryAccountNumber = accountNumberTo;
                        tranType.DestinationInstitutionCode = destinationInstCode;
                        tranType.Narration = narration;
                        tranType.OriginatorAccountNumber = accountNumberFrom;
                        tranType.PaymentReference = paymentReference;
                        tranType.RequestDate = DateTime.Now;
                        tranType.SessionID = sessionId;
                        tranType.BeneficiaryAccountName = accountNameTo;
                        tranType.OriginatorAccountName = account.Name;
                        tranType.ReversalStatus = DBNReversalStatus.NotApplicable;
                        tranType.NameEnquirySessionID = accountNameEnqSessionID;
                        tranType.OriginatorBankVerificationNumber = account.BankVerificationNumber;
                        tranType.OriginatorKYCLevel = account.AccountTierID == 4 ? account.PreviousAccountTierID.ToString() : account.AccountTierID.ToString();
                        tranType.BeneficiaryBankVerificationNumber = beneficiaryBVN;
                        tranType.BeneficiaryKYCLevel = beneficiaryKYCLevel;
                        tranType.ChannelCode = "2";

                        TransactionResponse trxResponse = new TransactionResponse();

                        try
                        {
                            trxResponse = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumberFrom, accountNumberTo, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                            if (trxResponse.Status == TransactionStatus.Successful.ToString())
                            {
                                realResponse = "true";
                            }
                            else
                            {
                                throw new Exception(trxResponse.ResponseMessage);
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    if (isNonAccountHolder)
                    {
                        Trace.TraceInformation("Type is to Non Account Holder");
                        if(accountNumberTo == accountNumberFrom)
                        {
                            throw new Exception(string.Format("Phone number {0} cannot be both the originator and the beneficiary.",accountNumberTo));
                        }
                        DBNFundsTransferTransactionType transactionType = TransactionType.Get(context);
                        if(transactionType != null)
                        {

                        }
                        string tokenAndRecordType = TokenAndRecordType.Get(context);
                        string transactionReference = "TNA" + DateTime.Now.ToString("ddMMyyhhmmss") + accountNumberFrom;
                        string narration = String.Format("Non-Account Transfer from {0} to {1} @ {2} ({3})", accountNumberFrom, accountNumberTo, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                        Trace.TraceInformation("About to call service");
                        IDBNFundsTransferTransactionType tranType = new DBNFundsTransferTransactionType();
                        tranType.DBNFundsTransferType = DBNFundsTransferTransactionTypeName.NonAccountHolder;
                        tranType.Amount = decimal.Parse(amount);
                        tranType.BeneficiaryAccountNumber = accountNumberTo;
                        tranType.DestinationInstitutionCode = mob.InstitutionCode;
                        tranType.Narration = narration;
                        tranType.OriginatorAccountNumber = accountNumberFrom;
                        tranType.PaymentReference = transactionReference;
                        tranType.RequestDate = DateTime.Now;
                        tranType.SessionID = tokenAndRecordType;
                        tranType.ReversalStatus = DBNReversalStatus.NotApplicable;

                        TransactionResponse trxResponse = new TransactionResponse();

                        try
                        {
                            trxResponse = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumberFrom, accountNumberTo, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                            if (trxResponse.Status == TransactionStatus.Successful.ToString())
                            {
                                realResponse = "true";
                            }
                            else
                            {
                                throw new Exception(trxResponse.ResponseMessage);
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents);
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));

            }
            catch (InvalidAgentCodeException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.InvalidAgentCode);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.SwitchUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.IssuerUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                realResponse = ex.Message;
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                realResponse = "System Error";
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {
                //TransactionSuccessful.Set(context, realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP...... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }



           //tring result = TransactionSystem.RunTransaction(pin);
            //context.SetValue(Notification,response.ResponseMessage);
            //string result = TransactionSystem.RunTransaction(pin);

            

        }
    }

}
