﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(ReceiveMoneyVerifyCardPANDesigner))]
    public class ReceiveMoneyVerifyCardPANAction : ActionNode
    {
        /// <summary>
        /// User PAN
        /// </summary>
        public InArgument<string> PAN
        {
            get
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    return activity.PAN;
                }
                return null;
            }
            set
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    activity.PAN = value;
                }
            }
        }
        
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> Bank
        {
            get
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    return activity.Bank;
                }
                return null;
            }
            set
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    activity.Bank = value;
                }
            }
        }

        /// <summary>
        /// AccountName on User Mobile Account
        /// </summary>
        public OutArgument<string> AccountName
        {
            get
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    return activity.AccountName;
                }
                return null;
            }
            set
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    activity.AccountName = value;
                }
            }
        }

        /// <summary>
        /// Account Number for speecified PAN and bank
        /// </summary>
        public OutArgument<string> AccountNumber
        {
            get
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                ReceiveMoneyVerifyCardPANActivity activity = this.InternalState as ReceiveMoneyVerifyCardPANActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }


        public ReceiveMoneyVerifyCardPANAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ReceiveMoneyVerifyCardPANActivity();
        }

    }

    public class ReceiveMoneyVerifyCardPANActivity : CodeActivity
    {
        /// <summary>
        /// User PAN
        /// </summary>
        public InArgument<string> PAN { get; set; }
        
        /// <summary>
        /// Selected bank
        /// </summary>
        public InArgument<string> Bank { get; set; }

        /// <summary>
        /// Account Name for speecified PAN and bank
        /// </summary>
        public OutArgument<string> AccountName { get; set; }

        /// <summary>
        /// Account Number for speecified PAN and bank
        /// </summary>
        public OutArgument<string> AccountNumber{ get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //string pin = pin.Get(context);
            //string accountNumber = AccountNumber.Get(context);

            ////TODO: Do Balance Enquiry
            //WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            //string sessionID = currentWorkflowInfo.SessionID;
            //MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            //string phoneNumber = mob.MobilePhone;

            //ITransactionType tranType = new BalanceInquiryTransactionType() ;

            //TransactionResponse response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            //decimal returnValue = Convert.ToDecimal(response.ResponseMessage);
            //Balance.Set(context,string.Format("{0:N}", returnValue));

        }
    }
}
