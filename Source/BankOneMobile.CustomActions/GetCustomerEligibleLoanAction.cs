﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetCustomerEligibleLoanDesigner))]
    public class GetCustomerEligibleLoanAction : ActionNode
    {
        /// <summary>
        /// Loan Product Name
        /// </summary>
        public OutArgument<string> LoanProductName
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.LoanProductName;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.LoanProductName = value;
                }
            }
        }
        public OutArgument<string> LoanProductID
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.LoanProductID;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.LoanProductID = value;
                }
            }
        }
        public OutArgument<string> LoanAmount
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.LoanAmount;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.LoanAmount = value;
                }
            }
        }
        public OutArgument<Int32> BetaFriendPerception
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.BetaFriendPerception;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.BetaFriendPerception = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public OutArgument<string> AccountNumber
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        /// bool is phone number
        /// </summary>
        public InArgument<bool> IsPhoneNumber
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.IsPhoneNumber;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumber = value;
                }
            }
        }
        public InArgument<bool> IsOnCoreBanking
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.IsOnCoreBanking;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.IsOnCoreBanking = value;
                }
            }
        }
        public InArgument<bool> ForLoggedOnCustomer
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.IsForLoggedOnCustomer;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.IsForLoggedOnCustomer = value;
                }
            }
        }

        /// <summary>
        /// account Name
        /// </summary>
        public OutArgument<string> CustomerID
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.CustomerID;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.CustomerID = value;
                }
            }
        }

        /// <summary>
        /// Institution Code
        /// </summary>
        public OutArgument<string> InstitutionCode
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.InstitutionCode;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.InstitutionCode = value;
                }
            }
        }

        public OutArgument<IList<CustomerEligibleLoan>> CustomerEligibleLoans
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.CustomerEligibleLoans;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.CustomerEligibleLoans = value;
                }
            }
        }
        public OutArgument<string> CoreBankingPhoneNumber
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.CoreBankingPhoneNumber;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.CoreBankingPhoneNumber = value;
                }
            }
        }
        public OutArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.CoreBankingCustomer;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.CoreBankingCustomer = value;
                }
            }
        }
        public OutArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount
        {
            get
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    return activity.CoreBankingAccount;
                }
                return null;
            }
            set
            {
                GetCustomerEligibleLoanActivity activity = this.InternalState as GetCustomerEligibleLoanActivity;
                if (activity != null)
                {
                    activity.CoreBankingAccount = value;
                }
            }
        }

        public GetCustomerEligibleLoanAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetCustomerEligibleLoanActivity();
        }

    }

    public class GetCustomerEligibleLoanActivity : CodeActivity
    {
        /// <summary>
        /// Used to check if its being used for the customer doing the transaction
        /// </summary>
        public InArgument<bool> IsForLoggedOnCustomer { get; set; }
        /// <summary>
        /// Used to check if the validation is from Core Banking or not
        /// </summary>
        public InArgument<bool> IsOnCoreBanking { get; set; }
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        /// <summary>
        /// Type OF ID i.e. account Number or Phone Number
        /// </summary>
        public InArgument<bool> IsPhoneNumber { get; set; }
        public InArgument<bool> IsForSelfService { get; set; }
        /// <summary>
        /// Recharge PIN 
        /// </summary>
        public OutArgument<string> AccountName { get; set; }
        public OutArgument<string> AccountNumber { get; set; }
        public OutArgument<string> LoanProductName { get; set; }
        public OutArgument<string> LoanProductID { get; set; }
        public OutArgument<string> LoanAmount { get; set; }
        public OutArgument<string> InstitutionCode { get; set; }
        public OutArgument<Int32> BetaFriendPerception { get; set; }

        /// <summary>
        /// Customer's ID tied to phone number
        /// </summary>
        public OutArgument<string> CustomerID { get; set; }

        /// <summary>
        /// Customer's List of Eligible loans
        /// </summary>
        public OutArgument<IList<CustomerEligibleLoan>> CustomerEligibleLoans { get; set; }

        /// <summary>
        /// Phone Number On Core Banking
        /// </summary>
        public OutArgument<string> CoreBankingPhoneNumber { get; set; }
        /// <summary>
        /// Core Banking Customer
        /// </summary>
        public OutArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer { get; set; }
        /// <summary>
        /// Core Banking Account
        /// </summary>
        public OutArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount { get; set; }

        XElement TransformXML(XElement incompleteXML, string[] relevantXmlNodes)
        {
            XElement transformedXML = new XElement(incompleteXML.Name);

            foreach (string node in relevantXmlNodes)
            {
                try
                {
                    transformedXML.Add(new XElement(node));
                    if (incompleteXML.Element(node).HasElements)
                    {
                        transformedXML.SetElementValue(node, incompleteXML.Element(node));
                    }
                    else
                    {
                        transformedXML.SetElementValue(node, incompleteXML.Element(node).Value);
                    }
                }
                catch
                {
                    transformedXML.SetElementValue(node, string.Empty);
                }
            }
            return transformedXML;
        }
        protected override void Execute(CodeActivityContext context)
        {
            string customerID = CustomerID.Get(context);
            string phoneNumber = PhoneNumber.Get(context);
            string accountNumber = AccountName.Get(context);
            string loanAmount = LoanAmount.Get(context);
            //string betafriendPerception = BetaFriendPerception.Get(context);
            bool isPhoneNumber = IsPhoneNumber.Get(context);
            bool isForLoggedOnCustomer = IsForLoggedOnCustomer.Get(context);
            bool isOnCoreBanking = IsOnCoreBanking.Get(context);
            bool isForSelfService = IsForSelfService.Get(context);
            MobileAccount mobile = null;
            LinkingBankAccount bankAccount = null;
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }

            if (isForLoggedOnCustomer)
            {
                string custID = mob.CustomerID;
                context.SetValue(CustomerID, custID);
                return;
            }
            if (!isOnCoreBanking)
            {

                if (isPhoneNumber)
                {
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {

                        //mobile = client.GetMobileAccountByPhoneNumber(phoneNumber);
                        mobile = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetMobileAccountByPhoneNumber(mob.InstitutionCode, phoneNumber) as MobileAccount;
                        //AccountName.Set(context, string.Format("{0}:{1}", mobile.LastName, mobile.OtherNames));
                    }
                    if (mobile == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

                    }
                    else
                    {
                    }
                }
                else
                {
                    bankAccount = null;
                    using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                    {
                        //  mobile = client.GetMobileAccountBy(phoneNumber);


                        bankAccount = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(phoneNumber, instCode); //client.GetAccountByAccountNumber(phoneNumber) as LinkingBankAccount;
                        mobile = bankAccount == null ? null : bankAccount.TheMobileAccount as MobileAccount;
                    }
                    // new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetBankAccountByAcccountNo(phoneNumber, mob.InstitutionCode);
                    if (mobile == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAccountNumber), Utilities.GetDescription(ErrorCodes.InvalidAccountNumber));
                    }
                    //mobile  =bankAccount.TheMobileAccount as MobileAccount;
                }
            }

            else
            {
                using (BankOneMobile.Services.CoreBankingService.SwitchingServiceClient client = new BankOneMobile.Services.CoreBankingService.SwitchingServiceClient())
                {
                    BankOneMobile.Services.CoreBankingService.Account ac = client.GetAccountByAccountNo(instCode, phoneNumber);
                    if (ac == null)
                    {
                        throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAccountNumber), Utilities.GetDescription(ErrorCodes.InvalidAccountNumber));
                    }
                    else
                    {

                        BankOneMobile.Services.CoreBankingService.Customer cust = client.GetCustomer(instCode, ac.CustomerID);
                        string accName = string.Format("{0},{1}", cust.LastName, cust.OtherNames);
                        AccountName.Set(context, accName);
                        context.SetValue<BankOneMobile.Services.CoreBankingService.Customer>(CoreBankingCustomer, cust);
                        context.SetValue<BankOneMobile.Services.CoreBankingService.Account>(CoreBankingAccount, ac);
                        //CoreBankingAccount.Set(context, ac);
                        //CoreBankingCustomer.Set(context,cust);
                        CoreBankingPhoneNumber.Set(context, cust.PhoneNo);
                        return;
                    }
                }
            }
            //string id = mobile.CustomerID;
            //context.SetValue(CustomerID, id);
            //TODO: Do Balance Enquiry
            //string rechargePInCode = TransactionSyst    unTransaction(pin);
            //RechargePIN.Set(context,rechargePInCode);
            IList<CustomerEligibleLoan> customerEligibleLoan = new List<CustomerEligibleLoan>();
            try
            {

                using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                {

                    //mobile = client.GetMobileAccountByPhoneNumber(phoneNumber); //new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
                    mobile = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetMobileAccountByPhoneNumber(mob.InstitutionCode, phoneNumber) as MobileAccount;

                    bankAccount = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetRecievingAccountByInstitution(mob.InstitutionCode, phoneNumber); //client.GetAccountByAccountNumber(phoneNumber) as LinkingBankAccount;
                    mobile = bankAccount == null ? null : bankAccount.TheMobileAccount as MobileAccount;
                    //AccountName.Set(context, string.Format("{0}:{1}", mobile.LastName, mobile.OtherNames));
                }

                //using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                //{
                //}
                if (mobile == null)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

                }
                else
                {
                    bankAccount = mobile.BankAccounts.First(x => x.InstitutionCode == mobile.InstitutionCode) as LinkingBankAccount;
                    #region Send Loan Request and get eligibleloans
                    string loanRequestXML = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
                                                            <LoanRequest>
                                                                <CustomerAccountNumber>{0}</CustomerAccountNumber>
                                                                <PhoneNumber>{1}</PhoneNumber>
                                                                <Amount></Amount>
                                                                <Tenure></Tenure>
                                                                <LoanProductID></LoanProductID>
                                                                <InstitutionCode>{2}</InstitutionCode>
                                                                <AgentPerceptionOfCustomer></AgentPerceptionOfCustomer>
                                                            </LoanRequest>",
                                                           bankAccount.NUBAN,phoneNumber,mobile.InstitutionCode);
                    WebRequest request_ = null;
                    if(bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsTestMode"]))
                        request_ = WebRequest.Create("http://localhost/CreditAssessment/services/LoanRequest.aspx");
                    else
                        request_ = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/LoanRequest.aspx");
                    // Create a request using a URL that can receive a post. 
                    //WebRequest request_ = WebRequest.Create("http://localhost/CreditAssessment/services/LoanRequest.aspx");
                    //WebRequest request_ = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/LoanRequest.aspx");
                    // Set the Method property of the request to POST.
                    request_.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    string postData = loanRequestXML;
                    byte[] byteArray_ = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    //request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request_.ContentLength = byteArray_.Length;
                    // Get the request stream.
                    Stream dataStream_ = request_.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream_.Write(byteArray_, 0, byteArray_.Length);
                    // Close the Stream object.
                    dataStream_.Close();
                    // Get the response.
                    WebResponse webResponse = request_.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream_ = webResponse.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader_ = new StreamReader(dataStream_);
                    // Read the content.
                    string responseFromServer = reader_.ReadToEnd();

                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader_.Close();
                    dataStream_.Close();
                    webResponse.Close();
                    #endregion
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Response: {0}", responseFromServer)));
                    XElement xml;
                    try
                    {
                        xml = XElement.Parse(responseFromServer);
                    }
                    catch
                    {
                        xml = XElement.Parse(responseFromServer.Replace("&nbsp;", string.Empty));
                    }
                    if (xml.Name.NamespaceName.Contains("http://schemas.microsoft.com/")) //"http://schemas.microsoft.com/2003/10/Serialization/"
                    {
                        xml = XElement.Parse(xml.Value);
                    }


                    if (xml.Name != "LoanRequestResponse")
                    {
                        throw new InvalidServerResponseForLoanRequestException("Error processing Loan Request");
                    }

                    XElement relevantXml = TransformXML(xml, new[] { "Status", "Error" });
                    var status = relevantXml.Element("Status").Value;
                    var error = relevantXml.Element("Error").Value;

                    if (status != "0")
                    {
                        throw new FailedLoanRequestException(error);
                    }

                    // Create a request using a URL that can receive a post. 
                    WebRequest request = null;
                    if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsTestMode"]))
                        request = WebRequest.Create("http://localhost/CreditAssessment/services/GetCustomerEligibleLoan.aspx");
                    else
                        request = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/GetCustomerEligibleLoan.aspx");
                    //WebRequest request = WebRequest.Create("http://localhost/CreditAssessment/services/GetCustomerEligibleLoan.aspx");
                    //WebRequest request = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/GetCustomerEligibleLoan.aspx");
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    postData = mobile.CustomerID + "|" + mobile.InstitutionCode;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    //request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;
                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    WebResponse response = request.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();

                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    //Console.Read();
                    if (responseFromServer != null && responseFromServer != "" && responseFromServer != "0")
                    {
                        Dictionary<long, string> result = BankOneMobile.Core.Helpers.ObjectSerialization.FromJsonToDictionary(responseFromServer);
                        foreach (KeyValuePair<long, string> kvp in result)
                        {
                            customerEligibleLoan.Add(new CustomerEligibleLoan
                            {
                                CustomerID = mobile.CustomerID,
                                LoanProductID = kvp.Key.ToString(),
                                LoanProductName = kvp.Value
                            });
                        }
                        //LoanProductName.Set(context, responseFromServer.Split('|')[1]);
                        //LoanProductID.Set(context, responseFromServer.Split('|')[0]);
                        CustomerEligibleLoans.Set(context, customerEligibleLoan);
                        AccountNumber.Set(context, bankAccount.BankAccount);
                        CustomerID.Set(context, mobile.CustomerID);
                    }
                    else if (responseFromServer == "0")
                    {
                        throw new NoEligibleLoanProductException("Customer is not profiled to request loan");
                    }
                }
            }
            catch (InvalidServerResponseForLoanRequestException ex)
            {
                System.Diagnostics.Trace.TraceError("Error processing Loan Request. The response from server was not in the right format");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw;
            }

            catch (FailedLoanRequestException ex)
            {
                System.Diagnostics.Trace.TraceError("Error processing Loan Request. The response from server returned an error");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw;
            }
            catch (NoEligibleLoanProductException ex)
            {
                System.Diagnostics.Trace.TraceError("Customer is not profiled to request loan");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.Source);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                throw;
            }
            catch (FaultException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), "Core Banking Unavailable");
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), ex.Message);
            }
            CustomerEligibleLoans.Set(context, customerEligibleLoan);
            LoanAmount.Set(context, loanAmount);
            InstitutionCode.Set(context, mobile.InstitutionCode);

        }

    }

}
