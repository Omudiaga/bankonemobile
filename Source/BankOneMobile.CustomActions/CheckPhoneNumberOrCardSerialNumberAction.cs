﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;
using System.Globalization;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CheckPhoneNumberOrCardSerialNumberDesigner))]
    public class CheckPhoneNumberOrCardSerialNumberAction : ActionNode
    {
        /// <summary>
        /// Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber
        {
            get
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {

                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        /// <summary>
        /// Card Serial Number
        /// </summary>
        public InArgument<string> CardSerialNumber
        {
            get
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {

                    return activity.CardSerialNumber;
                }
                return null;
            }
            set
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {
                    activity.CardSerialNumber = value;
                }
            }
        }

        /// <summary>
        /// Validation Type
        /// </summary>
        public InArgument<bool> IsPhoneNumberValidation
        {
            get
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {

                    return activity.IsPhoneNumberValidation;
                }
                return null;
            }
            set
            {
                CheckPhoneNumberOrCardSerialNumberActivity activity = this.InternalState as CheckPhoneNumberOrCardSerialNumberActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumberValidation = value;
                }
            }
        }

        public CheckPhoneNumberOrCardSerialNumberAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CheckPhoneNumberOrCardSerialNumberActivity();
        }

    }

    public class CheckPhoneNumberOrCardSerialNumberActivity : CodeActivity
    {
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<bool> IsPhoneNumberValidation { get; set; }
        public InArgument<string> CardSerialNumber { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            if(mob == null)
            {
                throw new Exception("Unable to retreive agent details.");
            }
           
            
            bool isPhoneNumberValidation = IsPhoneNumberValidation.Get(context);

            if (isPhoneNumberValidation)
            {
                string phoneNumber = PhoneNumber.Get(context);
                ICustomerRegistration custReg = new CustomerRegistrationSystem().GetCustomerRegistrationByPhoneNumber(mob.InstitutionCode, phoneNumber);
                if (custReg != null)
                {
                    throw new Exception("This Phone Number already exists.");
                }
            }
            else
            {
                string cardSerialNumber = CardSerialNumber.Get(context);
                ICustomerRegistration custReg = new CustomerRegistrationSystem().GetCustomerRegistrationBySerialNumber(cardSerialNumber);
                if (custReg != null)
                {
                    throw new Exception("This card has already been issued.");
                }
            }

        }
    }
}
