﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(MiniStatementDesigner))]
    public class MiniStatementAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        public OutArgument<IList<TransactionHistory>> TheTransactions
        {
            get
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    return activity.TheTransactions;
                }
                return null;
            }
            set
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    activity.TheTransactions = value;
                }
            }
        }
        public InArgument<string> CustomerAccountNumber
        {
            get
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {

                    return activity.CustomerAccountNumber;
                }
                return null;
            }
            set
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    activity.CustomerAccountNumber = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Mini Statement of User Account
        /// </summary>
        public OutArgument<string> Statement {
            get
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    return activity.Statement;
                }
                return null;
            }
            set
            {
                MiniStatementActivity activity = this.InternalState as MiniStatementActivity;
                if (activity != null)
                {
                    activity.Statement = value;
                }
            }
        }

        public MiniStatementAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new MiniStatementActivity();
        }

    }

    public class MiniStatementActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }

        /// <summary>
        ///Mini Statement on Customer Account
        /// </summary>
        public OutArgument<string> Statement { get; set; }
        public InArgument<string> CustomerAccountNumber { get; set; }
        public OutArgument<IList<TransactionHistory>> TheTransactions { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
            try
            {
                pin = HSMCenter.GeneratePinBlock(mob.MobilePhone, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string accountNumber = AccountNumber.Get(context);


            string custAcct = CustomerAccountNumber.Get(context);
            IMiniStatementTransactionType tranType = new MiniStatementTransactionType();

            if (!string.IsNullOrEmpty(custAcct))
            {
                tranType.IsForThirdParty = true;
                accountNumber = custAcct;
            }


            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                if (vals.Count > 0)
                {
                    try
                    {
                        if (vals.Count() > 1)
                        {
                            instCode = (new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]) as Institution).Code;
                           // instCode = inst.Code;
                            mob.InstitutionCode = instCode;
                        }
                        else
                        {
                            Agent ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                            if (ag == null)
                            {
                                throw new TransactionNotPermittedToNonAgentsException("Non -Agent Initiated Trnsactions");
                            }
                            instCode = ag.TheAgentAccount.InstitutionCode;
                            mob.InstitutionCode = instCode;
                        }
                    }
                    catch (InvalidInstitutionException)
                    {
                        throw new FailureException("51", "Your Institution is not set up for Mobile Transactions");
                    }
                }
                else
                {
                    // inst= 
                }
                mob.InstitutionCode = instCode;
            }


            
            
            TransactionResponse response = null;

            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                TheTransactions.Set(context, tranType.TheTransactions);
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }
           
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {
                 if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                 //STOP.....THIS IS CRIMINAL...
                 //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            Statement.Set(context, response.ResponseMessage);//set ledger balance 
            
        }
    }

}
