﻿//-------------------------------------------------------------------
// Copyright (c) Microsoft Corporation. All rights reserved
//-------------------------------------------------------------------



using System.Activities.Presentation.Metadata;
using BankOneMobile.SessionNodeDesigners;
using System;
using System.ComponentModel;
using System.Activities.Presentation.PropertyEditing;
using System.Activities.Presentation;
using System.Collections.Generic;
using BankOneMobile.SessionNodeDesigners.Screens;
using System.Collections;
using System.Linq;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.CustomActions
{
    
    // Interaction logic for SimpleNativeDesigner.xaml
    public partial class GetCommitmentSavingsProductsDesigner  : IStateDesigner
    {
        public static List<object> TheFlows { get; set; }

        public GetCommitmentSavingsProductsDesigner()
        {
            InitializeComponent();
        }

      

    }
}
