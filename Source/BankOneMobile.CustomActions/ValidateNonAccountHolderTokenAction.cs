﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(ValidateTokenActionDesigner))]
    public class ValidateNonAccountHolderTokenAction : ActionNode
    {
        
        public InArgument<string> PhoneNumber
        {
            get
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {
                 
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public InArgument<string> EnteredToken
        {
            get
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {

                    return activity.EnteredToken;
                }
                return null;
            }
            set
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {
                    activity.EnteredToken = value;
                }
            }
        }
        public OutArgument<bool> Response
        {
            get
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {

                    return activity.Response;
                }
                return null;
            }
            set
            {
                ValidateNonAccountHolderTokenActivity activity = this.InternalState as ValidateNonAccountHolderTokenActivity;
                if (activity != null)
                {
                    activity.Response = value;
                }
            }
        }
      
        public ValidateNonAccountHolderTokenAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ValidateNonAccountHolderTokenActivity();
        }

    }

    public class ValidateNonAccountHolderTokenActivity : CodeActivity
    {
        
        
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<string> EnteredToken { get; set; }
        public OutArgument<bool> Response { get; set; }

        
        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = PhoneNumber.Get(context);
            string token = EnteredToken.Get(context);          
            
            bool  rsp = false;       
            
            try
            {
                rsp = new TokenSystem().ValidateNonAccountHolderTokenMethod(phoneNumber, token);
               
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            Response.Set(context, rsp);
            
        
        }
    }
}
