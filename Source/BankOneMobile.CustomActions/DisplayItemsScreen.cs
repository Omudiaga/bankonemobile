﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    
    public class DisplayItemScreen<T> :MenuScreen
    {
        protected CompletionCallback _OnFullCompleted;

        /// <summary>
        /// Activation Code
        /// </summary>

        public OutArgument<string> DefaultOutPutParameter
        {
            get
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;

                if (activity != null)
                {
                    return activity.DefaultOutPutParameter;

                }
                return null;
            }
            set
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    activity.DefaultOutPutParameter = value;
                }
            }


        }
        public OutArgument<T> OutPutParameter
        {
            get
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    return activity.OutputParameter;

                }
                return null;
            }
            set
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    activity.OutputParameter = value;
                }
            }


        }
        public OutArgument<string> OutPutParameterName
        {
            get
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    return activity.OutputParameterName;

                }
                return null;
            }
            set
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    activity.OutputParameterName = value;
                }
            }


        }
        public InArgument<IList<T>> InputItems
        {
           get
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                   // ParameterList = activity.ListOfParameter;
                    return activity.InputItems;
                }
                return null;
            }
            set
            {
                GetItemActivity<T> activity = this.DisplayActivity as GetItemActivity<T>;
                if (activity != null)
                {
                    
                    activity.InputItems = value;
                }
            }


        }
       private   int _selectedValue;
       private IList<T> ParameterList;
       private T Parameter;
       private GetItemActivity<T> DisplayActivity { get; set; }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {;
        //this.DisplayActivity.InputItems = this.InputItems;// = this.InternalActivity;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedCompletedCallback = OnDisplayActivityCompleted;
            context.ScheduleActivity(this.DisplayActivity, this.TheFlow.Resume_Flow_Completed);
            _OnFullCompleted = onCompleted;
            nextNode = null;
            return false;
        }

        internal void OnDisplayActivityCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            int i = 1;
            this.MenuItems.Clear();
            ParameterList = (this.DisplayActivity as GetItemActivity<T>).ListOfParameter;
            foreach (T obj in ParameterList)
            {
                this.MenuItems.Add(new MenuItem() { Text = obj.ToString(), Index = i });
                i++;
            }
            
            State nextNode = null;
            base.Execute(context, _OnFullCompleted, out nextNode);
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {

            //OutPutParameter.Set(context, this.InputItems.Get(context)[_selectedValue]);
            //DefaultOutPutParameter.Set(context, this.InputItems.Get(context)[_selectedValue].ToString());
            
            int selectedMenu = 0;
            if (response != null)
            {
                Int32.TryParse(response, out selectedMenu);
            }
            if (selectedMenu == 0 || selectedMenu > this.MenuItems.Count)
            {
                currentWorkflowInfo.DisplayMessage = "Unknown number. Try again\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown number. Try again<br/>");
                 //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;
                
                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                if (this.Transitions != null && this.Transitions.Count == 1)
                {
                    this.Next = this.Transitions[0].To;
                }
                if (_OnFullCompleted != null)
                {

                    _OnFullCompleted.Invoke(context, null);
                }
                
            }
            
        }
        
        protected override string ProcessResponse(string value)
        {
             
            string response = string.Empty;
            int selectedMenu = 0;
            if (value != null)
            {
                if (Int32.TryParse(value, out selectedMenu))
                {
                    response = this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu).Text;

                   _selectedValue = this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu).Index;
                    
                   this.Parameter = ParameterList[_selectedValue-1];
                   (this.InternalActivity as DisplayItemActivity<T>).Parameter = this.Parameter;
                   //this.OutPutParameter = this.ParameterList[_sele]
                   
                }

            }
            return response;
        }

        public override void GetChildActivities(NativeActivityMetadata metadata)
        {
            
                
                metadata.AddChild(this.DisplayActivity);
            
            base.GetChildActivities(metadata);
        }
        public DisplayItemScreen()
        {
            ParameterList = new List<T>();
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new DisplayItemActivity<T>();
            this.DisplayActivity = new GetItemActivity<T>();           
           
        }
   
       
    }

    public class GetItemActivity<T> : CodeActivity
    {

        public  InArgument<IList<T>> InputItems{get;set;}
        public OutArgument<IList<T>> OutputItems { get; set; }
        public  OutArgument<T> OutputParameter{get;set;}
        public OutArgument<string> OutputParameterName
        {
            get;
            set;
        }
        public OutArgument<string> DefaultOutPutParameter{get;set;}
        public IList<T> ListOfParameter { get; set; }
        //public T Parameter { get; set; }



        public GetItemActivity()
        {

           ListOfParameter = new List<T>();

           // Parameter = new T();
        }

        protected override void Execute(CodeActivityContext context)
        {
            ListOfParameter = InputItems.Get(context);
            context.SetValue(OutputItems,ListOfParameter);
             //ListOfParameter = InputItems.Get(context);
             
            // context.SetValue(InputItems,ListOfParameter);

            ////TODO: Do Balance Enquiry
            //WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            //string sessionID = currentWorkflowInfo.SessionID;
            //MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            //string phoneNumber = mob.MobilePhone;
            //IList<Product> products = new ProductSystem().GetProductsByAgentPhone(mob.MobilePhone) as IList<Product>;
            //Products.Set(context, products);




        }
    }
    public class DisplayItemActivity<T>:SubActivity
    {
        public T Parameter { get; set; }
        public OutArgument<string> DefaultOutputParameter { get; set; }
        public OutArgument<T> OutputParameter
        {
            get;
            set;
        }
        public OutArgument<string> OutPutParameterName { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            context.SetValue(Response, TheResponse);
            context.SetValue(OutputParameter, Parameter);
            context.SetValue(OutPutParameterName,Parameter.ToString());
            base.Execute(context);
        }
        
    }
}
