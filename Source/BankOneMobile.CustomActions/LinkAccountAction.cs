﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(LinkAccountActionDesigner))]
    public class LinkAccountAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                 
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        public InArgument<string> PhoneNumberToUse
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.PhoneNumberToUse;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.PhoneNumberToUse = value;
                }
            }
        }
        public OutArgument<string> ActivationCode
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }
        }
        public InArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.CoreBankingAccount;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.CoreBankingAccount = value;
                }
            }
        }
        public InArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer
        {
            get
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    return activity.CoreBankingCustomer;
                }
                return null;
            }
            set
            {
                LinkAccountActivity activity = this.InternalState as LinkAccountActivity;
                if (activity != null)
                {
                    activity.CoreBankingCustomer = value;
                }
            }
        }


        public LinkAccountAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new LinkAccountActivity();
        }

    }

    public class LinkAccountActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }

        /// <summary>
        /// Balance on User Mobile Account
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        public OutArgument<string> ActivationCode { get; set; }
        public InArgument<string> PhoneNumberToUse { get; set; }
        public InArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount { get; set; }
        public InArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer{ get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source = DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount agentMob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = agentMob.MobilePhone;
            string pin = PIN.Get(context);
            string phoneToUse = PhoneNumberToUse.Get(context);
            BankOneMobile.Services.CoreBankingService.Customer customer = CoreBankingCustomer.Get(context);
            BankOneMobile.Services.CoreBankingService.Account account = CoreBankingAccount.Get(context);
            RegistrationTransactionType tp = new RegistrationTransactionType();
            //tp.Date = DateTime.Now;


            try
            {
                 Agent ag = null;
                
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(agentMob.MobilePhone);
                  if (ag == null)
                {
                    throw new TransactionNotPermittedToNonAgentsException("Transaction not allowed for Non-Agents");
                }
               else  if (!ag.IsActive)
                {
                    throw new TransactionNotPermittedToNonAgentsException("Transaction not allowed for Non-Agents");
                }
               

                string instCode = ag.TheAgentAccount.InstitutionCode; string custId = string.Empty; MobileAccount mob = null;

                string accountNumber = AccountNumber.Get(context);
                if (new MobileAccountSystem(currentWorkflowInfo.DataSource).GetBankAccountByAcccountNo(accountNumber,instCode)!=null)
                {
                    throw new AlreadyExistingAccountException("Account Number Already Exists");
                }
                pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
               


              
               

                    try
                    {

                        
                        if (account == null)
                        {
                            throw new InvalidAccountNumberException("Invalid Account Number");
                        }

                        
                        mob = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetMobileAccountFromCoreBanking(ag.InstitutionCode, account, customer);
                    }

                    catch (Exception ex)
                    {
                        throw new CoreBankingWebServiceException(String.Format("{0}", ex.Message));
                    }
                    mob.PersonalPhoneNumber = mob.MobilePhone;
                    mob.MobilePhone = phoneToUse;
                    using (var uniqueDataSource = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                    {
                        mob = new MobileAccountSystem(uniqueDataSource).InsertByAgent(mob, ag);
                    }
                    ActivationCode.Set(context, mob.ActivationCode);
                    

                
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotInitiatedByAgent), Utilities.GetDescription(ErrorCodes.TransactionNotInitiatedByAgent));
            }
            catch (AlreadyExistingAccountException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.AccountNumberAlreadyExists), Utilities.GetDescription(ErrorCodes.AccountNumberAlreadyExists));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }

            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            
                        
            
            }
            
            
       
        
            
        
        
    }
}
