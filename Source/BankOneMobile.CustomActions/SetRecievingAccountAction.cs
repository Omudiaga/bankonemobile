﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(SetRecievingAccountDesigner))]
    public class SetRecievingAccountAction : ActionNode
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        ///Selcted  Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                SetRecievingAccountActivity activity = this.InternalState as SetRecievingAccountActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        

        public SetRecievingAccountAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SetRecievingAccountActivity();
        }

    }

    public class SetRecievingAccountActivity : CodeActivity
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }

        

        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
            try
            {
                pin = HSMCenter.GeneratePinBlock(mob.MobilePhone, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            }
            catch (NoHSMResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            }
            string accountNumber = AccountNumber.Get(context);

            ITransaction trans = new Transaction();
            IConfigurationTransactionType tranType = new ConfigurationTransactionType();

            // ta.EncryptedPIN = pin1;
            

            trans.FromInstitutionCode = mob.InstitutionCode;
            trans.From = accountNumber;//mob.RecievingBankAccount.BankAccount;
            try
            {
                new TransactionSystem().RunTransaction(pin, tranType, mob, trans.From, string.Empty, 0.0M, sessionID);
                // (pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (InvalidAccountNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAccountNumber), Utilities.GetDescription(ErrorCodes.InvalidAccountNumber));

            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            //TODO: Set recieving sccount

           finally
           {


               if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
               {
                   try
                   {
                       new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                   }
                   catch
                   {
                   }

               }
               //STOP.....THIS IS CRIMINAL...
               //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

           }
            

        }
    }

}
