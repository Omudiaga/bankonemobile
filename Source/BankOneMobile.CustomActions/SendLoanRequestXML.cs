﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;
using System.Net;
using System.IO;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(SendLoanRequestXMLDesigner))]
    public class SendLoanRequestXMLAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// Loan Product Name
        /// </summary>
        public InArgument<string> LoanProductID
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.LoanProductID;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.LoanProductID = value;
                }
            }
        }
        public InArgument<string> LoanAmount
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.LoanAmount;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.LoanAmount = value;
                }
            }
        }
        public InArgument<Int32> BetaFriendPerception
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.BetaFriendPerception;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.BetaFriendPerception = value;
                }
            }
        }
        public InArgument<string> PhoneNumber
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public InArgument<string> AccountNumber
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        /// bool is phone number
        /// </summary>
        public InArgument<bool> IsPhoneNumber
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.IsPhoneNumber;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumber = value;
                }
            }
        }
        public InArgument<bool> IsOnCoreBanking
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.IsOnCoreBanking;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.IsOnCoreBanking = value;
                }
            }
        }
        public InArgument<bool> ForLoggedOnCustomer
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.IsForLoggedOnCustomer;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.IsForLoggedOnCustomer = value;
                }
            }
        }

        /// <summary>
        /// account Name
        /// </summary>
        public InArgument<string> CustomerID {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.CustomerID;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.CustomerID = value;
                }
            }
        }

        /// <summary>
        /// institution code
        /// </summary>
        public InArgument<string> InstitutionCode
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.InstitutionCode;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.InstitutionCode = value;
                }
            }
        }

        public InArgument<CustomerEligibleLoan> CustomerEligibleLoans
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.CustomerEligibleLoans;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.CustomerEligibleLoans = value;
                }
            }
        }
        public OutArgument<string> CoreBankingPhoneNumber
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.CoreBankingPhoneNumber;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.CoreBankingPhoneNumber = value;
                }
            }
        }
        public InArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.CoreBankingCustomer;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.CoreBankingCustomer = value;
                }
            }
        }
        public InArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount
        {
            get
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    return activity.CoreBankingAccount;
                }
                return null;
            }
            set
            {
                SendLoanRequestXMLActivity activity = this.InternalState as SendLoanRequestXMLActivity;
                if (activity != null)
                {
                    activity.CoreBankingAccount = value;
                }
            }
        }

        public SendLoanRequestXMLAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SendLoanRequestXMLActivity();
        }

    }

    public class SendLoanRequestXMLActivity : CodeActivity
    {
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Used to check if its being used for the customer doing the transaction
        /// </summary>
        public InArgument<bool> IsForLoggedOnCustomer { get; set; }
        /// <summary>
        /// Used to check if the validation is from Core Banking or not
        /// </summary>
        public InArgument<bool> IsOnCoreBanking { get; set; }
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<string> AccountNumber { get; set; }
        /// <summary>
        /// Type OF ID i.e. account Number or Phone Number
        /// </summary>
        public InArgument<bool> IsPhoneNumber { get; set; }
        public InArgument<bool> IsForSelfService { get; set; }
        public InArgument<string> LoanProductID { get; set; }
        public InArgument<string> LoanAmount { get; set; }
        public InArgument<Int32> BetaFriendPerception { get; set; }
        /// <summary>
        /// Recharge PIN 
        /// </summary>
        public InArgument<string> AccountName { get; set; }
        public OutArgument<string> LoanProductName { get; set; }
        

        /// <summary>
        /// Customer's ID tied to phone number
        /// </summary>
        public InArgument<string> CustomerID { get; set; }
        public InArgument<string> InstitutionCode { get; set; }

        /// <summary>
        /// Customer's List of Eligible loans
        /// </summary>
        public InArgument<CustomerEligibleLoan> CustomerEligibleLoans { get; set; }

        /// <summary>
        /// Phone Number On Core Banking
        /// </summary>
        public OutArgument<string> CoreBankingPhoneNumber { get; set; }
        /// <summary>
        /// Core Banking Customer
        /// </summary>
        public InArgument<BankOneMobile.Services.CoreBankingService.Customer> CoreBankingCustomer { get; set; }
        /// <summary>
        /// Core Banking Account
        /// </summary>
        public InArgument<BankOneMobile.Services.CoreBankingService.Account> CoreBankingAccount { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            string pin = PIN.Get(context);
            string customerID = CustomerID.Get(context);
            string phoneNumber = PhoneNumber.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string loanAmount = LoanAmount.Get(context);
            string betaFriendPerception = BetaFriendPerception.Get(context).ToString();
            string tenure = "30";
            CustomerEligibleLoan customerEligibleLoan = CustomerEligibleLoans.Get(context);
            string institutionCode = InstitutionCode.Get(context);
            string loanID = customerEligibleLoan.LoanProductID;
            bool isPhoneNumber = IsPhoneNumber.Get(context);
            bool isForLoggedOnCustomer = IsForLoggedOnCustomer.Get(context);
            bool isOnCoreBanking = IsOnCoreBanking.Get(context);
            bool isForSelfService = IsForSelfService.Get(context);
            MobileAccount mobile = null;
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }
            ILoanRequestTransactionType tranType = new LoanRequestTransactionType();
            tranType.AccountNumber = accountNumber;
            tranType.BetaFriendPerception = betaFriendPerception;
            tranType.InstitutionCode = institutionCode;
            tranType.LoanAmount = loanAmount;
            tranType.LoanProductID = loanID;
            tranType.PhoneNumber = phoneNumber;
            tranType.Tenure = tenure;

            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, mob.MobilePhone, mob.MobilePhone, 0.0M, sessionID);// use 0 cos we dont really care about amount in balance enquiry
            }
            catch (InvalidServerResponseForLoanRequestException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            catch (FailedLoanRequestException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }
            catch (TransactionNotPermittedToNonAgentsException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (AlreadyRegisterdCustomerException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberAlreadyRegistered), Utilities.GetDescription(ErrorCodes.PhoneNumberAlreadyRegistered));
            }
            catch (AlreadyExistingAccountException ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.AccountNumberAlreadyExists), Utilities.GetDescription(ErrorCodes.AccountNumberAlreadyExists));
            }
            catch (CoreBankingWebServiceException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), Utilities.GetDescription(ErrorCodes.UnableToConnectToCoreBanking));
            }
            catch (NoHSMResponseException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during transaction.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
            }
            finally
            {


                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                //if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                //{
                //   // new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally);

                //}
                //if (File.Exists(filename))
                //{
                //    System.Diagnostics.Trace.TraceInformation("Deleting {0} image...", filename);
                //    File.Delete(filename);
                //}

            }
            
        }
    }

}
