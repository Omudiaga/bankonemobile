﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Net;
using System.IO;

namespace BankOneMobile.CustomActions
{

    public class SelectLoanProductScreen : MenuScreen
    {

        State _NextNode;
        CompletionCallback _OnCompleted;
        public string SelectedLoanProduct { get; set; }
        
        public bool UseEnteredPhoneNo { get; set; }
        public Dictionary<int, string> InstitutionCodes { get; set; }
        public InArgument<string> EnteredPhoneNumber
        {
            get
            {
                if (this.InternalActivity == null) return null;
                return (this.InternalActivity as SelectLoanProductActivity).PhoneNumber;
            }
            set
            {
                if (this.InternalActivity != null)
                {
                    (this.InternalActivity as SelectLoanProductActivity).PhoneNumber = value;
                }
            }
        }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            _OnCompleted = onCompleted;
            nextNode = null;
            _NextNode = nextNode;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedCompletedCallback = OnDisplayComputeCompleted;
            context.ScheduleActivity(this.InternalActivity,this.TheFlow.Resume_Flow_Completed);

            nextNode = _NextNode;
            return false;
            
        }
        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            int selectedMenu = 0;
            if (response != null)
            {
                Int32.TryParse(response, out selectedMenu);
            }
            if (selectedMenu == 0 || selectedMenu > this.MenuItems.Count)
            {
                currentWorkflowInfo.DisplayMessage = "Unknown number. Try again\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown number. Try again<br/>");
                //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                if (this.Transitions != null && this.Transitions.Count == 1)
                {
                    this.Next = this.Transitions[0].To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }
        protected void OnDisplayComputeCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            MobileAccount mobile = null;
            InstitutionCodes = new Dictionary<int, string>();
            string phone = (this.InternalActivity as SelectLoanProductActivity).phoneNumber;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            IList<ILinkingBankAccount> accounts = null;
            string responseFromServer = string.Empty;
            if (!UseEnteredPhoneNo)
            {
                accounts = currentWorkflowInfo.InitiatedBy.BankAccounts.Where(x=>x.BankStatus!=BankAccountStatus.InActive).ToList();
            }
            else
            {
                using (MobileAccountService.MobileAccountServiceClient client = new MobileAccountService.MobileAccountServiceClient())
                {

                    mobile = client.GetMobileAccountByPhoneNumber(phone); //new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(phoneNumber);
                    //AccountName.Set(context, string.Format("{0}:{1}", mobile.LastName, mobile.OtherNames));
                }
                if (mobile == null)
                {
                    throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

                }
                else
                {
                    //accounts = new MobileAccountSystem(currentWorkflowInfo.DataSource).GetByPhoneNumber(phone).BankAccounts.Where(x => x.BankStatus != BankAccountStatus.InActive).ToList();
                    // Create a request using a URL that can receive a post. 
                    WebRequest request = WebRequest.Create("http://localhost/CreditAssessment/services/GetCustomerEligibleLoan.aspx");
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    string postData = mobile.CustomerID;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    //request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;
                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    WebResponse response = request.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();

                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    //Console.Read();
                    //if (responseFromServer != null || responseFromServer != "")
                    //{
                    //    LoanProductName.Set(context, responseFromServer.Split('|')[1]);
                    //    LoanProductID.Set(context, responseFromServer.Split('|')[0]);
                    //}
                }
            }
            int i = 1;
            this.MenuItems.Clear();
            //foreach (LinkingBankAccount lbc in accounts)
            //{
            this.MenuItems.Add(new MenuItem() { Text = string.Format("{0}:{1}", responseFromServer.Split('|')[1], responseFromServer.Split('|')[0]), Index = i });
                InstitutionCodes.Add(i, mobile.InstitutionCode);
                i++;
            //}

            //if (accounts.Count == 1)
            //{
                if (this.Transitions != null && this.Transitions.Count == 1)
                {
                    this.Next = this.Transitions[0].To;
                    (this.InternalActivity as SelectLoanProductActivity).LoanProduct = string.Format("{0}:{1}", responseFromServer.Split('|')[1], responseFromServer.Split('|')[0]);
                    currentWorkflowInfo.AttachedCompletedCallback = _OnCompleted;
                    context.ScheduleActivity(this.InternalActivity, this.TheFlow.Resume_Flow_Completed);
                   // _OnCompleted(context, instance);
                    return;
                }
                
            //}
             base.Execute(context, _OnCompleted, out _NextNode);

        }

        public SelectLoanProductScreen()
        {
            MenuItems = new List<MenuItem>();
            this.StateId = Guid.NewGuid().ToString();
            this.InternalActivity = new SelectLoanProductActivity();
        }

    }
    public class SelectLoanProductActivity : SubActivity
    {
        public string AccountNumber { get; set; }
        public string LoanProduct { get; set; }
        public string loanProdID { get; set; }
        public string instCode { get; set; }
        public string phoneNumber { get; set; }
        public OutArgument<string> InstitutionCode { get; set; }
        public OutArgument<string> LoanProductID { get; set; }
        public InArgument<string> PhoneNumber { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            base.Execute(context);
            phoneNumber = PhoneNumber.Get(context);
            if (!string.IsNullOrEmpty(LoanProduct))
            {
                context.SetValue(Response, LoanProduct);
            }
            context.SetValue(InstitutionCode, instCode);
            context.SetValue(LoanProductID, loanProdID);
            
        }
    }

}
