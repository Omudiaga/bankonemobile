﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodes;
using System.Activities;
using System.ComponentModel;
using Microsoft.Samples.UsingWorkflowItemPresenter;
using System.Activities.Presentation.Metadata;
using System.Activities.Presentation.PropertyEditing;
using BankOneMobile.SessionNodeDesigners.Screens;
using BankOneMobile.Services;
using BankOneMobile.SessionNodeDesigners;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Attributes;
using System.Reflection;

namespace BankOneMobile.CustomActions
{

    public class DisplayGenericItemScreen<T> : MenuScreen
    {
        protected CompletionCallback _OnFullCompleted;

        public OutArgument<string> OutputItemName
        {
            get
            {
                DisplayGenericItemActivity<T> activity = this.DisplayActivity as DisplayGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.OutPutItemName;

                }
                return null;
            }
            set
            {
                DisplayGenericItemActivity<T> activity = this.DisplayActivity as DisplayGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.OutPutItemName = value;
                }
            }


        }
        public OutArgument<string> OutPutNairaAmount
        {
            get
            {
                DisplayGenericItemActivity<T> activity = this.DisplayActivity as DisplayGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.OutPutNairaAmount;

                }
                return null;
            }
            set
            {
                DisplayGenericItemActivity<T> activity = this.DisplayActivity as DisplayGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.OutPutNairaAmount = value;
                }
            }


        }
        public OutArgument<string> OutputCustomerIDName
        {
            get
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.OutPutCustomerIDName;

                }
                return null;
            }
            set
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.OutPutCustomerIDName = value;
                }
            }


        }
        public OutArgument<string> OutputAmount
        {
            get
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.OutPutAmount;

                }
                return null;
            }
            set
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.OutPutAmount = value;
                }
            }


        }
        public OutArgument<T> OutputItem
        {
            get
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.OutputItem;

                }
                return null;
            }
            set
            {
                DisplayGenericItemActivity<T> activity = this.InternalActivity as DisplayGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.OutputItem = value;
                }
            }


        }

        public InArgument<IList<T>> InPutItems
        {
            get
            {
                GetGenericItemActivity<T> activity = this.DisplayActivity as GetGenericItemActivity<T>;

                if (activity != null)
                {
                    return activity.InputItems;


                }
                return null;
            }
            set
            {
                GetGenericItemActivity<T> activity = this.DisplayActivity as GetGenericItemActivity<T>;
                if (activity != null)
                {
                    activity.InputItems = value;
                }
            }


        }
        private Dictionary<int, T> DictItems;
        private List<T> Items;
        private GetGenericItemActivity<T> DisplayActivity { get; set; }
        public DisplayGenericItemScreen()
        {
            DictItems = new Dictionary<int, T>();
            DisplayGenericItemActivity<T> dis = new DisplayGenericItemActivity<T>();
            Items = new List<T>();
            dis.SelectedItem = Activator.CreateInstance<T>();
            this.InternalActivity = dis;
            GetGenericItemActivity<T> getProd = new GetGenericItemActivity<T>();
            getProd.TheItems = new List<T>();
            getProd.SelectedItem = Activator.CreateInstance<T>();

            this.DisplayActivity = getProd;

        }
        protected override bool Execute(NativeActivityContext context, CompletionCallback onCompleted, out State nextNode)
        {
            ;
            //this.DisplayActivity.InputItems = this.InputItems;// = this.InternalActivity;
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            currentWorkflowInfo.AttachedCompletedCallback = OnDisplayActivityCompleted;
            context.ScheduleActivity(this.DisplayActivity, this.TheFlow.Resume_Flow_Completed);
            _OnFullCompleted = onCompleted;
            nextNode = null;
            return false;
        }

        internal void OnDisplayActivityCompleted(NativeActivityContext context, ActivityInstance instance)
        {
            int i = 1;
            this.MenuItems.Clear();
            this.DictItems.Clear();
            Items = (this.DisplayActivity as GetGenericItemActivity<T>).TheItems.ToList();

            foreach (T obj in Items)
            {

                this.MenuItems.Add(new MenuItem() { Text = obj.ToString(), Index = i });

                DictItems.Add(i, obj);
                i++;
            }

            State nextNode = null;
            base.Execute(context, _OnFullCompleted, out nextNode);
        }
        public override void GetChildActivities(NativeActivityMetadata metadata)
        {


            metadata.AddChild(this.DisplayActivity);

            base.GetChildActivities(metadata);
        }

        protected override void Resume_Bookmark(WorkflowInfo currentWorkflowInfo, NativeActivityContext context, string response)
        {
            int selectedMenu = 0;
            if (response != null)
            {
                Int32.TryParse(response, out selectedMenu);
            }
            if (selectedMenu == 0 || selectedMenu > this.MenuItems.Count)
            {
                currentWorkflowInfo.DisplayMessage = "Unknown number. Try again\r\n\r\n" + this.GetDisplay(context);
                currentWorkflowInfo.XmlDisplayMessage = this.GetXmlDisplay(context, "Unknown number. Try again<br/>");
                //currentWorkflowInfo.BookmarkToResume = context.WorkflowInstanceId.ToString();
                //currentWorkflowInfo.AttachedBookmarkCallback = Resume_Bookmark;

                context.CreateBookmark(currentWorkflowInfo.BookmarkToResume, new BookmarkCallback(this.TheFlow.Resume_Flow_Bookmark));
            }
            else
            {
                Transition theTrans = this.Transitions[0];

                if (theTrans != null)
                {
                    this.Next = theTrans.To;
                }
                if (_OnCompleted != null)
                {
                    _OnCompleted.Invoke(context, null);
                }
            }
        }
        protected override string ProcessResponse(string value)
        {
            string response = string.Empty;
            int selectedMenu = 0;
            if (value != null)
            {
                if (Int32.TryParse(value, out selectedMenu))
                {
                    if (this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu) != null)
                    {
                        response = this.MenuItems.SingleOrDefault(x => x.Index == selectedMenu).Text;
                        (this.InternalActivity as DisplayGenericItemActivity<T>).SelectedItem = this.DictItems[selectedMenu];
                    }
                    else
                    {
                        //this.SelectedItem = this.DictItems[selectedMenu];

                        response = value;
                    }
                }

            }
            return response;
        }



    }
    public class DisplayGenericItemActivity<T> : SubActivity
    {
        public T SelectedItem;
        public InArgument<IList<T>> InputItems { get; set; }
        public OutArgument<string> OutPutItemName { get; set; }
        public OutArgument<string> OutPutCustomerIDName { get; set; }
        public OutArgument<string> OutPutNairaAmount { get; set; }
        public OutArgument<string> OutPutAmount { get; set; }
        public OutArgument<T> OutputItem
        {
            get;
            set;
        }
        protected override void Execute(CodeActivityContext context)
        {
            //context.SetValue(Response, TheResponse);

            decimal value = 0.0M;

            Type t = typeof(T);// Get the property set as amount
            IList<PropertyInfo> props = t.GetProperties().Where(
                 prop => Attribute.IsDefined(prop, typeof(AmountAttribute))).ToList();
            if (props.Count >= 1)
            {
                PropertyInfo theAmount = props.FirstOrDefault();
                string strAmount = theAmount.GetValue(this.SelectedItem, new Object[0]).ToString();
                decimal decAmount = Convert.ToDecimal(strAmount);

                context.SetValue<String>(OutPutAmount, decAmount.ToString("F2"));
                try
                {
                    context.SetValue<String>(OutPutNairaAmount, (decAmount / 100).ToString("F2"));
                }
                catch
                {
                    context.SetValue<String>(OutPutNairaAmount, "0.00");
                }

            }
            props = t.GetProperties().Where(
                 prop => Attribute.IsDefined(prop, typeof(CustomerIDAttribute))).ToList();
            if (props.Count >= 1)
            {
                PropertyInfo customerIdField = props.FirstOrDefault();
                 Object obj  = customerIdField.GetValue(this.SelectedItem, new Object[0]);
                string idName = obj!=null? obj.ToString():null;
                context.SetValue(OutPutCustomerIDName, idName);
            }





            context.SetValue(OutputItem, this.SelectedItem);
            context.SetValue(OutPutItemName, this.SelectedItem.ToString());
            base.Execute(context);
        }

    }
    public class GetGenericItemActivity<T> : SubActivity
    {
        public T SelectedItem;
        public InArgument<IList<T>> InputItems { get; set; }
        public OutArgument<string> OutPutItemName { get; set; }
        public OutArgument<T> OutputItem { get; set; }
        public IList<T> TheItems { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            TheItems = InputItems.Get(context);
            // TheItems = InputItems.Get(context);
            ////context.SetValue(Response, TheResponse);
            context.SetValue(OutputItem, this.SelectedItem);
            base.Execute(context);
        }

    }
}
