﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using System.Xml;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetDiamondBankAccountNameDesigner))]
    public class GetDiamondBankAccountNameAction : ActionNode
    {
        
        public InArgument<string> AccountNumber
        {
            get
            {
                GetDiamondBankAccountNameActivity activity = this.InternalState as GetDiamondBankAccountNameActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                GetDiamondBankAccountNameActivity activity = this.InternalState as GetDiamondBankAccountNameActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// account name
        /// </summary>
        public OutArgument<string> AccountName
        {
            get
            {
                GetDiamondBankAccountNameActivity activity = this.InternalState as GetDiamondBankAccountNameActivity;
                if (activity != null)
                {
                    return activity.AccountName;
                }
                return null;
            }
            set
            {
                GetDiamondBankAccountNameActivity activity = this.InternalState as GetDiamondBankAccountNameActivity;
                if (activity != null)
                {
                    activity.AccountName = value;
                }
            }
        }

        public GetDiamondBankAccountNameAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetDiamondBankAccountNameActivity();
        }

    }

    public class GetDiamondBankAccountNameActivity : CodeActivity
    {
    
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
    
        public OutArgument<string> AccountName { get; set; }

        protected override void Execute(CodeActivityContext context)
        {

            string accountNumber = AccountNumber.Get(context);
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }

            BankOneMobile.Services.SwitchServiceRef.Response response = null;
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to call service.")));
            try
            {
                using (BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient client = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    response = client.DiamondBankGetAccountName(accountNumber);
                }
            }
            catch(Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                throw new Exception("No response from Diamond Bank within time limit");
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("finished calling service. Response is: {0}", response == null ? "Null" : response.ResponseMessage)));
            if(response== null)
            {
                throw new Exception("No response from Service.");
            }
            if(!response.IsSuccessful)
            {
                throw new Exception(response.ResponseMessage);
            }
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(response.ResponseMessage);
            }
            catch (Exception)
            {
                throw new Exception("Invalid response from Diamond Bank.");
            }
            bool isAccountNumberValid = Convert.ToBoolean(xmlDoc.SelectSingleNode("/DiamondNameEnquiryResponse/Status").InnerText);
            if (!isAccountNumberValid)
            {
                throw new Exception(xmlDoc.SelectSingleNode("/DiamondNameEnquiryResponse/StatusMessage").InnerText);
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten dbn account name: {0}", response.ResponseMessage)));
            context.SetValue(AccountName, xmlDoc.SelectSingleNode("/DiamondNameEnquiryResponse/StatusMessage").InnerText);
        }
    }

}
