﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using System.Xml;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetOtherBanksAccountNameDesigner))]
    public class GetOtherBanksAccountNameAction : ActionNode
    {
        
        public InArgument<string> AccountNumber
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        public InArgument<string> DestinationInstitutionCode
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.DestinationInstitutionCode;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.DestinationInstitutionCode = value;
                }
            }
        }

        /// <summary>
        /// account name
        /// </summary>
        public OutArgument<string> AccountName
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.AccountName;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.AccountName = value;
                }
            }
        }
        public OutArgument<string> BankVerificationNumber
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.BankVerificationNumber;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.BankVerificationNumber = value;
                }
            }
        }
        public OutArgument<string> KYCLevel
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.KYCLevel;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.KYCLevel = value;
                }
            }
        }

        public OutArgument<string> SessionID
        {
            get
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    return activity.SessionID;
                }
                return null;
            }
            set
            {
                GetOtherBanksAccountNameActivity activity = this.InternalState as GetOtherBanksAccountNameActivity;
                if (activity != null)
                {
                    activity.SessionID = value;
                }
            }
        }

        public GetOtherBanksAccountNameAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetOtherBanksAccountNameActivity();
        }

    }

    public class GetOtherBanksAccountNameActivity : CodeActivity
    {
    
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> DestinationInstitutionCode { get; set; }
        public InArgument<string> AccountNumber { get; set; }

        public OutArgument<string> AccountName { get; set; }
        public OutArgument<string> BankVerificationNumber { get; set; }
        public OutArgument<string> KYCLevel { get; set; }
        public OutArgument<string> SessionID { get; set; }

        protected override void Execute(CodeActivityContext context)
        {

            string accountNumber = AccountNumber.Get(context);
            string destinationInstitutionCode = DestinationInstitutionCode.Get(context);
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }
            Trace.TraceInformation("*********Account Name Enquiry (Other Banks) Begins*********");
            Trace.TraceInformation(string.Format("AccountNumber: {0}", accountNumber));
            Trace.TraceInformation(string.Format("Destination Inst. Code: {0}", destinationInstitutionCode));

            Random rnd = new Random();
            string randomNumber = rnd.Next(0, 999999).ToString("000000") + rnd.Next(0, 999999).ToString("000000");
            string dt = DateTime.Now.ToString("yyMMddHHmmss");
            string sessionId = "999063" + dt + randomNumber;

            Trace.TraceInformation(string.Format("Session ID: {0}", sessionId));

            string xmlRequest = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8"" ?> 
                                                    <NESingleRequest>
	                                                    <SessionID>{0}</SessionID>
	                                                    <DestinationInstitutionCode>{1}</DestinationInstitutionCode>
	                                                    <ChannelCode>2</ChannelCode>
	                                                    <AccountNumber>{2}</AccountNumber>
                                                    </NESingleRequest>
                                                 ",sessionId,destinationInstitutionCode,accountNumber);

            Trace.TraceInformation(string.Format("Request XML: {0}", xmlRequest));



            //context.SetValue(AccountName, "Test Name");
            //context.SetValue(BankVerificationNumber, "123543323");
            //context.SetValue(KYCLevel, "1");
            //context.SetValue(SessionID, "9990632015121213131398198767");

            object[] response = null;
            Trace.TraceInformation("Service call begins.");
            try
            {
                using (var client = new Services.NibssServiceReference.localnibbsServiceSoapClient())
                {
                    response = client.executeNameEnquiryThruNibbs(xmlRequest);
                }
            }
            catch (TimeoutException ex)
            {
                Trace.TraceError("Check full error details generated by PANE. Message is: " + ex.Message);
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                throw new Exception("No Response from NIBSS within time limit");
            }
            catch (Exception ex)
            {
                Trace.TraceError("Check full error details generated by PANE. Message is: " + ex.Message);
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                throw new Exception("Did not receive Response from NIBSS within time limit");
            }
            Trace.TraceInformation(string.Format("Response: {0}", response == null ? "Null" : "Not Null"));
            Trace.TraceInformation("Service Call Ends.");

            if (response == null)
            {
                throw new Exception("Could not retrieve account name from NIBBS");
            }
            string responseCode = response[0].ToString();
            string responseXml = response[1].ToString();
            if (responseCode == "00")
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(responseXml);
                }
                catch (Exception)
                {
                    Trace.TraceInformation("Invalid response from Diamond Bank. Was not able to load the XML into an XMLDocument");
                    throw new Exception("Invalid response from Diamond Bank.");
                }

                string nameRequestResponseCode = xmlDoc.SelectSingleNode("/NESingleResponse/ResponseCode").InnerText;
                if (nameRequestResponseCode == "00")
                {
                    Trace.TraceInformation(string.Format("Response code: 00. Account Name: {0}, BVN: {1}, KYCLevel: {2}",
                        xmlDoc.SelectSingleNode("/NESingleResponse/AccountName").InnerText, xmlDoc.SelectSingleNode("/NESingleResponse/BankVerificationNumber").InnerText, xmlDoc.SelectSingleNode("/NESingleResponse/KYCLevel").InnerText));
                    context.SetValue(AccountName, xmlDoc.SelectSingleNode("/NESingleResponse/AccountName").InnerText);
                    context.SetValue(BankVerificationNumber, xmlDoc.SelectSingleNode("/NESingleResponse/BankVerificationNumber").InnerText);
                    context.SetValue(KYCLevel, xmlDoc.SelectSingleNode("/NESingleResponse/KYCLevel").InnerText);
                    context.SetValue(SessionID, sessionId);
                }
                else// if ((ResponseCodes)Enum.Parse(typeof(ResponseCodes),responseCode) == "07")
                {
                    Trace.TraceInformation(string.Format("Response Code Recieved: {0}", nameRequestResponseCode));
                    ResponseCodes theResponse = (ResponseCodes)Enum.Parse(typeof(ResponseCodes), nameRequestResponseCode);
                    throw new Exception(PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(theResponse.ToString()));

                }

            }
            else
            {
                Trace.TraceInformation(string.Format("Could not retrieve Account Name. Response Code Recieved: {0}, Message: {1}", response[0].ToString(), responseXml));
                ResponseCodes theResponse = (ResponseCodes)Enum.Parse(typeof(ResponseCodes), responseCode);
                throw new Exception(PANE.Framework.Utility.EnumBinder.SplitAtCapitalLetters(theResponse.ToString()));
            }
            Trace.TraceInformation("*********Account Name Enquiry (Other Banks) Ends*********");
        }
    }

}
