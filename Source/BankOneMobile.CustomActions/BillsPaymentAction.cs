﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(RechargeDesigner))]
    public class BillsPaymentAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        /// Merchant
        /// </summary>
        public InArgument<Merchant> Merchant
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.TheMerchant;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.TheMerchant = value;
                }
            }
        }
        /// <summary>
        /// The Payment Item
        /// </summary>
        public InArgument<PaymentItem> PaymentItem
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.ThePaymentItem;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.ThePaymentItem = value;
                }
            }
        }
        


        /// <summary>
        /// Recharege PIN Code
        /// </summary>
        public OutArgument<string> PurchaseValue {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.PurchaseValue;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.PurchaseValue = value;
                }
            }
        }
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        /// <summary>
        /// Customer ID
        /// </summary>
        public InArgument<string> CustomerID
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.CustomerID;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.CustomerID = value;
                }
            }
        }
        /// <summary>
        /// Recharege PIN Code
        /// </summary>
        public OutArgument<string> PaymentReference
        {
            get
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    return activity.PaymentReference;
                }
                return null;
            }
            set
            {
                BillsPaymentActivity activity = this.InternalState as BillsPaymentActivity;
                if (activity != null)
                {
                    activity.PaymentReference = value;
                }
            }
        }
        public BillsPaymentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new BillsPaymentActivity();
        }

    }

    public class BillsPaymentActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Accoun Number
        /// </summary>
        public InArgument<string>AccountNumber { get; set; }
        /// <summary>
        /// Merchant
        /// </summary>
        public InArgument<Merchant> TheMerchant { get; set; }
        /// <summary>
        /// Payment Item
        /// </summary>
        public InArgument<PaymentItem> ThePaymentItem { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string > Amount { get; set; }

        /// <summary>
        ///Purchase Value ( eg Recharge PIN)
        /// </summary>
        public OutArgument<string> PurchaseValue { get; set; }
        /// <summary>
        ///Payment Reference
        /// </summary>
        public OutArgument<string> PaymentReference { get; set; }
        /// <summary>
        ///Customer ID
        /// </summary>
        public InArgument<string> CustomerID { get; set; }
       

        protected override void Execute(CodeActivityContext context)
        {
            string pin = PIN.Get(context);
            string accountNumber = AccountNumber.Get(context);

            Merchant merch = TheMerchant.Get(context);

            PaymentItem item = ThePaymentItem.Get(context);
            string amount = Amount.Get(context);
            string customerID = CustomerID.Get(context);

            

            //IDataSource source = DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string instCode = string.Empty;
            if (accountNumber == null)
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;

            }
            else
            {
                List<String> vals = accountNumber.Split(':').ToList();
                accountNumber = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;
                mob.InstitutionCode = instCode;
            }
            
            pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);

            //   pin = ThalesSim.Core.Utility.HexStringToByteArray(pin,);

            //decimal amt=  item.UnitPrice == 0M ? item.UnitPrice : Convert.ToDecimal(amount)
                decimal amt = item.UnitPrice==0M?Convert.ToDecimal(amount)*100:Convert.ToDecimal(amount);
            IBillsPaymentTransactionType tranType = new BillsPaymentTransactionType();
            tranType.TheMobileAccount = mob;
            tranType.TheBankAccount = tranType.TheMobileAccount.BankAccounts.Where(x => x.BankAccount == accountNumber).FirstOrDefault() as LinkingBankAccount;
            tranType.TheMerchant = merch;
            tranType.ThePaymentItem = item;
            tranType.MerchantCode = merch.Code;
            tranType.MerchantName = merch.Name;
            tranType.PaymentItemName = item.Name;
            tranType.PaymentItemCode = item.Code;
            tranType.Amount = amt;
            tranType.CustomerID = customerID;
            tranType.TransactionReference = new TransactionSystem().GenerateTransactionRef();
            //tranType.Denomination= Dem
            TransactionResponse response = null;
            tranType.TheFee = new FeeTransactionType { Amount = 100 };

            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, accountNumber, accountNumber,amt, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));

            }
            catch (NoISWGatewayResponse)
            {
                string errorDesc = "No Response from ISW Gateway";
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), errorDesc);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                PaymentReference.Set(context, tranType.TransactionReference);
                //STOP.... CRIMINAL
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            PaymentReference.Set(context, tranType.PaymentReference);
             PurchaseValue.Set(context,tranType.ValuePurchased);
             


            

        }
    }

}
