﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Contracts;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(PhonelessCashInAgentDesigner))]
    public class PhonelessCashInAgentAction : ActionNode
    {
       
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        /// <summary>
        /// User Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public InArgument<bool> IsPhoneNumber
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.IsPhoneNumber;
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.IsPhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// User Amount
        /// </summary>
        public InArgument<string> Bank
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.Bank;
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.Bank = value;
                }
            }
        }
        
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhone_Account
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.CustomerPhone_Account;
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.CustomerPhone_Account = value;
                }
            }
        }
        /// <summary>
        ///Agent Code
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                        
                }
                return null;
            }
            set
            {
                PhonelessCashInAgentActivity activity = this.InternalState as PhonelessCashInAgentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
       
       
       

        public PhonelessCashInAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new PhonelessCashInAgentActivity();
        }

    }

    public class PhonelessCashInAgentActivity : CodeActivity
    {
        /// <summary>
        /// Notification of success/failure
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// Agent Code
        /// </summary>
        public InArgument<string> PIN { get; set; }
        
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// bank
        /// </summary>
        public InArgument<string> Bank { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhone_Account { get; set; }

        public InArgument<bool> IsPhoneNumber { get; set; }

        private LinkingBankAccount theBankAccount { get; set; }
        //private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
        //private ICashInTransactionTypeRepository _repository = SafeServiceLocator<ICashInTransactionTypeRepository>.GetService();
        protected override void Execute(CodeActivityContext context)
        {
            bool isPhoneNumber = IsPhoneNumber.Get(context);
            string pin = PIN.Get(context);
            
            string acctNo = CustomerPhone_Account.Get(context);
            if(isPhoneNumber)
            {
                
            }
           WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            string notification = Notification.Get(context);
            string amount = Amount.Get(context);
            string customerPhone_Account = CustomerPhone_Account.Get(context);
            Agent agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            decimal decAmt = Convert.ToDecimal(amount);
            if (agent.PostingLimit >Convert.ToDecimal( 0.00) && agent.PostingLimit < decAmt)
            {
                throw new AboveTransactionLimitException(string.Format("This amount is above your posting limit. please enter an amount below =N={0}",agent.PostingLimit.ToString("N")));
            }
            string agentCode =agent .Code;
            
            
            string realResponse = string.Empty;
            
            
            ICashInTransactionType tranType = new CashInTransactionType();
            
           // tranType.TheFee = new FeeTransactionType { Amount = 100 };
            string instCode = string.Empty;
            
            var mobileAcctSystem = new MobileAccountSystem(currentWorkflowInfo.DataSource);
            if (isPhoneNumber&& customerPhone_Account == null)// Meaning  the mobile acct has only 1  Bank Account
            {
                //customerPhone_Account = mob.BankAccounts[0].BankAccount;
                //instCode = mob.BankAccounts[0].InstitutionCode;
                //mob.InstitutionCode = instCode;
                //theBankAccount = mob.BankAccounts[0] as LinkingBankAccount;
                
                MobileAccount custMob = mobileAcctSystem.GetByPhoneNumber(Bank.Get(context));
                //MobileAccount custMob = null;
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("About to get mobile account. PHONE: {0} | INSTITUTION CODE: {1}", Bank.Get(context), mob.InstitutionCode)));
                //using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                //{
                //    custMob = new MobileAccountSystem(source).GetMobileAccountByPhoneNumber(mob.InstitutionCode, Bank.Get(context)) as MobileAccount;
                //}

                customerPhone_Account = custMob.BankAccounts[0].BankAccount;
                instCode = custMob.BankAccounts[0].InstitutionCode;
                theBankAccount = custMob.BankAccounts[0] as LinkingBankAccount;

            }
            else if (isPhoneNumber)
            {
            }
            //else if (isPhoneNumber)
            //{
            //    MobileAccount custMob =new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(customerPhone_Account); 
            //    customerPhone_Account =custMob.BankAccounts[0].BankAccount;
            //    instCode = custMob.BankAccounts[0].InstitutionCode;
            //    theBankAccount = custMob.BankAccounts[0];
            //}
            //else if (isPhoneNumber)
            //{
            //}
            else
            {
                List<String> vals = customerPhone_Account.Split(':').ToList();
                if (vals.Count == 1)
                {
                    LinkingBankAccount acc = mobileAcctSystem.GetBankAccountByAcccountNo(vals[0], agent.TheAgentAccount.InstitutionCode);
                    theBankAccount = acc;
                    instCode = acc.InstitutionCode;

                    customerPhone_Account = vals[0];
                }
                else
                {
                    customerPhone_Account = vals[0];
                    theBankAccount = mobileAcctSystem.GetBankAccountByAcccountNo(customerPhone_Account,agent.TheAgentAccount.InstitutionCode);
                    instCode = theBankAccount.InstitutionCode;
                }
            }
            if (isPhoneNumber)
            {
                List<String> vals = customerPhone_Account.Split(':').ToList();
                if (vals.Count > 1)
                {
                    customerPhone_Account = vals[0];
                }
               theBankAccount=mobileAcctSystem.GetBankAccountByAcccountNo(customerPhone_Account,agent.TheAgentAccount.InstitutionCode);
               IMobileAccount mm = theBankAccount.TheMobileAccount;
               instCode = theBankAccount.InstitutionCode;
                if (mm.MobileAccountStatus == MobileAccountStatus.AwaitingApproval || theBankAccount.BankStatus == BankAccountStatus.AwaitingApproval)
                {
                    
                    Transaction trans = new Transaction();
                    trans.AgentCode = agentCode;
                    
                    trans.Amount = Convert.ToDecimal(amount)*100;
                    trans.Date = DateTime.Now;
                    trans.From = agent.TheAgentAccount.BankAccount;
                    trans.FromPhoneNumber = agent.TheAgentAccount.TheMobileAccount.MobilePhone;
                    trans.InstitutionCode = agent.InstitutionCode;
                    trans.FromInstitutionCode =agent.InstitutionCode;
                    trans.To = customerPhone_Account;
                    trans.TransactionTypeName = TransactionTypeName.CashIn;
                    trans.StatusDetails = "Mobile Account not  Yet Approved";
                    trans.Status = TransactionStatus.Failed;
                    tranType.TheTransaction=trans;
                    tranType.TheBankAccount = theBankAccount;
                    tranType.EnteredParameter = customerPhone_Account;
                    tranType.IsPhoneNumber =! isPhoneNumber;
                    tranType.AgentCode = agentCode;
                    CashInTransactionTypeSystem.SaveCashInTransactionType(currentWorkflowInfo.DataSource, tranType as CashInTransactionType);
                   //_repository.Save(_theDataSource, tranType);
                   
                    //_repository.DbContext.CommitTransaction(_theDataSource);
                    throw new FailureException("06", "The Linking Between this phone number and the  customers Account  has not been approved." + System.Environment.NewLine + "Let him/her Enter the account number for the deposit");
                }
                if (theBankAccount.BankStatus == BankAccountStatus.InActive)
                {

                    Transaction trans = new Transaction();
                    trans.AgentCode = agentCode;

                    trans.Amount = Convert.ToDecimal(amount) * 100;
                    trans.Date = DateTime.Now;
                    trans.From = agent.TheAgentAccount.BankAccount;
                    trans.FromPhoneNumber = agent.TheAgentAccount.TheMobileAccount.MobilePhone;
                    trans.InstitutionCode = agent.InstitutionCode;
                    trans.FromInstitutionCode = agent.InstitutionCode;
                    trans.To = customerPhone_Account;
                    trans.TransactionTypeName = TransactionTypeName.CashIn;
                    trans.StatusDetails = "Mobile Account not  Yet Approved";
                    trans.Status = TransactionStatus.Failed;
                    tranType.TheTransaction = trans;
                    tranType.TheBankAccount = theBankAccount;
                    tranType.EnteredParameter = customerPhone_Account;
                    tranType.IsPhoneNumber = !isPhoneNumber;
                    tranType.AgentCode = agentCode;
                    CashInTransactionTypeSystem.SaveCashInTransactionType(currentWorkflowInfo.DataSource, tranType as CashInTransactionType);
                    //_repository.Save(_theDataSource, tranType);

                    ///_repository.DbContext.CommitTransaction(_theDataSource);
                    throw new FailureException("06", "Inactive Account");
                }
               

            }
           // string agentPhone = currentWorkflowInfo.ThePreviousWorkflow.InitiatedBy.MobilePhone;
            //Agent theAgent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(agentPhone) as Agent;
            tranType.Amount = Convert.ToDecimal(amount);
            tranType.AgentCode = agentCode;
            tranType.Token = instCode;// just save it her cos we need it
            tranType.IsPhoneNumber = isPhoneNumber;
            tranType.EnteredParameter = customerPhone_Account;
            tranType.TheBankAccount = theBankAccount;
            TransactionResponse response = new TransactionResponse();
           
            try
            {
                string mmg = string.Format("Dep values: agent Acc:{0}|customer Acc:{1}|", agent.TheAgentAccount.BankAccount, customerPhone_Account);
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(mmg));

                response = new TransactionSystem().RunTransaction(pin, tranType, mob, agent.TheAgentAccount.BankAccount, customerPhone_Account, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                if (response.Status == TransactionStatus.Successful.ToString())
                {
                    realResponse = "true";
                }
               
               
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents);
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
                
            }
            catch (TransactionNotSupportedByInstitution)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.InterMFBTransactionsNotSupportedByInstitution);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InterMFBTransactionsNotSupportedByInstitution), Utilities.GetDescription(ErrorCodes.InterMFBTransactionsNotSupportedByInstitution));

            }
            catch (InvalidAgentCodeException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.InvalidAgentCode);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.SwitchUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                realResponse=Utilities.GetDescription(ErrorCodes.IssuerUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                realResponse=ex.Message;
                throw new FailureException("13", ex.Message);
            }
            catch (AboveTransactionLimitException ex)
            {
                realResponse = ex.Message;
                throw new FailureException(Utilities.GetCode(ErrorCodes.AmountIsAbovePostingLimt), ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                realResponse="System Error";
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {
                
                Notification.Set(context,realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }



           //tring result = TransactionSystem.RunTransaction(pin);
            //context.SetValue(Notification,response.ResponseMessage);
            //string result = TransactionSystem.RunTransaction(pin);

            

        }

            

        }
    

}
