﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(ValidateDbnNonAccountTransferTokenDesigner))]
    public class ValidateDbnNonAccountTransferTokenAction : ActionNode
    {

        public InArgument<string> OriginatorPhoneNumber
        {
            get
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {

                    return activity.OriginatorPhoneNumber;
                }
                return null;
            }
            set
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.OriginatorPhoneNumber = value;
                }
            }
        }

        public InArgument<string> BeneficiaryPhoneNumber
        {
            get
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {

                    return activity.BeneficiaryPhoneNumber;
                }
                return null;
            }
            set
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.BeneficiaryPhoneNumber = value;
                }
            }
        }
        public InArgument<string> EnteredToken
        {
            get
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {

                    return activity.EnteredToken;
                }
                return null;
            }
            set
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.EnteredToken = value;
                }
            }
        }

        public InArgument<string> Amount
        {
            get
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public OutArgument<DBNFundsTransferTransactionType> TransactionType
        {
            get
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {

                    return activity.TransactionType;
                }
                return null;
            }
            set
            {
                ValidateDbnNonAccountTransferTokenActivity activity = this.InternalState as ValidateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.TransactionType = value;
                }
            }
        }
      
        public ValidateDbnNonAccountTransferTokenAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ValidateDbnNonAccountTransferTokenActivity();
        }

    }

    public class ValidateDbnNonAccountTransferTokenActivity : CodeActivity
    {


        public InArgument<string> OriginatorPhoneNumber { get; set; }
        public InArgument<string> BeneficiaryPhoneNumber { get; set; }
        public InArgument<string> EnteredToken { get; set; }
        public InArgument<string> Amount { get; set; }
        public OutArgument<DBNFundsTransferTransactionType> TransactionType { get; set; }

        
        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string originatorPhoneNumber = OriginatorPhoneNumber.Get(context);
            string beneficiaryPhoneNumber = BeneficiaryPhoneNumber.Get(context);
            string amount = Amount.Get(context);
            string token = EnteredToken.Get(context);

            List<IDBNFundsTransferTransactionType> tranType = null;
            Dictionary<string, object> propertyValuePairs = new Dictionary<string, object>();
            //propertyValuePairs.Add("AgentPhoneNumber", mob.MobilePhone);
            propertyValuePairs.Add("OriginatorAccountNumber", originatorPhoneNumber);
            propertyValuePairs.Add("BeneficiaryAccountNumber", beneficiaryPhoneNumber);
            propertyValuePairs.Add("SessionID", token);
            propertyValuePairs.Add("ResponseCode", "Pending");
            propertyValuePairs.Add("Amount", decimal.Parse(amount));
            propertyValuePairs.Add("DBNFundsTransferType", DBNFundsTransferTransactionTypeName.NonAccountHolder);
            try
            {
                tranType = new DBNFundsTransferTransactionTypeSystem().FindFundsTransferCashInTransactionTypes(currentWorkflowInfo.DataSource,propertyValuePairs);  
                if(tranType == null || tranType.Count == 0)
                {
                    throw new Exception(string.Format("There is no pending transfer from {0} to {1} with token: {2}", originatorPhoneNumber, beneficiaryPhoneNumber, token));
                }
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            finally
            {


                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.....THIS IS CRIMINAL...
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            TransactionType.Set(context, tranType.FirstOrDefault());
        }
    }
}
