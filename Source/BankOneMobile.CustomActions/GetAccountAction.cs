﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetAccountDesigner))]
    public class GetAccountAction : ActionNode
    {
        
        public InArgument<string> PhoneNumber
        {
            get
            {
                GetAccountActivity activity = this.InternalState as GetAccountActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                GetAccountActivity activity = this.InternalState as GetAccountActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        /// <summary>
        /// account
        /// </summary>
        public OutArgument<BankOneMobile.Services.SwitchServiceRef.Account> TheAccount 
        {
            get
            {
                GetAccountActivity activity = this.InternalState as GetAccountActivity;
                if (activity != null)
                {
                    return activity.TheAccount;
                }
                return null;
            }
            set
            {
                GetAccountActivity activity = this.InternalState as GetAccountActivity;
                if (activity != null)
                {
                    activity.TheAccount = value;
                }
            }
        }

        public GetAccountAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetAccountActivity();
        }

    }

    public class GetAccountActivity : CodeActivity
    {
    
        /// <summary>
        /// Recipient  Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
    
        public OutArgument<BankOneMobile.Services.SwitchServiceRef.Account> TheAccount { get; set; }

        protected override void Execute(CodeActivityContext context)
        {

            string phoneNumber = PhoneNumber.Get(context);
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }

            BankOneMobile.Services.SwitchServiceRef.Account[] ac = null;
            using (BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient client = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
            {
                ac = client.GetAccountsByPhoneNo(instCode, phoneNumber);
            }
            if (ac == null || ac.Length == 0)
            {
                throw new Exception("Phone number is not registered");
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("gotten account by phone: NUBAN: {0}", ac[0].NUBAN)));
            context.SetValue(TheAccount, ac[0]);
            //TODO: Do Balance Enquiry
            //string rechargePInCode = TransactionSyst    unTransaction(pin);
            //RechargePIN.Set(context,rechargePInCode);
        }
    }

}
