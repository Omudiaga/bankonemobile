﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;
using System.Net;
using System.IO;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(OneCreditLoanOriginationDesigner))]
    public class OneCreditLoanOriginationAction : ActionNode
    {
        public InArgument<string> PhoneNumber
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        public InArgument<string> AccountNumber
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        public InArgument<string> BusinessDescription
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.BusinessDescription;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.BusinessDescription = value;
                }
            }
        }

        public InArgument<string> BusinessAddress
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.BusinessAddress;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.BusinessAddress = value;
                }
            }
        }

        public InArgument<string> BusinessCity
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.BusinessCity;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.BusinessCity = value;
                }
            }
        }

        public InArgument<string> BusinessState
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.BusinessState;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.BusinessState = value;
                }
            }
        }

        public InArgument<string> HomeAddress
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.HomeAddress;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.HomeAddress = value;
                }
            }
        }

        public InArgument<string> HomeCity
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.HomeCity;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.HomeCity = value;
                }
            }
        }

        public InArgument<string> HomeState
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.HomeState;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.HomeState = value;
                }
            }
        }

        //public InArgument<string> IdentificationNumber
        //{
        //    get
        //    {
        //        OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
        //        if (activity != null)
        //        {
        //            return activity.IdentificationNumber;
        //        }
        //        return null;
        //    }
        //    set
        //    {
        //        OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
        //        if (activity != null)
        //        {
        //            activity.IdentificationNumber = value;
        //        }
        //    }
        //}

        //public InArgument<string> IdentificationType
        //{
        //    get
        //    {
        //        OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
        //        if (activity != null)
        //        {
        //            return activity.IdentificationType;
        //        }
        //        return null;
        //    }
        //    set
        //    {
        //        OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
        //        if (activity != null)
        //        {
        //            activity.IdentificationType = value;
        //        }
        //    }
        //}

        public InArgument<string> MaritalStatus
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.MaritalStatus;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.MaritalStatus = value;
                }
            }
        }

        public InArgument<string> LoanAmount
        {
            get
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    return activity.LoanAmount;
                }
                return null;
            }
            set
            {
                OneCreditLoanOriginationActivity activity = this.InternalState as OneCreditLoanOriginationActivity;
                if (activity != null)
                {
                    activity.LoanAmount = value;
                }
            }
        }

        public OneCreditLoanOriginationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new OneCreditLoanOriginationActivity();
        }

    }

    public class OneCreditLoanOriginationActivity : CodeActivity
    {
        public InArgument<string> PhoneNumber { get; set; }
        public InArgument<string> AccountNumber { get; set; }
        public InArgument<string> BusinessDescription { get; set; }
        public InArgument<string> BusinessAddress { get; set; }
        public InArgument<string> BusinessCity { get; set; }
        public InArgument<string> BusinessState { get; set; }
        public InArgument<string> HomeAddress { get; set; }
        public InArgument<string> HomeCity { get; set; }
        public InArgument<string> HomeState { get; set; }
        //public InArgument<string> IdentificationType { get; set; }
        //public InArgument<string> IdentificationNumber { get; set; }
        public InArgument<string> MaritalStatus { get; set; }
        public InArgument<string> LoanAmount { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            string phoneNumber = PhoneNumber.Get(context);
            string accountNumber = AccountNumber.Get(context);
            string businessDescription = BusinessDescription.Get(context);
            string businessAddress = BusinessAddress.Get(context);
            string businessCity = BusinessCity.Get(context);
            string businessState = BusinessState.Get(context);
            string homeAddress = HomeAddress.Get(context);
            string homeCity = HomeCity.Get(context);
            string homeState = HomeState.Get(context);
            //string idNumber = IdentificationNumber.Get(context);
            //string idType = IdentificationType.Get(context);
            string maritalStatus = MaritalStatus.Get(context);
            long loanAmount = long.Parse(LoanAmount.Get(context));

            MobileAccount customerMobileAccount = null;
            LinkingBankAccount bankAccount = null;
            string instCode = string.Empty;

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;

            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch
            {
            }

            try
            {
                using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                {
                    customerMobileAccount = new MobileAccountSystem(source).GetMobileAccountByPhoneNumber(mob.InstitutionCode, phoneNumber) as MobileAccount;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error trying to get customer mobile account.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
            }

            if (customerMobileAccount == null)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd), Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
            }
            else
            {
                if (loanAmount < 5000 || loanAmount > 50000)
                {
                    throw new Exception("Loan Amount must be between N5,000 and N50,000.");
                }

                bankAccount = customerMobileAccount.BankAccountToDebit as LinkingBankAccount;
                //Get customer info from corebanking and last 6 months transaction
                BankOneMobile.Services.SwitchServiceRef.Customer customer = null;
                //List<BankOneMobile.Services.SwitchServiceRef.MiniStatementHistory> transactions
                try
                {
                    System.Diagnostics.Trace.TraceError("About to get customer from core banking.");
                    using (var theClient = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
                    {
                        customer = theClient.GetCustomer(customerMobileAccount.InstitutionCode, bankAccount.CustomerID);
                    }
                    System.Diagnostics.Trace.TraceError("Gotten customer from core banking.");
                }
                catch(Exception ex)
                {
                    System.Diagnostics.Trace.TraceError("Caught an error trying to get customer details from corebanking.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                    //Do not display technical errors to external systems.
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
                }
                if(customer == null)
                {
                    throw new Exception("Could not retrieve customer details.");
                }

                if(string.IsNullOrWhiteSpace(customer.BankVerificationNumber))
                {
                    throw new Exception("Customer must have a BVN to proceed.");
                }

                string requestData = string.Concat(customerMobileAccount.InstitutionCode, "|", customerMobileAccount.CustomerID);
                string responseFromServer = string.Empty;
                try
                {
                    System.Diagnostics.Trace.TraceError("About to get customer score from credit assessment...Request Data: {0}",requestData);
                    WebRequest request_ = null;
                    // Create a request using a URL that can receive a post.
                    if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsTestMode"]))
                        request_ = WebRequest.Create("http://localhost/CreditAssessment/services/GetCustomerCreditScore.aspx");
                    else
                        request_ = WebRequest.Create("http://www.mybankone.com/CreditAssessment/services/GetCustomerCreditScore.aspx");

                    // Set the Method property of the request to POST.
                    request_.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    string postData = requestData;
                    byte[] byteArray_ = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    //request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request_.ContentLength = byteArray_.Length;
                    // Get the request stream.
                    Stream dataStream_ = request_.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream_.Write(byteArray_, 0, byteArray_.Length);
                    // Close the Stream object.
                    dataStream_.Close();
                    // Get the response.
                    WebResponse webResponse = request_.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream_ = webResponse.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader_ = new StreamReader(dataStream_);
                    // Read the content.
                    responseFromServer = reader_.ReadToEnd();

                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader_.Close();
                    dataStream_.Close();
                    webResponse.Close();
                    System.Diagnostics.Trace.TraceError("Gotten customer score from credit assessment...Response:{0}",responseFromServer);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError("Caught an error trying to get customer credit score.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                    //Do not display technical errors to external systems.
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
                }

                int value = 0;
                if(!int.TryParse(responseFromServer, out value))
                {
                    throw new Exception("Error retrieving customer credit score");
                }

                if(value == -1)
                {
                    throw new Exception("Error retrieving customer credit score");
                }
                
                #region Log Loan Request
                List<accountStatement> statements = new List<accountStatement>();
                var theHomeAddress = new address
                {
                    city = homeCity,
                    country = "",
                    line2 = "",
                    postalCode = "",
                    line1 = homeAddress,
                    state = homeState
                };
                var theBusinessAddress = new address
                {
                    city = businessCity,
                    country = "",
                    line2 = "",
                    postalCode = "",
                    line1 = businessAddress,
                    state = businessState
                };
                var nextOfKin = new nextOfKin
                {
                    name = customer.NextOfKinName,
                    phoneNumber = customer.NextOfKinPhoneNumber
                };
                //var statement = new accountStatement
                //{
                //    amount = 100,
                //    narration = "Test narration",
                //    transactionDate = DateTime.Now.Truncate(TimeSpan.FromMilliseconds(1)).Truncate(TimeSpan.FromSeconds(1)),
                //};
                //statements.Add(statement);

                //PASSPORT,NATIONAL_ID,DRIVERS_LICENSE,VOTERS_CARD,EMPLOYMENT_ID,OTHER
                //var identification = new identification
                //{
                //    idNumber = "12345",
                //    idType = "PASSPORT"
                //};
                var client = new client
                {
                    accountNumber = bankAccount.NUBAN,
                    birthDate = customer.DateOfBirth == null?new DateTime(1985,1,1) : customer.DateOfBirth.Value.Truncate(TimeSpan.FromMilliseconds(1)).Truncate(TimeSpan.FromSeconds(1)),
                    businessAddress = theBusinessAddress,
                    homeAddress = theHomeAddress,
                    businessDescription = businessDescription,
                    bvn = customer.BankVerificationNumber,
                    creditScore = value,
                    email = "",
                    firstName = customer.FirstName,
                    //FEMALE,MALE
                    gender = customer.Gender == Services.SwitchServiceRef.Gender.Female? "FEMALE" : "MALE",
                    identification = new identification(),
                    lastName = customer.LastName,
                    //SINGLE,MARRIED,WIDOWED,DIVORCED
                    maritalStatus = maritalStatus,
                    mobileNumber1 = customerMobileAccount.MobilePhone,
                    nextOfKin = nextOfKin
                };
                var onecreditLoanOrigination = new OneCreditLoanOrigination
                {
                    client = client,
                    amount = loanAmount * 100,//In Kobo
                    //statements = statements
                };

                string output = JsonConvert.SerializeObject(onecreditLoanOrigination);

                OneCreditLoanRequest loanRequest = new OneCreditLoanRequest
                {
                    AgentPhoneNumber = mob.MobilePhone,
                    CustomerPhoneNumber = customerMobileAccount.MobilePhone,
                    DateLogged = DateTime.Now,
                    InstitutionCode = customerMobileAccount.InstitutionCode,
                    JsonRequest = output,
                    Status = Core.Helpers.Enums.RegistrationStatus.Pending
                };

                try
                {
                    System.Diagnostics.Trace.TraceError("About to save one credit loan request");
                    new OneCreditLoanRequestSystem().SaveOneCreditLoanRequestSystem(currentWorkflowInfo.DataSource, loanRequest);
                    System.Diagnostics.Trace.TraceError("Done saving one credit loan request");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError("Caught an error trying to save service based customer registration details.");
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                    //Do not display technical errors to external systems.
                    throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
                }
                finally
                {
                }

                #endregion
                //PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Response: {0}", responseFromServer)));
            }


        }
    }

    public class Response
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }

    public static class Extension
    {
        public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
}
