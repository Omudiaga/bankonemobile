﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetMerchantsActionDesigner))]
    public class GetMerchantsAction : ActionNode
    {
      
       

        public OutArgument<IList<Merchant>> Merchants
        {
            get
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    return activity.Merchants;
                }
                return null;
            }
            set
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    activity.Merchants = value;
                }
            }
        }
        public InArgument<MerchantCategory> TheMerchantCategory
        {
            get
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    return activity.TheMerchantCategory;
                }
                return null;
            }
            set
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    activity.TheMerchantCategory = value;
                }
            }
        }
        public InArgument<bool> IsRecharge
        {
            get
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    return activity.IsRecharge;
                }
                return null;
            }
            set
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    activity.IsRecharge = value;
                }
            }
        }
        public InArgument<string> MerchantCategoryCode
        {
            get
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    return activity.MerchantCategoryCode;
                }
                return null;
            }
            set
            {
                GetMerchantsActivity activity = this.InternalState as GetMerchantsActivity;
                if (activity != null)
                {
                    activity.MerchantCategoryCode = value;
                }
            }
        }
        public GetMerchantsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetMerchantsActivity();
        }

    }

    public class GetMerchantsActivity : CodeActivity
    {


        public OutArgument<IList<Merchant>> Merchants { get; set; }
        public InArgument<MerchantCategory> TheMerchantCategory { get; set; }
        public InArgument<bool> IsRecharge { get; set; }
        public InArgument<string> MerchantCategoryCode { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            MerchantCategory cat = TheMerchantCategory.Get(context);
            bool isRecharge = IsRecharge.Get(context);
            string categoryCode = MerchantCategoryCode.Get(context);

            IList<Merchant> merch = null;
            try
            {
                if (isRecharge)
                {
                    merch = new MerchantSystem().GetRechargeMerchants();
                    foreach (var merchant in merch)
                    {
                        //Show only firat word
                        //e.g Airtel Mobile Topup ==> Airtel
                        var indexOfFirstSpaceCharacter = merchant.Name.IndexOf(' ');
                        merchant.Name = merchant.Name.Substring(0, indexOfFirstSpaceCharacter);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(categoryCode))
                    {
                        merch = new MerchantSystem().GetMerchantByCategory(cat);
                    }
                    else
                    {
                        merch = new MerchantSystem().GetMerchantsByCategoryID(categoryCode);
                    }
                }
            }
            catch (FaultException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), Utilities.GetDescription(ErrorCodes.ISWGatewayUnavailable));
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), "Error on ISW Gateway");
            }
            
            Merchants.Set(context, merch);
            


        }
    }
}
