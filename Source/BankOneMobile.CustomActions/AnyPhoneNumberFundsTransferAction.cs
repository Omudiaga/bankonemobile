﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Services.Utility;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(AnyPhoneNumberLocalFundsTransferDesigner))]
    public class AnyPhoneNumberFundsTransferAction : ActionNode
    {
        /// <summary>
        /// User PhoneNumber
        /// </summary>
        public InArgument<string> PhoneNumber
        {
            get
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }
        /// <summary>
        /// Notification
        /// </summary>
        public OutArgument<string> Notification
        {
            get
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Notification;
                }
                return null;
            }
            set
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    activity.Notification = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN {
            get
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Transfer Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                AnyPhoneNumberFundsTransferActivity activity = this.InternalState as AnyPhoneNumberFundsTransferActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        /// <summary>
        /// Transfer Amount
        /// </summary>



        public AnyPhoneNumberFundsTransferAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new AnyPhoneNumberFundsTransferActivity();
        }

    }

    public class AnyPhoneNumberFundsTransferActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> Notification { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PhoneNumber { get; set; }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Selected AccoutNumber
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        /// <summary>
        /// Transfer Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            string pin = PIN.Get(context);
            
            string amount = Amount.Get(context);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = PhoneNumber.Get(context);

            string accountNumber = AccountNumber.Get(context);
            if (string.IsNullOrEmpty(accountNumber))
            {
                accountNumber = mob.BankAccounts[0].BankAccount;
            }
            IFundsTransferCashInTransactionType tranType = new FundsTransferCashInTransactionType();
            tranType.FromSelfService = true;
            tranType.Amount = Convert.ToDecimal(amount);
            tranType.DepositorPhoneNumber = mob.MobilePhone;
            
            tranType.ClearToken = new FundsTransferCashInTransactionTypeSystem().GenerateToken();
            tranType.Token = new MD5Password().CreateSecurePassword(tranType.ClearToken);
            tranType.IsCash = false;// cash is always gotten from the customer to the agent
            tranType.DepositorPhoneNumber = mob.MobilePhone;
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            tranType.BeneficiaryPhoneNumber = phoneNumber;

            try
             {
              TransactionResponse  response = new TransactionSystem().RunTransaction(pin, tranType, mob,accountNumber,phoneNumber, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
              Notification.Set(context, tranType.ClearToken);
            }

            
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {


                //BankOneMobile.Data.NHibernate.DbContext.Instance.Close(DataSourceFactory.GetDataSource(DataCategory.Shared));

                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);

                }

            }

        }
    }

}
