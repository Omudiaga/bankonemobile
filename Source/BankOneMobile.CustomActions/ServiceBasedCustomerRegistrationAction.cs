﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CustomerRegistrationDesigner))]
    public class ServiceBasedCustomerRegistrationAction : ActionNode
    {
        /// <summary>
        /// Product Code
        /// </summary>
        public InArgument<string> ProductCode
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ProductCode;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ProductCode = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
       
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerName
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerName;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerName = value;
                }
            }
        }
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerFirstName
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerFirstName;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerFirstName = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Customer Gender
        /// </summary>
        public InArgument<string> Gender
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Gender;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Gender = value;
                }
            }
        }
        /// <summary>
        ///Product for the Account
        /// </summary>
        public InArgument<Product> Product
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }

        public InArgument<string> PlaceOfBirth
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PlaceOfBirth;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PlaceOfBirth = value;
                }
            }
        }
        public InArgument<string> NOKName
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.NOKName;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.NOKName = value;
                }
            }
        }
        public InArgument<string> NOKPhone
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.NOKPhone;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.NOKPhone = value;
                }
            }
        }
        public InArgument<string> Address
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Address;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Address = value;
                }
            }
        }
        public InArgument<string> DateOfBirth
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.DateOfBirth;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.DateOfBirth = value;
                }
            }
        }
        public InArgument<string> StarterPackNumber
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.StarterPackNumber;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.StarterPackNumber = value;
                }
            }
        }
        public InArgument<string> IDNumber
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.IDNumber;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.IDNumber = value;
                }
            }
        }
        public InArgument<string> ReferralNumber
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ReferralPhoneNumber;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ReferralPhoneNumber = value;
                }
            }
        }
        public InArgument<string> OtherAccountnInfo
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.OtherAccountInfo;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.OtherAccountInfo = value;
                }
            }
        }
        public InArgument<string> AccountSource
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountSource;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountSource = value;
                }
            }
        }
        public InArgument<bool> HasSufficientInfo
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.HasSufficientInfo;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.HasSufficientInfo = value;
                }
            }
        }

        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }


             
        }
        public OutArgument<string> AccountNumber
        {
            get
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                ServiceBasedCustomerRegistrationActivity activity = this.InternalState as ServiceBasedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }



        }

         
        public ServiceBasedCustomerRegistrationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ServiceBasedCustomerRegistrationActivity();
        }

    }

    public class ServiceBasedCustomerRegistrationActivity : CodeActivity
    {
        private const string CONFIRMATION_TEXT = "The Customer has been successfully registered on Bank One.Please notify them of this Activation Code:";
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Customer Full Name
        /// </summary>
        public InArgument<string> CustomerName { get; set; }
        
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Customer First Name
        /// </summary>
        public InArgument<string> CustomerFirstName { get; set; }

        /// <summary>
        /// Customer Gender
        /// </summary>
        public InArgument<string> Gender { get; set; }
        /// <summary>
        /// Product for the Account
        /// </summary>
        public InArgument<Product> Product { get; set; } 
        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode { get; set; }
        /// <summary>
        /// Product Code
        /// </summary>

        public InArgument<string> ProductCode { get; set; }

        public InArgument<string> PlaceOfBirth { get; set; }
        public InArgument<string> DateOfBirth { get; set; }
        public InArgument<string> IDNumber { get; set; }
        public InArgument<string> NOKName { get; set; }
        public InArgument<string> NOKPhone { get; set; }
        public InArgument<string> Address { get; set; }
        public InArgument<string> ReferralPhoneNumber { get; set; }
        public InArgument<string> OtherAccountInfo { get; set; }
        public InArgument<bool> HasSufficientInfo { get; set; }
        public InArgument<string> AccountSource { get; set; }

        public InArgument<string> StarterPackNumber { get; set; }

        public OutArgument<string> AccountNumber { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
            //System.Diagnostics.Trace.TraceInformation("start");
            string pin = PIN.Get(context);
            string customerName = CustomerName.Get(context);
            string customerFirstName = CustomerFirstName.Get(context);
            string customerPhone = CustomerPhoneNumber.Get(context);
            string gender = Gender.Get(context);
            Product prod = Product.Get(context);
            //System.Diagnostics.Trace.TraceInformation("Got here. -1");
           string starterPack = StarterPackNumber.Get(context);
           string address = Address.Get(context);
           
           string nokPhone = NOKPhone.Get(context);
           string nokName = NOKName.Get(context);
            string placeOfBirth = PlaceOfBirth.Get(context);
            //System.Diagnostics.Trace.TraceInformation("Got here. 0");

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string dateOfBirth = DateOfBirth.Get(context);
            //System.Diagnostics.Trace.TraceInformation("Got here. 1");
            ICustomerRegistration tranType = new CustomerRegistration();
            ICustomerRegistrationImage ImagetranType = new CustomerRegistrationImage();
            tranType.LastName = customerName;
            tranType.FirstName = customerFirstName;
            tranType.PhoneNumber = customerPhone;
            tranType.TheGender = (Gender)Enum.Parse(typeof(Gender), gender);
            //System.Diagnostics.Trace.TraceInformation("Got here. 2");
            tranType.ProductCode = prod.Code;
            tranType.ProductName = prod.Name;
            tranType.InstitutionCode = mob.InstitutionCode;
            //System.Diagnostics.Trace.TraceInformation("Got here. ABout to get inst");
            var institution = new InstitutionSystem().GetByCode(currentWorkflowInfo.DataSource, mob.InstitutionCode);
            if(institution != null)
            {
                tranType.InstitutionName = institution.Name;
            }
            //System.Diagnostics.Trace.TraceInformation("Got here. 3");
            tranType.NOKName = nokName;
            tranType.NOKPhone = nokPhone;
            tranType.Address = address;
            tranType.CardSerialNumber = starterPack;
            tranType.AgentName = mob.LastName + " " + mob.OtherNames;
            Agent agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (agent == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Agents");
            }
            if (!agent.IsActive)
            {
                throw new TransactionNotPermittedToNonAgentsException("This transaction is only permitted to Active Agents");
            }
            tranType.AccountOfficerCode = agent.TheAgentAccount.BankAccount;
            tranType.AgentCode = agent.Code;

            DateTime dob = ParseDate(dateOfBirth);

            if(dob ==null)
                throw new FailureException("06", "Invalid Date Entry");
            
            // DateTime.TryParse(dateOfBirth, out dob);
             tranType.DateOfBirth = dob;
            tranType.PlaceOfBirth = placeOfBirth;
            tranType.Date = DateTime.Now;
            if (mob.InstitutionCode != "100009")
            {
                //Get passport image
                System.Diagnostics.Trace.TraceInformation("About to get image from server location.");
                string path = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("ImageUploadPath") ?
                    System.Configuration.ConfigurationManager.AppSettings["ImageUploadPath"] : "C:\\";
                string filename = string.Format(@"{0}{1}.png", path, sessionID.Replace("+", "").Trim());

                try
                {
                    // Create a bitmap of the content of the fileUpload control in memory
                    Bitmap originalBMP = new Bitmap(filename);
                    //System.Diagnostics.Trace.TraceInformation(string.Format("Image width {0}, height {1}", image.Width, image.Height));
                    // Calculate the new image dimensions
                    decimal origWidth = originalBMP.Width;
                    decimal origHeight = originalBMP.Height;
                    decimal sngRatio = origWidth / origHeight;
                    int newWidth = 150;
                    int newHeight = decimal.ToInt32(newWidth / sngRatio);

                    // Create a new bitmap which will hold the previous resized bitmap
                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                    // Create a graphic based on the new bitmap
                    Graphics oGraphics = Graphics.FromImage(newBMP);

                    // Set the properties for the new graphic file
                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    // Draw the new graphic based on the resized bitmap
                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                    ImageCodecInfo pngEncoder = GetEncoder(ImageFormat.Jpeg);

                    System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);

                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                    myEncoderParameters.Param[0] = myEncoderParameter;

                    var ms = new MemoryStream();
                    newBMP.Save(ms, pngEncoder, myEncoderParameters);
                    System.Diagnostics.Trace.TraceInformation("Gotten image from server location. about to run transaction.");
                    ImagetranType.Passport = ms.ToArray();
                    //tranType.Passport = ms.ToArray();

                    // Save the new graphic file to the server
                    //newBMP.Save("C:\\Images\\test.jpg", pngEncoder, myEncoderParameters);

                    

                    // Once finished with the bitmap objects, we deallocate them.
                    originalBMP.Dispose();
                    newBMP.Dispose();
                    oGraphics.Dispose();

                    
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }

               
            }
            else
            {
                System.Diagnostics.Trace.TraceInformation("No image needed for TEST institution:100009. About to run transaction.");
                ImagetranType.Passport = null;           
            }

            ICustomerRegistration custReg = new CustomerRegistrationSystem().GetCustomerRegistrationByPhoneNumber(mob.InstitutionCode, customerPhone);
            if(custReg != null)
            {
                throw new Exception("This Phone Number already exists.");
            }
            custReg = new CustomerRegistrationSystem().GetCustomerRegistrationBySerialNumber(starterPack);
            if (custReg != null)
            {
                throw new Exception("This card has already been issued.");
            }
            tranType.Status = RegistrationStatus.Pending;
            ICustomerRegistrationImage imageResponse = null;
            try
            {
                imageResponse = new CustomerRegistrationImageSystem(currentWorkflowInfo.DataSource).SaveCustomerRegistrationImage(ImagetranType as CustomerRegistrationImage);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error trying to save service based customer registration details.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred when trying to save image. Please try again.");
            }
            try
            {
                tranType.PassportID = imageResponse.ID;
                var response = new CustomerRegistrationSystem(currentWorkflowInfo.DataSource).SaveCustomerRegistration(tranType as CustomerRegistration);
            }            
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error trying to save service based customer registration details.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
            }
            finally
            {
            }
            ActivationCode.Set(context, "");
            AccountNumber.Set(context, "");
        }

        DateTime ParseDate(string entry)
        {
            DateTime toReturn;
            string day = entry.Substring(0,2);
            string month = entry.Substring(2, 2);
            string year = entry.Substring(4);
            string dateValue = string.Format("{0}-{1}-{2}", month, day, year);
            DateTime.TryParse(dateValue, out toReturn);
            return toReturn;
            
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }

}
