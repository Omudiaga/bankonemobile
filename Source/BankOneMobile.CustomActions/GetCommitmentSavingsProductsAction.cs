﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetCommitmentSavingsProductsDesigner))]
    public class GetCommitmentSavingsProductsAction : ActionNode
    {



        /// <summary>
        /// List of Products
        /// </summary>
        public OutArgument<IList<Product>> Products
        {
            get
            {
                GetCommitmentSavingsProductsActivity activity = this.InternalState as GetCommitmentSavingsProductsActivity;
                if (activity != null)
                {
                    return activity.Products;
                }
                return null;
            }
            set
            {
                GetCommitmentSavingsProductsActivity activity = this.InternalState as GetCommitmentSavingsProductsActivity;
                if (activity != null)
                {
                    activity.Products = value;
                    activity.Products = value;
                }
            }
        }

        public GetCommitmentSavingsProductsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetCommitmentSavingsProductsActivity();
        }

    }

    public class GetCommitmentSavingsProductsActivity : CodeActivity
    {


        public OutArgument<IList<Product>> Products { get; set; }

        protected override void Execute(CodeActivityContext context)
        {


            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            IList<Product> products = new List<Product>();
            Services.SwitchServiceRef.Product[] productsFromCore = null;
            try
            {
                PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("about to get comm sav products. instcode:{0}", mob.InstitutionCode)));
                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    productsFromCore = client.GetCommitmentSavingsProducts(mob.InstitutionCode);
                }
                if (productsFromCore == null || productsFromCore.Count() == 0)
                {
                    throw new Exception("There are no commitment savings products to display");
                }
                foreach (var pro in productsFromCore)
                {
                    products.Add(new Product
                    {
                        ID = pro.ID,
                        Code = pro.Code,
                        Name = pro.Name,
                        Tenure = pro.Tenure
                    });
                }
                //products = productsFromCore.ToList();
            }
            catch (FaultException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), "Core Banking Unavailable");
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), ex.Message);
            }
            Products.Set(context, products);




        }
    }
}
