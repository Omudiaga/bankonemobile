﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankoneMobile.AdvansLafayette;
using System.Diagnostics;
using System.Xml;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(NonAccountToNonAccountTransferDesigner))]
    public class NonAccountToNonAccountTransferAction : ActionNode
    {

        public InArgument<bool> IsCustomerSending
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.IsCustomerSending;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.IsCustomerSending = value;
                }
            }
        }
        public InArgument<string> AccountNumberFrom
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.AccountNumberFrom;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.AccountNumberFrom = value;
                }
            }
        }

        public InArgument<string> PhoneNumberFrom
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.PhoneNumberFrom;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.PhoneNumberFrom = value;
                }
            }
        }
        public InArgument<string> PhoneNumberTo
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.PhoneNumberTo;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.PhoneNumberTo = value;
                }
            }
        }

        public InArgument<string> DestinationName
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.DestinationName;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.DestinationName = value;
                }
            }
        }
        public InArgument<string> Amount
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        public InArgument<string> Token
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.Token;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.Token = value;
                }
            }
        }
        public InArgument<string> PIN
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }

        public InArgument<DBNFundsTransferTransactionType> TransactionType
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.TransactionType;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.TransactionType = value;
                }
            }
        }    

        /// <summary>
        /// Transaction successful
        /// </summary>
        public OutArgument<string> TransactionSuccessful
        {
            get
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    return activity.TransactionSuccessful;
                }
                return null;
            }
            set
            {
                NonAccountToNonAccountTransferActivity activity = this.InternalState as NonAccountToNonAccountTransferActivity;
                if (activity != null)
                {
                    activity.TransactionSuccessful = value;
                }
            }
        }    
       
       
       

        public NonAccountToNonAccountTransferAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new NonAccountToNonAccountTransferActivity();
        }

    }

    public class NonAccountToNonAccountTransferActivity : CodeActivity
    {
        public InArgument<bool> IsCustomerSending { get; set; }
        public InArgument<string> PhoneNumberFrom { get; set; }
        public InArgument<string> AccountNumberFrom { get; set; }
        public InArgument<string> PhoneNumberTo { get; set; }
        public InArgument<string> DestinationName { get; set; }
        public InArgument<string> Amount { get; set; }
        public InArgument<string> Token { get; set; }
        public InArgument<string> PIN { get; set; }
        public InArgument<DBNFundsTransferTransactionType> TransactionType { get; set; }
        public OutArgument<string> TransactionSuccessful { get; set; }     

        
        
        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            bool isCustomerSending = IsCustomerSending.Get(context);
            string accountNumberFrom = AccountNumberFrom.Get(context);
            string phoneNumberFrom = PhoneNumberFrom.Get(context);
            string phoneNumberTo = PhoneNumberTo.Get(context);
            string destinationName = DestinationName.Get(context);
            string amount = Amount.Get(context);
            string pin = PIN.Get(context);
            string token = Token.Get(context);
            string realResponse = string.Empty;

            string instCode = string.Empty;
            Agent agent;
            agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (agent == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Non-Agent Initiated Trnsactions");
            }
            instCode = agent.MobileAccount.InstitutionCode;
            mob.InstitutionCode = instCode;
            //TransactionResponse response = new TransactionResponse();
            decimal amountToTransfer = decimal.Parse(amount);
            try
            {
                Trace.TraceInformation("Type is to Non Account Holder");
                if (phoneNumberFrom == phoneNumberTo)
                {
                    throw new Exception(string.Format("Phone number {0} cannot be both the originator and the beneficiary.", phoneNumberFrom));
                }
                var account = new BankOneMobile.Services.SwitchServiceRef.Account();
                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    account = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumberFrom);
                }
                if (account == null)
                {
                    throw new Exception("Could not retrieve account details");
                }
                IDBNFundsTransferTransactionType tranType = null;
                string transactionReference = "TNA" + DateTime.Now.ToString("ddMMyyhhmmss") + phoneNumberFrom;                
                //string narrationCr = String.Format("Trsf from {0} to {1} @ {2} ({3})", account.Name, phoneNumberTo, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                Trace.TraceInformation("About to call service");
                if (isCustomerSending)
                {
                    string narration = String.Format("Trsf from {0} to {1} @ {2} ({3})", account.Name, destinationName, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                    tranType = new DBNFundsTransferTransactionType();
                    tranType.DBNFundsTransferType = DBNFundsTransferTransactionTypeName.NonAccountHolder;
                    tranType.Amount = decimal.Parse(amount);
                    tranType.BeneficiaryAccountNumber = phoneNumberTo;
                    tranType.BeneficiaryAccountName = destinationName;
                    tranType.DestinationInstitutionCode = mob.InstitutionCode;
                    tranType.Narration = narration;
                    tranType.OriginatorAccountName = account.Name;
                    tranType.OriginatorAccountNumber = account.NUBAN;
                    tranType.PaymentReference = transactionReference;
                    tranType.RequestDate = DateTime.Now;
                    tranType.SessionID = token;
                    tranType.ReversalStatus = DBNReversalStatus.NotApplicable;
                    tranType.IsCustomerSending = true;
                }
                else
                {
                    tranType = TransactionType.Get(context);
                    if(tranType == null)
                    {
                        throw new Exception("Non-account transaction type is null");
                    }
                    string narration = String.Format("Trsf from {0} to {1} @ {2} ({3})", account.Name, tranType.BeneficiaryAccountName, agent.IsMobileTeller ? "Mobile Teller" : "Agent", agent.LastName + " , " + agent.OtherNames);
                    tranType.IsCustomerSending = false;
                    tranType.Narration = narration;
                }

                TransactionResponse trxResponse = new TransactionResponse();

                try
                {
                    trxResponse = new TransactionSystem().RunTransaction(pin, tranType, mob, phoneNumberFrom, phoneNumberTo, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
                    if (trxResponse.Status == TransactionStatus.Successful.ToString())
                    {
                        realResponse = "true";
                    }
                    else
                    {
                        throw new Exception(trxResponse.ResponseMessage);
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents);
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));

            }
            catch (InvalidAgentCodeException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.InvalidAgentCode);
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.SwitchUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                realResponse = Utilities.GetDescription(ErrorCodes.IssuerUnavailable);
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                realResponse = ex.Message;
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                realResponse = "System Error";
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {
                //TransactionSuccessful.Set(context, realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP...... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }



           //tring result = TransactionSystem.RunTransaction(pin);
            //context.SetValue(Notification,response.ResponseMessage);
            //string result = TransactionSystem.RunTransaction(pin);

            

        }
    }

}
