﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetSpecificSetOfCommercialBanksDesigner))]
    public class GetSpecificSetOfCommercialBanksAction : ActionNode
    {
      
       

        /// <summary>
        /// List of CommercialBanks
        /// </summary>
        public OutArgument<IList<CommercialBank>> CommercialBanks
        {
            get
            {
                GetSpecificSetOfCommercialBanksActivity activity = this.InternalState as GetSpecificSetOfCommercialBanksActivity;
                if (activity != null)
                {
                    return activity.CommercialBanks;
                }
                return null;
            }
            set
            {
                GetSpecificSetOfCommercialBanksActivity activity = this.InternalState as GetSpecificSetOfCommercialBanksActivity;
                if (activity != null)
                {
                    activity.CommercialBanks = value;
                }
            }
        }

        public InArgument<string> CommercialBankCodesToExclude
        {
            get
            {
                GetSpecificSetOfCommercialBanksActivity activity = this.InternalState as GetSpecificSetOfCommercialBanksActivity;
                if (activity != null)
                {
                    return activity.CommercialBankCodesToExclude;
                }
                return null;
            }
            set
            {
                GetSpecificSetOfCommercialBanksActivity activity = this.InternalState as GetSpecificSetOfCommercialBanksActivity;
                if (activity != null)
                {
                    activity.CommercialBankCodesToExclude = value;
                }
            }
        }

        public GetSpecificSetOfCommercialBanksAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetSpecificSetOfCommercialBanksActivity();
        }

    }

    public class GetSpecificSetOfCommercialBanksActivity : CodeActivity
    {


        public OutArgument<IList<CommercialBank>> CommercialBanks { get; set; }
        public InArgument<string> CommercialBankCodesToExclude { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string instCode = string.Empty;
            string commercialBankCodesToExclude = CommercialBankCodesToExclude.Get(context);
            Agent ag = null;
            if (!mob.IsAgentAccount)
            {
                instCode = mob.RecievingBankAccount.InstitutionCode;
            }

            try
            {
                ag = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
                instCode = ag.TheAgentAccount.InstitutionCode;
            }
            catch { }

            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("*******About to get specific set of commercial banks*******")));
            //var currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            //string instName = currentWorkflowInfo.InitiatedBy.BankAccountToDebit.InstitutionName;
            string instName = ag.TheAgentAccount.InstitutionName;
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Banks to exclude: {0}", commercialBankCodesToExclude)));
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Requesting Bank = {0}", instName)));
            IList<CommercialBank> commercialBanks = new CommercialBankSystem(currentWorkflowInfo.DataSource).GetActiveCommercialBanks().OrderBy(x => x.ShortName).Cast<CommercialBank>().ToList();
            string[] codes = commercialBankCodesToExclude.Split(',');
            commercialBanks = commercialBanks.Where(x => !codes.Contains(x.Code)).ToList();

            //commercialBanks.Where(x => x.Code == "000").FirstOrDefault().ShortName = instName;
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("Gotten Banks. count: {0}", commercialBanks.Count)));
            CommercialBanks.Set(context, commercialBanks);
        }
    }
}
