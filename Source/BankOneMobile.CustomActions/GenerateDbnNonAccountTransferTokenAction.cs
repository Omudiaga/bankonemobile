﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GenerateDbnNonAccountTransferTokenDesigner))]
    public class GenerateDbnNonAccountTransferTokenAction : ActionNode
    {
        public OutArgument<string> TokenValue
        {
            get
            {
                GenerateDbnNonAccountTransferTokenActivity activity = this.InternalState as GenerateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {

                    return activity.TokenValue;
                }
                return null;
            }
            set
            {
                GenerateDbnNonAccountTransferTokenActivity activity = this.InternalState as GenerateDbnNonAccountTransferTokenActivity;
                if (activity != null)
                {
                    activity.TokenValue = value;
                }
            }
        }
      
        public GenerateDbnNonAccountTransferTokenAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GenerateDbnNonAccountTransferTokenActivity();
        }

    }

    public class GenerateDbnNonAccountTransferTokenActivity : CodeActivity
    {
        public OutArgument<string> TokenValue { get; set; }
       
        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            
            string theToken = string.Empty;
            Random rand = new Random();
            lock (this)
            {
                theToken = rand.Next(10000, 99999).ToString();
            }
            TokenValue.Set(context, theToken);
        }
    }
}
