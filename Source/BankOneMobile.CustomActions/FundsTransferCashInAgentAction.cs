﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Services.Utility;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Data.Contracts;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(FundsTransferCashInAgentDesigner))]
    public class FundsTransferCashInAgentAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public OutArgument<string> FundsTransferCode
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.FundsTransferCode;
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.FundsTransferCode = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number 
        /// </summary>
        public InArgument<string> AccountNumber
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }
        /// <summary>
        ///Transfer Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.Amount;
                        
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        ///Pay by debit or cash out
        /// </summary>
        public InArgument<Boolean> IsCash
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.IsCash;

                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.IsCash = value;
                }
            }
        }

        /// <summary>
        ///Beneficiary Phone Number
        /// </summary>
        public InArgument<string> BeneficiaryPhoneNumber
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.BeneficiaryPhoneNumber;
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.BeneficiaryPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Beneficiary Phone Number
        /// </summary>
        public InArgument<string> AgentCode
        {
            get
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    return activity.AgentCode;
                }
                return null;
            }
            set
            {
                FundsTransferCashInAgentActivity activity = this.InternalState as FundsTransferCashInAgentActivity;
                if (activity != null)
                {
                    activity.AgentCode = value;
                }
            }
        }
       
       
       

        public FundsTransferCashInAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new FundsTransferCashInAgentActivity();
        }

    }

    public class FundsTransferCashInAgentActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Agent Code entered by the customer
        /// </summary>
        public InArgument<string> AgentCode { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }
        /// <summary>
        /// Beneficiairy Phone Number
        /// </summary>
        public InArgument<string> BeneficiaryPhoneNumber { get; set; }
        /// <summary>
        /// Funds Transfer Code
        /// </summary>
        public OutArgument<string> FundsTransferCode { get; set; }
             /// <summary>
        /// Checks if Cash or debit was selected
        /// </summary>
        public InArgument<Boolean > IsCash { get; set; }
       
       

        

        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
            //try
            //{
            //    pin = HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
            //}
            //catch (NoHSMResponseException)
            //{
            //    throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            //}
            //catch
            //{
            //    throw new FailureException(Utilities.GetCode(ErrorCodes.HSMError), Utilities.GetDescription(ErrorCodes.HSMError));
            //}
            
            string customerPhone = AccountNumber.Get(context);



            string instCode = string.Empty;
            if (customerPhone == null)
            {
                customerPhone = mob.BankAccounts[0].BankAccount;
                instCode = mob.BankAccounts[0].InstitutionCode;
                mob.InstitutionCode = instCode;

            }
            else
            {
                List<String> vals = customerPhone.Split(':').ToList();
                customerPhone = vals[0];
                instCode = new InstitutionSystem().GetByShortName(currentWorkflowInfo.DataSource, vals[1]).Code;
                mob.InstitutionCode = instCode;
            }


            string beneficiaryPhoneNumber = BeneficiaryPhoneNumber.Get(context);
            string amount = Amount.Get(context);
            //string fundsTransferCode = FundsTransferCode.Get(context);
            string agentCode = AgentCode.Get(context);
            
            bool cashDebit = IsCash.Get(context);

            MobileAccount depositorAcct = mob;// new MobileAccountSystem(DataSourceFactory.GetDataSource(DataCategory.Shared)).GetByPhoneNumber(customerPhone);
           // customerPhone = customerPhone == null ? depositorAcct.BankAccounts[0].BankAccount : customerPhone;


            if (depositorAcct == null)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),"Depositor is not registered on Bank One");
                
            }
            if (depositorAcct.MobileAccountStatus!=MobileAccountStatus.Active)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.CustomerIsInactive), Utilities.GetDescription(ErrorCodes.CustomerIsInactive));
                
            }

            IFundsTransferCashInTransactionType tranType = new FundsTransferCashInTransactionType();
            tranType.Amount = Convert.ToDecimal(amount);
            tranType.DepositorPhoneNumber = customerPhone;
            tranType.AgentCode = agentCode;
            tranType.ClearToken = new FundsTransferCashInTransactionTypeSystem().GenerateToken();
                tranType.Token= new MD5Password().CreateSecurePassword(tranType.ClearToken);
            tranType.IsCash = true;// cash is always gotten from the customer to the agent
            tranType.DepositorPhoneNumber = mob.MobilePhone;
            tranType.DepositorName = string.Format("{0} {1}", mob.LastName, mob.OtherNames);
            tranType.TheFee = new FeeTransactionType { Amount = 100 };
            tranType.BeneficiaryPhoneNumber = beneficiaryPhoneNumber;
            try
            {
              TransactionResponse  response = new TransactionSystem().RunTransaction(pin, tranType, mob,customerPhone,beneficiaryPhoneNumber, Convert.ToDecimal(amount) * 100, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
              FundsTransferCode.Set(context, tranType.ClearToken);
            }

            catch (InvalidAgentCodeException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));

            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }

            finally
            {
               
                

                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP.... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();
            }

        }
    }

}
