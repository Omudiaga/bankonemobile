﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;
using System.Globalization;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CheckForExistingLoansDesigner))]
    public class CheckForExistingLoansAction : ActionNode
    {
        /// <summary>
        /// Phone Number
        /// </summary>
        public InArgument<string> PhoneNumber
        {
            get
            {
                CheckForExistingLoanActivity activity = this.InternalState as CheckForExistingLoanActivity;
                if (activity != null)
                {

                    return activity.PhoneNumber;
                }
                return null;
            }
            set
            {
                CheckForExistingLoanActivity activity = this.InternalState as CheckForExistingLoanActivity;
                if (activity != null)
                {
                    activity.PhoneNumber = value;
                }
            }
        }

        public CheckForExistingLoansAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new CheckForExistingLoanActivity();
        }

    }

    public class CheckForExistingLoanActivity : CodeActivity
    {
        public InArgument<string> PhoneNumber { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            var agentMob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = PhoneNumber.Get(context);
            //MobileAccount mob = new MobileAccountSystem().GetByPhoneNumber(phoneNumber);// currentWorkflowInfo.InitiatedBy as MobileAccount;
            MobileAccount mob = new MobileAccountSystem().GetMobileAccountByPhoneNumber(agentMob.InstitutionCode, phoneNumber) as MobileAccount;

            if(mob == null)
            {
                throw new Exception("Phone number is not registered");
            }
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(string.Format("ABOUT TO CHECK WHETHER CUSTOMER HAS AN EXISTING LOAN. PHONENUMBER:{0}", phoneNumber)));
            string message = string.Empty;
            BankOneMobile.Services.ServiceReference1.Account[] loanAccounts = null;
            using (var client = new BankOneMobile.Services.ServiceReference1.CBAServiceClient())
            {
                loanAccounts = client.GetActiveLoanAccountByPhoneNo(out message, mob.InstitutionCode, phoneNumber);
            }
            if (loanAccounts != null && loanAccounts.Length > 0)
            {
                new PANE.ERRORLOG.Error().LogToFile(new Exception(message));
                throw new Exception("This customer already has an active loan");
            }

        }
    }
}
