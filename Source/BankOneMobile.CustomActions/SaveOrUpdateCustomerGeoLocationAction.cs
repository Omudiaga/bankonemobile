﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    /// <summary>
    /// This custom action is for Diamond Bank Target Savings
    /// </summary>
    [Designer(typeof(SaveOrUpdateCustomerGeoLocationDesigner))]
    public class SaveOrUpdateCustomerGeoLocationAction : ActionNode
    {
        /// <summary>
        /// CustomerPhoneNumber
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                SaveOrUpdateCustomerGeoLocationActivity activity = this.InternalState as SaveOrUpdateCustomerGeoLocationActivity;
                if (activity != null)
                {

                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                SaveOrUpdateCustomerGeoLocationActivity activity = this.InternalState as SaveOrUpdateCustomerGeoLocationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
       


        public SaveOrUpdateCustomerGeoLocationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SaveOrUpdateCustomerGeoLocationActivity();
        }

    }

    public class SaveOrUpdateCustomerGeoLocationActivity : CodeActivity
    {
        public InArgument<string> CustomerPhoneNumber { get; set; }
        

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string geoLocation = currentWorkflowInfo.GeoLocation;
            string agentPhoneNumber = mob.MobilePhone;
            string customerPhoneNumber = CustomerPhoneNumber.Get(context);

            string message = string.Format("Customer Default Location Parameters - agentPhone:{0},customerPhoneNumber:{1},geoLocation:{2}", agentPhoneNumber, customerPhoneNumber, geoLocation);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
            try
            {
                if (!string.IsNullOrWhiteSpace(geoLocation))
                {
                    ICustomerGeoLocation customerGeoLocation = null;
                    customerGeoLocation = new CustomerGeoLocationSystem().GetByCustomerPhoneNumber(currentWorkflowInfo.DataSource, mob.InstitutionCode, customerPhoneNumber);
                    if (customerGeoLocation == null)
                    {
                        customerGeoLocation = new CustomerGeoLocation
                        {
                            InstitutionCode = mob.InstitutionCode,
                            AgentPhoneNumber = agentPhoneNumber,
                            CustomerPhoneNumber = customerPhoneNumber,
                            Latitude = geoLocation.Split(';')[0],
                            Longitude = geoLocation.Split(';')[1],
                            DateLogged = DateTime.Now
                        };
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to save customer geo location"));
                        new CustomerGeoLocationSystem().Save(currentWorkflowInfo.DataSource, customerGeoLocation as CustomerGeoLocation);

                    }
                    else
                    {
                        PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to update customer geo location"));
                        customerGeoLocation.DateLogged = DateTime.Now;
                        customerGeoLocation.Latitude = geoLocation.Split(';')[0];
                        customerGeoLocation.Longitude = geoLocation.Split(';')[1];
                        new CustomerGeoLocationSystem().Update(currentWorkflowInfo.DataSource, customerGeoLocation as CustomerGeoLocation);
                    }
                }
            }
            catch (Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
            }
        }
    }
}
