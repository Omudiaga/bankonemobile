﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(CustomerRegistrationDesigner))]
    public class EnhancedCustomerRegistrationAction : ActionNode
    {
        /// <summary>
        /// Product Code
        /// </summary>
        public InArgument<string> ProductCode
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ProductCode;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ProductCode = value;
                }
            }
        }
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
       
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerName
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerName;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerName = value;
                }
            }
        }
        /// <summary>
        ///Customer Name
        /// </summary>
        public InArgument<string> CustomerFirstName
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerFirstName;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerFirstName = value;
                }
            }
        }
        /// <summary>
        ///Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        /// <summary>
        ///Customer Gender
        /// </summary>
        public InArgument<string> Gender
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Gender;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Gender = value;
                }
            }
        }
        /// <summary>
        ///Product for the Account
        /// </summary>
        public InArgument<Product> Product
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }

        public InArgument<string> PlaceOfBirth
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.PlaceOfBirth;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.PlaceOfBirth = value;
                }
            }
        }
        public InArgument<string> NOKName
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.NOKName;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.NOKName = value;
                }
            }
        }
        public InArgument<string> NOKPhone
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.NOKPhone;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.NOKPhone = value;
                }
            }
        }
        public InArgument<string> Address
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.Address;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.Address = value;
                }
            }
        }
        public InArgument<string> DateOfBirth
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.DateOfBirth;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.DateOfBirth = value;
                }
            }
        }
        public InArgument<string> StarterPackNumber
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.StarterPackNumber;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.StarterPackNumber = value;
                }
            }
        }
        public InArgument<string> IDNumber
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.IDNumber;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.IDNumber = value;
                }
            }
        }
        public InArgument<string> ReferralNumber
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ReferralPhoneNumber;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ReferralPhoneNumber = value;
                }
            }
        }
        public InArgument<string> OtherAccountnInfo
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.OtherAccountInfo;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.OtherAccountInfo = value;
                }
            }
        }
        public InArgument<string> AccountSource
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountSource;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountSource = value;
                }
            }
        }
        public InArgument<bool> HasSufficientInfo
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.HasSufficientInfo;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.HasSufficientInfo = value;
                }
            }
        }

        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.ActivationCode;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.ActivationCode = value;
                }
            }


             
        }
        public OutArgument<string> AccountNumber
        {
            get
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                EnhancedCustomerRegistrationActivity activity = this.InternalState as EnhancedCustomerRegistrationActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }



        }

         
        public EnhancedCustomerRegistrationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new EnhancedCustomerRegistrationActivity();
        }

    }

    public class EnhancedCustomerRegistrationActivity : CodeActivity
    {
        private const string CONFIRMATION_TEXT = "The Customer has been successfully registered on Bank One.Please notify them of this Activation Code:";
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        /// <summary>
        /// Customer Full Name
        /// </summary>
        public InArgument<string> CustomerName { get; set; }
        
        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Customer First Name
        /// </summary>
        public InArgument<string> CustomerFirstName { get; set; }

        /// <summary>
        /// Customer Gender
        /// </summary>
        public InArgument<string> Gender { get; set; }
        /// <summary>
        /// Product for the Account
        /// </summary>
        public InArgument<Product> Product { get; set; } 
        /// <summary>
        /// Activation Code
        /// </summary>
        public OutArgument<string> ActivationCode { get; set; }
        /// <summary>
        /// Product Code
        /// </summary>

        public InArgument<string> ProductCode { get; set; }

        public InArgument<string> PlaceOfBirth { get; set; }
        public InArgument<string> DateOfBirth { get; set; }
        public InArgument<string> IDNumber { get; set; }
        public InArgument<string> NOKName { get; set; }
        public InArgument<string> NOKPhone { get; set; }
        public InArgument<string> Address { get; set; }
        public InArgument<string> ReferralPhoneNumber { get; set; }
        public InArgument<string> OtherAccountInfo { get; set; }
        public InArgument<bool> HasSufficientInfo { get; set; }
        public InArgument<string> AccountSource { get; set; }

        public InArgument<string> StarterPackNumber { get; set; }
        public OutArgument<string> AccountNumber { get; set; }
        protected override void Execute(CodeActivityContext context)
        {
             
            string pin = PIN.Get(context);
            string customerName = CustomerName.Get(context);
            string customerFirstName = CustomerFirstName.Get(context);
            string customerPhone = CustomerPhoneNumber.Get(context);
            string gender = Gender.Get(context);
            Product prod = Product.Get(context);

            string productCode = prod.Code;
           string starterPack = StarterPackNumber.Get(context);
           string address = Address.Get(context);
           
           string nokPhone = NOKPhone.Get(context);
           string nokName = NOKName.Get(context);
           string idNumber = IDNumber.Get(context);
            string refNumber = ReferralPhoneNumber.Get(context);
            string placeOfBirth = PlaceOfBirth.Get(context);
            

            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string dateOfBirth = DateOfBirth.Get(context);
            string accountSource = AccountSource.Get(context);
            string otherAccountInfo = OtherAccountInfo.Get(context);
            bool hasInfo = HasSufficientInfo.Get(context);

            IRegistrationTransactionType tranType = new RegistrationTransactionType();
            tranType.LastName = customerName;
            tranType.PhoneNumber = customerPhone;
            tranType.Gender = gender;
            tranType.TheGender=(Gender) Enum.Parse(typeof(Gender),gender);
            tranType.ProductCode = productCode;
            tranType.TheProduct = prod;
            tranType.FirstName = customerFirstName;
            tranType.IsEnhanced = true;
            tranType.IDNumber = idNumber;
            tranType.NOKName = nokName;
            tranType.NOKPhone = nokPhone;
            tranType.Address = address;
            tranType.StarterPackNumber = starterPack;
            if(!string.IsNullOrEmpty(accountSource))
            {
                tranType.AccountSource = (AccountSource)Enum.Parse(typeof(AccountSource), accountSource);
            }
            
            tranType.OtherAccountInfo = otherAccountInfo;
            tranType.HasSufficientInfo = hasInfo;


            DateTime dob = ParseDate(dateOfBirth);

            if(dob ==null)
                throw new FailureException("06", "Invalid Date Entry");
            
            // DateTime.TryParse(dateOfBirth, out dob);
             tranType.DateOfBirth = dob;
            tranType.ReferralPhoneNumber = ReferralPhoneNumber.Get(context);
            tranType.PlaceOfBirth = placeOfBirth;
            if (mob.InstitutionCode != "100009")
            {
                //Get passport image
                System.Diagnostics.Trace.TraceInformation("About to get image from server location.");
                string path = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("ImageUploadPath") ?
                    System.Configuration.ConfigurationManager.AppSettings["ImageUploadPath"] : "C:\\";
                string filename = string.Format(@"{0}{1}.png", path, sessionID.Replace("+", "").Trim());
                Image image = null;
                try
                {
                    //image = Image.FromFile(string.Format("{0}{1}.png", path, sessionID.Replace("+", "")));
                    image = Image.FromFile(filename);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError(ex.Message);
                    System.Diagnostics.Trace.TraceError(ex.StackTrace);
                    System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                    throw;
                }
                var ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                System.Diagnostics.Trace.TraceInformation("Gotten image from server location. about to tun transaction.");
                tranType.Passport = ms.ToArray();
            }
            else
            {
                System.Diagnostics.Trace.TraceInformation("No image needed for TEST institution:100009. About to run transaction.");
                tranType.Passport = null;            
            }
            
            TransactionResponse response = null;
            try
            {
                response = new TransactionSystem().RunTransaction(pin, tranType, mob, mob.MobilePhone, mob.MobilePhone, 0.0M, sessionID);// use 0 cos we dont really care about amount in balance enquiry
            }
            catch (TransactionNotPermittedToNonAgentsException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (AlreadyRegisterdCustomerException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberAlreadyRegistered), Utilities.GetDescription(ErrorCodes.PhoneNumberAlreadyRegistered));
            }
            catch (AlreadyExistingAccountException ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.AccountNumberAlreadyExists), Utilities.GetDescription(ErrorCodes.AccountNumberAlreadyExists));
            }
            catch (CoreBankingWebServiceException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), Utilities.GetDescription(ErrorCodes.UnableToConnectToCoreBanking));
            }
            catch (NoHSMResponseException ex)
            {
                
                throw new FailureException(Utilities.GetCode(ErrorCodes.HSMUnavailable), Utilities.GetDescription(ErrorCodes.HSMUnavailable));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Caught an error during transaction.");
                System.Diagnostics.Trace.TraceError(ex.Message);
                System.Diagnostics.Trace.TraceError(ex.StackTrace);
                System.Diagnostics.Trace.TraceError(ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                //throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
                //Do not display technical errors to external systems.
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), "An error occurred. Please try again.");
            }
            finally
            {


                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

                //if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                //{
                //   // new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally);

                //}
                //if (File.Exists(filename))
                //{
                //    System.Diagnostics.Trace.TraceInformation("Deleting {0} image...", filename);
                //    File.Delete(filename);
                //}

            }
            ActivationCode.Set(context,  response.ResponseMessage);
            AccountNumber.Set(context, response.ResponseDescription);
            

        }

        DateTime ParseDate(string entry)
        {
            DateTime toReturn;
            string day = entry.Substring(0,2);
            string month = entry.Substring(2, 2);
            string year = entry.Substring(4);
            string dateValue = string.Format("{0}-{1}-{2}", month, day, year);
            DateTime.TryParse(dateValue, out toReturn);
            return toReturn;
            
        }
    }

}
