﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(ValidateAccountNumberDesigner))]
    public class ValidateAccountNumberAction : ActionNode
    {
        public InArgument<string> AccountNumber
        {
            get
            {
                ValidateAccountNumberActivity activity = this.InternalState as ValidateAccountNumberActivity;
                if (activity != null)
                {

                    return activity.AccountNumber;
                }
                return null;
            }
            set
            {
                ValidateAccountNumberActivity activity = this.InternalState as ValidateAccountNumberActivity;
                if (activity != null)
                {
                    activity.AccountNumber = value;
                }
            }
        }

        /// <summary>
        /// Account Name on User Mobile Account
        /// </summary>
        public OutArgument<BankOneMobile.Services.SwitchServiceRef.Account> Account
        {
            get
            {
                ValidateAccountNumberActivity activity = this.InternalState as ValidateAccountNumberActivity;
                if (activity != null)
                {
                    return activity.Account;
                }
                return null;
            }
            set
            {
                ValidateAccountNumberActivity activity = this.InternalState as ValidateAccountNumberActivity;
                if (activity != null)
                {
                    activity.Account = value;
                }
            }
        }


        public ValidateAccountNumberAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ValidateAccountNumberActivity();
        }

    }

    public class ValidateAccountNumberActivity : CodeActivity
    {
        /// <summary>
        /// Account Number
        /// </summary>
        public InArgument<string> AccountNumber { get; set; }

        /// <summary>
        /// Account Name
        /// </summary>
        /// 
        public OutArgument<BankOneMobile.Services.SwitchServiceRef.Account> Account { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string accountNumber = AccountNumber.Get(context);

            var account = new BankOneMobile.Services.SwitchServiceRef.Account();
            try
            {
                using (var client = new Services.SwitchServiceRef.SwitchingServiceClient())
                {
                    account = client.GetAccountByAccountNo(mob.InstitutionCode, accountNumber);
                }
                if(account == null)
                {
                    throw new Exception("Account Number Does Not Exist");
                }
                if(account.Status == Services.SwitchServiceRef.AccountStatus.Closed)
                {
                    throw new Exception("This Account Has Been Closed");
                }
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            Account.Set(context, account);//set account
        }
    }
}
