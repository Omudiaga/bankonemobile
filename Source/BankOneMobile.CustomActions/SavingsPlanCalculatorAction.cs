﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
namespace BankOneMobile.CustomActions
{
    [Designer(typeof(SavingsPlanCalculatorDesigner))]
    public class SavingsPlanCalculatorAction : ActionNode
    {

        /// <summary>
        ///Amount
        /// </summary>
        public InArgument<string> Amount
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.Amount;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.Amount = value;
                }
            }
        }
        /// <summary>
        ///Frequency
        /// </summary>
        public InArgument<string> Frequency
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.Frequency;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.Frequency = value;
                }
            }
        }

        public InArgument<Product> Product
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.Product;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.Product = value;
                }
            }
        }

        public InArgument<string> CalculatorType
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.CalculatorType;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.CalculatorType = value;
                }
            }
        }

        public OutArgument<string> TotalCommitmentAmount
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.TotalCommitmentAmount;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.TotalCommitmentAmount = value;
                }
            }
        }

        public OutArgument<string> TotalInterest
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.TotalInterest;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.TotalInterest = value;
                }
            }
        }

        public OutArgument<string> Total
        {
            get
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    return activity.Total;
                }
                return null;
            }
            set
            {
                SavingsPlanCalculatorActivity activity = this.InternalState as SavingsPlanCalculatorActivity;
                if (activity != null)
                {
                    activity.Total = value;
                }
            }
        }


        public SavingsPlanCalculatorAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SavingsPlanCalculatorActivity();
        }

    }

    public class SavingsPlanCalculatorActivity : CodeActivity
    {
        /// <summary>
        /// Amount
        /// </summary>
        public InArgument<string> Amount { get; set; }

        /// <summary>
        /// Frequency
        /// </summary>
        public InArgument<string> Frequency { get; set; }
        public InArgument<Product> Product { get; set; }
        public InArgument<string> CalculatorType { get; set; }
        public OutArgument<string> TotalCommitmentAmount { get; set; }
        public OutArgument<string> TotalInterest { get; set; }
        public OutArgument<string> Total { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string frequency = Frequency.Get(context);
            string amount = Amount.Get(context);
            Product product = Product.Get(context);
            string calculatorType = CalculatorType.Get(context);
            //decimal totalCommitmentAmount = 0;
            decimal interest = 0;


            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            if (long.Parse(amount) <= 0)
            {
                throw new Exception("Amount cannot be less than or equal to 0");
            }

            BankOneMobile.Services.SwitchServiceRef.ProductInterest[] interestBounds = null;
            using (var client = new BankOneMobile.Services.SwitchServiceRef.SwitchingServiceClient())
            {
                interestBounds = client.GetCommitmentSavingsProductInterest(mob.InstitutionCode, product.ID);
            }

            if (interestBounds == null || interestBounds.Length == 0)
            {
                throw new Exception("Could Not Retrieve Product Interest");
            }
            string headerSection = string.Format(@"<table align='center' style='border-top:1px solid gray; border-bottom:1px solid gray;margin-bottom:5px;'>
                                                           <tr>
                                                                <td align='center'><b>Target Savings Plan Calculator</b></td>
                                                           </tr>
                                                       </table>
                                                       <table align='center' border='0' style='margin-bottom:5px;'>
                                                            <tr>
                                                                <td align='center'><b>Result</b></td>
                                                            </tr>
                                                       </table>");
            StringBuilder sb = new StringBuilder();
            if (calculatorType == "ByTargetAmount")
            {
                decimal amt = decimal.Parse(amount);
                decimal totalDailyCommitmentAmount = amt / product.Tenure;
                decimal totalWeeklyCommitmentAmount = amt / (product.Tenure / 5);
                decimal totalMonthlyCommitmentAmount = amt / (product.Tenure / 30);

                string result = string.Format(@"<table align='center' style='font-size:{5}px;width:95%;border-collapse:collapse;border:3px solid gray;margin-top:5px;'>
                                                <tr>
                                                    <td style='border:2px solid gray;' align='left'>Target Amount:</td>
                                                    <td style='border:2px solid gray;'><b style='color:red'>{0}</b></td>
                                                </tr>
                                                <tr>
                                                    <td style='border:2px solid gray;' align='left'>Tenure:</td>
                                                    <td style='border:2px solid gray;'><b>{1} days</b></td>
                                                </tr>
                                                <tr>
                                                    <td style='border:2px solid gray;' align='left'>Daily Contribution:</td>
                                                    <td style='border:2px solid gray;'><b>{2}</b></td>
                                                </tr>
                                                <tr>
                                                    <td style='border:2px solid gray;' align='left'>Weekly Contribution:</td>
                                                    <td style='border:2px solid gray;'><b>{3}</b></td>
                                                </tr>
                                                <tr>
                                                    <td style='border:2px solid gray;' align='left'>Monthly Contribution:</td>
                                                    <td style='border:2px solid gray;'><b>{4}</b></td>
                                                </tr>
                                            </table>", amt.ToString("#,##0.##"), product.Tenure, totalDailyCommitmentAmount.ToString("#,##0.##"),
                                                     totalWeeklyCommitmentAmount.ToString("#,##0.##"), totalMonthlyCommitmentAmount.ToString("#,##0.##"),
                                                     System.Configuration.ConfigurationManager.AppSettings["SavingsPlanCalculatorFontSize"]);
                sb.Append(headerSection + result);

                TotalCommitmentAmount.Set(context, sb.ToString());
            }
            else if (calculatorType == "ByContribution")
            {
                decimal amt = decimal.Parse(amount);
                decimal totalDailyCommitmentAmount = 0, totalWeeklyCommitmentAmount = 0, totalMonthlyCommitmentAmount = 0;
                //Daily Contribution
                for (int i = 0; i < product.Tenure; i++)
                {
                    totalDailyCommitmentAmount += amt;
                    //Reorder so we can start from the lowest amount
                    foreach (var bound in interestBounds.OrderBy(x => x.MinimumBalance).ToList())
                    {
                        //if (currentAcctBalance >= bound.Amount) continue;
                        //Check if the customers balance falls between the minimum and maximum balance
                        if (totalDailyCommitmentAmount >= (bound.MinimumBalance) && totalDailyCommitmentAmount <= (bound.MaximumBalance))
                        {
                            interest += (totalDailyCommitmentAmount * (bound.Rate / Convert.ToDecimal(100 * bound.DaysInAYear)));
                        }
                    }
                }
                //Weekly Contribution
                for (int i = 0; i < product.Tenure / 5; i++)
                {
                    totalWeeklyCommitmentAmount += amt;
                    //Reorder so we can start from the lowest amount
                    foreach (var bound in interestBounds.OrderBy(x => x.MinimumBalance).ToList())
                    {
                        //if (currentAcctBalance >= bound.Amount) continue;
                        //Check if the customers balance falls between the minimum and maximum balance
                        if (totalWeeklyCommitmentAmount >= (bound.MinimumBalance) && totalWeeklyCommitmentAmount <= (bound.MaximumBalance))
                        {
                            interest += (totalWeeklyCommitmentAmount * (bound.Rate / Convert.ToDecimal(100 * bound.DaysInAYear)));
                        }
                    }
                }
                //Monthy Contribution
                for (int i = 0; i < product.Tenure / 30; i++)
                {
                    totalMonthlyCommitmentAmount += amt;
                    //Reorder so we can start from the lowest amount
                    foreach (var bound in interestBounds.OrderBy(x => x.MinimumBalance).ToList())
                    {
                        //if (currentAcctBalance >= bound.Amount) continue;
                        //Check if the customers balance falls between the minimum and maximum balance
                        if (totalMonthlyCommitmentAmount >= (bound.MinimumBalance) && totalMonthlyCommitmentAmount <= (bound.MaximumBalance))
                        {
                            interest += (totalMonthlyCommitmentAmount * (bound.Rate / Convert.ToDecimal(100 * bound.DaysInAYear)));
                        }
                    }
                }

                string result = string.Format(@"<table align='center' style='font-size:{5}px;width:98%;border-collapse:collapse;border:0px;margin-top:5px;'>
                                                <tr>
                                                    <td colspan='2'>
                                                        <table align='center' style='font-size:{5}px;width:100%;border-collapse:collapse;border:3px solid gray;margin-top:3px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;text-align:center' colspan='2'>=N={0} <b style='color:red'>Daily</b> Contribution</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Tenure:</td>
                                                                <td style='border:2px solid gray;'><b>{1} days</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Total Target Amount:</td>
                                                                <td style='border:2px solid gray;'><b style='color:red'>{2}</b></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'>
                                                        <table align='center' style='font-size:{5}px;width:100%;border-collapse:collapse;border:3px solid gray;margin-top:3px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;text-align:center' colspan='2'>=N={0} <b style='color:red'>Weekly</b> Contribution</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Tenure:</td>
                                                                <td style='border:2px solid gray;'><b>{1} days</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Total Target Amount:</td>
                                                                <td style='border:2px solid gray;'><b style='color:red'>{3}</b></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'>
                                                        <table align='center' style='font-size:{5}px;width:100%;border-collapse:collapse;border:3px solid gray;margin-top:3px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;text-align:center' colspan='2'>=N={0} <b style='color:red'>Monthly</b> Contribution</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Tenure:</td>
                                                                <td style='border:2px solid gray;'><b>{1} days</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;' align='left'>Total Target Amount:</td>
                                                                <td style='border:2px solid gray;'><b style='color:red'>{4}</b></td>
                                                            </tr>
                                                        </table>     
                                                    </td>
                                                </tr>
                                            </table>
                                            ", amt.ToString("#,##0.##"), product.Tenure, totalDailyCommitmentAmount.ToString("#,##0.##"),
                                                     totalWeeklyCommitmentAmount.ToString("#,##0.##"), totalMonthlyCommitmentAmount.ToString("#,##0.##"),
                                                     System.Configuration.ConfigurationManager.AppSettings["SavingsPlanCalculatorFontSize"]);
                sb.Append(headerSection + result);

                TotalCommitmentAmount.Set(context, sb.ToString());
                //TotalInterest.Set(context, Math.Round(interest, 2).ToString("#,##0.##"));
                //Total.Set(context, (totalCommitmentAmount + Math.Round(interest, 2)).ToString("#,##0.##"));
            }   
        }
    }

}
