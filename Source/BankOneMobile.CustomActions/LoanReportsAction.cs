﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.Activities;
using System.ComponentModel;
using BankOneMobile.Services;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankoneMobile.AdvansLafayette;
using System.Diagnostics;
using System.Xml;
using System.Globalization;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(LoanReportsDesigner))]
    public class LoanReportsAction : ActionNode
    {

        
        public InArgument<bool> IsForDefaultingLoans
        {
            get
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    return activity.IsForDefaultingLoans;
                }
                return null;
            }
            set
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    activity.IsForDefaultingLoans = value;
                }
            }
        }

        public InArgument<bool> IsForLoansDueInLessThanOrEqualToFiveDays
        {
            get
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    return activity.IsForLoansDueInLessThanOrEqualToFiveDays;
                }
                return null;
            }
            set
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    activity.IsForLoansDueInLessThanOrEqualToFiveDays = value;
                }
            }
        }

        public InArgument<bool> IsForActiveLoans
        {
            get
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    return activity.IsForActiveLoans;
                }
                return null;
            }
            set
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    activity.IsForActiveLoans = value;
                }
            }
        }

        public OutArgument<string> Result
        {
            get
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    return activity.Result;
                }
                return null;
            }
            set
            {
                LoanReportsActivity activity = this.InternalState as LoanReportsActivity;
                if (activity != null)
                {
                    activity.Result = value;
                }
            }
        }
       

        public LoanReportsAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new LoanReportsActivity();
        }

    }

    public class LoanReportsActivity : CodeActivity
    {
        public InArgument<bool> IsForDefaultingLoans { get; set; }
        public InArgument<bool> IsForLoansDueInLessThanOrEqualToFiveDays { get; set; }
        public InArgument<bool> IsForActiveLoans { get; set; }
        public OutArgument<string> Result { get; set; }
        
        
        protected override void Execute(CodeActivityContext context)
        {
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string agentPhoneNumber = mob.MobilePhone;
            bool isForDefaultingLoans = IsForDefaultingLoans.Get(context);
            bool isForLoansDueInLessThanOrEqualToFiveDays = IsForLoansDueInLessThanOrEqualToFiveDays.Get(context);
            bool isForActiveLoans = IsForActiveLoans.Get(context);

            string instCode = string.Empty;
            Agent agent;
            agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (agent == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Non-Agent Initiated Trnsactions");
            }
            instCode = agent.MobileAccount.InstitutionCode;

            DateTime currentFinancialDate;
            try
            {
                using (var client = new BankOneMobile.Services.Misc2ServiceRef.Misc2ServiceClient())
                {
                    currentFinancialDate = client.GetCurrentFinancialDate(instCode);
                }
            }
            catch(Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                throw new Exception("Could not retrieve current financial Date");
            }
            if(currentFinancialDate == default(DateTime))
            {
                throw new Exception("Could not retrieve current financial Date");
            }
            BankOneMobile.Services.CBAServiceRef.Account[] accounts;
            StringBuilder sb = new StringBuilder();
            string defaultMsg = string.Empty;
            if (isForActiveLoans)
                defaultMsg = "Active Loans";
            else if (isForDefaultingLoans)
                defaultMsg = "Defaulting Customers";
            else if (isForLoansDueInLessThanOrEqualToFiveDays)
                defaultMsg = "Loans Due In 5 days";

            string defaultMessage = string.Format(@"<table>
                                                <tr>
                                                    <td><b>{0}</b></td>
                                                </tr>
                                                <tr>
                                                    <td>No data to display</td>
                                                </tr>
                                            </table>", defaultMsg);
            try
            {
                using(var client = new BankOneMobile.Services.CBAServiceRef.CBAServiceClient())
                {
                    accounts = client.GetLoanAccountsByAccountOfficerCode(instCode, agent.AccountNumber);
                }
                if (accounts != null && accounts.Length > 0)
                {
                    #region Defaulting Loans
                    if (isForDefaultingLoans)
                    {
                        accounts = accounts.Where(x => x.Balance.AvailableBalance > 0 && x.DateCreatedFinancial.Value.AddDays(x.LoanCycle) < currentFinancialDate).ToArray();
                        if(accounts != null && accounts.Length > 0)
                        {
                            sb.Append(@"<table align='center' style='font-size:13px;border-top:1px solid gray; border-bottom:1px solid gray;margin-bottom:3px;'>
                                            <tr>
                                                <td colspan=2>Defaulting Customers</td>
                                            </tr>
                                        </table>
                                        ");
                            accounts = accounts.OrderByDescending(x => x.Balance.AvailableBalance).ToArray();
                            foreach(var account in accounts)
                            {
                                sb.Append(string.Format(@"
                                                        <table align='center' style='font-size:{3}px;width:98%;border-collapse:collapse;border:3px solid gray;margin-top:5px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Name: </td>
                                                                <td style='border:2px solid gray;'><b>{0}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Phone: </td>
                                                                <td style='border:2px solid gray;'><b>{1}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Amount: </td>
                                                                <td style='border:2px solid gray;'><b style='color:red'>=N={2}</b></td>
                                                            </tr>
                                                        </table>", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(account.Name.ToLower()), account.PhoneNo, string.Format("{0:#,###,###.##}", (Math.Round((account.Balance.AvailableBalance / 100), 2))),
                                                                 System.Configuration.ConfigurationManager.AppSettings["SavingsPlanCalculatorFontSize"]));
                            }
                        }
                        else
                        {
                            sb.Append(defaultMessage);
                        }
                    }
                    #endregion
                    #region Loans Due In Less Than Or Equal To Five Days
                    if (isForLoansDueInLessThanOrEqualToFiveDays)
                    {
                        //Here, we get the loans due in less than or equal to 5 days and sort in descending order.
                        List<BankOneMobile.Services.CBAServiceRef.Account> theAccounts = accounts.Where(x => x.DateCreatedFinancial.HasValue && x.DateCreatedFinancial.Value.AddDays(x.LoanCycle) >= currentFinancialDate 
                            && x.DateCreatedFinancial.Value.AddDays(x.LoanCycle) <= currentFinancialDate.AddDays(5)).ToList();
                        //theAccounts = theAccounts.Where(x => x.DateCreatedFinancial.Value.AddDays(x.LoanCycle) >= currentFinancialDate.AddDays(-5) ).ToList();
                        if (theAccounts != null && theAccounts.Count > 0)
                        {
                            //theAccounts.Sort(delegate(BankOneMobile.Services.CBAServiceRef.Account x, BankOneMobile.Services.CBAServiceRef.Account y)
                            //{
                            //    int a = x.DateCreatedFinancial.Value.AddDays(x.LoanCycle).CompareTo(y.DateCreatedFinancial.Value.AddDays(y.LoanCycle));
                            //    if (a == 0)
                            //    {
                            //        a = x.Name.CompareTo(y.Name);
                            //    }
                            //    return a;
                            //});
                            theAccounts = theAccounts.OrderByDescending(x => x.DateCreatedFinancial.Value.AddDays(x.LoanCycle)).ToList();

                            sb.Append(@"<table align='center' style='font-size:13px;border-top:1px solid gray; border-bottom:1px solid gray;margin-bottom:5px;'>
                                            <tr>
                                                <td colspan=2>Customers Due In 5 days</td>
                                            </tr>
                                        </table>
                                        ");
                            foreach (var account in theAccounts)
                            {
                                DateTime dueDate = account.DateCreatedFinancial.Value.AddDays(account.LoanCycle);
                                sb.Append(string.Format(@"
                                                        <table align='center' style='font-size:{3}px;width:98%;border-collapse:collapse;border:3px solid gray;margin-top:3px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Name: </td>
                                                                <td style='border:2px solid gray;'><b>{0}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Phone: </td>
                                                                <td style='border:2px solid gray;'><b>{1}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Date Due: </td>
                                                                <td style='border:2px solid gray;'><b style='color:red'>{2}</b></td>
                                                            </tr>
                                                        </table>", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(account.Name.ToLower()), account.PhoneNo, dueDate.Date.ToString("ddd, MMM d, yyyy"),
                                                                 System.Configuration.ConfigurationManager.AppSettings["SavingsPlanCalculatorFontSize"]));
                            }
                        }
                        else
                        {
                            sb.Append(defaultMessage);
                        }
                    }
                    #endregion
                    #region Active Loans
                    if (isForActiveLoans)
                    {
                        //Here, we get all the active loans disbursed by this agent.
                        List<BankOneMobile.Services.CBAServiceRef.Account> theAccounts = accounts.Where(x => x.Status == Services.CBAServiceRef.AccountStatus.Active).ToList();
                        if (theAccounts != null && theAccounts.Count > 0)
                        {
                            //theAccounts.Sort(delegate(BankOneMobile.Services.CBAServiceRef.Account x, BankOneMobile.Services.CBAServiceRef.Account y)
                            //{
                            //    int a = x.DateCreatedFinancial.Value.CompareTo(y.DateCreatedFinancial.Value);
                            //    if (a == 0)
                            //    {
                            //        a = x.Name.CompareTo(y.Name);
                            //    }
                            //    return a;
                            //});
                            theAccounts = theAccounts.OrderBy(x => x.DateCreatedFinancial.Value.AddDays(x.LoanCycle)).ToList();
                            sb.Append(@"<table align='center' style='font-size:13px;border-top:1px solid gray; border-bottom:1px solid gray;margin-bottom:5px;'>
                                            <tr>
                                                <td colspan=2>Active Loans</td>
                                            </tr>
                                        </table>
                                        ");
                            foreach (var account in theAccounts)
                            {
                                DateTime dueDate = account.DateCreatedFinancial.Value.AddDays(account.LoanCycle);
                                sb.Append(string.Format(@"
                                                        <table align='center' style='font-size:{0}px;width:98%;border-collapse:collapse;border:3px solid gray;margin-top:3px;'>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Name: </td>
                                                                <td style='border:2px solid gray;'><b>{1}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Phone: </td>
                                                                <td style='border:2px solid gray;'><b>{2}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Date Due: </td>
                                                                <td style='border:2px solid gray;'><b>{3}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='border:2px solid gray;width:1%;white-space:nowrap;padding:0px 5px;' align='left'>Days Left: </td>
                                                                <td style='border:2px solid gray;'><b style='color:{4}'>{5}</b></td>
                                                            </tr>
                                                        </table>", System.Configuration.ConfigurationManager.AppSettings["SavingsPlanCalculatorFontSize"],
                                                                 CultureInfo.CurrentCulture.TextInfo.ToTitleCase(account.Name.ToLower()), account.PhoneNo, dueDate.Date.ToString("ddd, MMM d, yyyy"),
                                                                 (dueDate - currentFinancialDate).TotalDays < 0 && account.Balance.AvailableBalance > 0 && account.DateCreatedFinancial.Value.AddDays(account.LoanCycle) < currentFinancialDate ? "Red" : (dueDate - currentFinancialDate).TotalDays < 0 && account.DateCreatedFinancial.Value.AddDays(account.LoanCycle) >= currentFinancialDate ? "Black" : "#06522C",
                                                                 (dueDate - currentFinancialDate).TotalDays < 0 && account.Balance.AvailableBalance > 0 && account.DateCreatedFinancial.Value.AddDays(account.LoanCycle) < currentFinancialDate ? "0 (Defaulting)" : (dueDate - currentFinancialDate).TotalDays < 0 && account.DateCreatedFinancial.Value.AddDays(account.LoanCycle) >= currentFinancialDate ? "0" : (Math.Round((dueDate - currentFinancialDate).TotalDays, 0)).ToString()
                                                                 ));
                            }
                        }
                        else
                        {
                            sb.Append(defaultMessage);
                        }
                    }
                    #endregion
                }
                else
                {
                    sb.Append(defaultMessage);
                }
                    
            }
            catch (TransactionNotPermittedToNonAgentsException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.TransactionNotPermittedToNonAgents), Utilities.GetDescription(ErrorCodes.TransactionNotPermittedToNonAgents));
            }
            catch (InvalidAgentCodeException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.InvalidAgentCode), Utilities.GetDescription(ErrorCodes.InvalidAgentCode));
            }
            catch (NoSwitchResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                throw new FailureException("13", ex.Message);
            }
            catch (Exception ex)
            {
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError), ex.Message);
            }



            finally
            {
                //TransactionSuccessful.Set(context, realResponse);
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["USSDDemoMode"]))
                {
                    try
                    {
                        new SessionSystem().StopSession(sessionID, SessionStatus.EndedNormally, currentWorkflowInfo.DataSource);
                    }
                    catch
                    {
                    }

                }
                //STOP...... THIS IS CRIMINAL....
                //BankOneMobile.Data.NHibernate.DbContext.Instance.Terminate();

            }
            Result.Set(context, sb.ToString());
        }
    }

}
