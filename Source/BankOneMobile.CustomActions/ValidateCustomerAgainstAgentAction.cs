﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    /// <summary>
    /// This custom action is for Diamond Bank Target Savings
    /// </summary>
    [Designer(typeof(ValidateCustomerAgainstAgentDesigner))]
    public class ValidateCustomerAgainstAgentAction : ActionNode
    {
        /// <summary>
        /// CustomerPhoneNumber
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                ValidateCustomerAgainstAgentActivity activity = this.InternalState as ValidateCustomerAgainstAgentActivity;
                if (activity != null)
                {

                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                ValidateCustomerAgainstAgentActivity activity = this.InternalState as ValidateCustomerAgainstAgentActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
       


        public ValidateCustomerAgainstAgentAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new ValidateCustomerAgainstAgentActivity();
        }

    }

    public class ValidateCustomerAgainstAgentActivity : CodeActivity
    {
        public InArgument<string> CustomerPhoneNumber { get; set; }
        

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string agentPhoneNumber = mob.MobilePhone;
            string customerPhoneNumber = CustomerPhoneNumber.Get(context);

            Agent agent;
            agent = new AgentSystem(currentWorkflowInfo.DataSource).GetAgentByPhoneNumber(mob.MobilePhone);
            if (agent == null)
            {
                throw new TransactionNotPermittedToNonAgentsException("Non-Agent Initiated Trnsactions");
            }

            //MobileAccount custMob = new MobileAccountSystem().GetByPhoneNumber(customerPhoneNumber);
            MobileAccount custMob = new MobileAccountSystem().GetMobileAccountByPhoneNumber(mob.InstitutionCode, customerPhoneNumber) as MobileAccount;

            if(custMob == null)
            {
                throw new Exception("Invalid Phone Number");
            }
            if(custMob.BankAccounts == null)
            {
                throw new Exception("This customer has no linked account");
            }
            if (custMob.BankAccounts.Count > 1 == null)
            {
                throw new Exception("Could not retrieve unique linked account for this customer");
            }

            string message = string.Format(" Validate Customer - agentPhone:{0},customerPhoneNumber:{1}",agentPhoneNumber, customerPhoneNumber);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
            try
            {
               BankOneMobile.Services.CBAServiceRef.Account[] accounts = null;
               using(var client = new BankOneMobile.Services.CBAServiceRef.CBAServiceClient())
               {
                   accounts = client.GetSavingsAccountsByAccountOfficerCode(mob.InstitutionCode, agent.AccountNumber);
               }
                if(accounts != null && accounts.Length > 0)
                {
                    if(!accounts.Any(x=>x.NUBAN == custMob.BankAccounts[0].BankAccount))
                    {
                        throw new Exception("This customer is tied to another Account Officer");
                    }
                }
                else
                {
                    throw new Exception("There are no accounts opened by you");
                }
            }
            catch (Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
                throw new Exception("An error occured");
            }
        }
    }
}
