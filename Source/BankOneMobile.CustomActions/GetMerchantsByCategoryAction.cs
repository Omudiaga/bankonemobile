﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetMerchantsActionDesigner))]
    public class GetMerchantsByCategoryAction : ActionNode
    {

        public InArgument<String> CategoryCode
        {
            get
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    return activity.CategoryCode;
                }
                return null;
            }
            set
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    activity.CategoryCode = value;
                }
            }
        }

        public OutArgument<IList<Merchant>> Merchants
        {
            get
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    return activity.Merchants;
                }
                return null;
            }
            set
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    activity.Merchants = value;
                }
            }
        }
        public InArgument<MerchantCategory> TheMerchantCategory
        {
            get
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    return activity.TheMerchantCategory;
                }
                return null;
            }
            set
            {
                GetMerchantsByCategoryActivity activity = this.InternalState as GetMerchantsByCategoryActivity;
                if (activity != null)
                {
                    activity.TheMerchantCategory = value;
                }
            }
        }

        public GetMerchantsByCategoryAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetMerchantsByCategoryActivity();
        }

    }

    public class GetMerchantsByCategoryActivity : CodeActivity
    {


        public OutArgument<IList<Merchant>> Merchants { get; set; }
        public InArgument<MerchantCategory> TheMerchantCategory { get; set; }
        public InArgument<String> CategoryCode { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            

            IList<Merchant> merch = null;
            try
            {
              merch =   new MerchantSystem().GetMerchantsByCategoryID(CategoryCode.Get(context));
            }
            catch (FaultException ex)
            {

                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), Utilities.GetDescription(ErrorCodes.ISWGatewayUnavailable));
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.ISWGatewayUnavailable), "Error on ISW Gateway");
            }
            
            Merchants.Set(context, merch);
            


        }
    }
}
