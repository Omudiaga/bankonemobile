﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.Diagnostics;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(BalanceEnquiryDesigner))]
    public class GetInstitutionAction : ActionNode
    {
        
        

        /// <summary>
        /// Produts for agent's institution
        /// </summary>
        public OutArgument<IInstitution> TheInstitution
        {
            get
            {
                GetInstitutionActivity activity = this.InternalState as GetInstitutionActivity;
                if (activity != null)
                {
                    return activity.TheInstitution;
                }
                return null;
            }
            set
            {
                GetInstitutionActivity activity = this.InternalState as GetInstitutionActivity;
                if (activity != null)
                {
                    activity.TheInstitution = value;
                }
            }
        }

        public GetInstitutionAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetInstitutionActivity();
        }

    }

    public class GetInstitutionActivity : CodeActivity
    {
       

        /// <summary>
        /// The agent's institution
        /// </summary>
        public OutArgument<IInstitution> TheInstitution { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            Trace.TraceInformation("About To Get Institution");
            IInstitution theInstitution = new InstitutionSystem().GetByCode(currentWorkflowInfo.DataSource, mob.InstitutionCode);
            Trace.TraceInformation(string.Format("Done. Institution: {0}", theInstitution == null ? "Null" : "Not Null"));
            Trace.TraceInformation(string.Format("Institution Name/Code: {0}/{1}", theInstitution == null ? "Null" : theInstitution.Name, theInstitution == null ? "Null" : theInstitution.Code));
            if (theInstitution == null)
            {
                throw new Exception("Could not retrieve agent's institution");
            }
            TheInstitution.Set(context, theInstitution);
        }
    }
}
