﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;


namespace BankOneMobile.CustomActions
{
    /// <summary>
    /// This custom action is for Diamond Bank Target Savings
    /// </summary>
    [Designer(typeof(GetAgentGeoLocationDesigner))]
    public class GetAgentGeoLocationAction : ActionNode
    {
        /// <summary>
        /// CustomerPhoneNumber
        /// </summary>
        public InArgument<string> CustomerPhoneNumber
        {
            get
            {
                GetAgentGeoLocationActivity activity = this.InternalState as GetAgentGeoLocationActivity;
                if (activity != null)
                {

                    return activity.CustomerPhoneNumber;
                }
                return null;
            }
            set
            {
                GetAgentGeoLocationActivity activity = this.InternalState as GetAgentGeoLocationActivity;
                if (activity != null)
                {
                    activity.CustomerPhoneNumber = value;
                }
            }
        }
        

        /// <summary>
        /// Transaction Type
        /// </summary>
        public InArgument<string> TransactionType
        {
            get
            {
                GetAgentGeoLocationActivity activity = this.InternalState as GetAgentGeoLocationActivity;
                if (activity != null)
                {
                    return activity.TransactionType;
                }
                return null;
            }
            set
            {
                GetAgentGeoLocationActivity activity = this.InternalState as GetAgentGeoLocationActivity;
                if (activity != null)
                {
                    activity.TransactionType = value;
                }
            }
        }
       


        public GetAgentGeoLocationAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetAgentGeoLocationActivity();
        }

    }

    public class GetAgentGeoLocationActivity : CodeActivity
    {
        /// <summary>
        /// Entered Phone Number
        /// </summary>
        public InArgument<string> CustomerPhoneNumber { get; set; }
        /// <summary>
        /// Selected Account Number
        /// </summary>
        public InArgument<string> TransactionType { get; set; }
        

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string geoLocation = currentWorkflowInfo.GeoLocation;
            string agentPhoneNumber = mob.MobilePhone;
            string customerPhoneNumber = CustomerPhoneNumber.Get(context);
            string transactionType = TransactionType.Get(context);
            string message = string.Format("Parameters - agentPhone:{0},customerPhoneNumber:{1},geoLocation:{2},transactionType:{3}", agentPhoneNumber, customerPhoneNumber, geoLocation, transactionType);
            PANE.ERRORLOG.ErrorLogger.Log(new Exception(message));
            try
            {
                if (!string.IsNullOrWhiteSpace(geoLocation))
                {
                    TransactionTypeName tranTypeEnum = (TransactionTypeName)Enum.Parse(typeof(TransactionTypeName), transactionType, true);
                    AgentGeoLocationTransactionType agentGeoLocation = new AgentGeoLocationTransactionType
                    {
                        InstitutionCode = mob.InstitutionCode,
                        AgentPhoneNumber = agentPhoneNumber,
                        CustomerPhoneNumber = customerPhoneNumber,
                        TransactionType = tranTypeEnum,
                        Latitude = geoLocation.Split(';')[0],
                        Longitude = geoLocation.Split(';')[1],
                        DateLogged = DateTime.Now
                    };
                    PANE.ERRORLOG.ErrorLogger.Log(new Exception("About to save geo location"));
                    new AgentGeoLocationTransactionTypeSystem().SaveAgentGeoLocationTransactionType(currentWorkflowInfo.DataSource, agentGeoLocation);
                }
            }
            catch(Exception ex)
            {
                PANE.ERRORLOG.ErrorLogger.Log(ex);
            }
        }
    }
}
