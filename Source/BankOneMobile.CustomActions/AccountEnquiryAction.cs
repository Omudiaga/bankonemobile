﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Exceptions;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using System.Configuration;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(AccountEnquiryDesigner))]
    public class AccountEnquiryAction : ActionNode
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN
        {
            get
            {
                AccountEnquiryActivity activity = this.InternalState as AccountEnquiryActivity;
                if (activity != null)
                {
                 
                    return activity.PIN;
                }
                return null;
            }
            set
            {
                AccountEnquiryActivity activity = this.InternalState as AccountEnquiryActivity;
                if (activity != null)
                {
                    activity.PIN = value;
                }
            }
        }
        
        /// <summary>
        /// Response ISO Transaction
        /// </summary>
        public OutArgument<string> StatusResponse
        {
            get
            {
                AccountEnquiryActivity activity = this.InternalState as AccountEnquiryActivity;
                if (activity != null)
                {
                    return activity.StatusResponse;
                }
                return null;
            }
            set
            {
                AccountEnquiryActivity activity = this.InternalState as AccountEnquiryActivity;
                if (activity != null)
                {
                    activity.StatusResponse = value;
                }
            }
        }

        public AccountEnquiryAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new AccountEnquiryActivity();
        }

    }

    public class AccountEnquiryActivity : CodeActivity
    {
        /// <summary>
        /// User PIN
        /// </summary>
        public InArgument<string> PIN { get; set; }
        
        public OutArgument<string> StatusResponse { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //IDataSource source =  DataSourceFactory.GetDataSource(DataCategory.Shared);
             WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;
            string pin = PIN.Get(context);
             pin  =  HSMCenter.GeneratePinBlock(phoneNumber, pin, AppZone.HsmInterface.PinBlockFormats.ANSI);
             byte[] encryptedPinBytes = new byte[8];
             if (!string.IsNullOrEmpty(pin))
             {
                 ThalesSim.Core.Utility.HexStringToByteArray(pin, ref encryptedPinBytes);
             }
             
             string response = string.Empty;
                       

            //response = new TransactionSystem().TestRunTransaction(pin, tranType, mob, accountNumber, accountNumber, 0.0M, sessionID);// u se 0 cos we dont really care about amount in balance enquiry
            try
            {
                response = new TransactionSystem().DoLinkedAccountEnquiry(mob,encryptedPinBytes);
                if (response != "00")
                {
                    throw new FailedTransactionException( response);
                }
                else
                {
                    StatusResponse.Set(context, "true");
                }
            }
            catch (NoSwitchResponseException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
                throw new FailureException(Utilities.GetCode(ErrorCodes.SwitchUnavailable), Utilities.GetDescription(ErrorCodes.SwitchUnavailable));
            }
            catch (NoIssuerResponseException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
                throw new FailureException(Utilities.GetCode(ErrorCodes.IssuerUnavailable), Utilities.GetDescription(ErrorCodes.IssuerUnavailable));
            }
            catch (FailedTransactionException ex)
            {
                StatusResponse.Set(context, ex.Message);
                throw new FailureException("13", ex.Message);
            }
            catch (InvalidPhoneNumberException)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                throw new FailureException(Utilities.GetCode(ErrorCodes.PhoneNumberNotRegisterd),Utilities.GetDescription(ErrorCodes.PhoneNumberNotRegisterd));
                
            }

            catch(Exception ex)
            {
                StatusResponse.Set(context, Utilities.GetDescription(ErrorCodes.SystemError));
                new PANE.ERRORLOG.Error().LogToFile(ex);
                throw new FailureException(Utilities.GetCode(ErrorCodes.SystemError),ex.Message);
            }

            finally
            {

               //
               

            }
           
            
        
        }
    }
}
