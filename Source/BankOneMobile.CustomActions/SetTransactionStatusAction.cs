﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;

namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetProductsDesigner))]
    public class SetTransactionStatusAction : ActionNode
    {



        /// <summary>
        /// List of Products
        /// </summary>
        public InArgument<Boolean> TransactionSuccessful
        {
            get
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.IsSuccessfulTransaction;
                }
                return null;
            }
            set
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    activity.IsSuccessfulTransaction = value;
                }
            }
        }
        public InArgument<string> FailureReason
        {
            get
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.FailureReason;
                }
                return null;
            }
            set
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    activity.FailureReason = value;
                }
            }
        }


        public InArgument<Boolean> TransactionCompleted
        {
            get
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    return activity.IsCompleted;
                }
                return null;
            }
            set
            {
                SetTransactionStatusActivity activity = this.InternalState as SetTransactionStatusActivity;
                if (activity != null)
                {
                    activity.IsCompleted = value;
                }
            }
        }

        public SetTransactionStatusAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new SetTransactionStatusActivity();
        }

    }

    public class SetTransactionStatusActivity : CodeActivity
    {


        public InArgument<Boolean> IsSuccessfulTransaction { get; set; }
        public InArgument<Boolean> IsCompleted { get; set; }
        public InArgument<string> FailureReason { get; set; }

        protected override void Execute(CodeActivityContext context)
        {


            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);

           // if (currentWorkflowInfo.HasPreviousWorkflow && currentWorkflowInfo.ThePreviousWorkflow != null)
            {
                currentWorkflowInfo.ChildTransactionSuccessful = IsSuccessfulTransaction.Get(context);
                currentWorkflowInfo.ChildTransactionCompleted = IsCompleted.Get(context);
                currentWorkflowInfo.FailureReason = FailureReason.Get(context);
            }

        }
    }
}
