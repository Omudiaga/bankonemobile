﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.SessionNodeDesigners;
using System.ComponentModel;
using System.Activities;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using System.ServiceModel;
using BankoneMobile.AdvansLafayette.Core.Implementation;
using BankoneMobile.AdvansLafayette.Core;


namespace BankOneMobile.CustomActions
{
    [Designer(typeof(GetProductsDesigner))]
    public class GetLafayetteIDTypesAction : ActionNode
    {
      
       

        /// <summary>
        /// List of IDType
        /// </summary>
        public OutArgument<IList<CoreIDType>> IDType
        {
            get
            {
                
                GetLafayetteIDTypesActivity activity = this.InternalState as GetLafayetteIDTypesActivity;
                if (activity != null)
                {
                    return activity.IDType;
                }
                return null;
            }
            set
            {
                GetLafayetteIDTypesActivity activity = this.InternalState as GetLafayetteIDTypesActivity;
                if (activity != null)
                {
                    activity.IDType = value;
                }
            }
        }

        public GetLafayetteIDTypesAction()
        {
            this.StateId = Guid.NewGuid().ToString();
            this.InternalState = new GetLafayetteIDTypesActivity();
        }

    }

    public class GetLafayetteIDTypesActivity : CodeActivity
    {


        public OutArgument<IList<CoreIDType>> IDType { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
           

            //TODO: Do Balance Enquiry
            WorkflowInfo currentWorkflowInfo = WorkflowCenter.Instance.GetWorkflowBy(context.WorkflowInstanceId);
            string sessionID = currentWorkflowInfo.SessionID;
            MobileAccount mob = currentWorkflowInfo.InitiatedBy as MobileAccount;
            string phoneNumber = mob.MobilePhone;

            List<CoreIDType> types = new List<CoreIDType>(); 
            try
            {

                //IDType =    new IDTypeystem(currentWorkflowInfo.DataSource).GetLafayetteIDTypesByAgentPhone(mob.MobilePhone);   
                //var res = new BankoneMobile.AdvansLafayette.Services.ServiceInterfaceSystem().GetIDTypes();
                //if (res != null)
                //{

                //    foreach (BankoneMobile.AdvansLafayette.AdvRef.IDType tt in res)
                //    {
                //        types.Add(new CoreIDType { Code = tt.Code, Name = tt.Name });
                //    }
                //}  
                types = new MobileAccountSystem().GetLafayetteIDTypes();
                
            }
            catch (FaultException ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), "Lafayette Service Unavailable");
            }
            catch (Exception ex)
            {
                throw new FailureException(Utilities.GetCode(ErrorCodes.UnableToConnectToCoreBanking), ex.Message);
            }
           IDType.Set(context, types);
            

           

        }
    }
}
