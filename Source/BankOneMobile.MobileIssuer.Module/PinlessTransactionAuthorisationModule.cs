﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppZoneSwitch.Services.Contracts;
using AppZoneSwitch.Services.Utility;
using Trx.Messaging.Iso8583;
using System.Diagnostics;
using AppZoneSwitch.Core.Implementations;
//using BankOneMobile.Services;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Data.Contracts;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.MobileIssuer.Module
{
    public class PinlessTransactionAuthorisationModule : IAuthorizationService
    {
        public Trx.Messaging.Iso8583.Iso8583Message Authorize(AppZoneSwitch.Core.Contracts.IScheme theScheme, Trx.Messaging.Iso8583.Iso8583Message theMessage)
        {
            Trace.TraceInformation("I'm in the Pinless authorizer");
            Trx.Messaging.Iso8583.Iso8583Message toReturn = new Trx.Messaging.Iso8583.Iso8583Message();
            try
            {
                toReturn = InternalAuthorize(theScheme, theMessage);
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Main: {0}, Inner :{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                throw;
            }

            return toReturn;
        }

        public static Iso8583Message InternalAuthorize(AppZoneSwitch.Core.Contracts.IScheme theScheme, Iso8583Message theMessage)
        {
            if (theMessage == null)
            {
                Trace.TraceInformation("Null ISO Message");

                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);

            }
            if (theMessage.Fields[2] == null || theMessage.Fields[2].Value == null)
            {
                Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "2");
                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.InvalidCardNumber);
            }
            if (theMessage.Fields[3] == null || theMessage.Fields[3].Value == null)
            {
                Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "3");
                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.TransactionNotPermitted);
            }
            string transactionType = theMessage.Fields[3].Value.ToString();
            if (transactionType.Substring(0, 2) == "31")
            {
                //Balance Enquiry
                return theMessage;
            }
            string to = (theMessage.Fields[103] != null && theMessage.Fields[103].Value != null) ? theMessage.Fields[103].Value.ToString() : string.Empty;
            if (!string.IsNullOrEmpty(to))
            {
                Trace.TraceInformation(string.Format("Aready contained {0} as to account", to));
            }
            string institutionCode = (theMessage.Fields[100] != null && theMessage.Fields[100].Value != null) ? theMessage.Fields[100].Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(institutionCode))
            {
                Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "100(Institution code)");
                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);
            }
            Trace.TraceInformation("About to call the AppZone income account service");
            string toAccount = null;
            //IDataSource dataSource = DataSourceFactory.GetDataSource(DataCategory.Shared);
            Institution inst = null;
            if(inst == null)
            {
                Trace.TraceInformation("No institution found with code");
                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);
            }
            using (AppZoneIncomeAccountService.UtilityServiceClient client = new AppZoneIncomeAccountService.UtilityServiceClient())
            {
                AppZoneIncomeAccountService.AppZoneIncomeAccount incomeAccount = client.GetIncomeAccountByInstitutionID(inst.ID);
                if (incomeAccount != null)
                {
                    toAccount = incomeAccount.AccountNumber;
                }
            }
            if (string.IsNullOrEmpty(toAccount))
            {
                Trace.TraceInformation("I'm sorry the  ISO Message Field {0} is null", "100(Institution code)");
                throw new AppZoneSwitch.Services.RoutingSystem.AuthorizationException(ResponseCodes.FormatError);
            }
            theMessage.Fields.Add(103, toAccount);
            return theMessage;
        }

    }
}
