﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class AccountCreatedNoFundsTranferException : ApplicationException
    {
        public AccountCreatedNoFundsTranferException(string msg)
            : base(msg)
        {
        }
    }
}
