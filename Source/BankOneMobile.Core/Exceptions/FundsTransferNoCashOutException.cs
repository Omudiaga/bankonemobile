﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class FundsTransferNoCashOutException : ApplicationException
    {
        public FundsTransferNoCashOutException(string msg)
            : base(msg)
        {
        }
    }
}
