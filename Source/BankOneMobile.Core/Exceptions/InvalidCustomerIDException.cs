﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidCustomerIDException : ApplicationException
    {
        public InvalidCustomerIDException(string msg)
            : base(msg)
        {
        }
    }
}
