﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidPinException : ApplicationException
    {
        public InvalidPinException(string msg)
            : base(msg)
        {
        }
    }
}
