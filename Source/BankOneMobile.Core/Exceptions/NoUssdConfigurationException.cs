﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class NoUssdConfigurationException : ApplicationException
    {
        public NoUssdConfigurationException(string msg)
            : base(msg)
        {
        }
    }
}