﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidSessionIdException : ApplicationException
    {
        public InvalidSessionIdException(string msg)
            : base(msg)
        {
        }
    }
}
