﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
   public  class InvalidAgentCodeException : ApplicationException
    {
       public InvalidAgentCodeException(string msg)
           : base(msg)
       {
       }
    }
}
