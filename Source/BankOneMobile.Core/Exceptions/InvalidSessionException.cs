﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class InvalidSessionException : ApplicationException
    {
        public InvalidSessionException(string msg)
            : base(msg)
        {
        }
    }
}
