﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    //[DataContract]
    public class AlreadyRegisterdCustomerException : ApplicationException
    {
        public AlreadyRegisterdCustomerException(string msg)
            : base(msg)
        {
        }
    }
}
