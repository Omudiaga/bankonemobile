﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class AboveTransactionLimitException : ApplicationException
    {
        public AboveTransactionLimitException(string msg)
            : base(msg)
        {
        }
    }
}
