﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    //[DataContract]
    public class AlreadyExistingAccountException : ApplicationException
    {
        public AlreadyExistingAccountException(string msg)
            : base(msg)
        {
        }
    }
}
