﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
    public class PhoneNumberAccountNumberMismatchException : ApplicationException
    {
        public PhoneNumberAccountNumberMismatchException(string msg)
            : base(msg)
        {
        }
    }
}
