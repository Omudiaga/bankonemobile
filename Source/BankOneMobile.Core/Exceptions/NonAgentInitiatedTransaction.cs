﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
  public   class NonAgentInitiatedTransaction : ApplicationException
    {
      public NonAgentInitiatedTransaction(string msg)
          : base(msg)
      {
      }
    }
}
