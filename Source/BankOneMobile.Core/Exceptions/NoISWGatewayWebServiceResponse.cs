﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Exceptions
{
   public  class NoISWGatewayWebServiceResponseException : ApplicationException
    {
       public NoISWGatewayWebServiceResponseException(string msg)
           : base(msg)
       {
       }
    }
}
