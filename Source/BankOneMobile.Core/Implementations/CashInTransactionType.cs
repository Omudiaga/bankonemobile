﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class CashInTransactionType : EntityWithEnableDisable, ICashInTransactionType
    {
       public virtual  string AgentCode { get; set; }
       public virtual string Token { get; set; }
       public virtual ITransaction TheTransaction { get; set; }
       public virtual decimal Amount { get; set; }
       public virtual LinkingBankAccount TheBankAccount { get; set; }
       public virtual  bool IsPhoneNumber { get; set; }
       public virtual   string EnteredParameter { get; set; }
       public virtual IFeeTransactionType TheFee
       {
           get;
           set;
       }

      
    }
}
