﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class MobileRequest: Entity, IMobileRequest
    {
        public virtual string SessionID { set; get; }
        public virtual DateTime RequestTime { set; get; }
        public virtual DateTime? ResponseTime { set; get; }
        public virtual string Operation { set; get; }
        public virtual string RequestResponse { set; get; }
        public virtual string Request { set; get; }

    }
}
