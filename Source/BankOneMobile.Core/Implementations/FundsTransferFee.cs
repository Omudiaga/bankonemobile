﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class FundsTransferFee : EntityWithEnableDisable, IFundsTransferFee
    {
        public virtual string InstitutionCode { get; set; }
        public virtual TransactionTypeName TransactionTypeName { set; get; }
        /// <summary>
        /// True for Flat, False for Percent
        /// </summary>
        public virtual bool FlatOrPercent { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual int VATInPercentage { set; get; }

    }
}
