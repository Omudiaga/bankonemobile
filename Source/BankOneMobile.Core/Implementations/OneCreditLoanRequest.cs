﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class OneCreditLoanRequest : Entity, IOneCreditLoanRequest
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string CustomerPhoneNumber { get; set; }
        public virtual string JsonRequest { get; set; }
        public virtual string Response { get; set; }
        public virtual RegistrationStatus Status { get; set; }
        public virtual string StatusMessage { get; set; }
        public virtual DateTime DateLogged { get; set; }
        public virtual DateTime? DateSent { get; set; }
    }
}
