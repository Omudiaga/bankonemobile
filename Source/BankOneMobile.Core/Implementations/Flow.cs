﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    public class Flow : EntityWithEnableDisable, IFlow
    {

        #region IFlow Members
        [DataMember]
        public virtual string Name
        {
            get;
            set;
        }
        [DataMember]
        public virtual string Description
        {
            get;
            set;
        }
        [DataMember]
        public virtual bool IsMainFlow
        {
            get;
            set;
        }

        [DataMember]
        public virtual bool IsAgent
        {
            get;
            set;
        }
        [DataMember]
        public virtual bool IsAccountActivation
        { 
            get; 
            set;
        }
        public virtual bool IsFundsTransfer
        {
            get;
            set;
        }
        #endregion

        #region IRentable Members

        public virtual IInstitution TheTenant
        {
            get;
            set;
        }

        #endregion

        #region IWFActivity Members
        [DataMember]
        public virtual string XamlDefinition
        {
            get;
            set;
        }

        [DataMember]
        public virtual Guid UniqueId
        {
            get;
            set;
        }

        public virtual System.Activities.Activity TheActivity
        {
            get { return null; }
        }

        #endregion
    }
}
