﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [KnownType(typeof(IBalanceInquiryTransactionType))]
    public class BalanceInquiryTransactionType : EntityWithEnableDisable, IBalanceInquiryTransactionType
    {
        
        public virtual ITransaction TheTransaction { get; set; }

        
        public virtual IMobileAccount TheMobileAccount { get; set; }

        public virtual bool IsForThirdParty { get; set; }
        public virtual bool IsForSelfService { get; set; }


        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }


   

        
    }
}
