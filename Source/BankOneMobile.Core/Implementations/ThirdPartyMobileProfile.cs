﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class ThirdPartyMobileProfile: EntityWithEnableDisable, IThirdPartyMobileProfile
    {
        public virtual string PhoneNumber { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual string LastName { get; set; }
        public virtual string OtherName { get; set; }
        public virtual string Gender { get; set; }
        public virtual string Status { get; set; }
        public virtual string InstitutionCode { get; set; }


    }
}
