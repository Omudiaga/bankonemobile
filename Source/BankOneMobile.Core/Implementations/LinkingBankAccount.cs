﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using System.Runtime.Serialization;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using PANE.Framework.AuditTrail.Attributes;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [Serializable]
    public class LinkingBankAccount : Entity, ILinkingBankAccount
    {
        [DataMember]
        [TrailableIgnore]
        public virtual string Name { get; set; }

        public virtual IMobileAccount TheMobileAccount { set; get; }
        IDataInjectionHelper BankAccountHelper { set; get; }
         [DataMember]

        //PLEASE Ensure that InstitutionCode is set before trying to get/set BankAccount
        //because that is the basis for setting Standard/NUBAN
        public virtual string BankAccount 
         {
            set 
            {
                BankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                var type = BankAccountHelper.GetInstitutionAccountNumberType(InstitutionCode);
                switch(type)
                {
                    case AccountNumberType.NUBAN:
                        NUBAN = value; break;
                    case AccountNumberType.Standard:
                    default:
                        StandardAccount = value; break;
                }
            }
            get 
            {
                BankAccountHelper = SafeServiceLocator<IDataInjectionHelper>.GetService();
                var type = BankAccountHelper.GetInstitutionAccountNumberType(InstitutionCode);
                switch(type)
                {
                    case AccountNumberType.NUBAN:
                        return NUBAN; 
                    case AccountNumberType.Standard:
                    default:
                        return StandardAccount; 
                }
            }
         }
        public virtual  string ProductName { get; set; }
        [TrailableIgnore]
       public virtual  string ProductCode { get; set; }
        [TrailableIgnore]
         public virtual IProduct TheProduct { get; set; }
        [TrailableIgnore]
        public virtual decimal Balance { get; set; }
        [TrailableIgnore]
        public virtual IHost TheHost { set; get; }
         [DataMember]

        public virtual string CustomerID { set; get; }
        public override bool Equals(object obj)
        {
            ILinkingBankAccount linkBankAccount = obj as ILinkingBankAccount;
            if (linkBankAccount != null)
            {
                if (linkBankAccount is LinkingBankAccount && this.ID == linkBankAccount.ID) return true;
                if (this.BankAccount == linkBankAccount.BankAccount) return true;
            }
            return base.Equals(obj);
        }
         [DataMember]
         [TrailableIgnore]
        public virtual bool Activated { get; set; }
         [DataMember]

        public virtual string InstitutionCode { get; set; }
        [TrailableName("Gender")]
         public virtual Gender TheGender { get; set; }
        [TrailableName("Institution")]
         public virtual string InstitutionName { get; set; }
        public virtual BankAccountStatus BankStatus { get; set; }
        public virtual string  CoreBankingNames { get; set; }
        public virtual byte[] Signature { get; set; }
        public virtual byte[] Passport { get; set; }
        public virtual string PlaceOfBirth { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual string Address { get; set; }
        public virtual string StarterPackNumber { get; set; }
        public virtual string IDNumber { get; set; }
        public virtual string NOKName { get; set; }
        public virtual string NOKPhone { get; set; }
        public virtual string ReferralPhoneNumber { get; set; }
        public virtual string NUBAN { get; set; }
        public virtual string StandardAccount { get; set; }
       
    }
}
 