﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    public class Transaction : EntityWithEnableDisable, ITransaction
    {
        [DataMember]
        public virtual string To { get; set; }
        [DataMember]
        public virtual string From { get; set; }
        [DataMember]
        public virtual decimal Amount { get; set; }
        public virtual bool NotValidatePin { get; set; }
        public virtual ISession Session { get; set; }
        [DataMember]
        public virtual DateTime Date { get; set; }
        
        public virtual TransactionStatus Status { get; set; }
        [DataMember]
        public virtual long TransactionTypeID { get; set; }
        [DataMember]
        public virtual TransactionTypeName TransactionTypeName {get;set;}
        [DataMember]
        public virtual string FromPhoneNumber { get; set; }
        public virtual string ToPhoneNumber { get; set; }
        [DataMember]
        public virtual byte[] EncryptedPIN { get; set; }
        [DataMember]
        public virtual string STAN { get; set; }
        [DataMember]
        public virtual DateTime SwitchTransactionTime { get; set; }
        public virtual  string FromInstitutionCode { get; set; }
       public virtual   string ToInstitutionCode { get; set; }
       public virtual string InstitutionCode { get; set; }
       public virtual string AgentCode { get; set; }
       public virtual string Narration { get; set; }
       public virtual Service TheService{get;set;}
       public virtual string StatusDetails { get; set; }
       public virtual string TheTransactionType
       {
           get
           {
               return BankOneMobile.Core.Helpers.Utility.Utility.SpaceWords(this.TransactionTypeName.ToString());
           }
       }
      public virtual  string StringAmount
       {
           get
           {
               return (this.Amount / 100).ToString("N");
           }
       }
     //public virtual  ILinkingBankAccount TheBankAccount { get; set; }

       #region ITransaction Members


       public virtual IFeeTransactionType TheFee
       {
           get;
           set;
       }
       public virtual ServiceCodes ServiceCodes
       {
           get;
           set;
       }
       public virtual MerchantType TheMerchantType
       {
           get;
           set;
       }
       #endregion

      
          


    }
}
