﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class MiniStatementTransactionType : EntityWithEnableDisable, IMiniStatementTransactionType
    {

        public virtual ITransaction TheTransaction { get; set; }
        public virtual IMobileAccount TheMobileAccount { get; set; }
        public virtual IList<TransactionHistory> TheTransactions { get; set; }
        public virtual bool  IsForThirdParty { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

       
    }
}
