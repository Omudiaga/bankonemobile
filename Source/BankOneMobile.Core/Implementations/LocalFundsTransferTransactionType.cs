﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class LocalFundsTransferTransactionType : EntityWithEnableDisable, ILocalFundsTransferTransactionType
    {


      

        public virtual string ToAccount { get; set; }
        public virtual string ToInstCode { get; set; }
        public virtual string ReceivingAccountName { get; set; }


        public virtual string AgentCode
        {
            get;
            set;
        }

        public virtual decimal Amount
        {
            get;
            set;
        }

        public virtual bool isPhoneNumber { get; set; }

        public virtual ITransaction TheTransaction
        {
            get;
            set;
        }



        #region ITransactionType Members


        public virtual  IFeeTransactionType TheFee
        {
            get;
            set;
        }

        #endregion
    }
}
