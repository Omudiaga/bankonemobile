﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class Host : EntityWithEnableDisable, IHost
    {
       public virtual string ClassType { set; get; }
       public virtual string Name { set; get; }
       public virtual string DllPath { set; get; }
    }
}
