﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BankOneMobile.Core.Implementations
{
    public class TargetSavingsAccount
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public string NUBAN { get; set; }
        public string CustomerID { get; set; }

        public override string ToString()
        {
            return Number;
        }
    }
}
