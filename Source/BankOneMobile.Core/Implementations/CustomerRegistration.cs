﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class CustomerRegistration : EntityWithEnableDisable, ICustomerRegistration
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string InstitutionName { get; set; }
        public virtual string ProductCode { get; set; }
        public virtual string ProductName { get; set; }
        public virtual long PassportID { get; set; }
        public virtual Gender TheGender { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string NOKName { get; set; }
        public virtual string NOKPhone { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual string Address { get; set; }
        public virtual string PlaceOfBirth { get; set; }
        public virtual string CardSerialNumber { get; set; }
        public virtual string ActivationCode { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual RegistrationStatus Status { get; set; }
        public virtual CommitPoint CommitPoint { get; set; }
        public virtual string ErrorInfo { get; set; }
        public virtual string NUBAN { get; set; }
        public virtual string StandardAccountNumber { get; set; }
        public virtual string CustomerID { get; set; }
        public virtual long AgentID { get; set; }
        public virtual string AgentName { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual string AccountOfficerCode { get; set; }
        public virtual bool IsImageSync { get; set; }
        public virtual bool IsSmsSent { get; set; }
    }
}
