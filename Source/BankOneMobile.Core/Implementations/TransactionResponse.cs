﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
   public  class TransactionResponse
    {
       /// <summary>
       /// The status of the transaction
       /// </summary>
       [DataMember]
       public virtual string  Status { get; set; }
       /// <summary>
       ///The response gotten 
       /// </summary>
       [DataMember]
       public virtual string ResponseDescription { get; set; }
       /// <summary>
       ///Descripton  of the  Response gotten
       /// </summary>
       [DataMember]
       public virtual string ResponseMessage { get; set; }
       [DataMember]
       public virtual string STAN { get; set; }
       public virtual TransactionStatus TransactionStatus { get; set; }
       
    }
}
