﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class client{
        public string accountNumber{get;set;}
        public DateTime birthDate{get;set;}
        public address businessAddress { get; set; }
        public string businessDescription { get; set; }
        public string bvn { get; set; }
        public int creditScore { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string gender { get; set; }
        public address homeAddress { get; set; }
        public identification identification { get; set; }
        public string lastName { get; set; }
        public string maritalStatus { get; set; }
        public string mobileNumber1 { get; set; }
        public nextOfKin nextOfKin { get; set; }
    }

    public class nextOfKin{
        public string name{get;set;}
        public string phoneNumber{get;set;}
    }

    public class accountStatement{
        public decimal amount{get;set;}
        public string narration{get;set;}
        public DateTime transactionDate{get;set;}
    }

    public class identification{
        public string idNumber{get;set;}
        public string idType{get;set;}
    }
    public class address{
        public string city {get;set;}
        public string country {get;set;}
        public string line1 {get;set;}
        public string line2 {get;set;}
        public string postalCode {get;set;}
        public string state {get;set;}
    }
    public class OneCreditLoanOrigination
    {
        public long amount { get; set; }
        public client client { get; set; }
        public List<accountStatement> statements { get; set; }
        
    }
}
