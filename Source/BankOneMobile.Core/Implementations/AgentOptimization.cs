﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public abstract class GenericAgentOptimization : Entity
    {
        public virtual string InstitutionCode { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string AgentName { get; set; }
        public virtual string Branch { get; set; }
        public virtual long BranchID { get; set; }
        public virtual int CdiIndex { get; set; }
        public virtual double ShortestAgentPath { get; set; }
        public virtual double OptimizedTotalClusterDistance { get; set; }
        public virtual DateTime DateGenerated { get; set; }
    }
    public class AgentOptimization : GenericAgentOptimization, IAgentOptimization
    {
        public AgentOptimization()
        {
            DateGenerated= new DateTime(1900, 1, 1);
        }
        
    }

    public class AgentOptimizationArchive : GenericAgentOptimization
    {

    }

}
