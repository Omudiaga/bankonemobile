﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Core.Implementations
{
    public class PinChangeTransactionType : EntityWithEnableDisable, IPinChangeTransactionType
    {

        public virtual ITransaction TheTransaction { get; set; }
        public virtual IMobileAccount TheMobileAccount { get; set; }
        public virtual string  OldPIN { get; set; }
        public virtual string  NewPIN { get; set; }
        public virtual byte[] EncryptedPin { get; set; }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }

    }
}
