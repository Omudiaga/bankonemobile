﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Attributes;

namespace BankOneMobile.Core.Implementations
{
    public class CommercialBank : EntityWithEnableDisable, ICommercialBank
    {
        [CustomerIDAttribute]
      public virtual   string Code { set; get; }
      public virtual string Name { set; get; }
      public virtual string ShortName { set; get; }


      public override string ToString()
      {
          return ShortName;
      }
     
     
    }
}
