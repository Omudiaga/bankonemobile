﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using System.Runtime.Serialization;
using PANE.Framework.AuditTrail.Attributes;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [Trailable]
    [Serializable]
    public class Agent : EntityWithEnableDisable, IAgent
    {       
               [DataMember]
      public virtual   string Code { set; get; }
        [DataMember]
        public virtual bool IsMobileTeller{get;set;}
      public virtual IMobileAccount MobileAccount { get; set; }
        [DataMember]
      public virtual string InstitutionCode { get; set; }
        [DataMember]
     public virtual  string LastName { get; set; }
        [DataMember]
     public virtual  string OtherNames { get; set; }
        [DataMember]
     public virtual string AddressLocation { get; set; }
        [DataMember]
     public virtual string PhoneNumber { get; set; }// Just to get the number from the interface
        [TrailableIgnore]
        //[System.Xml.Serialization.XmlIgnore]
     public virtual ILinkingBankAccount TheAgentAccount { get; set; }
        [DataMember]
        [TrailableIgnore]
     public virtual bool Linked { get; set; }
        [DataMember]
        [TrailableIgnore]
     public virtual string AccountNumber { get; set; }
     // public virtual IList<ILinkingCard> Cards { set; get; }
        [DataMember]
        public virtual string TheAgentAccountNumber
        {
            get
            {
                return TheAgentAccount == null ? null : TheAgentAccount.BankAccount;
            }
            set
            {
            }
        }
       public virtual string InstitutionName { get; set; }
       public virtual  string ProductName { get; set; }
       public virtual  string ProductCode { get; set; }
       public virtual Gender TheGender { get; set; }
       public virtual bool ActivateViaJava { get; set; }
       public virtual bool ActivateViaUSSD { get; set; }
       public virtual string PersonalPhoneNumber { get; set; }
       public virtual decimal PostingLimit { get; set; }
       public virtual long BranchID { get; set; }
             
     #region IAgent Members


    

     #endregion
    }
}
