﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class DBNFundsTransferTransactionType : EntityWithEnableDisable, IDBNFundsTransferTransactionType
    {
        public DBNFundsTransferTransactionType()
        {
            RequestDate = ResponseDate = new DateTime(1900, 1, 1);
        }
        public virtual string SessionID { get; set; }
        public virtual string NameEnquirySessionID { get; set; }
        public virtual string DestinationInstitutionCode { get; set; }
        public virtual string ChannelCode { get; set; }
        public virtual string BeneficiaryAccountName { get; set; }
        public virtual string BeneficiaryAccountNumber { get; set; }
        public virtual string BeneficiaryKYCLevel { get; set; }
        public virtual string BeneficiaryBankVerificationNumber { get; set; }
        public virtual string OriginatorAccountName { get; set; }
        public virtual string OriginatorAccountNumber { get; set; }
        public virtual string OriginatorBankVerificationNumber { get; set; }
        public virtual string OriginatorKYCLevel { get; set; }
        public virtual string TransactionLocation { get; set; }
        public virtual string Narration { get; set; }
        public virtual string PaymentReference { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal Fee { get; set; }
        public virtual int VATInPercentage { get; set; }
        public virtual string ResponseCode { get; set; }
        public virtual DBNReversalStatus ReversalStatus { get; set; }
        public virtual DateTime RequestDate { get; set; }
        public virtual DateTime ResponseDate { get; set; }
        public virtual DBNFundsTransferTransactionTypeName DBNFundsTransferType { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual bool IsCustomerSending { get; set; }
        public virtual DBNNonAccountCashCollectionStatus StatusMessage { get; set; }

        public virtual FundsTransferStatus TransferStatus { get; set; }
        public virtual ITransaction TheTransaction { get; set; }
        public virtual  IFeeTransactionType TheFee
        {
            get;
            set;
        }


      
    }
}
