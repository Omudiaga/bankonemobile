﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class CustomerGeoLocation : Entity, ICustomerGeoLocation
    {
        public CustomerGeoLocation()
        {
            DateLogged= new DateTime(1900, 1, 1);
        }
        public virtual string InstitutionCode { get; set; }
        public virtual string CustomerPhoneNumber { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }
        public virtual DateTime DateLogged { get; set; }
    }
}
