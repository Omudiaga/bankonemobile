﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class CommercialBankFundsTransferTransactionType : EntityWithEnableDisable, ICommercialBankFundsTransferTransactionType
    {
       public  virtual string AgentCode { get; set; }
       public virtual string Token { get; set; }
       public virtual string BankName { get; set; }
       public virtual string BankCode { get; set; }
       public virtual string AccountNumber { get; set; }
       public virtual ITransaction TheTransaction { get; set; }
       public virtual decimal Amount { get; set; }
       public virtual IMobileAccount TheMobileAccount { get; set; }
       public virtual string TransactionReference { get; set; }
       public virtual string PaymentReference { get; set; }
       public virtual string BeneficiaryLastName { get; set; }
       public virtual string BeneficiaryOtherNames { get; set; }
       public virtual string CoreBankingSTAN
       {
           get;
           set;
       }

       public virtual  string ISWSTAN
       {
           get;
           set;
       }

       public virtual IFeeTransactionType TheFee
       {
           get;
           set;
       }
       public virtual AccountMode TheAccountType { get; set; }


      


       public virtual  string BeneficiaryPhoneNumber
       {
           get;
           set;
       }

       public virtual LinkingBankAccount TheBankAccount { get; set; }
       public virtual long TransactionID
       {
           get;
           set;
       }
       public virtual DateTime TransactionDate
       {
           get;
           set;
       }
      public virtual string TransactionStatus
       {
           get;
           set;
       }
     public virtual  string TransactionDetails
       {
           get;
           set;
       }
      public virtual decimal TransactionAmount
       {
           get;
           set;
       }

      public virtual string MobilePhone
      {
          get;
          set;
      }
      public virtual string Institution
      {
          get;
          set;
      }
    }

    public class InterbankTransaction :  ICommercialBankFundsTransferTransactionType
    {
        public virtual string AgentCode { get; set; }
        public virtual string Token { get; set; }
        public virtual string BankName { get; set; }
        public virtual string BankCode { get; set; }
        public virtual string AccountNumber { get; set; }        
        public virtual decimal Amount { get; set; }        
        public virtual string TransactionReference { get; set; }
        public virtual string PaymentReference { get; set; }
        public virtual string BeneficiaryLastName { get; set; }
        public virtual string BeneficiaryOtherNames { get; set; }
        public virtual string CoreBankingSTAN
        {
            get;
            set;
        }
        public virtual string ISWSTAN
        {
            get;
            set;
        }
        public virtual IFeeTransactionType TheFee
        {
            get;
            set;
        }
        public virtual AccountMode TheAccountType { get; set; }
        public virtual string BeneficiaryPhoneNumber
        {
            get;
            set;
        }
        public virtual string BankAccountNumber { get; set; }

        public virtual long TransactionID
        {
            get;
            set;
        }
        public virtual DateTime TransactionDate
        {
            get;
            set;
        }
        public virtual string TransactionStatus
        {
            get;
            set;
        }
        public virtual string TransactionDetails
        {
            get;
            set;
        }
        public virtual decimal TransactionAmount
        {
            get;
            set;
        }
        public virtual string MobilePhone
        {
            get;
            set;
        }
        public virtual string Institution
        {
            get;
            set;
        }


        public IMobileAccount TheMobileAccount
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public LinkingBankAccount TheBankAccount
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public ITransaction TheTransaction
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public long ID
        {
            get;
            set;
        }

        public bool IsActive
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string DisplayMessage
        {
            get
            {
                return string.Empty;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
