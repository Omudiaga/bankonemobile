﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class OfflineDepositTransactionType : Entity, IOfflineDepositTransactionType
    {
        public OfflineDepositTransactionType()
        {
            DateLogged= new DateTime(1900, 1, 1);
        }
        public virtual string InstitutionCode { get; set; }
        public virtual string SessionID { get; set; }
        public virtual string AgentPhoneNumber { get; set; }
        public virtual string CustomerPhoneNumber { get; set; }
        public virtual string Amount { get; set; }
        public virtual DateTime DateLogged { get; set; }
        public virtual TransactionStatus Status { get; set; }
    }
}
