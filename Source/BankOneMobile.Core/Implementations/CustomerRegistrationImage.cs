﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
    public class CustomerRegistrationImage : Entity, ICustomerRegistrationImage
    {
        public virtual byte[] Passport { get; set; }
    }
}
