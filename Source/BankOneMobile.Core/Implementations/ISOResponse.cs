﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Implementations
{
   public  class ISOResponse
    {
       /// <summary>
       /// The status of the transaction
       /// </summary>
       public virtual TransactionStatus Status { get; set; }
       /// <summary>
       ///The response gotten 
       /// </summary>
       public virtual string ResponseDescription { get; set; }
       /// <summary>
       ///Descripton  of the  Response gotten
       /// </summary>
       public virtual string ResponseMessage { get; set; }
       
    }
}
