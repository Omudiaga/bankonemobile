﻿using BankOneMobile.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Implementations
{
    public class CustomerCreationLog : Entity, ICustomerCreationLog
    {
        public virtual string CustomerName { set; get; }
        public virtual string PhoneNumber { set; get; }
        public virtual string Institution { set; get; }
        public virtual string State { set; get; }
        public virtual string AccountNumber { set; get; }
    }
}
