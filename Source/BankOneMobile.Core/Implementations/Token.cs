﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;
using System.Runtime.Serialization;
using PANE.Framework.AuditTrail.Attributes;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.Core.Implementations
{
    [DataContract]
    [Trailable]
    [Serializable]
    public class Token : EntityWithEnableDisable, IToken
    {
      public virtual string TokenValue { set; get; }
      public virtual IInstitution TheInstitution { get; set; }
      public virtual DateTime DateGenerated { get; set; }
      public virtual DateTime ExpiryDate
        {
            get
            {
                if (TheInstitution == null || TheInstitution.TokenLifeSpan == null)
                {
                    throw new InvalidInstitutionException("Token has no Institution");
                }
                return DateGenerated.Add(TimeSpan.Parse(TheInstitution.TokenLifeSpan));
            }
        }
      public virtual bool IsExpired
        {
            get
            {
                try
                {
                    return DateTime.Now > ExpiryDate;

                }
                catch
                {
                    return false;
                }

            }
        }
      public virtual bool IsConsumed { get; set; }
      public virtual DateTime DateConsumed { get; set; }
      public virtual string PhoneNumber { get; set; }
      public virtual IMobileAccount TheMobileAccount { get; set; }
    }
}
