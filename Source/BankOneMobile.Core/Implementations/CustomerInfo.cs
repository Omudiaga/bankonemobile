﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Contracts;


namespace BankOneMobile.Core.Implementations
{
    public class CustomerInfo : ICustomerInfo
    {
      public string CustomerID { set; get; }
      public  string LastName { set; get; }
      public string OtherNames { set; get; }
      public string Address { set; get; }
      public string EmailAddress { set; get; }
      public string Sex { set; get; }
      public DateTime DOB { set; get; }
      



      


      public ICustomerInfo CreateCustomerInfo()
      {
          CustomerInfo toReturn = new CustomerInfo();
          toReturn.LastName = GenerateRandomName(false);
          toReturn.OtherNames = GenerateRandomName(true);


          return toReturn;
      }

      public string GenerateRandomName(bool firstname)
      {
          string toReturn = string.Empty;


          List<string> FirstNames = new List<string>()
          {
     "Sola","Moses","Kemi","Remi", "Grace","Jake", "Hannah","Bobo", "Farouk","Hassan", "Chucks", "Vicky"
          };

          List<string> LastNamesA = new List<string>()
          {
     "Okoro","Ahmed","Rasmus","White", "Rilwan","Jaja", "Bernard","Graham", "Dimas","abdulahi", "baba", "John"
          };

          List<string> LastNamesB = new List<string>()
          {
     "Solomon","danko","Hufe","Brown", "Cole","Andonni", "Uche","Green", "Jonah","Elelewon", "Remington", "Bassey"
          };

    var permutations = new List<Tuple<int, int, int>>();
    

    Random random = new Random();
    int a, b, c;

    
        a = random.Next(0, FirstNames.Count);
        b = random.Next(0, FirstNames.Count);
        c = random.Next(0, FirstNames.Count);







        toReturn = firstname ? FirstNames[a]:LastNamesB[c];
        
    

    

          return toReturn;
      }
    }
}
