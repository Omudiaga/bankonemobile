﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers
{
    public static class ObjectSerialization
    {
        public static Dictionary<long, string> FromJsonToDictionary(this string json)
        {
            string[] keyValueArray = json.Replace("{", string.Empty).Replace("}", string.Empty).Replace("\"", string.Empty).Split(',');
            Dictionary<long, string> result = keyValueArray.ToDictionary(item => long.Parse(item.Split(':')[0]), item => item.Split(':')[1]);
            return result;
        }
    }
}
