using System;
using System.Diagnostics;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Implementations;
using System.Text;

namespace BankOneMobile.Core.Helpers.Utility
{
    public class Utility
    {
        public static string PluralOf(string text)
        {
            var pluralString = text;
            var lastCharacter = pluralString.Substring(pluralString.Length - 1).ToLower();

            // y�s become ies (such as Category to Categories)
            if (string.Equals(lastCharacter, "y", StringComparison.InvariantCultureIgnoreCase) && !pluralString.EndsWith("ay", StringComparison.InvariantCultureIgnoreCase))
            {
                pluralString = pluralString.Remove(pluralString.Length - 1);
                pluralString += "ie";
            }

            // ch�s become ches (such as Pirch to Pirches)
            if (string.Equals(pluralString.Substring(pluralString.Length - 2), "ch",
                              StringComparison.InvariantCultureIgnoreCase))
            {
                pluralString += "e";
            }

            switch (lastCharacter)
            {
                case "s":
                    return pluralString + "es";

                default:
                    return pluralString + "s";

            }

        }

        public  static string SpaceWords(string words)
        {
            StringBuilder result = new StringBuilder();
            foreach (char letter in words.ToCharArray())
            {
                if (char.IsUpper(letter))
                {
                    result.Append(' ');
                }
                result.Append(letter);
            }
            return result.Replace('_', ' ').ToString();
        } 

        
        
    }
}
