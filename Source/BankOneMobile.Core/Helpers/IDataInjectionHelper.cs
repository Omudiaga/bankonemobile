﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers
{
    public interface IDataInjectionHelper
    {
        BankOneMobile.Core.Helpers.Enums.AccountNumberType GetInstitutionAccountNumberType(string institutionCode);
    }
}
