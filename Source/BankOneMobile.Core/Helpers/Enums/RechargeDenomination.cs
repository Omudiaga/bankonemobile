﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum RechargeDenomination
    {
        OneHundred=100,
        TwoFifty=150,
        ThreeHundred=200,
        FourHundred=400,
        FiveHundred =500,
        SixHundred=750,
        SevenFifity =1000,
        EightHundred=1500,
        Others=2000
            
    }
}
