using System;
using System.Collections.Generic;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum MerchantType
    {
        
        NullMerchantType=0000,
        Recharge=001,
        BillsPayment=002,
        CustomerCommercialBankTransfer=501,
        CustomerCommercialBankTransferCompletionCom= 503,
        CustomerNonCustomerTransfer=502,
        NonCustomerCommercialBankTransfer=511,
        NonCustomerNonCustomerTransfer=512,
        MobileTellerCashIn=211,
        MobileTellerCashOut=011,
        
        MobileTellerCustomerNonCustomerTransfer = 504,

        ServiceAccounTransferToCustomer = 503,
        RechargeCompletion=003,
        BillsPaymentCompletion=004

    }
}
