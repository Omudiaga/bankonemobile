﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Helpers.Enums
{
    [DataContract]
    public enum UpdateType
    {
        NormalUpdate=1,
        HotList=2,
        PinReset=3,
        PinTryReset =4
        
    }
}
