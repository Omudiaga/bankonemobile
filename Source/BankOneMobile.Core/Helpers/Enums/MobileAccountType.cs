﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Helpers.Enums
{
    [DataContract]
    public enum MobileAccountType
    {
        Customer=1,
        Agent=2,
        MobileTeller=3,
        
    }
}
