﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BankOneMobile.Core.Helpers.Enums
{
    [DataContract]
    public enum Gender
    {
        Male=1,
        Female=2,
        
    }
    [DataContract]
    public enum AccountSource
    {

        BankBranch = 1,
        FriendOrFamily = 2,
        MarketStorm=3,
        Others=4,
        RoadShow=5,
        TownHall=6

    }

    [DataContract]
    public enum RegistrationStatus
    {
        Pending = 1,
        Successful = 2,
        Failed = 3
    }
    [DataContract]
    public enum CommitPoint
    {
        CoreBanking = 10,
        BankOneMobile = 20,
        CardManagement = 30
    }
}
