﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum ResponseCodes
    {
        ApprovedOrCompletedSuccessfully = 00,
        StatusUnknown, PleaseWaitForSettlementReport = 01,
        InvalidSender = 03,
        DoNotHonor = 05,
        DormantAccount = 06,
        InvalidAccount = 07,
        AccountNameMismatch = 08,
        RequestProcessingInProgress = 09,
        InvalidTransaction = 12,
        InvalidAmount = 13,
        InvalidBatchNumber = 14,
        InvalidSessionOrRecordId = 15,
        UnknownBankCode = 16,
        InvalidChannel = 17,
        WrongMethodCall = 18,
        NoActionTaken = 21,
        UnableToLocateRecord = 25,
        DuplicateRecord = 26,
        FormatError = 30,
        SuspectedFraud = 34,
        ContactSendingBank = 35,
        NoSufficientFunds = 51,
        TransactionNotPermittedToSender = 57,
        TransactionNotPermittedOnChannel = 58,
        TransferLimitExceeded = 61,
        SecurityViolation = 63,
        ExceedsWithdrawalFrequency = 65,
        ResponseReceivedTooLate = 68,
        UnsuccessfulAccountOrAmountBlock = 69,
        UnsuccessfulAccountOrAmountUnblock = 70,
        EmptyMandateReferenceNumber = 71,
        BeneficiaryBankNotAvailable = 91,
        RoutingError = 92,
        DuplicateTransaction = 94,
        SystemMalfunction = 96,
        TimeoutWaitingForResponseFromDestination = 97
    }
}
