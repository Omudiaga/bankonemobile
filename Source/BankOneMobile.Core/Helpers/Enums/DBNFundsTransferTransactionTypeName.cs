﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum DBNFundsTransferTransactionTypeName 
    {
        BetaToBeta=1,
        BetaToDiamond =2,
        BetaToOtherBanks=3,
        NonAccountHolder=4
    }

    public enum FundsTransferStatus
    {
        Pending = 1,
        Successful = 2,
        Failed = 3,
    }

    public enum DBNReversalStatus
    {
        NotApplicable = 1,
        Successful = 2,
        NotSuccessful = 3
    }

    public enum DBNNonAccountCashCollectionStatus
    {
        Successful = 1,
        NotSuccessful = 2
    }
}
