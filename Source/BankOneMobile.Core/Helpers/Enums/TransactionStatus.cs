﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Helpers.Enums
{
    public enum TransactionStatus 
    {
        Pending =1,
        Failed=2,
        Successful=3,
        Reversed=4,
        FailedNotReversed=5
    }
}
