﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IOneCreditLoanRequest : IEntity
    {
        string InstitutionCode { get; set; }
        string AgentPhoneNumber { get; set; }
        string CustomerPhoneNumber { get; set; }
        string JsonRequest { get; set; }
        string Response { get; set; }
        DateTime DateLogged { get; set; }
        DateTime? DateSent { get; set; }
        RegistrationStatus Status { get; set; }
        string StatusMessage { get; set; }
    }
}
