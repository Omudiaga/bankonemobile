﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ILocalFundsTransferTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string AgentCode { get; set; }
        decimal Amount { get; set; }
        bool isPhoneNumber { get; set; }
        string ToAccount { get; set; }
        string ToInstCode { get; set; }
        string ReceivingAccountName { get; set; }

        
    }
}
