﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    /// <summary>
    /// Defines Basic Properties of an Entity
    /// </summary>
    public interface IEntity : IEntity<long>
    {

    }

    /// <summary>
    /// Defines Basic Properties of an Entity ID
    /// </summary>
    /// <typeparam name="idT"></typeparam>
    public interface IEntity<idT>
    {
        idT ID
        {
            get;
            set;
        }

       
    }
}
