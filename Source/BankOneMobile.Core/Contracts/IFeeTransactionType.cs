﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IFeeTransactionType : IEntity , IEnableDisable
    {
        string TransactionTypeName { set; get; }

        bool FlatOrPercent { get; set; }

        decimal Amount { get; set; }
    }
}
