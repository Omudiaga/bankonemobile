﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface ICustomerRegistrationImage : IEntity
    {
        byte[] Passport { get; set; }

    }
}
