﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ITargetSavingsDepositTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string TargetSavingsAccountNumber { get; set; }
        string BetaAccountNumber { get; set; }
        decimal Amount { get; set; }
        string AgentCode { get; set; }
        DateTime Date { get; set; }
        LinkingBankAccount TheBankAccount { get; set; }
        
    }
}
