﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IAgentOptimization : IEntity
    {
        string InstitutionCode { get; set; }
        string AgentPhoneNumber { get; set; }
        long BranchID { get; set; }
        int CdiIndex { get; set; }
        double ShortestAgentPath { get; set; }
        double OptimizedTotalClusterDistance { get; set; }
        DateTime DateGenerated { get; set; }
    }
}
