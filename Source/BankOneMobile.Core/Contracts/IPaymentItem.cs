﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Attributes;

namespace BankOneMobile.Core.Contracts
{
    public interface IPaymentItem : IEntity, IEnableDisable
    {
        string Code { set; get; }
        string Name { get; set; }
        IMerchant TheMerchant { get; set; }
        [AmountAttribute]
        decimal UnitPrice { get; set; }
        [CustomerIDAttribute]
         string CustomerIDName { get; set; }

        

    }
}
