﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
        public interface ICoreDAO<T, idT> : IRepositoryWithTypedId<T, idT> where T : class, IEntity<idT>
        {
            string InstitutionCode { get; set; }
            void Flush();
            void SqlBulkInsert(IList<T> items, string tableName, bool closeConnection = false);
            void SqlBulkRemove(IList<T> items, string tableName);
            //void CloseSimpleSession();
        }

        public interface ICoreDAO<T> : ICoreDAO<T, long> where T : class, IEntity
        {

        }
    }
