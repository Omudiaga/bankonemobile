﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IDBNFundsTransferTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string SessionID { get; set; }
        string NameEnquirySessionID { get; set; }
        string DestinationInstitutionCode { get; set; }
        string ChannelCode { get; set; }
        string BeneficiaryAccountName { get; set; }
        string BeneficiaryAccountNumber { get; set; }
        string BeneficiaryKYCLevel { get; set; }
        string BeneficiaryBankVerificationNumber { get; set; }
        string OriginatorAccountName { get; set; }
        string OriginatorAccountNumber { get; set; }
        string OriginatorBankVerificationNumber { get; set; }
        string OriginatorKYCLevel { get; set; }
        string TransactionLocation { get; set; }
        string Narration { get; set; }
        string PaymentReference { get; set; }
        decimal Amount { get; set; }
        decimal Fee { get; set; }
        int VATInPercentage { get; set; }
        string ResponseCode { get; set; }
        DBNReversalStatus ReversalStatus { get; set; }
        DateTime RequestDate { get; set; }
        DateTime ResponseDate { get; set; }
        string AgentPhoneNumber { get; set; }
        bool IsCustomerSending { get; set; }
        DBNNonAccountCashCollectionStatus StatusMessage { get; set; }
        DBNFundsTransferTransactionTypeName DBNFundsTransferType { get; set; }
        FundsTransferStatus TransferStatus { get; set; }
        
    }
}
