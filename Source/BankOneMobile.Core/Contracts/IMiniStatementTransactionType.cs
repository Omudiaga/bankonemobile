﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface IMiniStatementTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        
        IMobileAccount TheMobileAccount { get; set; }
        IList<TransactionHistory> TheTransactions { get; set; }
        bool IsForThirdParty { get; set; }
        
    }
}
