﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface IBillsPaymentTransactionType :ITransactionType,  IEntity, IEnableDisable
    {
        
        string GoodsDescription { get; set; }
        string PaymentReference { get; set; }
        string TransactionReference { get; set; }
        string CustomerID { get; set; }
        string ValuePurchased { get; set; }
        IMerchant TheMerchant { get; set; }
        string MerchantName { get; set; }
        string MerchantCode { get; set; }
        string PaymentItemName { get; set; }
        string PaymentItemCode { get; set; }
        string CoreBankingSTAN { get; set; }
        string ISWSTAN { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
        IPaymentItem ThePaymentItem { get; set; }
        decimal Amount { get; set; }

         LinkingBankAccount TheBankAccount { get; set; }
        

        
        
    }
}
