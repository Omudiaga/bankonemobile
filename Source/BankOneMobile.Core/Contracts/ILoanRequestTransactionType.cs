﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface ILoanRequestTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string LoanProductID { get; set; }
        string LoanAmount{get;set;}
        string Tenure{get;set;}
        string BetaFriendPerception { get; set; }
        string InstitutionCode { get; set; }
        string PhoneNumber{get;set;}
        string AccountNumber { get; set; }
        ITransaction TheTransaction { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
        DateTime Date { get; set; }
        IFeeTransactionType TheFee
        {
            get;
            set;
        }
        
    }
}
