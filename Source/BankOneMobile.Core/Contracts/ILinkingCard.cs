﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface ILinkingCard : IEntity
    {
        string InstitutionCode { set; get; }
        IMobileAccount TheMobileAccount { set; get; }
        string CardPAN { set; get; }
        DateTime ExpiryDate { set; get; }
    }
}
