﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface ISystemConfig :IEntity
    {
        /// <summary>
        /// The lenght of a session in seconds
        /// </summary>
          long SessionLength { get; set; }
        /// <summary>
        /// The Amount to be billed per session
        /// </summary>
          decimal AmountPerSession { get; set; }
          int MaximumPinTries { get; set; }

           string PEK { get; set; }
           string PVK { get; set; }
           string TMK { get; set; }
        

    }
}
