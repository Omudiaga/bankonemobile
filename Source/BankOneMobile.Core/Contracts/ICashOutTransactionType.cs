﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ICashOutTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        string AgentCode { get; set; }
        string Token { get; set; }
        string WithdrawalLocation { get; set; }
        decimal Amount { get; set; }
        string CustomerAccount { get; set; }
        string CustomerPhoneNumber { get; set; }
        LinkingBankAccount TheBankAccount { get; set; }
        
    }
}
