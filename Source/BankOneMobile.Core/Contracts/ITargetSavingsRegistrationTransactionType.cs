﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface ITargetSavingsRegistrationTransactionType : ITransactionType, IEntity, IEnableDisable
    {

        string AccountNumber { get; set; }
        string Frequency { get; set; }
        string Amount { get; set; }
        decimal TotalCommitmentAmount { get; set; }
        string Tenure { get; set; }
        Int32 TargetSavingsProductTenure { get; set; }
        string PhoneNumber { get; set; }
        string StandingOrder { get; set; }
        bool IsReapplying { get; set; }
        string TargetSavingsAccountNumberForReactivation { get; set; }
        Product SelectedProduct { get; set; }
        DateTime Date { get; set; }
        IMobileAccount TheMobileAccount { get; set; }
    }
}
