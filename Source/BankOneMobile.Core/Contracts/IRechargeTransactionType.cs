﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IRechargeTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        IMerchant TheMerchant { get; set; }
        string MerchantName { get; set; }
        string MerchantCode { get; set; }
        string PaymentItemCode { get; set; }
        RechargeType RechargeType {get; set;}
        IMobileAccount TheMobileAccount {get; set; }
        string Beneficiary {get; set;}
        decimal Amount { get; set; }
        string TransactionReference { get; set; }
        string PaymentReference { get; set; }
        string RechargeCode { get; set; }
        string CoreBankingSTAN { get; set; }
        string ISWSTAN { get; set; }
        LinkingBankAccount TheBankAccount { get; set; }
    }
}
