﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public  interface IProduct : IEntity , IEnableDisable
    {
        string Code { set; get; }
        string Name { get; set; }
        string InstitutionCode { get; set; }
        Int32 Tenure { get; set; }
      

    }
}
