﻿using BankOneMobile.Core.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface IFundsTransferFee : IEntity, IEnableDisable
    {
        string InstitutionCode { set; get; }
        TransactionTypeName TransactionTypeName { set; get; }
        /// <summary>
        /// True for Flat, False for Percent
        /// </summary>
        bool FlatOrPercent { get; set; }
        decimal Amount { get; set; }
        int VATInPercentage { set; get; }
    }
}
