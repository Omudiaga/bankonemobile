﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface IMobileRequest
    {
        string SessionID { set; get; }
        DateTime RequestTime { set; get; }
        DateTime? ResponseTime { set; get; }
        string Operation { set; get; }
         string RequestResponse { set; get; }
         string Request { set; get; }
    }
}
