﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IOfflineDepositTransactionType : IEntity
    {
        string InstitutionCode { get; set; }
        string SessionID { get; set; }
        string AgentPhoneNumber { get; set; }
        string CustomerPhoneNumber { get; set; }
        string Amount { get; set; }
        DateTime DateLogged { get; set; }
        TransactionStatus Status { get; set; }
    }
}
