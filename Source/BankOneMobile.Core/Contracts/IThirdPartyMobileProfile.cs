﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOneMobile.Core.Contracts
{
    public interface IThirdPartyMobileProfile : IEnableDisable
    {
         string PhoneNumber { get; set; }
         string AccountNumber { get; set; }
         string LastName { get; set; }
         string OtherName { get; set; }
         string Gender { get; set; }
         string Status { get; set; }
        string InstitutionCode { get; set; }


    }
}
