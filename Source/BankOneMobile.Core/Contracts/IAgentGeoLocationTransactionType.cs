﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface IAgentGeoLocationTransactionType : IEntity
    {
        string InstitutionCode { get; set; }
        string AgentPhoneNumber { get; set; }
        string CustomerPhoneNumber { get; set; }
        TransactionTypeName TransactionType { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        DateTime DateLogged { get; set; }  
    }
}
