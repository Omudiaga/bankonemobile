﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Core.Contracts
{
    public interface IPinChangeTransactionType : ITransactionType, IEntity, IEnableDisable
    {
        IMobileAccount TheMobileAccount { get; set; }
        string  OldPIN { get; set; }
        string  NewPIN { get; set; }
        byte[] EncryptedPin { get; set; }
        
    }
}
