﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Helpers.Enums;

namespace BankOneMobile.Core.Contracts
{
    public interface ICustomerRegistration : IEntity, IEnableDisable
    {
        string InstitutionCode { get; set; }
        string InstitutionName { get; set; }
        string ProductCode { get; set; }
        string ProductName { get; set; }
        long PassportID { get; set; }
        Gender TheGender { get; set; }
        string PhoneNumber { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string NOKName { get; set; }
        string NOKPhone { get; set; }
        DateTime DateOfBirth { get; set; }
        string Address { get; set; }
        string PlaceOfBirth { get; set; }
        string CardSerialNumber { get; set; }          
        string ActivationCode{get;set;}        
        DateTime Date { get; set; }
        RegistrationStatus Status { get; set; }
        CommitPoint CommitPoint { get; set; }
        string ErrorInfo { get; set; }
        string NUBAN { get; set; }
        string StandardAccountNumber { get; set; }
        string CustomerID { get; set; }
        long AgentID { get; set; }
        string AgentName { get; set; }
        string AgentCode { get; set; }
        string AccountOfficerCode { get; set; }
        bool IsImageSync { get; set; }
        bool IsSmsSent { get; set; }

    }
}
