﻿using BankOneMobile.Core.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankoneMobile.AdvansLafayette.Core.Implementation
{
    public class IDType : EntityWithEnableDisable,IIDType
    {
       public virtual string Name { get; set;}
       public virtual  string Code {get;set;}
    }
}                                    
