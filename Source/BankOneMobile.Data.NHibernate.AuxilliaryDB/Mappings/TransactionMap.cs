﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;

namespace BankOneMobile.Data.NHibernate.AuxilliaryDB.Mappings
{
    public class TransactionMap : EntityWithEnableDisableMap<Transaction>
    {
        public TransactionMap()
        {
            Table("MobileTransactions");
            Map(x => x.Amount);
            Map(x => x.From,"FromValue");
            Map(x => x.To,"ToValue");
            Map(x => x.Status);
            
            Map(x => x.FromPhoneNumber);
            Map(x => x.TransactionTypeID);
            Map(x => x.TransactionTypeName);
          //  Map(x => x.Date);
            References<Session>(x => x.Session).Cascade.All();
          
          //  HasMany<LinkingCard>(x => x.Cards);
        }
    }
}
