﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Data.NHibernate.Mappings;
using BankOneMobile.Core.Implementations;

namespace BankOneMobile.Data.NHibernate.AuxilliaryDB.Mappings
{
    public class FlowMap : EntityWithEnableDisableMap<Flow>
    {
        public FlowMap()
        {

            Map(x => x.Name);
            Map(x => x.Description);
            Map(x => x.IsMainFlow);
            Map(x => x.IsAgent);
            Map(x => x.XamlDefinition);
            Map(x => x.UniqueId);
            Map(x => x.IsAccountActivation);
        }
    }
}
