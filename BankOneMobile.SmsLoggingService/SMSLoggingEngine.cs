﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Diagnostics;
using BankOneMobile.Core;
using BankOneMobile.Data;
using BankOneMobile.Services;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Contracts;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Helpers;
using BankOneMobile.Data.Implementations;
using BankOneMobile.Core.Exceptions;

namespace BankOneMobile.SmsLogging.WindowsService
{
    public class SMSLoggingEngine
    {
        private Timer timer = null;
        private bool isBusy = false;
        public SMSLoggingEngine()
        {
            System.Threading.Thread thWorker = new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate
                {
                    try
                    {
                        isBusy = true;
                        System.Diagnostics.Trace.TraceInformation("Trace Just Began");
                        //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Trace Just Began"));

                        BankOneMobile.Extension.ServiceLocator ssl = new BankOneMobile.Extension.ServiceLocator();
                        // For ManagedSeervices BankOneMobile, Comment out all switch-related initialisations.  AND CALL InitWeb() INSTEAD OF Init()
                        //AppZoneSwitch.Extension.ServiceLocator ssl = new AppZoneSwitch.Extension.ServiceLocator();
                        ssl.Register<AppZoneSwitch.Core.Contracts.INodeFactory>(() => new AppZoneSwitch.Core.Implementations.Factories.NodeFactory());
                        ssl.Register<AppZoneSwitch.Core.Contracts.ITransaction>(() => new AppZoneSwitch.Core.Implementations.Transaction());
                        ssl.Register<AppZoneSwitch.Services.Contracts.ITransactionProcessor>(() => new AppZoneSwitch.Services.Implementations.TransactionProcessor());
                        ssl.Register<AppZoneSwitch.Services.Contracts.IReversalProcessor>(() => new AppZoneSwitch.Services.Implementations.ReversalProcessor());

                        System.Diagnostics.Trace.TraceInformation("Initialised Properties");
                        //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Initialised Properties"));

                        ssl.Register<AppZoneUI.Framework.ITextBox>(() => new AppZoneUI.Framework.TextBox() as AppZoneUI.Framework.ITextBox);
                        ssl.Register<AppZoneUI.Framework.ITextArea>(() => new AppZoneUI.Framework.TextArea() as AppZoneUI.Framework.ITextArea);
                        ssl.Register<AppZoneUI.Framework.ITextLabel>(() => new AppZoneUI.Framework.TextLabel() as AppZoneUI.Framework.ITextLabel);
                        ssl.Register<AppZoneUI.Framework.ICheckBox>(() => new AppZoneUI.Framework.CheckBox() as AppZoneUI.Framework.ICheckBox);
                        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
                        System.Diagnostics.Trace.TraceInformation("UI Okay abt to Init");
                        //PANE.ERRORLOG.ErrorLogger.Log(new Exception("UI Okay abt to Init"));
                        BankOneMobile.Extension.ApplicationInitializer.Init();

                        System.Diagnostics.Trace.TraceInformation("Init Succesfull");
                        //PANE.ERRORLOG.ErrorLogger.Log(new Exception("Init Succesfull"));
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation("Error Main:{0}, Inner{1}", ex.Message, ex.InnerException == null ? "None" : ex.InnerException.Message);
                        throw;
                    }
                    finally
                    {
                        //System.Diagnostics.Trace.TraceInformation("Now setting busy to false");
                        isBusy = false;
                    }
                }
                ));
            thWorker.Start();
            this.timer = new Timer();
            this.timer.Elapsed += new ElapsedEventHandler(this.timer_Elapsed);

            //System.Diagnostics.Trace.TraceInformation("Just added timer event");
        }



        public void Start()
        {
            //System.Diagnostics.Trace.TraceInformation("About to start timer");
            this.timer.Start();
            //System.Diagnostics.Trace.TraceInformation("Timer started");
        }
        public void Stop()
        {
            if (timer != null)
            {
                this.timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //System.Diagnostics.Trace.TraceInformation(string.Format("Timer just elapsed. Is busy is: {0}", isBusy));
            if (!isBusy)
            {
                this.timer.Stop();
                try
                {
                    try
                    {
                        #region Account creation SMS Logging
                        System.Diagnostics.Trace.TraceInformation(String.Format("BankOneMobile SMS Sending Service....BEGIN "));
                        //string mfbCode = ConfigurationManager.AppSettings["InstitutionCode"];
                        //IInstitution institution = null;
                        List<ICustomerRegistration> pendingSMSes = new List<ICustomerRegistration>();
                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Shared))
                        {
                            pendingSMSes = new CustomerRegistrationSystem(source).GetPendingSMSes();
                        }
                        System.Diagnostics.Trace.TraceInformation(String.Format("Gotten pending SMSes. {0} found... ", pendingSMSes.Count));
                        if (pendingSMSes != null && pendingSMSes.Count > 0)
                        {
                            foreach (var smsToSend in pendingSMSes)
                            {
                                string message = string.Format("Congratulations. You have now opened a {0} from {1}:\n AccountNumber:{2}\n Mobile PIN: [{3}]", smsToSend.ProductName, smsToSend.InstitutionName, smsToSend.NUBAN, smsToSend.ActivationCode);
                                bool sent = new MobileAccountSystem().SendInstitutionSMS(message, smsToSend.PhoneNumber, smsToSend.InstitutionCode, smsToSend.NUBAN);
                                System.Diagnostics.Trace.TraceInformation(string.Format("SMS Sending returned: {0}", sent));
                                if (!sent)
                                {
                                    System.Diagnostics.Trace.TraceInformation(string.Format("SMS was not sent to customer's phone. Message: {0}, Phone Number: {1}, Request ID: {2}", message, smsToSend.PhoneNumber, smsToSend.ID));
                                    try
                                    {
                                        using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                                        {
                                            string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                            string body = string.Format("Message:{0}\nAgentID:{1}\nCustomerPhoneNo: {2}", message, smsToSend.AgentID, smsToSend.PhoneNumber);
                                            client.SendEmail(string.IsNullOrEmpty(smsToSend.InstitutionCode) ? "BankOneMobile" : smsToSend.InstitutionCode, "donotreply@mybankone.com",
                                                emails, string.Format("BankOneMobile:{0}", "SMS Not Delivered To Customer"), body);
                                        }
                                    }
                                    catch (Exception emailException)
                                    {
                                        System.Diagnostics.Trace.TraceInformation(string.Format("Error while trying to send email. Exception:{0}", emailException.Message));
                                    }
                                }
                                else
                                {
                                    smsToSend.IsSmsSent = true;
                                    try
                                    {
                                        System.Diagnostics.Trace.TraceInformation(String.Format("About to update"));
                                        using (var source = DataSourceFactory.GetDataSourceWithUniqueKey(DataCategory.Core))
                                        {
                                            new CustomerRegistrationSystem(source).UpdateCustomerRegistration(smsToSend as CustomerRegistration);
                                        }
                                        System.Diagnostics.Trace.TraceInformation(String.Format("Update done"));
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Trace.TraceInformation(string.Format("SMS has been sent to the customer but could not update its status to successful. ID: {0}, Exception: {1}", smsToSend.ID, ex.ToString()));
                                    }
                                }
                            }
                        }
                        System.Diagnostics.Trace.TraceInformation(String.Format("BankOneMobile SMS Sending Service....END "));
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceInformation(String.Format("**SMS SENDING EXCEPTION** | {0}", ex.ToString()));
                        try
                        {
                            using (BankOneMobile.Services.SMSService.MessagingServiceClient client = new BankOneMobile.Services.SMSService.MessagingServiceClient())
                            {
                                string emails = "iagugua@appzonegroup.com;pdureke@appzonegroup.com";
                                string body = string.Format("Something has really gone wrong. {0}\n{1}\n{2}", ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.Message : "NO INNER EXCEPTION");
                                client.SendEmail(ConfigurationManager.AppSettings["InstitutionCode"], "donotreply@mybankone.com",
                                    emails, string.Format("BankOneMobile-CommitPoint: SMS Sending:{0}", ex.Message), body);
                            }
                        }
                        catch (Exception emailException)
                        {
                            System.Diagnostics.Trace.TraceInformation(string.Format("**SMS SENDING EXCEPTION** |Error while trying to send email. Exception:{0}", emailException.Message));
                        }
                    }
                    this.timer.Interval = 1000 * Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"]);
                }
                finally
                {
                    this.timer.Start();
                }
            }
        }
    }
}
