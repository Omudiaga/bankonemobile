﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankOneMobile.Core.Implementations;
using BankOneMobile.Core.Helpers.Enums;
using BankOneMobile.Core.Contracts;

using BankOneMobile.Core.Exceptions;
using System.Xml.Linq;


namespace BankOneMobile.ISWInterface
{
    public class Recharge
    {
        private static  Dictionary<string, Merchant> GetHardCodedRechargeMerchants()
        {
            Dictionary<string, Merchant> toReturn = new Dictionary<string, Merchant>();
            toReturn.Add("803", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "803" });//Use DisplayMessage For Prefix
            toReturn.Add("806", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "806" });
            toReturn.Add("703", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "703" });
            toReturn.Add("706", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "706" });
            toReturn.Add("813", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "813" });
            toReturn.Add("816", new Merchant { Name = "MTN", Code = "109", DisplayMessage = "816" });

            toReturn.Add("802", new Merchant { Name = "Airtel", Code = "901", DisplayMessage = "802" });
            toReturn.Add("808", new Merchant { Name = "Airtel", Code = "901", DisplayMessage = "808" });

            toReturn.Add("805", new Merchant { Name = "Globacom", Code = "913", DisplayMessage = "805" });
            toReturn.Add("807", new Merchant { Name = "Globacom", Code = "913", DisplayMessage = "807" });
            toReturn.Add("705", new Merchant { Name = "Globacom", Code = "913", DisplayMessage = "705" });
            toReturn.Add("707", new Merchant { Name = "Globacom", Code = "913", DisplayMessage = "707" });

            
            toReturn.Add("809", new Merchant { Name = "Etisalat", Code = "908", DisplayMessage = "809" });
            
            //toReturn.Add("", new Merchant { Name = "Starcom", Code = "0001", DisplayMessage = "" });
            //toReturn.Add("", new Merchant { Name = "MultiLinks", Code = "0001", DisplayMessage = "" });
            //toReturn.Add("", new Merchant { Name = "Zoom", Code = "0002", DisplayMessage = "" });
            //toReturn.Add("", new Merchant { Name = "Visafone", Code = "0003", DisplayMessage = "" });

            return toReturn;
        }
        public static  string GetMerchantCodeByPhoneNumber(string phoneNumber)
        {
            string prefix = GetPrefix(phoneNumber);
            Dictionary<string, Merchant> merchants = GetHardCodedRechargeMerchants();
            Merchant merch = merchants[prefix];
            return merch.Code;
        }
        private static string GetPrefix(string phoneNumber)
        {
            //TODO Get the prefix to use
            if (phoneNumber.StartsWith("234"))
            {
                return phoneNumber.Substring(3, 3);
            }
            else if (phoneNumber.StartsWith("+234"))
            {
                return phoneNumber.Substring(4, 3);
            }
            else
            {
                return phoneNumber.Substring(1, 3);
            }

        }


        public static string GetRechargeBillerCategory()
        {
            return "3";
        }
    }
}
