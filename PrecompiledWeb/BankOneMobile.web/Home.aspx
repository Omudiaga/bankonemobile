﻿<%@ page title="" language="C#" autoeventwireup="true" inherits="_Home, App_Web_x52rnb20" %>

<%@ Register Assembly="Coolite.Ext.Web" Namespace="Coolite.Ext.Web" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BankOne Mobile</title>
    <style type="text/css">
        .style1
        {
            width: 995px;
        }
        .style2
        {
            width: 100%;
        }
    </style>
</head>
<body>

    <script type="text/javascript">
   
    function setPageTheme(result) {
        Coolite.Ext.setTheme(result);
                        <%=  Pages.ClientID %>.iframe.dom.contentWindow.Coolite.Ext.setTheme(result);                                                        
        }
        
        var loadPage = function(node, module, iconClass) {
            var nodeTitle = module + ' - ' + node.text;
			<%=HomePage.ClientID %>.setTitle(node.text, iconClass);
			<%=HomePage.ClientID %>.autoLoad = {
                        showMask: true,
                        url: node.attributes.href.split(';')[0],
                        mode: 'iframe',
                        maskMsg: 'Loading ' + module + ' - <b>' + node.text + '</b>...'
                    };
			
			<%=HomePage.ClientID %>.reload();
			<%=Pages.ClientID %>.setActiveTab(<%=HomePage.ClientID %>);
			return;
        }
        
          var loadNewPage = function(node, module, iconClass) {

//refreshRecentAct(module,node.attributes.href.split(';')[1]);

            var tab = <%=Pages.ClientID %>.getItem(module+";"+node.attributes.href);
            var nodeTitle =  node.text;
            if (!tab) {
                tab = <%=Pages.ClientID %>.add({
                    id: module+";"+node.attributes.href,
                    title: nodeTitle,
                    closable: true,
                    iconCls: iconClass,
                    autoLoad: {
                        showMask: true,
                        url: node.attributes.href.split(';')[0],
                        mode: 'iframe',
                        maskMsg: 'Loading <b>' + node.text + '</b>...'
                    }
                });
		
            }
            <%=Pages.ClientID %>.setActiveTab(tab);
        }

var refreshTabAct = function(tabId)
{

var tabInfo = tabId.split(';');
//refreshRecentAct(tabInfo[0], tabInfo[2]);

}

var refreshRecentAct = function(appName, dataType)
{
    if(dataType)
    { 
            if(dataType == "-") 
            {

            }
            else 
            {
                var oldParam = new Object();
            }
    }
}
        
         var reLoadPage = function(node, module, iconClass) {
            var tab = <%=Pages.ClientID %>.getItem(module+";"+node.attributes.href);
            var nodeTitle = module + ' - ' + node.text;
            if (!tab) {
                loadPage(node, module, iconClass);
            }
            else {
                tab.reload();
            }
            <%=Pages.ClientID %>.setActiveTab(tab);
        }
        
        var loadNewPages = function(nodeTitle, url , iconClass)
         {   
            var tab = <%=Pages.ClientID %>.getItem(nodeTitle);
            //var nodeTitle = module + ' - ' + node;
            if (!tab) {
                tab = <%=Pages.ClientID %>.add({
                    id: url,
                    title: nodeTitle,
                    closable: true,
                    iconCls: iconClass,
                    autoLoad: {
                        showMask: true,
                        url: url,
                        mode: 'iframe',
                        maskMsg: 'Loading  <b>' + nodeTitle + '</b>...'
                    }
                });
            }
            else
            {
                tab.reload();   
            }
            
            <%=Pages.ClientID %>.setActiveTab(tab);
        }
        
        var changeHistory = function (token) {
            if (token) {
                var parts = token.split(":");
                var tabPanel = Ext.getCmp(parts[0]);
                var tabId = parts[1];
                tabPanel.show();
                tabPanel.setActiveTab(tabId);
            } else {
                // This is the initial default state.  Necessary if you navigate starting from the
                // page without any existing history token params and go back to the start state.
                <%=Pages.ClientID %>.setActiveTab(0);
            }
        }
       
       var notify = function (title, message)
       {
       Coolite.AjaxMethods.Notify(title, message);
       }
       
       
       var redirectToLoginPage = function()
       {
        Coolite.AjaxMethods.RedirectToLoginPage();
       }
       
       var closeCurrentTab = function()
       {
            <%=Pages.ClientID %>.closeCurrentTab();
       }
    </script>

    <form id="form1" runat="server">
    <ext:History runat="server" Enabled="true" ID="History1">
        <Listeners>
            <Change Fn="changeHistory" />
        </Listeners>
    </ext:History>
 
    <ext:ViewPort ID="ViewPort1" runat="server">
        <Body>
            <ext:BorderLayout ID="BorderLayout1" runat="server">
                <North Split="false" Collapsible="false" UseSplitTips="false">
 <ext:Toolbar ID="tbHeader" runat="server" Height="50" Cls="mainHeader">
                        <Items> 
                            <ext:Image runat="server" ImageUrl="~/Images/xplorer-logo.png" AlternateText='<%# System.Configuration.ConfigurationManager.AppSettings["CustomizedName"] %>' Align="Middle" AutoDataBind="true" />
                            <ext:ToolbarFill ID="fill1" runat="server" />
                            <ext:ToolbarButton ID="btnName" runat="server" Icon="StatusOnline" AutoDataBind="true" CtCls="header" StyleSpec="vertical-align:top">
                                <Menu>
                                    <ext:Menu ID="menu11" runat="server">
                                        <Items>
                                            <ext:MenuItem ID="menuProfile" Text="Profile" Icon="Magnifier" Enabled="false" />
                                           <%-- <ext:MenuItem ID="menuChangePassword" Text="Change Password" Icon="Magnifier" >
					                        <AjaxEvents>
					                            <Click OnEvent="ChangePasswordShow" />
					                        </AjaxEvents> 
					                        </ext:MenuItem>--%>
                                            <ext:MenuItem ID="menuOptions" Text="Options" Icon="Wrench">
                                                <Menu>
                                                    <ext:Menu ID="menu3" runat="server">
                                                        <Items>
                                                            <ext:MenuItem ID="menuTheme" Text="Choose a Theme...">
                                                                <Menu>
                                                                    <ext:Menu ID="menu4" runat="server">
                                                                        <Items>
                                                                            <ext:MenuItem ID="menuDefault" Text="Default">
                                                                                <Listeners>
                                                                                    <Click Handler="Coolite.Ext.setTheme('/extjs/resources/css/ext-all-embedded-css/coolite.axd');" />
                                                                                </Listeners>
                                                                            </ext:MenuItem>
                                                                            <ext:MenuItem ID="MenuItem11" Text="Gray">
                                                                                <Listeners>
                                                                                    <Click Handler="Coolite.Ext.setTheme('/extjs/resources/css/xtheme-gray-embedded-css/coolite.axd');" />
                                                                                </Listeners>
                                                                            </ext:MenuItem>
                                                                            <ext:MenuItem ID="MenuItem12" Text="Slate">
                                                                                <Listeners>
                                                                                    <Click Handler="Coolite.Ext.setTheme('/extjs/resources/css/xtheme-slate-embedded-css/coolite.axd');" />
                                                                                </Listeners>
                                                                            </ext:MenuItem>
                                                                        </Items>
                                                                    </ext:Menu>
                                                                </Menu>
                                                            </ext:MenuItem>
                                                        </Items>
                                                    </ext:Menu>
                                                </Menu>
                                            </ext:MenuItem>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:ToolbarButton>
                            <ext:ToolbarButton ID="btnHelp" runat="server" Icon="Help" Text="Help" CtCls="header">
                                <Menu>
                                    <ext:Menu ID="menu12" runat="server">
                                        <Items>
                                            <ext:MenuItem ID="menuReport" Text="Report a Defect" Icon="Bug">
                                                <Listeners>
                                                    <Click Handler="Pages.addTab({ title: el.text, url: '/Home/Form/', icon: el.iconCls });" />
                                                </Listeners>
                                            </ext:MenuItem>
                                            <ext:MenuItem ID="menuAbout" Text="About" Icon="Information">
                                                <Listeners>
                                                    <Click Handler="#{winAbout}.show();" />
                                                </Listeners>
                                            </ext:MenuItem>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:ToolbarButton>
                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server" />
                            <ext:ToolbarButton ID="btnLogout" runat="server" Icon="LockGo" Text="Logout" CtCls="header">
                                <AjaxEvents>
                                    <Click Before="return confirm('Are you sure you want to Logout?')" OnEvent="LogoutUser" Success="window.location = location.protocol;"/>
                                </AjaxEvents>
                            </ext:ToolbarButton>
                        </Items>
                    </ext:Toolbar>              
                    
                </North>
                <West MinWidth="225" MaxWidth="400" Split="true" Collapsible="true">
                    <ext:Panel ID="WestPanel" runat="server" Title="Main Menu" Width="225">
                        <Body>
                            <ext:Accordion ID="accSidePanel" runat="server" Animate="true">
                            </ext:Accordion>
                        </Body>
                    </ext:Panel>
                </West>
                <Center>
                <ext:TabPanel ID="Pages" runat="server" EnableTabScroll="true" >
                        <%--<Listeners>
                            <TabChange Handler="refreshTabAct(tab.id);if(tab.id != '#{Tab1}'){History1.add(el.id + ':' + tab.id);}" />
                        </Listeners>--%>
                        <Tabs>
                            <ext:Tab Title="Home" ID="HomePage" Closable="false" runat="server"
                                Icon="House" Header="true">
                                <AutoLoad ShowMask="true" Url="~/Dashboard.aspx" Mode="IFrame" MaskMsg="Loading Home..." />
                            </ext:Tab>                            
                        </Tabs>
       <BottomBar>
                                <%--DefaultText='<%# string.Format("Copyright &copy; {0} {1}. Powered by AppZone. Version {2}", System.Configuration.ConfigurationManager.AppSettings["CopyRightYear"],System.Configuration.ConfigurationManager.AppSettings["CopyRightName"], System.Configuration.ConfigurationManager.AppSettings["Version"] )%>'--%> 
                            <ext:StatusBar ID="StatusBar2" runat="server" 
                                AutoDataBind="true"   StatusAlign="Right">
                                <Items>
                                <ext:Image ID="Image1" runat="server" ImageUrl="~/Images/viacard_small.png" AlternateText="BankOneMobile" Align="Middle"  StyleSpec="float:left; margin-left:3px;" />
                                </Items> 
                                
                            </ext:StatusBar>
                        </BottomBar>
    </ext:TabPanel>

                   
                    
                </Center>
            </ext:BorderLayout>
        </Body>
    </ext:ViewPort>
    <ext:ScriptManager ID="ScriptManager1" runat="server" />
    <%--<uc1:ChangePassword ID="ucChangePassword" runat="server" />--%>

    </form>
</body>
</html>
