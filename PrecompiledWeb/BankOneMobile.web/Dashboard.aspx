﻿<%@ page title="" language="C#" masterpagefile="~/BankOneMobileMasterPage.master" autoeventwireup="true" inherits="Dashboard, App_Web_fy22lgww" %>

<%@ Register assembly="Coolite.Ext.Web" namespace="Coolite.Ext.Web" tagprefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Styles/main.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script type="text/javascript">

     var selectionChanged = function (dv, nodes) {
         if (nodes.length > 0) {
             var title = nodes[0].getAttribute('ext:title');
             var url = nodes[0].getAttribute('ext:url');


             window.top.loadNewPages(title, url);
         }
     }

     var viewClick = function (dv, e) {
         var group = e.getTarget('h2', 3, true);
         if (group) {
             group.up('div').toggleClass('collapsed');
         }
     }

    

    </script>
     <ext:Store ID="Store1" runat="server" AutoLoad="true" SerializationMode="Complex" OnRefreshData="RefreshHomeTabData">
        <Proxy>
            <ext:DataSourceProxy/>
        </Proxy>
        <AjaxEventConfig Method="GET"/>
        <Reader>
            <ext:JsonReader>
                <Fields>
                    <ext:RecordField Name="Key" />
                    <ext:RecordField Name="Title" />
                    <ext:RecordField Name="Url" />
                    <ext:RecordField Name="ResourceKey" />
                    <ext:RecordField Name="ChildNodes" />
                </Fields>
            </ext:JsonReader>
        </Reader>
        <Listeners>
            <%--<BeforeLoad Handler="#{tabHome}.body.mask('Loading...', 'x-mask-loading');" />
            <Load Handler="#{tabHome}.body.unmask();" />
            <LoadException Handler="#{tabHome}.body.unmask();" />--%>
        </Listeners>
    </ext:Store>
    <ext:ViewPort ID="ViewPort1" runat="server">
        <Body>
            <ext:BorderLayout ID="BorderLayout1" runat="server">
                <Center>
                                        <ext:Panel ID="ImagePanel" runat="server" Cls="images-view" Border="false" AutoScroll="true">
                                            <Body>
                                                <ext:FitLayout ID="FitLayout1" runat="server">
                                                    <ext:DataView ID="DataView1" 
                                                        IDMode="Ignore"
                                                        runat="server" 
                                                        StoreID="Store1" 
                                                        SingleSelect="true"
                                                        OverClass="x-view-over" 
                                                        ItemSelector="div.thumb-wrap" 
                                                        AutoHeight="true" 
                                                        EmptyText="No examples to display">
                                                        <Template ID="Template1" runat="server">
                                                            <div id="sample-ct">
	                                                            <tpl for=".">
	                                                                <div>
	                                                                    <a name="{Key}"></a>
	                                                                    <h2><div>{Title}</div></h2>
	                                                                    <dl>
		                                                                    <tpl for="ChildNodes">
			                                                                    <div class="thumb-wrap" ext:url="{Url}" ext:title="{Title}" ext:id="{Key}">
	                                                                                <img src="Images/{ResourceKey}" title="{Title}" />
	                                                                                <div>
	                                                                                    <H6></H6>
	                                                                                    <H4>{Title}</H4>
                                                                                        <P>{Description}</P>
	                                                                                </div>
                                                                                </div>
		                                                                    </tpl>
	                                                                        <div style="clear:left"></div>
	                                                                     </dl>
	                                                                </div>
	                                                            </tpl>
                                                            </div>
                                                        </Template>
                                                        <Listeners>
                                                           <SelectionChange Fn="selectionChanged" />
                                                            <ContainerClick Fn="viewClick" />
                                                        </Listeners>
                                                    </ext:DataView>
                                                </ext:FitLayout>
                                            </Body>
                                        </ext:Panel>
                </Center>
            </ext:BorderLayout>
        </Body>
    </ext:ViewPort>
</asp:Content>



